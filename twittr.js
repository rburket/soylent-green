// First, load the widgets.js file asynchronously
window.twttr = (function(document, script, id) {
  var js, fjs = document.getElementsByTagName(script)[0],
    t = window.twttr || {};
  if (document.getElementById(id)) return;
  js = document.createElement(script);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, "script", "twitter-wjs"));

// Define our custom event handlers
function tweetEventToAnalytics (intentEvent) {
    if(!intentEvent) return;
    console.log('tweet event');
    var related = window.open("http://vgn.techtarget.com/techtarget-ecm/services/digDeeper/content/1c1f7153962bc510VgnVCM100000ef01c80aRCRD/rows/6?select=all", "_blank", "location=yes,height=570,width=520,scrollbars=yes,status=yes");
}

// Wait for the asynchronous resources to load
twttr.ready(function (twttr) {
    console.log('twittr ready');
    // Now bind our custom intent events
    twttr.events.bind('tweet', tweetEventToAnalytics);
});
