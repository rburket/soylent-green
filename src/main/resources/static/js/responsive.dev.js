/*! responsive.dev.js - 02/10/2017 01:01 PM */
/*! JSON v3.3.0 | http://bestiejs.github.io/json3 | Copyright 2012-2014, Kit Cambridge | http://kit.mit-license.org */
(function(n){function K(p,q){function s(a){if(s[a]!==v)return s[a];var c;if("bug-string-char-index"==a)c="a"!="a"[0];else if("json"==a)c=s("json-stringify")&&s("json-parse");else{var e;if("json-stringify"==a){c=q.stringify;var b="function"==typeof c&&r;if(b){(e=function(){return 1}).toJSON=e;try{b="0"===c(0)&&"0"===c(new A)&&'""'==c(new B)&&c(t)===v&&c(v)===v&&c()===v&&"1"===c(e)&&"[1]"==c([e])&&"[null]"==c([v])&&"null"==c(null)&&"[null,null,null]"==c([v,t,null])&&'{"a":[1,true,false,null,"\\u0000\\b\\n\\f\\r\\t"]}'==
c({a:[e,!0,!1,null,"\x00\b\n\f\r\t"]})&&"1"===c(null,e)&&"[\n 1,\n 2\n]"==c([1,2],null,1)&&'"-271821-04-20T00:00:00.000Z"'==c(new D(-864E13))&&'"+275760-09-13T00:00:00.000Z"'==c(new D(864E13))&&'"-000001-01-01T00:00:00.000Z"'==c(new D(-621987552E5))&&'"1969-12-31T23:59:59.999Z"'==c(new D(-1))}catch(f){b=!1}}c=b}if("json-parse"==a){c=q.parse;if("function"==typeof c)try{if(0===c("0")&&!c(!1)){e=c('{"a":[1,true,false,null,"\\u0000\\b\\n\\f\\r\\t"]}');var l=5==e.a.length&&1===e.a[0];if(l){try{l=!c('"\t"')}catch(d){}if(l)try{l=
1!==c("01")}catch(h){}if(l)try{l=1!==c("1.")}catch(m){}}}}catch(X){l=!1}c=l}}return s[a]=!!c}p||(p=n.Object());q||(q=n.Object());var A=p.Number||n.Number,B=p.String||n.String,G=p.Object||n.Object,D=p.Date||n.Date,J=p.SyntaxError||n.SyntaxError,N=p.TypeError||n.TypeError,R=p.Math||n.Math,H=p.JSON||n.JSON;"object"==typeof H&&H&&(q.stringify=H.stringify,q.parse=H.parse);var G=G.prototype,t=G.toString,u,C,v,r=new D(-0xc782b5b800cec);try{r=-109252==r.getUTCFullYear()&&0===r.getUTCMonth()&&1===r.getUTCDate()&&
10==r.getUTCHours()&&37==r.getUTCMinutes()&&6==r.getUTCSeconds()&&708==r.getUTCMilliseconds()}catch(Y){}if(!s("json")){var E=s("bug-string-char-index");if(!r)var w=R.floor,S=[0,31,59,90,120,151,181,212,243,273,304,334],F=function(a,c){return S[c]+365*(a-1970)+w((a-1969+(c=+(1<c)))/4)-w((a-1901+c)/100)+w((a-1601+c)/400)};(u=G.hasOwnProperty)||(u=function(a){var c={},e;(c.__proto__=null,c.__proto__={toString:1},c).toString!=t?u=function(a){var c=this.__proto__;a=a in(this.__proto__=null,this);this.__proto__=
c;return a}:(e=c.constructor,u=function(a){var c=(this.constructor||e).prototype;return a in this&&!(a in c&&this[a]===c[a])});c=null;return u.call(this,a)});var T={"boolean":1,number:1,string:1,undefined:1};C=function(a,c){var e=0,b,f,l;(b=function(){this.valueOf=0}).prototype.valueOf=0;f=new b;for(l in f)u.call(f,l)&&e++;b=f=null;e?C=2==e?function(a,c){var e={},b="[object Function]"==t.call(a),f;for(f in a)b&&"prototype"==f||u.call(e,f)||!(e[f]=1)||!u.call(a,f)||c(f)}:function(a,c){var e="[object Function]"==
t.call(a),b,f;for(b in a)e&&"prototype"==b||!u.call(a,b)||(f="constructor"===b)||c(b);(f||u.call(a,b="constructor"))&&c(b)}:(f="valueOf toString toLocaleString propertyIsEnumerable isPrototypeOf hasOwnProperty constructor".split(" "),C=function(a,c){var e="[object Function]"==t.call(a),b,g;if(g=!e)if(g="function"!=typeof a.constructor)g=typeof a.hasOwnProperty,g="object"==g?!!a.hasOwnProperty:!T[g];g=g?a.hasOwnProperty:u;for(b in a)e&&"prototype"==b||!g.call(a,b)||c(b);for(e=f.length;b=f[--e];g.call(a,
b)&&c(b));});return C(a,c)};if(!s("json-stringify")){var U={92:"\\\\",34:'\\"',8:"\\b",12:"\\f",10:"\\n",13:"\\r",9:"\\t"},x=function(a,c){return("000000"+(c||0)).slice(-a)},O=function(a){for(var c='"',b=0,g=a.length,f=!E||10<g,l=f&&(E?a.split(""):a);b<g;b++){var d=a.charCodeAt(b);switch(d){case 8:case 9:case 10:case 12:case 13:case 34:case 92:c+=U[d];break;default:if(32>d){c+="\\u00"+x(2,d.toString(16));break}c+=f?l[b]:a.charAt(b)}}return c+'"'},L=function(a,c,b,g,f,l,d){var h,m,k,n,p,q,r,s,y;try{h=
c[a]}catch(z){}if("object"==typeof h&&h)if(m=t.call(h),"[object Date]"!=m||u.call(h,"toJSON"))"function"==typeof h.toJSON&&("[object Number]"!=m&&"[object String]"!=m&&"[object Array]"!=m||u.call(h,"toJSON"))&&(h=h.toJSON(a));else if(h>-1/0&&h<1/0){if(F){n=w(h/864E5);for(m=w(n/365.2425)+1970-1;F(m+1,0)<=n;m++);for(k=w((n-F(m,0))/30.42);F(m,k+1)<=n;k++);n=1+n-F(m,k);p=(h%864E5+864E5)%864E5;q=w(p/36E5)%24;r=w(p/6E4)%60;s=w(p/1E3)%60;p%=1E3}else m=h.getUTCFullYear(),k=h.getUTCMonth(),n=h.getUTCDate(),
q=h.getUTCHours(),r=h.getUTCMinutes(),s=h.getUTCSeconds(),p=h.getUTCMilliseconds();h=(0>=m||1E4<=m?(0>m?"-":"+")+x(6,0>m?-m:m):x(4,m))+"-"+x(2,k+1)+"-"+x(2,n)+"T"+x(2,q)+":"+x(2,r)+":"+x(2,s)+"."+x(3,p)+"Z"}else h=null;b&&(h=b.call(c,a,h));if(null===h)return"null";m=t.call(h);if("[object Boolean]"==m)return""+h;if("[object Number]"==m)return h>-1/0&&h<1/0?""+h:"null";if("[object String]"==m)return O(""+h);if("object"==typeof h){for(a=d.length;a--;)if(d[a]===h)throw N();d.push(h);y=[];c=l;l+=f;if("[object Array]"==
m){k=0;for(a=h.length;k<a;k++)m=L(k,h,b,g,f,l,d),y.push(m===v?"null":m);a=y.length?f?"[\n"+l+y.join(",\n"+l)+"\n"+c+"]":"["+y.join(",")+"]":"[]"}else C(g||h,function(a){var c=L(a,h,b,g,f,l,d);c!==v&&y.push(O(a)+":"+(f?" ":"")+c)}),a=y.length?f?"{\n"+l+y.join(",\n"+l)+"\n"+c+"}":"{"+y.join(",")+"}":"{}";d.pop();return a}};q.stringify=function(a,c,b){var g,f,l,d;if("function"==typeof c||"object"==typeof c&&c)if("[object Function]"==(d=t.call(c)))f=c;else if("[object Array]"==d){l={};for(var h=0,m=c.length,
k;h<m;k=c[h++],(d=t.call(k),"[object String]"==d||"[object Number]"==d)&&(l[k]=1));}if(b)if("[object Number]"==(d=t.call(b))){if(0<(b-=b%1))for(g="",10<b&&(b=10);g.length<b;g+=" ");}else"[object String]"==d&&(g=10>=b.length?b:b.slice(0,10));return L("",(k={},k[""]=a,k),f,l,g,"",[])}}if(!s("json-parse")){var V=B.fromCharCode,W={92:"\\",34:'"',47:"/",98:"\b",116:"\t",110:"\n",102:"\f",114:"\r"},b,I,k=function(){b=I=null;throw J();},z=function(){for(var a=I,c=a.length,e,g,f,l,d;b<c;)switch(d=a.charCodeAt(b),
d){case 9:case 10:case 13:case 32:b++;break;case 123:case 125:case 91:case 93:case 58:case 44:return e=E?a.charAt(b):a[b],b++,e;case 34:e="@";for(b++;b<c;)if(d=a.charCodeAt(b),32>d)k();else if(92==d)switch(d=a.charCodeAt(++b),d){case 92:case 34:case 47:case 98:case 116:case 110:case 102:case 114:e+=W[d];b++;break;case 117:g=++b;for(f=b+4;b<f;b++)d=a.charCodeAt(b),48<=d&&57>=d||97<=d&&102>=d||65<=d&&70>=d||k();e+=V("0x"+a.slice(g,b));break;default:k()}else{if(34==d)break;d=a.charCodeAt(b);for(g=b;32<=
d&&92!=d&&34!=d;)d=a.charCodeAt(++b);e+=a.slice(g,b)}if(34==a.charCodeAt(b))return b++,e;k();default:g=b;45==d&&(l=!0,d=a.charCodeAt(++b));if(48<=d&&57>=d){for(48==d&&(d=a.charCodeAt(b+1),48<=d&&57>=d)&&k();b<c&&(d=a.charCodeAt(b),48<=d&&57>=d);b++);if(46==a.charCodeAt(b)){for(f=++b;f<c&&(d=a.charCodeAt(f),48<=d&&57>=d);f++);f==b&&k();b=f}d=a.charCodeAt(b);if(101==d||69==d){d=a.charCodeAt(++b);43!=d&&45!=d||b++;for(f=b;f<c&&(d=a.charCodeAt(f),48<=d&&57>=d);f++);f==b&&k();b=f}return+a.slice(g,b)}l&&
k();if("true"==a.slice(b,b+4))return b+=4,!0;if("false"==a.slice(b,b+5))return b+=5,!1;if("null"==a.slice(b,b+4))return b+=4,null;k()}return"$"},M=function(a){var c,b;"$"==a&&k();if("string"==typeof a){if("@"==(E?a.charAt(0):a[0]))return a.slice(1);if("["==a){for(c=[];;b||(b=!0)){a=z();if("]"==a)break;b&&(","==a?(a=z(),"]"==a&&k()):k());","==a&&k();c.push(M(a))}return c}if("{"==a){for(c={};;b||(b=!0)){a=z();if("}"==a)break;b&&(","==a?(a=z(),"}"==a&&k()):k());","!=a&&"string"==typeof a&&"@"==(E?a.charAt(0):
a[0])&&":"==z()||k();c[a.slice(1)]=M(z())}return c}k()}return a},Q=function(a,b,e){e=P(a,b,e);e===v?delete a[b]:a[b]=e},P=function(a,b,e){var g=a[b],f;if("object"==typeof g&&g)if("[object Array]"==t.call(g))for(f=g.length;f--;)Q(g,f,e);else C(g,function(a){Q(g,a,e)});return e.call(a,b,g)};q.parse=function(a,c){var e,g;b=0;I=""+a;e=M(z());"$"!=z()&&k();b=I=null;return c&&"[object Function]"==t.call(c)?P((g={},g[""]=e,g),"",c):e}}}q.runInContext=K;return q}var J=typeof define==="function"&&define.amd,
A="object"==typeof global&&global;!A||A.global!==A&&A.window!==A||(n=A);if("object"!=typeof exports||!exports||exports.nodeType||J){var N=n.JSON,B=K(n,n.JSON3={noConflict:function(){n.JSON=N;return B}});n.JSON={parse:B.parse,stringify:B.stringify}}else K(n,exports);J&&define(function(){return B})})(this);
/*!

 handlebars v3.0.3

Copyright (C) 2011-2014 by Yehuda Katz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

@license
*/
!function(a,b){"object"==typeof exports&&"object"==typeof module?module.exports=b():"function"==typeof define&&define.amd?define(b):"object"==typeof exports?exports.Handlebars=b():a.Handlebars=b()}(this,function(){return function(a){function b(d){if(c[d])return c[d].exports;var e=c[d]={exports:{},id:d,loaded:!1};return a[d].call(e.exports,e,e.exports,b),e.loaded=!0,e.exports}var c={};return b.m=a,b.c=c,b.p="",b(0)}([function(a,b,c){"use strict";function d(){var a=r();return a.compile=function(b,c){return k.compile(b,c,a)},a.precompile=function(b,c){return k.precompile(b,c,a)},a.AST=i["default"],a.Compiler=k.Compiler,a.JavaScriptCompiler=m["default"],a.Parser=j.parser,a.parse=j.parse,a}var e=c(8)["default"];b.__esModule=!0;var f=c(1),g=e(f),h=c(2),i=e(h),j=c(3),k=c(4),l=c(5),m=e(l),n=c(6),o=e(n),p=c(7),q=e(p),r=g["default"].create,s=d();s.create=d,q["default"](s),s.Visitor=o["default"],s["default"]=s,b["default"]=s,a.exports=b["default"]},function(a,b,c){"use strict";function d(){var a=new h.HandlebarsEnvironment;return n.extend(a,h),a.SafeString=j["default"],a.Exception=l["default"],a.Utils=n,a.escapeExpression=n.escapeExpression,a.VM=p,a.template=function(b){return p.template(b,a)},a}var e=c(9)["default"],f=c(8)["default"];b.__esModule=!0;var g=c(10),h=e(g),i=c(11),j=f(i),k=c(12),l=f(k),m=c(13),n=e(m),o=c(14),p=e(o),q=c(7),r=f(q),s=d();s.create=d,r["default"](s),s["default"]=s,b["default"]=s,a.exports=b["default"]},function(a,b,c){"use strict";b.__esModule=!0;var d={Program:function(a,b,c,d){this.loc=d,this.type="Program",this.body=a,this.blockParams=b,this.strip=c},MustacheStatement:function(a,b,c,d,e,f){this.loc=f,this.type="MustacheStatement",this.path=a,this.params=b||[],this.hash=c,this.escaped=d,this.strip=e},BlockStatement:function(a,b,c,d,e,f,g,h,i){this.loc=i,this.type="BlockStatement",this.path=a,this.params=b||[],this.hash=c,this.program=d,this.inverse=e,this.openStrip=f,this.inverseStrip=g,this.closeStrip=h},PartialStatement:function(a,b,c,d,e){this.loc=e,this.type="PartialStatement",this.name=a,this.params=b||[],this.hash=c,this.indent="",this.strip=d},ContentStatement:function(a,b){this.loc=b,this.type="ContentStatement",this.original=this.value=a},CommentStatement:function(a,b,c){this.loc=c,this.type="CommentStatement",this.value=a,this.strip=b},SubExpression:function(a,b,c,d){this.loc=d,this.type="SubExpression",this.path=a,this.params=b||[],this.hash=c},PathExpression:function(a,b,c,d,e){this.loc=e,this.type="PathExpression",this.data=a,this.original=d,this.parts=c,this.depth=b},StringLiteral:function(a,b){this.loc=b,this.type="StringLiteral",this.original=this.value=a},NumberLiteral:function(a,b){this.loc=b,this.type="NumberLiteral",this.original=this.value=Number(a)},BooleanLiteral:function(a,b){this.loc=b,this.type="BooleanLiteral",this.original=this.value="true"===a},UndefinedLiteral:function(a){this.loc=a,this.type="UndefinedLiteral",this.original=this.value=void 0},NullLiteral:function(a){this.loc=a,this.type="NullLiteral",this.original=this.value=null},Hash:function(a,b){this.loc=b,this.type="Hash",this.pairs=a},HashPair:function(a,b,c){this.loc=c,this.type="HashPair",this.key=a,this.value=b},helpers:{helperExpression:function(a){return!("SubExpression"!==a.type&&!a.params.length&&!a.hash)},scopedId:function(a){return/^\.|this\b/.test(a.original)},simpleId:function(a){return 1===a.parts.length&&!d.helpers.scopedId(a)&&!a.depth}}};b["default"]=d,a.exports=b["default"]},function(a,b,c){"use strict";function d(a,b){if("Program"===a.type)return a;h["default"].yy=p,p.locInfo=function(a){return new p.SourceLocation(b&&b.srcName,a)};var c=new l["default"];return c.accept(h["default"].parse(a))}var e=c(8)["default"],f=c(9)["default"];b.__esModule=!0,b.parse=d;var g=c(15),h=e(g),i=c(2),j=e(i),k=c(16),l=e(k),m=c(17),n=f(m),o=c(13);b.parser=h["default"];var p={};o.extend(p,n,j["default"])},function(a,b,c){"use strict";function d(){}function e(a,b,c){if(null==a||"string"!=typeof a&&"Program"!==a.type)throw new k["default"]("You must pass a string or Handlebars AST to Handlebars.precompile. You passed "+a);b=b||{},"data"in b||(b.data=!0),b.compat&&(b.useDepths=!0);var d=c.parse(a,b),e=(new c.Compiler).compile(d,b);return(new c.JavaScriptCompiler).compile(e,b)}function f(a,b,c){function d(){var b=c.parse(a,f),d=(new c.Compiler).compile(b,f),e=(new c.JavaScriptCompiler).compile(d,f,void 0,!0);return c.template(e)}function e(a,b){return g||(g=d()),g.call(this,a,b)}var f=void 0===arguments[1]?{}:arguments[1];if(null==a||"string"!=typeof a&&"Program"!==a.type)throw new k["default"]("You must pass a string or Handlebars AST to Handlebars.compile. You passed "+a);"data"in f||(f.data=!0),f.compat&&(f.useDepths=!0);var g=void 0;return e._setup=function(a){return g||(g=d()),g._setup(a)},e._child=function(a,b,c,e){return g||(g=d()),g._child(a,b,c,e)},e}function g(a,b){if(a===b)return!0;if(l.isArray(a)&&l.isArray(b)&&a.length===b.length){for(var c=0;c<a.length;c++)if(!g(a[c],b[c]))return!1;return!0}}function h(a){if(!a.path.parts){var b=a.path;a.path=new n["default"].PathExpression(!1,0,[b.original+""],b.original+"",b.loc)}}var i=c(8)["default"];b.__esModule=!0,b.Compiler=d,b.precompile=e,b.compile=f;var j=c(12),k=i(j),l=c(13),m=c(2),n=i(m),o=[].slice;d.prototype={compiler:d,equals:function(a){var b=this.opcodes.length;if(a.opcodes.length!==b)return!1;for(var c=0;b>c;c++){var d=this.opcodes[c],e=a.opcodes[c];if(d.opcode!==e.opcode||!g(d.args,e.args))return!1}b=this.children.length;for(var c=0;b>c;c++)if(!this.children[c].equals(a.children[c]))return!1;return!0},guid:0,compile:function(a,b){this.sourceNode=[],this.opcodes=[],this.children=[],this.options=b,this.stringParams=b.stringParams,this.trackIds=b.trackIds,b.blockParams=b.blockParams||[];var c=b.knownHelpers;if(b.knownHelpers={helperMissing:!0,blockHelperMissing:!0,each:!0,"if":!0,unless:!0,"with":!0,log:!0,lookup:!0},c)for(var d in c)d in c&&(b.knownHelpers[d]=c[d]);return this.accept(a)},compileProgram:function(a){var b=new this.compiler,c=b.compile(a,this.options),d=this.guid++;return this.usePartial=this.usePartial||c.usePartial,this.children[d]=c,this.useDepths=this.useDepths||c.useDepths,d},accept:function(a){this.sourceNode.unshift(a);var b=this[a.type](a);return this.sourceNode.shift(),b},Program:function(a){this.options.blockParams.unshift(a.blockParams);for(var b=a.body,c=b.length,d=0;c>d;d++)this.accept(b[d]);return this.options.blockParams.shift(),this.isSimple=1===c,this.blockParams=a.blockParams?a.blockParams.length:0,this},BlockStatement:function(a){h(a);var b=a.program,c=a.inverse;b=b&&this.compileProgram(b),c=c&&this.compileProgram(c);var d=this.classifySexpr(a);"helper"===d?this.helperSexpr(a,b,c):"simple"===d?(this.simpleSexpr(a),this.opcode("pushProgram",b),this.opcode("pushProgram",c),this.opcode("emptyHash"),this.opcode("blockValue",a.path.original)):(this.ambiguousSexpr(a,b,c),this.opcode("pushProgram",b),this.opcode("pushProgram",c),this.opcode("emptyHash"),this.opcode("ambiguousBlockValue")),this.opcode("append")},PartialStatement:function(a){this.usePartial=!0;var b=a.params;if(b.length>1)throw new k["default"]("Unsupported number of partial arguments: "+b.length,a);b.length||b.push({type:"PathExpression",parts:[],depth:0});var c=a.name.original,d="SubExpression"===a.name.type;d&&this.accept(a.name),this.setupFullMustacheParams(a,void 0,void 0,!0);var e=a.indent||"";this.options.preventIndent&&e&&(this.opcode("appendContent",e),e=""),this.opcode("invokePartial",d,c,e),this.opcode("append")},MustacheStatement:function(a){this.SubExpression(a),this.opcode(a.escaped&&!this.options.noEscape?"appendEscaped":"append")},ContentStatement:function(a){a.value&&this.opcode("appendContent",a.value)},CommentStatement:function(){},SubExpression:function(a){h(a);var b=this.classifySexpr(a);"simple"===b?this.simpleSexpr(a):"helper"===b?this.helperSexpr(a):this.ambiguousSexpr(a)},ambiguousSexpr:function(a,b,c){var d=a.path,e=d.parts[0],f=null!=b||null!=c;this.opcode("getContext",d.depth),this.opcode("pushProgram",b),this.opcode("pushProgram",c),this.accept(d),this.opcode("invokeAmbiguous",e,f)},simpleSexpr:function(a){this.accept(a.path),this.opcode("resolvePossibleLambda")},helperSexpr:function(a,b,c){var d=this.setupFullMustacheParams(a,b,c),e=a.path,f=e.parts[0];if(this.options.knownHelpers[f])this.opcode("invokeKnownHelper",d.length,f);else{if(this.options.knownHelpersOnly)throw new k["default"]("You specified knownHelpersOnly, but used the unknown helper "+f,a);e.falsy=!0,this.accept(e),this.opcode("invokeHelper",d.length,e.original,n["default"].helpers.simpleId(e))}},PathExpression:function(a){this.addDepth(a.depth),this.opcode("getContext",a.depth);var b=a.parts[0],c=n["default"].helpers.scopedId(a),d=!a.depth&&!c&&this.blockParamIndex(b);d?this.opcode("lookupBlockParam",d,a.parts):b?a.data?(this.options.data=!0,this.opcode("lookupData",a.depth,a.parts)):this.opcode("lookupOnContext",a.parts,a.falsy,c):this.opcode("pushContext")},StringLiteral:function(a){this.opcode("pushString",a.value)},NumberLiteral:function(a){this.opcode("pushLiteral",a.value)},BooleanLiteral:function(a){this.opcode("pushLiteral",a.value)},UndefinedLiteral:function(){this.opcode("pushLiteral","undefined")},NullLiteral:function(){this.opcode("pushLiteral","null")},Hash:function(a){var b=a.pairs,c=0,d=b.length;for(this.opcode("pushHash");d>c;c++)this.pushParam(b[c].value);for(;c--;)this.opcode("assignToHash",b[c].key);this.opcode("popHash")},opcode:function(a){this.opcodes.push({opcode:a,args:o.call(arguments,1),loc:this.sourceNode[0].loc})},addDepth:function(a){a&&(this.useDepths=!0)},classifySexpr:function(a){var b=n["default"].helpers.simpleId(a.path),c=b&&!!this.blockParamIndex(a.path.parts[0]),d=!c&&n["default"].helpers.helperExpression(a),e=!c&&(d||b);if(e&&!d){var f=a.path.parts[0],g=this.options;g.knownHelpers[f]?d=!0:g.knownHelpersOnly&&(e=!1)}return d?"helper":e?"ambiguous":"simple"},pushParams:function(a){for(var b=0,c=a.length;c>b;b++)this.pushParam(a[b])},pushParam:function(a){var b=null!=a.value?a.value:a.original||"";if(this.stringParams)b.replace&&(b=b.replace(/^(\.?\.\/)*/g,"").replace(/\//g,".")),a.depth&&this.addDepth(a.depth),this.opcode("getContext",a.depth||0),this.opcode("pushStringParam",b,a.type),"SubExpression"===a.type&&this.accept(a);else{if(this.trackIds){var c=void 0;if(!a.parts||n["default"].helpers.scopedId(a)||a.depth||(c=this.blockParamIndex(a.parts[0])),c){var d=a.parts.slice(1).join(".");this.opcode("pushId","BlockParam",c,d)}else b=a.original||b,b.replace&&(b=b.replace(/^\.\//g,"").replace(/^\.$/g,"")),this.opcode("pushId",a.type,b)}this.accept(a)}},setupFullMustacheParams:function(a,b,c,d){var e=a.params;return this.pushParams(e),this.opcode("pushProgram",b),this.opcode("pushProgram",c),a.hash?this.accept(a.hash):this.opcode("emptyHash",d),e},blockParamIndex:function(a){for(var b=0,c=this.options.blockParams.length;c>b;b++){var d=this.options.blockParams[b],e=d&&l.indexOf(d,a);if(d&&e>=0)return[b,e]}}}},function(a,b,c){"use strict";function d(a){this.value=a}function e(){}function f(a,b,c,d){var e=b.popStack(),f=0,g=c.length;for(a&&g--;g>f;f++)e=b.nameLookup(e,c[f],d);return a?[b.aliasable("this.strict"),"(",e,", ",b.quotedString(c[f]),")"]:e}var g=c(8)["default"];b.__esModule=!0;var h=c(10),i=c(12),j=g(i),k=c(13),l=c(18),m=g(l);e.prototype={nameLookup:function(a,b){return e.isValidJavaScriptVariableName(b)?[a,".",b]:[a,"['",b,"']"]},depthedLookup:function(a){return[this.aliasable("this.lookup"),'(depths, "',a,'")']},compilerInfo:function(){var a=h.COMPILER_REVISION,b=h.REVISION_CHANGES[a];return[a,b]},appendToBuffer:function(a,b,c){return k.isArray(a)||(a=[a]),a=this.source.wrap(a,b),this.environment.isSimple?["return ",a,";"]:c?["buffer += ",a,";"]:(a.appendToBuffer=!0,a)},initializeBuffer:function(){return this.quotedString("")},compile:function(a,b,c,d){this.environment=a,this.options=b,this.stringParams=this.options.stringParams,this.trackIds=this.options.trackIds,this.precompile=!d,this.name=this.environment.name,this.isChild=!!c,this.context=c||{programs:[],environments:[]},this.preamble(),this.stackSlot=0,this.stackVars=[],this.aliases={},this.registers={list:[]},this.hashes=[],this.compileStack=[],this.inlineStack=[],this.blockParams=[],this.compileChildren(a,b),this.useDepths=this.useDepths||a.useDepths||this.options.compat,this.useBlockParams=this.useBlockParams||a.useBlockParams;var e=a.opcodes,f=void 0,g=void 0,h=void 0,i=void 0;for(h=0,i=e.length;i>h;h++)f=e[h],this.source.currentLocation=f.loc,g=g||f.loc,this[f.opcode].apply(this,f.args);if(this.source.currentLocation=g,this.pushSource(""),this.stackSlot||this.inlineStack.length||this.compileStack.length)throw new j["default"]("Compile completed with content left on stack");var k=this.createFunctionContext(d);if(this.isChild)return k;var l={compiler:this.compilerInfo(),main:k},m=this.context.programs;for(h=0,i=m.length;i>h;h++)m[h]&&(l[h]=m[h]);return this.environment.usePartial&&(l.usePartial=!0),this.options.data&&(l.useData=!0),this.useDepths&&(l.useDepths=!0),this.useBlockParams&&(l.useBlockParams=!0),this.options.compat&&(l.compat=!0),d?l.compilerOptions=this.options:(l.compiler=JSON.stringify(l.compiler),this.source.currentLocation={start:{line:1,column:0}},l=this.objectLiteral(l),b.srcName?(l=l.toStringWithSourceMap({file:b.destName}),l.map=l.map&&l.map.toString()):l=l.toString()),l},preamble:function(){this.lastContext=0,this.source=new m["default"](this.options.srcName)},createFunctionContext:function(a){var b="",c=this.stackVars.concat(this.registers.list);c.length>0&&(b+=", "+c.join(", "));var d=0;for(var e in this.aliases){var f=this.aliases[e];this.aliases.hasOwnProperty(e)&&f.children&&f.referenceCount>1&&(b+=", alias"+ ++d+"="+e,f.children[0]="alias"+d)}var g=["depth0","helpers","partials","data"];(this.useBlockParams||this.useDepths)&&g.push("blockParams"),this.useDepths&&g.push("depths");var h=this.mergeSource(b);return a?(g.push(h),Function.apply(this,g)):this.source.wrap(["function(",g.join(","),") {\n  ",h,"}"])},mergeSource:function(a){var b=this.environment.isSimple,c=!this.forceBuffer,d=void 0,e=void 0,f=void 0,g=void 0;return this.source.each(function(a){a.appendToBuffer?(f?a.prepend("  + "):f=a,g=a):(f&&(e?f.prepend("buffer += "):d=!0,g.add(";"),f=g=void 0),e=!0,b||(c=!1))}),c?f?(f.prepend("return "),g.add(";")):e||this.source.push('return "";'):(a+=", buffer = "+(d?"":this.initializeBuffer()),f?(f.prepend("return buffer + "),g.add(";")):this.source.push("return buffer;")),a&&this.source.prepend("var "+a.substring(2)+(d?"":";\n")),this.source.merge()},blockValue:function(a){var b=this.aliasable("helpers.blockHelperMissing"),c=[this.contextName(0)];this.setupHelperArgs(a,0,c);var d=this.popStack();c.splice(1,0,d),this.push(this.source.functionCall(b,"call",c))},ambiguousBlockValue:function(){var a=this.aliasable("helpers.blockHelperMissing"),b=[this.contextName(0)];this.setupHelperArgs("",0,b,!0),this.flushInline();var c=this.topStack();b.splice(1,0,c),this.pushSource(["if (!",this.lastHelper,") { ",c," = ",this.source.functionCall(a,"call",b),"}"])},appendContent:function(a){this.pendingContent?a=this.pendingContent+a:this.pendingLocation=this.source.currentLocation,this.pendingContent=a},append:function(){if(this.isInline())this.replaceStack(function(a){return[" != null ? ",a,' : ""']}),this.pushSource(this.appendToBuffer(this.popStack()));else{var a=this.popStack();this.pushSource(["if (",a," != null) { ",this.appendToBuffer(a,void 0,!0)," }"]),this.environment.isSimple&&this.pushSource(["else { ",this.appendToBuffer("''",void 0,!0)," }"])}},appendEscaped:function(){this.pushSource(this.appendToBuffer([this.aliasable("this.escapeExpression"),"(",this.popStack(),")"]))},getContext:function(a){this.lastContext=a},pushContext:function(){this.pushStackLiteral(this.contextName(this.lastContext))},lookupOnContext:function(a,b,c){var d=0;c||!this.options.compat||this.lastContext?this.pushContext():this.push(this.depthedLookup(a[d++])),this.resolvePath("context",a,d,b)},lookupBlockParam:function(a,b){this.useBlockParams=!0,this.push(["blockParams[",a[0],"][",a[1],"]"]),this.resolvePath("context",b,1)},lookupData:function(a,b){this.pushStackLiteral(a?"this.data(data, "+a+")":"data"),this.resolvePath("data",b,0,!0)},resolvePath:function(a,b,c,d){var e=this;if(this.options.strict||this.options.assumeObjects)return void this.push(f(this.options.strict,this,b,a));for(var g=b.length;g>c;c++)this.replaceStack(function(f){var g=e.nameLookup(f,b[c],a);return d?[" && ",g]:[" != null ? ",g," : ",f]})},resolvePossibleLambda:function(){this.push([this.aliasable("this.lambda"),"(",this.popStack(),", ",this.contextName(0),")"])},pushStringParam:function(a,b){this.pushContext(),this.pushString(b),"SubExpression"!==b&&("string"==typeof a?this.pushString(a):this.pushStackLiteral(a))},emptyHash:function(a){this.trackIds&&this.push("{}"),this.stringParams&&(this.push("{}"),this.push("{}")),this.pushStackLiteral(a?"undefined":"{}")},pushHash:function(){this.hash&&this.hashes.push(this.hash),this.hash={values:[],types:[],contexts:[],ids:[]}},popHash:function(){var a=this.hash;this.hash=this.hashes.pop(),this.trackIds&&this.push(this.objectLiteral(a.ids)),this.stringParams&&(this.push(this.objectLiteral(a.contexts)),this.push(this.objectLiteral(a.types))),this.push(this.objectLiteral(a.values))},pushString:function(a){this.pushStackLiteral(this.quotedString(a))},pushLiteral:function(a){this.pushStackLiteral(a)},pushProgram:function(a){this.pushStackLiteral(null!=a?this.programExpression(a):null)},invokeHelper:function(a,b,c){var d=this.popStack(),e=this.setupHelper(a,b),f=c?[e.name," || "]:"",g=["("].concat(f,d);this.options.strict||g.push(" || ",this.aliasable("helpers.helperMissing")),g.push(")"),this.push(this.source.functionCall(g,"call",e.callParams))},invokeKnownHelper:function(a,b){var c=this.setupHelper(a,b);this.push(this.source.functionCall(c.name,"call",c.callParams))},invokeAmbiguous:function(a,b){this.useRegister("helper");var c=this.popStack();this.emptyHash();var d=this.setupHelper(0,a,b),e=this.lastHelper=this.nameLookup("helpers",a,"helper"),f=["(","(helper = ",e," || ",c,")"];this.options.strict||(f[0]="(helper = ",f.push(" != null ? helper : ",this.aliasable("helpers.helperMissing"))),this.push(["(",f,d.paramsInit?["),(",d.paramsInit]:[],"),","(typeof helper === ",this.aliasable('"function"')," ? ",this.source.functionCall("helper","call",d.callParams)," : helper))"])},invokePartial:function(a,b,c){var d=[],e=this.setupParams(b,1,d,!1);a&&(b=this.popStack(),delete e.name),c&&(e.indent=JSON.stringify(c)),e.helpers="helpers",e.partials="partials",d.unshift(a?b:this.nameLookup("partials",b,"partial")),this.options.compat&&(e.depths="depths"),e=this.objectLiteral(e),d.push(e),this.push(this.source.functionCall("this.invokePartial","",d))},assignToHash:function(a){var b=this.popStack(),c=void 0,d=void 0,e=void 0;this.trackIds&&(e=this.popStack()),this.stringParams&&(d=this.popStack(),c=this.popStack());var f=this.hash;c&&(f.contexts[a]=c),d&&(f.types[a]=d),e&&(f.ids[a]=e),f.values[a]=b},pushId:function(a,b,c){"BlockParam"===a?this.pushStackLiteral("blockParams["+b[0]+"].path["+b[1]+"]"+(c?" + "+JSON.stringify("."+c):"")):"PathExpression"===a?this.pushString(b):this.pushStackLiteral("SubExpression"===a?"true":"null")},compiler:e,compileChildren:function(a,b){for(var c=a.children,d=void 0,e=void 0,f=0,g=c.length;g>f;f++){d=c[f],e=new this.compiler;var h=this.matchExistingProgram(d);null==h?(this.context.programs.push(""),h=this.context.programs.length,d.index=h,d.name="program"+h,this.context.programs[h]=e.compile(d,b,this.context,!this.precompile),this.context.environments[h]=d,this.useDepths=this.useDepths||e.useDepths,this.useBlockParams=this.useBlockParams||e.useBlockParams):(d.index=h,d.name="program"+h,this.useDepths=this.useDepths||d.useDepths,this.useBlockParams=this.useBlockParams||d.useBlockParams)}},matchExistingProgram:function(a){for(var b=0,c=this.context.environments.length;c>b;b++){var d=this.context.environments[b];if(d&&d.equals(a))return b}},programExpression:function(a){var b=this.environment.children[a],c=[b.index,"data",b.blockParams];return(this.useBlockParams||this.useDepths)&&c.push("blockParams"),this.useDepths&&c.push("depths"),"this.program("+c.join(", ")+")"},useRegister:function(a){this.registers[a]||(this.registers[a]=!0,this.registers.list.push(a))},push:function(a){return a instanceof d||(a=this.source.wrap(a)),this.inlineStack.push(a),a},pushStackLiteral:function(a){this.push(new d(a))},pushSource:function(a){this.pendingContent&&(this.source.push(this.appendToBuffer(this.source.quotedString(this.pendingContent),this.pendingLocation)),this.pendingContent=void 0),a&&this.source.push(a)},replaceStack:function(a){var b=["("],c=void 0,e=void 0,f=void 0;if(!this.isInline())throw new j["default"]("replaceStack on non-inline");var g=this.popStack(!0);if(g instanceof d)c=[g.value],b=["(",c],f=!0;else{e=!0;var h=this.incrStack();b=["((",this.push(h)," = ",g,")"],c=this.topStack()}var i=a.call(this,c);f||this.popStack(),e&&this.stackSlot--,this.push(b.concat(i,")"))},incrStack:function(){return this.stackSlot++,this.stackSlot>this.stackVars.length&&this.stackVars.push("stack"+this.stackSlot),this.topStackName()},topStackName:function(){return"stack"+this.stackSlot},flushInline:function(){var a=this.inlineStack;this.inlineStack=[];for(var b=0,c=a.length;c>b;b++){var e=a[b];if(e instanceof d)this.compileStack.push(e);else{var f=this.incrStack();this.pushSource([f," = ",e,";"]),this.compileStack.push(f)}}},isInline:function(){return this.inlineStack.length},popStack:function(a){var b=this.isInline(),c=(b?this.inlineStack:this.compileStack).pop();if(!a&&c instanceof d)return c.value;if(!b){if(!this.stackSlot)throw new j["default"]("Invalid stack pop");this.stackSlot--}return c},topStack:function(){var a=this.isInline()?this.inlineStack:this.compileStack,b=a[a.length-1];return b instanceof d?b.value:b},contextName:function(a){return this.useDepths&&a?"depths["+a+"]":"depth"+a},quotedString:function(a){return this.source.quotedString(a)},objectLiteral:function(a){return this.source.objectLiteral(a)},aliasable:function(a){var b=this.aliases[a];return b?(b.referenceCount++,b):(b=this.aliases[a]=this.source.wrap(a),b.aliasable=!0,b.referenceCount=1,b)},setupHelper:function(a,b,c){var d=[],e=this.setupHelperArgs(b,a,d,c),f=this.nameLookup("helpers",b,"helper");return{params:d,paramsInit:e,name:f,callParams:[this.contextName(0)].concat(d)}},setupParams:function(a,b,c){var d={},e=[],f=[],g=[],h=void 0;d.name=this.quotedString(a),d.hash=this.popStack(),this.trackIds&&(d.hashIds=this.popStack()),this.stringParams&&(d.hashTypes=this.popStack(),d.hashContexts=this.popStack());var i=this.popStack(),j=this.popStack();(j||i)&&(d.fn=j||"this.noop",d.inverse=i||"this.noop");for(var k=b;k--;)h=this.popStack(),c[k]=h,this.trackIds&&(g[k]=this.popStack()),this.stringParams&&(f[k]=this.popStack(),e[k]=this.popStack());return this.trackIds&&(d.ids=this.source.generateArray(g)),this.stringParams&&(d.types=this.source.generateArray(f),d.contexts=this.source.generateArray(e)),this.options.data&&(d.data="data"),this.useBlockParams&&(d.blockParams="blockParams"),d},setupHelperArgs:function(a,b,c,d){var e=this.setupParams(a,b,c,!0);return e=this.objectLiteral(e),d?(this.useRegister("options"),c.push("options"),["options=",e]):(c.push(e),"")}},function(){for(var a="break else new var case finally return void catch for switch while continue function this with default if throw delete in try do instanceof typeof abstract enum int short boolean export interface static byte extends long super char final native synchronized class float package throws const goto private transient debugger implements protected volatile double import public let yield await null true false".split(" "),b=e.RESERVED_WORDS={},c=0,d=a.length;d>c;c++)b[a[c]]=!0}(),e.isValidJavaScriptVariableName=function(a){return!e.RESERVED_WORDS[a]&&/^[a-zA-Z_$][0-9a-zA-Z_$]*$/.test(a)},b["default"]=e,a.exports=b["default"]},function(a,b,c){"use strict";function d(){this.parents=[]}var e=c(8)["default"];b.__esModule=!0;var f=c(12),g=e(f),h=c(2),i=e(h);d.prototype={constructor:d,mutating:!1,acceptKey:function(a,b){var c=this.accept(a[b]);if(this.mutating){if(c&&(!c.type||!i["default"][c.type]))throw new g["default"]('Unexpected node type "'+c.type+'" found when accepting '+b+" on "+a.type);a[b]=c}},acceptRequired:function(a,b){if(this.acceptKey(a,b),!a[b])throw new g["default"](a.type+" requires "+b)},acceptArray:function(a){for(var b=0,c=a.length;c>b;b++)this.acceptKey(a,b),a[b]||(a.splice(b,1),b--,c--)},accept:function(a){if(a){this.current&&this.parents.unshift(this.current),this.current=a;var b=this[a.type](a);return this.current=this.parents.shift(),!this.mutating||b?b:b!==!1?a:void 0}},Program:function(a){this.acceptArray(a.body)},MustacheStatement:function(a){this.acceptRequired(a,"path"),this.acceptArray(a.params),this.acceptKey(a,"hash")},BlockStatement:function(a){this.acceptRequired(a,"path"),this.acceptArray(a.params),this.acceptKey(a,"hash"),this.acceptKey(a,"program"),this.acceptKey(a,"inverse")},PartialStatement:function(a){this.acceptRequired(a,"name"),this.acceptArray(a.params),this.acceptKey(a,"hash")},ContentStatement:function(){},CommentStatement:function(){},SubExpression:function(a){this.acceptRequired(a,"path"),this.acceptArray(a.params),this.acceptKey(a,"hash")},PathExpression:function(){},StringLiteral:function(){},NumberLiteral:function(){},BooleanLiteral:function(){},UndefinedLiteral:function(){},NullLiteral:function(){},Hash:function(a){this.acceptArray(a.pairs)},HashPair:function(a){this.acceptRequired(a,"value")}},b["default"]=d,a.exports=b["default"]},function(a,b,c){(function(c){"use strict";b.__esModule=!0,b["default"]=function(a){var b="undefined"!=typeof c?c:window,d=b.Handlebars;a.noConflict=function(){b.Handlebars===a&&(b.Handlebars=d)}},a.exports=b["default"]}).call(b,function(){return this}())},function(a,b,c){"use strict";b["default"]=function(a){return a&&a.__esModule?a:{"default":a}},b.__esModule=!0},function(a,b,c){"use strict";b["default"]=function(a){if(a&&a.__esModule)return a;var b={};if("object"==typeof a&&null!==a)for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&(b[c]=a[c]);return b["default"]=a,b},b.__esModule=!0},function(a,b,c){"use strict";function d(a,b){this.helpers=a||{},this.partials=b||{},e(this)}function e(a){a.registerHelper("helperMissing",function(){if(1===arguments.length)return void 0;throw new l["default"]('Missing helper: "'+arguments[arguments.length-1].name+'"')}),a.registerHelper("blockHelperMissing",function(b,c){var d=c.inverse,e=c.fn;if(b===!0)return e(this);if(b===!1||null==b)return d(this);if(p(b))return b.length>0?(c.ids&&(c.ids=[c.name]),a.helpers.each(b,c)):d(this);if(c.data&&c.ids){var g=f(c.data);g.contextPath=j.appendContextPath(c.data.contextPath,c.name),c={data:g}}return e(b,c)}),a.registerHelper("each",function(a,b){function c(b,c,e){i&&(i.key=b,i.index=c,i.first=0===c,i.last=!!e,k&&(i.contextPath=k+b)),h+=d(a[b],{data:i,blockParams:j.blockParams([a[b],b],[k+b,null])})}if(!b)throw new l["default"]("Must pass iterator to #each");var d=b.fn,e=b.inverse,g=0,h="",i=void 0,k=void 0;if(b.data&&b.ids&&(k=j.appendContextPath(b.data.contextPath,b.ids[0])+"."),q(a)&&(a=a.call(this)),b.data&&(i=f(b.data)),a&&"object"==typeof a)if(p(a))for(var m=a.length;m>g;g++)c(g,g,g===a.length-1);else{var n=void 0;for(var o in a)a.hasOwnProperty(o)&&(n&&c(n,g-1),n=o,g++);n&&c(n,g-1,!0)}return 0===g&&(h=e(this)),h}),a.registerHelper("if",function(a,b){return q(a)&&(a=a.call(this)),!b.hash.includeZero&&!a||j.isEmpty(a)?b.inverse(this):b.fn(this)}),a.registerHelper("unless",function(b,c){return a.helpers["if"].call(this,b,{fn:c.inverse,inverse:c.fn,hash:c.hash})}),a.registerHelper("with",function(a,b){q(a)&&(a=a.call(this));var c=b.fn;if(j.isEmpty(a))return b.inverse(this);if(b.data&&b.ids){var d=f(b.data);d.contextPath=j.appendContextPath(b.data.contextPath,b.ids[0]),b={data:d}}return c(a,b)}),a.registerHelper("log",function(b,c){var d=c.data&&null!=c.data.level?parseInt(c.data.level,10):1;a.log(d,b)}),a.registerHelper("lookup",function(a,b){return a&&a[b]})}function f(a){var b=j.extend({},a);return b._parent=a,b}var g=c(9)["default"],h=c(8)["default"];b.__esModule=!0,b.HandlebarsEnvironment=d,b.createFrame=f;var i=c(13),j=g(i),k=c(12),l=h(k),m="3.0.1";b.VERSION=m;var n=6;b.COMPILER_REVISION=n;var o={1:"<= 1.0.rc.2",2:"== 1.0.0-rc.3",3:"== 1.0.0-rc.4",4:"== 1.x.x",5:"== 2.0.0-alpha.x",6:">= 2.0.0-beta.1"};b.REVISION_CHANGES=o;var p=j.isArray,q=j.isFunction,r=j.toString,s="[object Object]";d.prototype={constructor:d,logger:t,log:u,registerHelper:function(a,b){if(r.call(a)===s){if(b)throw new l["default"]("Arg not supported with multiple helpers");j.extend(this.helpers,a)}else this.helpers[a]=b},unregisterHelper:function(a){delete this.helpers[a]},registerPartial:function(a,b){if(r.call(a)===s)j.extend(this.partials,a);else{if("undefined"==typeof b)throw new l["default"]("Attempting to register a partial as undefined");this.partials[a]=b}},unregisterPartial:function(a){delete this.partials[a]}};var t={methodMap:{0:"debug",1:"info",2:"warn",3:"error"},DEBUG:0,INFO:1,WARN:2,ERROR:3,level:1,log:function(a,b){if("undefined"!=typeof console&&t.level<=a){var c=t.methodMap[a];(console[c]||console.log).call(console,b)}}};b.logger=t;var u=t.log;b.log=u},function(a,b,c){"use strict";function d(a){this.string=a}b.__esModule=!0,d.prototype.toString=d.prototype.toHTML=function(){return""+this.string},b["default"]=d,a.exports=b["default"]},function(a,b,c){"use strict";function d(a,b){var c=b&&b.loc,f=void 0,g=void 0;c&&(f=c.start.line,g=c.start.column,a+=" - "+f+":"+g);for(var h=Error.prototype.constructor.call(this,a),i=0;i<e.length;i++)this[e[i]]=h[e[i]];Error.captureStackTrace&&Error.captureStackTrace(this,d),c&&(this.lineNumber=f,this.column=g)}b.__esModule=!0;var e=["description","fileName","lineNumber","message","name","number","stack"];d.prototype=new Error,b["default"]=d,a.exports=b["default"]},function(a,b,c){"use strict";function d(a){return k[a]}function e(a){for(var b=1;b<arguments.length;b++)for(var c in arguments[b])Object.prototype.hasOwnProperty.call(arguments[b],c)&&(a[c]=arguments[b][c]);return a}function f(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1}function g(a){if("string"!=typeof a){if(a&&a.toHTML)return a.toHTML();if(null==a)return"";if(!a)return a+"";a=""+a}return m.test(a)?a.replace(l,d):a}function h(a){return a||0===a?p(a)&&0===a.length?!0:!1:!0}function i(a,b){return a.path=b,a}function j(a,b){return(a?a+".":"")+b}b.__esModule=!0,b.extend=e,b.indexOf=f,b.escapeExpression=g,b.isEmpty=h,b.blockParams=i,b.appendContextPath=j;var k={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","`":"&#x60;"},l=/[&<>"'`]/g,m=/[&<>"'`]/,n=Object.prototype.toString;b.toString=n;var o=function(a){return"function"==typeof a};o(/x/)&&(b.isFunction=o=function(a){return"function"==typeof a&&"[object Function]"===n.call(a)});var o;b.isFunction=o;var p=Array.isArray||function(a){return a&&"object"==typeof a?"[object Array]"===n.call(a):!1};b.isArray=p},function(a,b,c){"use strict";function d(a){var b=a&&a[0]||1,c=q.COMPILER_REVISION;if(b!==c){if(c>b){var d=q.REVISION_CHANGES[c],e=q.REVISION_CHANGES[b];throw new p["default"]("Template was precompiled with an older version of Handlebars than the current runtime. Please update your precompiler to a newer version ("+d+") or downgrade your runtime to an older version ("+e+").")}throw new p["default"]("Template was precompiled with a newer version of Handlebars than the current runtime. Please update your runtime to a newer version ("+a[1]+").")}}function e(a,b){function c(c,d,e){e.hash&&(d=n.extend({},d,e.hash)),c=b.VM.resolvePartial.call(this,c,d,e);var f=b.VM.invokePartial.call(this,c,d,e);if(null==f&&b.compile&&(e.partials[e.name]=b.compile(c,a.compilerOptions,b),
f=e.partials[e.name](d,e)),null!=f){if(e.indent){for(var g=f.split("\n"),h=0,i=g.length;i>h&&(g[h]||h+1!==i);h++)g[h]=e.indent+g[h];f=g.join("\n")}return f}throw new p["default"]("The partial "+e.name+" could not be compiled when running in runtime-only mode")}function d(b){var c=void 0===arguments[1]?{}:arguments[1],f=c.data;d._setup(c),!c.partial&&a.useData&&(f=j(b,f));var g=void 0,h=a.useBlockParams?[]:void 0;return a.useDepths&&(g=c.depths?[b].concat(c.depths):[b]),a.main.call(e,b,e.helpers,e.partials,f,h,g)}if(!b)throw new p["default"]("No environment passed to template");if(!a||!a.main)throw new p["default"]("Unknown template object: "+typeof a);b.VM.checkRevision(a.compiler);var e={strict:function(a,b){if(!(b in a))throw new p["default"]('"'+b+'" not defined in '+a);return a[b]},lookup:function(a,b){for(var c=a.length,d=0;c>d;d++)if(a[d]&&null!=a[d][b])return a[d][b]},lambda:function(a,b){return"function"==typeof a?a.call(b):a},escapeExpression:n.escapeExpression,invokePartial:c,fn:function(b){return a[b]},programs:[],program:function(a,b,c,d,e){var g=this.programs[a],h=this.fn(a);return b||e||d||c?g=f(this,a,h,b,c,d,e):g||(g=this.programs[a]=f(this,a,h)),g},data:function(a,b){for(;a&&b--;)a=a._parent;return a},merge:function(a,b){var c=a||b;return a&&b&&a!==b&&(c=n.extend({},b,a)),c},noop:b.VM.noop,compilerInfo:a.compiler};return d.isTop=!0,d._setup=function(c){c.partial?(e.helpers=c.helpers,e.partials=c.partials):(e.helpers=e.merge(c.helpers,b.helpers),a.usePartial&&(e.partials=e.merge(c.partials,b.partials)))},d._child=function(b,c,d,g){if(a.useBlockParams&&!d)throw new p["default"]("must pass block params");if(a.useDepths&&!g)throw new p["default"]("must pass parent depths");return f(e,b,a[b],c,0,d,g)},d}function f(a,b,c,d,e,f,g){function h(b){var e=void 0===arguments[1]?{}:arguments[1];return c.call(a,b,a.helpers,a.partials,e.data||d,f&&[e.blockParams].concat(f),g&&[b].concat(g))}return h.program=b,h.depth=g?g.length:0,h.blockParams=e||0,h}function g(a,b,c){return a?a.call||c.name||(c.name=a,a=c.partials[a]):a=c.partials[c.name],a}function h(a,b,c){if(c.partial=!0,void 0===a)throw new p["default"]("The partial "+c.name+" could not be found");return a instanceof Function?a(b,c):void 0}function i(){return""}function j(a,b){return b&&"root"in b||(b=b?q.createFrame(b):{},b.root=a),b}var k=c(9)["default"],l=c(8)["default"];b.__esModule=!0,b.checkRevision=d,b.template=e,b.wrapProgram=f,b.resolvePartial=g,b.invokePartial=h,b.noop=i;var m=c(13),n=k(m),o=c(12),p=l(o),q=c(10)},function(a,b,c){"use strict";b.__esModule=!0;var d=function(){function a(){this.yy={}}var b={trace:function(){},yy:{},symbols_:{error:2,root:3,program:4,EOF:5,program_repetition0:6,statement:7,mustache:8,block:9,rawBlock:10,partial:11,content:12,COMMENT:13,CONTENT:14,openRawBlock:15,END_RAW_BLOCK:16,OPEN_RAW_BLOCK:17,helperName:18,openRawBlock_repetition0:19,openRawBlock_option0:20,CLOSE_RAW_BLOCK:21,openBlock:22,block_option0:23,closeBlock:24,openInverse:25,block_option1:26,OPEN_BLOCK:27,openBlock_repetition0:28,openBlock_option0:29,openBlock_option1:30,CLOSE:31,OPEN_INVERSE:32,openInverse_repetition0:33,openInverse_option0:34,openInverse_option1:35,openInverseChain:36,OPEN_INVERSE_CHAIN:37,openInverseChain_repetition0:38,openInverseChain_option0:39,openInverseChain_option1:40,inverseAndProgram:41,INVERSE:42,inverseChain:43,inverseChain_option0:44,OPEN_ENDBLOCK:45,OPEN:46,mustache_repetition0:47,mustache_option0:48,OPEN_UNESCAPED:49,mustache_repetition1:50,mustache_option1:51,CLOSE_UNESCAPED:52,OPEN_PARTIAL:53,partialName:54,partial_repetition0:55,partial_option0:56,param:57,sexpr:58,OPEN_SEXPR:59,sexpr_repetition0:60,sexpr_option0:61,CLOSE_SEXPR:62,hash:63,hash_repetition_plus0:64,hashSegment:65,ID:66,EQUALS:67,blockParams:68,OPEN_BLOCK_PARAMS:69,blockParams_repetition_plus0:70,CLOSE_BLOCK_PARAMS:71,path:72,dataName:73,STRING:74,NUMBER:75,BOOLEAN:76,UNDEFINED:77,NULL:78,DATA:79,pathSegments:80,SEP:81,$accept:0,$end:1},terminals_:{2:"error",5:"EOF",13:"COMMENT",14:"CONTENT",16:"END_RAW_BLOCK",17:"OPEN_RAW_BLOCK",21:"CLOSE_RAW_BLOCK",27:"OPEN_BLOCK",31:"CLOSE",32:"OPEN_INVERSE",37:"OPEN_INVERSE_CHAIN",42:"INVERSE",45:"OPEN_ENDBLOCK",46:"OPEN",49:"OPEN_UNESCAPED",52:"CLOSE_UNESCAPED",53:"OPEN_PARTIAL",59:"OPEN_SEXPR",62:"CLOSE_SEXPR",66:"ID",67:"EQUALS",69:"OPEN_BLOCK_PARAMS",71:"CLOSE_BLOCK_PARAMS",74:"STRING",75:"NUMBER",76:"BOOLEAN",77:"UNDEFINED",78:"NULL",79:"DATA",81:"SEP"},productions_:[0,[3,2],[4,1],[7,1],[7,1],[7,1],[7,1],[7,1],[7,1],[12,1],[10,3],[15,5],[9,4],[9,4],[22,6],[25,6],[36,6],[41,2],[43,3],[43,1],[24,3],[8,5],[8,5],[11,5],[57,1],[57,1],[58,5],[63,1],[65,3],[68,3],[18,1],[18,1],[18,1],[18,1],[18,1],[18,1],[18,1],[54,1],[54,1],[73,2],[72,1],[80,3],[80,1],[6,0],[6,2],[19,0],[19,2],[20,0],[20,1],[23,0],[23,1],[26,0],[26,1],[28,0],[28,2],[29,0],[29,1],[30,0],[30,1],[33,0],[33,2],[34,0],[34,1],[35,0],[35,1],[38,0],[38,2],[39,0],[39,1],[40,0],[40,1],[44,0],[44,1],[47,0],[47,2],[48,0],[48,1],[50,0],[50,2],[51,0],[51,1],[55,0],[55,2],[56,0],[56,1],[60,0],[60,2],[61,0],[61,1],[64,1],[64,2],[70,1],[70,2]],performAction:function(a,b,c,d,e,f,g){var h=f.length-1;switch(e){case 1:return f[h-1];case 2:this.$=new d.Program(f[h],null,{},d.locInfo(this._$));break;case 3:this.$=f[h];break;case 4:this.$=f[h];break;case 5:this.$=f[h];break;case 6:this.$=f[h];break;case 7:this.$=f[h];break;case 8:this.$=new d.CommentStatement(d.stripComment(f[h]),d.stripFlags(f[h],f[h]),d.locInfo(this._$));break;case 9:this.$=new d.ContentStatement(f[h],d.locInfo(this._$));break;case 10:this.$=d.prepareRawBlock(f[h-2],f[h-1],f[h],this._$);break;case 11:this.$={path:f[h-3],params:f[h-2],hash:f[h-1]};break;case 12:this.$=d.prepareBlock(f[h-3],f[h-2],f[h-1],f[h],!1,this._$);break;case 13:this.$=d.prepareBlock(f[h-3],f[h-2],f[h-1],f[h],!0,this._$);break;case 14:this.$={path:f[h-4],params:f[h-3],hash:f[h-2],blockParams:f[h-1],strip:d.stripFlags(f[h-5],f[h])};break;case 15:this.$={path:f[h-4],params:f[h-3],hash:f[h-2],blockParams:f[h-1],strip:d.stripFlags(f[h-5],f[h])};break;case 16:this.$={path:f[h-4],params:f[h-3],hash:f[h-2],blockParams:f[h-1],strip:d.stripFlags(f[h-5],f[h])};break;case 17:this.$={strip:d.stripFlags(f[h-1],f[h-1]),program:f[h]};break;case 18:var i=d.prepareBlock(f[h-2],f[h-1],f[h],f[h],!1,this._$),j=new d.Program([i],null,{},d.locInfo(this._$));j.chained=!0,this.$={strip:f[h-2].strip,program:j,chain:!0};break;case 19:this.$=f[h];break;case 20:this.$={path:f[h-1],strip:d.stripFlags(f[h-2],f[h])};break;case 21:this.$=d.prepareMustache(f[h-3],f[h-2],f[h-1],f[h-4],d.stripFlags(f[h-4],f[h]),this._$);break;case 22:this.$=d.prepareMustache(f[h-3],f[h-2],f[h-1],f[h-4],d.stripFlags(f[h-4],f[h]),this._$);break;case 23:this.$=new d.PartialStatement(f[h-3],f[h-2],f[h-1],d.stripFlags(f[h-4],f[h]),d.locInfo(this._$));break;case 24:this.$=f[h];break;case 25:this.$=f[h];break;case 26:this.$=new d.SubExpression(f[h-3],f[h-2],f[h-1],d.locInfo(this._$));break;case 27:this.$=new d.Hash(f[h],d.locInfo(this._$));break;case 28:this.$=new d.HashPair(d.id(f[h-2]),f[h],d.locInfo(this._$));break;case 29:this.$=d.id(f[h-1]);break;case 30:this.$=f[h];break;case 31:this.$=f[h];break;case 32:this.$=new d.StringLiteral(f[h],d.locInfo(this._$));break;case 33:this.$=new d.NumberLiteral(f[h],d.locInfo(this._$));break;case 34:this.$=new d.BooleanLiteral(f[h],d.locInfo(this._$));break;case 35:this.$=new d.UndefinedLiteral(d.locInfo(this._$));break;case 36:this.$=new d.NullLiteral(d.locInfo(this._$));break;case 37:this.$=f[h];break;case 38:this.$=f[h];break;case 39:this.$=d.preparePath(!0,f[h],this._$);break;case 40:this.$=d.preparePath(!1,f[h],this._$);break;case 41:f[h-2].push({part:d.id(f[h]),original:f[h],separator:f[h-1]}),this.$=f[h-2];break;case 42:this.$=[{part:d.id(f[h]),original:f[h]}];break;case 43:this.$=[];break;case 44:f[h-1].push(f[h]);break;case 45:this.$=[];break;case 46:f[h-1].push(f[h]);break;case 53:this.$=[];break;case 54:f[h-1].push(f[h]);break;case 59:this.$=[];break;case 60:f[h-1].push(f[h]);break;case 65:this.$=[];break;case 66:f[h-1].push(f[h]);break;case 73:this.$=[];break;case 74:f[h-1].push(f[h]);break;case 77:this.$=[];break;case 78:f[h-1].push(f[h]);break;case 81:this.$=[];break;case 82:f[h-1].push(f[h]);break;case 85:this.$=[];break;case 86:f[h-1].push(f[h]);break;case 89:this.$=[f[h]];break;case 90:f[h-1].push(f[h]);break;case 91:this.$=[f[h]];break;case 92:f[h-1].push(f[h])}},table:[{3:1,4:2,5:[2,43],6:3,13:[2,43],14:[2,43],17:[2,43],27:[2,43],32:[2,43],46:[2,43],49:[2,43],53:[2,43]},{1:[3]},{5:[1,4]},{5:[2,2],7:5,8:6,9:7,10:8,11:9,12:10,13:[1,11],14:[1,18],15:16,17:[1,21],22:14,25:15,27:[1,19],32:[1,20],37:[2,2],42:[2,2],45:[2,2],46:[1,12],49:[1,13],53:[1,17]},{1:[2,1]},{5:[2,44],13:[2,44],14:[2,44],17:[2,44],27:[2,44],32:[2,44],37:[2,44],42:[2,44],45:[2,44],46:[2,44],49:[2,44],53:[2,44]},{5:[2,3],13:[2,3],14:[2,3],17:[2,3],27:[2,3],32:[2,3],37:[2,3],42:[2,3],45:[2,3],46:[2,3],49:[2,3],53:[2,3]},{5:[2,4],13:[2,4],14:[2,4],17:[2,4],27:[2,4],32:[2,4],37:[2,4],42:[2,4],45:[2,4],46:[2,4],49:[2,4],53:[2,4]},{5:[2,5],13:[2,5],14:[2,5],17:[2,5],27:[2,5],32:[2,5],37:[2,5],42:[2,5],45:[2,5],46:[2,5],49:[2,5],53:[2,5]},{5:[2,6],13:[2,6],14:[2,6],17:[2,6],27:[2,6],32:[2,6],37:[2,6],42:[2,6],45:[2,6],46:[2,6],49:[2,6],53:[2,6]},{5:[2,7],13:[2,7],14:[2,7],17:[2,7],27:[2,7],32:[2,7],37:[2,7],42:[2,7],45:[2,7],46:[2,7],49:[2,7],53:[2,7]},{5:[2,8],13:[2,8],14:[2,8],17:[2,8],27:[2,8],32:[2,8],37:[2,8],42:[2,8],45:[2,8],46:[2,8],49:[2,8],53:[2,8]},{18:22,66:[1,32],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{18:33,66:[1,32],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{4:34,6:3,13:[2,43],14:[2,43],17:[2,43],27:[2,43],32:[2,43],37:[2,43],42:[2,43],45:[2,43],46:[2,43],49:[2,43],53:[2,43]},{4:35,6:3,13:[2,43],14:[2,43],17:[2,43],27:[2,43],32:[2,43],42:[2,43],45:[2,43],46:[2,43],49:[2,43],53:[2,43]},{12:36,14:[1,18]},{18:38,54:37,58:39,59:[1,40],66:[1,32],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{5:[2,9],13:[2,9],14:[2,9],16:[2,9],17:[2,9],27:[2,9],32:[2,9],37:[2,9],42:[2,9],45:[2,9],46:[2,9],49:[2,9],53:[2,9]},{18:41,66:[1,32],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{18:42,66:[1,32],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{18:43,66:[1,32],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{31:[2,73],47:44,59:[2,73],66:[2,73],74:[2,73],75:[2,73],76:[2,73],77:[2,73],78:[2,73],79:[2,73]},{21:[2,30],31:[2,30],52:[2,30],59:[2,30],62:[2,30],66:[2,30],69:[2,30],74:[2,30],75:[2,30],76:[2,30],77:[2,30],78:[2,30],79:[2,30]},{21:[2,31],31:[2,31],52:[2,31],59:[2,31],62:[2,31],66:[2,31],69:[2,31],74:[2,31],75:[2,31],76:[2,31],77:[2,31],78:[2,31],79:[2,31]},{21:[2,32],31:[2,32],52:[2,32],59:[2,32],62:[2,32],66:[2,32],69:[2,32],74:[2,32],75:[2,32],76:[2,32],77:[2,32],78:[2,32],79:[2,32]},{21:[2,33],31:[2,33],52:[2,33],59:[2,33],62:[2,33],66:[2,33],69:[2,33],74:[2,33],75:[2,33],76:[2,33],77:[2,33],78:[2,33],79:[2,33]},{21:[2,34],31:[2,34],52:[2,34],59:[2,34],62:[2,34],66:[2,34],69:[2,34],74:[2,34],75:[2,34],76:[2,34],77:[2,34],78:[2,34],79:[2,34]},{21:[2,35],31:[2,35],52:[2,35],59:[2,35],62:[2,35],66:[2,35],69:[2,35],74:[2,35],75:[2,35],76:[2,35],77:[2,35],78:[2,35],79:[2,35]},{21:[2,36],31:[2,36],52:[2,36],59:[2,36],62:[2,36],66:[2,36],69:[2,36],74:[2,36],75:[2,36],76:[2,36],77:[2,36],78:[2,36],79:[2,36]},{21:[2,40],31:[2,40],52:[2,40],59:[2,40],62:[2,40],66:[2,40],69:[2,40],74:[2,40],75:[2,40],76:[2,40],77:[2,40],78:[2,40],79:[2,40],81:[1,45]},{66:[1,32],80:46},{21:[2,42],31:[2,42],52:[2,42],59:[2,42],62:[2,42],66:[2,42],69:[2,42],74:[2,42],75:[2,42],76:[2,42],77:[2,42],78:[2,42],79:[2,42],81:[2,42]},{50:47,52:[2,77],59:[2,77],66:[2,77],74:[2,77],75:[2,77],76:[2,77],77:[2,77],78:[2,77],79:[2,77]},{23:48,36:50,37:[1,52],41:51,42:[1,53],43:49,45:[2,49]},{26:54,41:55,42:[1,53],45:[2,51]},{16:[1,56]},{31:[2,81],55:57,59:[2,81],66:[2,81],74:[2,81],75:[2,81],76:[2,81],77:[2,81],78:[2,81],79:[2,81]},{31:[2,37],59:[2,37],66:[2,37],74:[2,37],75:[2,37],76:[2,37],77:[2,37],78:[2,37],79:[2,37]},{31:[2,38],59:[2,38],66:[2,38],74:[2,38],75:[2,38],76:[2,38],77:[2,38],78:[2,38],79:[2,38]},{18:58,66:[1,32],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{28:59,31:[2,53],59:[2,53],66:[2,53],69:[2,53],74:[2,53],75:[2,53],76:[2,53],77:[2,53],78:[2,53],79:[2,53]},{31:[2,59],33:60,59:[2,59],66:[2,59],69:[2,59],74:[2,59],75:[2,59],76:[2,59],77:[2,59],78:[2,59],79:[2,59]},{19:61,21:[2,45],59:[2,45],66:[2,45],74:[2,45],75:[2,45],76:[2,45],77:[2,45],78:[2,45],79:[2,45]},{18:65,31:[2,75],48:62,57:63,58:66,59:[1,40],63:64,64:67,65:68,66:[1,69],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{66:[1,70]},{21:[2,39],31:[2,39],52:[2,39],59:[2,39],62:[2,39],66:[2,39],69:[2,39],74:[2,39],75:[2,39],76:[2,39],77:[2,39],78:[2,39],79:[2,39],81:[1,45]},{18:65,51:71,52:[2,79],57:72,58:66,59:[1,40],63:73,64:67,65:68,66:[1,69],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{24:74,45:[1,75]},{45:[2,50]},{4:76,6:3,13:[2,43],14:[2,43],17:[2,43],27:[2,43],32:[2,43],37:[2,43],42:[2,43],45:[2,43],46:[2,43],49:[2,43],53:[2,43]},{45:[2,19]},{18:77,66:[1,32],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{4:78,6:3,13:[2,43],14:[2,43],17:[2,43],27:[2,43],32:[2,43],45:[2,43],46:[2,43],49:[2,43],53:[2,43]},{24:79,45:[1,75]},{45:[2,52]},{5:[2,10],13:[2,10],14:[2,10],17:[2,10],27:[2,10],32:[2,10],37:[2,10],42:[2,10],45:[2,10],46:[2,10],49:[2,10],53:[2,10]},{18:65,31:[2,83],56:80,57:81,58:66,59:[1,40],63:82,64:67,65:68,66:[1,69],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{59:[2,85],60:83,62:[2,85],66:[2,85],74:[2,85],75:[2,85],76:[2,85],77:[2,85],78:[2,85],79:[2,85]},{18:65,29:84,31:[2,55],57:85,58:66,59:[1,40],63:86,64:67,65:68,66:[1,69],69:[2,55],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{18:65,31:[2,61],34:87,57:88,58:66,59:[1,40],63:89,64:67,65:68,66:[1,69],69:[2,61],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{18:65,20:90,21:[2,47],57:91,58:66,59:[1,40],63:92,64:67,65:68,66:[1,69],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{31:[1,93]},{31:[2,74],59:[2,74],66:[2,74],74:[2,74],75:[2,74],76:[2,74],77:[2,74],78:[2,74],79:[2,74]},{31:[2,76]},{21:[2,24],31:[2,24],52:[2,24],59:[2,24],62:[2,24],66:[2,24],69:[2,24],74:[2,24],75:[2,24],76:[2,24],77:[2,24],78:[2,24],79:[2,24]},{21:[2,25],31:[2,25],52:[2,25],59:[2,25],62:[2,25],66:[2,25],69:[2,25],74:[2,25],75:[2,25],76:[2,25],77:[2,25],78:[2,25],79:[2,25]},{21:[2,27],31:[2,27],52:[2,27],62:[2,27],65:94,66:[1,95],69:[2,27]},{21:[2,89],31:[2,89],52:[2,89],62:[2,89],66:[2,89],69:[2,89]},{21:[2,42],31:[2,42],52:[2,42],59:[2,42],62:[2,42],66:[2,42],67:[1,96],69:[2,42],74:[2,42],75:[2,42],76:[2,42],77:[2,42],78:[2,42],79:[2,42],81:[2,42]},{21:[2,41],31:[2,41],52:[2,41],59:[2,41],62:[2,41],66:[2,41],69:[2,41],74:[2,41],75:[2,41],76:[2,41],77:[2,41],78:[2,41],79:[2,41],81:[2,41]},{52:[1,97]},{52:[2,78],59:[2,78],66:[2,78],74:[2,78],75:[2,78],76:[2,78],77:[2,78],78:[2,78],79:[2,78]},{52:[2,80]},{5:[2,12],13:[2,12],14:[2,12],17:[2,12],27:[2,12],32:[2,12],37:[2,12],42:[2,12],45:[2,12],46:[2,12],49:[2,12],53:[2,12]},{18:98,66:[1,32],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{36:50,37:[1,52],41:51,42:[1,53],43:100,44:99,45:[2,71]},{31:[2,65],38:101,59:[2,65],66:[2,65],69:[2,65],74:[2,65],75:[2,65],76:[2,65],77:[2,65],78:[2,65],79:[2,65]},{45:[2,17]},{5:[2,13],13:[2,13],14:[2,13],17:[2,13],27:[2,13],32:[2,13],37:[2,13],42:[2,13],45:[2,13],46:[2,13],49:[2,13],53:[2,13]},{31:[1,102]},{31:[2,82],59:[2,82],66:[2,82],74:[2,82],75:[2,82],76:[2,82],77:[2,82],78:[2,82],79:[2,82]},{31:[2,84]},{18:65,57:104,58:66,59:[1,40],61:103,62:[2,87],63:105,64:67,65:68,66:[1,69],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{30:106,31:[2,57],68:107,69:[1,108]},{31:[2,54],59:[2,54],66:[2,54],69:[2,54],74:[2,54],75:[2,54],76:[2,54],77:[2,54],78:[2,54],79:[2,54]},{31:[2,56],69:[2,56]},{31:[2,63],35:109,68:110,69:[1,108]},{31:[2,60],59:[2,60],66:[2,60],69:[2,60],74:[2,60],75:[2,60],76:[2,60],77:[2,60],78:[2,60],79:[2,60]},{31:[2,62],69:[2,62]},{21:[1,111]},{21:[2,46],59:[2,46],66:[2,46],74:[2,46],75:[2,46],76:[2,46],77:[2,46],78:[2,46],79:[2,46]},{21:[2,48]},{5:[2,21],13:[2,21],14:[2,21],17:[2,21],27:[2,21],32:[2,21],37:[2,21],42:[2,21],45:[2,21],46:[2,21],49:[2,21],53:[2,21]},{21:[2,90],31:[2,90],52:[2,90],62:[2,90],66:[2,90],69:[2,90]},{67:[1,96]},{18:65,57:112,58:66,59:[1,40],66:[1,32],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{5:[2,22],13:[2,22],14:[2,22],17:[2,22],27:[2,22],32:[2,22],37:[2,22],42:[2,22],45:[2,22],46:[2,22],49:[2,22],53:[2,22]},{31:[1,113]},{45:[2,18]},{45:[2,72]},{18:65,31:[2,67],39:114,57:115,58:66,59:[1,40],63:116,64:67,65:68,66:[1,69],69:[2,67],72:23,73:24,74:[1,25],75:[1,26],76:[1,27],77:[1,28],78:[1,29],79:[1,31],80:30},{5:[2,23],13:[2,23],14:[2,23],17:[2,23],27:[2,23],32:[2,23],37:[2,23],42:[2,23],45:[2,23],46:[2,23],49:[2,23],53:[2,23]},{62:[1,117]},{59:[2,86],62:[2,86],66:[2,86],74:[2,86],75:[2,86],76:[2,86],77:[2,86],78:[2,86],79:[2,86]},{62:[2,88]},{31:[1,118]},{31:[2,58]},{66:[1,120],70:119},{31:[1,121]},{31:[2,64]},{14:[2,11]},{21:[2,28],31:[2,28],52:[2,28],62:[2,28],66:[2,28],69:[2,28]},{5:[2,20],13:[2,20],14:[2,20],17:[2,20],27:[2,20],32:[2,20],37:[2,20],42:[2,20],45:[2,20],46:[2,20],49:[2,20],53:[2,20]},{31:[2,69],40:122,68:123,69:[1,108]},{31:[2,66],59:[2,66],66:[2,66],69:[2,66],74:[2,66],75:[2,66],76:[2,66],77:[2,66],78:[2,66],79:[2,66]},{31:[2,68],69:[2,68]},{21:[2,26],31:[2,26],52:[2,26],59:[2,26],62:[2,26],66:[2,26],69:[2,26],74:[2,26],75:[2,26],76:[2,26],77:[2,26],78:[2,26],79:[2,26]},{13:[2,14],14:[2,14],17:[2,14],27:[2,14],32:[2,14],37:[2,14],42:[2,14],45:[2,14],46:[2,14],49:[2,14],53:[2,14]},{66:[1,125],71:[1,124]},{66:[2,91],71:[2,91]},{13:[2,15],14:[2,15],17:[2,15],27:[2,15],32:[2,15],42:[2,15],45:[2,15],46:[2,15],49:[2,15],53:[2,15]},{31:[1,126]},{31:[2,70]},{31:[2,29]},{66:[2,92],71:[2,92]},{13:[2,16],14:[2,16],17:[2,16],27:[2,16],32:[2,16],37:[2,16],42:[2,16],45:[2,16],46:[2,16],49:[2,16],53:[2,16]}],defaultActions:{4:[2,1],49:[2,50],51:[2,19],55:[2,52],64:[2,76],73:[2,80],78:[2,17],82:[2,84],92:[2,48],99:[2,18],100:[2,72],105:[2,88],107:[2,58],110:[2,64],111:[2,11],123:[2,70],124:[2,29]},parseError:function(a,b){throw new Error(a)},parse:function(a){function b(){var a;return a=c.lexer.lex()||1,"number"!=typeof a&&(a=c.symbols_[a]||a),a}var c=this,d=[0],e=[null],f=[],g=this.table,h="",i=0,j=0,k=0;this.lexer.setInput(a),this.lexer.yy=this.yy,this.yy.lexer=this.lexer,this.yy.parser=this,"undefined"==typeof this.lexer.yylloc&&(this.lexer.yylloc={});var l=this.lexer.yylloc;f.push(l);var m=this.lexer.options&&this.lexer.options.ranges;"function"==typeof this.yy.parseError&&(this.parseError=this.yy.parseError);for(var n,o,p,q,r,s,t,u,v,w={};;){if(p=d[d.length-1],this.defaultActions[p]?q=this.defaultActions[p]:((null===n||"undefined"==typeof n)&&(n=b()),q=g[p]&&g[p][n]),"undefined"==typeof q||!q.length||!q[0]){var x="";if(!k){v=[];for(s in g[p])this.terminals_[s]&&s>2&&v.push("'"+this.terminals_[s]+"'");x=this.lexer.showPosition?"Parse error on line "+(i+1)+":\n"+this.lexer.showPosition()+"\nExpecting "+v.join(", ")+", got '"+(this.terminals_[n]||n)+"'":"Parse error on line "+(i+1)+": Unexpected "+(1==n?"end of input":"'"+(this.terminals_[n]||n)+"'"),this.parseError(x,{text:this.lexer.match,token:this.terminals_[n]||n,line:this.lexer.yylineno,loc:l,expected:v})}}if(q[0]instanceof Array&&q.length>1)throw new Error("Parse Error: multiple actions possible at state: "+p+", token: "+n);switch(q[0]){case 1:d.push(n),e.push(this.lexer.yytext),f.push(this.lexer.yylloc),d.push(q[1]),n=null,o?(n=o,o=null):(j=this.lexer.yyleng,h=this.lexer.yytext,i=this.lexer.yylineno,l=this.lexer.yylloc,k>0&&k--);break;case 2:if(t=this.productions_[q[1]][1],w.$=e[e.length-t],w._$={first_line:f[f.length-(t||1)].first_line,last_line:f[f.length-1].last_line,first_column:f[f.length-(t||1)].first_column,last_column:f[f.length-1].last_column},m&&(w._$.range=[f[f.length-(t||1)].range[0],f[f.length-1].range[1]]),r=this.performAction.call(w,h,j,i,this.yy,q[1],e,f),"undefined"!=typeof r)return r;t&&(d=d.slice(0,-1*t*2),e=e.slice(0,-1*t),f=f.slice(0,-1*t)),d.push(this.productions_[q[1]][0]),e.push(w.$),f.push(w._$),u=g[d[d.length-2]][d[d.length-1]],d.push(u);break;case 3:return!0}}return!0}},c=function(){var a={EOF:1,parseError:function(a,b){if(!this.yy.parser)throw new Error(a);this.yy.parser.parseError(a,b)},setInput:function(a){return this._input=a,this._more=this._less=this.done=!1,this.yylineno=this.yyleng=0,this.yytext=this.matched=this.match="",this.conditionStack=["INITIAL"],this.yylloc={first_line:1,first_column:0,last_line:1,last_column:0},this.options.ranges&&(this.yylloc.range=[0,0]),this.offset=0,this},input:function(){var a=this._input[0];this.yytext+=a,this.yyleng++,this.offset++,this.match+=a,this.matched+=a;var b=a.match(/(?:\r\n?|\n).*/g);return b?(this.yylineno++,this.yylloc.last_line++):this.yylloc.last_column++,this.options.ranges&&this.yylloc.range[1]++,this._input=this._input.slice(1),a},unput:function(a){var b=a.length,c=a.split(/(?:\r\n?|\n)/g);this._input=a+this._input,this.yytext=this.yytext.substr(0,this.yytext.length-b-1),this.offset-=b;var d=this.match.split(/(?:\r\n?|\n)/g);this.match=this.match.substr(0,this.match.length-1),this.matched=this.matched.substr(0,this.matched.length-1),c.length-1&&(this.yylineno-=c.length-1);var e=this.yylloc.range;return this.yylloc={first_line:this.yylloc.first_line,last_line:this.yylineno+1,first_column:this.yylloc.first_column,last_column:c?(c.length===d.length?this.yylloc.first_column:0)+d[d.length-c.length].length-c[0].length:this.yylloc.first_column-b},this.options.ranges&&(this.yylloc.range=[e[0],e[0]+this.yyleng-b]),this},more:function(){return this._more=!0,this},less:function(a){this.unput(this.match.slice(a))},pastInput:function(){var a=this.matched.substr(0,this.matched.length-this.match.length);return(a.length>20?"...":"")+a.substr(-20).replace(/\n/g,"")},upcomingInput:function(){var a=this.match;return a.length<20&&(a+=this._input.substr(0,20-a.length)),(a.substr(0,20)+(a.length>20?"...":"")).replace(/\n/g,"")},showPosition:function(){var a=this.pastInput(),b=new Array(a.length+1).join("-");return a+this.upcomingInput()+"\n"+b+"^"},next:function(){if(this.done)return this.EOF;this._input||(this.done=!0);var a,b,c,d,e;this._more||(this.yytext="",this.match="");for(var f=this._currentRules(),g=0;g<f.length&&(c=this._input.match(this.rules[f[g]]),!c||b&&!(c[0].length>b[0].length)||(b=c,d=g,this.options.flex));g++);return b?(e=b[0].match(/(?:\r\n?|\n).*/g),e&&(this.yylineno+=e.length),this.yylloc={first_line:this.yylloc.last_line,last_line:this.yylineno+1,first_column:this.yylloc.last_column,last_column:e?e[e.length-1].length-e[e.length-1].match(/\r?\n?/)[0].length:this.yylloc.last_column+b[0].length},this.yytext+=b[0],this.match+=b[0],this.matches=b,this.yyleng=this.yytext.length,this.options.ranges&&(this.yylloc.range=[this.offset,this.offset+=this.yyleng]),this._more=!1,this._input=this._input.slice(b[0].length),this.matched+=b[0],a=this.performAction.call(this,this.yy,this,f[d],this.conditionStack[this.conditionStack.length-1]),this.done&&this._input&&(this.done=!1),a?a:void 0):""===this._input?this.EOF:this.parseError("Lexical error on line "+(this.yylineno+1)+". Unrecognized text.\n"+this.showPosition(),{text:"",token:null,line:this.yylineno})},lex:function(){var a=this.next();return"undefined"!=typeof a?a:this.lex()},begin:function(a){this.conditionStack.push(a)},popState:function(){return this.conditionStack.pop()},_currentRules:function(){return this.conditions[this.conditionStack[this.conditionStack.length-1]].rules},topState:function(){return this.conditionStack[this.conditionStack.length-2]},pushState:function(a){this.begin(a)}};return a.options={},a.performAction=function(a,b,c,d){function e(a,c){return b.yytext=b.yytext.substr(a,b.yyleng-c)}switch(c){case 0:if("\\\\"===b.yytext.slice(-2)?(e(0,1),this.begin("mu")):"\\"===b.yytext.slice(-1)?(e(0,1),this.begin("emu")):this.begin("mu"),b.yytext)return 14;break;case 1:return 14;case 2:return this.popState(),14;case 3:return b.yytext=b.yytext.substr(5,b.yyleng-9),this.popState(),16;case 4:return 14;case 5:return this.popState(),13;case 6:return 59;case 7:return 62;case 8:return 17;case 9:return this.popState(),this.begin("raw"),21;case 10:return 53;case 11:return 27;case 12:return 45;case 13:return this.popState(),42;case 14:return this.popState(),42;case 15:return 32;case 16:return 37;case 17:return 49;case 18:return 46;case 19:this.unput(b.yytext),this.popState(),this.begin("com");break;case 20:return this.popState(),13;case 21:return 46;case 22:return 67;case 23:return 66;case 24:return 66;case 25:return 81;case 26:break;case 27:return this.popState(),52;case 28:return this.popState(),31;case 29:return b.yytext=e(1,2).replace(/\\"/g,'"'),74;case 30:return b.yytext=e(1,2).replace(/\\'/g,"'"),74;case 31:return 79;case 32:return 76;case 33:return 76;case 34:return 77;case 35:return 78;case 36:return 75;case 37:return 69;case 38:return 71;case 39:return 66;case 40:return 66;case 41:return"INVALID";case 42:return 5}},a.rules=[/^(?:[^\x00]*?(?=(\{\{)))/,/^(?:[^\x00]+)/,/^(?:[^\x00]{2,}?(?=(\{\{|\\\{\{|\\\\\{\{|$)))/,/^(?:\{\{\{\{\/[^\s!"#%-,\.\/;->@\[-\^`\{-~]+(?=[=}\s\/.])\}\}\}\})/,/^(?:[^\x00]*?(?=(\{\{\{\{\/)))/,/^(?:[\s\S]*?--(~)?\}\})/,/^(?:\()/,/^(?:\))/,/^(?:\{\{\{\{)/,/^(?:\}\}\}\})/,/^(?:\{\{(~)?>)/,/^(?:\{\{(~)?#)/,/^(?:\{\{(~)?\/)/,/^(?:\{\{(~)?\^\s*(~)?\}\})/,/^(?:\{\{(~)?\s*else\s*(~)?\}\})/,/^(?:\{\{(~)?\^)/,/^(?:\{\{(~)?\s*else\b)/,/^(?:\{\{(~)?\{)/,/^(?:\{\{(~)?&)/,/^(?:\{\{(~)?!--)/,/^(?:\{\{(~)?![\s\S]*?\}\})/,/^(?:\{\{(~)?)/,/^(?:=)/,/^(?:\.\.)/,/^(?:\.(?=([=~}\s\/.)|])))/,/^(?:[\/.])/,/^(?:\s+)/,/^(?:\}(~)?\}\})/,/^(?:(~)?\}\})/,/^(?:"(\\["]|[^"])*")/,/^(?:'(\\[']|[^'])*')/,/^(?:@)/,/^(?:true(?=([~}\s)])))/,/^(?:false(?=([~}\s)])))/,/^(?:undefined(?=([~}\s)])))/,/^(?:null(?=([~}\s)])))/,/^(?:-?[0-9]+(?:\.[0-9]+)?(?=([~}\s)])))/,/^(?:as\s+\|)/,/^(?:\|)/,/^(?:([^\s!"#%-,\.\/;->@\[-\^`\{-~]+(?=([=~}\s\/.)|]))))/,/^(?:\[[^\]]*\])/,/^(?:.)/,/^(?:$)/],a.conditions={mu:{rules:[6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42],inclusive:!1},emu:{rules:[2],inclusive:!1},com:{rules:[5],inclusive:!1},raw:{rules:[3,4],inclusive:!1},INITIAL:{rules:[0,1,42],inclusive:!0}},a}();return b.lexer=c,a.prototype=b,b.Parser=a,new a}();b["default"]=d,a.exports=b["default"]},function(a,b,c){"use strict";function d(){}function e(a,b,c){void 0===b&&(b=a.length);var d=a[b-1],e=a[b-2];return d?"ContentStatement"===d.type?(e||!c?/\r?\n\s*?$/:/(^|\r?\n)\s*?$/).test(d.original):void 0:c}function f(a,b,c){void 0===b&&(b=-1);var d=a[b+1],e=a[b+2];return d?"ContentStatement"===d.type?(e||!c?/^\s*?\r?\n/:/^\s*?(\r?\n|$)/).test(d.original):void 0:c}function g(a,b,c){var d=a[null==b?0:b+1];if(d&&"ContentStatement"===d.type&&(c||!d.rightStripped)){var e=d.value;d.value=d.value.replace(c?/^\s+/:/^[ \t]*\r?\n?/,""),d.rightStripped=d.value!==e}}function h(a,b,c){var d=a[null==b?a.length-1:b-1];if(d&&"ContentStatement"===d.type&&(c||!d.leftStripped)){var e=d.value;return d.value=d.value.replace(c?/\s+$/:/[ \t]+$/,""),d.leftStripped=d.value!==e,d.leftStripped}}var i=c(8)["default"];b.__esModule=!0;var j=c(6),k=i(j);d.prototype=new k["default"],d.prototype.Program=function(a){var b=!this.isRootSeen;this.isRootSeen=!0;for(var c=a.body,d=0,i=c.length;i>d;d++){var j=c[d],k=this.accept(j);if(k){var l=e(c,d,b),m=f(c,d,b),n=k.openStandalone&&l,o=k.closeStandalone&&m,p=k.inlineStandalone&&l&&m;k.close&&g(c,d,!0),k.open&&h(c,d,!0),p&&(g(c,d),h(c,d)&&"PartialStatement"===j.type&&(j.indent=/([ \t]+$)/.exec(c[d-1].original)[1])),n&&(g((j.program||j.inverse).body),h(c,d)),o&&(g(c,d),h((j.inverse||j.program).body))}}return a},d.prototype.BlockStatement=function(a){this.accept(a.program),this.accept(a.inverse);var b=a.program||a.inverse,c=a.program&&a.inverse,d=c,i=c;if(c&&c.chained)for(d=c.body[0].program;i.chained;)i=i.body[i.body.length-1].program;var j={open:a.openStrip.open,close:a.closeStrip.close,openStandalone:f(b.body),closeStandalone:e((d||b).body)};if(a.openStrip.close&&g(b.body,null,!0),c){var k=a.inverseStrip;k.open&&h(b.body,null,!0),k.close&&g(d.body,null,!0),a.closeStrip.open&&h(i.body,null,!0),e(b.body)&&f(d.body)&&(h(b.body),g(d.body))}else a.closeStrip.open&&h(b.body,null,!0);return j},d.prototype.MustacheStatement=function(a){return a.strip},d.prototype.PartialStatement=d.prototype.CommentStatement=function(a){var b=a.strip||{};return{inlineStandalone:!0,open:b.open,close:b.close}},b["default"]=d,a.exports=b["default"]},function(a,b,c){"use strict";function d(a,b){this.source=a,this.start={line:b.first_line,column:b.first_column},this.end={line:b.last_line,column:b.last_column}}function e(a){return/^\[.*\]$/.test(a)?a.substr(1,a.length-2):a}function f(a,b){return{open:"~"===a.charAt(2),close:"~"===b.charAt(b.length-3)}}function g(a){return a.replace(/^\{\{~?\!-?-?/,"").replace(/-?-?~?\}\}$/,"")}function h(a,b,c){c=this.locInfo(c);for(var d=a?"@":"",e=[],f=0,g="",h=0,i=b.length;i>h;h++){var j=b[h].part,k=b[h].original!==j;if(d+=(b[h].separator||"")+j,k||".."!==j&&"."!==j&&"this"!==j)e.push(j);else{if(e.length>0)throw new n["default"]("Invalid path: "+d,{loc:c});".."===j&&(f++,g+="../")}}return new this.PathExpression(a,f,e,d,c)}function i(a,b,c,d,e,f){var g=d.charAt(3)||d.charAt(2),h="{"!==g&&"&"!==g;return new this.MustacheStatement(a,b,c,h,e,this.locInfo(f))}function j(a,b,c,d){if(a.path.original!==c){var e={loc:a.path.loc};throw new n["default"](a.path.original+" doesn't match "+c,e)}d=this.locInfo(d);var f=new this.Program([b],null,{},d);return new this.BlockStatement(a.path,a.params,a.hash,f,void 0,{},{},{},d)}function k(a,b,c,d,e,f){if(d&&d.path&&a.path.original!==d.path.original){var g={loc:a.path.loc};throw new n["default"](a.path.original+" doesn't match "+d.path.original,g)}b.blockParams=a.blockParams;var h=void 0,i=void 0;return c&&(c.chain&&(c.program.body[0].closeStrip=d.strip),i=c.strip,h=c.program),e&&(e=h,h=b,b=e),new this.BlockStatement(a.path,a.params,a.hash,b,h,a.strip,i,d&&d.strip,this.locInfo(f))}var l=c(8)["default"];b.__esModule=!0,b.SourceLocation=d,b.id=e,b.stripFlags=f,b.stripComment=g,b.preparePath=h,b.prepareMustache=i,b.prepareRawBlock=j,b.prepareBlock=k;var m=c(12),n=l(m)},function(a,b,c){"use strict";function d(a,b,c){if(f.isArray(a)){for(var d=[],e=0,g=a.length;g>e;e++)d.push(b.wrap(a[e],c));return d}return"boolean"==typeof a||"number"==typeof a?a+"":a}function e(a){this.srcFile=a,this.source=[]}b.__esModule=!0;var f=c(13),g=void 0;try{}catch(h){}g||(g=function(a,b,c,d){this.src="",d&&this.add(d)},g.prototype={add:function(a){f.isArray(a)&&(a=a.join("")),this.src+=a},prepend:function(a){f.isArray(a)&&(a=a.join("")),this.src=a+this.src},toStringWithSourceMap:function(){return{code:this.toString()}},toString:function(){return this.src}}),e.prototype={prepend:function(a,b){this.source.unshift(this.wrap(a,b))},push:function(a,b){this.source.push(this.wrap(a,b))},merge:function(){var a=this.empty();return this.each(function(b){a.add(["  ",b,"\n"])}),a},each:function(a){for(var b=0,c=this.source.length;c>b;b++)a(this.source[b])},empty:function(){var a=void 0===arguments[0]?this.currentLocation||{start:{}}:arguments[0];return new g(a.start.line,a.start.column,this.srcFile)},wrap:function(a){var b=void 0===arguments[1]?this.currentLocation||{start:{}}:arguments[1];return a instanceof g?a:(a=d(a,this,b),new g(b.start.line,b.start.column,this.srcFile,a))},functionCall:function(a,b,c){return c=this.generateList(c),this.wrap([a,b?"."+b+"(":"(",c,")"]);

},quotedString:function(a){return'"'+(a+"").replace(/\\/g,"\\\\").replace(/"/g,'\\"').replace(/\n/g,"\\n").replace(/\r/g,"\\r").replace(/\u2028/g,"\\u2028").replace(/\u2029/g,"\\u2029")+'"'},objectLiteral:function(a){var b=[];for(var c in a)if(a.hasOwnProperty(c)){var e=d(a[c],this);"undefined"!==e&&b.push([this.quotedString(c),":",e])}var f=this.generateList(b);return f.prepend("{"),f.add("}"),f},generateList:function(a,b){for(var c=this.empty(b),e=0,f=a.length;f>e;e++)e&&c.add(","),c.add(d(a[e],this,b));return c},generateArray:function(a,b){var c=this.generateList(a,b);return c.prepend("["),c.add("]"),c}},b["default"]=e,a.exports=b["default"]}])});
/* Modernizr 2.6.2 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-csstransforms-csstransforms3d-csstransitions-input-touch-shiv-cssclasses-teststyles-testprop-testallprops-prefixes-domprefixes-load
 */
;window.Modernizr=function(a,b,c){function z(a){j.cssText=a}function A(a,b){return z(m.join(a+";")+(b||""))}function B(a,b){return typeof a===b}function C(a,b){return!!~(""+a).indexOf(b)}function D(a,b){for(var d in a){var e=a[d];if(!C(e,"-")&&j[e]!==c)return b=="pfx"?e:!0}return!1}function E(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:B(f,"function")?f.bind(d||b):f}return!1}function F(a,b,c){var d=a.charAt(0).toUpperCase()+a.slice(1),e=(a+" "+o.join(d+" ")+d).split(" ");return B(b,"string")||B(b,"undefined")?D(e,b):(e=(a+" "+p.join(d+" ")+d).split(" "),E(e,b,c))}function G(){e.input=function(c){for(var d=0,e=c.length;d<e;d++)s[c[d]]=c[d]in k;return s.list&&(s.list=!!b.createElement("datalist")&&!!a.HTMLDataListElement),s}("autocomplete autofocus list placeholder max min multiple pattern required step".split(" "))}var d="2.6.2",e={},f=!0,g=b.documentElement,h="modernizr",i=b.createElement(h),j=i.style,k=b.createElement("input"),l={}.toString,m=" -webkit- -moz- -o- -ms- ".split(" "),n="Webkit Moz O ms",o=n.split(" "),p=n.toLowerCase().split(" "),q={},r={},s={},t=[],u=t.slice,v,w=function(a,c,d,e){var f,i,j,k,l=b.createElement("div"),m=b.body,n=m||b.createElement("body");if(parseInt(d,10))while(d--)j=b.createElement("div"),j.id=e?e[d]:h+(d+1),l.appendChild(j);return f=["&#173;",'<style id="s',h,'">',a,"</style>"].join(""),l.id=h,(m?l:n).innerHTML+=f,n.appendChild(l),m||(n.style.background="",n.style.overflow="hidden",k=g.style.overflow,g.style.overflow="hidden",g.appendChild(n)),i=c(l,a),m?l.parentNode.removeChild(l):(n.parentNode.removeChild(n),g.style.overflow=k),!!i},x={}.hasOwnProperty,y;!B(x,"undefined")&&!B(x.call,"undefined")?y=function(a,b){return x.call(a,b)}:y=function(a,b){return b in a&&B(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=u.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(u.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(u.call(arguments)))};return e}),q.touch=function(){var c;return"ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch?c=!0:w(["@media (",m.join("touch-enabled),("),h,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(a){c=a.offsetTop===9}),c},q.csstransforms=function(){return!!F("transform")},q.csstransforms3d=function(){var a=!!F("perspective");return a&&"webkitPerspective"in g.style&&w("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}",function(b,c){a=b.offsetLeft===9&&b.offsetHeight===3}),a},q.csstransitions=function(){return F("transition")};for(var H in q)y(q,H)&&(v=H.toLowerCase(),e[v]=q[H](),t.push((e[v]?"":"no-")+v));return e.input||G(),e.addTest=function(a,b){if(typeof a=="object")for(var d in a)y(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof f!="undefined"&&f&&(g.className+=" "+(b?"":"no-")+a),e[a]=b}return e},z(""),i=k=null,function(a,b){function k(a,b){var c=a.createElement("p"),d=a.getElementsByTagName("head")[0]||a.documentElement;return c.innerHTML="x<style>"+b+"</style>",d.insertBefore(c.lastChild,d.firstChild)}function l(){var a=r.elements;return typeof a=="string"?a.split(" "):a}function m(a){var b=i[a[g]];return b||(b={},h++,a[g]=h,i[h]=b),b}function n(a,c,f){c||(c=b);if(j)return c.createElement(a);f||(f=m(c));var g;return f.cache[a]?g=f.cache[a].cloneNode():e.test(a)?g=(f.cache[a]=f.createElem(a)).cloneNode():g=f.createElem(a),g.canHaveChildren&&!d.test(a)?f.frag.appendChild(g):g}function o(a,c){a||(a=b);if(j)return a.createDocumentFragment();c=c||m(a);var d=c.frag.cloneNode(),e=0,f=l(),g=f.length;for(;e<g;e++)d.createElement(f[e]);return d}function p(a,b){b.cache||(b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag()),a.createElement=function(c){return r.shivMethods?n(c,a,b):b.createElem(c)},a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+l().join().replace(/\w+/g,function(a){return b.createElem(a),b.frag.createElement(a),'c("'+a+'")'})+");return n}")(r,b.frag)}function q(a){a||(a=b);var c=m(a);return r.shivCSS&&!f&&!c.hasCSS&&(c.hasCSS=!!k(a,"article,aside,figcaption,figure,footer,header,hgroup,nav,section{display:block}mark{background:#FF0;color:#000}")),j||p(a,c),a}var c=a.html5||{},d=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,e=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,f,g="_html5shiv",h=0,i={},j;(function(){try{var a=b.createElement("a");a.innerHTML="<xyz></xyz>",f="hidden"in a,j=a.childNodes.length==1||function(){b.createElement("a");var a=b.createDocumentFragment();return typeof a.cloneNode=="undefined"||typeof a.createDocumentFragment=="undefined"||typeof a.createElement=="undefined"}()}catch(c){f=!0,j=!0}})();var r={elements:c.elements||"abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video",shivCSS:c.shivCSS!==!1,supportsUnknownElements:j,shivMethods:c.shivMethods!==!1,type:"default",shivDocument:q,createElement:n,createDocumentFragment:o};a.html5=r,q(b)}(this,b),e._version=d,e._prefixes=m,e._domPrefixes=p,e._cssomPrefixes=o,e.testProp=function(a){return D([a])},e.testAllProps=F,e.testStyles=w,g.className=g.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(f?" js "+t.join(" "):""),e}(this,this.document),function(a,b,c){function d(a){return"[object Function]"==o.call(a)}function e(a){return"string"==typeof a}function f(){}function g(a){return!a||"loaded"==a||"complete"==a||"uninitialized"==a}function h(){var a=p.shift();q=1,a?a.t?m(function(){("c"==a.t?B.injectCss:B.injectJs)(a.s,0,a.a,a.x,a.e,1)},0):(a(),h()):q=0}function i(a,c,d,e,f,i,j){function k(b){if(!o&&g(l.readyState)&&(u.r=o=1,!q&&h(),l.onload=l.onreadystatechange=null,b)){"img"!=a&&m(function(){t.removeChild(l)},50);for(var d in y[c])y[c].hasOwnProperty(d)&&y[c][d].onload()}}var j=j||B.errorTimeout,l=b.createElement(a),o=0,r=0,u={t:d,s:c,e:f,a:i,x:j};1===y[c]&&(r=1,y[c]=[]),"object"==a?l.data=c:(l.src=c,l.type=a),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){k.call(this,r)},p.splice(e,0,u),"img"!=a&&(r||2===y[c]?(t.insertBefore(l,s?null:n),m(k,j)):y[c].push(l))}function j(a,b,c,d,f){return q=0,b=b||"j",e(a)?i("c"==b?v:u,a,b,this.i++,c,d,f):(p.splice(this.i++,0,a),1==p.length&&h()),this}function k(){var a=B;return a.loader={load:j,i:0},a}var l=b.documentElement,m=a.setTimeout,n=b.getElementsByTagName("script")[0],o={}.toString,p=[],q=0,r="MozAppearance"in l.style,s=r&&!!b.createRange().compareNode,t=s?l:n.parentNode,l=a.opera&&"[object Opera]"==o.call(a.opera),l=!!b.attachEvent&&!l,u=r?"object":l?"script":"img",v=l?"script":u,w=Array.isArray||function(a){return"[object Array]"==o.call(a)},x=[],y={},z={timeout:function(a,b){return b.length&&(a.timeout=b[0]),a}},A,B;B=function(a){function b(a){var a=a.split("!"),b=x.length,c=a.pop(),d=a.length,c={url:c,origUrl:c,prefixes:a},e,f,g;for(f=0;f<d;f++)g=a[f].split("="),(e=z[g.shift()])&&(c=e(c,g));for(f=0;f<b;f++)c=x[f](c);return c}function g(a,e,f,g,h){var i=b(a),j=i.autoCallback;i.url.split(".").pop().split("?").shift(),i.bypass||(e&&(e=d(e)?e:e[a]||e[g]||e[a.split("/").pop().split("?")[0]]),i.instead?i.instead(a,e,f,g,h):(y[i.url]?i.noexec=!0:y[i.url]=1,f.load(i.url,i.forceCSS||!i.forceJS&&"css"==i.url.split(".").pop().split("?").shift()?"c":c,i.noexec,i.attrs,i.timeout),(d(e)||d(j))&&f.load(function(){k(),e&&e(i.origUrl,h,g),j&&j(i.origUrl,h,g),y[i.url]=2})))}function h(a,b){function c(a,c){if(a){if(e(a))c||(j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}),g(a,j,b,0,h);else if(Object(a)===a)for(n in m=function(){var b=0,c;for(c in a)a.hasOwnProperty(c)&&b++;return b}(),a)a.hasOwnProperty(n)&&(!c&&!--m&&(d(j)?j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}:j[n]=function(a){return function(){var b=[].slice.call(arguments);a&&a.apply(this,b),l()}}(k[n])),g(a[n],j,b,n,h))}else!c&&l()}var h=!!a.test,i=a.load||a.both,j=a.callback||f,k=j,l=a.complete||f,m,n;c(h?a.yep:a.nope,!!i),i&&c(i)}var i,j,l=this.yepnope.loader;if(e(a))g(a,0,l,0);else if(w(a))for(i=0;i<a.length;i++)j=a[i],e(j)?g(j,0,l,0):w(j)?B(j):Object(j)===j&&h(j,l);else Object(a)===a&&h(a,l)},B.addPrefix=function(a,b){z[a]=b},B.addFilter=function(a){x.push(a)},B.errorTimeout=1e4,null==b.readyState&&b.addEventListener&&(b.readyState="loading",b.addEventListener("DOMContentLoaded",A=function(){b.removeEventListener("DOMContentLoaded",A,0),b.readyState="complete"},0)),a.yepnope=k(),a.yepnope.executeStack=h,a.yepnope.injectJs=function(a,c,d,e,i,j){var k=b.createElement("script"),l,o,e=e||B.errorTimeout;k.src=a;for(o in d)k.setAttribute(o,d[o]);c=j?h:c||f,k.onreadystatechange=k.onload=function(){!l&&g(k.readyState)&&(l=1,c(),k.onload=k.onreadystatechange=null)},m(function(){l||(l=1,c(1))},e),i?k.onload():n.parentNode.insertBefore(k,n)},a.yepnope.injectCss=function(a,c,d,e,g,i){var e=b.createElement("link"),j,c=i?h:c||f;e.href=a,e.rel="stylesheet",e.type="text/css";for(j in d)e.setAttribute(j,d[j]);g||(n.parentNode.insertBefore(e,n),m(c,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))};
/*! http://users.techtarget.com/registration/js/userreg-lib.js - 02/10/2017 01:01 PM */
function isNewUser(){var e=getCookieValue("crs");return e==""}function isTechtargetUser(){var e=getCookieValue("crs");return e!=""}function isUidLoggedIn(){var e=getCookieValue("uidLoggedIn");return e!=""}function loadCookies(e){var t="TTGTCHECKDONE";if(getCookieValue(t))logMessage("loadCookies called within the last 5 minutes. Do not call again."),e();else if(isNewUser()){logMessage("Unrecognized user; checking master domain for cookies");var r=["http://users.techtarget.com/registration/json/common/GetCookiesWithCallback.page?callback=loadCookiesCallback"],i=document.getElementsByTagName("head")[0];loadJs(i,r,e),createCookieDT(t,"y",5,"m",getTopLevelDomain())}else deleteCookie(t),logMessage("Recognized user; no need for master domain cookie check"),e()}function loadCookiesCallback(e){var t=e.results.length;logMessage(t+" cookies returned in callback");for(var r=0;t>r;r++)e.results[r].name in cookieNames?createCookieDT(e.results[r].name,e.results[r].value,5,"y",getTopLevelDomain()):logMessage("Cookie "+e.results[r].name+" not a userreg cookie; skipping")}function isCrossDomain(){var e=getTopLevelDomain(),t="techtarget.com";return e!=t}function setOrRedirectCookies(e){isCrossDomain()?(setLoginCookies(e,getTopLevelDomain()),logMessage("Cross domain cookie setting not implemented")):setLoginCookies(e,".techtarget.com")}function setLoginCookies(e,t){t&&t.substr(0,1)!="."&&(t="."+t);for(var r in e)createCookieDT(r,e[r],5,"y",t)}function getPromoCode(){var e=getQueryStrings().Offer;return e||(e=getCookieValue("Offer")),e}function getQueryStrings(){var e={},t=function(e){return decodeURIComponent(e.replace(/\+/g," "))},r=location.search.substring(1),i=r.split("&");for(var n in i){var o=i[n].split("=");o.length>1&&(e[t(o[0])]=t(o[1]))}return e}function createCookie(e,t,r){createCookieDT(e,t,r,"d",getTopLevelDomain())}function createCookieD(e,t,r,i){createCookieDT(e,t,r,"d",i)}function createCookieDT(e,t,r,i,n){var o=new Date;switch(i){case"y":i="years",o.setTime(o.getTime()+r*365*24*60*60*1e3);break;case"mo":i="months",o.setTime(o.getTime()+r*30*24*60*60*1e3);break;case"d":i="days",o.setTime(o.getTime()+r*24*60*60*1e3);break;case"h":i="hours",o.setTime(o.getTime()+r*60*60*1e3);break;case"m":i="minutes",o.setTime(o.getTime()+r*60*1e3);break;case"s":i="seconds",o.setTime(o.getTime()+r*1e3);break;case"ms":i="milliseconds",o.setTime(o.getTime()+r)}logMessage("Creating cookie "+e+" for "+r+" "+i+" on domain "+n+" with value: "+t);var a="";r&&(a="; expires="+o.toGMTString()),document.cookie=e+"="+t+a+"; path=/; domain="+n}function deleteCookie(e){getCookieValue(e)&&createCookie(e,"",-1)}function getTopLevelDomain(){var e=window.location.hostname,t=/(?:(?:dev\.eng|eng|qa)\.)?[^\.]+(?:\.|\.co\.|\.com\.)[^\.]+$/;return t.exec(e)}function getTopLevelDomainEnv(e){var t=getTopLevelDomain()[0];return e||(t=t.replace(/(dev\.eng|eng|qa)\./i,"")),t}function onScriptLoadCallback(e,t){var r=!1;e.onreadystatechange=e.onload=function(){e.readyState&&e.readyState!="loaded"&&e.readyState!="complete"||r||(r=!0,t())}}function loadJs(e,t,r){var i=document.createElement("script");i.type="text/javascript",i.src=t.pop(),t.length>0?onScriptLoadCallback(i,function(){loadJs(e,t,r)}):onScriptLoadCallback(i,r),logMessage("Loading script "+i.src),e.appendChild(i)}function loadCss(e,t,r){var i=document.createElement("link");i.setAttribute("rel","stylesheet"),i.setAttribute("type","text/css"),i.setAttribute("href",t),onScriptLoadCallback(i,r),e.appendChild(i)}function getCookieValue(e){var t=document.cookie,r=t.indexOf(e+"=");if(r!=-1){var i=r+e.length+1,n=t.indexOf(";",i);n==-1&&(n=t.length);var o=t.substring(i,n);return o}return""}function stringify(e){var r=typeof e;if(r!="object"||e===null)return t=="string"&&(e='"'+e+'"'),e+"";var i,n,o=[],a=e&&e.constructor==Array;for(i in e)n=e[i],t=typeof n,t=="string"?n='"'+n+'"':t=="object"&&n!==null&&(n=JSON.stringify(n)),o.push((a?"":'"'+i+'":')+(n+""));return(a?"[":"{")+(o+"")+(a?"]":"}")}function inlineCallback(id,content){for(var js="",html="",root=document.getElementById(id),size=content.length,x=0;size>x;x++){var row=content[x];logMessage(row),row.contentType=="BLOCK"?(row.CONTENT!=void 0&&(html+=generateBlock(row)),row.SCRIPT!=void 0&&(js+=generateJavascript(row))):row.contentType=="QUESTION"?html+=generateQuestion(row,id):logMessage("TODO:  implement process for "+row.contentType)}root.innerHTML=html,eval(js);var params=getPageParams(id);for(var key in params){var element=document.getElementById(id+"_"+key);element!=void 0&&element!=null&&(element.value=params[key])}if(countryListId!=""){var country=document.getElementById(countryListId);country.onchange=function(){provListId!=""&&getProvincesInline(),consentId!=""&&getConsentCountriesInline()},provListId!=""&&getProvincesInline()}consentId!=""&&getConsentCountriesInline()}function generateJavascript(e){var t="";return e.SCRIPT!=void 0&&(t=e.SCRIPT),t}function generateBlock(e){var t="";return e.CONTENT!=void 0&&(t=e.CONTENT),t}function generateQuestion(row,id){var inputId=row.id,value=row.value!==void 0?row.value:"",setVal=value||"";if(row.type=="hidden"){var result='<input type="hidden" name="'+row.code+'" id="'+inputId+'" ';return setVal&&(result+='value="'+setVal+'"'),result+="/>"}if(row.type=="label"){var result="";return result+=getQuestionBeforeHTML(row),row.DISPLAY_VALUE!=void 0&&row.DISPLAY_VALUE!=""&&(result+=row.DISPLAY_VALUE+"&nbsp;"),result+=getQuestionAfterHTML(row)}if(row.type=="display"){var result="";return result+=getQuestionBeforeHTML(row),row.DISPLAY_VALUE!=void 0&&row.DISPLAY_VALUE!=""&&(result+='<label for="'+inputId+'">'+row.DISPLAY_VALUE+"&nbsp;</label>"),result+=setVal,result+='<input type="hidden" id="'+inputId+'" name="'+row.code+'" value="'+setVal+'" />',result+=getQuestionAfterHTML(row)}if(row.type=="text"||row.type=="password"||row.type=="textarea"){var result="";result+=getQuestionBeforeHTML(row),row.DISPLAY_VALUE!=void 0&&row.DISPLAY_VALUE!=""&&(result+='<label for="'+inputId+'">'+row.DISPLAY_VALUE+"&nbsp;</label>"),row.DESCRIPTION!=void 0&&row.DESCRIPTION!=""&&(result+="<small>"+row.DESCRIPTION+"</small>");var txtStyle=row.STYLES!=void 0?row.STYLES+" ":"",txtEvents=row.EVENTS!=void 0?row.EVENTS+" ":"";if(row.type!="textarea")result+='<input id="'+inputId+'" name="'+row.code+'" '+txtStyle+txtEvents+' value="'+setVal+'" type="'+row.type+'" maxlength="'+row.MAX_LENGTH+'" size="25" '+getQuestionRequired(row)+" />";else{var txtRows=row.TEXTAREA_ROWS!=void 0?row.TEXTAREA_ROWS:5;result+='<textarea id="'+inputId+'" name="'+row.code+'" '+txtStyle+txtEvents+getQuestionRequired(row)+" rows="+txtRows+" cols=40>"+setVal+"</textarea>"}return result+=getQuestionErrorMessages(inputId,id,row),result+=getQuestionAfterHTML(row)}if(row.type=="submit"){var result="";return result+=getQuestionBeforeHTML(row),result+='<input type="submit" value="'+row.DISPLAY_VALUE+'" ',row.STYLES!=void 0&&(result+=row.STYLES+" "),row.EVENTS!=void 0&&(result+=row.EVENTS),result+="/>",result+=getQuestionAfterHTML(row)}if(row.type=="consent"){var result="";return result+=getQuestionBeforeHTML(row),result+=row.DISPLAY_VALUE,result+='<input type="hidden" name="'+row.code+'" value="'+row.answers[0].value+'" id="'+inputId+'"/>',result+=getQuestionAfterHTML(row),consentDivId=id+"_declarationOfConsent",consentId=inputId,result}if(row.type=="checkbox"||row.type=="radio"||row.type=="multiselect"||row.type=="intl_optin_checkbox"){var result="";result+=getQuestionBeforeHTML(row),row.DISPLAY_VALUE!=void 0&&row.DISPLAY_VALUE!=""&&(result+='<label for="'+inputId+'">'+row.DISPLAY_VALUE+"&nbsp;</label>"),result+=row.type!="radio"?"<fieldset class='checkboxList'><ul><li>":"<fieldset class='radioButtonList inline'><ul><li>";for(var j=0;row.answers.length>j;j++){var uniqueInputId=getUniqueMultiAnswerId(inputId,j);result+=row.type!="radio"?'<input type="checkbox" name="'+row.code+'" value="'+row.answers[j].value+'" id="'+uniqueInputId+'" '+getQuestionRequired(row)+"/>":'<input type="radio" name="'+row.code+'" value="'+row.answers[j].value+'" id="'+uniqueInputId+'" '+getQuestionRequired(row)+"/>",row.answers[j].DISPLAY_VALUE!=void 0&&row.answers[j].DISPLAY_VALUE!=""&&(result+='<label for="'+uniqueInputId+'">'+row.answers[j].DISPLAY_VALUE+"</label>")}return result+="</li></ul></fieldset>",result+=getQuestionErrorMessages(inputId,id,row),result+=getQuestionAfterHTML(row)}if(row.type=="select"||row.type=="country"||row.type=="province"){var result="";result+=getQuestionBeforeHTML(row),row.DISPLAY_VALUE!=void 0&&row.DISPLAY_VALUE!=""&&(result+='<label for="'+inputId+'">'+row.DISPLAY_VALUE+"&nbsp;</label>"),row.DESCRIPTION!=void 0&&row.DESCRIPTION!=""&&(result+="<small>"+row.DESCRIPTION+"</small>"),result+="<select ",result+='id="'+inputId+'" ',result+='name="'+row.code+'" '+getQuestionRequired(row),row.STYLES!=void 0&&row.STYLES!=""&&(result+=row.STYLES+" "),row.EVENTS!=void 0&&row.EVENTS!=""&&(result+=row.EVENTS+" "),result+=">";var defaultValue="",valueSelected=!1;switch(row.type){case"province":for(var s="window.provList = {",size=row.answers.length,x=0,y=0;size>x;x++){var selected=0,answer=row.answers[x];if(answer.value=="COUNTRY_MARKER")x>0&&(s+="],"),s+="'"+answer.DISPLAY_VALUE+"':[",y=0;else{var labelTxt=answer.DISPLAY_VALUE;y>0?s+=", ":row.PLACEHOLDER_LABEL!=void 0&&(labelTxt=row.PLACEHOLDER_LABEL),valueSelected||setVal==""||setVal!=answer.value||(selected=1,valueSelected=!0),y=1,s+=' { V:"'+answer.value+'", T:"'+labelTxt+'", S: '+selected+" }"}}s+="] }",eval(s),provListId=inputId;break;case"country":countryListId=inputId,defaultValue=setVal||window.defaultInlineRegCountry||"";default:for(var size=row.answers.length,x=0;size>x;x++){var answer=row.answers[x];result+="<option ",result+='value="'+answer.value+'"',valueSelected||defaultValue==""||defaultValue!=answer.value||(result+=" SELECTED",valueSelected=!0),result+=">",result+=x==0&&row.PLACEHOLDER_LABEL!=void 0?row.PLACEHOLDER_LABEL:answer.DISPLAY_VALUE,result+="</option>"}}return result+="</select>",result+=getQuestionErrorMessages(inputId,id,row),result+=getQuestionAfterHTML(row)}return row.type=="script"?(eval(row.DISPLAY_VALUE),""):(logMessage("TODO:  implement question for "+row.type),void 0)}function getUniqueMultiAnswerId(e,t){return e+getAnswerOptionIdDelimiter()+t}function getBaseInputElement(e){var t=getBaseInputId(e);return document.getElementById(t)}function getBaseInputId(e){return e.id.replace(/__answerid__(.*)/,"")}function getAnswerOptionIdDelimiter(){return"__answerid__"}function getQuestionBeforeHTML(e){var t="";return e.LIST_CLASS!=void 0&&(t="<li",e.LIST_CLASS!=""&&(t+=' class="'+e.LIST_CLASS+'"'),t+=">"),e.BEFORE_HTML!=void 0&&(t+=e.BEFORE_HTML),t}function getQuestionAfterHTML(e){var t="";return e.AFTER_HTML!=void 0&&(t+=e.AFTER_HTML),e.LIST_CLASS!=void 0&&(t+="</li>"),t}function getQuestionRequired(e){return e.required!=""?"data-required='"+e.required+"' ":void 0}function getQuestionErrorMessages(e,t,r){var i="",n="errorMessageInput";return(or=window.overrideInlineRegErrorMessageClass)&&or[t]&&(n=or[t]),(or=window.overrideInlineRegErrorMessageContainer)&&(ord=or[t])&&ord.open&&(i+=ord.open),r.VALIDATION_MESSAGE_QUESTION_BLANK!=void 0&&r.VALIDATION_MESSAGE_QUESTION_BLANK!=""&&(i+="<p id='"+e+".blank' class='"+n+" hidden'>"+r.VALIDATION_MESSAGE_QUESTION_BLANK+"</p>"),r.VALIDATION_MESSAGE_QUESTION_INVALID!=void 0&&r.VALIDATION_MESSAGE_QUESTION_INVALID!=""&&(i+="<p id='"+e+".invalid' class='"+n+" hidden'>"+r.VALIDATION_MESSAGE_QUESTION_INVALID+"</p>"),r.VALIDATION_MESSAGE_QUESTION_INVALID_EMAIL_DOMAIN!=void 0&&r.VALIDATION_MESSAGE_QUESTION_INVALID_INVALID_EMAIL_DOMAIN!=""&&(i+="<p id='"+e+".invalid.domain' class='"+n+" hidden'>"+r.VALIDATION_MESSAGE_QUESTION_INVALID_EMAIL_DOMAIN+"</p>"),r.VALIDATION_MESSAGE_QUESTION_EXISTS!=void 0&&r.VALIDATION_MESSAGE_QUESTION_EXISTS!=""&&(i+="<p id='"+e+".exists' class='"+n+" hidden'>"+r.VALIDATION_MESSAGE_QUESTION_EXISTS+"</p>"),r.MAX_LENGTH&&(r.VALIDATION_MESSAGE_QUESTION_MAXLENGTH!=void 0?i+="<p id='"+e+".maxlength' class='"+n+" hidden'>"+r.VALIDATION_MESSAGE_QUESTION_MAXLENGTH+"</p>":window[t+"_VALIDATION_MAXLENGTH"]&&(i+="<p id='"+e+".maxlength' class='"+n+" hidden'>"+window[t+"_VALIDATION_MAXLENGTH"]+"</p>")),ord&&ord.close&&(i+=ord.close),i}function getProvincesInline(){getProvincesCommon(provListId,countryListId,provList)}function getProvincesCommon(e,t,r){var i=document.getElementById(e),n=0;if(i!==void 0&&i!=null&&(country=document.getElementById(t),prov=r[country.options[country.selectedIndex].value],i.innerHTML="",typeof prov!="undefined"))for(x=0;prov.length>x;x++)option=document.createElement("option"),option.setAttribute("value",prov[x].V),option.innerHTML=prov[x].T,i.appendChild(option),prov[x].S==1&&(n=x);i.options.length>n&&(i.options[n].selected=!0)}function getConsentCountriesInline(){if(consentId!=""&&countryListId!=""&&typeof consentLangList!="undefined"&&Object.keys(consentLangList).length>0){var e=document.getElementById(countryListId).value,t=consentLangList[e];t!=null&&t!=""?(replaceLangCdInline(t),document.getElementById(consentId).value=t):(consentDivId!=""&&(document.getElementById(consentDivId).style.display="none"),document.getElementById(consentId).value="en")}showConsentLink()}function showConsentLink(){if(consentId!=""){var e=document.getElementById(consentId).value;e!="en"&&(consentDivId!=""&&(document.getElementById(consentDivId).style.display="block"),replaceLangCdInline(e))}}function replaceLangCdInline(e){$("a.consentWindow").attr("href",function(t,r){return r.replace(/(languageCd=)[a-z]+/gi,"$1"+e)})}function executeInline(e,t,r){executeInlineByPage(e,t,r,"1")}function executeInlineByPage(e,t,r,i){executeInlineByPageAndParams(e,t,r,i,null)}function executeInlineByPageAndParams(e,t,r,i,n){var o="type="+r+"&callback=inlineCallback&div="+t+"&pageNumber="+i;if(n&&typeof n=="object")for(key in n)o+="&"+key+"="+n[key];var a=["http://users.techtarget.com/registration/"+e+"/InlineRegister.page?"+o],s=document.getElementsByTagName("head")[0],l=function(){};loadJs(s,a,l)}function getQueryStringValue(e){for(var t=window.location.search.substring(1),r=t.split("&"),i=0;r.length>i;i++){var n=r[i].split("=");if(n[0]==e)return n[1]}return null}function getPageParams(e){return window[e+"_variables"]}function getTimezoneCode(){var e=getTimezone();return Math.floor(e+436)}function getTimezone(){var e=new Date,t=new Date(e.getFullYear(),0,1),r=new Date(e.getFullYear(),6,1),i=Math.max(t.getTimezoneOffset()/60,r.getTimezoneOffset()/60);return-i}function isUsEnglish(){var e=window.navigator.userLanguage||window.navigator.language;return e!=null&&e.match(/en-US/i)!=null}function isAssumedUs(){var e=getTimezoneCode();return isUsEnglish()&&e>=426&&431>=e}function logMessage(e){typeof console=="undefined"||console.log(e)}function vRequired(e,t,r){var i=!0;r=r==""?"Please complete all fields.":r;for(var n=0;t.length>n;n++){var o=e.form.elements[t[n]].value;if(o=o.replace(/^\s\s*/,"").replace(/\s\s*$/,""),o==""){alert(r),i=!1;break}}return i}function vEmail(e,t,r){var i=!0,n=/^[a-z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?(?:\.[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?)*$/i;return r=r==""?"Please enter a valid email address.":r,n.test(e.form.elements[t].value)||(alert(r),i=!1),i}var cookieNames=Array();cookieNames.tt_ut=!0,cookieNames.crs=!0;var provListId="",countryListId="",consentDivId="",consentId="";window.overrideInlineRegErrorMessageClass={},window.overrideInlineRegErrorMessageContainer={};var urValidation={validationCfg:{},currentForm:{},validateFields:[],skipemailExists:!1,skiphandleExists:!1,validate:function(e,t){var r=!1,i=!1,n=!1;e.maxlength===void 0&&(e.maxlength={}),this.validationCfg=e,this.currentForm=t;var o,a,s,l;this.validateFields=s=this.getValidationFields(e);var d={};try{for(var u=0;s.length>u;u++)if(a=s[u],l=!1,o=t.elements[a],o&&o.getAttribute("disabled")==null&&!d[o.name]&&!this.isFieldDisplayNone(o)){this.setError(o,!1,"");var c=this.trim(o.value);if(o.type!="text"&&o.type!="textarea"||c==o.value||(o.value=c),(required=e.required)&&this.arrayContains(required.fields,a)&&(o.type=="text"||o.type=="password"||o.type=="textarea"?l=c=="":o.type=="select-one"?l=1>o.options.length||o.selectedIndex==-1?!0:o.options[o.selectedIndex].value=="":(o.type=="radio"||o.type=="checkbox")&&(l=this.validateRadioCheckbox(l,t,d,o)),o.type!="radio"&&o.type!="checkbox"&&l&&this.setError(o,l,"required")),l||o.type!="text"&&o.type!="password"&&o.type!="textarea"||!(maxlength=o.getAttribute("maxlength"))||o.id.indexOf("vendor.bp.questions.response")!=-1||maxlength>=this.getUtf8Length(c)||(l=!0,this.setError(o,l,"maxlength")),l||o.type!="text"||(!l&&(emailFormat=e.emailFormat)&&this.arrayContains(emailFormat.fields,a)&&(l=!this.validateEmailFormat(c),l&&this.setError(o,l,"emailFormat")),!l&&(emailDomain=e.emailDomain)&&this.arrayContains(emailDomain.fields,a)&&(l=!this.validateEmailDomain(c.toLowerCase()),l&&this.setError(o,l,"emailDomain")),!l&&(phoneFormat=e.phoneFormat)&&this.arrayContains(phoneFormat.fields,a)&&phoneFormat.countryField&&(l=!this.validatePhoneFormat(o,c,phoneFormat,t),l&&this.setError(o,l,"phoneFormat")),!l&&(handleFormat=e.handleFormat)&&handleFormat.field==a&&(l=!this.validateHandleFormat(c),l&&this.setError(o,l,"handleFormat"))),!l&&o.type=="password"&&(pw=e.passwrdFormat)&&(pw.passwrdField==a||pw.confirmField==a)){var g=t.elements[pw.passwrdField],m=t.elements[pw.confirmField];if(g&&m){var v=this.trim(g.value),p=this.trim(m.value);v!=""&&p!=""&&(l=v!=p)}l&&this.setError(o,l,"passwrdFormat")}l&&(r=!0),(emailExists=e.emailExists)&&a==emailExists.field&&(i=l),(handleExists=e.handleExists)&&a==handleExists.field&&(n=l)}var f=e.loginOnExists;if(!l&&f&&this.loginIsVisible(f)){var h=this.validateLogin(f,t);if(h=="sent")return!1}if(!i){var E=this.loginIsVisible(f);if(!E){var I=this.validateFieldExists(s,e,t,"email");if(I=="sent")return!1;I=="errs"&&(r=!0)}}if(!n){var I=this.validateFieldExists(s,e,t,"handle");if(I=="sent")return!1;I=="errs"&&(r=!0)}}catch(w){logMessage("ERROR: "+w),r=!0}return this.setErrorHighlight(s,e,t),this.setErrorMessage(s,e,t),this.triggerJqueryEvent(e,r),logMessage("urValidation.validate - errs = "+r+" - returns "+!r),!r},validateLogin:function(e){if(this.loginIsVisible(e)&&window.jQuery){var t={email:this.trim(this.currentForm.elements[e.fields.un].value),password:encodeURIComponent(this.trim(this.currentForm.elements[e.fields.pw].value))};return(pg=e.processingGif)&&this.showProcessingGif(!0,pg),$.getJSON("http://users.techtarget.com/registration/rest/RegistrationService/Login?callback=?",t,function(e){logMessage("LOGIN CALLBACK"),urValidation.loginCallback(e)}),"sent"}},loginCallback:function(e){if(e&&(r=e.RegTx)&&(r=r[0])){var t=this.validationCfg,i=this.currentForm,n=this.validateFields,o=t.loginOnExists,a=i.elements[o.fields.pw];if(r.status=="SUCCESS"){var e=new Date;e.setTime(e.getTime()+15768e7);for(var s=e.toGMTString(),l=0;r.cookies.cookie.length>l;l++){var d=r.cookies.cookie[l];document.cookie=d.name+"="+d.value+"; expires="+s+"; path=/; domain="+getTopLevelDomainEnv(!1),d.name=="crs"&&(crs=d.value)}var u=i.elements,c={};typeof crs!="undefined"&&(c.crs=encodeURIComponent(crs)),(v=u.resId)&&v.value!=""&&(c.resId=v.value),(v=u.appCD)&&v.value!=""&&(c.appCD=v.value),(v=u.appName)&&v.value!=""&&(c.appName=v.value),(v=u.guideContentId)&&v.value!=""&&(c.guideContentId=v.value),(v=u.ad_id)&&v.value!=""&&(c.ad_id=v.value),c.fromURL=encodeURIComponent(window.location.href),c.isLogin=1,c.t=(new Date).getTime(),executeInlineByPageAndParams(o.siteName,o.targetDiv,"inlineregister",o.nextPageNumber,c)}else this.setError(a,!0,"loginOnExists"),this.setErrorHighlight(n,t,i),this.setErrorMessage(n,t,i),(pg=o.processingGif)&&this.showProcessingGif(!1,pg)}else this.setError(a,!0,"loginOnExists"),this.setErrorHighlight(n,t,i),this.setErrorMessage(n,t,i),(pg=o.processingGif)&&this.showProcessingGif(!1,pg)},loginIsVisible:function(e){return e&&e.fields&&(pw=this.currentForm.elements[e.fields.pw])?pw.getAttribute("disabled")==null:!1},validateRadioCheckbox:function(e,t,r,i){var n=t.elements[i.name],o=i.checked;if(!o)for(var a=0;n.length>a;a++)if(n[a].checked){o=!0;break}for(var a=0;n.length>a;a++)o?this.setError(n[a],!1,""):(e=!0,this.setError(n[a],e,"required"));return r[i.name]=!0,e},validateEmailDomain:function(e){for(var t=[/@gmail\./,/@yahoo\./,/@hotmail\./,/@msn\./,/@live\./,/@outlook\./,/@mailinator\./,/@mail\./,/@aol\./,/@comcast\.net/,/@verizon\.net/,/@earthlink\.net/,/@rediffmail\./,/@cox\.net/,/@gmx\./,/@ymail\./,/@qq\./,/@126\./,/@163\./,/@sbcglobal\.net/,/@rocketmail\./,/@indiatimes\./,/@sina\./,/@bellsouth\.net/,/@btinternet\./],r=0;t.length>r;r++)if(t[r].test(e))return!1;return!0},validateEmailFormat:function(e){var t=/^[a-z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?(?:\.[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?)*$/i;return t.test(e)},validateHandleFormat:function(e){var t=/^(?:[a-zA-Z0-9]{6,30})?$/;return t.test(e)},validatePhoneFormat:function(e,t,r,i){var n=i.elements[r.countryField].value;if(n=="")return!1;try{var o=i18n.phonenumbers.PhoneNumberUtil.getInstance()}catch(a){return logMessage("ERROR IN PHONE VALIDATION - LETTING PHONE PASS VALIDATION :: "+a),!0}try{var s=o.parseAndKeepRawInput(t,n),l=o.isValidNumber(s);if(!l){var d=o.parseAndKeepRawInput("+"+t,n);if(l=o.isValidNumber(d),!l)return!1;s=d}var u=i18n.phonenumbers.PhoneNumberFormat,c=o.getRegionCodeForNumber(s);if("US"==c||"CA"==c)var g=o.format(s,u.NATIONAL);else var g=o.format(s,u.INTERNATIONAL);e.value=g}catch(a){return logMessage("PHONE VALIDATION ERROR :: "+a),!1}return!0},validateFieldExists:function(e,t,r,i){var n=t[i+"Exists"],o=this["skip"+i+"Exists"];if(n&&(!o||r.elements[n.field].getAttribute("data-changed")=="true")){var a=r.elements[n.field];if(a&&a.type=="text"){value=this.trim(a.value);var s=a.getAttribute("data-changed")||"true",l=a.getAttribute("data-value")||"";if(value){if(s=="true"){this.setError(a,"false",""),this.setErrorHighlight(e,t,r),this.setErrorMessage(e,t,r);var d="http://users.techtarget.com/registration/json/common/GetFieldExistsWithCallback.page?callback=urValidation."+i+"ExistsCallback&"+i+"={"+i+"}&field={field}&formid={formid}",u=document.getElementsByTagName("head")[0];return d=[d.replace("{"+i+"}",encodeURIComponent(value)).replace("{field}",encodeURIComponent(a.name)).replace("{formid}",encodeURIComponent(r.id))],(pg=n.processingGif)&&this.showProcessingGif(!0,pg),loadJs(u,d,function(){}),"sent"}if(s=="false"&&(result=n.onfailure)){var c=window[result];return typeof c=="function"&&c(),"errs"}if(s=="false"&&a.getAttribute("data-"+i+"Exists-error")=="true"&&l&&value==l)return this.setError(a,!0,i+"Exists"),"errs"}}}},getErrCount:function(t){for(var r=0,i=0;t.elements.length>r;r++){var n=t.elements[r];(e=n.getAttribute("data-error"))&&e=="true"&&n.getAttribute("disabled")==null&&i++}return i},setError:function(e,t,r){e.setAttribute("data-error",t),r==""&&(currentError=e.getAttribute("data-error-type"))&&e.setAttribute("data-last-error-type",currentError),e.setAttribute("data-error-type",r)},removeAndHideError:function(e){var t=e.getAttribute("data-error")=="true";t&&(this.setError(e,!1,""),this.setErrorHighlight(this.validateFields,this.validationCfg,e.form),this.setErrorMessage(this.validateFields,this.validationCfg,e.form))},findErrorHighlightNode:function(e){if(logMessage("finding highlight node for "+e.tagName),e.type=="checkbox"||e.type=="radio"||e.type=="multiselect"||e.type=="intl_optin_checkbox"){for(var t=e.parentNode,r=0;10>r;r++){if(t.tagName=="FIELDSET")break;t=t.parentNode}return logMessage("parentNode: "+t.parentNode.tagName),t.parentNode}return logMessage("parentNode: "+e.parentNode.tagName),e&&e.parentNode?e.parentNode:e},setErrorHighlight:function(e,t,r){e=e||this.validateFields,t=t||this.validationCfg,r=r||this.currentForm;for(var i=0;e.length>i;i++){var n=r.elements[e[i]];if(n){logMessage("input: "+n+", length: "+n.length+", tagName: "+n.tagName+", id: "+n.id);var o=n.getAttribute("data-error")=="true",a=n.getAttribute("data-error-type"),s=(t.highlightNode||"self")=="parent"?this.findErrorHighlightNode(n):n,l=s.getAttribute("class");if(l&&(l=l.replace("error-NoStyle","").replace(t.highlightClass,"").replace(t.required?t.required.highlightClass||"":"","").replace(t.emailFormat?t.emailFormat.highlightClass||"":"","").replace(t.emailDomain?t.emailDomain.highlightClass||"":"","").replace(t.emailExists?t.emailExists.highlightClass||"":"","")),o){var d=t[a],u="error-NoStyle";d.highlightClass!==void 0?u=d.highlightClass||u:t.highlightClass!==void 0&&(u=t.highlightClass),s.setAttribute("class",(l?this.trim(l)+" ":"")+u)}else s.setAttribute("class",this.trim(l))}}},setErrorMessage:function(e,t,r){e=e||this.validationFields,r=r||this.currentForm,t=t||this.validationCfg;for(var i=t.errorMsgClass||"errorMessageInput",n=this.getErrCount(r),o=0;e.length>o;o++){var a,s=r.elements[e[o]];if(s){var l,d=this.trim(s.value),u=s.getAttribute("data-error")=="true",c=s.getAttribute("data-error-type"),g=s.getAttribute("data-last-error-type"),m=getBaseInputId(s);if(t.msgInline&&g&&g!=c&&(g=="required"&&(l=document.getElementById(m+".blank")),g.match(/.+Format/)&&(l=document.getElementById(m+".invalid")),g=="emailDomain"&&(l=document.getElementById(m+".invalid.domain")),g.match(/.+Exists/)&&(l=document.getElementById(m+".exists")),g=="maxlength"&&(l=document.getElementById(m+".maxlength")),l&&l.setAttribute("class",i+" hidden")),u){var v=t[c],p=v&&v.msgText?v.msgText:t.msgText?t.msgText:"Please complete all required fields.";if(t.msgInline&&(c=="required"&&(l=document.getElementById(m+".blank")),c.match(/.+Format/)&&(l=document.getElementById(m+".invalid")),c=="emailDomain"&&(l=document.getElementById(m+".invalid.domain")),c.match(/.+Exists/)&&(l=document.getElementById(m+".exists")),c=="maxlength"&&(l=document.getElementById(m+".maxlength")),l&&l.setAttribute("class",i)),t.msgType=="block"){var f=document.getElementById(t.msgBlockId);f.innerHTML=t.msgBlockFormat.replace("{msgText}",p.replace("{fieldValue}",d)),f.setAttribute("class",v.msgBlockClass?v.msgBlockClass:t.msgBlockClass),f.style.display="block"}t.msgType=="alert"&&error&&alert(p.replace("{fieldValue}",d)),a||(a=s)}else if(t.msgType=="block"){var f=document.getElementById(t.msgBlockId);n==0&&(f.innerHTML="",f.setAttribute("class",""),f.style.display="none")}}}a&&t.inputFocus!=0&&a.focus()},emailExistsCallback:function(e){this.fieldExistsCallback(e,"emailExists")},handleExistsCallback:function(e){this.fieldExistsCallback(e,"handleExists")},fieldExistsCallback:function(t,r){if(t){var i=(e=t[r])&&(e==1||e=="true"),n=t.formid?document.getElementById(t.formid):this.currentForm,o=n.elements[t.field];this["skip"+r]=!i;var a=this.validationCfg[r];if(logMessage("validation script "+r+"Callback -- isError: "+i),(pg=a.processingGif)&&this.showProcessingGif(!1,pg),o.setAttribute("data-changed",!1),o.setAttribute("data-value",this.trim(o.value)),o.setAttribute("data-"+r+"-error",i),o.onchange=function(){var e=urValidation.trim(this.value);this.setAttribute("data-changed",e!=this.getAttribute("data-value")),this.setAttribute("data-value",e);var t=this.getAttribute("data-error-type");this.setAttribute("data-"+t+"-error",e==this.getAttribute("data-value"))},this.setError(o,i,i?r:""),a.noHL===void 0&&this.setErrorHighlight(this.validateFields,this.validationCfg,n),a.noErrMsg===void 0&&this.setErrorMessage(this.validateFields,this.validationCfg,n),logMessage("validation script final callback -- formHasError: "+i+" skip"+r+" && do result"),!i&&this.getErrCount(n)==0){this.triggerJqueryEvent(this.validationCfg,i);var s="";if(a.onsuccess&&(s=a.onsuccess),s&&s!=""&&s!="submit")if(s=="donothing");else{var l=window[s];typeof l=="function"&&l()}else n.submit()}if(i){var s="";if(a.onfailure&&(s=a.onfailure),s&&s!=""){var l=window[s];typeof l=="function"&&l()}else;}}},triggerJqueryEvent:function(e,r){(t=e.triggerJqueryEvent)&&(window.jQuery?r?(logMessage("trigger event: "+t.error),$(this.currentForm).trigger(t.error)):(logMessage("trigger event: "+t.submit),$(this.currentForm).trigger(t.submit)):logMessage("Event Trigger Failed, no jquery"))},showProcessingGif:function(e,t){e?(t.hideSubmitId&&(document.getElementById(t.hideSubmitId).style.display="none"),document.getElementById(t.id).style.display=t.displayStyle):(t.hideSubmitId&&(document.getElementById(t.hideSubmitId).style.display="block"),document.getElementById(t.id).style.display="none")},getValidationFields:function(e){var t=[],r=[],i={};if(e&&((req=e.required)&&req.fields&&(t=t.concat(req.fields)),(ef=e.emailFormat)&&ef.fields&&(t=t.concat(ef.fields)),(ed=e.emailDomain)&&ed.fields&&(t=t.concat(ed.fields)),(ee=e.emailExists)&&ee.field&&(t=t.concat(ee.field)),(pf=e.phoneFormat)&&pf.fields&&(t=t.concat(pf.fields)),(hf=e.handleFormat)&&hf.field&&(t=t.concat(hf.field)),(he=e.handleExists)&&he.field&&(t=t.concat(he.field)),(pw=e.passwrdFormat)&&pw.passwrdField&&pw.confirmField&&(t=t.concat(pw.passwrdField),t=t.concat(pw.confirmField)),(loe=e.loginOnExists)&&loe.fields&&(t=t.concat(loe.fields.pw)),t.length>0))for(var n=0;t.length>n;n++)i[t[n]]||(i[t[n]]=!0,r.push(t[n]));return r},isFieldDisplayNone:function(e){do if(e&&(e.style.display=="none"||window.getComputedStyle(e).getPropertyValue("display")=="none"))return!0;while(e&&(e=e.parentNode)&&e.tagName!="HTML");return!1},getUtf8Length:function(e){for(var t=0,r=0;e.length>r;r++){var i=e.charCodeAt(r);i>127?i>2047?55296>i||i>57343?t+=65535>i?3:4:(t+=4,r++):t+=2:t+=1}return t},arrayContains:function(e,t){var r=!1;if(e&&t){if(e==t)return!0;var i=e.length;while(i--)if(e[i]===t){r=!0;break}}return r},trim:function(e){return e?e.replace(/^\s\s*/,"").replace(/\s\s*$/,""):""}};window.jQuery&&function(t){t.fn.formProgress=function(r){function i(){var e=n();t(this).stop().animate({width:e.toPercent*e.ratio},a.speed),a.bubble?(e.bubble.length===0?(t(this).parent().append('<div class="bubble"><div class="percent">'+e.toPercent+'%</div><div class="arrow"></div><div class="arrowInner"></div></div>'),e.bubble=t(this).next()):e.bubble.find(".percent").text(e.toPercent+"%"),e.bubble.stop().animate({left:e.toPercent*e.ratio-5},a.speed)):t(this).text(e.toPercent+"%")}function n(){var r=t(l).filter(function(){return t.support.placeholder!=void 0&&!t.support.placeholder&&(ph=this.getAttribute("placeholder"))&&ph==this.value?!1:(e=this.getAttribute("data-error"))&&e=="true"?!1:this.value!=""}).not(":checkbox, :radio, :hidden").length+t(l+":checked").filter(":visible").length,i={filled:r,ratio:s.parent().width()/100,toPercent:Math.round(r*100/o()),bubble:s.next()};return s.attr("class",a.style),i}function o(){var e=[];return t(l+":radio").filter(":visible").each(function(){var r=t(this).attr("name");0>t.inArray(r,e)&&e.push(r)}),t(l).not(":radio, :hidden").length+e.length}var a={speed:300,style:"green",bubble:!1,selector:".required",minPercent:!1,message:"Please fill the required fields !"};if(r)var a=t.extend({},a,r);var s=t(this),l=a.selector,d=t(l).parents("form");return t(d).find(l+":checkbox, "+l+":radio, select"+l).on("change",function(){i.call(s)}),t(d).find("input[type=text]"+l+", input[type=password]"+l+", textarea"+l).on("keyup",function(){(ev=this.getAttribute("data-formeter-event"))&&ev!=""?ev=="blur"&&(t(this).on(ev,function(){this.getAttribute("data-error")!="true"&&i.call(s)}),this.setAttribute("data-formeter-event","set")):i.call(s)}),this.each(function(){i.call(s)})}}(jQuery)
/*! http://cdn.ttgtmedia.com/rms/ux/javascript/itkeRepoReg.js - 02/10/2017 01:01 PM */
var UserregUser=function(){var e=this;return e.userId,e.userEmail,e.firstName,e.lastName,e.userDisplayName,e.siteId,e.appCode,e.userregUrl,e.authenticated=!1,e.hasValidHandle,e.crs,e.tt_ut,e.restService={cookieReflector:"GetUserFromCookies",register:"RegisterWithPassword",login:"Login",sendForgotPassword:"SendForgotPassword",updateUserDisplayName:"UpdateHandle"},e.validations=[],e.userregError={id:"service",status:"UNAVAILABLE"},e.initialize=function(){var r=RegExp(".eng"),a=RegExp(".qa"),t=window.location.hostname,s="";s=r.test(t)?"users-0.eng.":a.test(t)?"users.qa.":"users.";var o=t.split("."),i=o.slice(-2).join(".");return e.userregUrl="http://"+s+i+"/registration/rest/RegistrationService/",e.cookieReflector()},e.cookieReflector=function(){return $.getJSON(e.userregUrl+e.restService.cookieReflector+"?callback=?",function(r){r.RegTx[0].status=="SUCCESS"&&(e.authenticated=!0,e.crs=r.RegTx[0].cookies.cookie[1].value,e.tt_ut=r.RegTx[0].cookies.cookie[0].value,e.userId=r.RegTx[0].user.userId,e.userEmail=r.RegTx[0].user.email,e.firstName=r.RegTx[0].user.firstName,e.lastName=r.RegTx[0].user.lastName,e.userDisplayName=r.RegTx[0].user.handle,e.hasValidHandle=e.userDisplayName.length?!0:!1)}).fail(function(){e.validations=[e.userregError]})},e.login=function(r){var a={email:encodeURIComponent($.trim(e.userEmail)),password:encodeURIComponent(r)};return $.getJSON(e.userregUrl+e.restService.login+"?callback=?",a,function(r){r.RegTx[0].status=="SUCCESS"&&(e.authenticated=!0,e.crs=r.RegTx[0].cookies.cookie[1].value,e.tt_ut=r.RegTx[0].cookies.cookie[0].value,e.userId=r.RegTx[0].user.userId,e.firstName=r.RegTx[0].user.firstName,e.lastName=r.RegTx[0].user.lastName,e.userDisplayName=r.RegTx[0].user.handle,e.hasValidHandle=e.userDisplayName.length?!0:!1),e.validations=r.RegTx[0].validations.validation?e.prepValidations(r.RegTx[0].validations.validation):[]}).fail(function(){e.validations=[e.userregError]})},e.sendForgotPassword=function(r){var a={email:encodeURIComponent($.trim(r)),siteId:encodeURIComponent(e.siteId)};return $.getJSON(e.userregUrl+e.restService.sendForgotPassword+"?callback=?",a,function(e){return e}).fail(function(){return!1})},e.register=function(r){var a={email:encodeURIComponent($.trim(e.userEmail)),password:encodeURIComponent(r),handle:encodeURIComponent($.trim(e.userDisplayName)),siteId:encodeURIComponent(e.siteId),appCode:encodeURIComponent(e.appCode)};return $.getJSON(e.userregUrl+e.restService.register+"?callback=?",a,function(r){r.RegTx[0].status=="SUCCESS"&&(e.authenticated=!0,e.hasValidHandle=!0,e.crs=r.RegTx[0].cookies.cookie[1].value,e.tt_ut=r.RegTx[0].cookies.cookie[0].value,e.userId=r.RegTx[0].user.userId),e.validations=r.RegTx[0].validations.validation?e.prepValidations(r.RegTx[0].validations.validation):[]}).fail(function(){e.validations=[e.userregError]})},e.updateUserDisplayName=function(){var r={handle:encodeURIComponent($.trim(e.userDisplayName))};return $.getJSON(e.userregUrl+e.restService.updateUserDisplayName+"?callback=?",r,function(r){r.RegTx[0].status=="SUCCESS"&&(e.authenticated=!0,e.hasValidHandle=!0,e.crs=r.RegTx[0].cookies.cookie[1].value,e.tt_ut=r.RegTx[0].cookies.cookie[0].value),e.validations=r.RegTx[0].validations.validation?e.prepValidations(r.RegTx[0].validations.validation):[]}).fail(function(){e.validations=[e.userregError]})},e.setCookies=function(){if(!e.crs||!e.tt_ut)return!1;var r={crs:e.crs,tt_ut:e.tt_ut},a=window.location.hostname,t=/(?:(?:techtarget)\.)?[^\.]+(?:\.|\.co\.|\.com\.)[^\.]+$/,s=t.exec(a)[0];return s&&s.substr(0,1)!="."&&(s="."+s),$.each(r,function(e){var a=new Date,t=5;a.setTime(a.getTime()+t*365*24*60*60*1e3);var o="; expires="+a.toGMTString();document.cookie=e+"="+r[e]+o+"; path=/; domain="+s}),document.cookie.indexOf("crs")==-1||document.cookie.indexOf("tt_ut")==-1?!1:!0},e.prepValidations=function(e){if(e[0])return e;var r=[];return r.push(e),r},e},itkeRepoErrorHandling=function(e,r,a){var t=$(e).find('input[id*="-email"]'),s=$(e).find('input[id*="-handle"]'),o=$(e).find('input[id*="-password"]');t.attr("class","required"),s.attr("class","required"),o.attr("class","required"),$.each(a,function(a,i){if(i.id=="handle"&&(i.status=="REQUIRED"&&itkeRepoDisplayError("Please enter a user name that is 6-30 characters long and contains only letters and numbers.",s),i.status=="INVALID_LENGTH"&&itkeRepoDisplayError("User names must be 6-30 characters long and contain only letters and numbers.",s),i.status=="INVALID_FORMAT"&&itkeRepoDisplayError("User names must be 6-30 characters long and contain only letters and numbers.",s),i.status=="NOT_UNIQUE"&&itkeRepoDisplayError("This user name is already taken. Please try another.",s)),i.id=="email"){if(i.status=="REQUIRED"&&itkeRepoDisplayError('Please enter a valid email address. For example "John@domain.com."',t),i.status=="NO_MATCH"){var n='We could not locate your account. Not a member? <a href="#" class="toggleRegisterLink">Register</a>';itkeRepoDisplayError(n,t)}i.status=="INVALID_FORMAT"&&itkeRepoDisplayError('Please enter a valid email address. For example "John@domain.com."',t),i.status=="NOT_UNIQUE"&&($(e).find('a[id*="toggleRegisterLink"]').trigger("click"),itkeRepoErrorBanner("A TechTarget account already exists with that email address. Please login.",e,r))}i.id=="password"&&(i.status=="REQUIRED"&&itkeRepoDisplayError("Please enter a password between 1-30 characters long.",o),i.status=="NO_MATCH"&&itkeRepoDisplayError('Your password is incorrect.<a href="#forgotPasswordModal" class="forgotPasswordLink">Forgot Password?</a>',o),i.status=="INVALID_LENGTH"&&itkeRepoDisplayError("Please enter a password between 1-30 characters long.",o)),i.id=="service"&&i.status=="UNAVAILABLE"&&itkeRepoErrorBanner("There was an error processing your information. Please try again later.",e,r),i.id=="siteId"&&i.status=="REQUIRED"&&itkeRepoErrorBanner("There was an error processing your information. Please try again later.",e,r)})},itkeRepoDisplayError=function(e,r){r.attr("class","error");var a='<span class="errorDetail">'+e+"</span>";$(a).insertAfter(r)},itkeRepoErrorBanner=function(e,r,a){var t,s,o=$(r).find('textarea[id*="form-1"]').length,i='<div class="errorMessage"><span class="errorMessageImage"></span>'+e+"</div>";o?(t=$(a).find(".formContainer.top"),s=".title"):(t=$(a).find(".formContainer.bottom"),s=".formContainer.bottom"),$(t).find(".errorMessage").length||($(".errorMessage").remove(),t.prepend(i),$("html").animate({scrollTop:$(a).find(s).offset().top},"slow"),$(".errorMessage").delay(7e3).fadeOut("slow",function(){$(".errorMessage").remove()}))},itkeRepoMessageBanner=function(e,r){var a='<div class="errorMessage gray"><span class="errorMessageImage"></span>'+e+"</div>";$(r).find(".formContainer").prepend(a)},itkeRepoRemoveErrors=function(e){$(e).find(".errorDetail").remove(),$(e).find('textarea[id*="-post_content"]').attr("class","required"),$(e).find('input[id*="-email"]').attr("class","required"),$(e).find('input[id*="-handle"]').attr("class","required"),$(e).find('input[id*="-password"]').attr("class","required")},forgotPasswordLightbox=function(e,r){$.colorbox({inline:!0,height:"270",opacity:"0.50",href:"#forgotPasswordModal",onOpen:function(){var r=$(e).find('input[id*="-email"]').val();$("#forgotPasswordModalFormMessage").val(r)}}),$("#forgotPasswordModalFormSubmit").colorbox({inline:!0,height:"160",opacity:"0.50",href:"#forgotPasswordModalConfirm",onLoad:function(){var e=$("#forgotPasswordModalFormMessage").val();$("#forgotPasswordModalHeader .email").text(e),r.sendForgotPassword(e)}})}
/*! http://cdn.ttgtmedia.com/rms/ux/javascript/gpt-1.0.min.js - 02/10/2017 01:01 PM */
var GPT=function(){function e(e){function t(){}t.prototype=e;var o=new t;return o.proxyOf=e,o}function t(e){return this.display=function(){return g.display(e)},this}function o(e){if(o.on&&!n){var t="https:"==document.location.protocol,a=(t?"https:":"http:")+"//www.googletagservices.com/tag/js/gpt.js";if(o.async){var c=document.createElement("script");c.async=!0,c.type="text/javascript",c.src=a;var i=document.getElementsByTagName("script")[0];i.parentNode.insertBefore(c,i)}else googletag.cmd.push(function(){g.pubads().enableSyncRendering()}),document.write('<script src="'+a+'"></scr'+"ipt>");n=!0,o.debug||googletag.cmd.push(function(){g.disablePublisherConsole()})}googletag.cmd.push(function(){typeof e=="function"&&e.call(o)})}googletag=this.googletag||{},googletag.cmd=googletag.cmd||[];var n=!1,g=e(googletag);return g.defineSlot=function(o,n,a){return t.call(e(googletag.defineSlot(o,n,a).addService(g.pubads())),a)},g.defineOutOfPageSlot=function(o,n){return t.call(e(googletag.defineOutOfPageSlot(o,n).addService(g.pubads())),n)},g.display=function(e){g.enableServices(),googletag.display(e)},o.on=!0,o.async=!0,o.googletag=g,o.debug=/[\?&]debug_gpt=(1|0)/.test(window.location.search)?(document.cookie="debug_gpt="+RegExp.$1+"; path=/")&&!!parseInt(RegExp.$1,10):/\bdebug_gpt=([^;]+)/.test(document.cookie)&&!!parseInt(RegExp.$1,10),o}()
//@ sourceMappingURL=gpt-1.0.min.js.map

/*! TT modules */
/* 
 * Main JS module
 * Provides a synchronous module context through which modules can be defined and called.
 * Modules must be called in the order in which they are required.
 */
var TT = (function () {

	var global = this;
	var hasOwn = Object.prototype.hasOwnProperty;
	var toString = Object.prototype.toString;
	var pop = Array.prototype.pop;
	var push = Array.prototype.push;
	var slice = Array.prototype.slice;
	var modules = {};

	function typeOf(o) {
		return o === null || o === void 0 ? String(o) : toString.call(o).toLowerCase().slice(8, -1);
	}

	function every(a, fn, o) {
		if (a)
		for (var i = 0, j = a.length; i < j; i++) {
			if (!fn.call(o || null, a[i], i, a)) return false;
		}
		return true;
	}

	function resolve(id_, mod) {
		if (id_) {
			var id = id_.replace(/^[\s\/]+|[\s\/]+$/, "");
			var path = id.charAt(0) == "." && mod && mod.id && mod.id.split("/") || [];
			if (every(id.split("/"), function (seg) {
				if (seg == "..") pop.call(this);
				else if (seg != "." && seg) push.call(this, seg);
				return true;
			}, path)) {
				return path.join("/");
			}
			throw new TypeError('could not resolve "' + id_ + '" to a module id');
		}
	}

	function apply(o, o1) {
		if (o1 !== o) {
			for (var p in o1) if (hasOwn.call(o1, p)) o[p] = o1[p];
		}
		return o;
	}

	function load(mod_) {
		var id = mod_.id = resolve(mod_.id);
		var mod = !id ? mod_ : hasOwn.call(modules, id) ? apply(modules[id], mod_) : modules[id] = mod_;
		// resolve dependencies
		var deps = [];
		if (every(mod.deps, function (id_) {
			var id = resolve(id_, mod);
			if (id) {
				if (hasOwn.call(modules, id)) {
					var dep = modules[id];
					// module exists
					if (hasOwn.call(dep, "val")) {
						// module is loaded
						return push.call(this, dep.val);
					} else {
						// wait for module
						push.call(dep.notify = dep.notify || [], mod);
					}
				} else {
					modules[id] = {notify:[mod]};
				}
			}
		}, deps)) {
			if (id) mod.val = {}; // in case module references itself in factory
			// push contextual "require" function onto deps
			push.call(deps, function (id_) {
				var id = resolve(id_, mod);
				if (id && hasOwn.call(modules, id) && hasOwn.call(modules[id], "val")) return modules[id].val;
				throw new TypeError('module "' + id + '" is undefined');
			});
			mod.val = typeOf(mod.fac) == "function" ? mod.fac.apply(TT, deps) : mod.fac;
			if (every(mod.notify, function (mod) {
				load(mod);
				return true;
			})) {
				delete mod.notify;
			}
		}
	}

	function TT() {
		var args = slice.call(arguments), len = args.length;
		if (len) {
			var i = 0, mod = {};
			if (typeOf(args[i]) == "string") mod.id   = args[i++];
			if (typeOf(args[i]) == "array")  mod.deps = args[i++];
			var o = args[i], ot = typeOf(o);
			if (ot == "function" || (ot == "object" && o !== global && !o.nodeType)) mod.fac = args[i++];
			if (i == len && mod.fac) return load(mod);
		}
		throw new TypeError("incorrect arguments. Expecting: id?, dependencies?, factory");
	}

	return TT;

}).call(null);
/*
 * Module mappings for external libraries
 * Doing this provides a level of indirection within the code
 */
TT("lib/jquery", function () {return window.jQuery;});
TT("lib/json", function () {return window.JSON3.noConflict();});
TT("lib/handlebars", function () {
	// https://github.com/wycats/handlebars.js/pull/959
	// noConflict does not return an instance
	var hb = window.Handlebars;
	hb.noConflict();
	return hb;
});
TT("lib/gpt", function () {return window.GPT;});
TT("lib/itkeRepoRegUser", function () {return window.UserregUser;});
// Util url
TT("util/url", function () {
	return {
		getTld: function (stripEnv) {
			var result = /(?:(?:dev\.eng|eng|qa)\.)?[^\.]+(?:\.|\.co\.|\.com\.)[^\.]+$/.exec(window.location.hostname);
			var domain = result ? result[0] : "";
			if (stripEnv && domain) domain = this.stripEnv(domain);
			return domain;
		},
		stripEnv: function (domain) {
			return domain.replace(/(dev\.eng|eng|qa)\./i, "");
		},
		getParam: function (name) {
			var result = new RegExp("[\\?&]" + name + "=([^&]*)", "i").exec(window.location.search);
			return result ? decodeURIComponent(result[1]) : "";
		}
	};
});
// Util cookie
TT("util/cookie", ["../url"], function (url) {
	return {
		hasCookie: function (name) {
			return !!~document.cookie.indexOf(name + "=");
		},
		getCookie: function (name) {
			var result = new RegExp("\\b" + name + "=([^;]*)").exec(document.cookie);
			return result ? decodeURIComponent(result[1]) : "";
		},
		setCookie: function (name, value, expires, path, domain, secure) {
			document.cookie = name + "=" + encodeURIComponent(value) +
			((expires) ? "; expires=" + expires : "") +
			((path) ? "; path=" + path : "") +
			((domain) ? "; domain=" + domain : "") +
			((secure) ? "; secure" : "");
			return true;
		},
		setDomainCookie: function (name, value, expires) {
			// 99% use case within the code
			return this.setCookie(name, value, expires, "/", "." + url.getTld(true));
		},
		deleteCookie: function (name, path, domain) {
			document.cookie = name + "=; expires=Thu, 01 Jan 1970 05:00:00 GMT" +
			((path) ? "; path=" + path : "") +
			((domain) ? "; domain=" + domain : "");
			return true;
		},
		deleteDomainCookie: function (name) {
			// 99% use case within the code
			return this.deleteCookie(name, "/", "." + url.getTld(true));
		}
	};
});
// Util base64
TT("util/base64", (function () {
	var global = this;
	var key = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	function shift(i, j) {return key.charAt((i >> j) & 63)}
	function encode64(s) {
		s = s || "";
		var i = 0, j = s.length, k, x, y, z = "";
		while (i < j) {
			k = i + 3;
			x = [];
			for (; i < k; i++) {
				x.push(s.charCodeAt(i));
			}
			y = (x[0] << 16) | (x[1] << 8) | x[2];
			if (isNaN(x[1])) {
				z += shift(y, 18) + shift(y, 12) + "=="
			} else if (isNaN(x[2])) {
				z += shift(y, 18) + shift(y, 12) + shift(y, 6) + "=";
			} else {
				z += shift(y, 18) + shift(y, 12) + shift(y, 6) + shift(y, 0);
			}
		}
		return z;
	}
	function decode64(s) {return "Not supported"}
	return {
		encode: function (s) {
			return (global.btoa || encode64)(unescape(encodeURIComponent(s)));
		},
		decode: function (s) {
			return decodeURIComponent(escape((global.atob || decode64)(s)));
		}
	};
}).call(null));
/* Debug module
 * Provides a common method of implementing debug switches
 * using a token with a 1|0 value.
 * Debugging is [de]activated from the browser querystring
 * and is persisted across pages by session cookie.
 * E.g.
 * var isDebug = debug.check("debug_me");
 */
TT("util/debug", function () {

	function check(name) {
		var re = new RegExp("\\b" + name + "=(1|0)\\b"), m = re.exec(window.location.search);
		return !!((m && (document.cookie = name + "=" + m[1] + "; path=/") || (m = re.exec(document.cookie))) && parseInt(m[1], 10));
	}

	return {
		check: check
	};

});
/* Logging module
 * Provides a common method of obtaining the console so
 * statements like this are not needed in the code:
 * if (window.console and isDebug) window.console.log(...)
 * Instead:
 * var isDebug = ...
 * var logger = logging.getLogger(isDebug);
 * logger.log(...);
 * Logging statements no longer require checks before use.
 */
TT("util/logging", (function () {

	var global = this;
	var proto = {
		assert: function () {},
		log: function () {},
		warn: function () {},
		error: function () {},
		debug: function () {},
		dir: function () {},
		info: function () {}
	};

	return {
		getLogger: function (enabled) {
			return enabled && global.console || proto;
		}
	};

}).call(null));
// bit.ly module
TT("util/bitly", ["../logging", "lib/jquery"], function (logging, $) {

	var logger = logging.getLogger(true);

	// bit.ly API configuration
	var bitlyApi = {
		requestUrl: 'http://api.bit.ly/v3/shorten',
		login: 'uxtechtarget',
		key: 'R_3711c9f9013670f25d70b047c8deb6f2'
	};

	return {
		shortenUrl: function (urlToShorten, callback) {
			this.shortenUrlPromise(urlToShorten).done(function (url) {
				if (typeof callback == "function") {
					callback.call(this, url);
				} else {
					return url;
				};
			});
		},
		shortenUrlPromise: function (urlToShorten) {
			// ajax request to bit.ly service wrapped in an outer deferred
			// so that the request always resolves to a value
			return $.Deferred(function (defer) {
				$.ajax({
					url: bitlyApi.requestUrl,
					contentType: "application/x-www-form-urlencoded",
					dataType: "jsonp",
					data: ({
						login: bitlyApi.login,
						apiKey: bitlyApi.key,
						longUrl: urlToShorten
					})
				}).done(function (resp) {
					if (resp.status_txt === "OK") {
						var url = resp.data.url;
						logger.log("bit.ly API shortened: %s", url);
						defer.resolve(url);
					} else {
						logger.log("bit.ly API status: %s", resp.status_txt);
						defer.resolve('');
					}
				}).fail(function (jqXHR, textStatus) {
					logger.log("Error getting shortened url from bit.ly: %s", textStatus, jqXHR);
					defer.resolve('');
				});
			}).promise();
		}
	};

});
// Recaptcha module
TT("util/recaptcha", ["context", "lib/jquery"], function (ctx, $) {

	// recaptcha API configuration
	var recaptchaApi = {
		script: 'http://www.google.com/recaptcha/api/js/recaptcha_ajax.js',
		key: '6LeNJAQAAAAAALNfHl-1afGZI24Gn-xsuS48kTKE' // public global key, will work on all .techtarget.com domains
	};

	return {
		getRecaptcha: function (id, tabIndex, theme, cb) {
			// returns Recaptcha in specified id; http://code.google.com/apis/recaptcha/intro.html
			// required to pass in dom element (id) where recaptcha will appear
			$.ajax({
				url: recaptchaApi.script,
				dataType: 'script'

			}).done(function () {
				// this check probably isn't necessary since if the script fails to load request.done() won't fire
				// and no point doing request.fail since 404s don't get reported in jqXHR for cross-domain stuff
				// if the key is wrong, no recaptcha appears and no errors are thrown
				if (typeof Recaptcha != "undefined") {
					Recaptcha.create(recaptchaApi.key, id, {
						tabindex: tabIndex || 0,
						theme: theme || "red", // red, white, blackglass, clean
						callback: cb || "",
						lang: ctx.lang
					});
				}
			});
		}
	};

});
// Context module
TT("context", ["page", "util/url", "util/cookie", "lib/jquery"],
	function (page, url, cookie, $, require) {

	// Offer=
	var offer = url.getParam("Offer");
	if (offer) {
		cookie.setDomainCookie("Offer", offer);
	}

	var domain = url.getTld();
	var tld = url.stripEnv(domain);
	var envName = /(dev\.eng|eng|qa)\./i.exec(domain);

	var mod = {
		envName: envName ? envName[1] : "prod",
		domain: domain,
		tld: tld,
		isPreview: window.location.port == 8080,
		offer: offer,
		asrc: url.getParam("asrc")
	};

	$.extend(mod, page);

	mod.lang = mod.lang || "en";
	mod.i18n = require("i18n/" + mod.lang).say;

	mod.microsite = /^microsite/i.test(mod.siteType);

	mod.isDloWidget = !!(mod.dloid && mod.zone !== 'FULFILLMENT');
	mod.isDloFulfillment = !!(mod.dloid && mod.zone === 'FULFILLMENT');

	return mod;

});
/*
 * Context UI module
 *
 * Centralized window event handlers with a custom event chain
 * which the js modules can tie into based on specfic events
 * we care about: header gets pinned; layout changed; ads have
 * loaded, etc...
 * This will cut down on the number of calculations made during
 * a scroll or resize event, which will improve page speed.
 *
 * $(document).on(ui.events.xxx, function (evt) {
 *     var ui = evt.ui;
 *     ...
 * });
 * - or -
 * ui.on('xxx', function (evt) {
 *     var ui = evt.ui;
 *     ...
 * });
 *
 * This module will also serve as a cache store for computed
 * widths, heights, offsets, etc... of the page headers and
 * columns to avoid duplicate calculations in modules.
 *
 * The premise of this is that these computed values after
 * load will only change in response to a layout change, and
 * so caching the value on layout change will prevent actions
 * that cause browser reflow.
 */
TT("context/ui", ["util/logging", "util/debug", "lib/jquery"],
	function (logging, debug, $) {

	var isDebug = debug.check("debug_ui"),
		logger = logging.getLogger(isDebug);

	// state variables to throttle resize and scroll events
	var resizeTimeout,
		scrollTimeout;

	// default module
	var ui = {
		breakpoint: 320,
		layoutName: "mobile",
		viewport: {
			w: 0,
			h: 0
		},
		scroll: {
			x: 0,
			y: 0
		},
		breakpoints: {
			mobile:  320,
			tablet:  640,
			tabletH: 960,
			desktop: 1280
		},
		events: {
			ready:  "uiReady.tt",
			load:   "uiLoad.tt",
			resize: "uiResize.tt",
			layoutChange: "uiLayoutChange.tt",
			scroll: "uiScroll.tt",
			adLoad: "adLoad.tt",
			adInterstitial: "adInterstitial.tt",
			headerPinChange: "uiHeaderPinChange.tt"
		},
		fireEvent: function (type, args) {
			$(function () {
				fireEvent(type, args);
			});
		},
		on: listenEvent
	};

	/*
	 * Extend the UI with comparision functions to eliminate muddy
	 * conditionals in the code
	 *
	 * Old: TT.mq >= TT.mqs.desktop
	 *      TT.mq < TT.mqs.desktop
	 *      TT.mq === TT.mqs.desktop_w
	 * New: ui.isGtTablet()
	 *      ui.isLtTabletH()
	 *      ui.isDesktop()
	 */
	function extendUi() {
		var bps = this.breakpoints;
		for (var p in bps) {
			if (bps.hasOwnProperty(p)) {
				var bp = bps[p],
					base = p.charAt(0).toUpperCase() + p.substring(1),
					fn = "is" + base;
				this[fn] = new Function("return this.breakpoint == " + bp);
				if (bp > 320) {
					fn = "isLt" + base;
					this[fn] = new Function("return this.breakpoint < " + bp);
				}
				if (bp < 1280) {
					fn = "isGt" + base;
					this[fn] = new Function("return this.breakpoint > " + bp);
				}
			}
		}
		return this;
	}

	// cross-browser viewport values that will match @media queries
	function getViewport() {
		var w1 = document.documentElement.clientWidth,
			w2 = window.innerWidth  || 0,
			h1 = document.documentElement.clientHeight,
			h2 = window.innerHeight || 0;
		return {
			w: Math.max(w1, w2),
			h: Math.max(h1, h2),
			inner: {
				w: Math.min(w1, w2) || w1,
				h: Math.min(h1, h2) || h1
			}
		};
	}

	function setViewportAndLayout() {
		var d = false,
			w = (ui.viewport = getViewport()).w,
			x = w > 1279 ? 1280 : w > 959 ? 960 : w > 639 ? 640 : 320;
		if (x != ui.breakpoint) {
			ui.breakpoint = x;
			switch (x) {
				case 1280: ui.layoutName = "desktop";  break;
				case 960:  ui.layoutName = "tablet_h"; break;
				case 640:  ui.layoutName = "tablet";   break;
				case 320:  ui.layoutName = "mobile";   break;
			}
			d = true;
		}
		logger.log("viewport: %d x %d", ui.viewport.w, ui.viewport.h);
		return d;
	}

	function setScroll() {
		var d = false,
			w = $(window),
			x = w.scrollLeft(),
			y = w.scrollTop();
		if (y != ui.scroll.y || x != ui.scroll.x) {
			ui.scroll = {
				x: x,
				y: y,
				delta: {
					x: x - ui.scroll.x,
					y: y - ui.scroll.y
				}
			};
			d = true;
		}
		logger.log("scroll: %d, %d", ui.scroll.x, ui.scroll.y);
		return d;
	}

	function fireEvent(type, args) {
		var type = ui.events[type];
		if (type) {
			var evt = {type: type, ui: ui};
			if ($.type(args) == "object") $.extend(evt, args);
			$.event.trigger(evt);
		}
		return ui;
	}

	function listenEvent(type, handler) {
		var types = type.split(" "),
			type;
		for (var i = 0, len = types.length; i < len; i++) {
			type = ui.events[types[i]];
			if (type) {
				$(document).on(type, handler);
			}
		}
		return ui;
	}

	// call immediately to set values needed by ads, etc...
	setViewportAndLayout();

	$(document).ready(function () {
		// call again to set inner widths based on scrollbars
		setViewportAndLayout();
		fireEvent("ready");
	});

	$(window).load(function () {
		fireEvent("load");

	}).resize(function () {
		clearTimeout(resizeTimeout);
		resizeTimeout = setTimeout(function () {
			var layoutChanged = setViewportAndLayout();
			fireEvent("resize");
			if (layoutChanged) fireEvent("layoutChange");
		}, 100);

	}).scroll(function () {
		clearTimeout(scrollTimeout);
		scrollTimeout = setTimeout(function () {
			var scrollChanged = setScroll();
			if (scrollChanged) fireEvent("scroll");
		}, 11);

	});

	return extendUi.call(ui);

});
/* 
 * Context user module
 * This module extends the context with user methods;
 * it does not expose a model of its own
 */
TT(["context", "util/url", "util/cookie", "util/logging", "util/debug", "lib/jquery"],
	function (ctx, url, cookie, logging, debug, $) {

	var isDebug = debug.check("debug_user");
	var logger = logging.getLogger(isDebug);

	/*
	 * Return the user object immediately. Then reflect cookies
	 * and update the user object. This is so code can proceed
	 * in a best case scenario without deferring all tracking.
	 */
	var user, coData;

	init();

	$.extend(ctx, {
		getUser: function (callback) {
			if (!arguments.length) {
				// optimistic: return user immediately
				// cookie reflection might still happen
				// in the background and update the user
				// object
				return user;
			}
			// pessimistic: return user only after cookie
			// reflection has completed (deemed "pessimistic"
			// because cookies are usually reflected automatically
			// in any transaction and needs to be done only once,
			// so this overload of getUser is here to handle
			// disjunction cases between client and server side code)
			isFunc(callback);
			reflectCookies.always(function () {
				callback(user);
			});
		},
		getCoData: function (callback) {
			isFunc(callback);
			reflectCookies.always(function () {
				callback(coData);
			});
		},
		getFullUser: function (callback) {
			isFunc(callback);
			getUserFromCookies(callback);
		}
	});

	/*
	 * Returns a user object based on client cookies
	 * Cookies:
	 * crs: user is fully authenticated
	 * tt_ut: converted from uidLoggedIn or set with crs; user id is known
	 * uidLoggedIn: passthrough login from an email; user id is known
	 * Params:
	 * ?uid=: will be converted to uidLoggedIn after hitting a reg wall; user id is known
	 * 
	 * The only scenario in which the user is considered logged in is crs
	 */
	function getUser() {
		var user = {};
		var x = cookie.getCookie("crs");
		if (x) {
			logger.log("getUser: crs=%s", x);
			$.extend(user, $.parseJSON(x));
			user.loggedIn = true;
			var fn = user.firstName, ln = user.lastName;
			user.displayName = fn && ln ? (fn + " " + ln)
				.replace(/(\+)/g, " ")
				.replace(/^\s+|\s$/, "") : (user.email || "");
		} else if (x = cookie.getCookie("tt_ut")) {
			logger.log("getUser: tt_ut=%s", x);
			user.uid = x;
		} else if (x = cookie.getCookie("uidLoggedIn")) {
			logger.log("getUser: uidLoggedIn=%s", x);
			var result = /\bdgUserId(\d{1,}\b)/.exec(x);
			if (result) user.uid = result[1];
		} else if (x = url.getParam("uid")) {
			logger.log("getUser: ?uid=%s", x);
			user.uid = x;
		} else {
			logger.log("getUser: No user cookies or uid params found");
		}
		return user;
	}

	function init() {
		user = getUser();
		var co = cookie.getCookie("co");
		// code in activity tracking was setting co to string literal
		// 'undefined' this has been fixed but putting check in to
		// handle any users for which this cookie was set
		coData = co && co !== "undefined" ? $.parseJSON(co) : null;

		logger.log("init", user, coData);
	}

	/*
	 * Use a jQuery promise to hold a queue for cookie reflection;
	 * The promise is needed because all methods exposed by this
	 * module must check for the existence of cookies and reflect
	 * them from the master domain (techtarget.com) if they are
	 * not found. This queue returns immediately if the user is
	 * already on the master domain. Cookie reflection should only
	 * happen once on a page.
	 */
	var reflectCookies = $.Deferred(function (defer) {
		// check master domain
		if (ctx.tld != "techtarget.com") {
			// check if reflection is needed
			var needs = needCookies();
			if (needs) {
				$.ajax({
					url: ctx.regHost + "/registration/json/common/GetCookiesWithCallback",
					data: "fetchCookie=" + needs,
					contentType: "application/json",
					dataType: "jsonp",
					timeout: 1000,
					success: defer.resolve,
					error: defer.reject
				});
			} else {
				// cookies already reflected
				defer.rejectWith(null, [null, "no-op", "cookies reflected"]);
			}
		} else {
			// already on master domain
			defer.rejectWith(null, [null, "ineligible", "techtarget.com"]);
		}

	}).promise().then(function (data, textStatus) {
		logger.log("reflectCookies: %s", textStatus, data);
		var expires;
		$.each(data.results, function(i, it) {
			var name = it.name;
			if (!expires) {
				expires = new Date();
				expires.setTime(expires.getTime() + 7776000000); // 90 days
			}
			var val = decodeURIComponent(it.value);
			logger.log("reflecting '%s' cookie", name, val);
			cookie.setDomainCookie(name, val, expires.toGMTString());
		});
		return true;

	}, function (jqXHR, textStatus, errorThrown) {
		logger.log("reflectCookies %s: %s", textStatus, errorThrown);
		return false;

	}).done(init); // if successful then re-init objects

	/*
	 * Retrieves the full user from the reg service. Uses a jQuery
	 * promise to ensure the ajax call is made only once on a page.
	 * Cookie reflection is not needed prior to this call because
	 * the request happens on the master domain.
	 */
	var getUserFromCookies = function (callback) {

		logger.log("Getting user from cookies");

		var promise = $.Deferred(function (defer) {
			$.ajax({
				url: ctx.regHost + "/registration/rest/RegistrationService/GetUserFromCookies",
				contentType: "application/json",
				dataType: "jsonp",
				timeout: 1000,
				success: defer.resolve,
				error: defer.reject
			});

		}).promise().then(function (data, textStatus) {
			logger.log("getUserFromCookies: %s", textStatus, data);
			if (data.RegTx[0].status == "SUCCESS") {
				return data.RegTx[0].user;
			}
			return null;

		}, function (jqXHR, textStatus, errorThrown) {
			logger.log("getUserFromCookies %s: %s", textStatus, errorThrown);
			return null;
		});

		getUserFromCookies = function (callback) {
			promise.always(callback);
		};

		getUserFromCookies(callback);
	};

	function isFunc(fn) {
		if (typeof fn != "function") throw new TypeError("incorrect arguments. Expecting: function");
	}

	function needCookies() {
		var s = "";
		$.each(["crs", "tt_ut", "co"], function (i, val) {
			if (s.length) s += ",";
			if (!cookie.hasCookie(val)) s += val;
		});
		return s;
	}

});
// Sets a cookie that tracks the last page a user visited
// This cookie was requested to help out with SPOKE tracking
TT(["context", "util/cookie", "util/debug"], function (ctx, cookie, debug) {

	var isDebug = debug.check("debug_lastvisited");

	if ((ctx.is404 || ctx.envName != "prod" || ctx.isPreview) && !isDebug) {
		// exit if page is 404 or not prod delivery unless debugging
		return;
	}

	var cookieName = "lastVisited",
		cookieValue = window.location.href,
		expires = new Date();

	expires.setTime(expires.getTime() + 2592000000); // 30 days

	cookie.setDomainCookie(cookieName, cookieValue, expires.toGMTString());

});
/*
 * Language bundle de
 * Returns a function that will replace tokens within a string.
 * The string may contain embedded params, e.g. {0}, which will
 * be replaced in the resolved value from additional arguments.
 * E.g.
 * var tokens = {
 *     greeting: "You're logged in as {0}!",
 *     logout: "Logout"
 * };
 * var i18n = require("i18n/en");
 * i18n.say("greeting", "Sam I am") == "You're logged in as Sam I am!"
 * i18n.say("logout") == "Logout"
 */
TT("i18n/de", function () {

	// list of all foreign language tokens
	var tokens = {
		"ask_question.ask_a_question": "Stellen Sie eine Frage",
		"ask_question.thank_you_message": "Vielen Dank f\u00fcr Ihre Frage.",
		"commenting.a_techtarget_account_already_exists": "Es existiert bereits ein TechTarget-Account mit dieser E-Mail-Adresse. Bitte melden Sie sich an.",
		"commenting.comment": "Kommentar",
		"commenting.comments": "Kommentare",
		"commenting.comment_lowercase": "Kommentar",
		"commenting.comments_lowercase": "Kommentare",
		"commenting.please_enter_a_comment": "Bitte geben Sie einen Kommentar ab.",
		"commenting.please_enter_a_password": "Bitte geben Sie ein Passwort ein, welches 1-30 Zeichen lang ist.",
		"commenting.please_enter_a_username": "Bitte geben Sie einen Benutzernamen ein, der 6-30 Zeichen lang ist und nur Buchstaben und Ziffern enth\u00e4lt.",
		"commenting.please_enter_a_valid_email": "Bitte geben Sie eine g\u00fcltige E-Mail-Adresse ein. Zum Beispiel  <b>John@domain.com</b>",
		"commenting.there_was_an_error_processing_your_information": "Unsere Moderatoren \u00fcberpr\u00fcfen Ihren Beitrag. Sobald er freigegeben ist, wird er automatisch ver\u00f6ffentlicht. Sollte Ihr Text nicht innerhalb von 24 Stunden sichtbar sein, versuchen Sie es bitte erneut.",
		"commenting.this_username_is_already_taken": "Dieser Benutzername ist bereits vergeben. Bitte w\u00e4hlen Sie einen anderen.",
		"commenting.to_add_a_comment": "Bitte erstellen Sie einen Benutzernamen, um einen Kommentar abzugeben.",
		"commenting.usernames_must_be": "Benutzernamen m\u00fcssen zwischen 6-30 Zeichen umfassen und d\u00fcrfen nur Buchstaben und Ziffern enthalten.",
		"commenting.we_could_not_locate_your_account": "Ihr Account konnte nicht gefunden werden. Noch kein Mitglied? Bitte registrieren.",
		"commenting.your_password_is_incorrect": "Ihr Passwort wurde falsch eingegeben. {0}Passwort vergessen?</a>",
		"commenting.create_username_and_add_my_comment": "Usernamen erstellen und Kommentar hinzuf&#252;gen",
		"commenting.add_my_comment": "Kommentar hinzuf&#252;gen",
		"commenting.share_your_comment": "Kommentar abgeben",
		"commenting.cancel":"Abbrechen",
		"commenting.reply": "Antworten",
		"commenting.replies": "antworten",

		"contactUsForm.email_subject": "Techtarget User has sent feedback",
		"contactUsForm.please_enter_a_emailaddress": "Bitte geben Sie Ihre Email-Adresse ein",
		"contactUsForm.please_select_a_contact_option": "W\u00e4hlen Sie eine Kontaktoption",
		"contactUsForm.please_enter_feedback": "Bitte geben Sie Ihr Feedback ein",
		"date.shortMonths": "Jan.,Febr.,M\u00e4rz,Apr.,Mai,Juni,Juli,Aug.,Sept.,Okt.,Nov.,Dez.",
		"logout": "Logout",
		"navbar.search_default_text": "TechTarget-Netzwerk durchsuchen",
		"search.SPONSORED": "Gesponsert",
		"search.altdefinition": "Definition",
		"search.answer": "Antwort",
		"search.answer_content": "Antworten",
		"search.article_content": "@search.article_content@",
		"search.associated_glossaries": "Verwandte Glossarbegriffe",
		"search.awards": "Auszeichnungen",
		"search.blogpost_content": "@search.blogpost_content@",
		"search.buyers_guide": "Einkaufsf\u00fchrer",
		"search.by_contributors": "Von&nbsp;",
		"search.by_user": "Von {0}",
		"search.cartoon": "Cartoon",
		"search.conference_coverage": "Berichterstattung der Konferenz",
		"search.continue_reading": "Weiterlesen",
		"search.definition": "Definitionen",
		"search.e-book": "E-BOOK",
		"search.e-chapter": "@search.e-chapter@",
		"search.e-handbook": "E-HANDBOOK",
		"search.e-zine": "E-ZINE",
		"search.e-zine_title": "E-ZINE",
		"search.ehandbook_content": "E-handbooks",
		"search.essential_guide": "Essential Guide",
		"search.evaluate_infotype": "Bewertung",
		"search.feature": "Feature",
		"search.get_started_infotype": "Erste Schritte",
		"search.in_depth_content": "Features",
		"search.magazine": "Magazin",
		"search.manage_infotype": "Verwaltung",
		"search.misc": "Sonstiges",
		"search.news": "News",
		"search.news_content": "News",
		"search.news_infotype": "Nachrichten",
		"search.opinion": "Meinungen",
		"search.opinion_content": "Meinungen",
		"search.photostory": "Fotoreportagen",
		"search.photostory_content": "Fotoreportagen",
		"search.photostoryslide": "Fotoreportagen",
		"search.photostoryslide_content": "Fotoreportagen",
		"search.podcast": "@search.podcast@",
		"search.podcast_content": "Podcast",
		"search.problem_solve_infotype": "Probleml&ouml;sung",
		"search.quiz": "Quiz",
		"search.read_full_definition": "Ganze Definition lesen",
		"search.reference": "Referenzen",
		"search.report": "Report",
		"search.report_content": "@search.report_content@",
		"search.review": "Bewertung",
		"search.security_school": "@search.security_school@",
		"search.survey": "Umfrage",
		"search.tip": "Tipp",
		"search.tip_content": "Tipps",
		"search.tutorial": "Ratgeber",
		"search.tutorial_content": "Ratgeber",
		"search.video": "Video",
		"search.video_content": "Video",
		"toggle_box.show_more": "+ Mehr anzeigen",
		"toggle_box.show_less": "- Weniger anzeigen"
	};

	var hasOwn = Object.prototype.hasOwnProperty;
	var slice = Array.prototype.slice;
	var re = /\{(\d{1,})\}/g;

	return {
		say: function (token) {
			if (token && hasOwn.call(tokens, token)) {
				var params = slice.call(arguments, 1);
				return tokens[token].replace(re, function (m, $1) {
					return params[$1];
				});
			}
			return "???" + token + "???";
		}
	};

});
/*
 * Language bundle en
 * Returns a function that will replace tokens within a string.
 * The string may contain embedded params, e.g. {0}, which will
 * be replaced in the resolved value from additional arguments.
 * E.g.
 * var tokens = {
 *     greeting: "You're logged in as {0}!",
 *     logout: "Logout"
 * };
 * var i18n = require("i18n/en");
 * i18n.say("greeting", "Sam I am") == "You're logged in as Sam I am!"
 * i18n.say("logout") == "Logout"
 */
TT("i18n/en", function () {

	// list of all foreign language tokens
	var tokens = {
		"ask_question.ask_a_question": "Ask a question",
		"ask_question.thank_you_message": "Thank you for your question.",
		"commenting.a_techtarget_account_already_exists": "A TechTarget account already exists with that email address. Please login.",
		"commenting.comment": "Comment",
		"commenting.comments": "Comments",
		"commenting.comment_lowercase": "comment",
		"commenting.comments_lowercase": "comments",
		"commenting.please_enter_a_comment": "Please enter a Comment.",
		"commenting.please_enter_a_password": "Please enter a password between 1-30 characters long.",
		"commenting.please_enter_a_username": "Please enter a username that is 6-30 characters long and contains only letters and numbers.",
		"commenting.please_enter_a_valid_email": "Please enter a valid email address. For example <b>John@domain.com</b>",
		"commenting.there_was_an_error_processing_your_information": "Our moderators are reviewing your submission; if approved, it will appear shortly. If you do not see it posted within 24 hours, please try again.",
		"commenting.this_username_is_already_taken": "This username is already taken. Please try another.",
		"commenting.to_add_a_comment": "To add a comment, please create a username.",
		"commenting.usernames_must_be": "Usernames must be 6-30 characters long and contain only letters and numbers.",
		"commenting.we_could_not_locate_your_account": "We could not locate your account. Not a member? Please register.",
		"commenting.your_password_is_incorrect": "Your password is incorrect. {0}Forgot Password?</a>",
		"commenting.create_username_and_add_my_comment": "Create Username and Add My Comment",
		"commenting.add_my_comment": "Add My Comment",
		"commenting.share_your_comment": "Share your comment",
		"commenting.cancel":"Cancel",
		"commenting.reply": "Reply",
		"commenting.replies": "replies",
		
		"contactUsForm.email_subject": "Techtarget User has sent feedback",
		"contactUsForm.please_enter_a_emailaddress": "Please enter an Email Address",
		"contactUsForm.please_select_a_contact_option": "Please select a contact option",
		"contactUsForm.please_enter_feedback": "Please enter feedback",
		"date.shortMonths": "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec",
		"logout": "Logout",
		"navbar.search_default_text": "Search the TechTarget Network",
		"search.SPONSORED": "Sponsored",
		"search.altdefinition": "Definition",
		"search.answer": "Answer",
		"search.answer_content": "@search.answer_content@",
		"search.article_content": "Article",
		"search.associated_glossaries": "Associated Glossaries",
		"search.awards": "Awards",
		"search.blogpost_content": "Blog Post",
		"search.buyers_guide": "Buyer's Guide",
		"search.by_contributors": "By&nbsp;",
		"search.by_user": "By {0}",
		"search.cartoon": "Cartoon",
		"search.conference_coverage": "Conference Coverage",
		"search.continue_reading": "Continue Reading",
		"search.definition": "Definition",
		"search.e-book": "E-Book",
		"search.e-chapter": "E-Book",
		"search.e-handbook": "E-Handbook",
		"search.e-zine": "E-Zine",
		"search.e-zine_title": "E-Zine Title",
		"search.ehandbook_content": "E-handbooks",
		"search.essential_guide": "Essential Guide",
		"search.evaluate_infotype": "Evaluate",
		"search.feature": "Feature",
		"search.get_started_infotype": "Get Started",
		"search.in_depth_content": "In Depth",
		"search.magazine": "Magazine",
		"search.manage_infotype": "Manage",
		"search.misc": "Misc",
		"search.news": "News",
		"search.news_content": "News",
		"search.news_infotype": "News",
		"search.opinion": "Opinion",
		"search.opinion_content": "Opinion",
		"search.photostory": "Photo Story",
		"search.photostory_content": "Photo Story",
		"search.photostoryslide": "Photo Story",
		"search.photostoryslide_content": "Photo Story",
		"search.podcast": "Podcast",
		"search.podcast_content": "Podcast",
		"search.problem_solve_infotype": "Problem Solve",
		"search.quiz": "Quiz",
		"search.read_full_definition": "Read Full Definition",
		"search.reference": "Reference",
		"search.report": "Report",
		"search.report_content": "Report",
		"search.review": "Review",
		"search.security_school": "Security School",
		"search.survey": "Survey",
		"search.tip": "Tip",
		"search.tip_content": "Tips",
		"search.tutorial": "Tutorial",
		"search.tutorial_content": "@search.tutorial_content@",
		"search.video": "Video",
		"search.video_content": "Video",
		"toggle_box.show_more": "+ Show More",
		"toggle_box.show_less": "- Show Less"
	};

	var hasOwn = Object.prototype.hasOwnProperty;
	var slice = Array.prototype.slice;
	var re = /\{(\d{1,})\}/g;

	return {
		say: function (token) {
			if (token && hasOwn.call(tokens, token)) {
				var params = slice.call(arguments, 1);
				return tokens[token].replace(re, function (m, $1) {
					return params[$1];
				});
			}
			return "???" + token + "???";
		}
	};

});
/*
 * Language bundle es
 * Returns a function that will replace tokens within a string.
 * The string may contain embedded params, e.g. {0}, which will
 * be replaced in the resolved value from additional arguments.
 * E.g.
 * var tokens = {
 *     greeting: "You're logged in as {0}!",
 *     logout: "Logout"
 * };
 * var i18n = require("i18n/en");
 * i18n.say("greeting", "Sam I am") == "You're logged in as Sam I am!"
 * i18n.say("logout") == "Logout"
 */
TT("i18n/es", function () {

	// list of all foreign language tokens
	var tokens = {
		"ask_question.ask_a_question": "Haz una pregunta",
		"ask_question.thank_you_message": "Gracias por su pregunta.",
		"commenting.a_techtarget_account_already_exists": "Ya existe una cuenta de TechTarget con esa direcci\u00f3n de correo. Por favor inicie sesi\u00f3n.",
		"commenting.comment": "Comentario",
		"commenting.comments": "Comentarios",
		"commenting.comment_lowercase": "comentario",
		"commenting.comments_lowercase": "comentarios",
		"commenting.please_enter_a_comment": "Por favor, escriba un comentario.",
		"commenting.please_enter_a_password": "Por favor, escriba una contrase\u00f1a de 1-30 caracteres de longitud.",
		"commenting.please_enter_a_username": "Por favor, escriba un nombre de usuario que tenga entre 6-30 caracteres y contenga solo letras y n\u00fameros.",
		"commenting.please_enter_a_valid_email": "Por favor, escriba una direcci\u00f3n v\u00e1lida de correo electr\u00f3nico. Por ejemplo <b>Juan@domain.com</b>",
		"commenting.there_was_an_error_processing_your_information": "Nuestros moderadores est\u00e1n revisando su comentario; de ser aprobado, aparecer\u00e1 en breve. Si no lo ve publicado en 24 horas, vuelva a intentar.",
		"commenting.this_username_is_already_taken": "Este nombre de usuario ya est\u00e1 en uso. Por favor, trate con otro.",
		"commenting.to_add_a_comment": "Para escribir un comentario, por favor cree un nombre de usuario.",
		"commenting.usernames_must_be": "Los nombres de usuario deben ser de 6-30 caracteres de largo y contener solo letras y n\u00fameros.",
		"commenting.we_could_not_locate_your_account": "No podemos localizar su cuenta. \u00bfNo es miembro? Por favor reg\u00edstrese.",
		"commenting.your_password_is_incorrect": "Su contrase\u00f1a es incorrecta. {0}\u00bfOlvid\u00f3 su contrase\u00f1a?</a>",
		"commenting.create_username_and_add_my_comment": "Crear un Nombre de usuario y Agregar mi comentario",
		"commenting.add_my_comment": "Agregar mi comentario",
		"commenting.share_your_comment": "Comparta su comentario",
		"commenting.cancel":"Cancelar",
		"commenting.reply": "Responder",
		"commenting.replies": "respuestas",
		
		"contactUsForm.email_subject": "Techtarget User has sent feedback",
		"contactUsForm.please_enter_a_emailaddress": "Por favor, introduzca su direcci\u00f3n de correo electr\u00f3nico",
		"contactUsForm.please_select_a_contact_option": "Seleccionar opci\u00f3n de contacto",
		"contactUsForm.please_enter_feedback": "Por favor, introduzca sus sugerencias",
		"date.shortMonths": "ene,feb,mar,abr ,may,jun,jul,ago,sep,oct,nov,dic",
		"logout": "Cerrar sesi\u00f3n",
		"navbar.search_default_text": "Buscar en TechTarget Network",
		"search.SPONSORED": "Patrocinado",
		"search.altdefinition": "Definici\u00f3n",
		"search.answer": "Respuesta",
		"search.answer_content": "@search.answer_content@",
		"search.article_content": "@search.article_content@",
		"search.associated_glossaries": "Glosarios asociados",
		"search.awards": "Premios",
		"search.blogpost_content": "@search.blogpost_content@",
		"search.buyers_guide": "Gu\u00eda del comprador",
		"search.by_contributors": "Por&nbsp;",
		"search.by_user": "Por {0}",
		"search.cartoon": "Caricatura",
		"search.conference_coverage": "Cobertura de conferencia",
		"search.continue_reading": "Contin\u00fae Leyendo",
		"search.definition": "Definicion",
		"search.e-book": "Libros electr\u00f3nicos",
		"search.e-chapter": "Libros electr\u00f3nicos",
		"search.e-handbook": "Documentos electr\u00f3nicos",
		"search.e-zine": "Revista electr\u00f3nica",
		"search.e-zine_title": "Revista electr\u00f3nica",
		"search.ehandbook_content": "@search.ehandbook_content@",
		"search.essential_guide": "Gu\u00eda Esencial",
		"search.evaluate_infotype": "Evaluar",
		"search.feature": "Cr\u00f3nica / Reportaje",
		"search.get_started_infotype": "Lo b\u00e1sico",
		"search.in_depth_content": "A profundidad",
		"search.magazine": "Revista",
		"search.manage_infotype": "Gestionar",
		"search.misc": "Miscel\u00e1neo",
		"search.news": "Noticias",
		"search.news_content": "Noticias",
		"search.news_infotype": "Noticias",
		"search.opinion": "Opinion",
		"search.opinion_content": "Opinion",
		"search.photostory": "Fotohistoria",
		"search.photostory_content": "Fotohistoria",
		"search.photostoryslide": "Fotohistoria",
		"search.photostoryslide_content": "Fotohistoria",
		"search.podcast": "Podcast",
		"search.podcast_content": "Podcast",
		"search.problem_solve_infotype": "Resolver Problemas",
		"search.quiz": "Cuestionario",
		"search.read_full_definition": "Leer Definici\u00f3n Completa",
		"search.reference": "Referencia",
		"search.report": "Reporte",
		"search.report_content": "@search.report_content@",
		"search.review": "Revisi\u00f3n",
		"search.security_school": "Escuela de Seguridad",
		"search.survey": "Encuesta",
		"search.tip": "Consejo",
		"search.tip_content": "@search.tip_content@",
		"search.tutorial": "Tutorial",
		"search.tutorial_content": "@search.tutorial_content@",
		"search.video": "Video",
		"search.video_content": "Video",
		"toggle_box.show_more": "+ Mostrar M\u00e1s",
		"toggle_box.show_less": "- Mostrar Menos"
	};

	var hasOwn = Object.prototype.hasOwnProperty;
	var slice = Array.prototype.slice;
	var re = /\{(\d{1,})\}/g;

	return {
		say: function (token) {
			if (token && hasOwn.call(tokens, token)) {
				var params = slice.call(arguments, 1);
				return tokens[token].replace(re, function (m, $1) {
					return params[$1];
				});
			}
			return "???" + token + "???";
		}
	};

});
/*
 * Language bundle fr
 * Returns a function that will replace tokens within a string.
 * The string may contain embedded params, e.g. {0}, which will
 * be replaced in the resolved value from additional arguments.
 * E.g.
 * var tokens = {
 *     greeting: "You're logged in as {0}!",
 *     logout: "Logout"
 * };
 * var i18n = require("i18n/en");
 * i18n.say("greeting", "Sam I am") == "You're logged in as Sam I am!"
 * i18n.say("logout") == "Logout"
 */
TT("i18n/fr", function () {

	// list of all foreign language tokens
	var tokens = {
		"ask_question.ask_a_question": "Posez une question",
		"ask_question.thank_you_message": "Merci pour votre question.",
		"commenting.a_techtarget_account_already_exists": "Un compte TechTarget est d\u00e9j\u00e0 associ\u00e9 \u00e0 cette adresse e-mail. Merci de vous identifier.",
		"commenting.comment": "Commentaire",
		"commenting.comments": "Commentaires",
		"commenting.comment_lowercase": "commentaire",
		"commenting.comments_lowercase": "commentaires",
		"commenting.please_enter_a_comment": "Votre commentaire",
		"commenting.please_enter_a_password": "Entrez un mot de passe de 1 \u00e0 30 caract\u00e8res de long",
		"commenting.please_enter_a_username": " Entrez un identifiant de 6 \u00e0 30 caract\u00e8res de long et qui ne contient que des lettres et des chiffres.",
		"commenting.please_enter_a_valid_email": "Entrez une adresse e-mail valide. Par exemple : <b>John@domain.com</b>",
		"commenting.there_was_an_error_processing_your_information": "Votre commentaire sera publi\u00e9 d\u00e8s qu\u2019il aura \u00e9t\u00e9 valid\u00e9 par nos mod\u00e9rateurs. S\u2019il n\u2019appara\u00eet pas dans les 24 heures, merci de r\u00e9essayer.",
		"commenting.this_username_is_already_taken": "Cet identifiant a d\u00e9j\u00e0 \u00e9t\u00e9 pris. Merci d\u2019en essayer un autre.",
		"commenting.to_add_a_comment": "Pour ajouter un commentaire, merci de choisir un identifiant.",
		"commenting.usernames_must_be": "Les identifiants doivent \u00eatre de 6 \u00e0 30 caract\u00e8res de long et ne contenir que des lettres et des chiffres",
		"commenting.we_could_not_locate_your_account": "Nous n\u2019avons pas reconnu votre compte. Nouveau membre? S\u2019inscrire.",
		"commenting.your_password_is_incorrect": "Votre mot de passe est incorrect. {0}Mot de passe oubli\u00e9 ? </a>",
		"commenting.create_username_and_add_my_comment": "Cr&#233;er un identifiant et poster mon commentaire",
		"commenting.add_my_comment": "Publier mon commentaire",
		"commenting.share_your_comment": "Votre commentaire ici",
		"commenting.cancel":"Annuler ",
		"commenting.reply": "R&#232;pondre",
		"commenting.replies": "r&#232;ponses",
		
		"contactUsForm.email_subject": "Techtarget User has sent feedback",
		"contactUsForm.please_enter_a_emailaddress": "S'il vous pla\u00eet entrez votre adresse e-mail",
		"contactUsForm.please_select_a_contact_option": "Choisissez un objet ",
		"contactUsForm.please_enter_feedback": "S'il vous pla\u00eet saisissez votre avis",
		"date.shortMonths": "janv.,f\u00e9vr.,mars,avr.,mai,juin,juil.,ao\u00fbt,sept.,oct.,nov.,d\u00e9c.",
		"logout": "D\u00e9connexion",
		"navbar.search_default_text": "Rechercher sur LeMagIT",
		"search.SPONSORED": "Sponsoris\u00e9",
		"search.altdefinition": "D\u00e9finition",
		"search.answer": "R\u00e9pondre",
		"search.answer_content": "@search.answer_content@",
		"search.article_content": "@search.article_content@",
		"search.associated_glossaries": "Associated Glossaries",
		"search.awards": "Remises de prix",
		"search.blogpost_content": "@search.blogpost_content@",
		"search.buyers_guide": "Guide d'achat",
		"search.by_contributors": "Par&nbsp;",
		"search.by_user": "Par {0}",
		"search.cartoon": "Dessin",
		"search.conference_coverage": "Couverture d'\u00e9v\u00e9nements",
		"search.continue_reading": "Lire davantage",
		"search.definition": "D\u00e9finition",
		"search.e-book": "E-book",
		"search.e-chapter": "E-book",
		"search.e-handbook": "E-handbook",
		"search.e-zine": "E-zine",
		"search.e-zine_title": "E-zine",
		"search.ehandbook_content": "E-handbooks",
		"search.essential_guide": "Les Guides Essentiels",
		"search.evaluate_infotype": "\u00c9valuer",
		"search.feature": "Caract\u00e9ristique",
		"search.get_started_infotype": "D\u00e9marrer",
		"search.in_depth_content": "Approfondir",
		"search.magazine": "Magazine",
		"search.manage_infotype": "G\u00e9rer",
		"search.misc": "Divers",
		"search.news": "Actualit\u00e9s",
		"search.news_content": "Actualit\u00e9s",
		"search.news_infotype": "Actualit\u00e9s",
		"search.opinion": "Opinions",
		"search.opinion_content": "Opinion",
		"search.photostory": "@search.photostory@",
		"search.photostory_content": "@search.photostory_content@",
		"search.photostoryslide": "@search.photostoryslide@",
		"search.photostoryslide_content": "@search.photostoryslide_content@",
		"search.podcast": "Podcast",
		"search.podcast_content": "Podcast",
		"search.problem_solve_infotype": "R\u00e9solution de probl\u00e8me",
		"search.quiz": "Quiz",
		"search.read_full_definition": "Lire la d\u00e9finition compl\u00e8te",
		"search.reference": "R\u00e9f\u00e9rence",
		"search.report": "Signaler",
		"search.report_content": "Projets IT",
		"search.review": "Revoir",
		"search.security_school": "@search.security_school@",
		"search.survey": "Sondage",
		"search.tip": "Conseil",
		"search.tip_content": "Conseils IT",
		"search.tutorial": "Tutoriel",
		"search.tutorial_content": "@search.tutorial_content@",
		"search.video": "Vid\u00e9o",
		"search.video_content": "Vid\u00e9o",
		"toggle_box.show_more": "+ Voir plus",
		"toggle_box.show_less": "- Voir moins"
	};

	var hasOwn = Object.prototype.hasOwnProperty;
	var slice = Array.prototype.slice;
	var re = /\{(\d{1,})\}/g;

	return {
		say: function (token) {
			if (token && hasOwn.call(tokens, token)) {
				var params = slice.call(arguments, 1);
				return tokens[token].replace(re, function (m, $1) {
					return params[$1];
				});
			}
			return "???" + token + "???";
		}
	};

});
/*
 * Language bundle it
 * Returns a function that will replace tokens within a string.
 * The string may contain embedded params, e.g. {0}, which will
 * be replaced in the resolved value from additional arguments.
 * E.g.
 * var tokens = {
 *     greeting: "You're logged in as {0}!",
 *     logout: "Logout"
 * };
 * var i18n = require("i18n/en");
 * i18n.say("greeting", "Sam I am") == "You're logged in as Sam I am!"
 * i18n.say("logout") == "Logout"
 */
TT("i18n/it", function () {

	// list of all foreign language tokens
	var tokens = {
		"ask_question.ask_a_question": "@ask_question.ask_a_question@",
		"ask_question.thank_you_message": "@ask_question.thank_you_message@",
		"commenting.a_techtarget_account_already_exists": "@commenting.a_techtarget_account_already_exists@",
		"commenting.comment": "@commenting.comment@",
		"commenting.comments": "@commenting.comments@",
		"commenting.comment_lowercase": "@commenting.comment_lowercase@",
		"commenting.comments_lowercase": "@commenting.comments_lowercase@",
		"commenting.please_enter_a_comment": "@commenting.please_enter_a_comment@",
		"commenting.please_enter_a_password": "@commenting.please_enter_a_password@",
		"commenting.please_enter_a_username": "@commenting.please_enter_a_username@",
		"commenting.please_enter_a_valid_email": "@commenting.please_enter_a_valid_email@",
		"commenting.there_was_an_error_processing_your_information": "@commenting.there_was_an_error_processing_your_information@",
		"commenting.this_username_is_already_taken": "@commenting.this_username_is_already_taken@",
		"commenting.to_add_a_comment": "@commenting.to_add_a_comment@",
		"commenting.usernames_must_be": "@commenting.usernames_must_be@",
		"commenting.we_could_not_locate_your_account": "@commenting.we_could_not_locate_your_account@",
		"commenting.your_password_is_incorrect": "@commenting.your_password_is_incorrect@",
		"commenting.create_username_and_add_my_comment": "@commenting.create_username_and_add_my_comment@",
		"commenting.add_my_comment": "@commenting.add_my_comment@",
		"commenting.share_your_comment": "@commenting.share_your_comment@",
		"commenting.cancel":"@commenting.cancel@",
		"commenting.reply": "@commenting.reply@",
		"commenting.replies": "@commenting.replies@",
		"contactUsForm.email_text": "@contactUsForm.email_text@",
		"contactUsForm.email_subject": "@contactUsForm.email_subject@",
		"contactUsForm.please_enter_a_emailaddress": "@contactUsForm.please_enter_a_emailaddress@",
		"contactUsForm.please_select_a_contact_option": "@contactUsForm.please_select_a_contact_option@",
		"contactUsForm.please_enter_feedback": "@contactUsForm.please_enter_feedback@",
		"date.shortMonths": "@date.shortMonths@",
		"logout": "@logout@",
		"navbar.search_default_text": "@navbar.search_default_text@",
		"search.SPONSORED": "@search.SPONSORED@",
		"search.altdefinition": "@search.altdefinition@",
		"search.answer": "@search.answer@",
		"search.answer_content": "@search.answer_content@",
		"search.article_content": "@search.article_content@",
		"search.associated_glossaries": "@search.associated_glossaries@",
		"search.awards": "@search.awards@",
		"search.blogpost_content": "@search.blogpost_content@",
		"search.buyers_guide": "@search.buyers_guide@",
		"search.by_contributors": "@search.by_contributors@",
		"search.by_user": "@search.by_user@",
		"search.cartoon": "@search.cartoon@",
		"search.conference_coverage": "@search.conference_coverage@",
		"search.continue_reading": "@search.continue_reading@",
		"search.definition": "@search.definition@",
		"search.e-book": "@search.e-book@",
		"search.e-chapter": "@search.e-chapter@",
		"search.e-handbook": "@search.e-handbook@",
		"search.e-zine": "@search.e-zine@",
		"search.e-zine_title": "@search.e-zine_title@",
		"search.ehandbook_content": "@search.ehandbook_content@",
		"search.essential_guide": "@search.essential_guide@",
		"search.evaluate_infotype": "@search.evaluate_infotype@",
		"search.feature": "@search.feature@",
		"search.get_started_infotype": "@search.get_started_infotype@",
		"search.in_depth_content": "@search.in_depth_content@",
		"search.magazine": "@search.magazine@",
		"search.manage_infotype": "@search.manage_infotype@",
		"search.misc": "@search.misc@",
		"search.news": "@search.news@",
		"search.news_content": "@search.news_content@",
		"search.news_infotype": "@search.news_infotype@",
		"search.opinion": "@search.opinion@",
		"search.opinion_content": "@search.opinion_content@",
		"search.photostory": "@search.photostory@",
		"search.photostory_content": "@search.photostory_content@",
		"search.photostoryslide": "@search.photostoryslide@",
		"search.photostoryslide_content": "@search.photostoryslide_content@",
		"search.podcast": "@search.podcast@",
		"search.podcast_content": "@search.podcast_content@",
		"search.problem_solve_infotype": "@search.problem_solve_infotype@",
		"search.quiz": "@search.quiz@",
		"search.read_full_definition": "@search.read_full_definition@",
		"search.reference": "@search.reference@",
		"search.report": "@search.report@",
		"search.report_content": "@search.report_content@",
		"search.review": "@search.review@",
		"search.security_school": "@search.security_school@",
		"search.survey": "@search.survey@",
		"search.tip": "@search.tip@",
		"search.tip_content": "@search.tip_content@",
		"search.tutorial": "@search.tutorial@",
		"search.tutorial_content": "@search.tutorial_content@",
		"search.video": "@search.video@",
		"search.video_content": "@search.video_content@",
		"toggle_box.show_more": "@toggle_box.show_more@",
		"toggle_box.show_less": "@toggle_box.show_less@"
	};

	var hasOwn = Object.prototype.hasOwnProperty;
	var slice = Array.prototype.slice;
	var re = /\{(\d{1,})\}/g;

	return {
		say: function (token) {
			if (token && hasOwn.call(tokens, token)) {
				var params = slice.call(arguments, 1);
				return tokens[token].replace(re, function (m, $1) {
					return params[$1];
				});
			}
			return "???" + token + "???";
		}
	};

});
/*
 * Ads module (Google Publisher Tags)
 * Requires the TT GPT API library as a global javascript resource
 */
TT("ads", ["lib/gpt", "context", "context/ui", "util/url", "util/cookie", "util/logging", "lib/jquery"],
	function (GPT, ctx, ui, url, cookie, logging, $) {

	var isDebug = !!GPT.debug;
	var logger = logging.getLogger(isDebug);

	var hasOwn = Object.prototype.hasOwnProperty;

	/*
	 * Translate incoming context variables to their legacy names. Names like
	 * "gci" and "tax" go all the way back to V5 and are no longer present
	 * in the new Vignette architecture as such.
	 */
	var cfg = {
		acct: ctx.adAbbr,
		zone: ctx.zone,
		gci: !ctx.isChannel && ctx.adId,
		tax: ctx.topicId,
		defaultTax: ctx.adId,
		defaultTaxGuide: ctx.guideId,
		kw:  ctx.kw,
		clu: ctx.clusterId,
		adg: ctx.adg,
		iid: ctx.iid,
		rci: ctx.rci,
		microsite: ctx.microsite ? ctx.siteName : "",
		layout: ui.layoutName,
		viewport: ui.viewport.w,
		site: ctx.siteName.toLowerCase()
	};

	// Slot callback queue
	var called = [];
	var uncalled = [];

	// Layouts
	var desktop = ui.isDesktop(),
		tabletH = ui.isTabletH(),
		tablets = tabletH || ui.isTablet(),
		mobile  = ui.isTablet() || ui.isMobile(),
		mobilex = ui.isMobile(); // rare case: something @ 320 bp only

	// Facets for ad types
	var types = {
		// interstitial
		ist: function () {
			this.eligible = (desktop || tablets) && !ctx.membersOnly && !ctx.is404 && url.getParam("int") != "off";
			this.targeting = {type: "oop"};
		},
		// leaderboard
		lb: function () {
			this.eligible = !mobile && this.location == "header";
			this.sizes = [728, 90];
		},
		// mobile banner
		mb: function () {
			var header = this.location == "header";
			this.eligible = mobile && (header || /^content-body-\d/.test(this.location));
			this.sizes = [300, 50];
			this.targeting = {pos: header ? "top" : "bottom"};
		},
		// halfpage
		hp: function () {
			var contentHeader = this.location == "content-header",
				contentBody1 = this.location == "content-body-1";
			this.sizes = [[300, 600], [300, 251]];
			switch (this.scheme) {
				case 1:
					this.eligible = (desktop || tabletH) && contentBody1;
					this.targeting = {pos: "bottom"};
					break;
				case 7:
					this.eligible = desktop && contentHeader;
					this.targeting = {pos: "bottom"};
					break;
				case 9:
					this.eligible = desktop && contentBody1;
					this.targeting = {pos: "top"};
					break;
				case 10:
					this.eligible = desktop && (this.location == "content-body-2");
					this.targeting = {pos: "bottom"};
					break;
				case 11:
					this.eligible = (desktop || tabletH) && contentHeader;
					this.targeting = {pos: "top"};
					break;
				case 12:
					this.eligible = desktop && (this.location == "content-body-2");
					this.targeting = {pos: "bottom"};
					break;
				case 17:
					this.eligible = (desktop || tabletH) && contentHeader;
					this.targeting = {pos: "top"};
					break;
				default:
					this.eligible = desktop && contentHeader;
					this.targeting = {pos: "top"};
					break;
			}
		},
		// messaging unit
		mu: function () {
			this.eligible = false; // uses switch statement
			this.sizes = [300, 250];
			switch (this.scheme) {
				case 1:
					switch (this.location) {
						case "content-header":
							this.eligible = !mobile;
							this.targeting = {pos: "top"};
							break;
						case "content-body-1":
							this.eligible = mobile;
							this.targeting = {pos: "top"};
							break;
					}
					break;
				case 2:
					switch (this.location) {
						case "content-header":
							this.eligible = !desktop && !mobile; // tablet_h only
							this.targeting = {pos: "top"};
							break;
						case "content-body-1":
							this.eligible = true;
							this.targeting = {pos: mobile ? "top" : "bottom"};
							break;
					}
					break;
				case 3:
					switch (this.location) {
						case "content-body-1":
							this.eligible = !desktop;
							this.targeting = {pos: "top"};
							break;
						case "content-body-2":
							this.eligible = !mobile;
							this.targeting = {pos: "bottom"};
							break;
					}
					break;
				case 4:
					switch (this.location) {
						case "content-header":
							this.eligible = !mobile;
							this.targeting = {pos: "top"};
							break;
						case "content-body-1":
							this.eligible = true;
							this.targeting = {pos: mobile ? "top" : "bottom"};
							break;
					}
					break;
				case 6:
					if (this.location == "content-header") {
						this.eligible = desktop;
						this.targeting = {pos: "top"};
					}
					break;
				case 7:
					switch (this.location) {
						case "content-header":
							this.eligible = desktop;
							this.targeting = {pos: "top"};
							break;
						case "content-body-1":
							this.eligible = !desktop;
							this.targeting = {pos: "top"};
							break;
						case "content-body-2":
							this.eligible = !desktop;
							this.targeting = {pos: "bottom"};
							break;
					}
					break;
				case 8:
					switch (this.location) {
						case "content-header":
							this.eligible = tabletH;
							this.targeting = {pos: "top"};
							break;
						case "content-body-1":
							this.eligible = mobile;
							this.targeting = {pos: "top"};
							break;
						case "content-body-2":
							this.eligible = !mobile;
							this.targeting = {pos: "bottom"};
							break;
					}
					break;
				case 9:
					switch (this.location) {
						case "content-body-1":
							this.eligible = tablets;
							break;
						case "content-body-2":
							this.eligible = mobilex;
							break;
					}
					this.targeting = {pos: "top"};
					break;
				case 10:
					this.eligible = true; // visible all breakpoints
					this.targeting = {pos: "top"};
					break;
				case 11:
					if (this.location == "content-body-1") {
						this.eligible = mobile;
						this.targeting = {pos: "top"};
					}
					break;
				case 12:
					this.eligible = true; // visible all breakpoints
					this.targeting = {pos: "top"};
					break;
				case 13:
					this.eligible = tabletH;
					this.targeting = {pos: "top"};
					break;
				case 14:
					switch (this.location) {
						case "content-body-1":
							this.eligible = !mobile;
							break;
						case "content-body-2":
							this.eligible = mobile;
							break;
					}
					this.targeting = {pos: "top"};
				case 15:
					this.eligible = true; // visible all breakpoints
					this.targeting = {pos: "top"};
					break;
				case 16:
					this.eligible = true; // visible all breakpoints
					this.targeting = {pos: "top"};
					break;
				case 17:
					switch (this.location) {
					case "content-header":
						this.eligible = desktop;
						this.targeting = {pos: "top"};
						break;
					case "content-body-1":
						this.eligible = !desktop;
						this.targeting = {pos: "top"};
						break;
				}
				break;
			}
		},
		// promo
		pr: function () {
			var header = this.location == "header";
			var user = ctx.getUser();
			this.eligible = desktop && !user.loggedIn;
			this.sizes = [800, 45];
			this.targeting = {pos: header ? "top" : "bottom"};
		},
		// marketing button
		mbt: function () {
			var top = this.location == "content-body-1";
			this.eligible = (desktop || tablets);
			this.sizes = [300, 90];
			this.targeting = {pos: top ? "top" : "bottom"};
		}
		// end ads
	};

	// Prototypal scheme: All schemes support these slots
	// Specific schemes are applied by facet
	var scheme = {
		aa: {divId: "interstitial", type: "ist"},
		ab: {divId: "leaderboard", type: "lb", location: "header"},
		ac: {divId: "mobile-lb", type: "mb", location: "header"},
		ad: {divId: "promo-1", type: "pr", location: "header"},
		ae: {divId: "promo-2", type: "pr", location: "footer"}
	};

	// Scheme facets
	var schemes = [
		function () {}, // empty scheme 0
		function () {
			// 1. scheme for Article, Answer, Definition
			this.ba = {divId: "mu-top", type: "mu", location: "content-header"};
			this.bb = {divId: "halfpage", type: "hp", location: "content-body-1"};
			this.bc = {divId: "mu-1", type: "mu", location: "content-body-1"};
			this.bd = {divId: "mobile-2", type: "mb", location: "content-body-2"};
		},
		function () {
			// 2. scheme for Topics
			this.ca = {divId: "halfpage-top", type: "hp", location: "content-header"};
			this.cb = {divId: "mu-top", type: "mu", location: "content-header"};
			this.cc = {divId: "mu-1", type: "mu", location: "content-body-1"};
			this.cd = {divId: "mobile-2", type: "mb", location: "content-body-2"};
		},
		function () {
			// 3. scheme for Search, Tags, InfoTypes, i.e. results listing pages
			this.da = {divId: "halfpage-top", type: "hp", location: "content-header"};
			this.db = {divId: "mu-1", type: "mu", location: "content-body-1"};
			this.dc = {divId: "mu-2", type: "mu", location: "content-body-2"};
			this.dd = {divId: "mobile-2", type: "mb", location: "content-body-3"};
		},
		function () {
			// 4. scheme for Guide parent
			this.ea = {divId: "mu-top", type: "mu", location: "content-header"};
			this.eb = {divId: "mu-1", type: "mu", location: "content-body-1"};
			this.ec = {divId: "mobile-2", type: "mb", location: "content-body-2"};
		},
		function () {
			// 5. scheme for Home
			this.fa = {divId: "mu-1", type: "mu", location: "content-body-1"};
		},
		function () {
			// 6. scheme for Contributor Article Archive
			this.ga = {divId: "mu-top", type: "mu", location: "content-header"};
		},
		function () {
			// 7. scheme for Topic Takeover (InfoCenter)
			this.ha = {divId: "mu-top", type: "mu", location: "content-header"};
			this.hb = {divId: "halfpage-top", type: "hp", location: "content-header"};
			this.hc = {divId: "mu-1", type: "mu", location: "content-body-1"};
			this.hd = {divId: "mu-2", type: "mu", location: "content-body-2"};
		},
		function () {
			// 8. scheme for Photostory listing 
			this.ia = {divId: "halfpage-top", type: "hp", location: "content-header"};
			this.ib = {divId: "mu-top", type: "mu", location: "content-header"};
			this.ic = {divId: "mu-1", type: "mu", location: "content-body-1"};
			this.id = {divId: "mu-2", type: "mu", location: "content-body-2"};
			this.ie = {divId: "mobile-2", type: "mb", location: "content-body-2"};
		},
		function () {
			// 9. scheme for Photostory slide
			this.ja = {divId: "halfpage", type: "hp", location: "content-body-1"};
			this.jb = {divId: "mu-1", type: "mu", location: "content-body-1"};
			this.jc = {divId: "mu-2", type: "mu", location: "content-body-2"};
		},
		function () {
			// 10. scheme for CW home
			this.ka = {divId: "mu-1", type: "mu", location: "content-body-1"};
			this.kb = {divId: "halfpage", type: "hp", location: "content-body-2"};
		},
		function () {
			// 11. scheme for Cartoons listing
			this.la = {divId: "halfpage-top", type: "hp", location: "content-header"};
			this.lb = {divId: "mu-1", type: "mu", location: "content-body-1"};
		},
		function () {
			// 12. scheme for LeMagIT home
			var waitForMove = function () {
				var _ad = this;
				logger.log("  wait for latest news sidebar moved before rendering");
				$('.latest-news-sidebar').on('moved', function () {
					logger.log('  rendering %s', _ad.divId);
					_ad.slot.display();
				});
			};
			this.ma = {divId: "mu-1", type: "mu", location: "content-body-1"};
			this.mb = {divId: "halfpage", type: "hp", location: "content-body-2"};
			this.mc = {divId: "mbt-top", type: "mbt", location: "content-body-1", defer: waitForMove};
			this.md = {divId: "mbt-bottom", type: "mbt", location: "content-body-2", defer: waitForMove};
		},
		function () {
			// 13. scheme for marketing content, events listing
			this.na = {divId: "halfpage-top", type: "hp", location: "content-header"};
			this.nb = {divId: "mu-1", type: "mu", location: "content-body-1"};
			this.nc = {divId: "mobile-2", type: "mb", location: "content-body-2"};
		},
		function () {
			// 14. scheme for BrianMadden home page
			this.oa = {divId: "mu-1", type: "mu", location: "content-body-1"};
			this.ob = {divId: "mu-2", type: "mu", location: "content-body-2"};
		},
		function () {
			// 15. scheme for Microsites Home
			this.pa = {divId: "mu-top", type: "mu", location: "content-header"};
		},
		function () {
			// 16. scheme for Microsites Topics
			this.qa = {divId: "mu-top", type: "mu", location: "content-header"};
		},
		function () {
			// 17. scheme for WhatIs Listing
			this.ra = {divId: "mu-top", type: "mu", location: "content-header"};
			this.rb = {divId: "halfpage-top", type: "hp", location: "content-header"};
			this.rc = {divId: "mu-1", type: "mu", location: "content-body-1"};
		}
	];

	var schemeId = Math.max(parseInt(ctx.adScheme) || 0, 0);
	logger.log("Determined scheme from ctx.adScheme=%d: %d", ctx.adScheme, schemeId);
	schemes[schemeId].call(scheme);
	logger.log("Created ad scheme", scheme);

	// Set the ad scheme on all slots. Index the slot codes by divId.
	for (var p in scheme) {
		scheme[p].scheme = schemeId;
		scheme[scheme[p].divId] = scheme[p];
	}

	// Additional properties to extend onto the ad
	function extras() {
		this.layout = cfg.layout;
		this.zone = cfg.zone;
	}

	// Wrap all calls to GPT in a promise to ensure the DataPoint Media script has
	// been loaded first (to provide dpmSegList)
	var GPT_Deferred = function (fn) {
		// create a deferred queue within the closure
		// redefine the function after the first call
		// so that the script is fetched only once
		logger.log("Loading dpmSegList");
		var deferred = [],
			dpm = $.getScript("http://a.dpmsrv.com/dpmpxl/index.php?q=xSegList&cl=68");
		dpm.done(function () {
			logger.log("dpmSegList loaded: %s", typeof dpmSegList != "undefined" ? dpmSegList : "");
			// note: this loop is intentionally not optimized to allow for a "live" loop
			// in case items are pushed onto the array during this transition
			for (var i = 0; i < deferred.length; i++) {
				GPT(deferred[i]);
			}
			// set GPT_deferred to reference GPT for all future calls
			GPT_Deferred = GPT;
		});
		dpm.fail(function (jqXHR, textStatus, errorThrown) {
			logger.log("Failed to get dpmSegList %s: %s", textStatus, errorThrown);
		});
		GPT_Deferred = function (fn) {
			deferred.push(fn);
		};
		GPT_Deferred(fn);
	};

	// Extend GPT with a custom display function; this ties in with the html on the pages
	GPT.display = function (divId) {
		GPT_Deferred(function () {
			var ad = scheme[divId];
			if (ad) {
				types[ad.type].call(ad); // apply facets
				extras.call(ad); // apply extras
				defineAdSlot.call(this, ad); // create GPT slot
				if (ad.slot) {
					called.push(ad);
					if (!ad.defer) ad.slot.display();
				} else {
					uncalled.push(ad);
				}
			} else {
				logger.log("Invalid ad call for scheme %d: %s", schemeId, divId);
			}
		});
	};

	// Call GPT
	GPT_Deferred(function () {
		// Everything in this handler is deferred until GPT is loaded
		logger.log("Begin Vignette GPT callback");

		// 1) Setup
		logger.log("Processing ad properties:");
		if (url.getParam("utm_medium")) {
			cookie.setDomainCookie("utmMedium", url.getParam("utm_medium"));
		}
		var siteId = cfg.acct || "null";
		var zone = url.getParam("parentZone") || cfg.zone || "null";
		var gci = url.getParam("parentGci") || cfg.gci || "";
		var props = {
			networkId: "3618",
			siteId: siteId,
			zone: zone,
			pth: url.getParam("parentPth") || window.location.pathname.substring(1).replace(/\//g, "."),
			ss: siteId == "CW" ? "cw" : siteId != "swht" && siteId != "null" ? "1" : "",
			kw: url.getParam("parentKw") || cfg.kw || qTransformer(url.getParam("(?:query|q)")) || "",
			gci: gci,
			tax: url.getParam("parentTax") || topicTax(0) ||
				(gci || zone == "TOPICS" || zone == "MICROSITE_TOPICS" || zone == "FORUM" ? cfg.tax :
				/^(ATE|TIPS|MICROSITE_LP)$/.test(zone) ? cfg.defaultTax : ""),
			guide: cfg.defaultTaxGuide || url.getParam("parentDefaultTax") || (zone == "AIOG" && level() > 1 ? cfg.defaultTax : ""),
			clu: url.getParam("parentClu") || topicTax(1) || cfg.clu || "",
			adg: url.getParam("adg") || cfg.adg || "",
			iid: cfg.iid || url.getParam("parentIid") || /^INFOCENTERS?$/.test(zone) ? cfg.defaultTax : "",
			rci: cfg.rci || url.getParam("parentRci") || /^RESOURCECENTERS?$/.test(zone) ? cfg.defaultTax : "",
			mst: cfg.microsite || "", // microsite
			nl: url.getParam("ad"), // newsletter ad id
			ses: url.getParam("ses"), // persistent ad id
			track: url.getParam("track"), // track ad id
			ptag: url.getParam("ptag"), // itkeptag
			tag: url.getParam("tag"), // itketag
			blog: url.getParam("blog"), // itkeblog
			ppc: cookie.getCookie("utmMedium").toUpperCase() == "CPC" ? "1" : "0",
			ui: cookie.getCookie("tt_ut") || cookie.getCookie("ad_ut") || (function () {
				var adUtExpire = new Date();
				adUtExpire.setTime(adUtExpire.getTime() + 157680000000); // 5 years
				var adUt = Math.round(Math.random() * 100000000000);
				cookie.setDomainCookie("ad_ut", adUt, adUtExpire.toGMTString());
				return adUt;
			})(),
			iecmp: /^.+?MSIE 7\.0.+Trident\/[4-6]\.0/.test(window.navigator.userAgent) ? 1 : 0,
			layout: cfg.layout,
			viewport: cfg.viewport,
			aud: typeof dpmSegList != "undefined" ? dpmSegList : "",
			refer: referrerHost(),
			site: cfg.site
		};
		logger.log("  ad properties", props);

		// 2) Unit name
		this.unitName = "/" + [props.networkId, props.siteId, props.zone].join("/");
		logger.log("Setting default unitName: %s", this.unitName);

		// 3) Shared targeting
		logger.log("Setting shared targeting:");
		var pubads = this.googletag.pubads();
		for (var p in props) if (hasOwn.call(props, p) && !/^(networkId|siteId|zone)$/.test(p) && props[p]) {
			pubads.setTargeting(p, String(props[p]));
			logger.log("  pubads.setTargeting(%s, %s)", p, props[p]);
		}

		// 4) Cleanup
		pubads.collapseEmptyDivs();

		// 5) Render event handlers
		pubads.addEventListener("slotRenderEnded", function (evt) {
			logger.log("slot returned", evt);
			var slot = evt.slot,
				ads = called,
				ad;
			for (var i = 0, len = ads.length; i < len; i++) {
				if ((ad = ads[i]).slot.proxyOf === slot) {
					ad.empty = evt.isEmpty;
					ad.returnSize = evt.size;
					ad.returned = true;
					fireEvent(ad);
					break;
				}
			}
		});

		logger.log("End Vignette GPT callback");

		// End GPT callback
	});

	function defineAdSlot(ad) {
		logger.log("defineAdSlot called for divId: %s", ad.divId);
		if (hasOwn.call(ad, "eligible") && !ad.eligible) {
			logger.log("Suppressing %s because of business rules", ad.divId);
			return;
		}
		if (hasOwn.call(ad, "defer") && typeof ad.defer === "function") {
			ad.defer();
		}
		var unitName  = ad.unitName  || this.unitName,
			targeting = ad.targeting || null;
		ad.slot = ad.sizes
				? this.googletag.defineSlot(unitName, ad.sizes, ad.divId)
				: this.googletag.defineOutOfPageSlot(unitName, ad.divId);
		for (var p in targeting) if (hasOwn.call(targeting, p) && targeting[p]) {
			ad.slot.setTargeting(p, String(targeting[p]));
			logger.log("  slot.setTargeting(%s, %s)", p, targeting[p]);
		}
	}

	function qTransformer(q) {
		return q
			.toLowerCase() // lower
			.replace(/^\s+|\s(?=\s)|\s+$/g, "") // trim, reduce spaces
			.replace(/\s(and|or)\s/g, "+$1+") // retain "and" and "or"
			.replace(/\s\&(amp;)?\s|\s/g, "+and+") // replace standalone &, and space with +
		;
	}

	function level() {
		var lvl = window.location.pathname.substring(1).split("/").length;
		if (ctx.isPreview) lvl--; // minus first segment /siteName
		return lvl;
	}

	function topicTax(i) {
		if (~document.referrer.indexOf("informationCenter")) {
			return cookie.getCookie("topicTax").split(":")[i] || ""; // topicTax cookie in the format "tax:clu"
		}
		return "";
	}

	function fireEvent(ad) {
		ui.fireEvent("adLoad", {ad: ad});
	}

	function referrerHost() {
		var m = /^https?:\/\/([^\/]+)\/.*$/.exec(document.referrer);
		return m != null ? m[1] : "";
	}

	// Return the module API
	return {
		called: called,
		uncalled: uncalled,
		isDebug: isDebug
	};

});

TT("gtm", ["context", "context/ui", "util/cookie", "util/logging", "util/debug", "lib/jquery"],
	function (ctx, ui, cookie, logging, debug, $) {

	// 1) activate debugging and set logger
	var isDebug = debug.check("debug_ga"),
		logger = logging.getLogger(isDebug);

	// 2) exit if not enabled or page is not prod delivery unless debugging
	if (!ctx.ga || ((ctx.envName != "prod" || ctx.isPreview) && !isDebug)) {
		if (!ctx.ga) logger.log("GTM is disabled");
		return {
			exit: true
		};
	}

	logger.log("Executing gtm");

	var cfg = {
		sponsored: String(!!ctx.sponsored),
		topical: String(!!ctx.topical),
		microsite: !!ctx.microsite,
		pageType: ((ctx.isChannel) ? 'channel' : 'content'),
		trackType: ((ctx.zone == "SEARCH") ? 'application/internalSearch' : 'page')
	};

	window.dataLayer = window.dataLayer || [];
	dataLayer = window.dataLayer;

	dataLayer.push({
		'trackType': ((ctx.is404) ? 'ERROR 404' : cfg.trackType),
		'parentTopicId': ctx.parentTopicId || '0',
		'parentTopicName': ctx.parentTopicName || 'None',
		'topicId': ctx.topicId || '0',
		'topicName': ctx.topicName || 'None',
		'uaid': ctx.uaid,
		'uid': ctx.adId,
		'siteName': ctx.siteName,
		'uidType': cfg.pageType,
		'publicationDate': ctx.pubDate || 'None',
		'appCode': ctx.appCode || '0',
		'clusterId': ctx.clusterId || '0',
		'clusterName': ctx.clusterName || 'None',
		'regTopicId': ctx.regTopicId || '0',
		'regTopicName': ctx.regTopicName || 'None',
		'zone': ctx.zone || 'None',
		'topical': cfg.topical,
		'pageIndex': ctx.pageIndex || 0,
		'sponsored': cfg.sponsored,
		'dloId': ctx.dloid || '0',
		'dloTitle': ctx.dlotitle || 'None',
		'dloSrc': ctx.dlosrc || 'None',
		'is404': String(!!ctx.is404) || 'false',
		'lang': ctx.lang
	});

	ctx.getCoData(function (coData) {
		var user = ctx.getUser();
		cfg.memberStatus = !!user.loggedIn ? "SITE_MEMBER" : "NOT_MEMBER";
		cfg.demandbaseVar = getDemandbaseVar(coData);

		dataLayer.push({
			'db': cfg.demandbaseVar,
			'memberStatus': cfg.memberStatus
		});
	});

	function getDemandbaseVar(coData) {
		// format: /<IP>/<countryId>/<countryName>/<state>/<industryName>/<ID>/<companyName>/<employeeSize>
		var val;
		if (!coData) {
			val = "NO COOKIE DATA";
		} else {
			val = [
				coData.ipAddress,
				coData.countryId,
				coData.countryName,
				coData.state,
				coData.industry,
				coData.id,
				coData.name,
				coData.empSize
			].join("/").replace(/UNKNOWN/g, "U");
		}
		logger.log("Demandbase custom variable", val);
		return val;
	}

});
/*
 * Activity tracking module
 * @see http://wiki.techtarget.com/pgwiki/index.php?title=ClickTrack_General_API#Tracking_API
 */
TT(["context", "util/cookie", "util/logging", "util/debug", "lib/jquery", "lib/json"],
	function (ctx, cookie, logging, debug, $, json) {

	var hasOwn = Object.prototype.hasOwnProperty;
	var push = Array.prototype.push;
	var join = Array.prototype.join;
	var slice = Array.prototype.slice;

	var user = ctx.getUser();

	// 1) set the user id into a tt_ut cookie if the cookie is missing
	if (!cookie.getCookie("tt_ut") && hasOwn.call(user, "uid")) {
		var ttUtExpire = new Date();
		ttUtExpire.setTime(ttUtExpire.getTime() + 157680000000); // 5 years
		cookie.setDomainCookie("tt_ut", user.uid, ttUtExpire.toGMTString());
		cookie.deleteDomainCookie("ad_ut");
	}

	// 2) activate debugging and set logger
	var isDebug = debug.check("debug_at");
	var logger = logging.getLogger(isDebug);

	// 3) exit if page is 404 or not prod delivery unless debugging
	if ((ctx.is404 || ctx.envName != "prod" || ctx.isPreview) && !isDebug) {
		return;
	}

	var cfg = {
		atHost: (ctx.atHost || "").replace("clicktrack", "go"),
		gci: !ctx.isChannel && ctx.adId,
		tax: ctx.topicId,
		taxes: ctx.topicIds,
		offer: ctx.offer,
		asrc: ctx.asrc,
		uid: user.uid,
		estTimeStr: ctx.timestamp,
		homeAdId: ctx.homeAdId,
		siteType: ctx.siteType,
		siteName: ctx.siteName,
		isMicrosite : (ctx.siteType.indexOf('microsite') != '-1') ? 1 : 0
	};

	// 4) track page activity
	if (cookie.getCookie("moActivity") == "on") {
		// If this cookie is set user has been tracked as part of registration
		// prevents double tracking when user returns from registering
		cookie.setDomainCookie("moActivity", "off");
		logger.log("moActivity cookie shut off");
		
	} else if (cfg.isMicrosite && cfg.uid) {
		logger.log("tracking microsite named activity");
		trackImg(1,cfg.uid,[], cfg.homeAdId, cfg.estTimeStr, cfg.gci, cfg.offer, cfg.asrc, null, cfg.siteName, cfg.isMicrosite);
	} else if (cfg.isMicrosite) {
		logger.log("tracking microsite unnamed activity");
		trackImg(16,cfg.uid,[], cfg.homeAdId, cfg.estTimeStr, cfg.gci, cfg.offer, cfg.asrc, null, cfg.siteName, cfg.isMicrosite);
		
	} else if (cfg.tax && cfg.uid) {
		// Page is a Topic or a content page with primary topic
		// User is known
		logger.log("tracking named activity");
		trackImg(1, cfg.uid, cfg.taxes, cfg.tax, cfg.estTimeStr, cfg.gci, cfg.offer, cfg.asrc);

	} else if (cfg.tax) {
		// Page is a Topic or a content page with primary topic
		// User is not known
		logger.log("tracking unnamed activity");
		trackImg(16, cfg.uid, cfg.taxes, cfg.tax, cfg.estTimeStr, cfg.gci, cfg.offer, cfg.asrc);

		// set a prereg cookie of topic activity that will
		// be read if/when the user finally does register
		setPreRegCookie(cfg.taxes, cfg.tax, cfg.estTimeStr, cfg.gci, cfg.offer, cfg.asrc);

	} else {
		// Page is not topical. User might be known
		logger.log("tracking non-topical activity");

		if (ctx.tld == "techtarget.com") {
			trackImg(22, null, null, null, cfg.estTimeStr);

		} else if (ctx.tld == "computerweekly.com") {
			// TODO: check into change of host to go.computerweekly.com:
			// why is it only being done here and not for other activity?
			// should all non-techtarget sites be using go.<tld>?
			trackImg(22, null, null, null, cfg.estTimeStr);

		} else {
			trackJson();
			logger.log("Activity JSONP requested.");
		}

	}

	// 5) clear out old pre-reg cookie
	if (cfg.uid) {
		// If tt_ut is set means user has registered
		// If user still has a pre-reg cookie report all pre-reg tax values and delete cookie
		var ttPreReg = cookie.getCookie("tt_prereg");
		if (ttPreReg) {
			var history = ttPreReg.split(",");
			var reTax = /t\d{1,}@([^\$]+)\$/g;
			for (var i = 0, j = history.length; i < j; i++) {
				if (history[i]) {
					var parts = history[i].split("_");
					var taxes = [];
					var m;
					while(m = reTax.exec(parts[0])) {taxes.push(m[1])}
					var timestamp = parts[1];
					// timestamp part includes g=, promo=, and asrc= but they will be concatted ok
					// so pass 0 for those 3 parameters
					trackImg(1, cfg.uid, taxes, 0, timestamp, 0, 0, 0, "pre");
				}
			}
			cookie.deleteDomainCookie("tt_prereg");
			logger.log("Pre Reg Tracking Cookie Deleted: history tracked");
		}
	}

	// util functions
	function getTaxesStr(taxes, tax, c) {
		// c = cookie format
		var len = taxes && taxes.length;
		if (len) {
			var s = "";
			// up to 10 are captured by the API
			// @see trackImg
			len = Math.min(len, 10);
			for (var i = 0; i < len; i++) {
				c ?
				// cookie: t1@tax$ t2@tax$ t3@tax$
				s += "t" + (i + 1) + "@" + taxes[i] + "$" :
				// querystring: t= &t2= &t3=
				s += (i > 0 ? "&t" + (i + 1) : "t") + "=" + taxes[i];
			}
			return s;
		} else if (tax) {
			return c ? "t1@" + tax + "$" : "t=" + tax;
		}
	}

	/*
	 * The parameters that are passed need to be set up in the api first:
	 * @see http://wiki.techtarget.com/pgwiki/index.php?title=ClickTrack_General_API#Tracking_a_New_Activity_Type
	 * Current parameters tracked by type 1 & 16:
	 *  a	User activity date
	 *  c	Activity class
	 *  t	Primary topic adId
	 *  t2	Secondary topic adId
	 *  t3	Tertiary topic adId
	 *  ...t10 Tenth topic adId
	 *  g	Content adId
	 *  promo	Promo code
	 *  asrc	Arrival source
	 *  u	User id (available and tracked only on type=1)
	 *
	 * type=22 defines no parameters at all and so all values aside from type should be undefined
	 */
	function trackImg(type, uid, taxes, tax, timestamp, gci, offer, asrc, cparam, siteName, isMicrosite) {
		// ?activityTypeId= &u= &t= t2= t3= &a= &g= &promo= &c= &r= &mname= &msite= &debug= &ip=
		var qs = "?activityTypeId=" + type;
		if (uid) qs += "&u=" + uid;
		var taxes = getTaxesStr(taxes, tax, false);
		if (taxes) qs += "&" + taxes;
		qs += "&a=" + timestamp;
		if (gci) qs += "&g=" + gci;
		if (offer) qs += "&promo=" + offer;
		if (asrc) qs += "&asrc=" + asrc;
		qs += "&c=" + (cparam || "normal");
		if (isMicrosite) {
			qs += "&mname=" + siteName;
			qs += "&msite=1";
		}
		if (isDebug) qs += "&debug=true&ip=173.194.123.8"; //debug mode, send Google IP and turn on debugging
		qs += "&r=" + Math.round(Math.random() * 1000000);
		var url = cfg.atHost + "/clicktrack-r/activity/activity.gif" + encodeURI(qs);
		logger.log("Tracking image will be appended with url: %s", url);
		$(function () {
			$("body").append('<img src="' + url + '" class="track"/>');
			logger.log("Activity tracked: %s", url);
		});
	}

	function trackJson() {
		$.ajax({
			url: cfg.atHost + "/clicktrack-r/activity/activity.jsonp?activityTypeId=22",
			contentType: "application/json",
			dataType: "jsonp",
			timeout: 1000
		}).done(function (data, textStatus) {
			logger.log("trackJson: %s", textStatus, data);
			// if data comes back empty don't set the co cookie
			if (data) {
				var name = "co";
				var val = json.stringify(data);
				var expires = new Date();
				expires.setTime(expires.getTime() + 7776000000); // 90 days
				logger.log("reflecting '%s' cookie", name, val);
				cookie.setDomainCookie(name, val, expires.toGMTString());
			}
		}).fail(function (jqXHR, textStatus, errorThrown) {
			logger.log("trackJson %s: %s", textStatus, errorThrown);
		});
	}

	function setPreRegCookie(taxes, tax, timestamp, gci, offer, asrc) {
		// set cookie value
		var preRegVal = getTaxesStr(taxes, tax, true) + "_" + timestamp;
		if (gci) preRegVal += "&g=" + gci;
		if (offer) preRegVal += "&promo=" + offer;
		if (asrc) preRegVal += "&asrc=" + asrc;

		var preRegExpire = new Date();
		preRegExpire.setTime(preRegExpire.getTime() + 157680000000); // 5 years
		var action = "Created";
		var ttPreReg = cookie.getCookie("tt_prereg");
		if (ttPreReg) {
			action = "Updated";
			// add this page's taxonomy values with the history
			// and trunc the history to the most recent 5
			var history = ttPreReg.split(",");
			push.call(history, preRegVal);
			slice.call(history, -5);
			preRegVal = join.call(history);
		}
		cookie.setDomainCookie("tt_prereg", preRegVal, preRegExpire.toGMTString());
		logger.log("Pre Reg Tracking Cookie %s: %s", action, preRegVal);
	}

});
// Optimizely module (A/B testing)
TT("optimizely", ["util/debug", "util/logging", "context/ui", "lib/jquery"],
	function (debug, logging, ui, $) {

	var isDebug = debug.check("debug_optimizely");
	var logger = logging.getLogger(isDebug);
	var slice = Array.prototype.slice;
	var optly = window['optimizely'] = window['optimizely'] || [];

	if (isDebug) optly.push('log'); // turn on optimizely's logging

	function push() {
		var args = slice.call(arguments);
		if (isDebug) {
			logger.log("Pushing arguments to Optimizely", args);
		}
		return optly.push.apply(optly, args);
	}

	// Track inline reg page1 submission using the hook created for GA
	ui.on('ready', function () {
		$('#inlineRegistration').on('tt.inlinereg.emailOnly.submit', function() {
			push(['trackEvent', 'successfulpage1formsubmission']);
		});
	});

	return {
		push: push
	};

});
// Replacement for PodcastPlayerFactory (PPF) which was used in tt_scripts to load
// a flash audio player in v2 podcast content components. The latest version of
// podcast content component already uses audio tags. There are 698 articles in
// the VCM that have v2 podcast components.
var PPF = {
	newPlayer: function (url) {
		$(".podcastdownload > .left").removeClass("left");
		document.writeln('<audio controls style="width: 320px">');
		document.writeln('<source src="' + url + '" type="audio/mpeg">');
		document.writeln('Your browser does not support the audio tag.');
		document.writeln('</audio>');
		document.writeln('<br>');
	}
};
/*
 * Brightcove video module
 * 
 * Provides a context for a Brightcove video player
 * Note: this code and all of the Brightcove API examples assume there is ever only
 * one player on a page.
 *
 * http://support.brightcove.com/en/video-cloud/docs/player-configuration-parameters
 * http://support.brightcove.com/en/video-cloud/docs/using-smart-player-api
 * http://support.brightcove.com/en/video-cloud/docs/listening-events-using-smart-player-api
 * http://docs.brightcove.com/en/video-cloud/smart-player-api/
 * http://docs.brightcove.com/en/video-cloud/smart-player-api/samples/responsive-sizing.html
 */
TT("brightcove", ["util/logging", "util/debug", "lib/jquery", "lib/handlebars"],
	function (logging, debug, $, handlebars) {

	var isDebug = debug.check("debug_brightcove"),
		logger = logging.getLogger(isDebug);

	var player,
		APIModules,
		mediaEvent,
		videoPlayer,
		cuePoints,
		$object,
		events,
		initted = false,
		isReady = false;

	function init(experienceID) {
		logger.log('Initting: %s', experienceID);
		player = brightcove.api.getExperience(experienceID);
		APIModules = brightcove.api.modules.APIModules;
		mediaEvent = brightcove.api.events.MediaEvent;
		events = {
			begin: mediaEvent.BEGIN,
			change: mediaEvent.CHANGE,
			complete: mediaEvent.COMPLETE,
			error: mediaEvent.ERROR,
			play: mediaEvent.PLAY,
			progress: mediaEvent.PROGRESS,
			seek: mediaEvent.SEEK_NOTIFY,
			stop: mediaEvent.STOP
		}
		initted = true;
	}

	function ready() {
		videoPlayer = player.getModule(APIModules.VIDEO_PLAYER);
		cuePoints = player.getModule(APIModules.CUE_POINTS);
		$object = $('#' + player.id);
		logger.log('Displaying cue points');
		videoPlayer.getCurrentVideo(function (video) {
			cuePoints.getCuePoints(video.id, displayCuePoints);
		});
		isReady = true;
		dequeueWhenReady();
	}

	function displayCuePoints(cuePoints) {
		var $cuePoints = $object.closest('div.video-cuepoints-container')
			.children('div.video-cuepoints')
			.eq(0);
		if (!cuePoints) {
			logger.log('Video has no cue points to display');
			$cuePoints.empty().hide();
		} else {
			logger.log('Cue points', cuePoints);
			$cuePoints
				.html(function () {
					handlebars.registerHelper('fmttime', function (n) {
						var k = parseFloat(n, 10) * 1000,
							d = moment.duration(k),
							h = d.hours(),
							m = d.minutes(),
							s = d.seconds(),
							f = h === 0 ? '' : (h < 10 ? '0' + h : h) + ':',
							f = f + m + ':',
							f = f + (s < 10 ? '0' + s : s);
						return f;
					});
					var src = $('#cuePointTpl').html() || '';
					if (!src) {
						logger.log('No handlebars template script found');
					}
					logger.log('template src: %s', src);
					var template = handlebars.compile(src);
					var out = template($.grep(cuePoints, function (cp) {
						return cp.type === 1;
					}));
					logger.log('template out: %s', out);
					return out;
				})
				// remove extra text nodes that get created
				.find('> ul').addBack().contents().filter(function () {
					return this.nodeType === 3;
				}).remove().end()

				.on('click.cuepoint', 'li', function (evt) {
					$(this)
						.siblings('li')
						.find('> span.left').removeClass('active')
						.find('> i.icon').remove()
						.end()
						.end()
						.end()
						.find('> span.left:not(.active)')
						.addClass('active')
						.prepend('<i class="icon" data-icon="A"></i>')
						.end()
						.each(function () {
							var t = parseFloat($(this).attr('data-cue-time'), 10);
							seekCuePointOnPlay(t);
						});
				});
		}
	}

	function seekCuePointOnPlay(t) {
		// If video is not playing
		// 1. add a self-removing event listener that seeks after the video plays
		// 2. play the video
		videoPlayer.getIsPlaying(function (isPlaying) {
			if (isPlaying) {
				logger.log('Seeking: %d', t);
				videoPlayer.seek(t);
			} else {
				logger.log('Seeking: %d after play', t);
				var handler = function (evt) {
					videoPlayer.removeEventListener(mediaEvent.PROGRESS, handler);
					videoPlayer.seek(t);
				}
				videoPlayer.addEventListener(mediaEvent.PROGRESS, handler);
				videoPlayer.play();
			}
		});
	}

	function onEvent(eventName, handler) {
		if (isReady && events.hasOwnProperty(eventName)) {
			logger.log('Adding listener', arguments);
			videoPlayer.addEventListener(events[eventName], handler);
		}
	}

	function offEvent(eventName, handler) {
		if (isReady && events.hasOwnProperty(eventName)) {
			logger.log('Removing listener', arguments);
			videoPlayer.removeEventListener(events[eventName], handler);
		}
	}

	var readyQueue = [];

	function context(handler) {
		if (isReady) {
			handler.call(context, context);
		} else {
			logger.log('Listening for ready:', handler);
			readyQueue.push(handler);
		}
	}
	
	context.on = onEvent;
	context.off = offEvent;

	function dequeueWhenReady() {
		var fn;
		logger.log('Dequeuing ready handlers');
		while (fn = readyQueue.pop()) {
			fn.call(context, context);
		}
	}

	// Brightcove Learning Services Module
	TT.BCLS = {
		onTemplateLoad: function (experienceID) {
			logger.log('Template loaded for: %s', experienceID);
			init(experienceID);
		},
		onTemplateReady: function (evt) {
			logger.log('Template ready', evt);
			ready();
		}
	};

	return context;

});
// Polyfill for embedded videos that rely on experience_util.js,
// which is no longer included in the head, and do not include
// the script inline.
// This is meant to be a temporary fix until the content can be
// updated.
if (typeof createExperience != "function") {
	window.createExperience = function (cfg) {
		var id = "vid_" + cfg.playerId;
		document.write('<div id="' + id + '"></div>');
		var el = document.getElementById(id),
			iframe = document.createElement("iframe");
		iframe.width  = cfg.width;
		iframe.height = cfg.height;
		iframe.border = 0;
		iframe.frameBorder = 0;
		iframe.scrolling = "no";
		iframe.src = "about:blank"; 
		el.appendChild(iframe);
		var content = '<!DOCTYPE html>'
			+ '<html>'
			+ '<head>'
			+ '<meta charset="utf-8">'
			+ '<title>Dynamic iframe</title>'
			+ '<script src="http://admin.brightcove.com/js/experience_util.js"></scr'+'ipt>'
			+ '</head>'
			+ '<body style="margin:0;padding:0">'
			+ '<div id="' + id + '">'
			+ '<script>'
			+ 'var config = new Array();'
			+ 'config["videoId"] = null;'
			+ 'config["videoRef"] = null;'
			+ 'config["lineupId"] = null;'
			+ 'config["playerTag"] = null;'
			+ 'config["autoStart"] = false;'
			+ 'config["wmode"] = "transparent";'
			+ 'config["preloadBackColor"] = "#FFFFFF";'
			+ 'config["playerId"] = ' + cfg.playerId + ';'
			+ 'config["width"]  = ' + cfg.width +  ';'
			+ 'config["height"] = ' + cfg.height + ';'
			+ 'createExperience(config,8,"alignLeft");'
			+ '</scr'+'ipt>'
			+ '</div>'
			+ '</body></html>';
		iframe.contentWindow.contents = content;
		iframe.src = 'javascript:window["contents"]';
	}
}
// Module to load the Bizcard API on Abstract pages
// Note: the bpr-bizcard.js script needs to be rewritten as a module
// It also includes its own version of Thickbox which should be added
// as a 3rd party library. All of the window. variables are for backwards
// compatibility with the old file.
TT(["context", "lib/jquery"], function (ctx, $) {

	if (!!ctx.bprAbstract) {
		window.ENV_name = ctx.envName.toUpperCase();
		window.bprLanguage = ctx.lang;
		window.bprSiteHost = window.location.hostname;
		window.SITE_MS_ASRC = getAsrc();
	}

	function getAsrc() {
		// TODO: not sure whether needs to account for the alternate prefix
		// SS_MSE_ for embedded, but if so then add conditional using
		// ctx.siteType
		return "SS_MCR_" + ctx.domain;
	}
	
	$(window).load(function () {
		// Show the view now/title button when page fully loads. We don't want the onclick function
		// to be fired before all JS loads/page is ready.
		$("body#bprAbstract a.viewNow").show();
		$("body#bprAbstract div.resTitleLogo a.resTitleLink").show();
	});

});
