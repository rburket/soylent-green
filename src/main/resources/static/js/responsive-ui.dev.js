/*! responsive-ui.dev.js - 05/31/2017 05:03 PM */
/*! jQuery UI - v1.10.3 - 2013-07-15
* http://jqueryui.com
* Includes: jquery.ui.core.js, jquery.ui.widget.js, jquery.ui.position.js, jquery.ui.autocomplete.js, jquery.ui.menu.js
* Copyright 2013 jQuery Foundation and other contributors Licensed MIT */

(function(e,t){function i(t,i){var a,n,r,o=t.nodeName.toLowerCase();return"area"===o?(a=t.parentNode,n=a.name,t.href&&n&&"map"===a.nodeName.toLowerCase()?(r=e("img[usemap=#"+n+"]")[0],!!r&&s(r)):!1):(/input|select|textarea|button|object/.test(o)?!t.disabled:"a"===o?t.href||i:i)&&s(t)}function s(t){return e.expr.filters.visible(t)&&!e(t).parents().addBack().filter(function(){return"hidden"===e.css(this,"visibility")}).length}var a=0,n=/^ui-id-\d+$/;e.ui=e.ui||{},e.extend(e.ui,{version:"1.10.3",keyCode:{BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38}}),e.fn.extend({focus:function(t){return function(i,s){return"number"==typeof i?this.each(function(){var t=this;setTimeout(function(){e(t).focus(),s&&s.call(t)},i)}):t.apply(this,arguments)}}(e.fn.focus),scrollParent:function(){var t;return t=e.ui.ie&&/(static|relative)/.test(this.css("position"))||/absolute/.test(this.css("position"))?this.parents().filter(function(){return/(relative|absolute|fixed)/.test(e.css(this,"position"))&&/(auto|scroll)/.test(e.css(this,"overflow")+e.css(this,"overflow-y")+e.css(this,"overflow-x"))}).eq(0):this.parents().filter(function(){return/(auto|scroll)/.test(e.css(this,"overflow")+e.css(this,"overflow-y")+e.css(this,"overflow-x"))}).eq(0),/fixed/.test(this.css("position"))||!t.length?e(document):t},zIndex:function(i){if(i!==t)return this.css("zIndex",i);if(this.length)for(var s,a,n=e(this[0]);n.length&&n[0]!==document;){if(s=n.css("position"),("absolute"===s||"relative"===s||"fixed"===s)&&(a=parseInt(n.css("zIndex"),10),!isNaN(a)&&0!==a))return a;n=n.parent()}return 0},uniqueId:function(){return this.each(function(){this.id||(this.id="ui-id-"+ ++a)})},removeUniqueId:function(){return this.each(function(){n.test(this.id)&&e(this).removeAttr("id")})}}),e.extend(e.expr[":"],{data:e.expr.createPseudo?e.expr.createPseudo(function(t){return function(i){return!!e.data(i,t)}}):function(t,i,s){return!!e.data(t,s[3])},focusable:function(t){return i(t,!isNaN(e.attr(t,"tabindex")))},tabbable:function(t){var s=e.attr(t,"tabindex"),a=isNaN(s);return(a||s>=0)&&i(t,!a)}}),e("<a>").outerWidth(1).jquery||e.each(["Width","Height"],function(i,s){function a(t,i,s,a){return e.each(n,function(){i-=parseFloat(e.css(t,"padding"+this))||0,s&&(i-=parseFloat(e.css(t,"border"+this+"Width"))||0),a&&(i-=parseFloat(e.css(t,"margin"+this))||0)}),i}var n="Width"===s?["Left","Right"]:["Top","Bottom"],r=s.toLowerCase(),o={innerWidth:e.fn.innerWidth,innerHeight:e.fn.innerHeight,outerWidth:e.fn.outerWidth,outerHeight:e.fn.outerHeight};e.fn["inner"+s]=function(i){return i===t?o["inner"+s].call(this):this.each(function(){e(this).css(r,a(this,i)+"px")})},e.fn["outer"+s]=function(t,i){return"number"!=typeof t?o["outer"+s].call(this,t):this.each(function(){e(this).css(r,a(this,t,!0,i)+"px")})}}),e.fn.addBack||(e.fn.addBack=function(e){return this.add(null==e?this.prevObject:this.prevObject.filter(e))}),e("<a>").data("a-b","a").removeData("a-b").data("a-b")&&(e.fn.removeData=function(t){return function(i){return arguments.length?t.call(this,e.camelCase(i)):t.call(this)}}(e.fn.removeData)),e.ui.ie=!!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()),e.support.selectstart="onselectstart"in document.createElement("div"),e.fn.extend({disableSelection:function(){return this.bind((e.support.selectstart?"selectstart":"mousedown")+".ui-disableSelection",function(e){e.preventDefault()})},enableSelection:function(){return this.unbind(".ui-disableSelection")}}),e.extend(e.ui,{plugin:{add:function(t,i,s){var a,n=e.ui[t].prototype;for(a in s)n.plugins[a]=n.plugins[a]||[],n.plugins[a].push([i,s[a]])},call:function(e,t,i){var s,a=e.plugins[t];if(a&&e.element[0].parentNode&&11!==e.element[0].parentNode.nodeType)for(s=0;a.length>s;s++)e.options[a[s][0]]&&a[s][1].apply(e.element,i)}},hasScroll:function(t,i){if("hidden"===e(t).css("overflow"))return!1;var s=i&&"left"===i?"scrollLeft":"scrollTop",a=!1;return t[s]>0?!0:(t[s]=1,a=t[s]>0,t[s]=0,a)}})})(jQuery);(function(e,t){var i=0,s=Array.prototype.slice,n=e.cleanData;e.cleanData=function(t){for(var i,s=0;null!=(i=t[s]);s++)try{e(i).triggerHandler("remove")}catch(a){}n(t)},e.widget=function(i,s,n){var a,r,o,h,l={},u=i.split(".")[0];i=i.split(".")[1],a=u+"-"+i,n||(n=s,s=e.Widget),e.expr[":"][a.toLowerCase()]=function(t){return!!e.data(t,a)},e[u]=e[u]||{},r=e[u][i],o=e[u][i]=function(e,i){return this._createWidget?(arguments.length&&this._createWidget(e,i),t):new o(e,i)},e.extend(o,r,{version:n.version,_proto:e.extend({},n),_childConstructors:[]}),h=new s,h.options=e.widget.extend({},h.options),e.each(n,function(i,n){return e.isFunction(n)?(l[i]=function(){var e=function(){return s.prototype[i].apply(this,arguments)},t=function(e){return s.prototype[i].apply(this,e)};return function(){var i,s=this._super,a=this._superApply;return this._super=e,this._superApply=t,i=n.apply(this,arguments),this._super=s,this._superApply=a,i}}(),t):(l[i]=n,t)}),o.prototype=e.widget.extend(h,{widgetEventPrefix:r?h.widgetEventPrefix:i},l,{constructor:o,namespace:u,widgetName:i,widgetFullName:a}),r?(e.each(r._childConstructors,function(t,i){var s=i.prototype;e.widget(s.namespace+"."+s.widgetName,o,i._proto)}),delete r._childConstructors):s._childConstructors.push(o),e.widget.bridge(i,o)},e.widget.extend=function(i){for(var n,a,r=s.call(arguments,1),o=0,h=r.length;h>o;o++)for(n in r[o])a=r[o][n],r[o].hasOwnProperty(n)&&a!==t&&(i[n]=e.isPlainObject(a)?e.isPlainObject(i[n])?e.widget.extend({},i[n],a):e.widget.extend({},a):a);return i},e.widget.bridge=function(i,n){var a=n.prototype.widgetFullName||i;e.fn[i]=function(r){var o="string"==typeof r,h=s.call(arguments,1),l=this;return r=!o&&h.length?e.widget.extend.apply(null,[r].concat(h)):r,o?this.each(function(){var s,n=e.data(this,a);return n?e.isFunction(n[r])&&"_"!==r.charAt(0)?(s=n[r].apply(n,h),s!==n&&s!==t?(l=s&&s.jquery?l.pushStack(s.get()):s,!1):t):e.error("no such method '"+r+"' for "+i+" widget instance"):e.error("cannot call methods on "+i+" prior to initialization; "+"attempted to call method '"+r+"'")}):this.each(function(){var t=e.data(this,a);t?t.option(r||{})._init():e.data(this,a,new n(r,this))}),l}},e.Widget=function(){},e.Widget._childConstructors=[],e.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",defaultElement:"<div>",options:{disabled:!1,create:null},_createWidget:function(t,s){s=e(s||this.defaultElement||this)[0],this.element=e(s),this.uuid=i++,this.eventNamespace="."+this.widgetName+this.uuid,this.options=e.widget.extend({},this.options,this._getCreateOptions(),t),this.bindings=e(),this.hoverable=e(),this.focusable=e(),s!==this&&(e.data(s,this.widgetFullName,this),this._on(!0,this.element,{remove:function(e){e.target===s&&this.destroy()}}),this.document=e(s.style?s.ownerDocument:s.document||s),this.window=e(this.document[0].defaultView||this.document[0].parentWindow)),this._create(),this._trigger("create",null,this._getCreateEventData()),this._init()},_getCreateOptions:e.noop,_getCreateEventData:e.noop,_create:e.noop,_init:e.noop,destroy:function(){this._destroy(),this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(e.camelCase(this.widgetFullName)),this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName+"-disabled "+"ui-state-disabled"),this.bindings.unbind(this.eventNamespace),this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus")},_destroy:e.noop,widget:function(){return this.element},option:function(i,s){var n,a,r,o=i;if(0===arguments.length)return e.widget.extend({},this.options);if("string"==typeof i)if(o={},n=i.split("."),i=n.shift(),n.length){for(a=o[i]=e.widget.extend({},this.options[i]),r=0;n.length-1>r;r++)a[n[r]]=a[n[r]]||{},a=a[n[r]];if(i=n.pop(),s===t)return a[i]===t?null:a[i];a[i]=s}else{if(s===t)return this.options[i]===t?null:this.options[i];o[i]=s}return this._setOptions(o),this},_setOptions:function(e){var t;for(t in e)this._setOption(t,e[t]);return this},_setOption:function(e,t){return this.options[e]=t,"disabled"===e&&(this.widget().toggleClass(this.widgetFullName+"-disabled ui-state-disabled",!!t).attr("aria-disabled",t),this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus")),this},enable:function(){return this._setOption("disabled",!1)},disable:function(){return this._setOption("disabled",!0)},_on:function(i,s,n){var a,r=this;"boolean"!=typeof i&&(n=s,s=i,i=!1),n?(s=a=e(s),this.bindings=this.bindings.add(s)):(n=s,s=this.element,a=this.widget()),e.each(n,function(n,o){function h(){return i||r.options.disabled!==!0&&!e(this).hasClass("ui-state-disabled")?("string"==typeof o?r[o]:o).apply(r,arguments):t}"string"!=typeof o&&(h.guid=o.guid=o.guid||h.guid||e.guid++);var l=n.match(/^(\w+)\s*(.*)$/),u=l[1]+r.eventNamespace,c=l[2];c?a.delegate(c,u,h):s.bind(u,h)})},_off:function(e,t){t=(t||"").split(" ").join(this.eventNamespace+" ")+this.eventNamespace,e.unbind(t).undelegate(t)},_delay:function(e,t){function i(){return("string"==typeof e?s[e]:e).apply(s,arguments)}var s=this;return setTimeout(i,t||0)},_hoverable:function(t){this.hoverable=this.hoverable.add(t),this._on(t,{mouseenter:function(t){e(t.currentTarget).addClass("ui-state-hover")},mouseleave:function(t){e(t.currentTarget).removeClass("ui-state-hover")}})},_focusable:function(t){this.focusable=this.focusable.add(t),this._on(t,{focusin:function(t){e(t.currentTarget).addClass("ui-state-focus")},focusout:function(t){e(t.currentTarget).removeClass("ui-state-focus")}})},_trigger:function(t,i,s){var n,a,r=this.options[t];if(s=s||{},i=e.Event(i),i.type=(t===this.widgetEventPrefix?t:this.widgetEventPrefix+t).toLowerCase(),i.target=this.element[0],a=i.originalEvent)for(n in a)n in i||(i[n]=a[n]);return this.element.trigger(i,s),!(e.isFunction(r)&&r.apply(this.element[0],[i].concat(s))===!1||i.isDefaultPrevented())}},e.each({show:"fadeIn",hide:"fadeOut"},function(t,i){e.Widget.prototype["_"+t]=function(s,n,a){"string"==typeof n&&(n={effect:n});var r,o=n?n===!0||"number"==typeof n?i:n.effect||i:t;n=n||{},"number"==typeof n&&(n={duration:n}),r=!e.isEmptyObject(n),n.complete=a,n.delay&&s.delay(n.delay),r&&e.effects&&e.effects.effect[o]?s[t](n):o!==t&&s[o]?s[o](n.duration,n.easing,a):s.queue(function(i){e(this)[t](),a&&a.call(s[0]),i()})}})})(jQuery);(function(t,e){function i(t,e,i){return[parseFloat(t[0])*(p.test(t[0])?e/100:1),parseFloat(t[1])*(p.test(t[1])?i/100:1)]}function s(e,i){return parseInt(t.css(e,i),10)||0}function n(e){var i=e[0];return 9===i.nodeType?{width:e.width(),height:e.height(),offset:{top:0,left:0}}:t.isWindow(i)?{width:e.width(),height:e.height(),offset:{top:e.scrollTop(),left:e.scrollLeft()}}:i.preventDefault?{width:0,height:0,offset:{top:i.pageY,left:i.pageX}}:{width:e.outerWidth(),height:e.outerHeight(),offset:e.offset()}}t.ui=t.ui||{};var a,o=Math.max,r=Math.abs,h=Math.round,l=/left|center|right/,c=/top|center|bottom/,u=/[\+\-]\d+(\.[\d]+)?%?/,d=/^\w+/,p=/%$/,f=t.fn.position;t.position={scrollbarWidth:function(){if(a!==e)return a;var i,s,n=t("<div style='display:block;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),o=n.children()[0];return t("body").append(n),i=o.offsetWidth,n.css("overflow","scroll"),s=o.offsetWidth,i===s&&(s=n[0].clientWidth),n.remove(),a=i-s},getScrollInfo:function(e){var i=e.isWindow?"":e.element.css("overflow-x"),s=e.isWindow?"":e.element.css("overflow-y"),n="scroll"===i||"auto"===i&&e.width<e.element[0].scrollWidth,a="scroll"===s||"auto"===s&&e.height<e.element[0].scrollHeight;return{width:a?t.position.scrollbarWidth():0,height:n?t.position.scrollbarWidth():0}},getWithinInfo:function(e){var i=t(e||window),s=t.isWindow(i[0]);return{element:i,isWindow:s,offset:i.offset()||{left:0,top:0},scrollLeft:i.scrollLeft(),scrollTop:i.scrollTop(),width:s?i.width():i.outerWidth(),height:s?i.height():i.outerHeight()}}},t.fn.position=function(e){if(!e||!e.of)return f.apply(this,arguments);e=t.extend({},e);var a,p,m,g,v,b,_=t(e.of),y=t.position.getWithinInfo(e.within),w=t.position.getScrollInfo(y),x=(e.collision||"flip").split(" "),k={};return b=n(_),_[0].preventDefault&&(e.at="left top"),p=b.width,m=b.height,g=b.offset,v=t.extend({},g),t.each(["my","at"],function(){var t,i,s=(e[this]||"").split(" ");1===s.length&&(s=l.test(s[0])?s.concat(["center"]):c.test(s[0])?["center"].concat(s):["center","center"]),s[0]=l.test(s[0])?s[0]:"center",s[1]=c.test(s[1])?s[1]:"center",t=u.exec(s[0]),i=u.exec(s[1]),k[this]=[t?t[0]:0,i?i[0]:0],e[this]=[d.exec(s[0])[0],d.exec(s[1])[0]]}),1===x.length&&(x[1]=x[0]),"right"===e.at[0]?v.left+=p:"center"===e.at[0]&&(v.left+=p/2),"bottom"===e.at[1]?v.top+=m:"center"===e.at[1]&&(v.top+=m/2),a=i(k.at,p,m),v.left+=a[0],v.top+=a[1],this.each(function(){var n,l,c=t(this),u=c.outerWidth(),d=c.outerHeight(),f=s(this,"marginLeft"),b=s(this,"marginTop"),D=u+f+s(this,"marginRight")+w.width,T=d+b+s(this,"marginBottom")+w.height,C=t.extend({},v),M=i(k.my,c.outerWidth(),c.outerHeight());"right"===e.my[0]?C.left-=u:"center"===e.my[0]&&(C.left-=u/2),"bottom"===e.my[1]?C.top-=d:"center"===e.my[1]&&(C.top-=d/2),C.left+=M[0],C.top+=M[1],t.support.offsetFractions||(C.left=h(C.left),C.top=h(C.top)),n={marginLeft:f,marginTop:b},t.each(["left","top"],function(i,s){t.ui.position[x[i]]&&t.ui.position[x[i]][s](C,{targetWidth:p,targetHeight:m,elemWidth:u,elemHeight:d,collisionPosition:n,collisionWidth:D,collisionHeight:T,offset:[a[0]+M[0],a[1]+M[1]],my:e.my,at:e.at,within:y,elem:c})}),e.using&&(l=function(t){var i=g.left-C.left,s=i+p-u,n=g.top-C.top,a=n+m-d,h={target:{element:_,left:g.left,top:g.top,width:p,height:m},element:{element:c,left:C.left,top:C.top,width:u,height:d},horizontal:0>s?"left":i>0?"right":"center",vertical:0>a?"top":n>0?"bottom":"middle"};u>p&&p>r(i+s)&&(h.horizontal="center"),d>m&&m>r(n+a)&&(h.vertical="middle"),h.important=o(r(i),r(s))>o(r(n),r(a))?"horizontal":"vertical",e.using.call(this,t,h)}),c.offset(t.extend(C,{using:l}))})},t.ui.position={fit:{left:function(t,e){var i,s=e.within,n=s.isWindow?s.scrollLeft:s.offset.left,a=s.width,r=t.left-e.collisionPosition.marginLeft,h=n-r,l=r+e.collisionWidth-a-n;e.collisionWidth>a?h>0&&0>=l?(i=t.left+h+e.collisionWidth-a-n,t.left+=h-i):t.left=l>0&&0>=h?n:h>l?n+a-e.collisionWidth:n:h>0?t.left+=h:l>0?t.left-=l:t.left=o(t.left-r,t.left)},top:function(t,e){var i,s=e.within,n=s.isWindow?s.scrollTop:s.offset.top,a=e.within.height,r=t.top-e.collisionPosition.marginTop,h=n-r,l=r+e.collisionHeight-a-n;e.collisionHeight>a?h>0&&0>=l?(i=t.top+h+e.collisionHeight-a-n,t.top+=h-i):t.top=l>0&&0>=h?n:h>l?n+a-e.collisionHeight:n:h>0?t.top+=h:l>0?t.top-=l:t.top=o(t.top-r,t.top)}},flip:{left:function(t,e){var i,s,n=e.within,a=n.offset.left+n.scrollLeft,o=n.width,h=n.isWindow?n.scrollLeft:n.offset.left,l=t.left-e.collisionPosition.marginLeft,c=l-h,u=l+e.collisionWidth-o-h,d="left"===e.my[0]?-e.elemWidth:"right"===e.my[0]?e.elemWidth:0,p="left"===e.at[0]?e.targetWidth:"right"===e.at[0]?-e.targetWidth:0,f=-2*e.offset[0];0>c?(i=t.left+d+p+f+e.collisionWidth-o-a,(0>i||r(c)>i)&&(t.left+=d+p+f)):u>0&&(s=t.left-e.collisionPosition.marginLeft+d+p+f-h,(s>0||u>r(s))&&(t.left+=d+p+f))},top:function(t,e){var i,s,n=e.within,a=n.offset.top+n.scrollTop,o=n.height,h=n.isWindow?n.scrollTop:n.offset.top,l=t.top-e.collisionPosition.marginTop,c=l-h,u=l+e.collisionHeight-o-h,d="top"===e.my[1],p=d?-e.elemHeight:"bottom"===e.my[1]?e.elemHeight:0,f="top"===e.at[1]?e.targetHeight:"bottom"===e.at[1]?-e.targetHeight:0,m=-2*e.offset[1];0>c?(s=t.top+p+f+m+e.collisionHeight-o-a,t.top+p+f+m>c&&(0>s||r(c)>s)&&(t.top+=p+f+m)):u>0&&(i=t.top-e.collisionPosition.marginTop+p+f+m-h,t.top+p+f+m>u&&(i>0||u>r(i))&&(t.top+=p+f+m))}},flipfit:{left:function(){t.ui.position.flip.left.apply(this,arguments),t.ui.position.fit.left.apply(this,arguments)},top:function(){t.ui.position.flip.top.apply(this,arguments),t.ui.position.fit.top.apply(this,arguments)}}},function(){var e,i,s,n,a,o=document.getElementsByTagName("body")[0],r=document.createElement("div");e=document.createElement(o?"div":"body"),s={visibility:"hidden",width:0,height:0,border:0,margin:0,background:"none"},o&&t.extend(s,{position:"absolute",left:"-1000px",top:"-1000px"});for(a in s)e.style[a]=s[a];e.appendChild(r),i=o||document.documentElement,i.insertBefore(e,i.firstChild),r.style.cssText="position: absolute; left: 10.7432222px;",n=t(r).offset().left,t.support.offsetFractions=n>10&&11>n,e.innerHTML="",i.removeChild(e)}()})(jQuery);(function(t){var e=0;t.widget("ui.autocomplete",{version:"1.10.3",defaultElement:"<input>",options:{appendTo:null,autoFocus:!1,delay:300,minLength:1,position:{my:"left top",at:"left bottom",collision:"none"},source:null,change:null,close:null,focus:null,open:null,response:null,search:null,select:null},pending:0,_create:function(){var e,i,s,n=this.element[0].nodeName.toLowerCase(),a="textarea"===n,o="input"===n;this.isMultiLine=a?!0:o?!1:this.element.prop("isContentEditable"),this.valueMethod=this.element[a||o?"val":"text"],this.isNewMenu=!0,this.element.addClass("ui-autocomplete-input").attr("autocomplete","off"),this._on(this.element,{keydown:function(n){if(this.element.prop("readOnly"))return e=!0,s=!0,i=!0,undefined;e=!1,s=!1,i=!1;var a=t.ui.keyCode;switch(n.keyCode){case a.PAGE_UP:e=!0,this._move("previousPage",n);break;case a.PAGE_DOWN:e=!0,this._move("nextPage",n);break;case a.UP:e=!0,this._keyEvent("previous",n);break;case a.DOWN:e=!0,this._keyEvent("next",n);break;case a.ENTER:case a.NUMPAD_ENTER:this.menu.active&&(e=!0,n.preventDefault(),this.menu.select(n));break;case a.TAB:this.menu.active&&this.menu.select(n);break;case a.ESCAPE:this.menu.element.is(":visible")&&(this._value(this.term),this.close(n),n.preventDefault());break;default:i=!0,this._searchTimeout(n)}},keypress:function(s){if(e)return e=!1,(!this.isMultiLine||this.menu.element.is(":visible"))&&s.preventDefault(),undefined;if(!i){var n=t.ui.keyCode;switch(s.keyCode){case n.PAGE_UP:this._move("previousPage",s);break;case n.PAGE_DOWN:this._move("nextPage",s);break;case n.UP:this._keyEvent("previous",s);break;case n.DOWN:this._keyEvent("next",s)}}},input:function(t){return s?(s=!1,t.preventDefault(),undefined):(this._searchTimeout(t),undefined)},focus:function(){this.selectedItem=null,this.previous=this._value()},blur:function(t){return this.cancelBlur?(delete this.cancelBlur,undefined):(clearTimeout(this.searching),this.close(t),this._change(t),undefined)}}),this._initSource(),this.menu=t("<ul>").addClass("ui-autocomplete ui-front").appendTo(this._appendTo()).menu({role:null}).hide().data("ui-menu"),this._on(this.menu.element,{mousedown:function(e){e.preventDefault(),this.cancelBlur=!0,this._delay(function(){delete this.cancelBlur});var i=this.menu.element[0];t(e.target).closest(".ui-menu-item").length||this._delay(function(){var e=this;this.document.one("mousedown",function(s){s.target===e.element[0]||s.target===i||t.contains(i,s.target)||e.close()})})},menufocus:function(e,i){if(this.isNewMenu&&(this.isNewMenu=!1,e.originalEvent&&/^mouse/.test(e.originalEvent.type)))return this.menu.blur(),this.document.one("mousemove",function(){t(e.target).trigger(e.originalEvent)}),undefined;var s=i.item.data("ui-autocomplete-item");!1!==this._trigger("focus",e,{item:s})?e.originalEvent&&/^key/.test(e.originalEvent.type)&&this._value(s.value):this.liveRegion.text(s.value)},menuselect:function(t,e){var i=e.item.data("ui-autocomplete-item"),s=this.previous;this.element[0]!==this.document[0].activeElement&&(this.element.focus(),this.previous=s,this._delay(function(){this.previous=s,this.selectedItem=i})),!1!==this._trigger("select",t,{item:i})&&this._value(i.value),this.term=this._value(),this.close(t),this.selectedItem=i}}),this.liveRegion=t("<span>",{role:"status","aria-live":"polite"}).addClass("ui-helper-hidden-accessible").insertBefore(this.element),this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")}})},_destroy:function(){clearTimeout(this.searching),this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete"),this.menu.element.remove(),this.liveRegion.remove()},_setOption:function(t,e){this._super(t,e),"source"===t&&this._initSource(),"appendTo"===t&&this.menu.element.appendTo(this._appendTo()),"disabled"===t&&e&&this.xhr&&this.xhr.abort()},_appendTo:function(){var e=this.options.appendTo;return e&&(e=e.jquery||e.nodeType?t(e):this.document.find(e).eq(0)),e||(e=this.element.closest(".ui-front")),e.length||(e=this.document[0].body),e},_initSource:function(){var e,i,s=this;t.isArray(this.options.source)?(e=this.options.source,this.source=function(i,s){s(t.ui.autocomplete.filter(e,i.term))}):"string"==typeof this.options.source?(i=this.options.source,this.source=function(e,n){s.xhr&&s.xhr.abort(),s.xhr=t.ajax({url:i,data:e,dataType:"json",success:function(t){n(t)},error:function(){n([])}})}):this.source=this.options.source},_searchTimeout:function(t){clearTimeout(this.searching),this.searching=this._delay(function(){this.term!==this._value()&&(this.selectedItem=null,this.search(null,t))},this.options.delay)},search:function(t,e){return t=null!=t?t:this._value(),this.term=this._value(),t.length<this.options.minLength?this.close(e):this._trigger("search",e)!==!1?this._search(t):undefined},_search:function(t){this.pending++,this.element.addClass("ui-autocomplete-loading"),this.cancelSearch=!1,this.source({term:t},this._response())},_response:function(){var t=this,i=++e;return function(s){i===e&&t.__response(s),t.pending--,t.pending||t.element.removeClass("ui-autocomplete-loading")}},__response:function(t){t&&(t=this._normalize(t)),this._trigger("response",null,{content:t}),!this.options.disabled&&t&&t.length&&!this.cancelSearch?(this._suggest(t),this._trigger("open")):this._close()},close:function(t){this.cancelSearch=!0,this._close(t)},_close:function(t){this.menu.element.is(":visible")&&(this.menu.element.hide(),this.menu.blur(),this.isNewMenu=!0,this._trigger("close",t))},_change:function(t){this.previous!==this._value()&&this._trigger("change",t,{item:this.selectedItem})},_normalize:function(e){return e.length&&e[0].label&&e[0].value?e:t.map(e,function(e){return"string"==typeof e?{label:e,value:e}:t.extend({label:e.label||e.value,value:e.value||e.label},e)})},_suggest:function(e){var i=this.menu.element.empty();this._renderMenu(i,e),this.isNewMenu=!0,this.menu.refresh(),i.show(),this._resizeMenu(),i.position(t.extend({of:this.element},this.options.position)),this.options.autoFocus&&this.menu.next()},_resizeMenu:function(){var t=this.menu.element;t.outerWidth(Math.max(t.width("").outerWidth()+1,this.element.outerWidth()))},_renderMenu:function(e,i){var s=this;t.each(i,function(t,i){s._renderItemData(e,i)})},_renderItemData:function(t,e){return this._renderItem(t,e).data("ui-autocomplete-item",e)},_renderItem:function(e,i){return t("<li>").append(t("<a>").text(i.label)).appendTo(e)},_move:function(t,e){return this.menu.element.is(":visible")?this.menu.isFirstItem()&&/^previous/.test(t)||this.menu.isLastItem()&&/^next/.test(t)?(this._value(this.term),this.menu.blur(),undefined):(this.menu[t](e),undefined):(this.search(null,e),undefined)},widget:function(){return this.menu.element},_value:function(){return this.valueMethod.apply(this.element,arguments)},_keyEvent:function(t,e){(!this.isMultiLine||this.menu.element.is(":visible"))&&(this._move(t,e),e.preventDefault())}}),t.extend(t.ui.autocomplete,{escapeRegex:function(t){return t.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")},filter:function(e,i){var s=RegExp(t.ui.autocomplete.escapeRegex(i),"i");return t.grep(e,function(t){return s.test(t.label||t.value||t)})}}),t.widget("ui.autocomplete",t.ui.autocomplete,{options:{messages:{noResults:"No search results.",results:function(t){return t+(t>1?" results are":" result is")+" available, use up and down arrow keys to navigate."}}},__response:function(t){var e;this._superApply(arguments),this.options.disabled||this.cancelSearch||(e=t&&t.length?this.options.messages.results(t.length):this.options.messages.noResults,this.liveRegion.text(e))}})})(jQuery);(function(t){t.widget("ui.menu",{version:"1.10.3",defaultElement:"<ul>",delay:300,options:{icons:{submenu:"ui-icon-carat-1-e"},menus:"ul",position:{my:"left top",at:"right top"},role:"menu",blur:null,focus:null,select:null},_create:function(){this.activeMenu=this.element,this.mouseHandled=!1,this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content ui-corner-all").toggleClass("ui-menu-icons",!!this.element.find(".ui-icon").length).attr({role:this.options.role,tabIndex:0}).bind("click"+this.eventNamespace,t.proxy(function(t){this.options.disabled&&t.preventDefault()},this)),this.options.disabled&&this.element.addClass("ui-state-disabled").attr("aria-disabled","true"),this._on({"mousedown .ui-menu-item > a":function(t){t.preventDefault()},"click .ui-state-disabled > a":function(t){t.preventDefault()},"click .ui-menu-item:has(a)":function(e){var i=t(e.target).closest(".ui-menu-item");!this.mouseHandled&&i.not(".ui-state-disabled").length&&(this.mouseHandled=!0,this.select(e),i.has(".ui-menu").length?this.expand(e):this.element.is(":focus")||(this.element.trigger("focus",[!0]),this.active&&1===this.active.parents(".ui-menu").length&&clearTimeout(this.timer)))},"mouseenter .ui-menu-item":function(e){var i=t(e.currentTarget);i.siblings().children(".ui-state-active").removeClass("ui-state-active"),this.focus(e,i)},mouseleave:"collapseAll","mouseleave .ui-menu":"collapseAll",focus:function(t,e){var i=this.active||this.element.children(".ui-menu-item").eq(0);e||this.focus(t,i)},blur:function(e){this._delay(function(){t.contains(this.element[0],this.document[0].activeElement)||this.collapseAll(e)})},keydown:"_keydown"}),this.refresh(),this._on(this.document,{click:function(e){t(e.target).closest(".ui-menu").length||this.collapseAll(e),this.mouseHandled=!1}})},_destroy:function(){this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeClass("ui-menu ui-widget ui-widget-content ui-corner-all ui-menu-icons").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show(),this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").children("a").removeUniqueId().removeClass("ui-corner-all ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function(){var e=t(this);e.data("ui-menu-submenu-carat")&&e.remove()}),this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content")},_keydown:function(e){function i(t){return t.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")}var s,n,a,o,r,h=!0;switch(e.keyCode){case t.ui.keyCode.PAGE_UP:this.previousPage(e);break;case t.ui.keyCode.PAGE_DOWN:this.nextPage(e);break;case t.ui.keyCode.HOME:this._move("first","first",e);break;case t.ui.keyCode.END:this._move("last","last",e);break;case t.ui.keyCode.UP:this.previous(e);break;case t.ui.keyCode.DOWN:this.next(e);break;case t.ui.keyCode.LEFT:this.collapse(e);break;case t.ui.keyCode.RIGHT:this.active&&!this.active.is(".ui-state-disabled")&&this.expand(e);break;case t.ui.keyCode.ENTER:case t.ui.keyCode.SPACE:this._activate(e);break;case t.ui.keyCode.ESCAPE:this.collapse(e);break;default:h=!1,n=this.previousFilter||"",a=String.fromCharCode(e.keyCode),o=!1,clearTimeout(this.filterTimer),a===n?o=!0:a=n+a,r=RegExp("^"+i(a),"i"),s=this.activeMenu.children(".ui-menu-item").filter(function(){return r.test(t(this).children("a").text())}),s=o&&-1!==s.index(this.active.next())?this.active.nextAll(".ui-menu-item"):s,s.length||(a=String.fromCharCode(e.keyCode),r=RegExp("^"+i(a),"i"),s=this.activeMenu.children(".ui-menu-item").filter(function(){return r.test(t(this).children("a").text())})),s.length?(this.focus(e,s),s.length>1?(this.previousFilter=a,this.filterTimer=this._delay(function(){delete this.previousFilter},1e3)):delete this.previousFilter):delete this.previousFilter}h&&e.preventDefault()},_activate:function(t){this.active.is(".ui-state-disabled")||(this.active.children("a[aria-haspopup='true']").length?this.expand(t):this.select(t))},refresh:function(){var e,i=this.options.icons.submenu,s=this.element.find(this.options.menus);s.filter(":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-corner-all").hide().attr({role:this.options.role,"aria-hidden":"true","aria-expanded":"false"}).each(function(){var e=t(this),s=e.prev("a"),n=t("<span>").addClass("ui-menu-icon ui-icon "+i).data("ui-menu-submenu-carat",!0);s.attr("aria-haspopup","true").prepend(n),e.attr("aria-labelledby",s.attr("id"))}),e=s.add(this.element),e.children(":not(.ui-menu-item):has(a)").addClass("ui-menu-item").attr("role","presentation").children("a").uniqueId().addClass("ui-corner-all").attr({tabIndex:-1,role:this._itemRole()}),e.children(":not(.ui-menu-item)").each(function(){var e=t(this);/[^\-\u2014\u2013\s]/.test(e.text())||e.addClass("ui-widget-content ui-menu-divider")}),e.children(".ui-state-disabled").attr("aria-disabled","true"),this.active&&!t.contains(this.element[0],this.active[0])&&this.blur()},_itemRole:function(){return{menu:"menuitem",listbox:"option"}[this.options.role]},_setOption:function(t,e){"icons"===t&&this.element.find(".ui-menu-icon").removeClass(this.options.icons.submenu).addClass(e.submenu),this._super(t,e)},focus:function(t,e){var i,s;this.blur(t,t&&"focus"===t.type),this._scrollIntoView(e),this.active=e.first(),s=this.active.children("a").addClass("ui-state-focus"),this.options.role&&this.element.attr("aria-activedescendant",s.attr("id")),this.active.parent().closest(".ui-menu-item").children("a:first").addClass("ui-state-active"),t&&"keydown"===t.type?this._close():this.timer=this._delay(function(){this._close()},this.delay),i=e.children(".ui-menu"),i.length&&/^mouse/.test(t.type)&&this._startOpening(i),this.activeMenu=e.parent(),this._trigger("focus",t,{item:e})},_scrollIntoView:function(e){var i,s,n,a,o,r;this._hasScroll()&&(i=parseFloat(t.css(this.activeMenu[0],"borderTopWidth"))||0,s=parseFloat(t.css(this.activeMenu[0],"paddingTop"))||0,n=e.offset().top-this.activeMenu.offset().top-i-s,a=this.activeMenu.scrollTop(),o=this.activeMenu.height(),r=e.height(),0>n?this.activeMenu.scrollTop(a+n):n+r>o&&this.activeMenu.scrollTop(a+n-o+r))},blur:function(t,e){e||clearTimeout(this.timer),this.active&&(this.active.children("a").removeClass("ui-state-focus"),this.active=null,this._trigger("blur",t,{item:this.active}))},_startOpening:function(t){clearTimeout(this.timer),"true"===t.attr("aria-hidden")&&(this.timer=this._delay(function(){this._close(),this._open(t)},this.delay))},_open:function(e){var i=t.extend({of:this.active},this.options.position);clearTimeout(this.timer),this.element.find(".ui-menu").not(e.parents(".ui-menu")).hide().attr("aria-hidden","true"),e.show().removeAttr("aria-hidden").attr("aria-expanded","true").position(i)},collapseAll:function(e,i){clearTimeout(this.timer),this.timer=this._delay(function(){var s=i?this.element:t(e&&e.target).closest(this.element.find(".ui-menu"));s.length||(s=this.element),this._close(s),this.blur(e),this.activeMenu=s},this.delay)},_close:function(t){t||(t=this.active?this.active.parent():this.element),t.find(".ui-menu").hide().attr("aria-hidden","true").attr("aria-expanded","false").end().find("a.ui-state-active").removeClass("ui-state-active")},collapse:function(t){var e=this.active&&this.active.parent().closest(".ui-menu-item",this.element);e&&e.length&&(this._close(),this.focus(t,e))},expand:function(t){var e=this.active&&this.active.children(".ui-menu ").children(".ui-menu-item").first();e&&e.length&&(this._open(e.parent()),this._delay(function(){this.focus(t,e)}))},next:function(t){this._move("next","first",t)},previous:function(t){this._move("prev","last",t)},isFirstItem:function(){return this.active&&!this.active.prevAll(".ui-menu-item").length},isLastItem:function(){return this.active&&!this.active.nextAll(".ui-menu-item").length},_move:function(t,e,i){var s;this.active&&(s="first"===t||"last"===t?this.active["first"===t?"prevAll":"nextAll"](".ui-menu-item").eq(-1):this.active[t+"All"](".ui-menu-item").eq(0)),s&&s.length&&this.active||(s=this.activeMenu.children(".ui-menu-item")[e]()),this.focus(i,s)},nextPage:function(e){var i,s,n;return this.active?(this.isLastItem()||(this._hasScroll()?(s=this.active.offset().top,n=this.element.height(),this.active.nextAll(".ui-menu-item").each(function(){return i=t(this),0>i.offset().top-s-n}),this.focus(e,i)):this.focus(e,this.activeMenu.children(".ui-menu-item")[this.active?"last":"first"]())),undefined):(this.next(e),undefined)},previousPage:function(e){var i,s,n;return this.active?(this.isFirstItem()||(this._hasScroll()?(s=this.active.offset().top,n=this.element.height(),this.active.prevAll(".ui-menu-item").each(function(){return i=t(this),i.offset().top-s+n>0}),this.focus(e,i)):this.focus(e,this.activeMenu.children(".ui-menu-item").first())),undefined):(this.next(e),undefined)},_hasScroll:function(){return this.element.outerHeight()<this.element.prop("scrollHeight")},select:function(e){this.active=this.active||t(e.target).closest(".ui-menu-item");var i={item:this.active};this.active.has(".ui-menu").length||this.collapseAll(e,!0),this._trigger("select",e,i)}})})(jQuery);
/*!
 * jScrollPane - v2.0.14 - 2013-05-01
 * http://jscrollpane.kelvinluck.com/
 *
 * Copyright (c) 2010 Kelvin Luck
 * Dual licensed under the MIT or GPL licenses.
 */
(function(b,a,c){b.fn.jScrollPane=function(e){function d(D,O){var ay,Q=this,Y,aj,v,al,T,Z,y,q,az,aE,au,i,I,h,j,aa,U,ap,X,t,A,aq,af,am,G,l,at,ax,x,av,aH,f,L,ai=true,P=true,aG=false,k=false,ao=D.clone(false,false).empty(),ac=b.fn.mwheelIntent?"mwheelIntent.jsp":"mousewheel.jsp";aH=D.css("paddingTop")+" "+D.css("paddingRight")+" "+D.css("paddingBottom")+" "+D.css("paddingLeft");f=(parseInt(D.css("paddingLeft"),10)||0)+(parseInt(D.css("paddingRight"),10)||0);function ar(aQ){var aL,aN,aM,aJ,aI,aP,aO=false,aK=false;ay=aQ;if(Y===c){aI=D.scrollTop();aP=D.scrollLeft();D.css({overflow:"hidden",padding:0});aj=D.innerWidth()+f;v=D.innerHeight();D.width(aj);Y=b('<div class="jspPane" />').css("padding",aH).append(D.children());al=b('<div class="jspContainer" />').css({width:aj+"px",height:v+"px"}).append(Y).appendTo(D)}else{D.css("width","");aO=ay.stickToBottom&&K();aK=ay.stickToRight&&B();aJ=D.innerWidth()+f!=aj||D.outerHeight()!=v;if(aJ){aj=D.innerWidth()+f;v=D.innerHeight();al.css({width:aj+"px",height:v+"px"})}if(!aJ&&L==T&&Y.outerHeight()==Z){D.width(aj);return}L=T;Y.css("width","");D.width(aj);al.find(">.jspVerticalBar,>.jspHorizontalBar").remove().end()}Y.css("overflow","auto");if(aQ.contentWidth){T=aQ.contentWidth}else{T=Y[0].scrollWidth}Z=Y[0].scrollHeight;Y.css("overflow","");y=T/aj;q=Z/v;az=q>1;aE=y>1;if(!(aE||az)){D.removeClass("jspScrollable");Y.css({top:0,width:al.width()-f});n();E();R();w()}else{D.addClass("jspScrollable");aL=ay.maintainPosition&&(I||aa);if(aL){aN=aC();aM=aA()}aF();z();F();if(aL){N(aK?(T-aj):aN,false);M(aO?(Z-v):aM,false)}J();ag();an();if(ay.enableKeyboardNavigation){S()}if(ay.clickOnTrack){p()}C();if(ay.hijackInternalLinks){m()}}if(ay.autoReinitialise&&!av){av=setInterval(function(){ar(ay)},ay.autoReinitialiseDelay)}else{if(!ay.autoReinitialise&&av){clearInterval(av)}}aI&&D.scrollTop(0)&&M(aI,false);aP&&D.scrollLeft(0)&&N(aP,false);D.trigger("jsp-initialised",[aE||az])}function aF(){if(az){al.append(b('<div class="jspVerticalBar" />').append(b('<div class="jspCap jspCapTop" />'),b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragTop" />'),b('<div class="jspDragBottom" />'))),b('<div class="jspCap jspCapBottom" />')));U=al.find(">.jspVerticalBar");ap=U.find(">.jspTrack");au=ap.find(">.jspDrag");if(ay.showArrows){aq=b('<a class="jspArrow jspArrowUp" />').bind("mousedown.jsp",aD(0,-1)).bind("click.jsp",aB);af=b('<a class="jspArrow jspArrowDown" />').bind("mousedown.jsp",aD(0,1)).bind("click.jsp",aB);if(ay.arrowScrollOnHover){aq.bind("mouseover.jsp",aD(0,-1,aq));af.bind("mouseover.jsp",aD(0,1,af))}ak(ap,ay.verticalArrowPositions,aq,af)}t=v;al.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function(){t-=b(this).outerHeight()});au.hover(function(){au.addClass("jspHover")},function(){au.removeClass("jspHover")}).bind("mousedown.jsp",function(aI){b("html").bind("dragstart.jsp selectstart.jsp",aB);au.addClass("jspActive");var s=aI.pageY-au.position().top;b("html").bind("mousemove.jsp",function(aJ){V(aJ.pageY-s,false)}).bind("mouseup.jsp mouseleave.jsp",aw);return false});o()}}function o(){ap.height(t+"px");I=0;X=ay.verticalGutter+ap.outerWidth();Y.width(aj-X-f);try{if(U.position().left===0){Y.css("margin-left",X+"px")}}catch(s){}}function z(){if(aE){al.append(b('<div class="jspHorizontalBar" />').append(b('<div class="jspCap jspCapLeft" />'),b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragLeft" />'),b('<div class="jspDragRight" />'))),b('<div class="jspCap jspCapRight" />')));am=al.find(">.jspHorizontalBar");G=am.find(">.jspTrack");h=G.find(">.jspDrag");if(ay.showArrows){ax=b('<a class="jspArrow jspArrowLeft" />').bind("mousedown.jsp",aD(-1,0)).bind("click.jsp",aB);x=b('<a class="jspArrow jspArrowRight" />').bind("mousedown.jsp",aD(1,0)).bind("click.jsp",aB);
if(ay.arrowScrollOnHover){ax.bind("mouseover.jsp",aD(-1,0,ax));x.bind("mouseover.jsp",aD(1,0,x))}ak(G,ay.horizontalArrowPositions,ax,x)}h.hover(function(){h.addClass("jspHover")},function(){h.removeClass("jspHover")}).bind("mousedown.jsp",function(aI){b("html").bind("dragstart.jsp selectstart.jsp",aB);h.addClass("jspActive");var s=aI.pageX-h.position().left;b("html").bind("mousemove.jsp",function(aJ){W(aJ.pageX-s,false)}).bind("mouseup.jsp mouseleave.jsp",aw);return false});l=al.innerWidth();ah()}}function ah(){al.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function(){l-=b(this).outerWidth()});G.width(l+"px");aa=0}function F(){if(aE&&az){var aI=G.outerHeight(),s=ap.outerWidth();t-=aI;b(am).find(">.jspCap:visible,>.jspArrow").each(function(){l+=b(this).outerWidth()});l-=s;v-=s;aj-=aI;G.parent().append(b('<div class="jspCorner" />').css("width",aI+"px"));o();ah()}if(aE){Y.width((al.outerWidth()-f)+"px")}Z=Y.outerHeight();q=Z/v;if(aE){at=Math.ceil(1/y*l);if(at>ay.horizontalDragMaxWidth){at=ay.horizontalDragMaxWidth}else{if(at<ay.horizontalDragMinWidth){at=ay.horizontalDragMinWidth}}h.width(at+"px");j=l-at;ae(aa)}if(az){A=Math.ceil(1/q*t);if(A>ay.verticalDragMaxHeight){A=ay.verticalDragMaxHeight}else{if(A<ay.verticalDragMinHeight){A=ay.verticalDragMinHeight}}au.height(A+"px");i=t-A;ad(I)}}function ak(aJ,aL,aI,s){var aN="before",aK="after",aM;if(aL=="os"){aL=/Mac/.test(navigator.platform)?"after":"split"}if(aL==aN){aK=aL}else{if(aL==aK){aN=aL;aM=aI;aI=s;s=aM}}aJ[aN](aI)[aK](s)}function aD(aI,s,aJ){return function(){H(aI,s,this,aJ);this.blur();return false}}function H(aL,aK,aO,aN){aO=b(aO).addClass("jspActive");var aM,aJ,aI=true,s=function(){if(aL!==0){Q.scrollByX(aL*ay.arrowButtonSpeed)}if(aK!==0){Q.scrollByY(aK*ay.arrowButtonSpeed)}aJ=setTimeout(s,aI?ay.initialDelay:ay.arrowRepeatFreq);aI=false};s();aM=aN?"mouseout.jsp":"mouseup.jsp";aN=aN||b("html");aN.bind(aM,function(){aO.removeClass("jspActive");aJ&&clearTimeout(aJ);aJ=null;aN.unbind(aM)})}function p(){w();if(az){ap.bind("mousedown.jsp",function(aN){if(aN.originalTarget===c||aN.originalTarget==aN.currentTarget){var aL=b(this),aO=aL.offset(),aM=aN.pageY-aO.top-I,aJ,aI=true,s=function(){var aR=aL.offset(),aS=aN.pageY-aR.top-A/2,aP=v*ay.scrollPagePercent,aQ=i*aP/(Z-v);if(aM<0){if(I-aQ>aS){Q.scrollByY(-aP)}else{V(aS)}}else{if(aM>0){if(I+aQ<aS){Q.scrollByY(aP)}else{V(aS)}}else{aK();return}}aJ=setTimeout(s,aI?ay.initialDelay:ay.trackClickRepeatFreq);aI=false},aK=function(){aJ&&clearTimeout(aJ);aJ=null;b(document).unbind("mouseup.jsp",aK)};s();b(document).bind("mouseup.jsp",aK);return false}})}if(aE){G.bind("mousedown.jsp",function(aN){if(aN.originalTarget===c||aN.originalTarget==aN.currentTarget){var aL=b(this),aO=aL.offset(),aM=aN.pageX-aO.left-aa,aJ,aI=true,s=function(){var aR=aL.offset(),aS=aN.pageX-aR.left-at/2,aP=aj*ay.scrollPagePercent,aQ=j*aP/(T-aj);if(aM<0){if(aa-aQ>aS){Q.scrollByX(-aP)}else{W(aS)}}else{if(aM>0){if(aa+aQ<aS){Q.scrollByX(aP)}else{W(aS)}}else{aK();return}}aJ=setTimeout(s,aI?ay.initialDelay:ay.trackClickRepeatFreq);aI=false},aK=function(){aJ&&clearTimeout(aJ);aJ=null;b(document).unbind("mouseup.jsp",aK)};s();b(document).bind("mouseup.jsp",aK);return false}})}}function w(){if(G){G.unbind("mousedown.jsp")}if(ap){ap.unbind("mousedown.jsp")}}function aw(){b("html").unbind("dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp");if(au){au.removeClass("jspActive")}if(h){h.removeClass("jspActive")}}function V(s,aI){if(!az){return}if(s<0){s=0}else{if(s>i){s=i}}if(aI===c){aI=ay.animateScroll}if(aI){Q.animate(au,"top",s,ad)}else{au.css("top",s);ad(s)}}function ad(aI){if(aI===c){aI=au.position().top}al.scrollTop(0);I=aI;var aL=I===0,aJ=I==i,aK=aI/i,s=-aK*(Z-v);if(ai!=aL||aG!=aJ){ai=aL;aG=aJ;D.trigger("jsp-arrow-change",[ai,aG,P,k])}u(aL,aJ);Y.css("top",s);D.trigger("jsp-scroll-y",[-s,aL,aJ]).trigger("scroll")}function W(aI,s){if(!aE){return}if(aI<0){aI=0}else{if(aI>j){aI=j}}if(s===c){s=ay.animateScroll}if(s){Q.animate(h,"left",aI,ae)
}else{h.css("left",aI);ae(aI)}}function ae(aI){if(aI===c){aI=h.position().left}al.scrollTop(0);aa=aI;var aL=aa===0,aK=aa==j,aJ=aI/j,s=-aJ*(T-aj);if(P!=aL||k!=aK){P=aL;k=aK;D.trigger("jsp-arrow-change",[ai,aG,P,k])}r(aL,aK);Y.css("left",s);D.trigger("jsp-scroll-x",[-s,aL,aK]).trigger("scroll")}function u(aI,s){if(ay.showArrows){aq[aI?"addClass":"removeClass"]("jspDisabled");af[s?"addClass":"removeClass"]("jspDisabled")}}function r(aI,s){if(ay.showArrows){ax[aI?"addClass":"removeClass"]("jspDisabled");x[s?"addClass":"removeClass"]("jspDisabled")}}function M(s,aI){var aJ=s/(Z-v);V(aJ*i,aI)}function N(aI,s){var aJ=aI/(T-aj);W(aJ*j,s)}function ab(aV,aQ,aJ){var aN,aK,aL,s=0,aU=0,aI,aP,aO,aS,aR,aT;try{aN=b(aV)}catch(aM){return}aK=aN.outerHeight();aL=aN.outerWidth();al.scrollTop(0);al.scrollLeft(0);while(!aN.is(".jspPane")){s+=aN.position().top;aU+=aN.position().left;aN=aN.offsetParent();if(/^body|html$/i.test(aN[0].nodeName)){return}}aI=aA();aO=aI+v;if(s<aI||aQ){aR=s-ay.verticalGutter}else{if(s+aK>aO){aR=s-v+aK+ay.verticalGutter}}if(aR){M(aR,aJ)}aP=aC();aS=aP+aj;if(aU<aP||aQ){aT=aU-ay.horizontalGutter}else{if(aU+aL>aS){aT=aU-aj+aL+ay.horizontalGutter}}if(aT){N(aT,aJ)}}function aC(){return -Y.position().left}function aA(){return -Y.position().top}function K(){var s=Z-v;return(s>20)&&(s-aA()<10)}function B(){var s=T-aj;return(s>20)&&(s-aC()<10)}function ag(){al.unbind(ac).bind(ac,function(aL,aM,aK,aI){var aJ=aa,s=I;Q.scrollBy(aK*ay.mouseWheelSpeed,-aI*ay.mouseWheelSpeed,false);return aJ==aa&&s==I})}function n(){al.unbind(ac)}function aB(){return false}function J(){Y.find(":input,a").unbind("focus.jsp").bind("focus.jsp",function(s){ab(s.target,false)})}function E(){Y.find(":input,a").unbind("focus.jsp")}function S(){var s,aI,aK=[];aE&&aK.push(am[0]);az&&aK.push(U[0]);Y.focus(function(){D.focus()});D.attr("tabindex",0).unbind("keydown.jsp keypress.jsp").bind("keydown.jsp",function(aN){if(aN.target!==this&&!(aK.length&&b(aN.target).closest(aK).length)){return}var aM=aa,aL=I;switch(aN.keyCode){case 40:case 38:case 34:case 32:case 33:case 39:case 37:s=aN.keyCode;aJ();break;case 35:M(Z-v);s=null;break;case 36:M(0);s=null;break}aI=aN.keyCode==s&&aM!=aa||aL!=I;return !aI}).bind("keypress.jsp",function(aL){if(aL.keyCode==s){aJ()}return !aI});if(ay.hideFocus){D.css("outline","none");if("hideFocus" in al[0]){D.attr("hideFocus",true)}}else{D.css("outline","");if("hideFocus" in al[0]){D.attr("hideFocus",false)}}function aJ(){var aM=aa,aL=I;switch(s){case 40:Q.scrollByY(ay.keyboardSpeed,false);break;case 38:Q.scrollByY(-ay.keyboardSpeed,false);break;case 34:case 32:Q.scrollByY(v*ay.scrollPagePercent,false);break;case 33:Q.scrollByY(-v*ay.scrollPagePercent,false);break;case 39:Q.scrollByX(ay.keyboardSpeed,false);break;case 37:Q.scrollByX(-ay.keyboardSpeed,false);break}aI=aM!=aa||aL!=I;return aI}}function R(){D.attr("tabindex","-1").removeAttr("tabindex").unbind("keydown.jsp keypress.jsp")}function C(){if(location.hash&&location.hash.length>1){var aK,aI,aJ=escape(location.hash.substr(1));try{aK=b("#"+aJ+', a[name="'+aJ+'"]')}catch(s){return}if(aK.length&&Y.find(aJ)){if(al.scrollTop()===0){aI=setInterval(function(){if(al.scrollTop()>0){ab(aK,true);b(document).scrollTop(al.position().top);clearInterval(aI)}},50)}else{ab(aK,true);b(document).scrollTop(al.position().top)}}}}function m(){if(b(document.body).data("jspHijack")){return}b(document.body).data("jspHijack",true);b(document.body).delegate("a[href*=#]","click",function(s){var aI=this.href.substr(0,this.href.indexOf("#")),aK=location.href,aO,aP,aJ,aM,aL,aN;if(location.href.indexOf("#")!==-1){aK=location.href.substr(0,location.href.indexOf("#"))}if(aI!==aK){return}aO=escape(this.href.substr(this.href.indexOf("#")+1));aP;try{aP=b("#"+aO+', a[name="'+aO+'"]')}catch(aQ){return}if(!aP.length){return}aJ=aP.closest(".jspScrollable");aM=aJ.data("jsp");aM.scrollToElement(aP,true);if(aJ[0].scrollIntoView){aL=b(a).scrollTop();aN=aP.offset().top;if(aN<aL||aN>aL+b(a).height()){aJ[0].scrollIntoView()}}s.preventDefault()
})}function an(){var aJ,aI,aL,aK,aM,s=false;al.unbind("touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick").bind("touchstart.jsp",function(aN){var aO=aN.originalEvent.touches[0];aJ=aC();aI=aA();aL=aO.pageX;aK=aO.pageY;aM=false;s=true}).bind("touchmove.jsp",function(aQ){if(!s){return}var aP=aQ.originalEvent.touches[0],aO=aa,aN=I;Q.scrollTo(aJ+aL-aP.pageX,aI+aK-aP.pageY);aM=aM||Math.abs(aL-aP.pageX)>5||Math.abs(aK-aP.pageY)>5;return aO==aa&&aN==I}).bind("touchend.jsp",function(aN){s=false}).bind("click.jsp-touchclick",function(aN){if(aM){aM=false;return false}})}function g(){var s=aA(),aI=aC();D.removeClass("jspScrollable").unbind(".jsp");D.replaceWith(ao.append(Y.children()));ao.scrollTop(s);ao.scrollLeft(aI);if(av){clearInterval(av)}}b.extend(Q,{reinitialise:function(aI){aI=b.extend({},ay,aI);ar(aI)},scrollToElement:function(aJ,aI,s){ab(aJ,aI,s)},scrollTo:function(aJ,s,aI){N(aJ,aI);M(s,aI)},scrollToX:function(aI,s){N(aI,s)},scrollToY:function(s,aI){M(s,aI)},scrollToPercentX:function(aI,s){N(aI*(T-aj),s)},scrollToPercentY:function(aI,s){M(aI*(Z-v),s)},scrollBy:function(aI,s,aJ){Q.scrollByX(aI,aJ);Q.scrollByY(s,aJ)},scrollByX:function(s,aJ){var aI=aC()+Math[s<0?"floor":"ceil"](s),aK=aI/(T-aj);W(aK*j,aJ)},scrollByY:function(s,aJ){var aI=aA()+Math[s<0?"floor":"ceil"](s),aK=aI/(Z-v);V(aK*i,aJ)},positionDragX:function(s,aI){W(s,aI)},positionDragY:function(aI,s){V(aI,s)},animate:function(aI,aL,s,aK){var aJ={};aJ[aL]=s;aI.animate(aJ,{duration:ay.animateDuration,easing:ay.animateEase,queue:false,step:aK})},getContentPositionX:function(){return aC()},getContentPositionY:function(){return aA()},getContentWidth:function(){return T},getContentHeight:function(){return Z},getPercentScrolledX:function(){return aC()/(T-aj)},getPercentScrolledY:function(){return aA()/(Z-v)},getIsScrollableH:function(){return aE},getIsScrollableV:function(){return az},getContentPane:function(){return Y},scrollToBottom:function(s){V(i,s)},hijackInternalLinks:b.noop,destroy:function(){g()}});ar(O)}e=b.extend({},b.fn.jScrollPane.defaults,e);b.each(["arrowButtonSpeed","trackClickSpeed","keyboardSpeed"],function(){e[this]=e[this]||e.speed});return this.each(function(){var f=b(this),g=f.data("jsp");if(g){g.reinitialise(e)}else{b("script",f).filter('[type="text/javascript"],:not([type])').remove();g=new d(f,e);f.data("jsp",g)}})};b.fn.jScrollPane.defaults={showArrows:false,maintainPosition:true,stickToBottom:false,stickToRight:false,clickOnTrack:true,autoReinitialise:false,autoReinitialiseDelay:500,verticalDragMinHeight:0,verticalDragMaxHeight:99999,horizontalDragMinWidth:0,horizontalDragMaxWidth:99999,contentWidth:c,animateScroll:false,animateDuration:300,animateEase:"linear",hijackInternalLinks:false,verticalGutter:4,horizontalGutter:4,mouseWheelSpeed:35,arrowButtonSpeed:0,arrowRepeatFreq:50,arrowScrollOnHover:false,trackClickSpeed:0,trackClickRepeatFreq:70,verticalArrowPositions:"split",horizontalArrowPositions:"split",enableKeyboardNavigation:true,hideFocus:false,keyboardSpeed:0,initialDelay:300,speed:30,scrollPagePercent:0.8}})(jQuery,this);
/*! Copyright (c) 2013 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.1.3
 *
 * Requires: 1.2.2+
 */
(function(e){typeof define=="function"&&define.amd?define(["jquery"],e):typeof exports=="object"?module.exports=e:e(jQuery)})(function(e){function t(t){var l,i=t||window.event,s=[].slice.call(arguments,1),h=0,u=0,a=0,r=0,d=0;return t=e.event.fix(i),t.type="mousewheel",i.wheelDelta&&(h=i.wheelDelta),i.detail&&(h=i.detail*-1),i.deltaY&&(a=i.deltaY*-1,h=a),i.deltaX&&(u=i.deltaX,h=u*-1),i.wheelDeltaY!==void 0&&(a=i.wheelDeltaY),i.wheelDeltaX!==void 0&&(u=i.wheelDeltaX*-1),r=Math.abs(h),(!o||o>r)&&(o=r),d=Math.max(Math.abs(a),Math.abs(u)),(!n||n>d)&&(n=d),l=h>0?"floor":"ceil",h=Math[l](h/o),u=Math[l](u/n),a=Math[l](a/n),s.unshift(t,h,u,a),(e.event.dispatch||e.event.handle).apply(this,s)}var o,n,l=["wheel","mousewheel","DOMMouseScroll","MozMousePixelScroll"],i="onwheel"in document||document.documentMode>=9?["wheel"]:["mousewheel","DomMouseScroll","MozMousePixelScroll"];if(e.event.fixHooks)for(var s=l.length;s;)e.event.fixHooks[l[--s]]=e.event.mouseHooks;e.event.special.mousewheel={setup:function(){if(this.addEventListener)for(var e=i.length;e;)this.addEventListener(i[--e],t,!1);else this.onmousewheel=t},teardown:function(){if(this.removeEventListener)for(var e=i.length;e;)this.removeEventListener(i[--e],t,!1);else this.onmousewheel=null}},e.fn.extend({mousewheel:function(e){return e?this.bind("mousewheel",e):this.trigger("mousewheel")},unmousewheel:function(e){return this.unbind("mousewheel",e)}})})
/**
 * Copyright (c) 2007-2013 Ariel Flesler - aflesler<a>gmail<d>com | http://flesler.blogspot.com
 * Dual licensed under MIT and GPL.
 * @author Ariel Flesler
 * @version 1.4.6
 */
;(function($){var h=$.scrollTo=function(a,b,c){$(window).scrollTo(a,b,c)};h.defaults={axis:'xy',duration:parseFloat($.fn.jquery)>=1.3?0:1,limit:true};h.window=function(a){return $(window)._scrollable()};$.fn._scrollable=function(){return this.map(function(){var a=this,isWin=!a.nodeName||$.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!isWin)return a;var b=(a.contentWindow||a).document||a.ownerDocument||a;return/webkit/i.test(navigator.userAgent)||b.compatMode=='BackCompat'?b.body:b.documentElement})};$.fn.scrollTo=function(e,f,g){if(typeof f=='object'){g=f;f=0}if(typeof g=='function')g={onAfter:g};if(e=='max')e=9e9;g=$.extend({},h.defaults,g);f=f||g.duration;g.queue=g.queue&&g.axis.length>1;if(g.queue)f/=2;g.offset=both(g.offset);g.over=both(g.over);return this._scrollable().each(function(){if(e==null)return;var d=this,$elem=$(d),targ=e,toff,attr={},win=$elem.is('html,body');switch(typeof targ){case'number':case'string':if(/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)){targ=both(targ);break}targ=$(targ,this);if(!targ.length)return;case'object':if(targ.is||targ.style)toff=(targ=$(targ)).offset()}$.each(g.axis.split(''),function(i,a){var b=a=='x'?'Left':'Top',pos=b.toLowerCase(),key='scroll'+b,old=d[key],max=h.max(d,a);if(toff){attr[key]=toff[pos]+(win?0:old-$elem.offset()[pos]);if(g.margin){attr[key]-=parseInt(targ.css('margin'+b))||0;attr[key]-=parseInt(targ.css('border'+b+'Width'))||0}attr[key]+=g.offset[pos]||0;if(g.over[pos])attr[key]+=targ[a=='x'?'width':'height']()*g.over[pos]}else{var c=targ[pos];attr[key]=c.slice&&c.slice(-1)=='%'?parseFloat(c)/100*max:c}if(g.limit&&/^\d+$/.test(attr[key]))attr[key]=attr[key]<=0?0:Math.min(attr[key],max);if(!i&&g.queue){if(old!=attr[key])animate(g.onAfterFirst);delete attr[key]}});animate(g.onAfter);function animate(a){$elem.animate(attr,f,g.easing,a&&function(){a.call(this,targ,g)})}}).end()};h.max=function(a,b){var c=b=='x'?'Width':'Height',scroll='scroll'+c;if(!$(a).is('html,body'))return a[scroll]-$(a)[c.toLowerCase()]();var d='client'+c,html=a.ownerDocument.documentElement,body=a.ownerDocument.body;return Math.max(html[scroll],body[scroll])-Math.min(html[d],body[d])};function both(a){return typeof a=='object'?a:{top:a,left:a}}})(jQuery);
/*! Hammer.JS - v1.0.5 - 2013-04-07
 * http://eightmedia.github.com/hammer.js
 *
 * Copyright (c) 2013 Jorik Tangelder <j.tangelder@gmail.com>;
 * Licensed under the MIT license */

(function(t,e){"use strict";function n(){if(!i.READY){i.event.determineEventTypes();for(var t in i.gestures)i.gestures.hasOwnProperty(t)&&i.detection.register(i.gestures[t]);i.event.onTouch(i.DOCUMENT,i.EVENT_MOVE,i.detection.detect),i.event.onTouch(i.DOCUMENT,i.EVENT_END,i.detection.detect),i.READY=!0}}var i=function(t,e){return new i.Instance(t,e||{})};i.defaults={stop_browser_behavior:{userSelect:"",touchAction:"none",touchCallout:"none",contentZooming:"none",userDrag:"none",tapHighlightColor:"rgba(0,0,0,0)"}},i.HAS_POINTEREVENTS=navigator.pointerEnabled||navigator.msPointerEnabled,i.HAS_TOUCHEVENTS="ontouchstart"in t,i.MOBILE_REGEX=/mobile|tablet|ip(ad|hone|od)|android/i,i.NO_MOUSEEVENTS=i.HAS_TOUCHEVENTS&&navigator.userAgent.match(i.MOBILE_REGEX),i.EVENT_TYPES={},i.DIRECTION_DOWN="down",i.DIRECTION_LEFT="left",i.DIRECTION_UP="up",i.DIRECTION_RIGHT="right",i.POINTER_MOUSE="mouse",i.POINTER_TOUCH="touch",i.POINTER_PEN="pen",i.EVENT_START="start",i.EVENT_MOVE="move",i.EVENT_END="end",i.DOCUMENT=document,i.plugins={},i.READY=!1,i.Instance=function(t,e){var r=this;return n(),this.element=t,this.enabled=!0,this.options=i.utils.extend(i.utils.extend({},i.defaults),e||{}),this.options.stop_browser_behavior&&i.utils.stopDefaultBrowserBehavior(this.element,this.options.stop_browser_behavior),i.event.onTouch(t,i.EVENT_START,function(t){r.enabled&&i.detection.startDetect(r,t)}),this},i.Instance.prototype={on:function(t,e){for(var n=t.split(" "),i=0;n.length>i;i++)this.element.addEventListener(n[i],e,!1);return this},off:function(t,e){for(var n=t.split(" "),i=0;n.length>i;i++)this.element.removeEventListener(n[i],e,!1);return this},trigger:function(t,e){var n=i.DOCUMENT.createEvent("Event");n.initEvent(t,!0,!0),n.gesture=e;var r=this.element;return i.utils.hasParent(e.target,r)&&(r=e.target),r.dispatchEvent(n),this},enable:function(t){return this.enabled=t,this}};var r=null,o=!1,s=!1;i.event={bindDom:function(t,e,n){for(var i=e.split(" "),r=0;i.length>r;r++)t.addEventListener(i[r],n,!1)},onTouch:function(t,e,n){var a=this;this.bindDom(t,i.EVENT_TYPES[e],function(c){var u=c.type.toLowerCase();if(!u.match(/mouse/)||!s){(u.match(/touch/)||u.match(/pointerdown/)||u.match(/mouse/)&&1===c.which)&&(o=!0),u.match(/touch|pointer/)&&(s=!0);var h=0;o&&(i.HAS_POINTEREVENTS&&e!=i.EVENT_END?h=i.PointerEvent.updatePointer(e,c):u.match(/touch/)?h=c.touches.length:s||(h=u.match(/up/)?0:1),h>0&&e==i.EVENT_END?e=i.EVENT_MOVE:h||(e=i.EVENT_END),h||null===r?r=c:c=r,n.call(i.detection,a.collectEventData(t,e,c)),i.HAS_POINTEREVENTS&&e==i.EVENT_END&&(h=i.PointerEvent.updatePointer(e,c))),h||(r=null,o=!1,s=!1,i.PointerEvent.reset())}})},determineEventTypes:function(){var t;t=i.HAS_POINTEREVENTS?i.PointerEvent.getEvents():i.NO_MOUSEEVENTS?["touchstart","touchmove","touchend touchcancel"]:["touchstart mousedown","touchmove mousemove","touchend touchcancel mouseup"],i.EVENT_TYPES[i.EVENT_START]=t[0],i.EVENT_TYPES[i.EVENT_MOVE]=t[1],i.EVENT_TYPES[i.EVENT_END]=t[2]},getTouchList:function(t){return i.HAS_POINTEREVENTS?i.PointerEvent.getTouchList():t.touches?t.touches:[{identifier:1,pageX:t.pageX,pageY:t.pageY,target:t.target}]},collectEventData:function(t,e,n){var r=this.getTouchList(n,e),o=i.POINTER_TOUCH;return(n.type.match(/mouse/)||i.PointerEvent.matchType(i.POINTER_MOUSE,n))&&(o=i.POINTER_MOUSE),{center:i.utils.getCenter(r),timeStamp:(new Date).getTime(),target:n.target,touches:r,eventType:e,pointerType:o,srcEvent:n,preventDefault:function(){this.srcEvent.preventManipulation&&this.srcEvent.preventManipulation(),this.srcEvent.preventDefault&&this.srcEvent.preventDefault()},stopPropagation:function(){this.srcEvent.stopPropagation()},stopDetect:function(){return i.detection.stopDetect()}}}},i.PointerEvent={pointers:{},getTouchList:function(){var t=this,e=[];return Object.keys(t.pointers).sort().forEach(function(n){e.push(t.pointers[n])}),e},updatePointer:function(t,e){return t==i.EVENT_END?this.pointers={}:(e.identifier=e.pointerId,this.pointers[e.pointerId]=e),Object.keys(this.pointers).length},matchType:function(t,e){if(!e.pointerType)return!1;var n={};return n[i.POINTER_MOUSE]=e.pointerType==e.MSPOINTER_TYPE_MOUSE||e.pointerType==i.POINTER_MOUSE,n[i.POINTER_TOUCH]=e.pointerType==e.MSPOINTER_TYPE_TOUCH||e.pointerType==i.POINTER_TOUCH,n[i.POINTER_PEN]=e.pointerType==e.MSPOINTER_TYPE_PEN||e.pointerType==i.POINTER_PEN,n[t]},getEvents:function(){return["pointerdown MSPointerDown","pointermove MSPointerMove","pointerup pointercancel MSPointerUp MSPointerCancel"]},reset:function(){this.pointers={}}},i.utils={extend:function(t,n,i){for(var r in n)t[r]!==e&&i||(t[r]=n[r]);return t},hasParent:function(t,e){for(;t;){if(t==e)return!0;t=t.parentNode}return!1},getCenter:function(t){for(var e=[],n=[],i=0,r=t.length;r>i;i++)e.push(t[i].pageX),n.push(t[i].pageY);return{pageX:(Math.min.apply(Math,e)+Math.max.apply(Math,e))/2,pageY:(Math.min.apply(Math,n)+Math.max.apply(Math,n))/2}},getVelocity:function(t,e,n){return{x:Math.abs(e/t)||0,y:Math.abs(n/t)||0}},getAngle:function(t,e){var n=e.pageY-t.pageY,i=e.pageX-t.pageX;return 180*Math.atan2(n,i)/Math.PI},getDirection:function(t,e){var n=Math.abs(t.pageX-e.pageX),r=Math.abs(t.pageY-e.pageY);return n>=r?t.pageX-e.pageX>0?i.DIRECTION_LEFT:i.DIRECTION_RIGHT:t.pageY-e.pageY>0?i.DIRECTION_UP:i.DIRECTION_DOWN},getDistance:function(t,e){var n=e.pageX-t.pageX,i=e.pageY-t.pageY;return Math.sqrt(n*n+i*i)},getScale:function(t,e){return t.length>=2&&e.length>=2?this.getDistance(e[0],e[1])/this.getDistance(t[0],t[1]):1},getRotation:function(t,e){return t.length>=2&&e.length>=2?this.getAngle(e[1],e[0])-this.getAngle(t[1],t[0]):0},isVertical:function(t){return t==i.DIRECTION_UP||t==i.DIRECTION_DOWN},stopDefaultBrowserBehavior:function(t,e){var n,i=["webkit","khtml","moz","ms","o",""];if(e&&t.style){for(var r=0;i.length>r;r++)for(var o in e)e.hasOwnProperty(o)&&(n=o,i[r]&&(n=i[r]+n.substring(0,1).toUpperCase()+n.substring(1)),t.style[n]=e[o]);"none"==e.userSelect&&(t.onselectstart=function(){return!1})}}},i.detection={gestures:[],current:null,previous:null,stopped:!1,startDetect:function(t,e){this.current||(this.stopped=!1,this.current={inst:t,startEvent:i.utils.extend({},e),lastEvent:!1,name:""},this.detect(e))},detect:function(t){if(this.current&&!this.stopped){t=this.extendEventData(t);for(var e=this.current.inst.options,n=0,r=this.gestures.length;r>n;n++){var o=this.gestures[n];if(!this.stopped&&e[o.name]!==!1&&o.handler.call(o,t,this.current.inst)===!1){this.stopDetect();break}}return this.current&&(this.current.lastEvent=t),t.eventType==i.EVENT_END&&!t.touches.length-1&&this.stopDetect(),t}},stopDetect:function(){this.previous=i.utils.extend({},this.current),this.current=null,this.stopped=!0},extendEventData:function(t){var e=this.current.startEvent;if(e&&(t.touches.length!=e.touches.length||t.touches===e.touches)){e.touches=[];for(var n=0,r=t.touches.length;r>n;n++)e.touches.push(i.utils.extend({},t.touches[n]))}var o=t.timeStamp-e.timeStamp,s=t.center.pageX-e.center.pageX,a=t.center.pageY-e.center.pageY,c=i.utils.getVelocity(o,s,a);return i.utils.extend(t,{deltaTime:o,deltaX:s,deltaY:a,velocityX:c.x,velocityY:c.y,distance:i.utils.getDistance(e.center,t.center),angle:i.utils.getAngle(e.center,t.center),direction:i.utils.getDirection(e.center,t.center),scale:i.utils.getScale(e.touches,t.touches),rotation:i.utils.getRotation(e.touches,t.touches),startEvent:e}),t},register:function(t){var n=t.defaults||{};return n[t.name]===e&&(n[t.name]=!0),i.utils.extend(i.defaults,n,!0),t.index=t.index||1e3,this.gestures.push(t),this.gestures.sort(function(t,e){return t.index<e.index?-1:t.index>e.index?1:0}),this.gestures}},i.gestures=i.gestures||{},i.gestures.Hold={name:"hold",index:10,defaults:{hold_timeout:500,hold_threshold:1},timer:null,handler:function(t,e){switch(t.eventType){case i.EVENT_START:clearTimeout(this.timer),i.detection.current.name=this.name,this.timer=setTimeout(function(){"hold"==i.detection.current.name&&e.trigger("hold",t)},e.options.hold_timeout);break;case i.EVENT_MOVE:t.distance>e.options.hold_threshold&&clearTimeout(this.timer);break;case i.EVENT_END:clearTimeout(this.timer)}}},i.gestures.Tap={name:"tap",index:100,defaults:{tap_max_touchtime:250,tap_max_distance:10,tap_always:!0,doubletap_distance:20,doubletap_interval:300},handler:function(t,e){if(t.eventType==i.EVENT_END){var n=i.detection.previous,r=!1;if(t.deltaTime>e.options.tap_max_touchtime||t.distance>e.options.tap_max_distance)return;n&&"tap"==n.name&&t.timeStamp-n.lastEvent.timeStamp<e.options.doubletap_interval&&t.distance<e.options.doubletap_distance&&(e.trigger("doubletap",t),r=!0),(!r||e.options.tap_always)&&(i.detection.current.name="tap",e.trigger(i.detection.current.name,t))}}},i.gestures.Swipe={name:"swipe",index:40,defaults:{swipe_max_touches:1,swipe_velocity:.7},handler:function(t,e){if(t.eventType==i.EVENT_END){if(e.options.swipe_max_touches>0&&t.touches.length>e.options.swipe_max_touches)return;(t.velocityX>e.options.swipe_velocity||t.velocityY>e.options.swipe_velocity)&&(e.trigger(this.name,t),e.trigger(this.name+t.direction,t))}}},i.gestures.Drag={name:"drag",index:50,defaults:{drag_min_distance:10,drag_max_touches:1,drag_block_horizontal:!1,drag_block_vertical:!1,drag_lock_to_axis:!1,drag_lock_min_distance:25},triggered:!1,handler:function(t,n){if(i.detection.current.name!=this.name&&this.triggered)return n.trigger(this.name+"end",t),this.triggered=!1,e;if(!(n.options.drag_max_touches>0&&t.touches.length>n.options.drag_max_touches))switch(t.eventType){case i.EVENT_START:this.triggered=!1;break;case i.EVENT_MOVE:if(t.distance<n.options.drag_min_distance&&i.detection.current.name!=this.name)return;i.detection.current.name=this.name,(i.detection.current.lastEvent.drag_locked_to_axis||n.options.drag_lock_to_axis&&n.options.drag_lock_min_distance<=t.distance)&&(t.drag_locked_to_axis=!0);var r=i.detection.current.lastEvent.direction;t.drag_locked_to_axis&&r!==t.direction&&(t.direction=i.utils.isVertical(r)?0>t.deltaY?i.DIRECTION_UP:i.DIRECTION_DOWN:0>t.deltaX?i.DIRECTION_LEFT:i.DIRECTION_RIGHT),this.triggered||(n.trigger(this.name+"start",t),this.triggered=!0),n.trigger(this.name,t),n.trigger(this.name+t.direction,t),(n.options.drag_block_vertical&&i.utils.isVertical(t.direction)||n.options.drag_block_horizontal&&!i.utils.isVertical(t.direction))&&t.preventDefault();break;case i.EVENT_END:this.triggered&&n.trigger(this.name+"end",t),this.triggered=!1}}},i.gestures.Transform={name:"transform",index:45,defaults:{transform_min_scale:.01,transform_min_rotation:1,transform_always_block:!1},triggered:!1,handler:function(t,n){if(i.detection.current.name!=this.name&&this.triggered)return n.trigger(this.name+"end",t),this.triggered=!1,e;if(!(2>t.touches.length))switch(n.options.transform_always_block&&t.preventDefault(),t.eventType){case i.EVENT_START:this.triggered=!1;break;case i.EVENT_MOVE:var r=Math.abs(1-t.scale),o=Math.abs(t.rotation);if(n.options.transform_min_scale>r&&n.options.transform_min_rotation>o)return;i.detection.current.name=this.name,this.triggered||(n.trigger(this.name+"start",t),this.triggered=!0),n.trigger(this.name,t),o>n.options.transform_min_rotation&&n.trigger("rotate",t),r>n.options.transform_min_scale&&(n.trigger("pinch",t),n.trigger("pinch"+(1>t.scale?"in":"out"),t));break;case i.EVENT_END:this.triggered&&n.trigger(this.name+"end",t),this.triggered=!1}}},i.gestures.Touch={name:"touch",index:-1/0,defaults:{prevent_default:!1,prevent_mouseevents:!1},handler:function(t,n){return n.options.prevent_mouseevents&&t.pointerType==i.POINTER_MOUSE?(t.stopDetect(),e):(n.options.prevent_default&&t.preventDefault(),t.eventType==i.EVENT_START&&n.trigger(this.name,t),e)}},i.gestures.Release={name:"release",index:1/0,handler:function(t,e){t.eventType==i.EVENT_END&&e.trigger(this.name,t)}},"object"==typeof module&&"object"==typeof module.exports?module.exports=i:(t.Hammer=i,"function"==typeof t.define&&t.define.amd&&t.define("hammer",[],function(){return i}))})(this),function(t,e){"use strict";t!==e&&(Hammer.event.bindDom=function(n,i,r){t(n).on(i,function(t){var n=t.originalEvent||t;n.pageX===e&&(n.pageX=t.pageX,n.pageY=t.pageY),n.target||(n.target=t.target),n.which===e&&(n.which=n.button),n.preventDefault||(n.preventDefault=t.preventDefault),n.stopPropagation||(n.stopPropagation=t.stopPropagation),r.call(this,n)})},Hammer.Instance.prototype.on=function(e,n){return t(this.element).on(e,n)},Hammer.Instance.prototype.off=function(e,n){return t(this.element).off(e,n)},Hammer.Instance.prototype.trigger=function(e,n){var i=t(this.element);return i.has(n.target).length&&(i=t(n.target)),i.trigger({type:e,gesture:n})},t.fn.hammer=function(e){return this.each(function(){var n=t(this),i=n.data("hammer");i?i&&e&&Hammer.utils.extend(i.options,e):n.data("hammer",new Hammer(this,e||{}))})})}(window.jQuery||window.Zepto);
/*!
	Colorbox v1.5.5 - 2014-03-13
	jQuery lightbox and modal window plugin
	(c) 2014 Jack Moore - http://www.jacklmoore.com/colorbox
	license: http://www.opensource.org/licenses/mit-license.php
*/
(function(t,e,i){function n(i,n,o){var r=e.createElement(i);return n&&(r.id=Z+n),o&&(r.style.cssText=o),t(r)}function o(){return i.innerHeight?i.innerHeight:t(i).height()}function r(e,i){i!==Object(i)&&(i={}),this.cache={},this.el=e,this.value=function(e){var n;return void 0===this.cache[e]&&(n=t(this.el).attr("data-cbox-"+e),void 0!==n?this.cache[e]=n:void 0!==i[e]?this.cache[e]=i[e]:void 0!==X[e]&&(this.cache[e]=X[e])),this.cache[e]},this.get=function(e){var i=this.value(e);return t.isFunction(i)?i.call(this.el,this):i}}function h(t){var e=E.length,i=(z+t)%e;return 0>i?e+i:i}function s(t,e){return Math.round((/%/.test(t)?("x"===e?W.width():o())/100:1)*parseInt(t,10))}function a(t,e){return t.get("photo")||t.get("photoRegex").test(e)}function l(t,e){return t.get("retinaUrl")&&i.devicePixelRatio>1?e.replace(t.get("photoRegex"),t.get("retinaSuffix")):e}function d(t){"contains"in x[0]&&!x[0].contains(t.target)&&t.target!==v[0]&&(t.stopPropagation(),x.focus())}function c(t){c.str!==t&&(x.add(v).removeClass(c.str).addClass(t),c.str=t)}function g(){z=0,rel&&"nofollow"!==rel?(E=t("."+te).filter(function(){var e=t.data(this,Y),i=new r(this,e);return i.get("rel")===rel}),z=E.index(_.el),-1===z&&(E=E.add(_.el),z=E.length-1)):E=t(_.el)}function u(i){t(e).trigger(i),se.triggerHandler(i)}function f(i){var o;G||(o=t(i).data("colorbox"),_=new r(i,o),rel=_.get("rel"),g(),$||($=q=!0,c(_.get("className")),x.css({visibility:"hidden",display:"block",opacity:""}),L=n(ae,"LoadedContent","width:0; height:0; overflow:hidden; visibility:hidden"),b.css({width:"",height:""}).append(L),D=T.height()+k.height()+b.outerHeight(!0)-b.height(),j=C.width()+H.width()+b.outerWidth(!0)-b.width(),A=L.outerHeight(!0),N=L.outerWidth(!0),_.w=s(_.get("initialWidth"),"x"),_.h=s(_.get("initialHeight"),"y"),L.css({width:"",height:_.h}),J.position(),u(ee),_.get("onOpen"),O.add(R).hide(),x.focus(),_.get("trapFocus")&&e.addEventListener&&(e.addEventListener("focus",d,!0),se.one(re,function(){e.removeEventListener("focus",d,!0)})),_.get("returnFocus")&&se.one(re,function(){t(_.el).focus()})),v.css({opacity:parseFloat(_.get("opacity"))||"",cursor:_.get("overlayClose")?"pointer":"",visibility:"visible"}).show(),_.get("closeButton")?B.html(_.get("close")).appendTo(b):B.appendTo("<div/>"),w())}function p(){!x&&e.body&&(V=!1,W=t(i),x=n(ae).attr({id:Y,"class":t.support.opacity===!1?Z+"IE":"",role:"dialog",tabindex:"-1"}).hide(),v=n(ae,"Overlay").hide(),M=t([n(ae,"LoadingOverlay")[0],n(ae,"LoadingGraphic")[0]]),y=n(ae,"Wrapper"),b=n(ae,"Content").append(R=n(ae,"Title"),F=n(ae,"Current"),P=t('<button type="button"/>').attr({id:Z+"Previous"}),K=t('<button type="button"/>').attr({id:Z+"Next"}),I=n("button","Slideshow"),M),B=t('<button type="button"/>').attr({id:Z+"Close"}),y.append(n(ae).append(n(ae,"TopLeft"),T=n(ae,"TopCenter"),n(ae,"TopRight")),n(ae,!1,"clear:left").append(C=n(ae,"MiddleLeft"),b,H=n(ae,"MiddleRight")),n(ae,!1,"clear:left").append(n(ae,"BottomLeft"),k=n(ae,"BottomCenter"),n(ae,"BottomRight"))).find("div div").css({"float":"left"}),S=n(ae,!1,"position:absolute; width:9999px; visibility:hidden; display:none; max-width:none;"),O=K.add(P).add(F).add(I),t(e.body).append(v,x.append(y,S)))}function m(){function i(t){t.which>1||t.shiftKey||t.altKey||t.metaKey||t.ctrlKey||(t.preventDefault(),f(this))}return x?(V||(V=!0,K.click(function(){J.next()}),P.click(function(){J.prev()}),B.click(function(){J.close()}),v.click(function(){_.get("overlayClose")&&J.close()}),t(e).bind("keydown."+Z,function(t){var e=t.keyCode;$&&_.get("escKey")&&27===e&&(t.preventDefault(),J.close()),$&&_.get("arrowKey")&&E[1]&&!t.altKey&&(37===e?(t.preventDefault(),P.click()):39===e&&(t.preventDefault(),K.click()))}),t.isFunction(t.fn.on)?t(e).on("click."+Z,"."+te,i):t("."+te).live("click."+Z,i)),!0):!1}function w(){var o,r,h,d=J.prep,c=++le;q=!0,U=!1,u(he),u(ie),_.get("onLoad"),_.h=_.get("height")?s(_.get("height"),"y")-A-D:_.get("innerHeight")&&s(_.get("innerHeight"),"y"),_.w=_.get("width")?s(_.get("width"),"x")-N-j:_.get("innerWidth")&&s(_.get("innerWidth"),"x"),_.mw=_.w,_.mh=_.h,_.get("maxWidth")&&(_.mw=s(_.get("maxWidth"),"x")-N-j,_.mw=_.w&&_.w<_.mw?_.w:_.mw),_.get("maxHeight")&&(_.mh=s(_.get("maxHeight"),"y")-A-D,_.mh=_.h&&_.h<_.mh?_.h:_.mh),o=_.get("href"),Q=setTimeout(function(){M.show()},100),_.get("inline")?(h=n(ae).hide().insertBefore(t(o)[0]),se.one(he,function(){h.replaceWith(L.children())}),d(t(o))):_.get("iframe")?d(" "):_.get("html")?d(_.get("html")):a(_,o)?(o=l(_,o),U=e.createElement("img"),t(U).addClass(Z+"Photo").bind("error",function(){d(n(ae,"Error").html(_.get("imgError")))}).one("load",function(){var e;c===le&&(t.each(["alt","longdesc","aria-describedby"],function(e,i){var n=t(_.el).attr(i)||t(_.el).attr("data-"+i);n&&U.setAttribute(i,n)}),_.get("retinaImage")&&i.devicePixelRatio>1&&(U.height=U.height/i.devicePixelRatio,U.width=U.width/i.devicePixelRatio),_.get("scalePhotos")&&(r=function(){U.height-=U.height*e,U.width-=U.width*e},_.mw&&U.width>_.mw&&(e=(U.width-_.mw)/U.width,r()),_.mh&&U.height>_.mh&&(e=(U.height-_.mh)/U.height,r())),_.h&&(U.style.marginTop=Math.max(_.mh-U.height,0)/2+"px"),E[1]&&(_.get("loop")||E[z+1])&&(U.style.cursor="pointer",U.onclick=function(){J.next()}),U.style.width=U.width+"px",U.style.height=U.height+"px",setTimeout(function(){d(U)},1))}),setTimeout(function(){U.src=o},1)):o&&S.load(o,_.get("data"),function(e,i){c===le&&d("error"===i?n(ae,"Error").html(_.get("xhrError")):t(this).contents())})}var v,x,y,b,T,C,H,k,E,W,L,S,M,R,F,I,K,P,B,O,_,D,j,A,N,z,U,$,q,G,Q,J,V,X={html:!1,photo:!1,iframe:!1,inline:!1,transition:"elastic",speed:300,fadeOut:300,width:!1,initialWidth:"600",innerWidth:!1,maxWidth:!1,height:!1,initialHeight:"450",innerHeight:!1,maxHeight:!1,scalePhotos:!0,scrolling:!0,opacity:.9,preloading:!0,className:!1,overlayClose:!0,escKey:!0,arrowKey:!0,top:!1,bottom:!1,left:!1,right:!1,fixed:!1,data:void 0,closeButton:!0,fastIframe:!0,open:!1,reposition:!0,loop:!0,slideshow:!1,slideshowAuto:!0,slideshowSpeed:2500,slideshowStart:"start slideshow",slideshowStop:"stop slideshow",photoRegex:/\.(gif|png|jp(e|g|eg)|bmp|ico|webp|jxr|svg)((#|\?).*)?$/i,retinaImage:!1,retinaUrl:!1,retinaSuffix:"@2x.$1",current:"image {current} of {total}",previous:"previous",next:"next",close:"close",xhrError:"This content failed to load.",imgError:"This image failed to load.",returnFocus:!0,trapFocus:!0,onOpen:!1,onLoad:!1,onComplete:!1,onCleanup:!1,onClosed:!1,rel:function(){return this.rel},href:function(){return t(this).attr("href")},title:function(){return this.title}},Y="colorbox",Z="cbox",te=Z+"Element",ee=Z+"_open",ie=Z+"_load",ne=Z+"_complete",oe=Z+"_cleanup",re=Z+"_closed",he=Z+"_purge",se=t("<a/>"),ae="div",le=0,de={},ce=function(){function t(){clearTimeout(h)}function e(){(_.get("loop")||E[z+1])&&(t(),h=setTimeout(J.next,_.get("slideshowSpeed")))}function i(){I.html(_.get("slideshowStop")).unbind(a).one(a,n),se.bind(ne,e).bind(ie,t),x.removeClass(s+"off").addClass(s+"on")}function n(){t(),se.unbind(ne,e).unbind(ie,t),I.html(_.get("slideshowStart")).unbind(a).one(a,function(){J.next(),i()}),x.removeClass(s+"on").addClass(s+"off")}function o(){r=!1,I.hide(),t(),se.unbind(ne,e).unbind(ie,t),x.removeClass(s+"off "+s+"on")}var r,h,s=Z+"Slideshow_",a="click."+Z;return function(){r?_.get("slideshow")||(se.unbind(oe,o),o()):_.get("slideshow")&&E[1]&&(r=!0,se.one(oe,o),_.get("slideshowAuto")?i():n(),I.show())}}();t.colorbox||(t(p),J=t.fn[Y]=t[Y]=function(e,i){var n,o=this;if(e=e||{},t.isFunction(o))o=t("<a/>"),e.open=!0;else if(!o[0])return o;return o[0]?(p(),m()&&(i&&(e.onComplete=i),o.each(function(){var i=t.data(this,Y)||{};t.data(this,Y,t.extend(i,e))}).addClass(te),n=new r(o[0],e),n.get("open")&&f(o[0])),o):o},J.position=function(e,i){function n(){T[0].style.width=k[0].style.width=b[0].style.width=parseInt(x[0].style.width,10)-j+"px",b[0].style.height=C[0].style.height=H[0].style.height=parseInt(x[0].style.height,10)-D+"px"}var r,h,a,l=0,d=0,c=x.offset();if(W.unbind("resize."+Z),x.css({top:-9e4,left:-9e4}),h=W.scrollTop(),a=W.scrollLeft(),_.get("fixed")?(c.top-=h,c.left-=a,x.css({position:"fixed"})):(l=h,d=a,x.css({position:"absolute"})),d+=_.get("right")!==!1?Math.max(W.width()-_.w-N-j-s(_.get("right"),"x"),0):_.get("left")!==!1?s(_.get("left"),"x"):Math.round(Math.max(W.width()-_.w-N-j,0)/2),l+=_.get("bottom")!==!1?Math.max(o()-_.h-A-D-s(_.get("bottom"),"y"),0):_.get("top")!==!1?s(_.get("top"),"y"):Math.round(Math.max(o()-_.h-A-D,0)/2),x.css({top:c.top,left:c.left,visibility:"visible"}),y[0].style.width=y[0].style.height="9999px",r={width:_.w+N+j,height:_.h+A+D,top:l,left:d},e){var g=0;t.each(r,function(t){return r[t]!==de[t]?(g=e,void 0):void 0}),e=g}de=r,e||x.css(r),x.dequeue().animate(r,{duration:e||0,complete:function(){n(),q=!1,y[0].style.width=_.w+N+j+"px",y[0].style.height=_.h+A+D+"px",_.get("reposition")&&setTimeout(function(){W.bind("resize."+Z,J.position)},1),i&&i()},step:n})},J.resize=function(t){var e;$&&(t=t||{},t.width&&(_.w=s(t.width,"x")-N-j),t.innerWidth&&(_.w=s(t.innerWidth,"x")),L.css({width:_.w}),t.height&&(_.h=s(t.height,"y")-A-D),t.innerHeight&&(_.h=s(t.innerHeight,"y")),t.innerHeight||t.height||(e=L.scrollTop(),L.css({height:"auto"}),_.h=L.height()),L.css({height:_.h}),e&&L.scrollTop(e),J.position("none"===_.get("transition")?0:_.get("speed")))},J.prep=function(i){function o(){return _.w=_.w||L.width(),_.w=_.mw&&_.mw<_.w?_.mw:_.w,_.w}function s(){return _.h=_.h||L.height(),_.h=_.mh&&_.mh<_.h?_.mh:_.h,_.h}if($){var d,g="none"===_.get("transition")?0:_.get("speed");L.remove(),L=n(ae,"LoadedContent").append(i),L.hide().appendTo(S.show()).css({width:o(),overflow:_.get("scrolling")?"auto":"hidden"}).css({height:s()}).prependTo(b),S.hide(),t(U).css({"float":"none"}),c(_.get("className")),d=function(){function i(){t.support.opacity===!1&&x[0].style.removeAttribute("filter")}var n,o,s=E.length;$&&(o=function(){clearTimeout(Q),M.hide(),u(ne),_.get("onComplete")},R.html(_.get("title")).show(),L.show(),s>1?("string"==typeof _.get("current")&&F.html(_.get("current").replace("{current}",z+1).replace("{total}",s)).show(),K[_.get("loop")||s-1>z?"show":"hide"]().html(_.get("next")),P[_.get("loop")||z?"show":"hide"]().html(_.get("previous")),ce(),_.get("preloading")&&t.each([h(-1),h(1)],function(){var i,n=E[this],o=new r(n,t.data(n,Y)),h=o.get("href");h&&a(o,h)&&(h=l(o,h),i=e.createElement("img"),i.src=h)})):O.hide(),_.get("iframe")?(n=e.createElement("iframe"),"frameBorder"in n&&(n.frameBorder=0),"allowTransparency"in n&&(n.allowTransparency="true"),_.get("scrolling")||(n.scrolling="no"),t(n).attr({src:_.get("href"),name:(new Date).getTime(),"class":Z+"Iframe",allowFullScreen:!0}).one("load",o).appendTo(L),se.one(he,function(){n.src="//about:blank"}),_.get("fastIframe")&&t(n).trigger("load")):o(),"fade"===_.get("transition")?x.fadeTo(g,1,i):i())},"fade"===_.get("transition")?x.fadeTo(g,0,function(){J.position(0,d)}):J.position(g,d)}},J.next=function(){!q&&E[1]&&(_.get("loop")||E[z+1])&&(z=h(1),f(E[z]))},J.prev=function(){!q&&E[1]&&(_.get("loop")||z)&&(z=h(-1),f(E[z]))},J.close=function(){$&&!G&&(G=!0,$=!1,u(oe),_.get("onCleanup"),W.unbind("."+Z),v.fadeTo(_.get("fadeOut")||0,0),x.stop().fadeTo(_.get("fadeOut")||0,0,function(){x.hide(),v.hide(),u(he),L.remove(),setTimeout(function(){G=!1,u(re),_.get("onClosed")},1)}))},J.remove=function(){x&&(x.stop(),t.colorbox.close(),x.stop().remove(),v.remove(),G=!1,x=null,t("."+te).removeData(Y).removeClass(te),t(e).unbind("click."+Z))},J.element=function(){return t(_.el)},J.settings=X)})(jQuery,document,window);
(function($){var pluginName="carousel",initSelector="."+pluginName,transitionAttr="data-transition",transitioningClass=pluginName+"-transitioning",itemClass=pluginName+"-item",activeClass=pluginName+"-active",prevClass=pluginName+"-item-prev",nextClass=pluginName+"-item-next",inClass=pluginName+"-in",outClass=pluginName+"-out",navClass=pluginName+"-nav",cssTransitionsSupport=function(){var prefixes="webkit Moz O Ms".split(" "),supported=false,property;while(prefixes.length){property=prefixes.shift()+"Transition";if(property in document.documentElement.style!==undefined&&property in document.documentElement.style!==false){supported=true;break}}return supported}(),methods={_create:function(){$(this).trigger("beforecreate."+pluginName)[pluginName]("_init")[pluginName]("_addNextPrev").trigger("create."+pluginName)},_init:function(){var trans=$(this).attr(transitionAttr);if(!trans){cssTransitionsSupport=false}$(this).addClass(pluginName+" "+(trans?pluginName+"-"+trans:"")+" ").children().addClass(itemClass).first().addClass(activeClass);$(this)[pluginName]("_addNextPrevClasses")},_addNextPrevClasses:function(){var $items=$(this).find("."+itemClass),$active=$items.filter("."+activeClass),$next=$active.next("."+itemClass),$prev=$active.prev("."+itemClass);if(!$next.length){$next=$items.first().not("."+activeClass)}if(!$prev.length){$prev=$items.last().not("."+activeClass)}$items.removeClass(prevClass+" "+nextClass);$prev.addClass(prevClass);$next.addClass(nextClass)},next:function(){$(this)[pluginName]("goTo","+1")},prev:function(){$(this)[pluginName]("goTo","-1")},goTo:function(num){var $self=$(this),trans=$self.attr(transitionAttr),reverseClass=" "+pluginName+"-"+trans+"-reverse";$(this).find("."+itemClass).removeClass([outClass,inClass,reverseClass].join(" "));var $from=$(this).find("."+activeClass),prevs=$from.index(),activeNum=(prevs<0?0:prevs)+1,nextNum=typeof num==="number"?num:activeNum+parseFloat(num),$to=$(this).find(".carousel-item").eq(nextNum-1),reverse=typeof num==="string"&&!parseFloat(num)||nextNum>activeNum?"":reverseClass;if(!$to.length){$to=$(this).find("."+itemClass)[reverse.length?"last":"first"]()}if(cssTransitionsSupport){$self[pluginName]("_transitionStart",$from,$to,reverse)}else{$to.addClass(activeClass);$self[pluginName]("_transitionEnd",$from,$to,reverse)}$self.trigger("goto."+pluginName,$to)},update:function(){$(this).children().not("."+navClass).addClass(itemClass);return $(this).trigger("update."+pluginName)},_transitionStart:function($from,$to,reverseClass){var $self=$(this);$to.one(navigator.userAgent.indexOf("AppleWebKit")>-1?"webkitTransitionEnd":"transitionend otransitionend",function(){$self[pluginName]("_transitionEnd",$from,$to,reverseClass)});$(this).addClass(reverseClass);$from.addClass(outClass);$to.addClass(inClass)},_transitionEnd:function($from,$to,reverseClass){$(this).removeClass(reverseClass);$from.removeClass(outClass+" "+activeClass);$to.removeClass(inClass).addClass(activeClass);$(this)[pluginName]("_addNextPrevClasses")},_bindEventListeners:function(){var $elem=$(this).bind("click",function(e){var targ=$(e.target).closest("a[href='#next'],a[href='#prev']");if(targ.length){$elem[pluginName](targ.is("[href='#next']")?"next":"prev");e.preventDefault()}});return this},_addNextPrev:function(){return $(this).append("<nav class='"+navClass+"'><a href='#prev' class='prev' aria-hidden='true' title='Previous'>Prev</a><a href='#next' class='next' aria-hidden='true' title='Next'>Next</a></nav>")[pluginName]("_bindEventListeners")},destroy:function(){}};$.fn[pluginName]=function(arrg,a,b,c){return this.each(function(){if(arrg&&typeof arrg==="string"){return $.fn[pluginName].prototype[arrg].call(this,a,b,c)}if($(this).data(pluginName+"data")){return $(this)}$(this).data(pluginName+"active",true);$.fn[pluginName].prototype._create.call(this)})};$.extend($.fn[pluginName].prototype,methods)})(jQuery);(function($){var pluginName="carousel",initSelector="."+pluginName,noTrans=pluginName+"-no-transition",iOS=/iPhone|iPad|iPod/.test(navigator.platform)&&navigator.userAgent.indexOf("AppleWebKit")>-1,touchMethods={_dragBehavior:function(){var $self=$(this),origin,data={},xPerc,yPerc,setData=function(e){var touches=e.touches||e.originalEvent.touches,$elem=$(e.target).closest(initSelector);if(e.type==="touchstart"){origin={x:touches[0].pageX,y:touches[0].pageY}}if(touches[0]&&touches[0].pageX){data.touches=touches;data.deltaX=touches[0].pageX-origin.x;data.deltaY=touches[0].pageY-origin.y;data.w=$elem.width();data.h=$elem.height();data.xPercent=data.deltaX/data.w;data.yPercent=data.deltaY/data.h;data.srcEvent=e}},emitEvents=function(e){setData(e);if(data.touches.length===1){$(e.target).closest(initSelector).trigger("drag"+e.type.split("touch")[1],data)}};$(this).bind("touchstart",function(e){$(this).addClass(noTrans);emitEvents(e)}).bind("touchmove",function(e){setData(e);emitEvents(e);if(!iOS){e.preventDefault();window.scrollBy(0,-data.deltaY)}}).bind("touchend",function(e){$(this).removeClass(noTrans);emitEvents(e)})}};$.extend($.fn[pluginName].prototype,touchMethods);$(document).on("create."+pluginName,initSelector,function(){$(this)[pluginName]("_dragBehavior")})})(jQuery);(function($){var pluginName="carousel",initSelector="."+pluginName,activeClass=pluginName+"-active",itemClass=pluginName+"-item",dragThreshold=function(deltaX){return Math.abs(deltaX)>4},getActiveSlides=function($carousel,deltaX){var $from=$carousel.find("."+pluginName+"-active"),activeNum=$from.prevAll().length+1,forward=deltaX<0,nextNum=activeNum+(forward?1:-1),$to=$carousel.find("."+itemClass).eq(nextNum-1);if(!$to.length){$to=$carousel.find("."+itemClass)[forward?"first":"last"]()}return[$from,$to]};$(document).on("dragmove",initSelector,function(e,data){if(!dragThreshold(data.deltaX)){return}var activeSlides=getActiveSlides($(this),data.deltaX);activeSlides[0].css("left",data.deltaX+"px");activeSlides[1].css("left",data.deltaX<0?data.w+data.deltaX+"px":-data.w+data.deltaX+"px")}).on("dragend",initSelector,function(e,data){if(!dragThreshold(data.deltaX)){return}var activeSlides=getActiveSlides($(this),data.deltaX),newSlide=Math.abs(data.deltaX)>45;$(this).one(navigator.userAgent.indexOf("AppleWebKit")?"webkitTransitionEnd":"transitionEnd",function(){activeSlides[0].add(activeSlides[1]).css("left","");$(this).trigger("goto."+pluginName,activeSlides[1])});if(newSlide){activeSlides[0].removeClass(activeClass).css("left",data.deltaX>0?data.w+"px":-data.w+"px");activeSlides[1].addClass(activeClass).css("left",0)}else{activeSlides[0].css("left",0);activeSlides[1].css("left",data.deltaX>0?-data.w+"px":data.w+"px")}})})(jQuery);(function($,undefined){var pluginName="carousel",initSelector="."+pluginName+"[data-paginate]",paginationClass=pluginName+"-pagination",activeClass=pluginName+"-active-page",paginationMethods={_createPagination:function(){var nav=$(this).find("."+pluginName+"-nav"),items=$(this).find("."+pluginName+"-item"),pNav=$("<ol class='"+paginationClass+"'></ol>"),num,thumb,content;nav.find("."+paginationClass).remove();items.each(function(i){num=i+1;thumb=$(this).attr("data-thumb");content=num;if(thumb){content="<img src='"+thumb+"' alt=''>"}pNav.append("<li><a href='#"+num+"' title='Go to slide "+num+"'>"+content+"</a>")});if(thumb){pNav.addClass(pluginName+"-nav-thumbs")}nav.addClass(pluginName+"-nav-paginated").find("a").first().after(pNav)},_bindPaginationEvents:function(){$(this).bind("click",function(e){var pagLink=$(e.target);if(e.target.nodeName==="IMG"){pagLink=pagLink.parent()}pagLink=pagLink.closest("a");var href=pagLink.attr("href");if(pagLink.closest("."+paginationClass).length&&href){$(this)[pluginName]("goTo",parseFloat(href.split("#")[1]));e.preventDefault()}}).bind("goto."+pluginName,function(e,to){var index=to?$(to).index():0;$(this).find("ol."+paginationClass+" li").removeClass(activeClass).eq(index).addClass(activeClass)}).trigger("goto."+pluginName)}};$.extend($.fn[pluginName].prototype,paginationMethods);$(document).on("create."+pluginName,initSelector,function(){$(this)[pluginName]("_createPagination")[pluginName]("_bindPaginationEvents")}).on("update."+pluginName,initSelector,function(){$(this)[pluginName]("_createPagination")})})(jQuery);(function($){$(function(){$(".carousel").carousel()})})(jQuery);(function($,undefined){var pluginName="carousel",interval=4e3,autoPlayMethods={play:function(){var $self=$(this),intAttr=$self.attr("data-interval"),thisInt=parseFloat(intAttr)||interval;return $self.data("timer",setInterval(function(){$self[pluginName]("next")},thisInt))},stop:function(){clearTimeout($(this).data("timer"))},_bindStopListener:function(){return $(this).bind("mousedown",function(){$(this)[pluginName]("stop")})},_initAutoPlay:function(){var autoplayAttr=$(this).attr("data-autoplay"),autoplay=typeof autoplayAttr!=="undefined"&&autoplayAttr.toLowerCase()!=="false";if(autoplay){$(this)[pluginName]("_bindStopListener")[pluginName]("play")}}};$.extend($.fn[pluginName].prototype,autoPlayMethods);$(document).bind("create."+pluginName,function(e){$(e.target)[pluginName]("_initAutoPlay")})})(jQuery);
//! moment.js
//! version : 2.9.0
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com
(function(a){function b(a,b,c){switch(arguments.length){case 2:return null!=a?a:b;case 3:return null!=a?a:null!=b?b:c;default:throw new Error("Implement me")}}function c(a,b){return Bb.call(a,b)}function d(){return{empty:!1,unusedTokens:[],unusedInput:[],overflow:-2,charsLeftOver:0,nullInput:!1,invalidMonth:null,invalidFormat:!1,userInvalidated:!1,iso:!1}}function e(a){vb.suppressDeprecationWarnings===!1&&"undefined"!=typeof console&&console.warn&&console.warn("Deprecation warning: "+a)}function f(a,b){var c=!0;return o(function(){return c&&(e(a),c=!1),b.apply(this,arguments)},b)}function g(a,b){sc[a]||(e(b),sc[a]=!0)}function h(a,b){return function(c){return r(a.call(this,c),b)}}function i(a,b){return function(c){return this.localeData().ordinal(a.call(this,c),b)}}function j(a,b){var c,d,e=12*(b.year()-a.year())+(b.month()-a.month()),f=a.clone().add(e,"months");return 0>b-f?(c=a.clone().add(e-1,"months"),d=(b-f)/(f-c)):(c=a.clone().add(e+1,"months"),d=(b-f)/(c-f)),-(e+d)}function k(a,b,c){var d;return null==c?b:null!=a.meridiemHour?a.meridiemHour(b,c):null!=a.isPM?(d=a.isPM(c),d&&12>b&&(b+=12),d||12!==b||(b=0),b):b}function l(){}function m(a,b){b!==!1&&H(a),p(this,a),this._d=new Date(+a._d),uc===!1&&(uc=!0,vb.updateOffset(this),uc=!1)}function n(a){var b=A(a),c=b.year||0,d=b.quarter||0,e=b.month||0,f=b.week||0,g=b.day||0,h=b.hour||0,i=b.minute||0,j=b.second||0,k=b.millisecond||0;this._milliseconds=+k+1e3*j+6e4*i+36e5*h,this._days=+g+7*f,this._months=+e+3*d+12*c,this._data={},this._locale=vb.localeData(),this._bubble()}function o(a,b){for(var d in b)c(b,d)&&(a[d]=b[d]);return c(b,"toString")&&(a.toString=b.toString),c(b,"valueOf")&&(a.valueOf=b.valueOf),a}function p(a,b){var c,d,e;if("undefined"!=typeof b._isAMomentObject&&(a._isAMomentObject=b._isAMomentObject),"undefined"!=typeof b._i&&(a._i=b._i),"undefined"!=typeof b._f&&(a._f=b._f),"undefined"!=typeof b._l&&(a._l=b._l),"undefined"!=typeof b._strict&&(a._strict=b._strict),"undefined"!=typeof b._tzm&&(a._tzm=b._tzm),"undefined"!=typeof b._isUTC&&(a._isUTC=b._isUTC),"undefined"!=typeof b._offset&&(a._offset=b._offset),"undefined"!=typeof b._pf&&(a._pf=b._pf),"undefined"!=typeof b._locale&&(a._locale=b._locale),Kb.length>0)for(c in Kb)d=Kb[c],e=b[d],"undefined"!=typeof e&&(a[d]=e);return a}function q(a){return 0>a?Math.ceil(a):Math.floor(a)}function r(a,b,c){for(var d=""+Math.abs(a),e=a>=0;d.length<b;)d="0"+d;return(e?c?"+":"":"-")+d}function s(a,b){var c={milliseconds:0,months:0};return c.months=b.month()-a.month()+12*(b.year()-a.year()),a.clone().add(c.months,"M").isAfter(b)&&--c.months,c.milliseconds=+b-+a.clone().add(c.months,"M"),c}function t(a,b){var c;return b=M(b,a),a.isBefore(b)?c=s(a,b):(c=s(b,a),c.milliseconds=-c.milliseconds,c.months=-c.months),c}function u(a,b){return function(c,d){var e,f;return null===d||isNaN(+d)||(g(b,"moment()."+b+"(period, number) is deprecated. Please use moment()."+b+"(number, period)."),f=c,c=d,d=f),c="string"==typeof c?+c:c,e=vb.duration(c,d),v(this,e,a),this}}function v(a,b,c,d){var e=b._milliseconds,f=b._days,g=b._months;d=null==d?!0:d,e&&a._d.setTime(+a._d+e*c),f&&pb(a,"Date",ob(a,"Date")+f*c),g&&nb(a,ob(a,"Month")+g*c),d&&vb.updateOffset(a,f||g)}function w(a){return"[object Array]"===Object.prototype.toString.call(a)}function x(a){return"[object Date]"===Object.prototype.toString.call(a)||a instanceof Date}function y(a,b,c){var d,e=Math.min(a.length,b.length),f=Math.abs(a.length-b.length),g=0;for(d=0;e>d;d++)(c&&a[d]!==b[d]||!c&&C(a[d])!==C(b[d]))&&g++;return g+f}function z(a){if(a){var b=a.toLowerCase().replace(/(.)s$/,"$1");a=lc[a]||mc[b]||b}return a}function A(a){var b,d,e={};for(d in a)c(a,d)&&(b=z(d),b&&(e[b]=a[d]));return e}function B(b){var c,d;if(0===b.indexOf("week"))c=7,d="day";else{if(0!==b.indexOf("month"))return;c=12,d="month"}vb[b]=function(e,f){var g,h,i=vb._locale[b],j=[];if("number"==typeof e&&(f=e,e=a),h=function(a){var b=vb().utc().set(d,a);return i.call(vb._locale,b,e||"")},null!=f)return h(f);for(g=0;c>g;g++)j.push(h(g));return j}}function C(a){var b=+a,c=0;return 0!==b&&isFinite(b)&&(c=b>=0?Math.floor(b):Math.ceil(b)),c}function D(a,b){return new Date(Date.UTC(a,b+1,0)).getUTCDate()}function E(a,b,c){return jb(vb([a,11,31+b-c]),b,c).week}function F(a){return G(a)?366:365}function G(a){return a%4===0&&a%100!==0||a%400===0}function H(a){var b;a._a&&-2===a._pf.overflow&&(b=a._a[Db]<0||a._a[Db]>11?Db:a._a[Eb]<1||a._a[Eb]>D(a._a[Cb],a._a[Db])?Eb:a._a[Fb]<0||a._a[Fb]>24||24===a._a[Fb]&&(0!==a._a[Gb]||0!==a._a[Hb]||0!==a._a[Ib])?Fb:a._a[Gb]<0||a._a[Gb]>59?Gb:a._a[Hb]<0||a._a[Hb]>59?Hb:a._a[Ib]<0||a._a[Ib]>999?Ib:-1,a._pf._overflowDayOfYear&&(Cb>b||b>Eb)&&(b=Eb),a._pf.overflow=b)}function I(b){return null==b._isValid&&(b._isValid=!isNaN(b._d.getTime())&&b._pf.overflow<0&&!b._pf.empty&&!b._pf.invalidMonth&&!b._pf.nullInput&&!b._pf.invalidFormat&&!b._pf.userInvalidated,b._strict&&(b._isValid=b._isValid&&0===b._pf.charsLeftOver&&0===b._pf.unusedTokens.length&&b._pf.bigHour===a)),b._isValid}function J(a){return a?a.toLowerCase().replace("_","-"):a}function K(a){for(var b,c,d,e,f=0;f<a.length;){for(e=J(a[f]).split("-"),b=e.length,c=J(a[f+1]),c=c?c.split("-"):null;b>0;){if(d=L(e.slice(0,b).join("-")))return d;if(c&&c.length>=b&&y(e,c,!0)>=b-1)break;b--}f++}return null}function L(a){var b=null;if(!Jb[a]&&Lb)try{b=vb.locale(),require("./locale/"+a),vb.locale(b)}catch(c){}return Jb[a]}function M(a,b){var c,d;return b._isUTC?(c=b.clone(),d=(vb.isMoment(a)||x(a)?+a:+vb(a))-+c,c._d.setTime(+c._d+d),vb.updateOffset(c,!1),c):vb(a).local()}function N(a){return a.match(/\[[\s\S]/)?a.replace(/^\[|\]$/g,""):a.replace(/\\/g,"")}function O(a){var b,c,d=a.match(Pb);for(b=0,c=d.length;c>b;b++)d[b]=rc[d[b]]?rc[d[b]]:N(d[b]);return function(e){var f="";for(b=0;c>b;b++)f+=d[b]instanceof Function?d[b].call(e,a):d[b];return f}}function P(a,b){return a.isValid()?(b=Q(b,a.localeData()),nc[b]||(nc[b]=O(b)),nc[b](a)):a.localeData().invalidDate()}function Q(a,b){function c(a){return b.longDateFormat(a)||a}var d=5;for(Qb.lastIndex=0;d>=0&&Qb.test(a);)a=a.replace(Qb,c),Qb.lastIndex=0,d-=1;return a}function R(a,b){var c,d=b._strict;switch(a){case"Q":return _b;case"DDDD":return bc;case"YYYY":case"GGGG":case"gggg":return d?cc:Tb;case"Y":case"G":case"g":return ec;case"YYYYYY":case"YYYYY":case"GGGGG":case"ggggg":return d?dc:Ub;case"S":if(d)return _b;case"SS":if(d)return ac;case"SSS":if(d)return bc;case"DDD":return Sb;case"MMM":case"MMMM":case"dd":case"ddd":case"dddd":return Wb;case"a":case"A":return b._locale._meridiemParse;case"x":return Zb;case"X":return $b;case"Z":case"ZZ":return Xb;case"T":return Yb;case"SSSS":return Vb;case"MM":case"DD":case"YY":case"GG":case"gg":case"HH":case"hh":case"mm":case"ss":case"ww":case"WW":return d?ac:Rb;case"M":case"D":case"d":case"H":case"h":case"m":case"s":case"w":case"W":case"e":case"E":return Rb;case"Do":return d?b._locale._ordinalParse:b._locale._ordinalParseLenient;default:return c=new RegExp($(Z(a.replace("\\","")),"i"))}}function S(a){a=a||"";var b=a.match(Xb)||[],c=b[b.length-1]||[],d=(c+"").match(jc)||["-",0,0],e=+(60*d[1])+C(d[2]);return"+"===d[0]?e:-e}function T(a,b,c){var d,e=c._a;switch(a){case"Q":null!=b&&(e[Db]=3*(C(b)-1));break;case"M":case"MM":null!=b&&(e[Db]=C(b)-1);break;case"MMM":case"MMMM":d=c._locale.monthsParse(b,a,c._strict),null!=d?e[Db]=d:c._pf.invalidMonth=b;break;case"D":case"DD":null!=b&&(e[Eb]=C(b));break;case"Do":null!=b&&(e[Eb]=C(parseInt(b.match(/\d{1,2}/)[0],10)));break;case"DDD":case"DDDD":null!=b&&(c._dayOfYear=C(b));break;case"YY":e[Cb]=vb.parseTwoDigitYear(b);break;case"YYYY":case"YYYYY":case"YYYYYY":e[Cb]=C(b);break;case"a":case"A":c._meridiem=b;break;case"h":case"hh":c._pf.bigHour=!0;case"H":case"HH":e[Fb]=C(b);break;case"m":case"mm":e[Gb]=C(b);break;case"s":case"ss":e[Hb]=C(b);break;case"S":case"SS":case"SSS":case"SSSS":e[Ib]=C(1e3*("0."+b));break;case"x":c._d=new Date(C(b));break;case"X":c._d=new Date(1e3*parseFloat(b));break;case"Z":case"ZZ":c._useUTC=!0,c._tzm=S(b);break;case"dd":case"ddd":case"dddd":d=c._locale.weekdaysParse(b),null!=d?(c._w=c._w||{},c._w.d=d):c._pf.invalidWeekday=b;break;case"w":case"ww":case"W":case"WW":case"d":case"e":case"E":a=a.substr(0,1);case"gggg":case"GGGG":case"GGGGG":a=a.substr(0,2),b&&(c._w=c._w||{},c._w[a]=C(b));break;case"gg":case"GG":c._w=c._w||{},c._w[a]=vb.parseTwoDigitYear(b)}}function U(a){var c,d,e,f,g,h,i;c=a._w,null!=c.GG||null!=c.W||null!=c.E?(g=1,h=4,d=b(c.GG,a._a[Cb],jb(vb(),1,4).year),e=b(c.W,1),f=b(c.E,1)):(g=a._locale._week.dow,h=a._locale._week.doy,d=b(c.gg,a._a[Cb],jb(vb(),g,h).year),e=b(c.w,1),null!=c.d?(f=c.d,g>f&&++e):f=null!=c.e?c.e+g:g),i=kb(d,e,f,h,g),a._a[Cb]=i.year,a._dayOfYear=i.dayOfYear}function V(a){var c,d,e,f,g=[];if(!a._d){for(e=X(a),a._w&&null==a._a[Eb]&&null==a._a[Db]&&U(a),a._dayOfYear&&(f=b(a._a[Cb],e[Cb]),a._dayOfYear>F(f)&&(a._pf._overflowDayOfYear=!0),d=fb(f,0,a._dayOfYear),a._a[Db]=d.getUTCMonth(),a._a[Eb]=d.getUTCDate()),c=0;3>c&&null==a._a[c];++c)a._a[c]=g[c]=e[c];for(;7>c;c++)a._a[c]=g[c]=null==a._a[c]?2===c?1:0:a._a[c];24===a._a[Fb]&&0===a._a[Gb]&&0===a._a[Hb]&&0===a._a[Ib]&&(a._nextDay=!0,a._a[Fb]=0),a._d=(a._useUTC?fb:eb).apply(null,g),null!=a._tzm&&a._d.setUTCMinutes(a._d.getUTCMinutes()-a._tzm),a._nextDay&&(a._a[Fb]=24)}}function W(a){var b;a._d||(b=A(a._i),a._a=[b.year,b.month,b.day||b.date,b.hour,b.minute,b.second,b.millisecond],V(a))}function X(a){var b=new Date;return a._useUTC?[b.getUTCFullYear(),b.getUTCMonth(),b.getUTCDate()]:[b.getFullYear(),b.getMonth(),b.getDate()]}function Y(b){if(b._f===vb.ISO_8601)return void ab(b);b._a=[],b._pf.empty=!0;var c,d,e,f,g,h=""+b._i,i=h.length,j=0;for(e=Q(b._f,b._locale).match(Pb)||[],c=0;c<e.length;c++)f=e[c],d=(h.match(R(f,b))||[])[0],d&&(g=h.substr(0,h.indexOf(d)),g.length>0&&b._pf.unusedInput.push(g),h=h.slice(h.indexOf(d)+d.length),j+=d.length),rc[f]?(d?b._pf.empty=!1:b._pf.unusedTokens.push(f),T(f,d,b)):b._strict&&!d&&b._pf.unusedTokens.push(f);b._pf.charsLeftOver=i-j,h.length>0&&b._pf.unusedInput.push(h),b._pf.bigHour===!0&&b._a[Fb]<=12&&(b._pf.bigHour=a),b._a[Fb]=k(b._locale,b._a[Fb],b._meridiem),V(b),H(b)}function Z(a){return a.replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g,function(a,b,c,d,e){return b||c||d||e})}function $(a){return a.replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&")}function _(a){var b,c,e,f,g;if(0===a._f.length)return a._pf.invalidFormat=!0,void(a._d=new Date(0/0));for(f=0;f<a._f.length;f++)g=0,b=p({},a),null!=a._useUTC&&(b._useUTC=a._useUTC),b._pf=d(),b._f=a._f[f],Y(b),I(b)&&(g+=b._pf.charsLeftOver,g+=10*b._pf.unusedTokens.length,b._pf.score=g,(null==e||e>g)&&(e=g,c=b));o(a,c||b)}function ab(a){var b,c,d=a._i,e=fc.exec(d);if(e){for(a._pf.iso=!0,b=0,c=hc.length;c>b;b++)if(hc[b][1].exec(d)){a._f=hc[b][0]+(e[6]||" ");break}for(b=0,c=ic.length;c>b;b++)if(ic[b][1].exec(d)){a._f+=ic[b][0];break}d.match(Xb)&&(a._f+="Z"),Y(a)}else a._isValid=!1}function bb(a){ab(a),a._isValid===!1&&(delete a._isValid,vb.createFromInputFallback(a))}function cb(a,b){var c,d=[];for(c=0;c<a.length;++c)d.push(b(a[c],c));return d}function db(b){var c,d=b._i;d===a?b._d=new Date:x(d)?b._d=new Date(+d):null!==(c=Mb.exec(d))?b._d=new Date(+c[1]):"string"==typeof d?bb(b):w(d)?(b._a=cb(d.slice(0),function(a){return parseInt(a,10)}),V(b)):"object"==typeof d?W(b):"number"==typeof d?b._d=new Date(d):vb.createFromInputFallback(b)}function eb(a,b,c,d,e,f,g){var h=new Date(a,b,c,d,e,f,g);return 1970>a&&h.setFullYear(a),h}function fb(a){var b=new Date(Date.UTC.apply(null,arguments));return 1970>a&&b.setUTCFullYear(a),b}function gb(a,b){if("string"==typeof a)if(isNaN(a)){if(a=b.weekdaysParse(a),"number"!=typeof a)return null}else a=parseInt(a,10);return a}function hb(a,b,c,d,e){return e.relativeTime(b||1,!!c,a,d)}function ib(a,b,c){var d=vb.duration(a).abs(),e=Ab(d.as("s")),f=Ab(d.as("m")),g=Ab(d.as("h")),h=Ab(d.as("d")),i=Ab(d.as("M")),j=Ab(d.as("y")),k=e<oc.s&&["s",e]||1===f&&["m"]||f<oc.m&&["mm",f]||1===g&&["h"]||g<oc.h&&["hh",g]||1===h&&["d"]||h<oc.d&&["dd",h]||1===i&&["M"]||i<oc.M&&["MM",i]||1===j&&["y"]||["yy",j];return k[2]=b,k[3]=+a>0,k[4]=c,hb.apply({},k)}function jb(a,b,c){var d,e=c-b,f=c-a.day();return f>e&&(f-=7),e-7>f&&(f+=7),d=vb(a).add(f,"d"),{week:Math.ceil(d.dayOfYear()/7),year:d.year()}}function kb(a,b,c,d,e){var f,g,h=fb(a,0,1).getUTCDay();return h=0===h?7:h,c=null!=c?c:e,f=e-h+(h>d?7:0)-(e>h?7:0),g=7*(b-1)+(c-e)+f+1,{year:g>0?a:a-1,dayOfYear:g>0?g:F(a-1)+g}}function lb(b){var c,d=b._i,e=b._f;return b._locale=b._locale||vb.localeData(b._l),null===d||e===a&&""===d?vb.invalid({nullInput:!0}):("string"==typeof d&&(b._i=d=b._locale.preparse(d)),vb.isMoment(d)?new m(d,!0):(e?w(e)?_(b):Y(b):db(b),c=new m(b),c._nextDay&&(c.add(1,"d"),c._nextDay=a),c))}function mb(a,b){var c,d;if(1===b.length&&w(b[0])&&(b=b[0]),!b.length)return vb();for(c=b[0],d=1;d<b.length;++d)b[d][a](c)&&(c=b[d]);return c}function nb(a,b){var c;return"string"==typeof b&&(b=a.localeData().monthsParse(b),"number"!=typeof b)?a:(c=Math.min(a.date(),D(a.year(),b)),a._d["set"+(a._isUTC?"UTC":"")+"Month"](b,c),a)}function ob(a,b){return a._d["get"+(a._isUTC?"UTC":"")+b]()}function pb(a,b,c){return"Month"===b?nb(a,c):a._d["set"+(a._isUTC?"UTC":"")+b](c)}function qb(a,b){return function(c){return null!=c?(pb(this,a,c),vb.updateOffset(this,b),this):ob(this,a)}}function rb(a){return 400*a/146097}function sb(a){return 146097*a/400}function tb(a){vb.duration.fn[a]=function(){return this._data[a]}}function ub(a){"undefined"==typeof ender&&(wb=zb.moment,zb.moment=a?f("Accessing Moment through the global scope is deprecated, and will be removed in an upcoming release.",vb):vb)}for(var vb,wb,xb,yb="2.9.0",zb="undefined"==typeof global||"undefined"!=typeof window&&window!==global.window?this:global,Ab=Math.round,Bb=Object.prototype.hasOwnProperty,Cb=0,Db=1,Eb=2,Fb=3,Gb=4,Hb=5,Ib=6,Jb={},Kb=[],Lb="undefined"!=typeof module&&module&&module.exports,Mb=/^\/?Date\((\-?\d+)/i,Nb=/(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/,Ob=/^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/,Pb=/(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Q|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,4}|x|X|zz?|ZZ?|.)/g,Qb=/(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g,Rb=/\d\d?/,Sb=/\d{1,3}/,Tb=/\d{1,4}/,Ub=/[+\-]?\d{1,6}/,Vb=/\d+/,Wb=/[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i,Xb=/Z|[\+\-]\d\d:?\d\d/gi,Yb=/T/i,Zb=/[\+\-]?\d+/,$b=/[\+\-]?\d+(\.\d{1,3})?/,_b=/\d/,ac=/\d\d/,bc=/\d{3}/,cc=/\d{4}/,dc=/[+-]?\d{6}/,ec=/[+-]?\d+/,fc=/^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,gc="YYYY-MM-DDTHH:mm:ssZ",hc=[["YYYYYY-MM-DD",/[+-]\d{6}-\d{2}-\d{2}/],["YYYY-MM-DD",/\d{4}-\d{2}-\d{2}/],["GGGG-[W]WW-E",/\d{4}-W\d{2}-\d/],["GGGG-[W]WW",/\d{4}-W\d{2}/],["YYYY-DDD",/\d{4}-\d{3}/]],ic=[["HH:mm:ss.SSSS",/(T| )\d\d:\d\d:\d\d\.\d+/],["HH:mm:ss",/(T| )\d\d:\d\d:\d\d/],["HH:mm",/(T| )\d\d:\d\d/],["HH",/(T| )\d\d/]],jc=/([\+\-]|\d\d)/gi,kc=("Date|Hours|Minutes|Seconds|Milliseconds".split("|"),{Milliseconds:1,Seconds:1e3,Minutes:6e4,Hours:36e5,Days:864e5,Months:2592e6,Years:31536e6}),lc={ms:"millisecond",s:"second",m:"minute",h:"hour",d:"day",D:"date",w:"week",W:"isoWeek",M:"month",Q:"quarter",y:"year",DDD:"dayOfYear",e:"weekday",E:"isoWeekday",gg:"weekYear",GG:"isoWeekYear"},mc={dayofyear:"dayOfYear",isoweekday:"isoWeekday",isoweek:"isoWeek",weekyear:"weekYear",isoweekyear:"isoWeekYear"},nc={},oc={s:45,m:45,h:22,d:26,M:11},pc="DDD w W M D d".split(" "),qc="M D H h m s w W".split(" "),rc={M:function(){return this.month()+1},MMM:function(a){return this.localeData().monthsShort(this,a)},MMMM:function(a){return this.localeData().months(this,a)},D:function(){return this.date()},DDD:function(){return this.dayOfYear()},d:function(){return this.day()},dd:function(a){return this.localeData().weekdaysMin(this,a)},ddd:function(a){return this.localeData().weekdaysShort(this,a)},dddd:function(a){return this.localeData().weekdays(this,a)},w:function(){return this.week()},W:function(){return this.isoWeek()},YY:function(){return r(this.year()%100,2)},YYYY:function(){return r(this.year(),4)},YYYYY:function(){return r(this.year(),5)},YYYYYY:function(){var a=this.year(),b=a>=0?"+":"-";return b+r(Math.abs(a),6)},gg:function(){return r(this.weekYear()%100,2)},gggg:function(){return r(this.weekYear(),4)},ggggg:function(){return r(this.weekYear(),5)},GG:function(){return r(this.isoWeekYear()%100,2)},GGGG:function(){return r(this.isoWeekYear(),4)},GGGGG:function(){return r(this.isoWeekYear(),5)},e:function(){return this.weekday()},E:function(){return this.isoWeekday()},a:function(){return this.localeData().meridiem(this.hours(),this.minutes(),!0)},A:function(){return this.localeData().meridiem(this.hours(),this.minutes(),!1)},H:function(){return this.hours()},h:function(){return this.hours()%12||12},m:function(){return this.minutes()},s:function(){return this.seconds()},S:function(){return C(this.milliseconds()/100)},SS:function(){return r(C(this.milliseconds()/10),2)},SSS:function(){return r(this.milliseconds(),3)},SSSS:function(){return r(this.milliseconds(),3)},Z:function(){var a=this.utcOffset(),b="+";return 0>a&&(a=-a,b="-"),b+r(C(a/60),2)+":"+r(C(a)%60,2)},ZZ:function(){var a=this.utcOffset(),b="+";return 0>a&&(a=-a,b="-"),b+r(C(a/60),2)+r(C(a)%60,2)},z:function(){return this.zoneAbbr()},zz:function(){return this.zoneName()},x:function(){return this.valueOf()},X:function(){return this.unix()},Q:function(){return this.quarter()}},sc={},tc=["months","monthsShort","weekdays","weekdaysShort","weekdaysMin"],uc=!1;pc.length;)xb=pc.pop(),rc[xb+"o"]=i(rc[xb],xb);for(;qc.length;)xb=qc.pop(),rc[xb+xb]=h(rc[xb],2);rc.DDDD=h(rc.DDD,3),o(l.prototype,{set:function(a){var b,c;for(c in a)b=a[c],"function"==typeof b?this[c]=b:this["_"+c]=b;this._ordinalParseLenient=new RegExp(this._ordinalParse.source+"|"+/\d{1,2}/.source)},_months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),months:function(a){return this._months[a.month()]},_monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),monthsShort:function(a){return this._monthsShort[a.month()]},monthsParse:function(a,b,c){var d,e,f;for(this._monthsParse||(this._monthsParse=[],this._longMonthsParse=[],this._shortMonthsParse=[]),d=0;12>d;d++){if(e=vb.utc([2e3,d]),c&&!this._longMonthsParse[d]&&(this._longMonthsParse[d]=new RegExp("^"+this.months(e,"").replace(".","")+"$","i"),this._shortMonthsParse[d]=new RegExp("^"+this.monthsShort(e,"").replace(".","")+"$","i")),c||this._monthsParse[d]||(f="^"+this.months(e,"")+"|^"+this.monthsShort(e,""),this._monthsParse[d]=new RegExp(f.replace(".",""),"i")),c&&"MMMM"===b&&this._longMonthsParse[d].test(a))return d;if(c&&"MMM"===b&&this._shortMonthsParse[d].test(a))return d;if(!c&&this._monthsParse[d].test(a))return d}},_weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdays:function(a){return this._weekdays[a.day()]},_weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysShort:function(a){return this._weekdaysShort[a.day()]},_weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),weekdaysMin:function(a){return this._weekdaysMin[a.day()]},weekdaysParse:function(a){var b,c,d;for(this._weekdaysParse||(this._weekdaysParse=[]),b=0;7>b;b++)if(this._weekdaysParse[b]||(c=vb([2e3,1]).day(b),d="^"+this.weekdays(c,"")+"|^"+this.weekdaysShort(c,"")+"|^"+this.weekdaysMin(c,""),this._weekdaysParse[b]=new RegExp(d.replace(".",""),"i")),this._weekdaysParse[b].test(a))return b},_longDateFormat:{LTS:"h:mm:ss A",LT:"h:mm A",L:"MM/DD/YYYY",LL:"MMMM D, YYYY",LLL:"MMMM D, YYYY LT",LLLL:"dddd, MMMM D, YYYY LT"},longDateFormat:function(a){var b=this._longDateFormat[a];return!b&&this._longDateFormat[a.toUpperCase()]&&(b=this._longDateFormat[a.toUpperCase()].replace(/MMMM|MM|DD|dddd/g,function(a){return a.slice(1)}),this._longDateFormat[a]=b),b},isPM:function(a){return"p"===(a+"").toLowerCase().charAt(0)},_meridiemParse:/[ap]\.?m?\.?/i,meridiem:function(a,b,c){return a>11?c?"pm":"PM":c?"am":"AM"},_calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},calendar:function(a,b,c){var d=this._calendar[a];return"function"==typeof d?d.apply(b,[c]):d},_relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},relativeTime:function(a,b,c,d){var e=this._relativeTime[c];return"function"==typeof e?e(a,b,c,d):e.replace(/%d/i,a)},pastFuture:function(a,b){var c=this._relativeTime[a>0?"future":"past"];return"function"==typeof c?c(b):c.replace(/%s/i,b)},ordinal:function(a){return this._ordinal.replace("%d",a)},_ordinal:"%d",_ordinalParse:/\d{1,2}/,preparse:function(a){return a},postformat:function(a){return a},week:function(a){return jb(a,this._week.dow,this._week.doy).week},_week:{dow:0,doy:6},firstDayOfWeek:function(){return this._week.dow},firstDayOfYear:function(){return this._week.doy},_invalidDate:"Invalid date",invalidDate:function(){return this._invalidDate}}),vb=function(b,c,e,f){var g;return"boolean"==typeof e&&(f=e,e=a),g={},g._isAMomentObject=!0,g._i=b,g._f=c,g._l=e,g._strict=f,g._isUTC=!1,g._pf=d(),lb(g)},vb.suppressDeprecationWarnings=!1,vb.createFromInputFallback=f("moment construction falls back to js Date. This is discouraged and will be removed in upcoming major release. Please refer to https://github.com/moment/moment/issues/1407 for more info.",function(a){a._d=new Date(a._i+(a._useUTC?" UTC":""))}),vb.min=function(){var a=[].slice.call(arguments,0);return mb("isBefore",a)},vb.max=function(){var a=[].slice.call(arguments,0);return mb("isAfter",a)},vb.utc=function(b,c,e,f){var g;return"boolean"==typeof e&&(f=e,e=a),g={},g._isAMomentObject=!0,g._useUTC=!0,g._isUTC=!0,g._l=e,g._i=b,g._f=c,g._strict=f,g._pf=d(),lb(g).utc()},vb.unix=function(a){return vb(1e3*a)},vb.duration=function(a,b){var d,e,f,g,h=a,i=null;return vb.isDuration(a)?h={ms:a._milliseconds,d:a._days,M:a._months}:"number"==typeof a?(h={},b?h[b]=a:h.milliseconds=a):(i=Nb.exec(a))?(d="-"===i[1]?-1:1,h={y:0,d:C(i[Eb])*d,h:C(i[Fb])*d,m:C(i[Gb])*d,s:C(i[Hb])*d,ms:C(i[Ib])*d}):(i=Ob.exec(a))?(d="-"===i[1]?-1:1,f=function(a){var b=a&&parseFloat(a.replace(",","."));return(isNaN(b)?0:b)*d},h={y:f(i[2]),M:f(i[3]),d:f(i[4]),h:f(i[5]),m:f(i[6]),s:f(i[7]),w:f(i[8])}):null==h?h={}:"object"==typeof h&&("from"in h||"to"in h)&&(g=t(vb(h.from),vb(h.to)),h={},h.ms=g.milliseconds,h.M=g.months),e=new n(h),vb.isDuration(a)&&c(a,"_locale")&&(e._locale=a._locale),e},vb.version=yb,vb.defaultFormat=gc,vb.ISO_8601=function(){},vb.momentProperties=Kb,vb.updateOffset=function(){},vb.relativeTimeThreshold=function(b,c){return oc[b]===a?!1:c===a?oc[b]:(oc[b]=c,!0)},vb.lang=f("moment.lang is deprecated. Use moment.locale instead.",function(a,b){return vb.locale(a,b)}),vb.locale=function(a,b){var c;return a&&(c="undefined"!=typeof b?vb.defineLocale(a,b):vb.localeData(a),c&&(vb.duration._locale=vb._locale=c)),vb._locale._abbr},vb.defineLocale=function(a,b){return null!==b?(b.abbr=a,Jb[a]||(Jb[a]=new l),Jb[a].set(b),vb.locale(a),Jb[a]):(delete Jb[a],null)},vb.langData=f("moment.langData is deprecated. Use moment.localeData instead.",function(a){return vb.localeData(a)}),vb.localeData=function(a){var b;if(a&&a._locale&&a._locale._abbr&&(a=a._locale._abbr),!a)return vb._locale;if(!w(a)){if(b=L(a))return b;a=[a]}return K(a)},vb.isMoment=function(a){return a instanceof m||null!=a&&c(a,"_isAMomentObject")},vb.isDuration=function(a){return a instanceof n};for(xb=tc.length-1;xb>=0;--xb)B(tc[xb]);vb.normalizeUnits=function(a){return z(a)},vb.invalid=function(a){var b=vb.utc(0/0);return null!=a?o(b._pf,a):b._pf.userInvalidated=!0,b},vb.parseZone=function(){return vb.apply(null,arguments).parseZone()},vb.parseTwoDigitYear=function(a){return C(a)+(C(a)>68?1900:2e3)},vb.isDate=x,o(vb.fn=m.prototype,{clone:function(){return vb(this)},valueOf:function(){return+this._d-6e4*(this._offset||0)},unix:function(){return Math.floor(+this/1e3)},toString:function(){return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")},toDate:function(){return this._offset?new Date(+this):this._d},toISOString:function(){var a=vb(this).utc();return 0<a.year()&&a.year()<=9999?"function"==typeof Date.prototype.toISOString?this.toDate().toISOString():P(a,"YYYY-MM-DD[T]HH:mm:ss.SSS[Z]"):P(a,"YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]")},toArray:function(){var a=this;return[a.year(),a.month(),a.date(),a.hours(),a.minutes(),a.seconds(),a.milliseconds()]},isValid:function(){return I(this)},isDSTShifted:function(){return this._a?this.isValid()&&y(this._a,(this._isUTC?vb.utc(this._a):vb(this._a)).toArray())>0:!1},parsingFlags:function(){return o({},this._pf)},invalidAt:function(){return this._pf.overflow},utc:function(a){return this.utcOffset(0,a)},local:function(a){return this._isUTC&&(this.utcOffset(0,a),this._isUTC=!1,a&&this.subtract(this._dateUtcOffset(),"m")),this},format:function(a){var b=P(this,a||vb.defaultFormat);return this.localeData().postformat(b)},add:u(1,"add"),subtract:u(-1,"subtract"),diff:function(a,b,c){var d,e,f=M(a,this),g=6e4*(f.utcOffset()-this.utcOffset());return b=z(b),"year"===b||"month"===b||"quarter"===b?(e=j(this,f),"quarter"===b?e/=3:"year"===b&&(e/=12)):(d=this-f,e="second"===b?d/1e3:"minute"===b?d/6e4:"hour"===b?d/36e5:"day"===b?(d-g)/864e5:"week"===b?(d-g)/6048e5:d),c?e:q(e)},from:function(a,b){return vb.duration({to:this,from:a}).locale(this.locale()).humanize(!b)},fromNow:function(a){return this.from(vb(),a)},calendar:function(a){var b=a||vb(),c=M(b,this).startOf("day"),d=this.diff(c,"days",!0),e=-6>d?"sameElse":-1>d?"lastWeek":0>d?"lastDay":1>d?"sameDay":2>d?"nextDay":7>d?"nextWeek":"sameElse";return this.format(this.localeData().calendar(e,this,vb(b)))},isLeapYear:function(){return G(this.year())},isDST:function(){return this.utcOffset()>this.clone().month(0).utcOffset()||this.utcOffset()>this.clone().month(5).utcOffset()},day:function(a){var b=this._isUTC?this._d.getUTCDay():this._d.getDay();return null!=a?(a=gb(a,this.localeData()),this.add(a-b,"d")):b},month:qb("Month",!0),startOf:function(a){switch(a=z(a)){case"year":this.month(0);case"quarter":case"month":this.date(1);case"week":case"isoWeek":case"day":this.hours(0);case"hour":this.minutes(0);case"minute":this.seconds(0);case"second":this.milliseconds(0)}return"week"===a?this.weekday(0):"isoWeek"===a&&this.isoWeekday(1),"quarter"===a&&this.month(3*Math.floor(this.month()/3)),this},endOf:function(b){return b=z(b),b===a||"millisecond"===b?this:this.startOf(b).add(1,"isoWeek"===b?"week":b).subtract(1,"ms")},isAfter:function(a,b){var c;return b=z("undefined"!=typeof b?b:"millisecond"),"millisecond"===b?(a=vb.isMoment(a)?a:vb(a),+this>+a):(c=vb.isMoment(a)?+a:+vb(a),c<+this.clone().startOf(b))},isBefore:function(a,b){var c;return b=z("undefined"!=typeof b?b:"millisecond"),"millisecond"===b?(a=vb.isMoment(a)?a:vb(a),+a>+this):(c=vb.isMoment(a)?+a:+vb(a),+this.clone().endOf(b)<c)},isBetween:function(a,b,c){return this.isAfter(a,c)&&this.isBefore(b,c)},isSame:function(a,b){var c;return b=z(b||"millisecond"),"millisecond"===b?(a=vb.isMoment(a)?a:vb(a),+this===+a):(c=+vb(a),+this.clone().startOf(b)<=c&&c<=+this.clone().endOf(b))},min:f("moment().min is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548",function(a){return a=vb.apply(null,arguments),this>a?this:a}),max:f("moment().max is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548",function(a){return a=vb.apply(null,arguments),a>this?this:a}),zone:f("moment().zone is deprecated, use moment().utcOffset instead. https://github.com/moment/moment/issues/1779",function(a,b){return null!=a?("string"!=typeof a&&(a=-a),this.utcOffset(a,b),this):-this.utcOffset()}),utcOffset:function(a,b){var c,d=this._offset||0;return null!=a?("string"==typeof a&&(a=S(a)),Math.abs(a)<16&&(a=60*a),!this._isUTC&&b&&(c=this._dateUtcOffset()),this._offset=a,this._isUTC=!0,null!=c&&this.add(c,"m"),d!==a&&(!b||this._changeInProgress?v(this,vb.duration(a-d,"m"),1,!1):this._changeInProgress||(this._changeInProgress=!0,vb.updateOffset(this,!0),this._changeInProgress=null)),this):this._isUTC?d:this._dateUtcOffset()},isLocal:function(){return!this._isUTC},isUtcOffset:function(){return this._isUTC},isUtc:function(){return this._isUTC&&0===this._offset},zoneAbbr:function(){return this._isUTC?"UTC":""},zoneName:function(){return this._isUTC?"Coordinated Universal Time":""},parseZone:function(){return this._tzm?this.utcOffset(this._tzm):"string"==typeof this._i&&this.utcOffset(S(this._i)),this},hasAlignedHourOffset:function(a){return a=a?vb(a).utcOffset():0,(this.utcOffset()-a)%60===0},daysInMonth:function(){return D(this.year(),this.month())},dayOfYear:function(a){var b=Ab((vb(this).startOf("day")-vb(this).startOf("year"))/864e5)+1;return null==a?b:this.add(a-b,"d")},quarter:function(a){return null==a?Math.ceil((this.month()+1)/3):this.month(3*(a-1)+this.month()%3)},weekYear:function(a){var b=jb(this,this.localeData()._week.dow,this.localeData()._week.doy).year;return null==a?b:this.add(a-b,"y")},isoWeekYear:function(a){var b=jb(this,1,4).year;return null==a?b:this.add(a-b,"y")},week:function(a){var b=this.localeData().week(this);return null==a?b:this.add(7*(a-b),"d")},isoWeek:function(a){var b=jb(this,1,4).week;return null==a?b:this.add(7*(a-b),"d")},weekday:function(a){var b=(this.day()+7-this.localeData()._week.dow)%7;return null==a?b:this.add(a-b,"d")},isoWeekday:function(a){return null==a?this.day()||7:this.day(this.day()%7?a:a-7)},isoWeeksInYear:function(){return E(this.year(),1,4)},weeksInYear:function(){var a=this.localeData()._week;return E(this.year(),a.dow,a.doy)},get:function(a){return a=z(a),this[a]()},set:function(a,b){var c;if("object"==typeof a)for(c in a)this.set(c,a[c]);else a=z(a),"function"==typeof this[a]&&this[a](b);return this},locale:function(b){var c;return b===a?this._locale._abbr:(c=vb.localeData(b),null!=c&&(this._locale=c),this)},lang:f("moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.",function(b){return b===a?this.localeData():this.locale(b)}),localeData:function(){return this._locale},_dateUtcOffset:function(){return 15*-Math.round(this._d.getTimezoneOffset()/15)}}),vb.fn.millisecond=vb.fn.milliseconds=qb("Milliseconds",!1),vb.fn.second=vb.fn.seconds=qb("Seconds",!1),vb.fn.minute=vb.fn.minutes=qb("Minutes",!1),vb.fn.hour=vb.fn.hours=qb("Hours",!0),vb.fn.date=qb("Date",!0),vb.fn.dates=f("dates accessor is deprecated. Use date instead.",qb("Date",!0)),vb.fn.year=qb("FullYear",!0),vb.fn.years=f("years accessor is deprecated. Use year instead.",qb("FullYear",!0)),vb.fn.days=vb.fn.day,vb.fn.months=vb.fn.month,vb.fn.weeks=vb.fn.week,vb.fn.isoWeeks=vb.fn.isoWeek,vb.fn.quarters=vb.fn.quarter,vb.fn.toJSON=vb.fn.toISOString,vb.fn.isUTC=vb.fn.isUtc,o(vb.duration.fn=n.prototype,{_bubble:function(){var a,b,c,d=this._milliseconds,e=this._days,f=this._months,g=this._data,h=0;g.milliseconds=d%1e3,a=q(d/1e3),g.seconds=a%60,b=q(a/60),g.minutes=b%60,c=q(b/60),g.hours=c%24,e+=q(c/24),h=q(rb(e)),e-=q(sb(h)),f+=q(e/30),e%=30,h+=q(f/12),f%=12,g.days=e,g.months=f,g.years=h},abs:function(){return this._milliseconds=Math.abs(this._milliseconds),this._days=Math.abs(this._days),this._months=Math.abs(this._months),this._data.milliseconds=Math.abs(this._data.milliseconds),this._data.seconds=Math.abs(this._data.seconds),this._data.minutes=Math.abs(this._data.minutes),this._data.hours=Math.abs(this._data.hours),this._data.months=Math.abs(this._data.months),this._data.years=Math.abs(this._data.years),this},weeks:function(){return q(this.days()/7)},valueOf:function(){return this._milliseconds+864e5*this._days+this._months%12*2592e6+31536e6*C(this._months/12)
},humanize:function(a){var b=ib(this,!a,this.localeData());return a&&(b=this.localeData().pastFuture(+this,b)),this.localeData().postformat(b)},add:function(a,b){var c=vb.duration(a,b);return this._milliseconds+=c._milliseconds,this._days+=c._days,this._months+=c._months,this._bubble(),this},subtract:function(a,b){var c=vb.duration(a,b);return this._milliseconds-=c._milliseconds,this._days-=c._days,this._months-=c._months,this._bubble(),this},get:function(a){return a=z(a),this[a.toLowerCase()+"s"]()},as:function(a){var b,c;if(a=z(a),"month"===a||"year"===a)return b=this._days+this._milliseconds/864e5,c=this._months+12*rb(b),"month"===a?c:c/12;switch(b=this._days+Math.round(sb(this._months/12)),a){case"week":return b/7+this._milliseconds/6048e5;case"day":return b+this._milliseconds/864e5;case"hour":return 24*b+this._milliseconds/36e5;case"minute":return 24*b*60+this._milliseconds/6e4;case"second":return 24*b*60*60+this._milliseconds/1e3;case"millisecond":return Math.floor(24*b*60*60*1e3)+this._milliseconds;default:throw new Error("Unknown unit "+a)}},lang:vb.fn.lang,locale:vb.fn.locale,toIsoString:f("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)",function(){return this.toISOString()}),toISOString:function(){var a=Math.abs(this.years()),b=Math.abs(this.months()),c=Math.abs(this.days()),d=Math.abs(this.hours()),e=Math.abs(this.minutes()),f=Math.abs(this.seconds()+this.milliseconds()/1e3);return this.asSeconds()?(this.asSeconds()<0?"-":"")+"P"+(a?a+"Y":"")+(b?b+"M":"")+(c?c+"D":"")+(d||e||f?"T":"")+(d?d+"H":"")+(e?e+"M":"")+(f?f+"S":""):"P0D"},localeData:function(){return this._locale},toJSON:function(){return this.toISOString()}}),vb.duration.fn.toString=vb.duration.fn.toISOString;for(xb in kc)c(kc,xb)&&tb(xb.toLowerCase());vb.duration.fn.asMilliseconds=function(){return this.as("ms")},vb.duration.fn.asSeconds=function(){return this.as("s")},vb.duration.fn.asMinutes=function(){return this.as("m")},vb.duration.fn.asHours=function(){return this.as("h")},vb.duration.fn.asDays=function(){return this.as("d")},vb.duration.fn.asWeeks=function(){return this.as("weeks")},vb.duration.fn.asMonths=function(){return this.as("M")},vb.duration.fn.asYears=function(){return this.as("y")},vb.locale("en",{ordinalParse:/\d{1,2}(th|st|nd|rd)/,ordinal:function(a){var b=a%10,c=1===C(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c}}),function(a){a(vb)}(function(a){return a.defineLocale("af",{months:"Januarie_Februarie_Maart_April_Mei_Junie_Julie_Augustus_September_Oktober_November_Desember".split("_"),monthsShort:"Jan_Feb_Mar_Apr_Mei_Jun_Jul_Aug_Sep_Okt_Nov_Des".split("_"),weekdays:"Sondag_Maandag_Dinsdag_Woensdag_Donderdag_Vrydag_Saterdag".split("_"),weekdaysShort:"Son_Maa_Din_Woe_Don_Vry_Sat".split("_"),weekdaysMin:"So_Ma_Di_Wo_Do_Vr_Sa".split("_"),meridiemParse:/vm|nm/i,isPM:function(a){return/^nm$/i.test(a)},meridiem:function(a,b,c){return 12>a?c?"vm":"VM":c?"nm":"NM"},longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd, D MMMM YYYY LT"},calendar:{sameDay:"[Vandag om] LT",nextDay:"[Môre om] LT",nextWeek:"dddd [om] LT",lastDay:"[Gister om] LT",lastWeek:"[Laas] dddd [om] LT",sameElse:"L"},relativeTime:{future:"oor %s",past:"%s gelede",s:"'n paar sekondes",m:"'n minuut",mm:"%d minute",h:"'n uur",hh:"%d ure",d:"'n dag",dd:"%d dae",M:"'n maand",MM:"%d maande",y:"'n jaar",yy:"%d jaar"},ordinalParse:/\d{1,2}(ste|de)/,ordinal:function(a){return a+(1===a||8===a||a>=20?"ste":"de")},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("ar-ma",{months:"يناير_فبراير_مارس_أبريل_ماي_يونيو_يوليوز_غشت_شتنبر_أكتوبر_نونبر_دجنبر".split("_"),monthsShort:"يناير_فبراير_مارس_أبريل_ماي_يونيو_يوليوز_غشت_شتنبر_أكتوبر_نونبر_دجنبر".split("_"),weekdays:"الأحد_الإتنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),weekdaysShort:"احد_اتنين_ثلاثاء_اربعاء_خميس_جمعة_سبت".split("_"),weekdaysMin:"ح_ن_ث_ر_خ_ج_س".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D MMMM YYYY LT"},calendar:{sameDay:"[اليوم على الساعة] LT",nextDay:"[غدا على الساعة] LT",nextWeek:"dddd [على الساعة] LT",lastDay:"[أمس على الساعة] LT",lastWeek:"dddd [على الساعة] LT",sameElse:"L"},relativeTime:{future:"في %s",past:"منذ %s",s:"ثوان",m:"دقيقة",mm:"%d دقائق",h:"ساعة",hh:"%d ساعات",d:"يوم",dd:"%d أيام",M:"شهر",MM:"%d أشهر",y:"سنة",yy:"%d سنوات"},week:{dow:6,doy:12}})}),function(a){a(vb)}(function(a){var b={1:"١",2:"٢",3:"٣",4:"٤",5:"٥",6:"٦",7:"٧",8:"٨",9:"٩",0:"٠"},c={"١":"1","٢":"2","٣":"3","٤":"4","٥":"5","٦":"6","٧":"7","٨":"8","٩":"9","٠":"0"};return a.defineLocale("ar-sa",{months:"يناير_فبراير_مارس_أبريل_مايو_يونيو_يوليو_أغسطس_سبتمبر_أكتوبر_نوفمبر_ديسمبر".split("_"),monthsShort:"يناير_فبراير_مارس_أبريل_مايو_يونيو_يوليو_أغسطس_سبتمبر_أكتوبر_نوفمبر_ديسمبر".split("_"),weekdays:"الأحد_الإثنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),weekdaysShort:"أحد_إثنين_ثلاثاء_أربعاء_خميس_جمعة_سبت".split("_"),weekdaysMin:"ح_ن_ث_ر_خ_ج_س".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D MMMM YYYY LT"},meridiemParse:/ص|م/,isPM:function(a){return"م"===a},meridiem:function(a){return 12>a?"ص":"م"},calendar:{sameDay:"[اليوم على الساعة] LT",nextDay:"[غدا على الساعة] LT",nextWeek:"dddd [على الساعة] LT",lastDay:"[أمس على الساعة] LT",lastWeek:"dddd [على الساعة] LT",sameElse:"L"},relativeTime:{future:"في %s",past:"منذ %s",s:"ثوان",m:"دقيقة",mm:"%d دقائق",h:"ساعة",hh:"%d ساعات",d:"يوم",dd:"%d أيام",M:"شهر",MM:"%d أشهر",y:"سنة",yy:"%d سنوات"},preparse:function(a){return a.replace(/[١٢٣٤٥٦٧٨٩٠]/g,function(a){return c[a]}).replace(/،/g,",")},postformat:function(a){return a.replace(/\d/g,function(a){return b[a]}).replace(/,/g,"،")},week:{dow:6,doy:12}})}),function(a){a(vb)}(function(a){return a.defineLocale("ar-tn",{months:"جانفي_فيفري_مارس_أفريل_ماي_جوان_جويلية_أوت_سبتمبر_أكتوبر_نوفمبر_ديسمبر".split("_"),monthsShort:"جانفي_فيفري_مارس_أفريل_ماي_جوان_جويلية_أوت_سبتمبر_أكتوبر_نوفمبر_ديسمبر".split("_"),weekdays:"الأحد_الإثنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),weekdaysShort:"أحد_إثنين_ثلاثاء_أربعاء_خميس_جمعة_سبت".split("_"),weekdaysMin:"ح_ن_ث_ر_خ_ج_س".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D MMMM YYYY LT"},calendar:{sameDay:"[اليوم على الساعة] LT",nextDay:"[غدا على الساعة] LT",nextWeek:"dddd [على الساعة] LT",lastDay:"[أمس على الساعة] LT",lastWeek:"dddd [على الساعة] LT",sameElse:"L"},relativeTime:{future:"في %s",past:"منذ %s",s:"ثوان",m:"دقيقة",mm:"%d دقائق",h:"ساعة",hh:"%d ساعات",d:"يوم",dd:"%d أيام",M:"شهر",MM:"%d أشهر",y:"سنة",yy:"%d سنوات"},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){var b={1:"١",2:"٢",3:"٣",4:"٤",5:"٥",6:"٦",7:"٧",8:"٨",9:"٩",0:"٠"},c={"١":"1","٢":"2","٣":"3","٤":"4","٥":"5","٦":"6","٧":"7","٨":"8","٩":"9","٠":"0"},d=function(a){return 0===a?0:1===a?1:2===a?2:a%100>=3&&10>=a%100?3:a%100>=11?4:5},e={s:["أقل من ثانية","ثانية واحدة",["ثانيتان","ثانيتين"],"%d ثوان","%d ثانية","%d ثانية"],m:["أقل من دقيقة","دقيقة واحدة",["دقيقتان","دقيقتين"],"%d دقائق","%d دقيقة","%d دقيقة"],h:["أقل من ساعة","ساعة واحدة",["ساعتان","ساعتين"],"%d ساعات","%d ساعة","%d ساعة"],d:["أقل من يوم","يوم واحد",["يومان","يومين"],"%d أيام","%d يومًا","%d يوم"],M:["أقل من شهر","شهر واحد",["شهران","شهرين"],"%d أشهر","%d شهرا","%d شهر"],y:["أقل من عام","عام واحد",["عامان","عامين"],"%d أعوام","%d عامًا","%d عام"]},f=function(a){return function(b,c){var f=d(b),g=e[a][d(b)];return 2===f&&(g=g[c?0:1]),g.replace(/%d/i,b)}},g=["كانون الثاني يناير","شباط فبراير","آذار مارس","نيسان أبريل","أيار مايو","حزيران يونيو","تموز يوليو","آب أغسطس","أيلول سبتمبر","تشرين الأول أكتوبر","تشرين الثاني نوفمبر","كانون الأول ديسمبر"];return a.defineLocale("ar",{months:g,monthsShort:g,weekdays:"الأحد_الإثنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),weekdaysShort:"أحد_إثنين_ثلاثاء_أربعاء_خميس_جمعة_سبت".split("_"),weekdaysMin:"ح_ن_ث_ر_خ_ج_س".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D MMMM YYYY LT"},meridiemParse:/ص|م/,isPM:function(a){return"م"===a},meridiem:function(a){return 12>a?"ص":"م"},calendar:{sameDay:"[اليوم عند الساعة] LT",nextDay:"[غدًا عند الساعة] LT",nextWeek:"dddd [عند الساعة] LT",lastDay:"[أمس عند الساعة] LT",lastWeek:"dddd [عند الساعة] LT",sameElse:"L"},relativeTime:{future:"بعد %s",past:"منذ %s",s:f("s"),m:f("m"),mm:f("m"),h:f("h"),hh:f("h"),d:f("d"),dd:f("d"),M:f("M"),MM:f("M"),y:f("y"),yy:f("y")},preparse:function(a){return a.replace(/[١٢٣٤٥٦٧٨٩٠]/g,function(a){return c[a]}).replace(/،/g,",")},postformat:function(a){return a.replace(/\d/g,function(a){return b[a]}).replace(/,/g,"،")},week:{dow:6,doy:12}})}),function(a){a(vb)}(function(a){var b={1:"-inci",5:"-inci",8:"-inci",70:"-inci",80:"-inci",2:"-nci",7:"-nci",20:"-nci",50:"-nci",3:"-üncü",4:"-üncü",100:"-üncü",6:"-ncı",9:"-uncu",10:"-uncu",30:"-uncu",60:"-ıncı",90:"-ıncı"};return a.defineLocale("az",{months:"yanvar_fevral_mart_aprel_may_iyun_iyul_avqust_sentyabr_oktyabr_noyabr_dekabr".split("_"),monthsShort:"yan_fev_mar_apr_may_iyn_iyl_avq_sen_okt_noy_dek".split("_"),weekdays:"Bazar_Bazar ertəsi_Çərşənbə axşamı_Çərşənbə_Cümə axşamı_Cümə_Şənbə".split("_"),weekdaysShort:"Baz_BzE_ÇAx_Çər_CAx_Cüm_Şən".split("_"),weekdaysMin:"Bz_BE_ÇA_Çə_CA_Cü_Şə".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd, D MMMM YYYY LT"},calendar:{sameDay:"[bugün saat] LT",nextDay:"[sabah saat] LT",nextWeek:"[gələn həftə] dddd [saat] LT",lastDay:"[dünən] LT",lastWeek:"[keçən həftə] dddd [saat] LT",sameElse:"L"},relativeTime:{future:"%s sonra",past:"%s əvvəl",s:"birneçə saniyyə",m:"bir dəqiqə",mm:"%d dəqiqə",h:"bir saat",hh:"%d saat",d:"bir gün",dd:"%d gün",M:"bir ay",MM:"%d ay",y:"bir il",yy:"%d il"},meridiemParse:/gecə|səhər|gündüz|axşam/,isPM:function(a){return/^(gündüz|axşam)$/.test(a)},meridiem:function(a){return 4>a?"gecə":12>a?"səhər":17>a?"gündüz":"axşam"},ordinalParse:/\d{1,2}-(ıncı|inci|nci|üncü|ncı|uncu)/,ordinal:function(a){if(0===a)return a+"-ıncı";var c=a%10,d=a%100-c,e=a>=100?100:null;return a+(b[c]||b[d]||b[e])},week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){function b(a,b){var c=a.split("_");return b%10===1&&b%100!==11?c[0]:b%10>=2&&4>=b%10&&(10>b%100||b%100>=20)?c[1]:c[2]}function c(a,c,d){var e={mm:c?"хвіліна_хвіліны_хвілін":"хвіліну_хвіліны_хвілін",hh:c?"гадзіна_гадзіны_гадзін":"гадзіну_гадзіны_гадзін",dd:"дзень_дні_дзён",MM:"месяц_месяцы_месяцаў",yy:"год_гады_гадоў"};return"m"===d?c?"хвіліна":"хвіліну":"h"===d?c?"гадзіна":"гадзіну":a+" "+b(e[d],+a)}function d(a,b){var c={nominative:"студзень_люты_сакавік_красавік_травень_чэрвень_ліпень_жнівень_верасень_кастрычнік_лістапад_снежань".split("_"),accusative:"студзеня_лютага_сакавіка_красавіка_траўня_чэрвеня_ліпеня_жніўня_верасня_кастрычніка_лістапада_снежня".split("_")},d=/D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/.test(b)?"accusative":"nominative";return c[d][a.month()]}function e(a,b){var c={nominative:"нядзеля_панядзелак_аўторак_серада_чацвер_пятніца_субота".split("_"),accusative:"нядзелю_панядзелак_аўторак_сераду_чацвер_пятніцу_суботу".split("_")},d=/\[ ?[Вв] ?(?:мінулую|наступную)? ?\] ?dddd/.test(b)?"accusative":"nominative";return c[d][a.day()]}return a.defineLocale("be",{months:d,monthsShort:"студ_лют_сак_крас_трав_чэрв_ліп_жнів_вер_каст_ліст_снеж".split("_"),weekdays:e,weekdaysShort:"нд_пн_ат_ср_чц_пт_сб".split("_"),weekdaysMin:"нд_пн_ат_ср_чц_пт_сб".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY г.",LLL:"D MMMM YYYY г., LT",LLLL:"dddd, D MMMM YYYY г., LT"},calendar:{sameDay:"[Сёння ў] LT",nextDay:"[Заўтра ў] LT",lastDay:"[Учора ў] LT",nextWeek:function(){return"[У] dddd [ў] LT"},lastWeek:function(){switch(this.day()){case 0:case 3:case 5:case 6:return"[У мінулую] dddd [ў] LT";case 1:case 2:case 4:return"[У мінулы] dddd [ў] LT"}},sameElse:"L"},relativeTime:{future:"праз %s",past:"%s таму",s:"некалькі секунд",m:c,mm:c,h:c,hh:c,d:"дзень",dd:c,M:"месяц",MM:c,y:"год",yy:c},meridiemParse:/ночы|раніцы|дня|вечара/,isPM:function(a){return/^(дня|вечара)$/.test(a)},meridiem:function(a){return 4>a?"ночы":12>a?"раніцы":17>a?"дня":"вечара"},ordinalParse:/\d{1,2}-(і|ы|га)/,ordinal:function(a,b){switch(b){case"M":case"d":case"DDD":case"w":case"W":return a%10!==2&&a%10!==3||a%100===12||a%100===13?a+"-ы":a+"-і";case"D":return a+"-га";default:return a}},week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){return a.defineLocale("bg",{months:"януари_февруари_март_април_май_юни_юли_август_септември_октомври_ноември_декември".split("_"),monthsShort:"янр_фев_мар_апр_май_юни_юли_авг_сеп_окт_ное_дек".split("_"),weekdays:"неделя_понеделник_вторник_сряда_четвъртък_петък_събота".split("_"),weekdaysShort:"нед_пон_вто_сря_чет_пет_съб".split("_"),weekdaysMin:"нд_пн_вт_ср_чт_пт_сб".split("_"),longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"D.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd, D MMMM YYYY LT"},calendar:{sameDay:"[Днес в] LT",nextDay:"[Утре в] LT",nextWeek:"dddd [в] LT",lastDay:"[Вчера в] LT",lastWeek:function(){switch(this.day()){case 0:case 3:case 6:return"[В изминалата] dddd [в] LT";case 1:case 2:case 4:case 5:return"[В изминалия] dddd [в] LT"}},sameElse:"L"},relativeTime:{future:"след %s",past:"преди %s",s:"няколко секунди",m:"минута",mm:"%d минути",h:"час",hh:"%d часа",d:"ден",dd:"%d дни",M:"месец",MM:"%d месеца",y:"година",yy:"%d години"},ordinalParse:/\d{1,2}-(ев|ен|ти|ви|ри|ми)/,ordinal:function(a){var b=a%10,c=a%100;return 0===a?a+"-ев":0===c?a+"-ен":c>10&&20>c?a+"-ти":1===b?a+"-ви":2===b?a+"-ри":7===b||8===b?a+"-ми":a+"-ти"},week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){var b={1:"১",2:"২",3:"৩",4:"৪",5:"৫",6:"৬",7:"৭",8:"৮",9:"৯",0:"০"},c={"১":"1","২":"2","৩":"3","৪":"4","৫":"5","৬":"6","৭":"7","৮":"8","৯":"9","০":"0"};return a.defineLocale("bn",{months:"জানুয়ারী_ফেবুয়ারী_মার্চ_এপ্রিল_মে_জুন_জুলাই_অগাস্ট_সেপ্টেম্বর_অক্টোবর_নভেম্বর_ডিসেম্বর".split("_"),monthsShort:"জানু_ফেব_মার্চ_এপর_মে_জুন_জুল_অগ_সেপ্ট_অক্টো_নভ_ডিসেম্".split("_"),weekdays:"রবিবার_সোমবার_মঙ্গলবার_বুধবার_বৃহস্পত্তিবার_শুক্রুবার_শনিবার".split("_"),weekdaysShort:"রবি_সোম_মঙ্গল_বুধ_বৃহস্পত্তি_শুক্রু_শনি".split("_"),weekdaysMin:"রব_সম_মঙ্গ_বু_ব্রিহ_শু_শনি".split("_"),longDateFormat:{LT:"A h:mm সময়",LTS:"A h:mm:ss সময়",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, LT",LLLL:"dddd, D MMMM YYYY, LT"},calendar:{sameDay:"[আজ] LT",nextDay:"[আগামীকাল] LT",nextWeek:"dddd, LT",lastDay:"[গতকাল] LT",lastWeek:"[গত] dddd, LT",sameElse:"L"},relativeTime:{future:"%s পরে",past:"%s আগে",s:"কএক সেকেন্ড",m:"এক মিনিট",mm:"%d মিনিট",h:"এক ঘন্টা",hh:"%d ঘন্টা",d:"এক দিন",dd:"%d দিন",M:"এক মাস",MM:"%d মাস",y:"এক বছর",yy:"%d বছর"},preparse:function(a){return a.replace(/[১২৩৪৫৬৭৮৯০]/g,function(a){return c[a]})},postformat:function(a){return a.replace(/\d/g,function(a){return b[a]})},meridiemParse:/রাত|শকাল|দুপুর|বিকেল|রাত/,isPM:function(a){return/^(দুপুর|বিকেল|রাত)$/.test(a)},meridiem:function(a){return 4>a?"রাত":10>a?"শকাল":17>a?"দুপুর":20>a?"বিকেল":"রাত"},week:{dow:0,doy:6}})}),function(a){a(vb)}(function(a){var b={1:"༡",2:"༢",3:"༣",4:"༤",5:"༥",6:"༦",7:"༧",8:"༨",9:"༩",0:"༠"},c={"༡":"1","༢":"2","༣":"3","༤":"4","༥":"5","༦":"6","༧":"7","༨":"8","༩":"9","༠":"0"};return a.defineLocale("bo",{months:"ཟླ་བ་དང་པོ_ཟླ་བ་གཉིས་པ_ཟླ་བ་གསུམ་པ_ཟླ་བ་བཞི་པ_ཟླ་བ་ལྔ་པ_ཟླ་བ་དྲུག་པ_ཟླ་བ་བདུན་པ_ཟླ་བ་བརྒྱད་པ_ཟླ་བ་དགུ་པ_ཟླ་བ་བཅུ་པ_ཟླ་བ་བཅུ་གཅིག་པ_ཟླ་བ་བཅུ་གཉིས་པ".split("_"),monthsShort:"ཟླ་བ་དང་པོ_ཟླ་བ་གཉིས་པ_ཟླ་བ་གསུམ་པ_ཟླ་བ་བཞི་པ_ཟླ་བ་ལྔ་པ_ཟླ་བ་དྲུག་པ_ཟླ་བ་བདུན་པ_ཟླ་བ་བརྒྱད་པ_ཟླ་བ་དགུ་པ_ཟླ་བ་བཅུ་པ_ཟླ་བ་བཅུ་གཅིག་པ_ཟླ་བ་བཅུ་གཉིས་པ".split("_"),weekdays:"གཟའ་ཉི་མ་_གཟའ་ཟླ་བ་_གཟའ་མིག་དམར་_གཟའ་ལྷག་པ་_གཟའ་ཕུར་བུ_གཟའ་པ་སངས་_གཟའ་སྤེན་པ་".split("_"),weekdaysShort:"ཉི་མ་_ཟླ་བ་_མིག་དམར་_ལྷག་པ་_ཕུར་བུ_པ་སངས་_སྤེན་པ་".split("_"),weekdaysMin:"ཉི་མ་_ཟླ་བ་_མིག་དམར་_ལྷག་པ་_ཕུར་བུ_པ་སངས་_སྤེན་པ་".split("_"),longDateFormat:{LT:"A h:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, LT",LLLL:"dddd, D MMMM YYYY, LT"},calendar:{sameDay:"[དི་རིང] LT",nextDay:"[སང་ཉིན] LT",nextWeek:"[བདུན་ཕྲག་རྗེས་མ], LT",lastDay:"[ཁ་སང] LT",lastWeek:"[བདུན་ཕྲག་མཐའ་མ] dddd, LT",sameElse:"L"},relativeTime:{future:"%s ལ་",past:"%s སྔན་ལ",s:"ལམ་སང",m:"སྐར་མ་གཅིག",mm:"%d སྐར་མ",h:"ཆུ་ཚོད་གཅིག",hh:"%d ཆུ་ཚོད",d:"ཉིན་གཅིག",dd:"%d ཉིན་",M:"ཟླ་བ་གཅིག",MM:"%d ཟླ་བ",y:"ལོ་གཅིག",yy:"%d ལོ"},preparse:function(a){return a.replace(/[༡༢༣༤༥༦༧༨༩༠]/g,function(a){return c[a]})},postformat:function(a){return a.replace(/\d/g,function(a){return b[a]})},meridiemParse:/མཚན་མོ|ཞོགས་ཀས|ཉིན་གུང|དགོང་དག|མཚན་མོ/,isPM:function(a){return/^(ཉིན་གུང|དགོང་དག|མཚན་མོ)$/.test(a)},meridiem:function(a){return 4>a?"མཚན་མོ":10>a?"ཞོགས་ཀས":17>a?"ཉིན་གུང":20>a?"དགོང་དག":"མཚན་མོ"},week:{dow:0,doy:6}})}),function(a){a(vb)}(function(b){function c(a,b,c){var d={mm:"munutenn",MM:"miz",dd:"devezh"};return a+" "+f(d[c],a)}function d(a){switch(e(a)){case 1:case 3:case 4:case 5:case 9:return a+" bloaz";default:return a+" vloaz"}}function e(a){return a>9?e(a%10):a}function f(a,b){return 2===b?g(a):a}function g(b){var c={m:"v",b:"v",d:"z"};return c[b.charAt(0)]===a?b:c[b.charAt(0)]+b.substring(1)}return b.defineLocale("br",{months:"Genver_C'hwevrer_Meurzh_Ebrel_Mae_Mezheven_Gouere_Eost_Gwengolo_Here_Du_Kerzu".split("_"),monthsShort:"Gen_C'hwe_Meu_Ebr_Mae_Eve_Gou_Eos_Gwe_Her_Du_Ker".split("_"),weekdays:"Sul_Lun_Meurzh_Merc'her_Yaou_Gwener_Sadorn".split("_"),weekdaysShort:"Sul_Lun_Meu_Mer_Yao_Gwe_Sad".split("_"),weekdaysMin:"Su_Lu_Me_Mer_Ya_Gw_Sa".split("_"),longDateFormat:{LT:"h[e]mm A",LTS:"h[e]mm:ss A",L:"DD/MM/YYYY",LL:"D [a viz] MMMM YYYY",LLL:"D [a viz] MMMM YYYY LT",LLLL:"dddd, D [a viz] MMMM YYYY LT"},calendar:{sameDay:"[Hiziv da] LT",nextDay:"[Warc'hoazh da] LT",nextWeek:"dddd [da] LT",lastDay:"[Dec'h da] LT",lastWeek:"dddd [paset da] LT",sameElse:"L"},relativeTime:{future:"a-benn %s",past:"%s 'zo",s:"un nebeud segondennoù",m:"ur vunutenn",mm:c,h:"un eur",hh:"%d eur",d:"un devezh",dd:c,M:"ur miz",MM:c,y:"ur bloaz",yy:d},ordinalParse:/\d{1,2}(añ|vet)/,ordinal:function(a){var b=1===a?"añ":"vet";return a+b},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){function b(a,b,c){var d=a+" ";switch(c){case"m":return b?"jedna minuta":"jedne minute";case"mm":return d+=1===a?"minuta":2===a||3===a||4===a?"minute":"minuta";case"h":return b?"jedan sat":"jednog sata";case"hh":return d+=1===a?"sat":2===a||3===a||4===a?"sata":"sati";case"dd":return d+=1===a?"dan":"dana";case"MM":return d+=1===a?"mjesec":2===a||3===a||4===a?"mjeseca":"mjeseci";case"yy":return d+=1===a?"godina":2===a||3===a||4===a?"godine":"godina"}}return a.defineLocale("bs",{months:"januar_februar_mart_april_maj_juni_juli_august_septembar_oktobar_novembar_decembar".split("_"),monthsShort:"jan._feb._mar._apr._maj._jun._jul._aug._sep._okt._nov._dec.".split("_"),weekdays:"nedjelja_ponedjeljak_utorak_srijeda_četvrtak_petak_subota".split("_"),weekdaysShort:"ned._pon._uto._sri._čet._pet._sub.".split("_"),weekdaysMin:"ne_po_ut_sr_če_pe_su".split("_"),longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"DD. MM. YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY LT",LLLL:"dddd, D. MMMM YYYY LT"},calendar:{sameDay:"[danas u] LT",nextDay:"[sutra u] LT",nextWeek:function(){switch(this.day()){case 0:return"[u] [nedjelju] [u] LT";case 3:return"[u] [srijedu] [u] LT";case 6:return"[u] [subotu] [u] LT";case 1:case 2:case 4:case 5:return"[u] dddd [u] LT"}},lastDay:"[jučer u] LT",lastWeek:function(){switch(this.day()){case 0:case 3:return"[prošlu] dddd [u] LT";case 6:return"[prošle] [subote] [u] LT";case 1:case 2:case 4:case 5:return"[prošli] dddd [u] LT"}},sameElse:"L"},relativeTime:{future:"za %s",past:"prije %s",s:"par sekundi",m:b,mm:b,h:b,hh:b,d:"dan",dd:b,M:"mjesec",MM:b,y:"godinu",yy:b},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){return a.defineLocale("ca",{months:"gener_febrer_març_abril_maig_juny_juliol_agost_setembre_octubre_novembre_desembre".split("_"),monthsShort:"gen._febr._mar._abr._mai._jun._jul._ag._set._oct._nov._des.".split("_"),weekdays:"diumenge_dilluns_dimarts_dimecres_dijous_divendres_dissabte".split("_"),weekdaysShort:"dg._dl._dt._dc._dj._dv._ds.".split("_"),weekdaysMin:"Dg_Dl_Dt_Dc_Dj_Dv_Ds".split("_"),longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D MMMM YYYY LT"},calendar:{sameDay:function(){return"[avui a "+(1!==this.hours()?"les":"la")+"] LT"},nextDay:function(){return"[demà a "+(1!==this.hours()?"les":"la")+"] LT"},nextWeek:function(){return"dddd [a "+(1!==this.hours()?"les":"la")+"] LT"},lastDay:function(){return"[ahir a "+(1!==this.hours()?"les":"la")+"] LT"},lastWeek:function(){return"[el] dddd [passat a "+(1!==this.hours()?"les":"la")+"] LT"},sameElse:"L"},relativeTime:{future:"en %s",past:"fa %s",s:"uns segons",m:"un minut",mm:"%d minuts",h:"una hora",hh:"%d hores",d:"un dia",dd:"%d dies",M:"un mes",MM:"%d mesos",y:"un any",yy:"%d anys"},ordinalParse:/\d{1,2}(r|n|t|è|a)/,ordinal:function(a,b){var c=1===a?"r":2===a?"n":3===a?"r":4===a?"t":"è";return("w"===b||"W"===b)&&(c="a"),a+c},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){function b(a){return a>1&&5>a&&1!==~~(a/10)}function c(a,c,d,e){var f=a+" ";switch(d){case"s":return c||e?"pár sekund":"pár sekundami";case"m":return c?"minuta":e?"minutu":"minutou";case"mm":return c||e?f+(b(a)?"minuty":"minut"):f+"minutami";break;case"h":return c?"hodina":e?"hodinu":"hodinou";case"hh":return c||e?f+(b(a)?"hodiny":"hodin"):f+"hodinami";break;case"d":return c||e?"den":"dnem";case"dd":return c||e?f+(b(a)?"dny":"dní"):f+"dny";break;case"M":return c||e?"měsíc":"měsícem";case"MM":return c||e?f+(b(a)?"měsíce":"měsíců"):f+"měsíci";break;case"y":return c||e?"rok":"rokem";case"yy":return c||e?f+(b(a)?"roky":"let"):f+"lety"}}var d="leden_únor_březen_duben_květen_červen_červenec_srpen_září_říjen_listopad_prosinec".split("_"),e="led_úno_bře_dub_kvě_čvn_čvc_srp_zář_říj_lis_pro".split("_");return a.defineLocale("cs",{months:d,monthsShort:e,monthsParse:function(a,b){var c,d=[];for(c=0;12>c;c++)d[c]=new RegExp("^"+a[c]+"$|^"+b[c]+"$","i");return d}(d,e),weekdays:"neděle_pondělí_úterý_středa_čtvrtek_pátek_sobota".split("_"),weekdaysShort:"ne_po_út_st_čt_pá_so".split("_"),weekdaysMin:"ne_po_út_st_čt_pá_so".split("_"),longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY LT",LLLL:"dddd D. MMMM YYYY LT"},calendar:{sameDay:"[dnes v] LT",nextDay:"[zítra v] LT",nextWeek:function(){switch(this.day()){case 0:return"[v neděli v] LT";case 1:case 2:return"[v] dddd [v] LT";case 3:return"[ve středu v] LT";case 4:return"[ve čtvrtek v] LT";case 5:return"[v pátek v] LT";case 6:return"[v sobotu v] LT"}},lastDay:"[včera v] LT",lastWeek:function(){switch(this.day()){case 0:return"[minulou neděli v] LT";case 1:case 2:return"[minulé] dddd [v] LT";case 3:return"[minulou středu v] LT";case 4:case 5:return"[minulý] dddd [v] LT";case 6:return"[minulou sobotu v] LT"}},sameElse:"L"},relativeTime:{future:"za %s",past:"před %s",s:c,m:c,mm:c,h:c,hh:c,d:c,dd:c,M:c,MM:c,y:c,yy:c},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("cv",{months:"кăрлач_нарăс_пуш_ака_май_çĕртме_утă_çурла_авăн_юпа_чӳк_раштав".split("_"),monthsShort:"кăр_нар_пуш_ака_май_çĕр_утă_çур_ав_юпа_чӳк_раш".split("_"),weekdays:"вырсарникун_тунтикун_ытларикун_юнкун_кĕçнерникун_эрнекун_шăматкун".split("_"),weekdaysShort:"выр_тун_ытл_юн_кĕç_эрн_шăм".split("_"),weekdaysMin:"вр_тн_ыт_юн_кç_эр_шм".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD-MM-YYYY",LL:"YYYY [çулхи] MMMM [уйăхĕн] D[-мĕшĕ]",LLL:"YYYY [çулхи] MMMM [уйăхĕн] D[-мĕшĕ], LT",LLLL:"dddd, YYYY [çулхи] MMMM [уйăхĕн] D[-мĕшĕ], LT"},calendar:{sameDay:"[Паян] LT [сехетре]",nextDay:"[Ыран] LT [сехетре]",lastDay:"[Ĕнер] LT [сехетре]",nextWeek:"[Çитес] dddd LT [сехетре]",lastWeek:"[Иртнĕ] dddd LT [сехетре]",sameElse:"L"},relativeTime:{future:function(a){var b=/сехет$/i.exec(a)?"рен":/çул$/i.exec(a)?"тан":"ран";return a+b},past:"%s каялла",s:"пĕр-ик çеккунт",m:"пĕр минут",mm:"%d минут",h:"пĕр сехет",hh:"%d сехет",d:"пĕр кун",dd:"%d кун",M:"пĕр уйăх",MM:"%d уйăх",y:"пĕр çул",yy:"%d çул"},ordinalParse:/\d{1,2}-мĕш/,ordinal:"%d-мĕш",week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){return a.defineLocale("cy",{months:"Ionawr_Chwefror_Mawrth_Ebrill_Mai_Mehefin_Gorffennaf_Awst_Medi_Hydref_Tachwedd_Rhagfyr".split("_"),monthsShort:"Ion_Chwe_Maw_Ebr_Mai_Meh_Gor_Aws_Med_Hyd_Tach_Rhag".split("_"),weekdays:"Dydd Sul_Dydd Llun_Dydd Mawrth_Dydd Mercher_Dydd Iau_Dydd Gwener_Dydd Sadwrn".split("_"),weekdaysShort:"Sul_Llun_Maw_Mer_Iau_Gwe_Sad".split("_"),weekdaysMin:"Su_Ll_Ma_Me_Ia_Gw_Sa".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd, D MMMM YYYY LT"},calendar:{sameDay:"[Heddiw am] LT",nextDay:"[Yfory am] LT",nextWeek:"dddd [am] LT",lastDay:"[Ddoe am] LT",lastWeek:"dddd [diwethaf am] LT",sameElse:"L"},relativeTime:{future:"mewn %s",past:"%s yn ôl",s:"ychydig eiliadau",m:"munud",mm:"%d munud",h:"awr",hh:"%d awr",d:"diwrnod",dd:"%d diwrnod",M:"mis",MM:"%d mis",y:"blwyddyn",yy:"%d flynedd"},ordinalParse:/\d{1,2}(fed|ain|af|il|ydd|ed|eg)/,ordinal:function(a){var b=a,c="",d=["","af","il","ydd","ydd","ed","ed","ed","fed","fed","fed","eg","fed","eg","eg","fed","eg","eg","fed","eg","fed"];return b>20?c=40===b||50===b||60===b||80===b||100===b?"fed":"ain":b>0&&(c=d[b]),a+c},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("da",{months:"januar_februar_marts_april_maj_juni_juli_august_september_oktober_november_december".split("_"),monthsShort:"jan_feb_mar_apr_maj_jun_jul_aug_sep_okt_nov_dec".split("_"),weekdays:"søndag_mandag_tirsdag_onsdag_torsdag_fredag_lørdag".split("_"),weekdaysShort:"søn_man_tir_ons_tor_fre_lør".split("_"),weekdaysMin:"sø_ma_ti_on_to_fr_lø".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY LT",LLLL:"dddd [d.] D. MMMM YYYY LT"},calendar:{sameDay:"[I dag kl.] LT",nextDay:"[I morgen kl.] LT",nextWeek:"dddd [kl.] LT",lastDay:"[I går kl.] LT",lastWeek:"[sidste] dddd [kl] LT",sameElse:"L"},relativeTime:{future:"om %s",past:"%s siden",s:"få sekunder",m:"et minut",mm:"%d minutter",h:"en time",hh:"%d timer",d:"en dag",dd:"%d dage",M:"en måned",MM:"%d måneder",y:"et år",yy:"%d år"},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){function b(a,b,c){var d={m:["eine Minute","einer Minute"],h:["eine Stunde","einer Stunde"],d:["ein Tag","einem Tag"],dd:[a+" Tage",a+" Tagen"],M:["ein Monat","einem Monat"],MM:[a+" Monate",a+" Monaten"],y:["ein Jahr","einem Jahr"],yy:[a+" Jahre",a+" Jahren"]};return b?d[c][0]:d[c][1]}return a.defineLocale("de-at",{months:"Jänner_Februar_März_April_Mai_Juni_Juli_August_September_Oktober_November_Dezember".split("_"),monthsShort:"Jän._Febr._Mrz._Apr._Mai_Jun._Jul._Aug._Sept._Okt._Nov._Dez.".split("_"),weekdays:"Sonntag_Montag_Dienstag_Mittwoch_Donnerstag_Freitag_Samstag".split("_"),weekdaysShort:"So._Mo._Di._Mi._Do._Fr._Sa.".split("_"),weekdaysMin:"So_Mo_Di_Mi_Do_Fr_Sa".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY LT",LLLL:"dddd, D. MMMM YYYY LT"},calendar:{sameDay:"[Heute um] LT [Uhr]",sameElse:"L",nextDay:"[Morgen um] LT [Uhr]",nextWeek:"dddd [um] LT [Uhr]",lastDay:"[Gestern um] LT [Uhr]",lastWeek:"[letzten] dddd [um] LT [Uhr]"},relativeTime:{future:"in %s",past:"vor %s",s:"ein paar Sekunden",m:b,mm:"%d Minuten",h:b,hh:"%d Stunden",d:b,dd:b,M:b,MM:b,y:b,yy:b},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){function b(a,b,c){var d={m:["eine Minute","einer Minute"],h:["eine Stunde","einer Stunde"],d:["ein Tag","einem Tag"],dd:[a+" Tage",a+" Tagen"],M:["ein Monat","einem Monat"],MM:[a+" Monate",a+" Monaten"],y:["ein Jahr","einem Jahr"],yy:[a+" Jahre",a+" Jahren"]};return b?d[c][0]:d[c][1]}return a.defineLocale("de",{months:"Januar_Februar_März_April_Mai_Juni_Juli_August_September_Oktober_November_Dezember".split("_"),monthsShort:"Jan._Febr._Mrz._Apr._Mai_Jun._Jul._Aug._Sept._Okt._Nov._Dez.".split("_"),weekdays:"Sonntag_Montag_Dienstag_Mittwoch_Donnerstag_Freitag_Samstag".split("_"),weekdaysShort:"So._Mo._Di._Mi._Do._Fr._Sa.".split("_"),weekdaysMin:"So_Mo_Di_Mi_Do_Fr_Sa".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY LT",LLLL:"dddd, D. MMMM YYYY LT"},calendar:{sameDay:"[Heute um] LT [Uhr]",sameElse:"L",nextDay:"[Morgen um] LT [Uhr]",nextWeek:"dddd [um] LT [Uhr]",lastDay:"[Gestern um] LT [Uhr]",lastWeek:"[letzten] dddd [um] LT [Uhr]"},relativeTime:{future:"in %s",past:"vor %s",s:"ein paar Sekunden",m:b,mm:"%d Minuten",h:b,hh:"%d Stunden",d:b,dd:b,M:b,MM:b,y:b,yy:b},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("el",{monthsNominativeEl:"Ιανουάριος_Φεβρουάριος_Μάρτιος_Απρίλιος_Μάιος_Ιούνιος_Ιούλιος_Αύγουστος_Σεπτέμβριος_Οκτώβριος_Νοέμβριος_Δεκέμβριος".split("_"),monthsGenitiveEl:"Ιανουαρίου_Φεβρουαρίου_Μαρτίου_Απριλίου_Μαΐου_Ιουνίου_Ιουλίου_Αυγούστου_Σεπτεμβρίου_Οκτωβρίου_Νοεμβρίου_Δεκεμβρίου".split("_"),months:function(a,b){return/D/.test(b.substring(0,b.indexOf("MMMM")))?this._monthsGenitiveEl[a.month()]:this._monthsNominativeEl[a.month()]},monthsShort:"Ιαν_Φεβ_Μαρ_Απρ_Μαϊ_Ιουν_Ιουλ_Αυγ_Σεπ_Οκτ_Νοε_Δεκ".split("_"),weekdays:"Κυριακή_Δευτέρα_Τρίτη_Τετάρτη_Πέμπτη_Παρασκευή_Σάββατο".split("_"),weekdaysShort:"Κυρ_Δευ_Τρι_Τετ_Πεμ_Παρ_Σαβ".split("_"),weekdaysMin:"Κυ_Δε_Τρ_Τε_Πε_Πα_Σα".split("_"),meridiem:function(a,b,c){return a>11?c?"μμ":"ΜΜ":c?"πμ":"ΠΜ"},isPM:function(a){return"μ"===(a+"").toLowerCase()[0]},meridiemParse:/[ΠΜ]\.?Μ?\.?/i,longDateFormat:{LT:"h:mm A",LTS:"h:mm:ss A",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd, D MMMM YYYY LT"},calendarEl:{sameDay:"[Σήμερα {}] LT",nextDay:"[Αύριο {}] LT",nextWeek:"dddd [{}] LT",lastDay:"[Χθες {}] LT",lastWeek:function(){switch(this.day()){case 6:return"[το προηγούμενο] dddd [{}] LT";default:return"[την προηγούμενη] dddd [{}] LT"}},sameElse:"L"},calendar:function(a,b){var c=this._calendarEl[a],d=b&&b.hours();return"function"==typeof c&&(c=c.apply(b)),c.replace("{}",d%12===1?"στη":"στις")},relativeTime:{future:"σε %s",past:"%s πριν",s:"λίγα δευτερόλεπτα",m:"ένα λεπτό",mm:"%d λεπτά",h:"μία ώρα",hh:"%d ώρες",d:"μία μέρα",dd:"%d μέρες",M:"ένας μήνας",MM:"%d μήνες",y:"ένας χρόνος",yy:"%d χρόνια"},ordinalParse:/\d{1,2}η/,ordinal:"%dη",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("en-au",{months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),longDateFormat:{LT:"h:mm A",LTS:"h:mm:ss A",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd, D MMMM YYYY LT"},calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},ordinalParse:/\d{1,2}(st|nd|rd|th)/,ordinal:function(a){var b=a%10,c=1===~~(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("en-ca",{months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),longDateFormat:{LT:"h:mm A",LTS:"h:mm:ss A",L:"YYYY-MM-DD",LL:"D MMMM, YYYY",LLL:"D MMMM, YYYY LT",LLLL:"dddd, D MMMM, YYYY LT"},calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},ordinalParse:/\d{1,2}(st|nd|rd|th)/,ordinal:function(a){var b=a%10,c=1===~~(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";
return a+c}})}),function(a){a(vb)}(function(a){return a.defineLocale("en-gb",{months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd, D MMMM YYYY LT"},calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},ordinalParse:/\d{1,2}(st|nd|rd|th)/,ordinal:function(a){var b=a%10,c=1===~~(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("eo",{months:"januaro_februaro_marto_aprilo_majo_junio_julio_aŭgusto_septembro_oktobro_novembro_decembro".split("_"),monthsShort:"jan_feb_mar_apr_maj_jun_jul_aŭg_sep_okt_nov_dec".split("_"),weekdays:"Dimanĉo_Lundo_Mardo_Merkredo_Ĵaŭdo_Vendredo_Sabato".split("_"),weekdaysShort:"Dim_Lun_Mard_Merk_Ĵaŭ_Ven_Sab".split("_"),weekdaysMin:"Di_Lu_Ma_Me_Ĵa_Ve_Sa".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"YYYY-MM-DD",LL:"D[-an de] MMMM, YYYY",LLL:"D[-an de] MMMM, YYYY LT",LLLL:"dddd, [la] D[-an de] MMMM, YYYY LT"},meridiemParse:/[ap]\.t\.m/i,isPM:function(a){return"p"===a.charAt(0).toLowerCase()},meridiem:function(a,b,c){return a>11?c?"p.t.m.":"P.T.M.":c?"a.t.m.":"A.T.M."},calendar:{sameDay:"[Hodiaŭ je] LT",nextDay:"[Morgaŭ je] LT",nextWeek:"dddd [je] LT",lastDay:"[Hieraŭ je] LT",lastWeek:"[pasinta] dddd [je] LT",sameElse:"L"},relativeTime:{future:"je %s",past:"antaŭ %s",s:"sekundoj",m:"minuto",mm:"%d minutoj",h:"horo",hh:"%d horoj",d:"tago",dd:"%d tagoj",M:"monato",MM:"%d monatoj",y:"jaro",yy:"%d jaroj"},ordinalParse:/\d{1,2}a/,ordinal:"%da",week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){var b="ene._feb._mar._abr._may._jun._jul._ago._sep._oct._nov._dic.".split("_"),c="ene_feb_mar_abr_may_jun_jul_ago_sep_oct_nov_dic".split("_");return a.defineLocale("es",{months:"enero_febrero_marzo_abril_mayo_junio_julio_agosto_septiembre_octubre_noviembre_diciembre".split("_"),monthsShort:function(a,d){return/-MMM-/.test(d)?c[a.month()]:b[a.month()]},weekdays:"domingo_lunes_martes_miércoles_jueves_viernes_sábado".split("_"),weekdaysShort:"dom._lun._mar._mié._jue._vie._sáb.".split("_"),weekdaysMin:"Do_Lu_Ma_Mi_Ju_Vi_Sá".split("_"),longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D [de] MMMM [de] YYYY",LLL:"D [de] MMMM [de] YYYY LT",LLLL:"dddd, D [de] MMMM [de] YYYY LT"},calendar:{sameDay:function(){return"[hoy a la"+(1!==this.hours()?"s":"")+"] LT"},nextDay:function(){return"[mañana a la"+(1!==this.hours()?"s":"")+"] LT"},nextWeek:function(){return"dddd [a la"+(1!==this.hours()?"s":"")+"] LT"},lastDay:function(){return"[ayer a la"+(1!==this.hours()?"s":"")+"] LT"},lastWeek:function(){return"[el] dddd [pasado a la"+(1!==this.hours()?"s":"")+"] LT"},sameElse:"L"},relativeTime:{future:"en %s",past:"hace %s",s:"unos segundos",m:"un minuto",mm:"%d minutos",h:"una hora",hh:"%d horas",d:"un día",dd:"%d días",M:"un mes",MM:"%d meses",y:"un año",yy:"%d años"},ordinalParse:/\d{1,2}º/,ordinal:"%dº",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){function b(a,b,c,d){var e={s:["mõne sekundi","mõni sekund","paar sekundit"],m:["ühe minuti","üks minut"],mm:[a+" minuti",a+" minutit"],h:["ühe tunni","tund aega","üks tund"],hh:[a+" tunni",a+" tundi"],d:["ühe päeva","üks päev"],M:["kuu aja","kuu aega","üks kuu"],MM:[a+" kuu",a+" kuud"],y:["ühe aasta","aasta","üks aasta"],yy:[a+" aasta",a+" aastat"]};return b?e[c][2]?e[c][2]:e[c][1]:d?e[c][0]:e[c][1]}return a.defineLocale("et",{months:"jaanuar_veebruar_märts_aprill_mai_juuni_juuli_august_september_oktoober_november_detsember".split("_"),monthsShort:"jaan_veebr_märts_apr_mai_juuni_juuli_aug_sept_okt_nov_dets".split("_"),weekdays:"pühapäev_esmaspäev_teisipäev_kolmapäev_neljapäev_reede_laupäev".split("_"),weekdaysShort:"P_E_T_K_N_R_L".split("_"),weekdaysMin:"P_E_T_K_N_R_L".split("_"),longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY LT",LLLL:"dddd, D. MMMM YYYY LT"},calendar:{sameDay:"[Täna,] LT",nextDay:"[Homme,] LT",nextWeek:"[Järgmine] dddd LT",lastDay:"[Eile,] LT",lastWeek:"[Eelmine] dddd LT",sameElse:"L"},relativeTime:{future:"%s pärast",past:"%s tagasi",s:b,m:b,mm:b,h:b,hh:b,d:b,dd:"%d päeva",M:b,MM:b,y:b,yy:b},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("eu",{months:"urtarrila_otsaila_martxoa_apirila_maiatza_ekaina_uztaila_abuztua_iraila_urria_azaroa_abendua".split("_"),monthsShort:"urt._ots._mar._api._mai._eka._uzt._abu._ira._urr._aza._abe.".split("_"),weekdays:"igandea_astelehena_asteartea_asteazkena_osteguna_ostirala_larunbata".split("_"),weekdaysShort:"ig._al._ar._az._og._ol._lr.".split("_"),weekdaysMin:"ig_al_ar_az_og_ol_lr".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"YYYY-MM-DD",LL:"YYYY[ko] MMMM[ren] D[a]",LLL:"YYYY[ko] MMMM[ren] D[a] LT",LLLL:"dddd, YYYY[ko] MMMM[ren] D[a] LT",l:"YYYY-M-D",ll:"YYYY[ko] MMM D[a]",lll:"YYYY[ko] MMM D[a] LT",llll:"ddd, YYYY[ko] MMM D[a] LT"},calendar:{sameDay:"[gaur] LT[etan]",nextDay:"[bihar] LT[etan]",nextWeek:"dddd LT[etan]",lastDay:"[atzo] LT[etan]",lastWeek:"[aurreko] dddd LT[etan]",sameElse:"L"},relativeTime:{future:"%s barru",past:"duela %s",s:"segundo batzuk",m:"minutu bat",mm:"%d minutu",h:"ordu bat",hh:"%d ordu",d:"egun bat",dd:"%d egun",M:"hilabete bat",MM:"%d hilabete",y:"urte bat",yy:"%d urte"},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){var b={1:"۱",2:"۲",3:"۳",4:"۴",5:"۵",6:"۶",7:"۷",8:"۸",9:"۹",0:"۰"},c={"۱":"1","۲":"2","۳":"3","۴":"4","۵":"5","۶":"6","۷":"7","۸":"8","۹":"9","۰":"0"};return a.defineLocale("fa",{months:"ژانویه_فوریه_مارس_آوریل_مه_ژوئن_ژوئیه_اوت_سپتامبر_اکتبر_نوامبر_دسامبر".split("_"),monthsShort:"ژانویه_فوریه_مارس_آوریل_مه_ژوئن_ژوئیه_اوت_سپتامبر_اکتبر_نوامبر_دسامبر".split("_"),weekdays:"یک‌شنبه_دوشنبه_سه‌شنبه_چهارشنبه_پنج‌شنبه_جمعه_شنبه".split("_"),weekdaysShort:"یک‌شنبه_دوشنبه_سه‌شنبه_چهارشنبه_پنج‌شنبه_جمعه_شنبه".split("_"),weekdaysMin:"ی_د_س_چ_پ_ج_ش".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd, D MMMM YYYY LT"},meridiemParse:/قبل از ظهر|بعد از ظهر/,isPM:function(a){return/بعد از ظهر/.test(a)},meridiem:function(a){return 12>a?"قبل از ظهر":"بعد از ظهر"},calendar:{sameDay:"[امروز ساعت] LT",nextDay:"[فردا ساعت] LT",nextWeek:"dddd [ساعت] LT",lastDay:"[دیروز ساعت] LT",lastWeek:"dddd [پیش] [ساعت] LT",sameElse:"L"},relativeTime:{future:"در %s",past:"%s پیش",s:"چندین ثانیه",m:"یک دقیقه",mm:"%d دقیقه",h:"یک ساعت",hh:"%d ساعت",d:"یک روز",dd:"%d روز",M:"یک ماه",MM:"%d ماه",y:"یک سال",yy:"%d سال"},preparse:function(a){return a.replace(/[۰-۹]/g,function(a){return c[a]}).replace(/،/g,",")},postformat:function(a){return a.replace(/\d/g,function(a){return b[a]}).replace(/,/g,"،")},ordinalParse:/\d{1,2}م/,ordinal:"%dم",week:{dow:6,doy:12}})}),function(a){a(vb)}(function(a){function b(a,b,d,e){var f="";switch(d){case"s":return e?"muutaman sekunnin":"muutama sekunti";case"m":return e?"minuutin":"minuutti";case"mm":f=e?"minuutin":"minuuttia";break;case"h":return e?"tunnin":"tunti";case"hh":f=e?"tunnin":"tuntia";break;case"d":return e?"päivän":"päivä";case"dd":f=e?"päivän":"päivää";break;case"M":return e?"kuukauden":"kuukausi";case"MM":f=e?"kuukauden":"kuukautta";break;case"y":return e?"vuoden":"vuosi";case"yy":f=e?"vuoden":"vuotta"}return f=c(a,e)+" "+f}function c(a,b){return 10>a?b?e[a]:d[a]:a}var d="nolla yksi kaksi kolme neljä viisi kuusi seitsemän kahdeksan yhdeksän".split(" "),e=["nolla","yhden","kahden","kolmen","neljän","viiden","kuuden",d[7],d[8],d[9]];return a.defineLocale("fi",{months:"tammikuu_helmikuu_maaliskuu_huhtikuu_toukokuu_kesäkuu_heinäkuu_elokuu_syyskuu_lokakuu_marraskuu_joulukuu".split("_"),monthsShort:"tammi_helmi_maalis_huhti_touko_kesä_heinä_elo_syys_loka_marras_joulu".split("_"),weekdays:"sunnuntai_maanantai_tiistai_keskiviikko_torstai_perjantai_lauantai".split("_"),weekdaysShort:"su_ma_ti_ke_to_pe_la".split("_"),weekdaysMin:"su_ma_ti_ke_to_pe_la".split("_"),longDateFormat:{LT:"HH.mm",LTS:"HH.mm.ss",L:"DD.MM.YYYY",LL:"Do MMMM[ta] YYYY",LLL:"Do MMMM[ta] YYYY, [klo] LT",LLLL:"dddd, Do MMMM[ta] YYYY, [klo] LT",l:"D.M.YYYY",ll:"Do MMM YYYY",lll:"Do MMM YYYY, [klo] LT",llll:"ddd, Do MMM YYYY, [klo] LT"},calendar:{sameDay:"[tänään] [klo] LT",nextDay:"[huomenna] [klo] LT",nextWeek:"dddd [klo] LT",lastDay:"[eilen] [klo] LT",lastWeek:"[viime] dddd[na] [klo] LT",sameElse:"L"},relativeTime:{future:"%s päästä",past:"%s sitten",s:b,m:b,mm:b,h:b,hh:b,d:b,dd:b,M:b,MM:b,y:b,yy:b},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("fo",{months:"januar_februar_mars_apríl_mai_juni_juli_august_september_oktober_november_desember".split("_"),monthsShort:"jan_feb_mar_apr_mai_jun_jul_aug_sep_okt_nov_des".split("_"),weekdays:"sunnudagur_mánadagur_týsdagur_mikudagur_hósdagur_fríggjadagur_leygardagur".split("_"),weekdaysShort:"sun_mán_týs_mik_hós_frí_ley".split("_"),weekdaysMin:"su_má_tý_mi_hó_fr_le".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D. MMMM, YYYY LT"},calendar:{sameDay:"[Í dag kl.] LT",nextDay:"[Í morgin kl.] LT",nextWeek:"dddd [kl.] LT",lastDay:"[Í gjár kl.] LT",lastWeek:"[síðstu] dddd [kl] LT",sameElse:"L"},relativeTime:{future:"um %s",past:"%s síðani",s:"fá sekund",m:"ein minutt",mm:"%d minuttir",h:"ein tími",hh:"%d tímar",d:"ein dagur",dd:"%d dagar",M:"ein mánaði",MM:"%d mánaðir",y:"eitt ár",yy:"%d ár"},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("fr-ca",{months:"janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre".split("_"),monthsShort:"janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.".split("_"),weekdays:"dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),weekdaysShort:"dim._lun._mar._mer._jeu._ven._sam.".split("_"),weekdaysMin:"Di_Lu_Ma_Me_Je_Ve_Sa".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"YYYY-MM-DD",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D MMMM YYYY LT"},calendar:{sameDay:"[Aujourd'hui à] LT",nextDay:"[Demain à] LT",nextWeek:"dddd [à] LT",lastDay:"[Hier à] LT",lastWeek:"dddd [dernier à] LT",sameElse:"L"},relativeTime:{future:"dans %s",past:"il y a %s",s:"quelques secondes",m:"une minute",mm:"%d minutes",h:"une heure",hh:"%d heures",d:"un jour",dd:"%d jours",M:"un mois",MM:"%d mois",y:"un an",yy:"%d ans"},ordinalParse:/\d{1,2}(er|)/,ordinal:function(a){return a+(1===a?"er":"")}})}),function(a){a(vb)}(function(a){return a.defineLocale("fr",{months:"janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre".split("_"),monthsShort:"janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.".split("_"),weekdays:"dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),weekdaysShort:"dim._lun._mar._mer._jeu._ven._sam.".split("_"),weekdaysMin:"Di_Lu_Ma_Me_Je_Ve_Sa".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D MMMM YYYY LT"},calendar:{sameDay:"[Aujourd'hui à] LT",nextDay:"[Demain à] LT",nextWeek:"dddd [à] LT",lastDay:"[Hier à] LT",lastWeek:"dddd [dernier à] LT",sameElse:"L"},relativeTime:{future:"dans %s",past:"il y a %s",s:"quelques secondes",m:"une minute",mm:"%d minutes",h:"une heure",hh:"%d heures",d:"un jour",dd:"%d jours",M:"un mois",MM:"%d mois",y:"un an",yy:"%d ans"},ordinalParse:/\d{1,2}(er|)/,ordinal:function(a){return a+(1===a?"er":"")},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){var b="jan._feb._mrt._apr._mai_jun._jul._aug._sep._okt._nov._des.".split("_"),c="jan_feb_mrt_apr_mai_jun_jul_aug_sep_okt_nov_des".split("_");return a.defineLocale("fy",{months:"jannewaris_febrewaris_maart_april_maaie_juny_july_augustus_septimber_oktober_novimber_desimber".split("_"),monthsShort:function(a,d){return/-MMM-/.test(d)?c[a.month()]:b[a.month()]},weekdays:"snein_moandei_tiisdei_woansdei_tongersdei_freed_sneon".split("_"),weekdaysShort:"si._mo._ti._wo._to._fr._so.".split("_"),weekdaysMin:"Si_Mo_Ti_Wo_To_Fr_So".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD-MM-YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D MMMM YYYY LT"},calendar:{sameDay:"[hjoed om] LT",nextDay:"[moarn om] LT",nextWeek:"dddd [om] LT",lastDay:"[juster om] LT",lastWeek:"[ôfrûne] dddd [om] LT",sameElse:"L"},relativeTime:{future:"oer %s",past:"%s lyn",s:"in pear sekonden",m:"ien minút",mm:"%d minuten",h:"ien oere",hh:"%d oeren",d:"ien dei",dd:"%d dagen",M:"ien moanne",MM:"%d moannen",y:"ien jier",yy:"%d jierren"},ordinalParse:/\d{1,2}(ste|de)/,ordinal:function(a){return a+(1===a||8===a||a>=20?"ste":"de")},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("gl",{months:"Xaneiro_Febreiro_Marzo_Abril_Maio_Xuño_Xullo_Agosto_Setembro_Outubro_Novembro_Decembro".split("_"),monthsShort:"Xan._Feb._Mar._Abr._Mai._Xuñ._Xul._Ago._Set._Out._Nov._Dec.".split("_"),weekdays:"Domingo_Luns_Martes_Mércores_Xoves_Venres_Sábado".split("_"),weekdaysShort:"Dom._Lun._Mar._Mér._Xov._Ven._Sáb.".split("_"),weekdaysMin:"Do_Lu_Ma_Mé_Xo_Ve_Sá".split("_"),longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D MMMM YYYY LT"},calendar:{sameDay:function(){return"[hoxe "+(1!==this.hours()?"ás":"á")+"] LT"},nextDay:function(){return"[mañá "+(1!==this.hours()?"ás":"á")+"] LT"},nextWeek:function(){return"dddd ["+(1!==this.hours()?"ás":"a")+"] LT"},lastDay:function(){return"[onte "+(1!==this.hours()?"á":"a")+"] LT"},lastWeek:function(){return"[o] dddd [pasado "+(1!==this.hours()?"ás":"a")+"] LT"},sameElse:"L"},relativeTime:{future:function(a){return"uns segundos"===a?"nuns segundos":"en "+a},past:"hai %s",s:"uns segundos",m:"un minuto",mm:"%d minutos",h:"unha hora",hh:"%d horas",d:"un día",dd:"%d días",M:"un mes",MM:"%d meses",y:"un ano",yy:"%d anos"},ordinalParse:/\d{1,2}º/,ordinal:"%dº",week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){return a.defineLocale("he",{months:"ינואר_פברואר_מרץ_אפריל_מאי_יוני_יולי_אוגוסט_ספטמבר_אוקטובר_נובמבר_דצמבר".split("_"),monthsShort:"ינו׳_פבר׳_מרץ_אפר׳_מאי_יוני_יולי_אוג׳_ספט׳_אוק׳_נוב׳_דצמ׳".split("_"),weekdays:"ראשון_שני_שלישי_רביעי_חמישי_שישי_שבת".split("_"),weekdaysShort:"א׳_ב׳_ג׳_ד׳_ה׳_ו׳_ש׳".split("_"),weekdaysMin:"א_ב_ג_ד_ה_ו_ש".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D [ב]MMMM YYYY",LLL:"D [ב]MMMM YYYY LT",LLLL:"dddd, D [ב]MMMM YYYY LT",l:"D/M/YYYY",ll:"D MMM YYYY",lll:"D MMM YYYY LT",llll:"ddd, D MMM YYYY LT"},calendar:{sameDay:"[היום ב־]LT",nextDay:"[מחר ב־]LT",nextWeek:"dddd [בשעה] LT",lastDay:"[אתמול ב־]LT",lastWeek:"[ביום] dddd [האחרון בשעה] LT",sameElse:"L"},relativeTime:{future:"בעוד %s",past:"לפני %s",s:"מספר שניות",m:"דקה",mm:"%d דקות",h:"שעה",hh:function(a){return 2===a?"שעתיים":a+" שעות"},d:"יום",dd:function(a){return 2===a?"יומיים":a+" ימים"},M:"חודש",MM:function(a){return 2===a?"חודשיים":a+" חודשים"},y:"שנה",yy:function(a){return 2===a?"שנתיים":a%10===0&&10!==a?a+" שנה":a+" שנים"}}})}),function(a){a(vb)}(function(a){var b={1:"१",2:"२",3:"३",4:"४",5:"५",6:"६",7:"७",8:"८",9:"९",0:"०"},c={"१":"1","२":"2","३":"3","४":"4","५":"5","६":"6","७":"7","८":"8","९":"9","०":"0"};return a.defineLocale("hi",{months:"जनवरी_फ़रवरी_मार्च_अप्रैल_मई_जून_जुलाई_अगस्त_सितम्बर_अक्टूबर_नवम्बर_दिसम्बर".split("_"),monthsShort:"जन._फ़र._मार्च_अप्रै._मई_जून_जुल._अग._सित._अक्टू._नव._दिस.".split("_"),weekdays:"रविवार_सोमवार_मंगलवार_बुधवार_गुरूवार_शुक्रवार_शनिवार".split("_"),weekdaysShort:"रवि_सोम_मंगल_बुध_गुरू_शुक्र_शनि".split("_"),weekdaysMin:"र_सो_मं_बु_गु_शु_श".split("_"),longDateFormat:{LT:"A h:mm बजे",LTS:"A h:mm:ss बजे",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, LT",LLLL:"dddd, D MMMM YYYY, LT"},calendar:{sameDay:"[आज] LT",nextDay:"[कल] LT",nextWeek:"dddd, LT",lastDay:"[कल] LT",lastWeek:"[पिछले] dddd, LT",sameElse:"L"},relativeTime:{future:"%s में",past:"%s पहले",s:"कुछ ही क्षण",m:"एक मिनट",mm:"%d मिनट",h:"एक घंटा",hh:"%d घंटे",d:"एक दिन",dd:"%d दिन",M:"एक महीने",MM:"%d महीने",y:"एक वर्ष",yy:"%d वर्ष"},preparse:function(a){return a.replace(/[१२३४५६७८९०]/g,function(a){return c[a]})},postformat:function(a){return a.replace(/\d/g,function(a){return b[a]})},meridiemParse:/रात|सुबह|दोपहर|शाम/,meridiemHour:function(a,b){return 12===a&&(a=0),"रात"===b?4>a?a:a+12:"सुबह"===b?a:"दोपहर"===b?a>=10?a:a+12:"शाम"===b?a+12:void 0},meridiem:function(a){return 4>a?"रात":10>a?"सुबह":17>a?"दोपहर":20>a?"शाम":"रात"},week:{dow:0,doy:6}})}),function(a){a(vb)}(function(a){function b(a,b,c){var d=a+" ";switch(c){case"m":return b?"jedna minuta":"jedne minute";case"mm":return d+=1===a?"minuta":2===a||3===a||4===a?"minute":"minuta";case"h":return b?"jedan sat":"jednog sata";case"hh":return d+=1===a?"sat":2===a||3===a||4===a?"sata":"sati";case"dd":return d+=1===a?"dan":"dana";case"MM":return d+=1===a?"mjesec":2===a||3===a||4===a?"mjeseca":"mjeseci";case"yy":return d+=1===a?"godina":2===a||3===a||4===a?"godine":"godina"}}return a.defineLocale("hr",{months:"sječanj_veljača_ožujak_travanj_svibanj_lipanj_srpanj_kolovoz_rujan_listopad_studeni_prosinac".split("_"),monthsShort:"sje._vel._ožu._tra._svi._lip._srp._kol._ruj._lis._stu._pro.".split("_"),weekdays:"nedjelja_ponedjeljak_utorak_srijeda_četvrtak_petak_subota".split("_"),weekdaysShort:"ned._pon._uto._sri._čet._pet._sub.".split("_"),weekdaysMin:"ne_po_ut_sr_če_pe_su".split("_"),longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"DD. MM. YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY LT",LLLL:"dddd, D. MMMM YYYY LT"},calendar:{sameDay:"[danas u] LT",nextDay:"[sutra u] LT",nextWeek:function(){switch(this.day()){case 0:return"[u] [nedjelju] [u] LT";case 3:return"[u] [srijedu] [u] LT";case 6:return"[u] [subotu] [u] LT";case 1:case 2:case 4:case 5:return"[u] dddd [u] LT"}},lastDay:"[jučer u] LT",lastWeek:function(){switch(this.day()){case 0:case 3:return"[prošlu] dddd [u] LT";case 6:return"[prošle] [subote] [u] LT";case 1:case 2:case 4:case 5:return"[prošli] dddd [u] LT"}},sameElse:"L"},relativeTime:{future:"za %s",past:"prije %s",s:"par sekundi",m:b,mm:b,h:b,hh:b,d:"dan",dd:b,M:"mjesec",MM:b,y:"godinu",yy:b},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){function b(a,b,c,d){var e=a;switch(c){case"s":return d||b?"néhány másodperc":"néhány másodperce";case"m":return"egy"+(d||b?" perc":" perce");case"mm":return e+(d||b?" perc":" perce");case"h":return"egy"+(d||b?" óra":" órája");case"hh":return e+(d||b?" óra":" órája");case"d":return"egy"+(d||b?" nap":" napja");case"dd":return e+(d||b?" nap":" napja");case"M":return"egy"+(d||b?" hónap":" hónapja");case"MM":return e+(d||b?" hónap":" hónapja");case"y":return"egy"+(d||b?" év":" éve");case"yy":return e+(d||b?" év":" éve")}return""}function c(a){return(a?"":"[múlt] ")+"["+d[this.day()]+"] LT[-kor]"}var d="vasárnap hétfőn kedden szerdán csütörtökön pénteken szombaton".split(" ");return a.defineLocale("hu",{months:"január_február_március_április_május_június_július_augusztus_szeptember_október_november_december".split("_"),monthsShort:"jan_feb_márc_ápr_máj_jún_júl_aug_szept_okt_nov_dec".split("_"),weekdays:"vasárnap_hétfő_kedd_szerda_csütörtök_péntek_szombat".split("_"),weekdaysShort:"vas_hét_kedd_sze_csüt_pén_szo".split("_"),weekdaysMin:"v_h_k_sze_cs_p_szo".split("_"),longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"YYYY.MM.DD.",LL:"YYYY. MMMM D.",LLL:"YYYY. MMMM D., LT",LLLL:"YYYY. MMMM D., dddd LT"},meridiemParse:/de|du/i,isPM:function(a){return"u"===a.charAt(1).toLowerCase()},meridiem:function(a,b,c){return 12>a?c===!0?"de":"DE":c===!0?"du":"DU"},calendar:{sameDay:"[ma] LT[-kor]",nextDay:"[holnap] LT[-kor]",nextWeek:function(){return c.call(this,!0)},lastDay:"[tegnap] LT[-kor]",lastWeek:function(){return c.call(this,!1)},sameElse:"L"},relativeTime:{future:"%s múlva",past:"%s",s:b,m:b,mm:b,h:b,hh:b,d:b,dd:b,M:b,MM:b,y:b,yy:b},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){function b(a,b){var c={nominative:"հունվար_փետրվար_մարտ_ապրիլ_մայիս_հունիս_հուլիս_օգոստոս_սեպտեմբեր_հոկտեմբեր_նոյեմբեր_դեկտեմբեր".split("_"),accusative:"հունվարի_փետրվարի_մարտի_ապրիլի_մայիսի_հունիսի_հուլիսի_օգոստոսի_սեպտեմբերի_հոկտեմբերի_նոյեմբերի_դեկտեմբերի".split("_")},d=/D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/.test(b)?"accusative":"nominative";return c[d][a.month()]}function c(a){var b="հնվ_փտր_մրտ_ապր_մյս_հնս_հլս_օգս_սպտ_հկտ_նմբ_դկտ".split("_");return b[a.month()]}function d(a){var b="կիրակի_երկուշաբթի_երեքշաբթի_չորեքշաբթի_հինգշաբթի_ուրբաթ_շաբաթ".split("_");return b[a.day()]}return a.defineLocale("hy-am",{months:b,monthsShort:c,weekdays:d,weekdaysShort:"կրկ_երկ_երք_չրք_հնգ_ուրբ_շբթ".split("_"),weekdaysMin:"կրկ_երկ_երք_չրք_հնգ_ուրբ_շբթ".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY թ.",LLL:"D MMMM YYYY թ., LT",LLLL:"dddd, D MMMM YYYY թ., LT"},calendar:{sameDay:"[այսօր] LT",nextDay:"[վաղը] LT",lastDay:"[երեկ] LT",nextWeek:function(){return"dddd [օրը ժամը] LT"},lastWeek:function(){return"[անցած] dddd [օրը ժամը] LT"},sameElse:"L"},relativeTime:{future:"%s հետո",past:"%s առաջ",s:"մի քանի վայրկյան",m:"րոպե",mm:"%d րոպե",h:"ժամ",hh:"%d ժամ",d:"օր",dd:"%d օր",M:"ամիս",MM:"%d ամիս",y:"տարի",yy:"%d տարի"},meridiemParse:/գիշերվա|առավոտվա|ցերեկվա|երեկոյան/,isPM:function(a){return/^(ցերեկվա|երեկոյան)$/.test(a)},meridiem:function(a){return 4>a?"գիշերվա":12>a?"առավոտվա":17>a?"ցերեկվա":"երեկոյան"},ordinalParse:/\d{1,2}|\d{1,2}-(ին|րդ)/,ordinal:function(a,b){switch(b){case"DDD":case"w":case"W":case"DDDo":return 1===a?a+"-ին":a+"-րդ";default:return a}},week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){return a.defineLocale("id",{months:"Januari_Februari_Maret_April_Mei_Juni_Juli_Agustus_September_Oktober_November_Desember".split("_"),monthsShort:"Jan_Feb_Mar_Apr_Mei_Jun_Jul_Ags_Sep_Okt_Nov_Des".split("_"),weekdays:"Minggu_Senin_Selasa_Rabu_Kamis_Jumat_Sabtu".split("_"),weekdaysShort:"Min_Sen_Sel_Rab_Kam_Jum_Sab".split("_"),weekdaysMin:"Mg_Sn_Sl_Rb_Km_Jm_Sb".split("_"),longDateFormat:{LT:"HH.mm",LTS:"LT.ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY [pukul] LT",LLLL:"dddd, D MMMM YYYY [pukul] LT"},meridiemParse:/pagi|siang|sore|malam/,meridiemHour:function(a,b){return 12===a&&(a=0),"pagi"===b?a:"siang"===b?a>=11?a:a+12:"sore"===b||"malam"===b?a+12:void 0},meridiem:function(a){return 11>a?"pagi":15>a?"siang":19>a?"sore":"malam"},calendar:{sameDay:"[Hari ini pukul] LT",nextDay:"[Besok pukul] LT",nextWeek:"dddd [pukul] LT",lastDay:"[Kemarin pukul] LT",lastWeek:"dddd [lalu pukul] LT",sameElse:"L"},relativeTime:{future:"dalam %s",past:"%s yang lalu",s:"beberapa detik",m:"semenit",mm:"%d menit",h:"sejam",hh:"%d jam",d:"sehari",dd:"%d hari",M:"sebulan",MM:"%d bulan",y:"setahun",yy:"%d tahun"},week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){function b(a){return a%100===11?!0:a%10===1?!1:!0}function c(a,c,d,e){var f=a+" ";switch(d){case"s":return c||e?"nokkrar sekúndur":"nokkrum sekúndum";case"m":return c?"mínúta":"mínútu";case"mm":return b(a)?f+(c||e?"mínútur":"mínútum"):c?f+"mínúta":f+"mínútu";case"hh":return b(a)?f+(c||e?"klukkustundir":"klukkustundum"):f+"klukkustund";case"d":return c?"dagur":e?"dag":"degi";case"dd":return b(a)?c?f+"dagar":f+(e?"daga":"dögum"):c?f+"dagur":f+(e?"dag":"degi");case"M":return c?"mánuður":e?"mánuð":"mánuði";case"MM":return b(a)?c?f+"mánuðir":f+(e?"mánuði":"mánuðum"):c?f+"mánuður":f+(e?"mánuð":"mánuði");case"y":return c||e?"ár":"ári";case"yy":return b(a)?f+(c||e?"ár":"árum"):f+(c||e?"ár":"ári")}}return a.defineLocale("is",{months:"janúar_febrúar_mars_apríl_maí_júní_júlí_ágúst_september_október_nóvember_desember".split("_"),monthsShort:"jan_feb_mar_apr_maí_jún_júl_ágú_sep_okt_nóv_des".split("_"),weekdays:"sunnudagur_mánudagur_þriðjudagur_miðvikudagur_fimmtudagur_föstudagur_laugardagur".split("_"),weekdaysShort:"sun_mán_þri_mið_fim_fös_lau".split("_"),weekdaysMin:"Su_Má_Þr_Mi_Fi_Fö_La".split("_"),longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY [kl.] LT",LLLL:"dddd, D. MMMM YYYY [kl.] LT"},calendar:{sameDay:"[í dag kl.] LT",nextDay:"[á morgun kl.] LT",nextWeek:"dddd [kl.] LT",lastDay:"[í gær kl.] LT",lastWeek:"[síðasta] dddd [kl.] LT",sameElse:"L"},relativeTime:{future:"eftir %s",past:"fyrir %s síðan",s:c,m:c,mm:c,h:"klukkustund",hh:c,d:c,dd:c,M:c,MM:c,y:c,yy:c},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("it",{months:"gennaio_febbraio_marzo_aprile_maggio_giugno_luglio_agosto_settembre_ottobre_novembre_dicembre".split("_"),monthsShort:"gen_feb_mar_apr_mag_giu_lug_ago_set_ott_nov_dic".split("_"),weekdays:"Domenica_Lunedì_Martedì_Mercoledì_Giovedì_Venerdì_Sabato".split("_"),weekdaysShort:"Dom_Lun_Mar_Mer_Gio_Ven_Sab".split("_"),weekdaysMin:"D_L_Ma_Me_G_V_S".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd, D MMMM YYYY LT"},calendar:{sameDay:"[Oggi alle] LT",nextDay:"[Domani alle] LT",nextWeek:"dddd [alle] LT",lastDay:"[Ieri alle] LT",lastWeek:function(){switch(this.day()){case 0:return"[la scorsa] dddd [alle] LT";default:return"[lo scorso] dddd [alle] LT"}},sameElse:"L"},relativeTime:{future:function(a){return(/^[0-9].+$/.test(a)?"tra":"in")+" "+a},past:"%s fa",s:"alcuni secondi",m:"un minuto",mm:"%d minuti",h:"un'ora",hh:"%d ore",d:"un giorno",dd:"%d giorni",M:"un mese",MM:"%d mesi",y:"un anno",yy:"%d anni"},ordinalParse:/\d{1,2}º/,ordinal:"%dº",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("ja",{months:"1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),monthsShort:"1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),weekdays:"日曜日_月曜日_火曜日_水曜日_木曜日_金曜日_土曜日".split("_"),weekdaysShort:"日_月_火_水_木_金_土".split("_"),weekdaysMin:"日_月_火_水_木_金_土".split("_"),longDateFormat:{LT:"Ah時m分",LTS:"LTs秒",L:"YYYY/MM/DD",LL:"YYYY年M月D日",LLL:"YYYY年M月D日LT",LLLL:"YYYY年M月D日LT dddd"},meridiemParse:/午前|午後/i,isPM:function(a){return"午後"===a},meridiem:function(a){return 12>a?"午前":"午後"},calendar:{sameDay:"[今日] LT",nextDay:"[明日] LT",nextWeek:"[来週]dddd LT",lastDay:"[昨日] LT",lastWeek:"[前週]dddd LT",sameElse:"L"},relativeTime:{future:"%s後",past:"%s前",s:"数秒",m:"1分",mm:"%d分",h:"1時間",hh:"%d時間",d:"1日",dd:"%d日",M:"1ヶ月",MM:"%dヶ月",y:"1年",yy:"%d年"}})}),function(a){a(vb)}(function(a){function b(a,b){var c={nominative:"იანვარი_თებერვალი_მარტი_აპრილი_მაისი_ივნისი_ივლისი_აგვისტო_სექტემბერი_ოქტომბერი_ნოემბერი_დეკემბერი".split("_"),accusative:"იანვარს_თებერვალს_მარტს_აპრილის_მაისს_ივნისს_ივლისს_აგვისტს_სექტემბერს_ოქტომბერს_ნოემბერს_დეკემბერს".split("_")},d=/D[oD] *MMMM?/.test(b)?"accusative":"nominative";return c[d][a.month()]}function c(a,b){var c={nominative:"კვირა_ორშაბათი_სამშაბათი_ოთხშაბათი_ხუთშაბათი_პარასკევი_შაბათი".split("_"),accusative:"კვირას_ორშაბათს_სამშაბათს_ოთხშაბათს_ხუთშაბათს_პარასკევს_შაბათს".split("_")},d=/(წინა|შემდეგ)/.test(b)?"accusative":"nominative";return c[d][a.day()]}return a.defineLocale("ka",{months:b,monthsShort:"იან_თებ_მარ_აპრ_მაი_ივნ_ივლ_აგვ_სექ_ოქტ_ნოე_დეკ".split("_"),weekdays:c,weekdaysShort:"კვი_ორშ_სამ_ოთხ_ხუთ_პარ_შაბ".split("_"),weekdaysMin:"კვ_ორ_სა_ოთ_ხუ_პა_შა".split("_"),longDateFormat:{LT:"h:mm A",LTS:"h:mm:ss A",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd, D MMMM YYYY LT"},calendar:{sameDay:"[დღეს] LT[-ზე]",nextDay:"[ხვალ] LT[-ზე]",lastDay:"[გუშინ] LT[-ზე]",nextWeek:"[შემდეგ] dddd LT[-ზე]",lastWeek:"[წინა] dddd LT-ზე",sameElse:"L"},relativeTime:{future:function(a){return/(წამი|წუთი|საათი|წელი)/.test(a)?a.replace(/ი$/,"ში"):a+"ში"},past:function(a){return/(წამი|წუთი|საათი|დღე|თვე)/.test(a)?a.replace(/(ი|ე)$/,"ის წინ"):/წელი/.test(a)?a.replace(/წელი$/,"წლის წინ"):void 0},s:"რამდენიმე წამი",m:"წუთი",mm:"%d წუთი",h:"საათი",hh:"%d საათი",d:"დღე",dd:"%d დღე",M:"თვე",MM:"%d თვე",y:"წელი",yy:"%d წელი"},ordinalParse:/0|1-ლი|მე-\d{1,2}|\d{1,2}-ე/,ordinal:function(a){return 0===a?a:1===a?a+"-ლი":20>a||100>=a&&a%20===0||a%100===0?"მე-"+a:a+"-ე"},week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){return a.defineLocale("km",{months:"មករា_កុម្ភៈ_មិនា_មេសា_ឧសភា_មិថុនា_កក្កដា_សីហា_កញ្ញា_តុលា_វិច្ឆិកា_ធ្នូ".split("_"),monthsShort:"មករា_កុម្ភៈ_មិនា_មេសា_ឧសភា_មិថុនា_កក្កដា_សីហា_កញ្ញា_តុលា_វិច្ឆិកា_ធ្នូ".split("_"),weekdays:"អាទិត្យ_ច័ន្ទ_អង្គារ_ពុធ_ព្រហស្បតិ៍_សុក្រ_សៅរ៍".split("_"),weekdaysShort:"អាទិត្យ_ច័ន្ទ_អង្គារ_ពុធ_ព្រហស្បតិ៍_សុក្រ_សៅរ៍".split("_"),weekdaysMin:"អាទិត្យ_ច័ន្ទ_អង្គារ_ពុធ_ព្រហស្បតិ៍_សុក្រ_សៅរ៍".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd, D MMMM YYYY LT"},calendar:{sameDay:"[ថ្ងៃនៈ ម៉ោង] LT",nextDay:"[ស្អែក ម៉ោង] LT",nextWeek:"dddd [ម៉ោង] LT",lastDay:"[ម្សិលមិញ ម៉ោង] LT",lastWeek:"dddd [សប្តាហ៍មុន] [ម៉ោង] LT",sameElse:"L"},relativeTime:{future:"%sទៀត",past:"%sមុន",s:"ប៉ុន្មានវិនាទី",m:"មួយនាទី",mm:"%d នាទី",h:"មួយម៉ោង",hh:"%d ម៉ោង",d:"មួយថ្ងៃ",dd:"%d ថ្ងៃ",M:"មួយខែ",MM:"%d ខែ",y:"មួយឆ្នាំ",yy:"%d ឆ្នាំ"},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("ko",{months:"1월_2월_3월_4월_5월_6월_7월_8월_9월_10월_11월_12월".split("_"),monthsShort:"1월_2월_3월_4월_5월_6월_7월_8월_9월_10월_11월_12월".split("_"),weekdays:"일요일_월요일_화요일_수요일_목요일_금요일_토요일".split("_"),weekdaysShort:"일_월_화_수_목_금_토".split("_"),weekdaysMin:"일_월_화_수_목_금_토".split("_"),longDateFormat:{LT:"A h시 m분",LTS:"A h시 m분 s초",L:"YYYY.MM.DD",LL:"YYYY년 MMMM D일",LLL:"YYYY년 MMMM D일 LT",LLLL:"YYYY년 MMMM D일 dddd LT"},calendar:{sameDay:"오늘 LT",nextDay:"내일 LT",nextWeek:"dddd LT",lastDay:"어제 LT",lastWeek:"지난주 dddd LT",sameElse:"L"},relativeTime:{future:"%s 후",past:"%s 전",s:"몇초",ss:"%d초",m:"일분",mm:"%d분",h:"한시간",hh:"%d시간",d:"하루",dd:"%d일",M:"한달",MM:"%d달",y:"일년",yy:"%d년"},ordinalParse:/\d{1,2}일/,ordinal:"%d일",meridiemParse:/오전|오후/,isPM:function(a){return"오후"===a},meridiem:function(a){return 12>a?"오전":"오후"}})}),function(a){a(vb)}(function(a){function b(a,b,c){var d={m:["eng Minutt","enger Minutt"],h:["eng Stonn","enger Stonn"],d:["een Dag","engem Dag"],M:["ee Mount","engem Mount"],y:["ee Joer","engem Joer"]};return b?d[c][0]:d[c][1]}function c(a){var b=a.substr(0,a.indexOf(" "));return e(b)?"a "+a:"an "+a}function d(a){var b=a.substr(0,a.indexOf(" "));return e(b)?"viru "+a:"virun "+a}function e(a){if(a=parseInt(a,10),isNaN(a))return!1;if(0>a)return!0;if(10>a)return a>=4&&7>=a?!0:!1;if(100>a){var b=a%10,c=a/10;return e(0===b?c:b)}if(1e4>a){for(;a>=10;)a/=10;return e(a)}return a/=1e3,e(a)}return a.defineLocale("lb",{months:"Januar_Februar_Mäerz_Abrëll_Mee_Juni_Juli_August_September_Oktober_November_Dezember".split("_"),monthsShort:"Jan._Febr._Mrz._Abr._Mee_Jun._Jul._Aug._Sept._Okt._Nov._Dez.".split("_"),weekdays:"Sonndeg_Méindeg_Dënschdeg_Mëttwoch_Donneschdeg_Freideg_Samschdeg".split("_"),weekdaysShort:"So._Mé._Dë._Më._Do._Fr._Sa.".split("_"),weekdaysMin:"So_Mé_Dë_Më_Do_Fr_Sa".split("_"),longDateFormat:{LT:"H:mm [Auer]",LTS:"H:mm:ss [Auer]",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY LT",LLLL:"dddd, D. MMMM YYYY LT"},calendar:{sameDay:"[Haut um] LT",sameElse:"L",nextDay:"[Muer um] LT",nextWeek:"dddd [um] LT",lastDay:"[Gëschter um] LT",lastWeek:function(){switch(this.day()){case 2:case 4:return"[Leschten] dddd [um] LT";default:return"[Leschte] dddd [um] LT"}}},relativeTime:{future:c,past:d,s:"e puer Sekonnen",m:b,mm:"%d Minutten",h:b,hh:"%d Stonnen",d:b,dd:"%d Deeg",M:b,MM:"%d Méint",y:b,yy:"%d Joer"},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){function b(a,b,c,d){return b?"kelios sekundės":d?"kelių sekundžių":"kelias sekundes"}function c(a,b,c,d){return b?e(c)[0]:d?e(c)[1]:e(c)[2]
}function d(a){return a%10===0||a>10&&20>a}function e(a){return h[a].split("_")}function f(a,b,f,g){var h=a+" ";return 1===a?h+c(a,b,f[0],g):b?h+(d(a)?e(f)[1]:e(f)[0]):g?h+e(f)[1]:h+(d(a)?e(f)[1]:e(f)[2])}function g(a,b){var c=-1===b.indexOf("dddd HH:mm"),d=i[a.day()];return c?d:d.substring(0,d.length-2)+"į"}var h={m:"minutė_minutės_minutę",mm:"minutės_minučių_minutes",h:"valanda_valandos_valandą",hh:"valandos_valandų_valandas",d:"diena_dienos_dieną",dd:"dienos_dienų_dienas",M:"mėnuo_mėnesio_mėnesį",MM:"mėnesiai_mėnesių_mėnesius",y:"metai_metų_metus",yy:"metai_metų_metus"},i="sekmadienis_pirmadienis_antradienis_trečiadienis_ketvirtadienis_penktadienis_šeštadienis".split("_");return a.defineLocale("lt",{months:"sausio_vasario_kovo_balandžio_gegužės_birželio_liepos_rugpjūčio_rugsėjo_spalio_lapkričio_gruodžio".split("_"),monthsShort:"sau_vas_kov_bal_geg_bir_lie_rgp_rgs_spa_lap_grd".split("_"),weekdays:g,weekdaysShort:"Sek_Pir_Ant_Tre_Ket_Pen_Šeš".split("_"),weekdaysMin:"S_P_A_T_K_Pn_Š".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"YYYY-MM-DD",LL:"YYYY [m.] MMMM D [d.]",LLL:"YYYY [m.] MMMM D [d.], LT [val.]",LLLL:"YYYY [m.] MMMM D [d.], dddd, LT [val.]",l:"YYYY-MM-DD",ll:"YYYY [m.] MMMM D [d.]",lll:"YYYY [m.] MMMM D [d.], LT [val.]",llll:"YYYY [m.] MMMM D [d.], ddd, LT [val.]"},calendar:{sameDay:"[Šiandien] LT",nextDay:"[Rytoj] LT",nextWeek:"dddd LT",lastDay:"[Vakar] LT",lastWeek:"[Praėjusį] dddd LT",sameElse:"L"},relativeTime:{future:"po %s",past:"prieš %s",s:b,m:c,mm:f,h:c,hh:f,d:c,dd:f,M:c,MM:f,y:c,yy:f},ordinalParse:/\d{1,2}-oji/,ordinal:function(a){return a+"-oji"},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){function b(a,b,c){var d=a.split("_");return c?b%10===1&&11!==b?d[2]:d[3]:b%10===1&&11!==b?d[0]:d[1]}function c(a,c,e){return a+" "+b(d[e],a,c)}var d={mm:"minūti_minūtes_minūte_minūtes",hh:"stundu_stundas_stunda_stundas",dd:"dienu_dienas_diena_dienas",MM:"mēnesi_mēnešus_mēnesis_mēneši",yy:"gadu_gadus_gads_gadi"};return a.defineLocale("lv",{months:"janvāris_februāris_marts_aprīlis_maijs_jūnijs_jūlijs_augusts_septembris_oktobris_novembris_decembris".split("_"),monthsShort:"jan_feb_mar_apr_mai_jūn_jūl_aug_sep_okt_nov_dec".split("_"),weekdays:"svētdiena_pirmdiena_otrdiena_trešdiena_ceturtdiena_piektdiena_sestdiena".split("_"),weekdaysShort:"Sv_P_O_T_C_Pk_S".split("_"),weekdaysMin:"Sv_P_O_T_C_Pk_S".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD.MM.YYYY",LL:"YYYY. [gada] D. MMMM",LLL:"YYYY. [gada] D. MMMM, LT",LLLL:"YYYY. [gada] D. MMMM, dddd, LT"},calendar:{sameDay:"[Šodien pulksten] LT",nextDay:"[Rīt pulksten] LT",nextWeek:"dddd [pulksten] LT",lastDay:"[Vakar pulksten] LT",lastWeek:"[Pagājušā] dddd [pulksten] LT",sameElse:"L"},relativeTime:{future:"%s vēlāk",past:"%s agrāk",s:"dažas sekundes",m:"minūti",mm:c,h:"stundu",hh:c,d:"dienu",dd:c,M:"mēnesi",MM:c,y:"gadu",yy:c},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("mk",{months:"јануари_февруари_март_април_мај_јуни_јули_август_септември_октомври_ноември_декември".split("_"),monthsShort:"јан_фев_мар_апр_мај_јун_јул_авг_сеп_окт_ное_дек".split("_"),weekdays:"недела_понеделник_вторник_среда_четврток_петок_сабота".split("_"),weekdaysShort:"нед_пон_вто_сре_чет_пет_саб".split("_"),weekdaysMin:"нe_пo_вт_ср_че_пе_сa".split("_"),longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"D.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd, D MMMM YYYY LT"},calendar:{sameDay:"[Денес во] LT",nextDay:"[Утре во] LT",nextWeek:"dddd [во] LT",lastDay:"[Вчера во] LT",lastWeek:function(){switch(this.day()){case 0:case 3:case 6:return"[Во изминатата] dddd [во] LT";case 1:case 2:case 4:case 5:return"[Во изминатиот] dddd [во] LT"}},sameElse:"L"},relativeTime:{future:"после %s",past:"пред %s",s:"неколку секунди",m:"минута",mm:"%d минути",h:"час",hh:"%d часа",d:"ден",dd:"%d дена",M:"месец",MM:"%d месеци",y:"година",yy:"%d години"},ordinalParse:/\d{1,2}-(ев|ен|ти|ви|ри|ми)/,ordinal:function(a){var b=a%10,c=a%100;return 0===a?a+"-ев":0===c?a+"-ен":c>10&&20>c?a+"-ти":1===b?a+"-ви":2===b?a+"-ри":7===b||8===b?a+"-ми":a+"-ти"},week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){return a.defineLocale("ml",{months:"ജനുവരി_ഫെബ്രുവരി_മാർച്ച്_ഏപ്രിൽ_മേയ്_ജൂൺ_ജൂലൈ_ഓഗസ്റ്റ്_സെപ്റ്റംബർ_ഒക്ടോബർ_നവംബർ_ഡിസംബർ".split("_"),monthsShort:"ജനു._ഫെബ്രു._മാർ._ഏപ്രി._മേയ്_ജൂൺ_ജൂലൈ._ഓഗ._സെപ്റ്റ._ഒക്ടോ._നവം._ഡിസം.".split("_"),weekdays:"ഞായറാഴ്ച_തിങ്കളാഴ്ച_ചൊവ്വാഴ്ച_ബുധനാഴ്ച_വ്യാഴാഴ്ച_വെള്ളിയാഴ്ച_ശനിയാഴ്ച".split("_"),weekdaysShort:"ഞായർ_തിങ്കൾ_ചൊവ്വ_ബുധൻ_വ്യാഴം_വെള്ളി_ശനി".split("_"),weekdaysMin:"ഞാ_തി_ചൊ_ബു_വ്യാ_വെ_ശ".split("_"),longDateFormat:{LT:"A h:mm -നു",LTS:"A h:mm:ss -നു",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, LT",LLLL:"dddd, D MMMM YYYY, LT"},calendar:{sameDay:"[ഇന്ന്] LT",nextDay:"[നാളെ] LT",nextWeek:"dddd, LT",lastDay:"[ഇന്നലെ] LT",lastWeek:"[കഴിഞ്ഞ] dddd, LT",sameElse:"L"},relativeTime:{future:"%s കഴിഞ്ഞ്",past:"%s മുൻപ്",s:"അൽപ നിമിഷങ്ങൾ",m:"ഒരു മിനിറ്റ്",mm:"%d മിനിറ്റ്",h:"ഒരു മണിക്കൂർ",hh:"%d മണിക്കൂർ",d:"ഒരു ദിവസം",dd:"%d ദിവസം",M:"ഒരു മാസം",MM:"%d മാസം",y:"ഒരു വർഷം",yy:"%d വർഷം"},meridiemParse:/രാത്രി|രാവിലെ|ഉച്ച കഴിഞ്ഞ്|വൈകുന്നേരം|രാത്രി/i,isPM:function(a){return/^(ഉച്ച കഴിഞ്ഞ്|വൈകുന്നേരം|രാത്രി)$/.test(a)},meridiem:function(a){return 4>a?"രാത്രി":12>a?"രാവിലെ":17>a?"ഉച്ച കഴിഞ്ഞ്":20>a?"വൈകുന്നേരം":"രാത്രി"}})}),function(a){a(vb)}(function(a){var b={1:"१",2:"२",3:"३",4:"४",5:"५",6:"६",7:"७",8:"८",9:"९",0:"०"},c={"१":"1","२":"2","३":"3","४":"4","५":"5","६":"6","७":"7","८":"8","९":"9","०":"0"};return a.defineLocale("mr",{months:"जानेवारी_फेब्रुवारी_मार्च_एप्रिल_मे_जून_जुलै_ऑगस्ट_सप्टेंबर_ऑक्टोबर_नोव्हेंबर_डिसेंबर".split("_"),monthsShort:"जाने._फेब्रु._मार्च._एप्रि._मे._जून._जुलै._ऑग._सप्टें._ऑक्टो._नोव्हें._डिसें.".split("_"),weekdays:"रविवार_सोमवार_मंगळवार_बुधवार_गुरूवार_शुक्रवार_शनिवार".split("_"),weekdaysShort:"रवि_सोम_मंगळ_बुध_गुरू_शुक्र_शनि".split("_"),weekdaysMin:"र_सो_मं_बु_गु_शु_श".split("_"),longDateFormat:{LT:"A h:mm वाजता",LTS:"A h:mm:ss वाजता",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, LT",LLLL:"dddd, D MMMM YYYY, LT"},calendar:{sameDay:"[आज] LT",nextDay:"[उद्या] LT",nextWeek:"dddd, LT",lastDay:"[काल] LT",lastWeek:"[मागील] dddd, LT",sameElse:"L"},relativeTime:{future:"%s नंतर",past:"%s पूर्वी",s:"सेकंद",m:"एक मिनिट",mm:"%d मिनिटे",h:"एक तास",hh:"%d तास",d:"एक दिवस",dd:"%d दिवस",M:"एक महिना",MM:"%d महिने",y:"एक वर्ष",yy:"%d वर्षे"},preparse:function(a){return a.replace(/[१२३४५६७८९०]/g,function(a){return c[a]})},postformat:function(a){return a.replace(/\d/g,function(a){return b[a]})},meridiemParse:/रात्री|सकाळी|दुपारी|सायंकाळी/,meridiemHour:function(a,b){return 12===a&&(a=0),"रात्री"===b?4>a?a:a+12:"सकाळी"===b?a:"दुपारी"===b?a>=10?a:a+12:"सायंकाळी"===b?a+12:void 0},meridiem:function(a){return 4>a?"रात्री":10>a?"सकाळी":17>a?"दुपारी":20>a?"सायंकाळी":"रात्री"},week:{dow:0,doy:6}})}),function(a){a(vb)}(function(a){return a.defineLocale("ms-my",{months:"Januari_Februari_Mac_April_Mei_Jun_Julai_Ogos_September_Oktober_November_Disember".split("_"),monthsShort:"Jan_Feb_Mac_Apr_Mei_Jun_Jul_Ogs_Sep_Okt_Nov_Dis".split("_"),weekdays:"Ahad_Isnin_Selasa_Rabu_Khamis_Jumaat_Sabtu".split("_"),weekdaysShort:"Ahd_Isn_Sel_Rab_Kha_Jum_Sab".split("_"),weekdaysMin:"Ah_Is_Sl_Rb_Km_Jm_Sb".split("_"),longDateFormat:{LT:"HH.mm",LTS:"LT.ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY [pukul] LT",LLLL:"dddd, D MMMM YYYY [pukul] LT"},meridiemParse:/pagi|tengahari|petang|malam/,meridiemHour:function(a,b){return 12===a&&(a=0),"pagi"===b?a:"tengahari"===b?a>=11?a:a+12:"petang"===b||"malam"===b?a+12:void 0},meridiem:function(a){return 11>a?"pagi":15>a?"tengahari":19>a?"petang":"malam"},calendar:{sameDay:"[Hari ini pukul] LT",nextDay:"[Esok pukul] LT",nextWeek:"dddd [pukul] LT",lastDay:"[Kelmarin pukul] LT",lastWeek:"dddd [lepas pukul] LT",sameElse:"L"},relativeTime:{future:"dalam %s",past:"%s yang lepas",s:"beberapa saat",m:"seminit",mm:"%d minit",h:"sejam",hh:"%d jam",d:"sehari",dd:"%d hari",M:"sebulan",MM:"%d bulan",y:"setahun",yy:"%d tahun"},week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){var b={1:"၁",2:"၂",3:"၃",4:"၄",5:"၅",6:"၆",7:"၇",8:"၈",9:"၉",0:"၀"},c={"၁":"1","၂":"2","၃":"3","၄":"4","၅":"5","၆":"6","၇":"7","၈":"8","၉":"9","၀":"0"};return a.defineLocale("my",{months:"ဇန်နဝါရီ_ဖေဖော်ဝါရီ_မတ်_ဧပြီ_မေ_ဇွန်_ဇူလိုင်_သြဂုတ်_စက်တင်ဘာ_အောက်တိုဘာ_နိုဝင်ဘာ_ဒီဇင်ဘာ".split("_"),monthsShort:"ဇန်_ဖေ_မတ်_ပြီ_မေ_ဇွန်_လိုင်_သြ_စက်_အောက်_နို_ဒီ".split("_"),weekdays:"တနင်္ဂနွေ_တနင်္လာ_အင်္ဂါ_ဗုဒ္ဓဟူး_ကြာသပတေး_သောကြာ_စနေ".split("_"),weekdaysShort:"နွေ_လာ_င်္ဂါ_ဟူး_ကြာ_သော_နေ".split("_"),weekdaysMin:"နွေ_လာ_င်္ဂါ_ဟူး_ကြာ_သော_နေ".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D MMMM YYYY LT"},calendar:{sameDay:"[ယနေ.] LT [မှာ]",nextDay:"[မနက်ဖြန်] LT [မှာ]",nextWeek:"dddd LT [မှာ]",lastDay:"[မနေ.က] LT [မှာ]",lastWeek:"[ပြီးခဲ့သော] dddd LT [မှာ]",sameElse:"L"},relativeTime:{future:"လာမည့် %s မှာ",past:"လွန်ခဲ့သော %s က",s:"စက္ကန်.အနည်းငယ်",m:"တစ်မိနစ်",mm:"%d မိနစ်",h:"တစ်နာရီ",hh:"%d နာရီ",d:"တစ်ရက်",dd:"%d ရက်",M:"တစ်လ",MM:"%d လ",y:"တစ်နှစ်",yy:"%d နှစ်"},preparse:function(a){return a.replace(/[၁၂၃၄၅၆၇၈၉၀]/g,function(a){return c[a]})},postformat:function(a){return a.replace(/\d/g,function(a){return b[a]})},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("nb",{months:"januar_februar_mars_april_mai_juni_juli_august_september_oktober_november_desember".split("_"),monthsShort:"jan_feb_mar_apr_mai_jun_jul_aug_sep_okt_nov_des".split("_"),weekdays:"søndag_mandag_tirsdag_onsdag_torsdag_fredag_lørdag".split("_"),weekdaysShort:"søn_man_tirs_ons_tors_fre_lør".split("_"),weekdaysMin:"sø_ma_ti_on_to_fr_lø".split("_"),longDateFormat:{LT:"H.mm",LTS:"LT.ss",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY [kl.] LT",LLLL:"dddd D. MMMM YYYY [kl.] LT"},calendar:{sameDay:"[i dag kl.] LT",nextDay:"[i morgen kl.] LT",nextWeek:"dddd [kl.] LT",lastDay:"[i går kl.] LT",lastWeek:"[forrige] dddd [kl.] LT",sameElse:"L"},relativeTime:{future:"om %s",past:"for %s siden",s:"noen sekunder",m:"ett minutt",mm:"%d minutter",h:"en time",hh:"%d timer",d:"en dag",dd:"%d dager",M:"en måned",MM:"%d måneder",y:"ett år",yy:"%d år"},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){var b={1:"१",2:"२",3:"३",4:"४",5:"५",6:"६",7:"७",8:"८",9:"९",0:"०"},c={"१":"1","२":"2","३":"3","४":"4","५":"5","६":"6","७":"7","८":"8","९":"9","०":"0"};return a.defineLocale("ne",{months:"जनवरी_फेब्रुवरी_मार्च_अप्रिल_मई_जुन_जुलाई_अगष्ट_सेप्टेम्बर_अक्टोबर_नोभेम्बर_डिसेम्बर".split("_"),monthsShort:"जन._फेब्रु._मार्च_अप्रि._मई_जुन_जुलाई._अग._सेप्ट._अक्टो._नोभे._डिसे.".split("_"),weekdays:"आइतबार_सोमबार_मङ्गलबार_बुधबार_बिहिबार_शुक्रबार_शनिबार".split("_"),weekdaysShort:"आइत._सोम._मङ्गल._बुध._बिहि._शुक्र._शनि.".split("_"),weekdaysMin:"आइ._सो._मङ्_बु._बि._शु._श.".split("_"),longDateFormat:{LT:"Aको h:mm बजे",LTS:"Aको h:mm:ss बजे",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, LT",LLLL:"dddd, D MMMM YYYY, LT"},preparse:function(a){return a.replace(/[१२३४५६७८९०]/g,function(a){return c[a]})},postformat:function(a){return a.replace(/\d/g,function(a){return b[a]})},meridiemParse:/राती|बिहान|दिउँसो|बेलुका|साँझ|राती/,meridiemHour:function(a,b){return 12===a&&(a=0),"राती"===b?3>a?a:a+12:"बिहान"===b?a:"दिउँसो"===b?a>=10?a:a+12:"बेलुका"===b||"साँझ"===b?a+12:void 0},meridiem:function(a){return 3>a?"राती":10>a?"बिहान":15>a?"दिउँसो":18>a?"बेलुका":20>a?"साँझ":"राती"},calendar:{sameDay:"[आज] LT",nextDay:"[भोली] LT",nextWeek:"[आउँदो] dddd[,] LT",lastDay:"[हिजो] LT",lastWeek:"[गएको] dddd[,] LT",sameElse:"L"},relativeTime:{future:"%sमा",past:"%s अगाडी",s:"केही समय",m:"एक मिनेट",mm:"%d मिनेट",h:"एक घण्टा",hh:"%d घण्टा",d:"एक दिन",dd:"%d दिन",M:"एक महिना",MM:"%d महिना",y:"एक बर्ष",yy:"%d बर्ष"},week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){var b="jan._feb._mrt._apr._mei_jun._jul._aug._sep._okt._nov._dec.".split("_"),c="jan_feb_mrt_apr_mei_jun_jul_aug_sep_okt_nov_dec".split("_");return a.defineLocale("nl",{months:"januari_februari_maart_april_mei_juni_juli_augustus_september_oktober_november_december".split("_"),monthsShort:function(a,d){return/-MMM-/.test(d)?c[a.month()]:b[a.month()]},weekdays:"zondag_maandag_dinsdag_woensdag_donderdag_vrijdag_zaterdag".split("_"),weekdaysShort:"zo._ma._di._wo._do._vr._za.".split("_"),weekdaysMin:"Zo_Ma_Di_Wo_Do_Vr_Za".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD-MM-YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D MMMM YYYY LT"},calendar:{sameDay:"[vandaag om] LT",nextDay:"[morgen om] LT",nextWeek:"dddd [om] LT",lastDay:"[gisteren om] LT",lastWeek:"[afgelopen] dddd [om] LT",sameElse:"L"},relativeTime:{future:"over %s",past:"%s geleden",s:"een paar seconden",m:"één minuut",mm:"%d minuten",h:"één uur",hh:"%d uur",d:"één dag",dd:"%d dagen",M:"één maand",MM:"%d maanden",y:"één jaar",yy:"%d jaar"},ordinalParse:/\d{1,2}(ste|de)/,ordinal:function(a){return a+(1===a||8===a||a>=20?"ste":"de")},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("nn",{months:"januar_februar_mars_april_mai_juni_juli_august_september_oktober_november_desember".split("_"),monthsShort:"jan_feb_mar_apr_mai_jun_jul_aug_sep_okt_nov_des".split("_"),weekdays:"sundag_måndag_tysdag_onsdag_torsdag_fredag_laurdag".split("_"),weekdaysShort:"sun_mån_tys_ons_tor_fre_lau".split("_"),weekdaysMin:"su_må_ty_on_to_fr_lø".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D MMMM YYYY LT"},calendar:{sameDay:"[I dag klokka] LT",nextDay:"[I morgon klokka] LT",nextWeek:"dddd [klokka] LT",lastDay:"[I går klokka] LT",lastWeek:"[Føregåande] dddd [klokka] LT",sameElse:"L"},relativeTime:{future:"om %s",past:"for %s sidan",s:"nokre sekund",m:"eit minutt",mm:"%d minutt",h:"ein time",hh:"%d timar",d:"ein dag",dd:"%d dagar",M:"ein månad",MM:"%d månader",y:"eit år",yy:"%d år"},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){function b(a){return 5>a%10&&a%10>1&&~~(a/10)%10!==1}function c(a,c,d){var e=a+" ";switch(d){case"m":return c?"minuta":"minutę";case"mm":return e+(b(a)?"minuty":"minut");case"h":return c?"godzina":"godzinę";case"hh":return e+(b(a)?"godziny":"godzin");case"MM":return e+(b(a)?"miesiące":"miesięcy");case"yy":return e+(b(a)?"lata":"lat")}}var d="styczeń_luty_marzec_kwiecień_maj_czerwiec_lipiec_sierpień_wrzesień_październik_listopad_grudzień".split("_"),e="stycznia_lutego_marca_kwietnia_maja_czerwca_lipca_sierpnia_września_października_listopada_grudnia".split("_");return a.defineLocale("pl",{months:function(a,b){return/D MMMM/.test(b)?e[a.month()]:d[a.month()]},monthsShort:"sty_lut_mar_kwi_maj_cze_lip_sie_wrz_paź_lis_gru".split("_"),weekdays:"niedziela_poniedziałek_wtorek_środa_czwartek_piątek_sobota".split("_"),weekdaysShort:"nie_pon_wt_śr_czw_pt_sb".split("_"),weekdaysMin:"N_Pn_Wt_Śr_Cz_Pt_So".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd, D MMMM YYYY LT"},calendar:{sameDay:"[Dziś o] LT",nextDay:"[Jutro o] LT",nextWeek:"[W] dddd [o] LT",lastDay:"[Wczoraj o] LT",lastWeek:function(){switch(this.day()){case 0:return"[W zeszłą niedzielę o] LT";case 3:return"[W zeszłą środę o] LT";case 6:return"[W zeszłą sobotę o] LT";default:return"[W zeszły] dddd [o] LT"}},sameElse:"L"},relativeTime:{future:"za %s",past:"%s temu",s:"kilka sekund",m:c,mm:c,h:c,hh:c,d:"1 dzień",dd:"%d dni",M:"miesiąc",MM:c,y:"rok",yy:c},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("pt-br",{months:"janeiro_fevereiro_março_abril_maio_junho_julho_agosto_setembro_outubro_novembro_dezembro".split("_"),monthsShort:"jan_fev_mar_abr_mai_jun_jul_ago_set_out_nov_dez".split("_"),weekdays:"domingo_segunda-feira_terça-feira_quarta-feira_quinta-feira_sexta-feira_sábado".split("_"),weekdaysShort:"dom_seg_ter_qua_qui_sex_sáb".split("_"),weekdaysMin:"dom_2ª_3ª_4ª_5ª_6ª_sáb".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D [de] MMMM [de] YYYY",LLL:"D [de] MMMM [de] YYYY [às] LT",LLLL:"dddd, D [de] MMMM [de] YYYY [às] LT"},calendar:{sameDay:"[Hoje às] LT",nextDay:"[Amanhã às] LT",nextWeek:"dddd [às] LT",lastDay:"[Ontem às] LT",lastWeek:function(){return 0===this.day()||6===this.day()?"[Último] dddd [às] LT":"[Última] dddd [às] LT"},sameElse:"L"},relativeTime:{future:"em %s",past:"%s atrás",s:"segundos",m:"um minuto",mm:"%d minutos",h:"uma hora",hh:"%d horas",d:"um dia",dd:"%d dias",M:"um mês",MM:"%d meses",y:"um ano",yy:"%d anos"},ordinalParse:/\d{1,2}º/,ordinal:"%dº"})}),function(a){a(vb)}(function(a){return a.defineLocale("pt",{months:"janeiro_fevereiro_março_abril_maio_junho_julho_agosto_setembro_outubro_novembro_dezembro".split("_"),monthsShort:"jan_fev_mar_abr_mai_jun_jul_ago_set_out_nov_dez".split("_"),weekdays:"domingo_segunda-feira_terça-feira_quarta-feira_quinta-feira_sexta-feira_sábado".split("_"),weekdaysShort:"dom_seg_ter_qua_qui_sex_sáb".split("_"),weekdaysMin:"dom_2ª_3ª_4ª_5ª_6ª_sáb".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D [de] MMMM [de] YYYY",LLL:"D [de] MMMM [de] YYYY LT",LLLL:"dddd, D [de] MMMM [de] YYYY LT"},calendar:{sameDay:"[Hoje às] LT",nextDay:"[Amanhã às] LT",nextWeek:"dddd [às] LT",lastDay:"[Ontem às] LT",lastWeek:function(){return 0===this.day()||6===this.day()?"[Último] dddd [às] LT":"[Última] dddd [às] LT"},sameElse:"L"},relativeTime:{future:"em %s",past:"há %s",s:"segundos",m:"um minuto",mm:"%d minutos",h:"uma hora",hh:"%d horas",d:"um dia",dd:"%d dias",M:"um mês",MM:"%d meses",y:"um ano",yy:"%d anos"},ordinalParse:/\d{1,2}º/,ordinal:"%dº",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){function b(a,b,c){var d={mm:"minute",hh:"ore",dd:"zile",MM:"luni",yy:"ani"},e=" ";return(a%100>=20||a>=100&&a%100===0)&&(e=" de "),a+e+d[c]}return a.defineLocale("ro",{months:"ianuarie_februarie_martie_aprilie_mai_iunie_iulie_august_septembrie_octombrie_noiembrie_decembrie".split("_"),monthsShort:"ian._febr._mart._apr._mai_iun._iul._aug._sept._oct._nov._dec.".split("_"),weekdays:"duminică_luni_marți_miercuri_joi_vineri_sâmbătă".split("_"),weekdaysShort:"Dum_Lun_Mar_Mie_Joi_Vin_Sâm".split("_"),weekdaysMin:"Du_Lu_Ma_Mi_Jo_Vi_Sâ".split("_"),longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY H:mm",LLLL:"dddd, D MMMM YYYY H:mm"},calendar:{sameDay:"[azi la] LT",nextDay:"[mâine la] LT",nextWeek:"dddd [la] LT",lastDay:"[ieri la] LT",lastWeek:"[fosta] dddd [la] LT",sameElse:"L"},relativeTime:{future:"peste %s",past:"%s în urmă",s:"câteva secunde",m:"un minut",mm:b,h:"o oră",hh:b,d:"o zi",dd:b,M:"o lună",MM:b,y:"un an",yy:b},week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){function b(a,b){var c=a.split("_");return b%10===1&&b%100!==11?c[0]:b%10>=2&&4>=b%10&&(10>b%100||b%100>=20)?c[1]:c[2]}function c(a,c,d){var e={mm:c?"минута_минуты_минут":"минуту_минуты_минут",hh:"час_часа_часов",dd:"день_дня_дней",MM:"месяц_месяца_месяцев",yy:"год_года_лет"};return"m"===d?c?"минута":"минуту":a+" "+b(e[d],+a)}function d(a,b){var c={nominative:"январь_февраль_март_апрель_май_июнь_июль_август_сентябрь_октябрь_ноябрь_декабрь".split("_"),accusative:"января_февраля_марта_апреля_мая_июня_июля_августа_сентября_октября_ноября_декабря".split("_")},d=/D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/.test(b)?"accusative":"nominative";return c[d][a.month()]}function e(a,b){var c={nominative:"янв_фев_март_апр_май_июнь_июль_авг_сен_окт_ноя_дек".split("_"),accusative:"янв_фев_мар_апр_мая_июня_июля_авг_сен_окт_ноя_дек".split("_")},d=/D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/.test(b)?"accusative":"nominative";return c[d][a.month()]}function f(a,b){var c={nominative:"воскресенье_понедельник_вторник_среда_четверг_пятница_суббота".split("_"),accusative:"воскресенье_понедельник_вторник_среду_четверг_пятницу_субботу".split("_")},d=/\[ ?[Вв] ?(?:прошлую|следующую|эту)? ?\] ?dddd/.test(b)?"accusative":"nominative";return c[d][a.day()]}return a.defineLocale("ru",{months:d,monthsShort:e,weekdays:f,weekdaysShort:"вс_пн_вт_ср_чт_пт_сб".split("_"),weekdaysMin:"вс_пн_вт_ср_чт_пт_сб".split("_"),monthsParse:[/^янв/i,/^фев/i,/^мар/i,/^апр/i,/^ма[й|я]/i,/^июн/i,/^июл/i,/^авг/i,/^сен/i,/^окт/i,/^ноя/i,/^дек/i],longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY г.",LLL:"D MMMM YYYY г., LT",LLLL:"dddd, D MMMM YYYY г., LT"},calendar:{sameDay:"[Сегодня в] LT",nextDay:"[Завтра в] LT",lastDay:"[Вчера в] LT",nextWeek:function(){return 2===this.day()?"[Во] dddd [в] LT":"[В] dddd [в] LT"},lastWeek:function(a){if(a.week()===this.week())return 2===this.day()?"[Во] dddd [в] LT":"[В] dddd [в] LT";switch(this.day()){case 0:return"[В прошлое] dddd [в] LT";case 1:case 2:case 4:return"[В прошлый] dddd [в] LT";case 3:case 5:case 6:return"[В прошлую] dddd [в] LT"}},sameElse:"L"},relativeTime:{future:"через %s",past:"%s назад",s:"несколько секунд",m:c,mm:c,h:"час",hh:c,d:"день",dd:c,M:"месяц",MM:c,y:"год",yy:c},meridiemParse:/ночи|утра|дня|вечера/i,isPM:function(a){return/^(дня|вечера)$/.test(a)},meridiem:function(a){return 4>a?"ночи":12>a?"утра":17>a?"дня":"вечера"},ordinalParse:/\d{1,2}-(й|го|я)/,ordinal:function(a,b){switch(b){case"M":case"d":case"DDD":return a+"-й";case"D":return a+"-го";case"w":case"W":return a+"-я";default:return a}},week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){function b(a){return a>1&&5>a}function c(a,c,d,e){var f=a+" ";switch(d){case"s":return c||e?"pár sekúnd":"pár sekundami";case"m":return c?"minúta":e?"minútu":"minútou";case"mm":return c||e?f+(b(a)?"minúty":"minút"):f+"minútami";break;case"h":return c?"hodina":e?"hodinu":"hodinou";case"hh":return c||e?f+(b(a)?"hodiny":"hodín"):f+"hodinami";break;case"d":return c||e?"deň":"dňom";case"dd":return c||e?f+(b(a)?"dni":"dní"):f+"dňami";break;case"M":return c||e?"mesiac":"mesiacom";case"MM":return c||e?f+(b(a)?"mesiace":"mesiacov"):f+"mesiacmi";break;case"y":return c||e?"rok":"rokom";case"yy":return c||e?f+(b(a)?"roky":"rokov"):f+"rokmi"}}var d="január_február_marec_apríl_máj_jún_júl_august_september_október_november_december".split("_"),e="jan_feb_mar_apr_máj_jún_júl_aug_sep_okt_nov_dec".split("_");return a.defineLocale("sk",{months:d,monthsShort:e,monthsParse:function(a,b){var c,d=[];for(c=0;12>c;c++)d[c]=new RegExp("^"+a[c]+"$|^"+b[c]+"$","i");return d}(d,e),weekdays:"nedeľa_pondelok_utorok_streda_štvrtok_piatok_sobota".split("_"),weekdaysShort:"ne_po_ut_st_št_pi_so".split("_"),weekdaysMin:"ne_po_ut_st_št_pi_so".split("_"),longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY LT",LLLL:"dddd D. MMMM YYYY LT"},calendar:{sameDay:"[dnes o] LT",nextDay:"[zajtra o] LT",nextWeek:function(){switch(this.day()){case 0:return"[v nedeľu o] LT";case 1:case 2:return"[v] dddd [o] LT";case 3:return"[v stredu o] LT";case 4:return"[vo štvrtok o] LT";case 5:return"[v piatok o] LT";case 6:return"[v sobotu o] LT"}},lastDay:"[včera o] LT",lastWeek:function(){switch(this.day()){case 0:return"[minulú nedeľu o] LT";case 1:case 2:return"[minulý] dddd [o] LT";case 3:return"[minulú stredu o] LT";case 4:case 5:return"[minulý] dddd [o] LT";case 6:return"[minulú sobotu o] LT"}},sameElse:"L"},relativeTime:{future:"za %s",past:"pred %s",s:c,m:c,mm:c,h:c,hh:c,d:c,dd:c,M:c,MM:c,y:c,yy:c},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){function b(a,b,c){var d=a+" ";switch(c){case"m":return b?"ena minuta":"eno minuto";case"mm":return d+=1===a?"minuta":2===a?"minuti":3===a||4===a?"minute":"minut";case"h":return b?"ena ura":"eno uro";case"hh":return d+=1===a?"ura":2===a?"uri":3===a||4===a?"ure":"ur";case"dd":return d+=1===a?"dan":"dni";case"MM":return d+=1===a?"mesec":2===a?"meseca":3===a||4===a?"mesece":"mesecev";case"yy":return d+=1===a?"leto":2===a?"leti":3===a||4===a?"leta":"let"}}return a.defineLocale("sl",{months:"januar_februar_marec_april_maj_junij_julij_avgust_september_oktober_november_december".split("_"),monthsShort:"jan._feb._mar._apr._maj._jun._jul._avg._sep._okt._nov._dec.".split("_"),weekdays:"nedelja_ponedeljek_torek_sreda_četrtek_petek_sobota".split("_"),weekdaysShort:"ned._pon._tor._sre._čet._pet._sob.".split("_"),weekdaysMin:"ne_po_to_sr_če_pe_so".split("_"),longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"DD. MM. YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY LT",LLLL:"dddd, D. MMMM YYYY LT"},calendar:{sameDay:"[danes ob] LT",nextDay:"[jutri ob] LT",nextWeek:function(){switch(this.day()){case 0:return"[v] [nedeljo] [ob] LT";case 3:return"[v] [sredo] [ob] LT";case 6:return"[v] [soboto] [ob] LT";case 1:case 2:case 4:case 5:return"[v] dddd [ob] LT"}},lastDay:"[včeraj ob] LT",lastWeek:function(){switch(this.day()){case 0:case 3:case 6:return"[prejšnja] dddd [ob] LT";case 1:case 2:case 4:case 5:return"[prejšnji] dddd [ob] LT"}},sameElse:"L"},relativeTime:{future:"čez %s",past:"%s nazaj",s:"nekaj sekund",m:b,mm:b,h:b,hh:b,d:"en dan",dd:b,M:"en mesec",MM:b,y:"eno leto",yy:b},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){return a.defineLocale("sq",{months:"Janar_Shkurt_Mars_Prill_Maj_Qershor_Korrik_Gusht_Shtator_Tetor_Nëntor_Dhjetor".split("_"),monthsShort:"Jan_Shk_Mar_Pri_Maj_Qer_Kor_Gus_Sht_Tet_Nën_Dhj".split("_"),weekdays:"E Diel_E Hënë_E Martë_E Mërkurë_E Enjte_E Premte_E Shtunë".split("_"),weekdaysShort:"Die_Hën_Mar_Mër_Enj_Pre_Sht".split("_"),weekdaysMin:"D_H_Ma_Më_E_P_Sh".split("_"),meridiemParse:/PD|MD/,isPM:function(a){return"M"===a.charAt(0)},meridiem:function(a){return 12>a?"PD":"MD"},longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd, D MMMM YYYY LT"},calendar:{sameDay:"[Sot në] LT",nextDay:"[Nesër në] LT",nextWeek:"dddd [në] LT",lastDay:"[Dje në] LT",lastWeek:"dddd [e kaluar në] LT",sameElse:"L"},relativeTime:{future:"në %s",past:"%s më parë",s:"disa sekonda",m:"një minutë",mm:"%d minuta",h:"një orë",hh:"%d orë",d:"një ditë",dd:"%d ditë",M:"një muaj",MM:"%d muaj",y:"një vit",yy:"%d vite"},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){var b={words:{m:["један минут","једне минуте"],mm:["минут","минуте","минута"],h:["један сат","једног сата"],hh:["сат","сата","сати"],dd:["дан","дана","дана"],MM:["месец","месеца","месеци"],yy:["година","године","година"]},correctGrammaticalCase:function(a,b){return 1===a?b[0]:a>=2&&4>=a?b[1]:b[2]},translate:function(a,c,d){var e=b.words[d];return 1===d.length?c?e[0]:e[1]:a+" "+b.correctGrammaticalCase(a,e)}};return a.defineLocale("sr-cyrl",{months:["јануар","фебруар","март","април","мај","јун","јул","август","септембар","октобар","новембар","децембар"],monthsShort:["јан.","феб.","мар.","апр.","мај","јун","јул","авг.","сеп.","окт.","нов.","дец."],weekdays:["недеља","понедељак","уторак","среда","четвртак","петак","субота"],weekdaysShort:["нед.","пон.","уто.","сре.","чет.","пет.","суб."],weekdaysMin:["не","по","ут","ср","че","пе","су"],longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"DD. MM. YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY LT",LLLL:"dddd, D. MMMM YYYY LT"},calendar:{sameDay:"[данас у] LT",nextDay:"[сутра у] LT",nextWeek:function(){switch(this.day()){case 0:return"[у] [недељу] [у] LT";case 3:return"[у] [среду] [у] LT";case 6:return"[у] [суботу] [у] LT";case 1:case 2:case 4:case 5:return"[у] dddd [у] LT"}},lastDay:"[јуче у] LT",lastWeek:function(){var a=["[прошле] [недеље] [у] LT","[прошлог] [понедељка] [у] LT","[прошлог] [уторка] [у] LT","[прошле] [среде] [у] LT","[прошлог] [четвртка] [у] LT","[прошлог] [петка] [у] LT","[прошле] [суботе] [у] LT"];return a[this.day()]},sameElse:"L"},relativeTime:{future:"за %s",past:"пре %s",s:"неколико секунди",m:b.translate,mm:b.translate,h:b.translate,hh:b.translate,d:"дан",dd:b.translate,M:"месец",MM:b.translate,y:"годину",yy:b.translate},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){var b={words:{m:["jedan minut","jedne minute"],mm:["minut","minute","minuta"],h:["jedan sat","jednog sata"],hh:["sat","sata","sati"],dd:["dan","dana","dana"],MM:["mesec","meseca","meseci"],yy:["godina","godine","godina"]},correctGrammaticalCase:function(a,b){return 1===a?b[0]:a>=2&&4>=a?b[1]:b[2]},translate:function(a,c,d){var e=b.words[d];return 1===d.length?c?e[0]:e[1]:a+" "+b.correctGrammaticalCase(a,e)}};return a.defineLocale("sr",{months:["januar","februar","mart","april","maj","jun","jul","avgust","septembar","oktobar","novembar","decembar"],monthsShort:["jan.","feb.","mar.","apr.","maj","jun","jul","avg.","sep.","okt.","nov.","dec."],weekdays:["nedelja","ponedeljak","utorak","sreda","četvrtak","petak","subota"],weekdaysShort:["ned.","pon.","uto.","sre.","čet.","pet.","sub."],weekdaysMin:["ne","po","ut","sr","če","pe","su"],longDateFormat:{LT:"H:mm",LTS:"LT:ss",L:"DD. MM. YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY LT",LLLL:"dddd, D. MMMM YYYY LT"},calendar:{sameDay:"[danas u] LT",nextDay:"[sutra u] LT",nextWeek:function(){switch(this.day()){case 0:return"[u] [nedelju] [u] LT";case 3:return"[u] [sredu] [u] LT";case 6:return"[u] [subotu] [u] LT";case 1:case 2:case 4:case 5:return"[u] dddd [u] LT"}},lastDay:"[juče u] LT",lastWeek:function(){var a=["[prošle] [nedelje] [u] LT","[prošlog] [ponedeljka] [u] LT","[prošlog] [utorka] [u] LT","[prošle] [srede] [u] LT","[prošlog] [četvrtka] [u] LT","[prošlog] [petka] [u] LT","[prošle] [subote] [u] LT"];return a[this.day()]},sameElse:"L"},relativeTime:{future:"za %s",past:"pre %s",s:"nekoliko sekundi",m:b.translate,mm:b.translate,h:b.translate,hh:b.translate,d:"dan",dd:b.translate,M:"mesec",MM:b.translate,y:"godinu",yy:b.translate},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){return a.defineLocale("sv",{months:"januari_februari_mars_april_maj_juni_juli_augusti_september_oktober_november_december".split("_"),monthsShort:"jan_feb_mar_apr_maj_jun_jul_aug_sep_okt_nov_dec".split("_"),weekdays:"söndag_måndag_tisdag_onsdag_torsdag_fredag_lördag".split("_"),weekdaysShort:"sön_mån_tis_ons_tor_fre_lör".split("_"),weekdaysMin:"sö_må_ti_on_to_fr_lö".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"YYYY-MM-DD",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D MMMM YYYY LT"},calendar:{sameDay:"[Idag] LT",nextDay:"[Imorgon] LT",lastDay:"[Igår] LT",nextWeek:"dddd LT",lastWeek:"[Förra] dddd[en] LT",sameElse:"L"},relativeTime:{future:"om %s",past:"för %s sedan",s:"några sekunder",m:"en minut",mm:"%d minuter",h:"en timme",hh:"%d timmar",d:"en dag",dd:"%d dagar",M:"en månad",MM:"%d månader",y:"ett år",yy:"%d år"},ordinalParse:/\d{1,2}(e|a)/,ordinal:function(a){var b=a%10,c=1===~~(a%100/10)?"e":1===b?"a":2===b?"a":"e";return a+c},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("ta",{months:"ஜனவரி_பிப்ரவரி_மார்ச்_ஏப்ரல்_மே_ஜூன்_ஜூலை_ஆகஸ்ட்_செப்டெம்பர்_அக்டோபர்_நவம்பர்_டிசம்பர்".split("_"),monthsShort:"ஜனவரி_பிப்ரவரி_மார்ச்_ஏப்ரல்_மே_ஜூன்_ஜூலை_ஆகஸ்ட்_செப்டெம்பர்_அக்டோபர்_நவம்பர்_டிசம்பர்".split("_"),weekdays:"ஞாயிற்றுக்கிழமை_திங்கட்கிழமை_செவ்வாய்கிழமை_புதன்கிழமை_வியாழக்கிழமை_வெள்ளிக்கிழமை_சனிக்கிழமை".split("_"),weekdaysShort:"ஞாயிறு_திங்கள்_செவ்வாய்_புதன்_வியாழன்_வெள்ளி_சனி".split("_"),weekdaysMin:"ஞா_தி_செ_பு_வி_வெ_ச".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, LT",LLLL:"dddd, D MMMM YYYY, LT"},calendar:{sameDay:"[இன்று] LT",nextDay:"[நாளை] LT",nextWeek:"dddd, LT",lastDay:"[நேற்று] LT",lastWeek:"[கடந்த வாரம்] dddd, LT",sameElse:"L"},relativeTime:{future:"%s இல்",past:"%s முன்",s:"ஒரு சில விநாடிகள்",m:"ஒரு நிமிடம்",mm:"%d நிமிடங்கள்",h:"ஒரு மணி நேரம்",hh:"%d மணி நேரம்",d:"ஒரு நாள்",dd:"%d நாட்கள்",M:"ஒரு மாதம்",MM:"%d மாதங்கள்",y:"ஒரு வருடம்",yy:"%d ஆண்டுகள்"},ordinalParse:/\d{1,2}வது/,ordinal:function(a){return a+"வது"},meridiemParse:/யாமம்|வைகறை|காலை|நண்பகல்|எற்பாடு|மாலை/,meridiem:function(a){return 2>a?" யாமம்":6>a?" வைகறை":10>a?" காலை":14>a?" நண்பகல்":18>a?" எற்பாடு":22>a?" மாலை":" யாமம்"},meridiemHour:function(a,b){return 12===a&&(a=0),"யாமம்"===b?2>a?a:a+12:"வைகறை"===b||"காலை"===b?a:"நண்பகல்"===b&&a>=10?a:a+12},week:{dow:0,doy:6}})}),function(a){a(vb)}(function(a){return a.defineLocale("th",{months:"มกราคม_กุมภาพันธ์_มีนาคม_เมษายน_พฤษภาคม_มิถุนายน_กรกฎาคม_สิงหาคม_กันยายน_ตุลาคม_พฤศจิกายน_ธันวาคม".split("_"),monthsShort:"มกรา_กุมภา_มีนา_เมษา_พฤษภา_มิถุนา_กรกฎา_สิงหา_กันยา_ตุลา_พฤศจิกา_ธันวา".split("_"),weekdays:"อาทิตย์_จันทร์_อังคาร_พุธ_พฤหัสบดี_ศุกร์_เสาร์".split("_"),weekdaysShort:"อาทิตย์_จันทร์_อังคาร_พุธ_พฤหัส_ศุกร์_เสาร์".split("_"),weekdaysMin:"อา._จ._อ._พ._พฤ._ศ._ส.".split("_"),longDateFormat:{LT:"H นาฬิกา m นาที",LTS:"LT s วินาที",L:"YYYY/MM/DD",LL:"D MMMM YYYY",LLL:"D MMMM YYYY เวลา LT",LLLL:"วันddddที่ D MMMM YYYY เวลา LT"},meridiemParse:/ก่อนเที่ยง|หลังเที่ยง/,isPM:function(a){return"หลังเที่ยง"===a
},meridiem:function(a){return 12>a?"ก่อนเที่ยง":"หลังเที่ยง"},calendar:{sameDay:"[วันนี้ เวลา] LT",nextDay:"[พรุ่งนี้ เวลา] LT",nextWeek:"dddd[หน้า เวลา] LT",lastDay:"[เมื่อวานนี้ เวลา] LT",lastWeek:"[วัน]dddd[ที่แล้ว เวลา] LT",sameElse:"L"},relativeTime:{future:"อีก %s",past:"%sที่แล้ว",s:"ไม่กี่วินาที",m:"1 นาที",mm:"%d นาที",h:"1 ชั่วโมง",hh:"%d ชั่วโมง",d:"1 วัน",dd:"%d วัน",M:"1 เดือน",MM:"%d เดือน",y:"1 ปี",yy:"%d ปี"}})}),function(a){a(vb)}(function(a){return a.defineLocale("tl-ph",{months:"Enero_Pebrero_Marso_Abril_Mayo_Hunyo_Hulyo_Agosto_Setyembre_Oktubre_Nobyembre_Disyembre".split("_"),monthsShort:"Ene_Peb_Mar_Abr_May_Hun_Hul_Ago_Set_Okt_Nob_Dis".split("_"),weekdays:"Linggo_Lunes_Martes_Miyerkules_Huwebes_Biyernes_Sabado".split("_"),weekdaysShort:"Lin_Lun_Mar_Miy_Huw_Biy_Sab".split("_"),weekdaysMin:"Li_Lu_Ma_Mi_Hu_Bi_Sab".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"MM/D/YYYY",LL:"MMMM D, YYYY",LLL:"MMMM D, YYYY LT",LLLL:"dddd, MMMM DD, YYYY LT"},calendar:{sameDay:"[Ngayon sa] LT",nextDay:"[Bukas sa] LT",nextWeek:"dddd [sa] LT",lastDay:"[Kahapon sa] LT",lastWeek:"dddd [huling linggo] LT",sameElse:"L"},relativeTime:{future:"sa loob ng %s",past:"%s ang nakalipas",s:"ilang segundo",m:"isang minuto",mm:"%d minuto",h:"isang oras",hh:"%d oras",d:"isang araw",dd:"%d araw",M:"isang buwan",MM:"%d buwan",y:"isang taon",yy:"%d taon"},ordinalParse:/\d{1,2}/,ordinal:function(a){return a},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){var b={1:"'inci",5:"'inci",8:"'inci",70:"'inci",80:"'inci",2:"'nci",7:"'nci",20:"'nci",50:"'nci",3:"'üncü",4:"'üncü",100:"'üncü",6:"'ncı",9:"'uncu",10:"'uncu",30:"'uncu",60:"'ıncı",90:"'ıncı"};return a.defineLocale("tr",{months:"Ocak_Şubat_Mart_Nisan_Mayıs_Haziran_Temmuz_Ağustos_Eylül_Ekim_Kasım_Aralık".split("_"),monthsShort:"Oca_Şub_Mar_Nis_May_Haz_Tem_Ağu_Eyl_Eki_Kas_Ara".split("_"),weekdays:"Pazar_Pazartesi_Salı_Çarşamba_Perşembe_Cuma_Cumartesi".split("_"),weekdaysShort:"Paz_Pts_Sal_Çar_Per_Cum_Cts".split("_"),weekdaysMin:"Pz_Pt_Sa_Ça_Pe_Cu_Ct".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd, D MMMM YYYY LT"},calendar:{sameDay:"[bugün saat] LT",nextDay:"[yarın saat] LT",nextWeek:"[haftaya] dddd [saat] LT",lastDay:"[dün] LT",lastWeek:"[geçen hafta] dddd [saat] LT",sameElse:"L"},relativeTime:{future:"%s sonra",past:"%s önce",s:"birkaç saniye",m:"bir dakika",mm:"%d dakika",h:"bir saat",hh:"%d saat",d:"bir gün",dd:"%d gün",M:"bir ay",MM:"%d ay",y:"bir yıl",yy:"%d yıl"},ordinalParse:/\d{1,2}'(inci|nci|üncü|ncı|uncu|ıncı)/,ordinal:function(a){if(0===a)return a+"'ıncı";var c=a%10,d=a%100-c,e=a>=100?100:null;return a+(b[c]||b[d]||b[e])},week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){return a.defineLocale("tzm-latn",{months:"innayr_brˤayrˤ_marˤsˤ_ibrir_mayyw_ywnyw_ywlywz_ɣwšt_šwtanbir_ktˤwbrˤ_nwwanbir_dwjnbir".split("_"),monthsShort:"innayr_brˤayrˤ_marˤsˤ_ibrir_mayyw_ywnyw_ywlywz_ɣwšt_šwtanbir_ktˤwbrˤ_nwwanbir_dwjnbir".split("_"),weekdays:"asamas_aynas_asinas_akras_akwas_asimwas_asiḍyas".split("_"),weekdaysShort:"asamas_aynas_asinas_akras_akwas_asimwas_asiḍyas".split("_"),weekdaysMin:"asamas_aynas_asinas_akras_akwas_asimwas_asiḍyas".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D MMMM YYYY LT"},calendar:{sameDay:"[asdkh g] LT",nextDay:"[aska g] LT",nextWeek:"dddd [g] LT",lastDay:"[assant g] LT",lastWeek:"dddd [g] LT",sameElse:"L"},relativeTime:{future:"dadkh s yan %s",past:"yan %s",s:"imik",m:"minuḍ",mm:"%d minuḍ",h:"saɛa",hh:"%d tassaɛin",d:"ass",dd:"%d ossan",M:"ayowr",MM:"%d iyyirn",y:"asgas",yy:"%d isgasn"},week:{dow:6,doy:12}})}),function(a){a(vb)}(function(a){return a.defineLocale("tzm",{months:"ⵉⵏⵏⴰⵢⵔ_ⴱⵕⴰⵢⵕ_ⵎⴰⵕⵚ_ⵉⴱⵔⵉⵔ_ⵎⴰⵢⵢⵓ_ⵢⵓⵏⵢⵓ_ⵢⵓⵍⵢⵓⵣ_ⵖⵓⵛⵜ_ⵛⵓⵜⴰⵏⴱⵉⵔ_ⴽⵟⵓⴱⵕ_ⵏⵓⵡⴰⵏⴱⵉⵔ_ⴷⵓⵊⵏⴱⵉⵔ".split("_"),monthsShort:"ⵉⵏⵏⴰⵢⵔ_ⴱⵕⴰⵢⵕ_ⵎⴰⵕⵚ_ⵉⴱⵔⵉⵔ_ⵎⴰⵢⵢⵓ_ⵢⵓⵏⵢⵓ_ⵢⵓⵍⵢⵓⵣ_ⵖⵓⵛⵜ_ⵛⵓⵜⴰⵏⴱⵉⵔ_ⴽⵟⵓⴱⵕ_ⵏⵓⵡⴰⵏⴱⵉⵔ_ⴷⵓⵊⵏⴱⵉⵔ".split("_"),weekdays:"ⴰⵙⴰⵎⴰⵙ_ⴰⵢⵏⴰⵙ_ⴰⵙⵉⵏⴰⵙ_ⴰⴽⵔⴰⵙ_ⴰⴽⵡⴰⵙ_ⴰⵙⵉⵎⵡⴰⵙ_ⴰⵙⵉⴹⵢⴰⵙ".split("_"),weekdaysShort:"ⴰⵙⴰⵎⴰⵙ_ⴰⵢⵏⴰⵙ_ⴰⵙⵉⵏⴰⵙ_ⴰⴽⵔⴰⵙ_ⴰⴽⵡⴰⵙ_ⴰⵙⵉⵎⵡⴰⵙ_ⴰⵙⵉⴹⵢⴰⵙ".split("_"),weekdaysMin:"ⴰⵙⴰⵎⴰⵙ_ⴰⵢⵏⴰⵙ_ⴰⵙⵉⵏⴰⵙ_ⴰⴽⵔⴰⵙ_ⴰⴽⵡⴰⵙ_ⴰⵙⵉⵎⵡⴰⵙ_ⴰⵙⵉⴹⵢⴰⵙ".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"dddd D MMMM YYYY LT"},calendar:{sameDay:"[ⴰⵙⴷⵅ ⴴ] LT",nextDay:"[ⴰⵙⴽⴰ ⴴ] LT",nextWeek:"dddd [ⴴ] LT",lastDay:"[ⴰⵚⴰⵏⵜ ⴴ] LT",lastWeek:"dddd [ⴴ] LT",sameElse:"L"},relativeTime:{future:"ⴷⴰⴷⵅ ⵙ ⵢⴰⵏ %s",past:"ⵢⴰⵏ %s",s:"ⵉⵎⵉⴽ",m:"ⵎⵉⵏⵓⴺ",mm:"%d ⵎⵉⵏⵓⴺ",h:"ⵙⴰⵄⴰ",hh:"%d ⵜⴰⵙⵙⴰⵄⵉⵏ",d:"ⴰⵙⵙ",dd:"%d oⵙⵙⴰⵏ",M:"ⴰⵢoⵓⵔ",MM:"%d ⵉⵢⵢⵉⵔⵏ",y:"ⴰⵙⴳⴰⵙ",yy:"%d ⵉⵙⴳⴰⵙⵏ"},week:{dow:6,doy:12}})}),function(a){a(vb)}(function(a){function b(a,b){var c=a.split("_");return b%10===1&&b%100!==11?c[0]:b%10>=2&&4>=b%10&&(10>b%100||b%100>=20)?c[1]:c[2]}function c(a,c,d){var e={mm:"хвилина_хвилини_хвилин",hh:"година_години_годин",dd:"день_дні_днів",MM:"місяць_місяці_місяців",yy:"рік_роки_років"};return"m"===d?c?"хвилина":"хвилину":"h"===d?c?"година":"годину":a+" "+b(e[d],+a)}function d(a,b){var c={nominative:"січень_лютий_березень_квітень_травень_червень_липень_серпень_вересень_жовтень_листопад_грудень".split("_"),accusative:"січня_лютого_березня_квітня_травня_червня_липня_серпня_вересня_жовтня_листопада_грудня".split("_")},d=/D[oD]? *MMMM?/.test(b)?"accusative":"nominative";return c[d][a.month()]}function e(a,b){var c={nominative:"неділя_понеділок_вівторок_середа_четвер_п’ятниця_субота".split("_"),accusative:"неділю_понеділок_вівторок_середу_четвер_п’ятницю_суботу".split("_"),genitive:"неділі_понеділка_вівторка_середи_четверга_п’ятниці_суботи".split("_")},d=/(\[[ВвУу]\]) ?dddd/.test(b)?"accusative":/\[?(?:минулої|наступної)? ?\] ?dddd/.test(b)?"genitive":"nominative";return c[d][a.day()]}function f(a){return function(){return a+"о"+(11===this.hours()?"б":"")+"] LT"}}return a.defineLocale("uk",{months:d,monthsShort:"січ_лют_бер_квіт_трав_черв_лип_серп_вер_жовт_лист_груд".split("_"),weekdays:e,weekdaysShort:"нд_пн_вт_ср_чт_пт_сб".split("_"),weekdaysMin:"нд_пн_вт_ср_чт_пт_сб".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY р.",LLL:"D MMMM YYYY р., LT",LLLL:"dddd, D MMMM YYYY р., LT"},calendar:{sameDay:f("[Сьогодні "),nextDay:f("[Завтра "),lastDay:f("[Вчора "),nextWeek:f("[У] dddd ["),lastWeek:function(){switch(this.day()){case 0:case 3:case 5:case 6:return f("[Минулої] dddd [").call(this);case 1:case 2:case 4:return f("[Минулого] dddd [").call(this)}},sameElse:"L"},relativeTime:{future:"за %s",past:"%s тому",s:"декілька секунд",m:c,mm:c,h:"годину",hh:c,d:"день",dd:c,M:"місяць",MM:c,y:"рік",yy:c},meridiemParse:/ночі|ранку|дня|вечора/,isPM:function(a){return/^(дня|вечора)$/.test(a)},meridiem:function(a){return 4>a?"ночі":12>a?"ранку":17>a?"дня":"вечора"},ordinalParse:/\d{1,2}-(й|го)/,ordinal:function(a,b){switch(b){case"M":case"d":case"DDD":case"w":case"W":return a+"-й";case"D":return a+"-го";default:return a}},week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){return a.defineLocale("uz",{months:"январь_февраль_март_апрель_май_июнь_июль_август_сентябрь_октябрь_ноябрь_декабрь".split("_"),monthsShort:"янв_фев_мар_апр_май_июн_июл_авг_сен_окт_ноя_дек".split("_"),weekdays:"Якшанба_Душанба_Сешанба_Чоршанба_Пайшанба_Жума_Шанба".split("_"),weekdaysShort:"Якш_Душ_Сеш_Чор_Пай_Жум_Шан".split("_"),weekdaysMin:"Як_Ду_Се_Чо_Па_Жу_Ша".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY LT",LLLL:"D MMMM YYYY, dddd LT"},calendar:{sameDay:"[Бугун соат] LT [да]",nextDay:"[Эртага] LT [да]",nextWeek:"dddd [куни соат] LT [да]",lastDay:"[Кеча соат] LT [да]",lastWeek:"[Утган] dddd [куни соат] LT [да]",sameElse:"L"},relativeTime:{future:"Якин %s ичида",past:"Бир неча %s олдин",s:"фурсат",m:"бир дакика",mm:"%d дакика",h:"бир соат",hh:"%d соат",d:"бир кун",dd:"%d кун",M:"бир ой",MM:"%d ой",y:"бир йил",yy:"%d йил"},week:{dow:1,doy:7}})}),function(a){a(vb)}(function(a){return a.defineLocale("vi",{months:"tháng 1_tháng 2_tháng 3_tháng 4_tháng 5_tháng 6_tháng 7_tháng 8_tháng 9_tháng 10_tháng 11_tháng 12".split("_"),monthsShort:"Th01_Th02_Th03_Th04_Th05_Th06_Th07_Th08_Th09_Th10_Th11_Th12".split("_"),weekdays:"chủ nhật_thứ hai_thứ ba_thứ tư_thứ năm_thứ sáu_thứ bảy".split("_"),weekdaysShort:"CN_T2_T3_T4_T5_T6_T7".split("_"),weekdaysMin:"CN_T2_T3_T4_T5_T6_T7".split("_"),longDateFormat:{LT:"HH:mm",LTS:"LT:ss",L:"DD/MM/YYYY",LL:"D MMMM [năm] YYYY",LLL:"D MMMM [năm] YYYY LT",LLLL:"dddd, D MMMM [năm] YYYY LT",l:"DD/M/YYYY",ll:"D MMM YYYY",lll:"D MMM YYYY LT",llll:"ddd, D MMM YYYY LT"},calendar:{sameDay:"[Hôm nay lúc] LT",nextDay:"[Ngày mai lúc] LT",nextWeek:"dddd [tuần tới lúc] LT",lastDay:"[Hôm qua lúc] LT",lastWeek:"dddd [tuần rồi lúc] LT",sameElse:"L"},relativeTime:{future:"%s tới",past:"%s trước",s:"vài giây",m:"một phút",mm:"%d phút",h:"một giờ",hh:"%d giờ",d:"một ngày",dd:"%d ngày",M:"một tháng",MM:"%d tháng",y:"một năm",yy:"%d năm"},ordinalParse:/\d{1,2}/,ordinal:function(a){return a},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("zh-cn",{months:"一月_二月_三月_四月_五月_六月_七月_八月_九月_十月_十一月_十二月".split("_"),monthsShort:"1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),weekdays:"星期日_星期一_星期二_星期三_星期四_星期五_星期六".split("_"),weekdaysShort:"周日_周一_周二_周三_周四_周五_周六".split("_"),weekdaysMin:"日_一_二_三_四_五_六".split("_"),longDateFormat:{LT:"Ah点mm",LTS:"Ah点m分s秒",L:"YYYY-MM-DD",LL:"YYYY年MMMD日",LLL:"YYYY年MMMD日LT",LLLL:"YYYY年MMMD日ddddLT",l:"YYYY-MM-DD",ll:"YYYY年MMMD日",lll:"YYYY年MMMD日LT",llll:"YYYY年MMMD日ddddLT"},meridiemParse:/凌晨|早上|上午|中午|下午|晚上/,meridiemHour:function(a,b){return 12===a&&(a=0),"凌晨"===b||"早上"===b||"上午"===b?a:"下午"===b||"晚上"===b?a+12:a>=11?a:a+12},meridiem:function(a,b){var c=100*a+b;return 600>c?"凌晨":900>c?"早上":1130>c?"上午":1230>c?"中午":1800>c?"下午":"晚上"},calendar:{sameDay:function(){return 0===this.minutes()?"[今天]Ah[点整]":"[今天]LT"},nextDay:function(){return 0===this.minutes()?"[明天]Ah[点整]":"[明天]LT"},lastDay:function(){return 0===this.minutes()?"[昨天]Ah[点整]":"[昨天]LT"},nextWeek:function(){var b,c;return b=a().startOf("week"),c=this.unix()-b.unix()>=604800?"[下]":"[本]",0===this.minutes()?c+"dddAh点整":c+"dddAh点mm"},lastWeek:function(){var b,c;return b=a().startOf("week"),c=this.unix()<b.unix()?"[上]":"[本]",0===this.minutes()?c+"dddAh点整":c+"dddAh点mm"},sameElse:"LL"},ordinalParse:/\d{1,2}(日|月|周)/,ordinal:function(a,b){switch(b){case"d":case"D":case"DDD":return a+"日";case"M":return a+"月";case"w":case"W":return a+"周";default:return a}},relativeTime:{future:"%s内",past:"%s前",s:"几秒",m:"1分钟",mm:"%d分钟",h:"1小时",hh:"%d小时",d:"1天",dd:"%d天",M:"1个月",MM:"%d个月",y:"1年",yy:"%d年"},week:{dow:1,doy:4}})}),function(a){a(vb)}(function(a){return a.defineLocale("zh-tw",{months:"一月_二月_三月_四月_五月_六月_七月_八月_九月_十月_十一月_十二月".split("_"),monthsShort:"1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),weekdays:"星期日_星期一_星期二_星期三_星期四_星期五_星期六".split("_"),weekdaysShort:"週日_週一_週二_週三_週四_週五_週六".split("_"),weekdaysMin:"日_一_二_三_四_五_六".split("_"),longDateFormat:{LT:"Ah點mm",LTS:"Ah點m分s秒",L:"YYYY年MMMD日",LL:"YYYY年MMMD日",LLL:"YYYY年MMMD日LT",LLLL:"YYYY年MMMD日ddddLT",l:"YYYY年MMMD日",ll:"YYYY年MMMD日",lll:"YYYY年MMMD日LT",llll:"YYYY年MMMD日ddddLT"},meridiemParse:/早上|上午|中午|下午|晚上/,meridiemHour:function(a,b){return 12===a&&(a=0),"早上"===b||"上午"===b?a:"中午"===b?a>=11?a:a+12:"下午"===b||"晚上"===b?a+12:void 0},meridiem:function(a,b){var c=100*a+b;return 900>c?"早上":1130>c?"上午":1230>c?"中午":1800>c?"下午":"晚上"},calendar:{sameDay:"[今天]LT",nextDay:"[明天]LT",nextWeek:"[下]ddddLT",lastDay:"[昨天]LT",lastWeek:"[上]ddddLT",sameElse:"L"},ordinalParse:/\d{1,2}(日|月|週)/,ordinal:function(a,b){switch(b){case"d":case"D":case"DDD":return a+"日";case"M":return a+"月";case"w":case"W":return a+"週";default:return a}},relativeTime:{future:"%s內",past:"%s前",s:"幾秒",m:"一分鐘",mm:"%d分鐘",h:"一小時",hh:"%d小時",d:"一天",dd:"%d天",M:"一個月",MM:"%d個月",y:"一年",yy:"%d年"}})}),vb.locale("en"),Lb?module.exports=vb:"function"==typeof define&&define.amd?(define(function(a,b,c){return c.config&&c.config()&&c.config().noGlobal===!0&&(zb.moment=wb),vb}),ub(!0)):ub()}).call(this);
//! moment-timezone.js
//! version : 0.4.0
//! author : Tim Wood
//! license : MIT
//! github.com/moment/moment-timezone
!function(a,b){"use strict";"function"==typeof define&&define.amd?define(["moment"],b):"object"==typeof exports?module.exports=b(require("moment")):b(a.moment)}(this,function(a){"use strict";function b(a){return a>96?a-87:a>64?a-29:a-48}function c(a){var c,d=0,e=a.split("."),f=e[0],g=e[1]||"",h=1,i=0,j=1;for(45===a.charCodeAt(0)&&(d=1,j=-1),d;d<f.length;d++)c=b(f.charCodeAt(d)),i=60*i+c;for(d=0;d<g.length;d++)h/=60,c=b(g.charCodeAt(d)),i+=c*h;return i*j}function d(a){for(var b=0;b<a.length;b++)a[b]=c(a[b])}function e(a,b){for(var c=0;b>c;c++)a[c]=Math.round((a[c-1]||0)+6e4*a[c]);a[b-1]=1/0}function f(a,b){var c,d=[];for(c=0;c<b.length;c++)d[c]=a[b[c]];return d}function g(a){var b=a.split("|"),c=b[2].split(" "),g=b[3].split(""),h=b[4].split(" ");return d(c),d(g),d(h),e(h,g.length),{name:b[0],abbrs:f(b[1].split(" "),g),offsets:f(c,g),untils:h}}function h(a){a&&this._set(g(a))}function i(a){return(a||"").toLowerCase().replace(/\//g,"_")}function j(a){var b,c,d;for("string"==typeof a&&(a=[a]),b=0;b<a.length;b++)c=a[b].split("|")[0],d=i(c),v[d]=a[b],x[d]=c}function k(a,b){a=i(a);var c,d=v[a];return d instanceof h?d:"string"==typeof d?(d=new h(d),v[a]=d,d):w[a]&&b!==k&&(c=k(w[a],k))?(d=v[a]=new h,d._set(c),d.name=x[a],d):null}function l(){var a,b=[];for(a in x)x.hasOwnProperty(a)&&(v[a]||v[w[a]])&&x[a]&&b.push(x[a]);return b.sort()}function m(a){var b,c,d,e;for("string"==typeof a&&(a=[a]),b=0;b<a.length;b++)c=a[b].split("|"),d=i(c[0]),e=i(c[1]),w[d]=e,x[d]=c[0],w[e]=d,x[e]=c[1]}function n(a){j(a.zones),m(a.links),r.dataVersion=a.version}function o(a){return o.didShowError||(o.didShowError=!0,q("moment.tz.zoneExists('"+a+"') has been deprecated in favor of !moment.tz.zone('"+a+"')")),!!k(a)}function p(a){return!(!a._a||void 0!==a._tzm)}function q(a){"undefined"!=typeof console&&"function"==typeof console.error&&console.error(a)}function r(b){var c=Array.prototype.slice.call(arguments,0,-1),d=arguments[arguments.length-1],e=k(d),f=a.utc.apply(null,c);return e&&!a.isMoment(b)&&p(f)&&f.add(e.parse(f),"minutes"),f.tz(d),f}function s(a){return function(){return this._z?this._z.abbr(this):a.call(this)}}function t(a){return function(){return this._z=null,a.apply(this,arguments)}}if(void 0!==a.tz)return q("Moment Timezone "+a.tz.version+" was already loaded "+(a.tz.dataVersion?"with data from ":"without any data")+a.tz.dataVersion),a;var u="0.4.0",v={},w={},x={},y=a.version.split("."),z=+y[0],A=+y[1];(2>z||2===z&&6>A)&&q("Moment Timezone requires Moment.js >= 2.6.0. You are using Moment.js "+a.version+". See momentjs.com"),h.prototype={_set:function(a){this.name=a.name,this.abbrs=a.abbrs,this.untils=a.untils,this.offsets=a.offsets},_index:function(a){var b,c=+a,d=this.untils;for(b=0;b<d.length;b++)if(c<d[b])return b},parse:function(a){var b,c,d,e,f=+a,g=this.offsets,h=this.untils,i=h.length-1;for(e=0;i>e;e++)if(b=g[e],c=g[e+1],d=g[e?e-1:e],c>b&&r.moveAmbiguousForward?b=c:b>d&&r.moveInvalidForward&&(b=d),f<h[e]-6e4*b)return g[e];return g[i]},abbr:function(a){return this.abbrs[this._index(a)]},offset:function(a){return this.offsets[this._index(a)]}},r.version=u,r.dataVersion="",r._zones=v,r._links=w,r._names=x,r.add=j,r.link=m,r.load=n,r.zone=k,r.zoneExists=o,r.names=l,r.Zone=h,r.unpack=g,r.unpackBase60=c,r.needsOffset=p,r.moveInvalidForward=!0,r.moveAmbiguousForward=!1;var B=a.fn;a.tz=r,a.defaultZone=null,a.updateOffset=function(b,c){var d,e=a.defaultZone;void 0===b._z&&(e&&p(b)&&!b._isUTC&&(b._d=a.utc(b._a)._d,b.utc().add(e.parse(b),"minutes")),b._z=e),b._z&&(d=b._z.offset(b),Math.abs(d)<16&&(d/=60),void 0!==b.utcOffset?b.utcOffset(-d,c):b.zone(d,c))},B.tz=function(b){return b?(this._z=k(b),this._z?a.updateOffset(this):q("Moment Timezone has no data for "+b+". See http://momentjs.com/timezone/docs/#/data-loading/."),this):this._z?this._z.name:void 0},B.zoneName=s(B.zoneName),B.zoneAbbr=s(B.zoneAbbr),B.utc=t(B.utc),a.tz.setDefault=function(b){return(2>z||2===z&&9>A)&&q("Moment Timezone setDefault() requires Moment.js >= 2.9.0. You are using Moment.js "+a.version+"."),a.defaultZone=b?k(b):null,a};var C=a.momentProperties;return"[object Array]"===Object.prototype.toString.call(C)?(C.push("_z"),C.push("_a")):C&&(C._z=null),n({version:"2015d",zones:["Africa/Abidjan|LMT GMT|g.8 0|01|-2ldXH.Q","Africa/Accra|LMT GMT GHST|.Q 0 -k|012121212121212121212121212121212121212121212121|-26BbX.8 6tzX.8 MnE 1BAk MnE 1BAk MnE 1BAk MnE 1C0k MnE 1BAk MnE 1BAk MnE 1BAk MnE 1C0k MnE 1BAk MnE 1BAk MnE 1BAk MnE 1C0k MnE 1BAk MnE 1BAk MnE 1BAk MnE 1C0k MnE 1BAk MnE 1BAk MnE 1BAk MnE 1C0k MnE 1BAk MnE 1BAk MnE","Africa/Addis_Ababa|LMT EAT BEAT BEAUT|-2r.g -30 -2u -2J|01231|-1F3Cr.g 3Dzr.g okMu MFXJ","Africa/Algiers|PMT WET WEST CET CEST|-9.l 0 -10 -10 -20|0121212121212121343431312123431213|-2nco9.l cNb9.l HA0 19A0 1iM0 11c0 1oo0 Wo0 1rc0 QM0 1EM0 UM0 DA0 Imo0 rd0 De0 9Xz0 1fb0 1ap0 16K0 2yo0 mEp0 hwL0 jxA0 11A0 dDd0 17b0 11B0 1cN0 2Dy0 1cN0 1fB0 1cL0","Africa/Bangui|LMT WAT|-d.A -10|01|-22y0d.A","Africa/Bissau|LMT WAT GMT|12.k 10 0|012|-2ldWV.E 2xonV.E","Africa/Blantyre|LMT CAT|-2a.k -20|01|-2GJea.k","Africa/Cairo|EET EEST|-20 -30|0101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-1bIO0 vb0 1ip0 11z0 1iN0 1nz0 12p0 1pz0 10N0 1pz0 16p0 1jz0 s3d0 Vz0 1oN0 11b0 1oO0 10N0 1pz0 10N0 1pb0 10N0 1pb0 10N0 1pb0 10N0 1pz0 10N0 1pb0 10N0 1pb0 11d0 1oL0 11d0 1pb0 11d0 1oL0 11d0 1oL0 11d0 1oL0 11d0 1pb0 11d0 1oL0 11d0 1oL0 11d0 1oL0 11d0 1pb0 11d0 1oL0 11d0 1oL0 11d0 1oL0 11d0 1pb0 11d0 1oL0 11d0 1WL0 rd0 1Rz0 wp0 1pb0 11d0 1oL0 11d0 1oL0 11d0 1oL0 11d0 1pb0 11d0 1qL0 Xd0 1oL0 11d0 1oL0 11d0 1pb0 11d0 1oL0 11d0 1oL0 11d0 1ny0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 1o10 WL0 1qN0 Rb0 1wp0 On0 1zd0 Lz0 1EN0 Fb0 c10 8n0 8Nd0 gL0 e10 mn0","Africa/Casablanca|LMT WET WEST CET|u.k 0 -10 -10|012121212121212121312121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2gMnt.E 130Lt.E rb0 Dd0 dVb0 b6p0 TX0 EoB0 LL0 gnd0 rz0 43d0 AL0 1Nd0 XX0 1Cp0 pz0 dEp0 4mn0 SyN0 AL0 1Nd0 wn0 1FB0 Db0 1zd0 Lz0 1Nf0 wM0 co0 go0 1o00 s00 dA0 vc0 11A0 A00 e00 y00 11A0 uo0 e00 DA0 11A0 rA0 e00 Jc0 WM0 m00 gM0 M00 WM0 jc0 e00 RA0 11A0 dA0 e00 Uo0 11A0 800 gM0 Xc0 11A0 5c0 e00 17A0 WM0 2o0 e00 1ao0 19A0 1g00 16M0 1iM0 1400 1lA0 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qo0 1200 1kM0 14M0 1i00","Africa/Ceuta|WET WEST CET CEST|0 -10 -10 -20|010101010101010101010232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232|-25KN0 11z0 drd0 18o0 3I00 17c0 1fA0 1a00 1io0 1a00 1y7p0 LL0 gnd0 rz0 43d0 AL0 1Nd0 XX0 1Cp0 pz0 dEp0 4VB0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Africa/El_Aaiun|LMT WAT WET WEST|Q.M 10 0 -10|0123232323232323232323232323232323232323232323232323232323232323232323232323232323232323232|-1rDz7.c 1GVA7.c 6L0 AL0 1Nd0 XX0 1Cp0 pz0 1cBB0 AL0 1Nd0 wn0 1FB0 Db0 1zd0 Lz0 1Nf0 wM0 co0 go0 1o00 s00 dA0 vc0 11A0 A00 e00 y00 11A0 uo0 e00 DA0 11A0 rA0 e00 Jc0 WM0 m00 gM0 M00 WM0 jc0 e00 RA0 11A0 dA0 e00 Uo0 11A0 800 gM0 Xc0 11A0 5c0 e00 17A0 WM0 2o0 e00 1ao0 19A0 1g00 16M0 1iM0 1400 1lA0 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qo0 1200 1kM0 14M0 1i00","Africa/Johannesburg|SAST SAST SAST|-1u -20 -30|012121|-2GJdu 1Ajdu 1cL0 1cN0 1cL0","Africa/Juba|LMT CAT CAST EAT|-2a.8 -20 -30 -30|01212121212121212121212121212121213|-1yW2a.8 1zK0a.8 16L0 1iN0 17b0 1jd0 17b0 1ip0 17z0 1i10 17X0 1hB0 18n0 1hd0 19b0 1gp0 19z0 1iN0 17b0 1ip0 17z0 1i10 18n0 1hd0 18L0 1gN0 19b0 1gp0 19z0 1iN0 17z0 1i10 17X0 yGd0","Africa/Monrovia|MMT LRT GMT|H.8 I.u 0|012|-23Lzg.Q 29s01.m","Africa/Ndjamena|LMT WAT WAST|-10.c -10 -20|0121|-2le10.c 2J3c0.c Wn0","Africa/Tripoli|LMT CET CEST EET|-Q.I -10 -20 -20|012121213121212121212121213123123|-21JcQ.I 1hnBQ.I vx0 4iP0 xx0 4eN0 Bb0 7ip0 U0n0 A10 1db0 1cN0 1db0 1dd0 1db0 1eN0 1bb0 1e10 1cL0 1c10 1db0 1dd0 1db0 1cN0 1db0 1q10 fAn0 1ep0 1db0 AKq0 TA0 1o00","Africa/Tunis|PMT CET CEST|-9.l -10 -20|0121212121212121212121212121212121|-2nco9.l 18pa9.l 1qM0 DA0 3Tc0 11B0 1ze0 WM0 7z0 3d0 14L0 1cN0 1f90 1ar0 16J0 1gXB0 WM0 1rA0 11c0 nwo0 Ko0 1cM0 1cM0 1rA0 10M0 zuM0 10N0 1aN0 1qM0 WM0 1qM0 11A0 1o00","Africa/Windhoek|SWAT SAST SAST CAT WAT WAST|-1u -20 -30 -20 -10 -20|012134545454545454545454545454545454545454545454545454545454545454545454545454545454545454545|-2GJdu 1Ajdu 1cL0 1SqL0 9NA0 11D0 1nX0 11B0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1qL0 11B0 1nX0 11B0","America/Adak|NST NWT NPT BST BDT AHST HST HDT|b0 a0 a0 b0 a0 a0 a0 90|012034343434343434343434343434343456767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676|-17SX0 8wW0 iB0 Qlb0 52O0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 cm0 10q0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Anchorage|CAT CAWT CAPT AHST AHDT YST AKST AKDT|a0 90 90 a0 90 90 90 80|012034343434343434343434343434343456767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676|-17T00 8wX0 iA0 Qlb0 52O0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 cm0 10q0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Anguilla|LMT AST|46.4 40|01|-2kNvR.U","America/Araguaina|LMT BRT BRST|3c.M 30 20|0121212121212121212121212121212121212121212121212121|-2glwL.c HdKL.c 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 qe10 xb0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0 1EN0 FX0 1HB0 Lz0 dMN0 Lz0 1zd0 Rb0 1wN0 Wn0 1tB0 Rb0 1tB0 WL0 1tB0 Rb0 1zd0 On0 1HB0 FX0 ny10 Lz0","America/Argentina/Buenos_Aires|CMT ART ARST ART ARST|4g.M 40 30 30 20|0121212121212121212121212121212121212121213434343434343234343|-20UHH.c pKnH.c Mn0 1iN0 Tb0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 MN0 2jz0 MN0 4lX0 u10 5Lb0 1pB0 Fnz0 u10 uL0 1vd0 SL0 1vd0 SL0 1vd0 17z0 1cN0 1fz0 1cN0 1cL0 1cN0 asn0 Db0 zvd0 Bz0 1tB0 TX0 1wp0 Rb0 1wp0 Rb0 1wp0 TX0 g0p0 10M0 j3c0 uL0 1qN0 WL0","America/Argentina/Catamarca|CMT ART ARST ART ARST WART|4g.M 40 30 30 20 40|0121212121212121212121212121212121212121213434343454343235343|-20UHH.c pKnH.c Mn0 1iN0 Tb0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 MN0 2jz0 MN0 4lX0 u10 5Lb0 1pB0 Fnz0 u10 uL0 1vd0 SL0 1vd0 SL0 1vd0 17z0 1cN0 1fz0 1cN0 1cL0 1cN0 asn0 Db0 zvd0 Bz0 1tB0 TX0 1wp0 Rb0 1wq0 Ra0 1wp0 TX0 g0p0 10M0 ako0 7B0 8zb0 uL0","America/Argentina/Cordoba|CMT ART ARST ART ARST WART|4g.M 40 30 30 20 40|0121212121212121212121212121212121212121213434343454343234343|-20UHH.c pKnH.c Mn0 1iN0 Tb0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 MN0 2jz0 MN0 4lX0 u10 5Lb0 1pB0 Fnz0 u10 uL0 1vd0 SL0 1vd0 SL0 1vd0 17z0 1cN0 1fz0 1cN0 1cL0 1cN0 asn0 Db0 zvd0 Bz0 1tB0 TX0 1wp0 Rb0 1wq0 Ra0 1wp0 TX0 g0p0 10M0 j3c0 uL0 1qN0 WL0","America/Argentina/Jujuy|CMT ART ARST ART ARST WART WARST|4g.M 40 30 30 20 40 30|01212121212121212121212121212121212121212134343456543432343|-20UHH.c pKnH.c Mn0 1iN0 Tb0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 MN0 2jz0 MN0 4lX0 u10 5Lb0 1pB0 Fnz0 u10 uL0 1vd0 SL0 1vd0 SL0 1vd0 17z0 1cN0 1fz0 1cN0 1cL0 1cN0 asn0 Db0 zvd0 Bz0 1tB0 TX0 1ze0 TX0 1ld0 WK0 1wp0 TX0 g0p0 10M0 j3c0 uL0","America/Argentina/La_Rioja|CMT ART ARST ART ARST WART|4g.M 40 30 30 20 40|01212121212121212121212121212121212121212134343434534343235343|-20UHH.c pKnH.c Mn0 1iN0 Tb0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 MN0 2jz0 MN0 4lX0 u10 5Lb0 1pB0 Fnz0 u10 uL0 1vd0 SL0 1vd0 SL0 1vd0 17z0 1cN0 1fz0 1cN0 1cL0 1cN0 asn0 Db0 zvd0 Bz0 1tB0 TX0 1wp0 Qn0 qO0 16n0 Rb0 1wp0 TX0 g0p0 10M0 ako0 7B0 8zb0 uL0","America/Argentina/Mendoza|CMT ART ARST ART ARST WART WARST|4g.M 40 30 30 20 40 30|0121212121212121212121212121212121212121213434345656543235343|-20UHH.c pKnH.c Mn0 1iN0 Tb0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 MN0 2jz0 MN0 4lX0 u10 5Lb0 1pB0 Fnz0 u10 uL0 1vd0 SL0 1vd0 SL0 1vd0 17z0 1cN0 1fz0 1cN0 1cL0 1cN0 asn0 Db0 zvd0 Bz0 1tB0 TX0 1u20 SL0 1vd0 Tb0 1wp0 TW0 g0p0 10M0 agM0 Op0 7TX0 uL0","America/Argentina/Rio_Gallegos|CMT ART ARST ART ARST WART|4g.M 40 30 30 20 40|0121212121212121212121212121212121212121213434343434343235343|-20UHH.c pKnH.c Mn0 1iN0 Tb0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 MN0 2jz0 MN0 4lX0 u10 5Lb0 1pB0 Fnz0 u10 uL0 1vd0 SL0 1vd0 SL0 1vd0 17z0 1cN0 1fz0 1cN0 1cL0 1cN0 asn0 Db0 zvd0 Bz0 1tB0 TX0 1wp0 Rb0 1wp0 Rb0 1wp0 TX0 g0p0 10M0 ako0 7B0 8zb0 uL0","America/Argentina/Salta|CMT ART ARST ART ARST WART|4g.M 40 30 30 20 40|01212121212121212121212121212121212121212134343434543432343|-20UHH.c pKnH.c Mn0 1iN0 Tb0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 MN0 2jz0 MN0 4lX0 u10 5Lb0 1pB0 Fnz0 u10 uL0 1vd0 SL0 1vd0 SL0 1vd0 17z0 1cN0 1fz0 1cN0 1cL0 1cN0 asn0 Db0 zvd0 Bz0 1tB0 TX0 1wp0 Rb0 1wq0 Ra0 1wp0 TX0 g0p0 10M0 j3c0 uL0","America/Argentina/San_Juan|CMT ART ARST ART ARST WART|4g.M 40 30 30 20 40|01212121212121212121212121212121212121212134343434534343235343|-20UHH.c pKnH.c Mn0 1iN0 Tb0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 MN0 2jz0 MN0 4lX0 u10 5Lb0 1pB0 Fnz0 u10 uL0 1vd0 SL0 1vd0 SL0 1vd0 17z0 1cN0 1fz0 1cN0 1cL0 1cN0 asn0 Db0 zvd0 Bz0 1tB0 TX0 1wp0 Qn0 qO0 16n0 Rb0 1wp0 TX0 g0p0 10M0 ak00 m10 8lb0 uL0","America/Argentina/San_Luis|CMT ART ARST ART ARST WART WARST|4g.M 40 30 30 20 40 30|01212121212121212121212121212121212121212134343456536353465653|-20UHH.c pKnH.c Mn0 1iN0 Tb0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 MN0 2jz0 MN0 4lX0 u10 5Lb0 1pB0 Fnz0 u10 uL0 1vd0 SL0 1vd0 SL0 1vd0 17z0 1cN0 1fz0 1cN0 1cL0 1cN0 asn0 Db0 zvd0 Bz0 1tB0 XX0 1q20 SL0 AN0 kin0 10M0 ak00 m10 8lb0 8L0 jd0 1qN0 WL0 1qN0","America/Argentina/Tucuman|CMT ART ARST ART ARST WART|4g.M 40 30 30 20 40|012121212121212121212121212121212121212121343434345434323534343|-20UHH.c pKnH.c Mn0 1iN0 Tb0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 MN0 2jz0 MN0 4lX0 u10 5Lb0 1pB0 Fnz0 u10 uL0 1vd0 SL0 1vd0 SL0 1vd0 17z0 1cN0 1fz0 1cN0 1cL0 1cN0 asn0 Db0 zvd0 Bz0 1tB0 TX0 1wp0 Rb0 1wq0 Ra0 1wp0 TX0 g0p0 10M0 ako0 4N0 8BX0 uL0 1qN0 WL0","America/Argentina/Ushuaia|CMT ART ARST ART ARST WART|4g.M 40 30 30 20 40|0121212121212121212121212121212121212121213434343434343235343|-20UHH.c pKnH.c Mn0 1iN0 Tb0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 1C10 LX0 1C10 LX0 1C10 LX0 1C10 Mn0 MN0 2jz0 MN0 4lX0 u10 5Lb0 1pB0 Fnz0 u10 uL0 1vd0 SL0 1vd0 SL0 1vd0 17z0 1cN0 1fz0 1cN0 1cL0 1cN0 asn0 Db0 zvd0 Bz0 1tB0 TX0 1wp0 Rb0 1wp0 Rb0 1wp0 TX0 g0p0 10M0 ajA0 8p0 8zb0 uL0","America/Aruba|LMT ANT AST|4z.L 4u 40|012|-2kV7o.d 28KLS.d","America/Asuncion|AMT PYT PYT PYST|3O.E 40 30 30|012131313131313131313131313131313131313131313131313131313131313131313131313131313131313131313131313131313131313131313131313131313|-1x589.k 1DKM9.k 3CL0 3Dd0 10L0 1pB0 10n0 1pB0 10n0 1pB0 1cL0 1dd0 1db0 1dd0 1cL0 1dd0 1cL0 1dd0 1cL0 1dd0 1db0 1dd0 1cL0 1dd0 1cL0 1dd0 1cL0 1dd0 1db0 1dd0 1cL0 1lB0 14n0 1dd0 1cL0 1fd0 WL0 1rd0 1aL0 1dB0 Xz0 1qp0 Xb0 1qN0 10L0 1rB0 TX0 1tB0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1qN0 1cL0 WN0 1qL0 11B0 1nX0 1ip0 WL0 1qN0 WL0 1qN0 WL0 1tB0 TX0 1tB0 TX0 1tB0 19X0 1a10 1fz0 1a10 1fz0 1cN0 17b0 1ip0 17b0 1ip0 17b0 1ip0 19X0 1fB0 19X0 1fB0 19X0 1ip0 17b0 1ip0 17b0 1ip0 19X0 1fB0 19X0 1fB0 19X0 1fB0 19X0 1ip0 17b0 1ip0 17b0 1ip0 19X0 1fB0 19X0 1fB0 19X0 1ip0 17b0 1ip0 17b0 1ip0 19X0 1fB0 19X0 1fB0 19X0 1fB0 19X0 1ip0 17b0 1ip0 17b0 1ip0","America/Atikokan|CST CDT CWT CPT EST|60 50 50 50 50|0101234|-25TQ0 1in0 Rnb0 3je0 8x30 iw0","America/Bahia|LMT BRT BRST|2y.4 30 20|01212121212121212121212121212121212121212121212121212121212121|-2glxp.U HdLp.U 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 qe10 xb0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0 1EN0 FX0 1HB0 Lz0 1EN0 Lz0 1C10 IL0 1HB0 Db0 1HB0 On0 1zd0 On0 1zd0 Lz0 1zd0 Rb0 1wN0 Wn0 1tB0 Rb0 1tB0 WL0 1tB0 Rb0 1zd0 On0 1HB0 FX0 l5B0 Rb0","America/Bahia_Banderas|LMT MST CST PST MDT CDT|71 70 60 80 60 50|0121212131414141414141414141414141414152525252525252525252525252525252525252525252525252525252|-1UQF0 deL0 8lc0 17c0 10M0 1dd0 otX0 gmN0 P2N0 13Vd0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 1fB0 WL0 1fB0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nW0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0","America/Barbados|LMT BMT AST ADT|3W.t 3W.t 40 30|01232323232|-1Q0I1.v jsM0 1ODC1.v IL0 1ip0 17b0 1ip0 17b0 1ld0 13b0","America/Belem|LMT BRT BRST|3d.U 30 20|012121212121212121212121212121|-2glwK.4 HdKK.4 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 qe10 xb0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0","America/Belize|LMT CST CHDT CDT|5Q.M 60 5u 50|01212121212121212121212121212121212121212121212121213131|-2kBu7.c fPA7.c Onu 1zcu Rbu 1wou Rbu 1wou Rbu 1zcu Onu 1zcu Onu 1zcu Rbu 1wou Rbu 1wou Rbu 1wou Rbu 1zcu Onu 1zcu Onu 1zcu Rbu 1wou Rbu 1wou Rbu 1zcu Onu 1zcu Onu 1zcu Onu 1zcu Rbu 1wou Rbu 1wou Rbu 1zcu Onu 1zcu Onu 1zcu Rbu 1wou Rbu 1f0Mu qn0 lxB0 mn0","America/Blanc-Sablon|AST ADT AWT APT|40 30 30 30|010230|-25TS0 1in0 UGp0 8x50 iu0","America/Boa_Vista|LMT AMT AMST|42.E 40 30|0121212121212121212121212121212121|-2glvV.k HdKV.k 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 qe10 xb0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0 smp0 WL0 1tB0 2L0","America/Bogota|BMT COT COST|4U.g 50 40|0121|-2eb73.I 38yo3.I 2en0","America/Boise|PST PDT MST MWT MPT MDT|80 70 70 60 60 60|0101023425252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252|-261q0 1nX0 11B0 1nX0 8C10 JCL0 8x20 ix0 QwN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 Dd0 1Kn0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Cambridge_Bay|zzz MST MWT MPT MDDT MDT CST CDT EST|0 70 60 60 50 60 60 50 50|0123141515151515151515151515151515151515151515678651515151515151515151515151515151515151515151515151515151515151515151515151|-21Jc0 RO90 8x20 ix0 LCL0 1fA0 zgO0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11A0 1nX0 2K0 WQ0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Campo_Grande|LMT AMT AMST|3C.s 40 30|012121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212|-2glwl.w HdLl.w 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 qe10 xb0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0 1EN0 FX0 1HB0 Lz0 1EN0 Lz0 1C10 IL0 1HB0 Db0 1HB0 On0 1zd0 On0 1zd0 Lz0 1zd0 Rb0 1wN0 Wn0 1tB0 Rb0 1tB0 WL0 1tB0 Rb0 1zd0 On0 1HB0 FX0 1C10 Lz0 1Ip0 HX0 1zd0 On0 1HB0 IL0 1wp0 On0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 Rb0 1zd0 Lz0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 On0 1zd0 On0 1C10 Lz0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 Rb0 1wp0 On0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 On0 1zd0 On0 1C10 Lz0 1C10 Lz0 1C10 Lz0 1C10 On0 1zd0 Rb0 1wp0 On0 1C10 Lz0 1C10 On0 1zd0","America/Cancun|LMT CST EST EDT CDT|5L.4 60 50 40 50|0123232341414141414141414141414141414141412|-1UQG0 2q2o0 yLB0 1lb0 14p0 1lb0 14p0 Lz0 xB0 14p0 1nX0 11B0 1nX0 1fB0 WL0 1fB0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 Dd0","America/Caracas|CMT VET VET|4r.E 4u 40|0121|-2kV7w.k 28KM2.k 1IwOu","America/Cayenne|LMT GFT GFT|3t.k 40 30|012|-2mrwu.E 2gWou.E","America/Cayman|CMT EST|5j.A 50|01|-2uduE.o","America/Chicago|CST CDT EST CWT CPT|60 50 50 50 50|01010101010101010101010101010101010102010101010103401010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-261s0 1nX0 11B0 1nX0 1wp0 TX0 WN0 1qL0 1cN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 11B0 1Hz0 14p0 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 RB0 8x30 iw0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Chihuahua|LMT MST CST CDT MDT|74.k 70 60 50 60|0121212323241414141414141414141414141414141414141414141414141414141414141414141414141414141|-1UQF0 deL0 8lc0 17c0 10M0 1dd0 2zQN0 1lb0 14p0 1lb0 14q0 1lb0 14p0 1nX0 11B0 1nX0 1fB0 WL0 1fB0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0","America/Costa_Rica|SJMT CST CDT|5A.d 60 50|0121212121|-1Xd6n.L 2lu0n.L Db0 1Kp0 Db0 pRB0 15b0 1kp0 mL0","America/Creston|MST PST|70 80|010|-29DR0 43B0","America/Cuiaba|LMT AMT AMST|3I.k 40 30|0121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212|-2glwf.E HdLf.E 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 qe10 xb0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0 1EN0 FX0 1HB0 Lz0 1EN0 Lz0 1C10 IL0 1HB0 Db0 1HB0 On0 1zd0 On0 1zd0 Lz0 1zd0 Rb0 1wN0 Wn0 1tB0 Rb0 1tB0 WL0 1tB0 Rb0 1zd0 On0 1HB0 FX0 4a10 HX0 1zd0 On0 1HB0 IL0 1wp0 On0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 Rb0 1zd0 Lz0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 On0 1zd0 On0 1C10 Lz0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 Rb0 1wp0 On0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 On0 1zd0 On0 1C10 Lz0 1C10 Lz0 1C10 Lz0 1C10 On0 1zd0 Rb0 1wp0 On0 1C10 Lz0 1C10 On0 1zd0","America/Danmarkshavn|LMT WGT WGST GMT|1e.E 30 20 0|01212121212121212121212121212121213|-2a5WJ.k 2z5fJ.k 19U0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 DC0","America/Dawson|YST YDT YWT YPT YDDT PST PDT|90 80 80 80 70 80 70|0101023040565656565656565656565656565656565656565656565656565656565656565656565656565656565656565656565656565656565656565656565|-25TN0 1in0 1o10 13V0 Ser0 8x00 iz0 LCL0 1fA0 jrA0 fNd0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Dawson_Creek|PST PDT PWT PPT MST|80 70 70 70 70|0102301010101010101010101010101010101010101010101010101014|-25TO0 1in0 UGp0 8x10 iy0 3NB0 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 ML0","America/Denver|MST MDT MWT MPT|70 60 60 60|01010101023010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-261r0 1nX0 11B0 1nX0 11B0 1qL0 WN0 mn0 Ord0 8x20 ix0 LCN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Detroit|LMT CST EST EWT EPT EDT|5w.b 60 50 40 40 40|01234252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252|-2Cgir.N peqr.N 156L0 8x40 iv0 6fd0 11z0 Jy10 SL0 dnB0 1cL0 s10 1Vz0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Edmonton|LMT MST MDT MWT MPT|7x.Q 70 60 60 60|01212121212121341212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2yd4q.8 shdq.8 1in0 17d0 hz0 2dB0 1fz0 1a10 11z0 1qN0 WL0 1qN0 11z0 IGN0 8x20 ix0 3NB0 11z0 LFB0 1cL0 3Cp0 1cL0 66N0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Eirunepe|LMT ACT ACST AMT|4D.s 50 40 40|0121212121212121212121212121212131|-2glvk.w HdLk.w 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 qe10 xb0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0 dPB0 On0 yTd0 d5X0","America/El_Salvador|LMT CST CDT|5U.M 60 50|012121|-1XiG3.c 2Fvc3.c WL0 1qN0 WL0","America/Ensenada|LMT MST PST PDT PWT PPT|7M.4 70 80 70 70 70|012123245232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232|-1UQE0 4PX0 8mM0 8lc0 SN0 1cL0 pHB0 83r0 zI0 5O10 1Rz0 cOP0 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 BUp0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 U10 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Fort_Wayne|CST CDT CWT CPT EST EDT|60 50 50 50 50 40|010101023010101010101010101040454545454545454545454545454545454545454545454545454545454545454545454|-261s0 1nX0 11B0 1nX0 QI10 Db0 RB0 8x30 iw0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 5Tz0 1o10 qLb0 1cL0 1cN0 1cL0 1qhd0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Fortaleza|LMT BRT BRST|2y 30 20|0121212121212121212121212121212121212121|-2glxq HdLq 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 qe10 xb0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0 1EN0 FX0 1HB0 Lz0 nsp0 WL0 1tB0 5z0 2mN0 On0","America/Glace_Bay|LMT AST ADT AWT APT|3X.M 40 30 30 30|012134121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2IsI0.c CwO0.c 1in0 UGp0 8x50 iu0 iq10 11z0 Jg10 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Godthab|LMT WGT WGST|3q.U 30 20|0121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2a5Ux.4 2z5dx.4 19U0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","America/Goose_Bay|NST NDT NST NDT NWT NPT AST ADT ADDT|3u.Q 2u.Q 3u 2u 2u 2u 40 30 20|010232323232323245232323232323232323232323232323232323232326767676767676767676767676767676767676767676768676767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676|-25TSt.8 1in0 DXb0 2HbX.8 WL0 1qN0 WL0 1qN0 WL0 1tB0 TX0 1tB0 WL0 1qN0 WL0 1qN0 7UHu itu 1tB0 WL0 1qN0 WL0 1qN0 WL0 1qN0 WL0 1tB0 WL0 1ld0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 S10 g0u 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14n1 1lb0 14p0 1nW0 11C0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zcX Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Grand_Turk|KMT EST EDT AST|57.b 50 40 40|0121212121212121212121212121212121212121212121212121212121212121212121212123|-2l1uQ.N 2HHBQ.N 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Guatemala|LMT CST CDT|62.4 60 50|0121212121|-24KhV.U 2efXV.U An0 mtd0 Nz0 ifB0 17b0 zDB0 11z0","America/Guayaquil|QMT ECT|5e 50|01|-1yVSK","America/Guyana|LMT GBGT GYT GYT GYT|3Q.E 3J 3J 30 40|01234|-2dvU7.k 24JzQ.k mlc0 Bxbf","America/Halifax|LMT AST ADT AWT APT|4e.o 40 30 30 30|0121212121212121212121212121212121212121212121212134121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2IsHJ.A xzzJ.A 1db0 3I30 1in0 3HX0 IL0 1E10 ML0 1yN0 Pb0 1Bd0 Mn0 1Bd0 Rz0 1w10 Xb0 1w10 LX0 1w10 Xb0 1w10 Lz0 1C10 Jz0 1E10 OL0 1yN0 Un0 1qp0 Xb0 1qp0 11X0 1w10 Lz0 1HB0 LX0 1C10 FX0 1w10 Xb0 1qp0 Xb0 1BB0 LX0 1td0 Xb0 1qp0 Xb0 Rf0 8x50 iu0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 3Qp0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 3Qp0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 6i10 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Havana|HMT CST CDT|5t.A 50 40|012121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-1Meuu.o 72zu.o ML0 sld0 An0 1Nd0 Db0 1Nd0 An0 6Ep0 An0 1Nd0 An0 JDd0 Mn0 1Ap0 On0 1fd0 11X0 1qN0 WL0 1wp0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 14n0 1ld0 14L0 1kN0 15b0 1kp0 1cL0 1cN0 1fz0 1a10 1fz0 1fB0 11z0 14p0 1nX0 11B0 1nX0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 14n0 1ld0 14n0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 1a10 1in0 1a10 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1cM0 1cM0 1fA0 17c0 1o00 11A0 1qM0 11A0 1o00 11A0 1o00 14o0 1lc0 14o0 1lc0 11A0 6i00 Rc0 1wo0 U00 1tA0 Rc0 1wo0 U00 1wo0 U00 1zc0 U00 1qM0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0","America/Hermosillo|LMT MST CST PST MDT|7n.Q 70 60 80 60|0121212131414141|-1UQF0 deL0 8lc0 17c0 10M0 1dd0 otX0 gmN0 P2N0 13Vd0 1lb0 14p0 1lb0 14p0 1lb0","America/Indiana/Knox|CST CDT CWT CPT EST|60 50 50 50 50|0101023010101010101010101010101010101040101010101010101010101010101010101010101010101010141010101010101010101010101010101010101010101010101010101010101010|-261s0 1nX0 11B0 1nX0 SgN0 8x30 iw0 3NB0 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 1fz0 1cN0 1cL0 1cN0 11z0 1o10 11z0 1o10 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 3Cn0 8wp0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 z8o0 1o00 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Indiana/Marengo|CST CDT CWT CPT EST EDT|60 50 50 50 50 40|0101023010101010101010104545454545414545454545454545454545454545454545454545454545454545454545454545454|-261s0 1nX0 11B0 1nX0 SgN0 8x30 iw0 dyN0 11z0 6fd0 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 jrz0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1VA0 LA0 1BX0 1e6p0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Indiana/Petersburg|CST CDT CWT CPT EST EDT|60 50 50 50 50 40|01010230101010101010101010104010101010101010101010141014545454545454545454545454545454545454545454545454545454545454|-261s0 1nX0 11B0 1nX0 SgN0 8x30 iw0 njX0 WN0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 3Fb0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 19co0 1o00 Rd0 1zb0 Oo0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Indiana/Tell_City|CST CDT CWT CPT EST EDT|60 50 50 50 50 40|01010230101010101010101010101010454541010101010101010101010101010101010101010101010101010101010101010|-261s0 1nX0 11B0 1nX0 SgN0 8x30 iw0 1o10 11z0 g0p0 11z0 1o10 11z0 1qL0 WN0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 1fz0 1cN0 WL0 1qN0 1cL0 1cN0 1cL0 1cN0 caL0 1cL0 1cN0 1cL0 1qhd0 1o00 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Indiana/Vevay|CST CDT CWT CPT EST EDT|60 50 50 50 50 40|010102304545454545454545454545454545454545454545454545454545454545454545454545454|-261s0 1nX0 11B0 1nX0 SgN0 8x30 iw0 kPB0 Awn0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1lnd0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Indiana/Vincennes|CST CDT CWT CPT EST EDT|60 50 50 50 50 40|01010230101010101010101010101010454541014545454545454545454545454545454545454545454545454545454545454|-261s0 1nX0 11B0 1nX0 SgN0 8x30 iw0 1o10 11z0 g0p0 11z0 1o10 11z0 1qL0 WN0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 1fz0 1cN0 WL0 1qN0 1cL0 1cN0 1cL0 1cN0 caL0 1cL0 1cN0 1cL0 1qhd0 1o00 Rd0 1zb0 Oo0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Indiana/Winamac|CST CDT CWT CPT EST EDT|60 50 50 50 50 40|01010230101010101010101010101010101010454541054545454545454545454545454545454545454545454545454545454545454|-261s0 1nX0 11B0 1nX0 SgN0 8x30 iw0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 1fz0 1cN0 1cL0 1cN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 jrz0 1cL0 1cN0 1cL0 1qhd0 1o00 Rd0 1za0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Inuvik|zzz PST PDDT MST MDT|0 80 60 70 60|0121343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343|-FnA0 tWU0 1fA0 wPe0 2pz0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Iqaluit|zzz EWT EPT EST EDDT EDT CST CDT|0 40 40 50 30 40 60 50|01234353535353535353535353535353535353535353567353535353535353535353535353535353535353535353535353535353535353535353535353|-16K00 7nX0 iv0 LCL0 1fA0 zgO0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11C0 1nX0 11A0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Jamaica|KMT EST EDT|57.b 50 40|0121212121212121212121|-2l1uQ.N 2uM1Q.N 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0","America/Juneau|PST PWT PPT PDT YDT YST AKST AKDT|80 70 70 70 80 90 90 80|01203030303030303030303030403030356767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676|-17T20 8x10 iy0 Vo10 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cM0 1cM0 1cL0 1cN0 1fz0 1a10 1fz0 co0 10q0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Kentucky/Louisville|CST CDT CWT CPT EST EDT|60 50 50 50 50 40|0101010102301010101010101010101010101454545454545414545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454|-261s0 1nX0 11B0 1nX0 3Fd0 Nb0 LPd0 11z0 RB0 8x30 iw0 Bb0 10N0 2bB0 8in0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 xz0 gso0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1VA0 LA0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Kentucky/Monticello|CST CDT CWT CPT EST EDT|60 50 50 50 50 40|0101023010101010101010101010101010101010101010101010101010101010101010101454545454545454545454545454545454545454545454545454545454545454545454545454|-261s0 1nX0 11B0 1nX0 SgN0 8x30 iw0 SWp0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11A0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/La_Paz|CMT BOST BOT|4w.A 3w.A 40|012|-1x37r.o 13b0","America/Lima|LMT PET PEST|58.A 50 40|0121212121212121|-2tyGP.o 1bDzP.o zX0 1aN0 1cL0 1cN0 1cL0 1PrB0 zX0 1O10 zX0 6Gp0 zX0 98p0 zX0","America/Los_Angeles|PST PDT PWT PPT|80 70 70 70|010102301010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-261q0 1nX0 11B0 1nX0 SgN0 8x10 iy0 5Wp0 1Vb0 3dB0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Maceio|LMT BRT BRST|2m.Q 30 20|012121212121212121212121212121212121212121|-2glxB.8 HdLB.8 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 qe10 xb0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0 1EN0 FX0 1HB0 Lz0 dMN0 Lz0 8Q10 WL0 1tB0 5z0 2mN0 On0","America/Managua|MMT CST EST CDT|5J.c 60 50 50|0121313121213131|-1quie.M 1yAMe.M 4mn0 9Up0 Dz0 1K10 Dz0 s3F0 1KH0 DB0 9In0 k8p0 19X0 1o30 11y0","America/Manaus|LMT AMT AMST|40.4 40 30|01212121212121212121212121212121|-2glvX.U HdKX.U 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 qe10 xb0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0 dPB0 On0","America/Martinique|FFMT AST ADT|44.k 40 30|0121|-2mPTT.E 2LPbT.E 19X0","America/Matamoros|LMT CST CDT|6E 60 50|0121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-1UQG0 2FjC0 1nX0 i6p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 1fB0 WL0 1fB0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 U10 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Mazatlan|LMT MST CST PST MDT|75.E 70 60 80 60|0121212131414141414141414141414141414141414141414141414141414141414141414141414141414141414141|-1UQF0 deL0 8lc0 17c0 10M0 1dd0 otX0 gmN0 P2N0 13Vd0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 1fB0 WL0 1fB0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0","America/Menominee|CST CDT CWT CPT EST|60 50 50 50 50|01010230101041010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-261s0 1nX0 11B0 1nX0 SgN0 8x30 iw0 1o10 11z0 LCN0 1fz0 6410 9Jb0 1cM0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Merida|LMT CST EST CDT|5W.s 60 50 50|0121313131313131313131313131313131313131313131313131313131313131313131313131313131313131|-1UQG0 2q2o0 2hz0 wu30 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 1fB0 WL0 1fB0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0","America/Metlakatla|PST PWT PPT PDT|80 70 70 70|0120303030303030303030303030303030|-17T20 8x10 iy0 Vo10 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0","America/Mexico_City|LMT MST CST CDT CWT|6A.A 70 60 50 50|012121232324232323232323232323232323232323232323232323232323232323232323232323232323232323232323232|-1UQF0 deL0 8lc0 17c0 10M0 1dd0 gEn0 TX0 3xd0 Jb0 6zB0 SL0 e5d0 17b0 1Pff0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 1fB0 WL0 1fB0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0","America/Miquelon|LMT AST PMST PMDT|3I.E 40 30 20|012323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232|-2mKkf.k 2LTAf.k gQ10 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Moncton|EST AST ADT AWT APT|50 40 30 30 30|012121212121212121212134121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2IsH0 CwN0 1in0 zAo0 An0 1Nd0 An0 1Nd0 An0 1Nd0 An0 1Nd0 An0 1Nd0 An0 1K10 Lz0 1zB0 NX0 1u10 Wn0 S20 8x50 iu0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 11z0 1o10 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 3Cp0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14n1 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 ReX 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Monterrey|LMT CST CDT|6F.g 60 50|0121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-1UQG0 2FjC0 1nX0 i6p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 1fB0 WL0 1fB0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0","America/Montevideo|MMT UYT UYHST UYST UYT UYHST|3I.I 3u 30 20 30 2u|012121212121212121212121213434343434345454543453434343434343434343434343434343434343434343434343434343434343434343434343434343434343|-20UIf.g 8jzJ.g 1cLu 1dcu 1cLu 1dcu 1cLu ircu 11zu 1o0u 11zu 1o0u 11zu 1qMu WLu 1qMu WLu 1qMu WLu 1qMu 11zu 1o0u 11zu NAu 11bu 2iMu zWu Dq10 19X0 pd0 jz0 cm10 19X0 1fB0 1on0 11d0 1oL0 1nB0 1fzu 1aou 1fzu 1aou 1fzu 3nAu Jb0 3MN0 1SLu 4jzu 2PB0 Lb0 3Dd0 1pb0 ixd0 An0 1MN0 An0 1wp0 On0 1wp0 Rb0 1zd0 On0 1wp0 Rb0 s8p0 1fB0 1ip0 11z0 1ld0 14n0 1o10 11z0 1o10 11z0 1o10 14n0 1ld0 14n0 1ld0 14n0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 14n0 1ld0 14n0 1ld0 14n0 1o10 11z0 1o10 11z0 1o10 14n0 1ld0 14n0 1ld0 14n0 1ld0 14n0 1o10 11z0 1o10 11z0 1o10 14n0 1ld0 14n0 1ld0 14n0 1o10 11z0 1o10 11z0 1o10 14n0 1ld0 14n0 1ld0 14n0 1ld0 14n0 1o10 11z0 1o10 11z0 1o10","America/Montreal|EST EDT EWT EPT|50 40 40 40|01010101010101010101010101010101010101010101012301010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-25TR0 1in0 11Wu 1nzu 1fD0 WJ0 1wr0 Nb0 1Ap0 On0 1zd0 On0 1wp0 TX0 1tB0 TX0 1tB0 TX0 1tB0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 4kM0 8x40 iv0 1o10 11z0 1nX0 11z0 1o10 11z0 1o10 1qL0 11D0 1nX0 11B0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 11z0 1o10 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Nassau|LMT EST EDT|59.u 50 40|012121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2kNuO.u 26XdO.u 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/New_York|EST EDT EWT EPT|50 40 40 40|01010101010101010101010101010101010101010101010102301010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-261t0 1nX0 11B0 1nX0 11B0 1qL0 1a10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 RB0 8x40 iv0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Nipigon|EST EDT EWT EPT|50 40 40 40|010123010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-25TR0 1in0 Rnb0 3je0 8x40 iv0 19yN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Nome|NST NWT NPT BST BDT YST AKST AKDT|b0 a0 a0 b0 a0 90 90 80|012034343434343434343434343434343456767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676767676|-17SX0 8wW0 iB0 Qlb0 52O0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 cl0 10q0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Noronha|LMT FNT FNST|29.E 20 10|0121212121212121212121212121212121212121|-2glxO.k HdKO.k 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 qe10 xb0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0 1EN0 FX0 1HB0 Lz0 nsp0 WL0 1tB0 2L0 2pB0 On0","America/North_Dakota/Beulah|MST MDT MWT MPT CST CDT|70 60 60 60 60 50|010102301010101010101010101010101010101010101010101010101010101010101010101010101010101010101014545454545454545454545454545454545454545454545454545454|-261r0 1nX0 11B0 1nX0 SgN0 8x20 ix0 QwN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Oo0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/North_Dakota/Center|MST MDT MWT MPT CST CDT|70 60 60 60 60 50|010102301010101010101010101010101010101010101010101010101014545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454|-261r0 1nX0 11B0 1nX0 SgN0 8x20 ix0 QwN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14o0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/North_Dakota/New_Salem|MST MDT MWT MPT CST CDT|70 60 60 60 60 50|010102301010101010101010101010101010101010101010101010101010101010101010101010101454545454545454545454545454545454545454545454545454545454545454545454|-261r0 1nX0 11B0 1nX0 SgN0 8x20 ix0 QwN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14o0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Ojinaga|LMT MST CST CDT MDT|6V.E 70 60 50 60|0121212323241414141414141414141414141414141414141414141414141414141414141414141414141414141|-1UQF0 deL0 8lc0 17c0 10M0 1dd0 2zQN0 1lb0 14p0 1lb0 14q0 1lb0 14p0 1nX0 11B0 1nX0 1fB0 WL0 1fB0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 U10 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Pangnirtung|zzz AST AWT APT ADDT ADT EDT EST CST CDT|0 40 30 30 20 30 40 50 60 50|012314151515151515151515151515151515167676767689767676767676767676767676767676767676767676767676767676767676767676767676767|-1XiM0 PnG0 8x50 iu0 LCL0 1fA0 zgO0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1o00 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11C0 1nX0 11A0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Paramaribo|LMT PMT PMT NEGT SRT SRT|3E.E 3E.Q 3E.A 3u 3u 30|012345|-2nDUj.k Wqo0.c qanX.I 1dmLN.o lzc0","America/Phoenix|MST MDT MWT|70 60 60|01010202010|-261r0 1nX0 11B0 1nX0 SgN0 4Al1 Ap0 1db0 SWqX 1cL0","America/Port-au-Prince|PPMT EST EDT|4N 50 40|0121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-28RHb 2FnMb 19X0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14q0 1o00 11A0 1o00 11A0 1o00 14o0 1lc0 14o0 1lc0 14o0 1o00 11A0 1o00 11A0 1o00 14o0 1lc0 14o0 1lc0 i6n0 1nX0 11B0 1nX0 d430 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Porto_Acre|LMT ACT ACST AMT|4v.c 50 40 40|01212121212121212121212121212131|-2glvs.M HdLs.M 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 qe10 xb0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0 NBd0 d5X0","America/Porto_Velho|LMT AMT AMST|4f.A 40 30|012121212121212121212121212121|-2glvI.o HdKI.o 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 qe10 xb0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0","America/Puerto_Rico|AST AWT APT|40 30 30|0120|-17lU0 7XT0 iu0","America/Rainy_River|CST CDT CWT CPT|60 50 50 50|010123010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-25TQ0 1in0 Rnb0 3je0 8x30 iw0 19yN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Rankin_Inlet|zzz CST CDDT CDT EST|0 60 40 50 50|012131313131313131313131313131313131313131313431313131313131313131313131313131313131313131313131313131313131313131313131|-vDc0 keu0 1fA0 zgO0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Recife|LMT BRT BRST|2j.A 30 20|0121212121212121212121212121212121212121|-2glxE.o HdLE.o 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 qe10 xb0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0 1EN0 FX0 1HB0 Lz0 nsp0 WL0 1tB0 2L0 2pB0 On0","America/Regina|LMT MST MDT MWT MPT CST|6W.A 70 60 60 60 60|012121212121212121212121341212121212121212121212121215|-2AD51.o uHe1.o 1in0 s2L0 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 66N0 1cL0 1cN0 19X0 1fB0 1cL0 1fB0 1cL0 1cN0 1cL0 M30 8x20 ix0 1ip0 1cL0 1ip0 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 3NB0 1cL0 1cN0","America/Resolute|zzz CST CDDT CDT EST|0 60 40 50 50|012131313131313131313131313131313131313131313431313131313431313131313131313131313131313131313131313131313131313131313131|-SnA0 GWS0 1fA0 zgO0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Santa_Isabel|LMT MST PST PDT PWT PPT|7D.s 70 80 70 70 70|012123245232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232|-1UQE0 4PX0 8mM0 8lc0 SN0 1cL0 pHB0 83r0 zI0 5O10 1Rz0 cOP0 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 BUp0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0","America/Santarem|LMT AMT AMST BRT|3C.M 40 30 30|0121212121212121212121212121213|-2glwl.c HdLl.c 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 qe10 xb0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0 NBd0","America/Santiago|SMT CLT CLT CLST CLST CLT|4G.K 50 40 40 30 30|01020313131313121242124242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424245|-2q2jh.e fJAh.e 5knG.K 1Vzh.e jRAG.K 1pbh.e 11d0 1oL0 11d0 1oL0 11d0 1oL0 11d0 1pb0 11d0 nHX0 op0 9Bz0 jb0 1oN0 ko0 Qeo0 WL0 1zd0 On0 1ip0 11z0 1o10 11z0 1qN0 WL0 1ld0 14n0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 WL0 1qN0 1cL0 1cN0 11z0 1o10 11z0 1qN0 WL0 1fB0 19X0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 17b0 1ip0 11z0 1ip0 1fz0 1fB0 11z0 1qN0 WL0 1qN0 WL0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 17b0 1ip0 11z0 1o10 19X0 1fB0 1nX0 G10 1EL0 Op0 1zb0 Rd0 1wn0 Rd0 1wn0","America/Santo_Domingo|SDMT EST EDT EHDT AST|4E 50 40 4u 40|01213131313131414|-1ttjk 1lJMk Mn0 6sp0 Lbu 1Cou yLu 1RAu wLu 1QMu xzu 1Q0u xXu 1PAu 13jB0 e00","America/Sao_Paulo|LMT BRT BRST|36.s 30 20|012121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212|-2glwR.w HdKR.w 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 pTd0 PX0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0 1EN0 FX0 1HB0 Lz0 1EN0 Lz0 1C10 IL0 1HB0 Db0 1HB0 On0 1zd0 On0 1zd0 Lz0 1zd0 Rb0 1wN0 Wn0 1tB0 Rb0 1tB0 WL0 1tB0 Rb0 1zd0 On0 1HB0 FX0 1C10 Lz0 1Ip0 HX0 1zd0 On0 1HB0 IL0 1wp0 On0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 Rb0 1zd0 Lz0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 On0 1zd0 On0 1C10 Lz0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 Rb0 1wp0 On0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 On0 1zd0 On0 1C10 Lz0 1C10 Lz0 1C10 Lz0 1C10 On0 1zd0 Rb0 1wp0 On0 1C10 Lz0 1C10 On0 1zd0","America/Scoresbysund|LMT CGT CGST EGST EGT|1r.Q 20 10 0 10|0121343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434|-2a5Ww.8 2z5ew.8 1a00 1cK0 1cL0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","America/Sitka|PST PWT PPT PDT YST AKST AKDT|80 70 70 70 90 90 80|01203030303030303030303030303030345656565656565656565656565656565656565656565656565656565656565656565656565656565656565656565656565656565656565|-17T20 8x10 iy0 Vo10 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 co0 10q0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/St_Johns|NST NDT NST NDT NWT NPT NDDT|3u.Q 2u.Q 3u 2u 2u 2u 1u|01010101010101010101010101010101010102323232323232324523232323232323232323232323232323232323232323232323232323232323232323232323232323232326232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232|-28oit.8 14L0 1nB0 1in0 1gm0 Dz0 1JB0 1cL0 1cN0 1cL0 1fB0 19X0 1fB0 19X0 1fB0 19X0 1fB0 19X0 1fB0 1cL0 1cN0 1cL0 1fB0 19X0 1fB0 19X0 1fB0 19X0 1fB0 19X0 1fB0 1cL0 1fB0 19X0 1fB0 19X0 10O0 eKX.8 19X0 1iq0 WL0 1qN0 WL0 1qN0 WL0 1tB0 TX0 1tB0 WL0 1qN0 WL0 1qN0 7UHu itu 1tB0 WL0 1qN0 WL0 1qN0 WL0 1qN0 WL0 1tB0 WL0 1ld0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14n1 1lb0 14p0 1nW0 11C0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zcX Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Swift_Current|LMT MST MDT MWT MPT CST|7b.k 70 60 60 60 60|012134121212121212121215|-2AD4M.E uHdM.E 1in0 UGp0 8x20 ix0 1o10 17b0 1ip0 11z0 1o10 11z0 1o10 11z0 isN0 1cL0 3Cp0 1cL0 1cN0 11z0 1qN0 WL0 pMp0","America/Tegucigalpa|LMT CST CDT|5M.Q 60 50|01212121|-1WGGb.8 2ETcb.8 WL0 1qN0 WL0 GRd0 AL0","America/Thule|LMT AST ADT|4z.8 40 30|012121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2a5To.Q 31NBo.Q 1cL0 1cN0 1cL0 1fB0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Thunder_Bay|CST EST EWT EPT EDT|60 50 40 40 40|0123141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141|-2q5S0 1iaN0 8x40 iv0 XNB0 1cL0 1cN0 1fz0 1cN0 1cL0 3Cp0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Vancouver|PST PDT PWT PPT|80 70 70 70|0102301010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-25TO0 1in0 UGp0 8x10 iy0 1o10 17b0 1ip0 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Whitehorse|YST YDT YWT YPT YDDT PST PDT|90 80 80 80 70 80 70|0101023040565656565656565656565656565656565656565656565656565656565656565656565656565656565656565656565656565656565656565656565|-25TN0 1in0 1o10 13V0 Ser0 8x00 iz0 LCL0 1fA0 3NA0 vrd0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Winnipeg|CST CDT CWT CPT|60 50 50 50|010101023010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-2aIi0 WL0 3ND0 1in0 Jap0 Rb0 aCN0 8x30 iw0 1tB0 11z0 1ip0 11z0 1o10 11z0 1o10 11z0 1rd0 10L0 1op0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 1cL0 1cN0 11z0 6i10 WL0 6i10 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1a00 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1a00 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 14o0 1lc0 14o0 1o00 11A0 1o00 11A0 1o00 14o0 1lc0 14o0 1lc0 14o0 1o00 11A0 1o00 11A0 1o00 14o0 1lc0 14o0 1lc0 14o0 1lc0 14o0 1o00 11A0 1o00 11A0 1o00 14o0 1lc0 14o0 1lc0 14o0 1o00 11A0 1o00 11A0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Yakutat|YST YWT YPT YDT AKST AKDT|90 80 80 80 90 80|01203030303030303030303030303030304545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454|-17T10 8x00 iz0 Vo10 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 cn0 10q0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","America/Yellowknife|zzz MST MWT MPT MDDT MDT|0 70 60 60 50 60|012314151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151|-1pdA0 hix0 8x20 ix0 LCL0 1fA0 zgO0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","Antarctica/Casey|zzz AWST CAST|0 -80 -b0|012121|-2q00 1DjS0 T90 40P0 KL0","Antarctica/Davis|zzz DAVT DAVT|0 -70 -50|01012121|-vyo0 iXt0 alj0 1D7v0 VB0 3Wn0 KN0","Antarctica/DumontDUrville|zzz PMT DDUT|0 -a0 -a0|0102|-U0o0 cfq0 bFm0","Antarctica/Macquarie|AEST AEDT zzz MIST|-a0 -b0 0 -b0|0102010101010101010101010101010101010101010101010101010101010101010101010101010101010101013|-29E80 19X0 4SL0 1ayy0 Lvs0 1cM0 1o00 Rc0 1wo0 Rc0 1wo0 U00 1wo0 LA0 1C00 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 11A0 1qM0 WM0 1qM0 Oo0 1zc0 Oo0 1zc0 Oo0 1wo0 WM0 1tA0 WM0 1tA0 U00 1tA0 U00 1tA0 11A0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 11A0 1o00 1io0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1cM0 1a00 1io0 1cM0 1cM0 1cM0 1cM0 1cM0","Antarctica/Mawson|zzz MAWT MAWT|0 -60 -50|012|-CEo0 2fyk0","Antarctica/McMurdo|NZMT NZST NZST NZDT|-bu -cu -c0 -d0|01020202020202020202020202023232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323|-1GCVu Lz0 1tB0 11zu 1o0u 11zu 1o0u 11zu 1o0u 14nu 1lcu 14nu 1lcu 1lbu 11Au 1nXu 11Au 1nXu 11Au 1nXu 11Au 1nXu 11Au 1qLu WMu 1qLu 11Au 1n1bu IM0 1C00 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1qM0 14o0 1lc0 14o0 1lc0 14o0 1lc0 17c0 1io0 17c0 1io0 17c0 1io0 17c0 1lc0 14o0 1lc0 14o0 1lc0 17c0 1io0 17c0 1io0 17c0 1lc0 14o0 1lc0 14o0 1lc0 17c0 1io0 17c0 1io0 17c0 1io0 17c0 1io0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1io0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1fA0 1a00 1fA0 1a00","Antarctica/Palmer|zzz ARST ART ART ARST CLT CLST CLT|0 30 40 30 20 40 30 30|012121212123435656565656565656565656565656565656565656565656565656565656565656567|-cao0 nD0 1vd0 SL0 1vd0 17z0 1cN0 1fz0 1cN0 1cL0 1cN0 asn0 Db0 jsN0 14N0 11z0 1o10 11z0 1qN0 WL0 1qN0 WL0 1qN0 1cL0 1cN0 11z0 1o10 11z0 1qN0 WL0 1fB0 19X0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 17b0 1ip0 11z0 1ip0 1fz0 1fB0 11z0 1qN0 WL0 1qN0 WL0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 17b0 1ip0 11z0 1o10 19X0 1fB0 1nX0 G10 1EL0 Op0 1zb0 Rd0 1wn0 Rd0 1wn0","Antarctica/Rothera|zzz ROTT|0 30|01|gOo0","Antarctica/Syowa|zzz SYOT|0 -30|01|-vs00","Antarctica/Troll|zzz UTC CEST|0 0 -20|01212121212121212121212121212121212121212121212121212121212121212121|1puo0 hd0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Antarctica/Vostok|zzz VOST|0 -60|01|-tjA0","Arctic/Longyearbyen|CET CEST|-10 -20|010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-2awM0 Qm0 W6o0 5pf0 WM0 1fA0 1cM0 1cM0 1cM0 1cM0 wJc0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1qM0 WM0 zpc0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Asia/Aden|LMT AST|-36.Q -30|01|-TvD6.Q","Asia/Almaty|LMT ALMT ALMT ALMST|-57.M -50 -60 -70|0123232323232323232323232323232323232323232323232|-1Pc57.M eUo7.M 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 3Cl0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0","Asia/Amman|LMT EET EEST|-2n.I -20 -30|0121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-1yW2n.I 1HiMn.I KL0 1oN0 11b0 1oN0 11b0 1pd0 1dz0 1cp0 11b0 1op0 11b0 fO10 1db0 1e10 1cL0 1cN0 1cL0 1cN0 1fz0 1pd0 10n0 1ld0 14n0 1hB0 15b0 1ip0 19X0 1cN0 1cL0 1cN0 17b0 1ld0 14o0 1lc0 17c0 1io0 17c0 1io0 17c0 1So0 y00 1fc0 1dc0 1co0 1dc0 1cM0 1cM0 1cM0 1o00 11A0 1lc0 17c0 1cM0 1cM0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 4bX0 Dd0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0","Asia/Anadyr|LMT ANAT ANAT ANAST ANAST ANAST ANAT|-bN.U -c0 -d0 -e0 -d0 -c0 -b0|01232414141414141414141561414141414141414141414141414141414141561|-1PcbN.U eUnN.U 23CL0 1db0 1cN0 1dc0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qN0 WM0","Asia/Aqtau|LMT FORT FORT SHET SHET SHEST AQTT AQTST AQTST AQTT|-3l.4 -40 -50 -50 -60 -60 -50 -60 -50 -40|012345353535353535353536767676898989898989898989896|-1Pc3l.4 eUnl.4 1jcL0 JDc0 1cL0 1dc0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 2UK0 Fz0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cN0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 RW0","Asia/Aqtobe|LMT AKTT AKTT AKTST AKTT AQTT AQTST|-3M.E -40 -50 -60 -60 -50 -60|01234323232323232323232565656565656565656565656565|-1Pc3M.E eUnM.E 23CL0 1db0 1cM0 1dc0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 2UK0 Fz0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0","Asia/Ashgabat|LMT ASHT ASHT ASHST ASHST TMT TMT|-3R.w -40 -50 -60 -50 -40 -50|012323232323232323232324156|-1Pc3R.w eUnR.w 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 ba0 xC0","Asia/Baghdad|BMT AST ADT|-2V.A -30 -40|012121212121212121212121212121212121212121212121212121|-26BeV.A 2ACnV.A 11b0 1cp0 1dz0 1dd0 1db0 1cN0 1cp0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1de0 1dc0 1dc0 1dc0 1cM0 1dc0 1cM0 1dc0 1cM0 1dc0 1dc0 1dc0 1cM0 1dc0 1cM0 1dc0 1cM0 1dc0 1dc0 1dc0 1cM0 1dc0 1cM0 1dc0 1cM0 1dc0 1dc0 1dc0 1cM0 1dc0 1cM0 1dc0 1cM0 1dc0","Asia/Bahrain|LMT GST AST|-3q.8 -40 -30|012|-21Jfq.8 27BXq.8","Asia/Baku|LMT BAKT BAKT BAKST BAKST AZST AZT AZT AZST|-3j.o -30 -40 -50 -40 -40 -30 -40 -50|0123232323232323232323245657878787878787878787878787878787878787878787878787878787878787878787878787878787878787|-1Pc3j.o 1jUoj.o WCL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 10K0 c30 1cJ0 1cL0 8wu0 1o00 11z0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Asia/Bangkok|BMT ICT|-6G.4 -70|01|-218SG.4","Asia/Beirut|EET EEST|-20 -30|010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-21aq0 1on0 1410 1db0 19B0 1in0 1ip0 WL0 1lQp0 11b0 1oN0 11b0 1oN0 11b0 1pd0 11b0 1oN0 11b0 q6N0 En0 1oN0 11b0 1oN0 11b0 1oN0 11b0 1pd0 11b0 1oN0 11b0 1op0 11b0 dA10 17b0 1iN0 17b0 1iN0 17b0 1iN0 17b0 1vB0 SL0 1mp0 13z0 1iN0 17b0 1iN0 17b0 1jd0 12n0 1a10 1cL0 1cN0 1cL0 1cN0 1cL0 1fB0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1qL0 WN0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 WN0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 WN0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0","Asia/Bishkek|LMT FRUT FRUT FRUST FRUST KGT KGST KGT|-4W.o -50 -60 -70 -60 -50 -60 -60|01232323232323232323232456565656565656565656565656567|-1Pc4W.o eUnW.o 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 11c0 1tX0 17b0 1ip0 17b0 1ip0 17b0 1ip0 17b0 1ip0 19X0 1cPu 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 T8u","Asia/Brunei|LMT BNT BNT|-7D.E -7u -80|012|-1KITD.E gDc9.E","Asia/Calcutta|HMT BURT IST IST|-5R.k -6u -5u -6u|01232|-18LFR.k 1unn.k HB0 7zX0","Asia/Chita|LMT YAKT YAKT YAKST YAKST YAKT IRKT|-7x.Q -80 -90 -a0 -90 -a0 -80|012323232323232323232324123232323232323232323232323232323232323256|-21Q7x.Q pAnx.Q 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 8Hz0","Asia/Choibalsan|LMT ULAT ULAT CHOST CHOT CHOT CHOST|-7C -70 -80 -a0 -90 -80 -90|0123434343434343434343434343434343434343434343456565656565656565656565656565656565656565656565|-2APHC 2UkoC cKn0 1da0 1dd0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1fB0 1cL0 1cN0 1cL0 1cN0 1cL0 6hD0 11z0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 3Db0 h1f0 1cJ0 1cP0 1cJ0 1cP0 1fx0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1fx0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1fx0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1fx0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0","Asia/Chongqing|CST CDT|-80 -90|01010101010101010|-1c1I0 LX0 16p0 1jz0 1Myp0 Rb0 1o10 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 1o10 11z0","Asia/Colombo|MMT IST IHST IST LKT LKT|-5j.w -5u -60 -6u -6u -60|01231451|-2zOtj.w 1rFbN.w 1zzu 7Apu 23dz0 11zu n3cu","Asia/Dacca|HMT BURT IST DACT BDT BDST|-5R.k -6u -5u -60 -60 -70|01213454|-18LFR.k 1unn.k HB0 m6n0 LqMu 1x6n0 1i00","Asia/Damascus|LMT EET EEST|-2p.c -20 -30|01212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-21Jep.c Hep.c 17b0 1ip0 17b0 1ip0 17b0 1ip0 19X0 1xRB0 11X0 1oN0 10L0 1pB0 11b0 1oN0 10L0 1mp0 13X0 1oN0 11b0 1pd0 11b0 1oN0 11b0 1oN0 11b0 1oN0 11b0 1pd0 11b0 1oN0 11b0 1oN0 11b0 1oN0 11b0 1pd0 11b0 1oN0 Nb0 1AN0 Nb0 bcp0 19X0 1gp0 19X0 3ld0 1xX0 Vd0 1Bz0 Sp0 1vX0 10p0 1dz0 1cN0 1cL0 1db0 1db0 1g10 1an0 1ap0 1db0 1fd0 1db0 1cN0 1db0 1dd0 1db0 1cp0 1dz0 1c10 1dX0 1cN0 1db0 1dd0 1db0 1cN0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1db0 1cN0 1db0 1cN0 19z0 1fB0 1qL0 11B0 1on0 Wp0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 1qL0 WN0 1qL0","Asia/Dili|LMT TLT JST TLT WITA|-8m.k -80 -90 -90 -80|012343|-2le8m.k 1dnXm.k 8HA0 1ew00 Xld0","Asia/Dubai|LMT GST|-3F.c -40|01|-21JfF.c","Asia/Dushanbe|LMT DUST DUST DUSST DUSST TJT|-4z.c -50 -60 -70 -60 -50|0123232323232323232323245|-1Pc4z.c eUnz.c 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 14N0","Asia/Gaza|EET EET EEST IST IDT|-20 -30 -30 -20 -30|010101010102020202020202020202023434343434343434343434343430202020202020202020202020202020202020202020202020202020202020202020202020202020202020|-1c2q0 5Rb0 10r0 1px0 10N0 1pz0 16p0 1jB0 16p0 1jx0 pBd0 Vz0 1oN0 11b0 1oO0 10N0 1pz0 10N0 1pb0 10N0 1pb0 10N0 1pb0 10N0 1pz0 10N0 1pb0 10N0 1pb0 11d0 1oL0 dW0 hfB0 Db0 1fB0 Rb0 npB0 11z0 1C10 IL0 1s10 10n0 1o10 WL0 1zd0 On0 1ld0 11z0 1o10 14n0 1o10 14n0 1nd0 12n0 1nd0 Xz0 1q10 12n0 M10 C00 17c0 1io0 17c0 1io0 17c0 1o00 1cL0 1fB0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 17c0 1io0 18N0 1bz0 19z0 1gp0 1610 1iL0 11z0 1o10 14o0 1lA1 SKX 1xd1 MKX 1AN0 1a00 1fA0 1cL0 1cN0 1nX0 1210 1nz0 1210 1nz0 14N0 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 14N0 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 14N0 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 14N0 1nz0 1210 1nz0 1210 1nz0 1210 1nz0","Asia/Hebron|EET EET EEST IST IDT|-20 -30 -30 -20 -30|01010101010202020202020202020202343434343434343434343434343020202020202020202020202020202020202020202020202020202020202020202020202020202020202020|-1c2q0 5Rb0 10r0 1px0 10N0 1pz0 16p0 1jB0 16p0 1jx0 pBd0 Vz0 1oN0 11b0 1oO0 10N0 1pz0 10N0 1pb0 10N0 1pb0 10N0 1pb0 10N0 1pz0 10N0 1pb0 10N0 1pb0 11d0 1oL0 dW0 hfB0 Db0 1fB0 Rb0 npB0 11z0 1C10 IL0 1s10 10n0 1o10 WL0 1zd0 On0 1ld0 11z0 1o10 14n0 1o10 14n0 1nd0 12n0 1nd0 Xz0 1q10 12n0 M10 C00 17c0 1io0 17c0 1io0 17c0 1o00 1cL0 1fB0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 17c0 1io0 18N0 1bz0 19z0 1gp0 1610 1iL0 12L0 1mN0 14o0 1lc0 Tb0 1xd1 MKX bB0 cn0 1cN0 1a00 1fA0 1cL0 1cN0 1nX0 1210 1nz0 1210 1nz0 14N0 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 14N0 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 14N0 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 1210 1nz0 14N0 1nz0 1210 1nz0 1210 1nz0 1210 1nz0","Asia/Ho_Chi_Minh|LMT PLMT ICT IDT JST|-76.E -76.u -70 -80 -90|0123423232|-2yC76.E bK00.a 1h7b6.u 5lz0 18o0 3Oq0 k5b0 aW00 BAM0","Asia/Hong_Kong|LMT HKT HKST JST|-7A.G -80 -90 -90|0121312121212121212121212121212121212121212121212121212121212121212121|-2CFHA.G 1sEP6.G 1cL0 ylu 93X0 1qQu 1tX0 Rd0 1In0 NB0 1cL0 11B0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1kL0 14N0 1nX0 U10 1tz0 U10 1wn0 Rd0 1wn0 U10 1tz0 U10 1tz0 U10 1tz0 U10 1wn0 Rd0 1wn0 Rd0 1wn0 U10 1tz0 U10 1tz0 17d0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 s10 1Vz0 1cN0 1cL0 1cN0 1cL0 6fd0 14n0","Asia/Hovd|LMT HOVT HOVT HOVST|-66.A -60 -70 -80|012323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232|-2APG6.A 2Uko6.A cKn0 1db0 1dd0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1fB0 1cL0 1cN0 1cL0 1cN0 1cL0 6hD0 11z0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 kEp0 1cJ0 1cP0 1cJ0 1cP0 1fx0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1fx0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1fx0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1fx0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0","Asia/Irkutsk|IMT IRKT IRKT IRKST IRKST IRKT|-6V.5 -70 -80 -90 -80 -90|012323232323232323232324123232323232323232323232323232323232323252|-21zGV.5 pjXV.5 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 8Hz0","Asia/Istanbul|IMT EET EEST TRST TRT|-1U.U -20 -30 -40 -30|012121212121212121212121212121212121212121212121212121234343434342121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2ogNU.U dzzU.U 11b0 8tB0 1on0 1410 1db0 19B0 1in0 3Rd0 Un0 1oN0 11b0 zSp0 CL0 mN0 1Vz0 1gN0 1pz0 5Rd0 1fz0 1yp0 ML0 1kp0 17b0 1ip0 17b0 1fB0 19X0 1jB0 18L0 1ip0 17z0 qdd0 xX0 3S10 Tz0 dA10 11z0 1o10 11z0 1qN0 11z0 1ze0 11B0 WM0 1qO0 WI0 1nX0 1rB0 10L0 11B0 1in0 17d0 1in0 2pX0 19E0 1fU0 16Q0 1iI0 16Q0 1iI0 1Vd0 pb0 3Kp0 14o0 1df0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cL0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WO0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 Xc0 1qo0 WM0 1qM0 11A0 1o00 1200 1nA0 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Asia/Jakarta|BMT JAVT WIB JST WIB WIB|-77.c -7k -7u -90 -80 -70|01232425|-1Q0Tk luM0 mPzO 8vWu 6kpu 4PXu xhcu","Asia/Jayapura|LMT WIT ACST|-9m.M -90 -9u|0121|-1uu9m.M sMMm.M L4nu","Asia/Jerusalem|JMT IST IDT IDDT|-2k.E -20 -30 -40|01212121212132121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-26Bek.E SyMk.E 5Rb0 10r0 1px0 10N0 1pz0 16p0 1jB0 16p0 1jx0 3LB0 Em0 or0 1cn0 1dB0 16n0 10O0 1ja0 1tC0 14o0 1cM0 1a00 11A0 1Na0 An0 1MP0 AJ0 1Kp0 LC0 1oo0 Wl0 EQN0 Db0 1fB0 Rb0 npB0 11z0 1C10 IL0 1s10 10n0 1o10 WL0 1zd0 On0 1ld0 11z0 1o10 14n0 1o10 14n0 1nd0 12n0 1nd0 Xz0 1q10 12n0 1hB0 1dX0 1ep0 1aL0 1eN0 17X0 1nf0 11z0 1tB0 19W0 1e10 17b0 1ep0 1gL0 18N0 1fz0 1eN0 17b0 1gq0 1gn0 19d0 1dz0 1c10 17X0 1hB0 1gn0 19d0 1dz0 1c10 17X0 1kp0 1dz0 1c10 1aL0 1eN0 1oL0 10N0 1oL0 10N0 1oL0 10N0 1rz0 W10 1rz0 W10 1rz0 10N0 1oL0 10N0 1oL0 10N0 1rz0 W10 1rz0 W10 1rz0 10N0 1oL0 10N0 1oL0 10N0 1oL0 10N0 1rz0 W10 1rz0 W10 1rz0 10N0 1oL0 10N0 1oL0 10N0 1rz0 W10 1rz0 W10 1rz0 W10 1rz0 10N0 1oL0 10N0 1oL0","Asia/Kabul|AFT AFT|-40 -4u|01|-10Qs0","Asia/Kamchatka|LMT PETT PETT PETST PETST|-ay.A -b0 -c0 -d0 -c0|01232323232323232323232412323232323232323232323232323232323232412|-1SLKy.A ivXy.A 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qN0 WM0","Asia/Karachi|LMT IST IST KART PKT PKST|-4s.c -5u -6u -50 -50 -60|012134545454|-2xoss.c 1qOKW.c 7zX0 eup0 LqMu 1fy01 1cL0 dK0X 11b0 1610 1jX0","Asia/Kashgar|LMT XJT|-5O.k -60|01|-1GgtO.k","Asia/Kathmandu|LMT IST NPT|-5F.g -5u -5J|012|-21JhF.g 2EGMb.g","Asia/Khandyga|LMT YAKT YAKT YAKST YAKST VLAT VLAST VLAT YAKT|-92.d -80 -90 -a0 -90 -a0 -b0 -b0 -a0|01232323232323232323232412323232323232323232323232565656565656565782|-21Q92.d pAp2.d 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 qK0 yN0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 17V0 7zD0","Asia/Krasnoyarsk|LMT KRAT KRAT KRAST KRAST KRAT|-6b.q -60 -70 -80 -70 -80|012323232323232323232324123232323232323232323232323232323232323252|-21Hib.q prAb.q 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 8Hz0","Asia/Kuala_Lumpur|SMT MALT MALST MALT MALT JST MYT|-6T.p -70 -7k -7k -7u -90 -80|01234546|-2Bg6T.p 17anT.p 7hXE dM00 17bO 8Fyu 1so1u","Asia/Kuching|LMT BORT BORT BORTST JST MYT|-7l.k -7u -80 -8k -90 -80|01232323232323232425|-1KITl.k gDbP.k 6ynu AnE 1O0k AnE 1NAk AnE 1NAk AnE 1NAk AnE 1O0k AnE 1NAk AnE pAk 8Fz0 1so10","Asia/Macao|LMT MOT MOST CST|-7y.k -80 -90 -80|0121212121212121212121212121212121212121213|-2le7y.k 1XO34.k 1wn0 Rd0 1wn0 R9u 1wqu U10 1tz0 TVu 1tz0 17gu 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cJu 1cL0 1cN0 1fz0 1cN0 1cOu 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cJu 1cL0 1cN0 1fz0 1cN0 1cL0 KEp0","Asia/Magadan|LMT MAGT MAGT MAGST MAGST MAGT|-a3.c -a0 -b0 -c0 -b0 -c0|012323232323232323232324123232323232323232323232323232323232323251|-1Pca3.c eUo3.c 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 8Hz0","Asia/Makassar|LMT MMT WITA JST|-7V.A -7V.A -80 -90|01232|-21JjV.A vfc0 myLV.A 8ML0","Asia/Manila|PHT PHST JST|-80 -90 -90|010201010|-1kJI0 AL0 cK10 65X0 mXB0 vX0 VK10 1db0","Asia/Nicosia|LMT EET EEST|-2d.s -20 -30|01212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-1Vc2d.s 2a3cd.s 1cL0 1qp0 Xz0 19B0 19X0 1fB0 1db0 1cp0 1cL0 1fB0 19X0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1fB0 1cL0 1cN0 1cL0 1cN0 1o30 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Asia/Novokuznetsk|LMT KRAT KRAT KRAST KRAST NOVST NOVT NOVT|-5M.M -60 -70 -80 -70 -70 -60 -70|012323232323232323232324123232323232323232323232323232323232325672|-1PctM.M eULM.M 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qN0 WM0 8Hz0","Asia/Novosibirsk|LMT NOVT NOVT NOVST NOVST|-5v.E -60 -70 -80 -70|0123232323232323232323241232341414141414141414141414141414141414121|-21Qnv.E pAFv.E 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 ml0 Os0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 8Hz0","Asia/Omsk|LMT OMST OMST OMSST OMSST OMST|-4R.u -50 -60 -70 -60 -70|012323232323232323232324123232323232323232323232323232323232323252|-224sR.u pMLR.u 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 8Hz0","Asia/Oral|LMT URAT URAT URAST URAT URAST ORAT ORAST ORAT|-3p.o -40 -50 -60 -60 -50 -40 -50 -50|012343232323232323251516767676767676767676767676768|-1Pc3p.o eUnp.o 23CL0 1db0 1cM0 1dc0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cN0 1cM0 1fA0 2UK0 Fz0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 RW0","Asia/Pontianak|LMT PMT WIB JST WIB WITA WIB|-7h.k -7h.k -7u -90 -80 -80 -70|012324256|-2ua7h.k XE00 munL.k 8Rau 6kpu 4PXu xhcu Wqnu","Asia/Pyongyang|LMT KST JCST JST KST|-8n -8u -90 -90 -90|01234|-2um8n 97XR 12FXu jdA0","Asia/Qyzylorda|LMT KIZT KIZT KIZST KIZT QYZT QYZT QYZST|-4l.Q -40 -50 -60 -60 -50 -60 -70|012343232323232323232325676767676767676767676767676|-1Pc4l.Q eUol.Q 23CL0 1db0 1cM0 1dc0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 2UK0 dC0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0","Asia/Rangoon|RMT BURT JST MMT|-6o.E -6u -90 -6u|0123|-21Jio.E SmnS.E 7j9u","Asia/Sakhalin|LMT JCST JST SAKT SAKST SAKST SAKT|-9u.M -90 -90 -b0 -c0 -b0 -a0|0123434343434343434343435634343434343565656565656565656565656565636|-2AGVu.M 1iaMu.M je00 1qFa0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o10 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 8Hz0","Asia/Samarkand|LMT SAMT SAMT SAMST TAST UZST UZT|-4r.R -40 -50 -60 -60 -60 -50|01234323232323232323232356|-1Pc4r.R eUor.R 23CL0 1db0 1cM0 1dc0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 11x0 bf0","Asia/Seoul|LMT KST JCST JST KST KDT KDT|-8r.Q -8u -90 -90 -90 -9u -a0|01234151515151515146464|-2um8r.Q 97XV.Q 12FXu jjA0 kKo0 2I0u OL0 1FB0 Rb0 1qN0 TX0 1tB0 TX0 1tB0 TX0 1tB0 TX0 2ap0 12FBu 11A0 1o00 11A0","Asia/Singapore|SMT MALT MALST MALT MALT JST SGT SGT|-6T.p -70 -7k -7k -7u -90 -7u -80|012345467|-2Bg6T.p 17anT.p 7hXE dM00 17bO 8Fyu Mspu DTA0","Asia/Srednekolymsk|LMT MAGT MAGT MAGST MAGST MAGT SRET|-ae.Q -a0 -b0 -c0 -b0 -c0 -b0|012323232323232323232324123232323232323232323232323232323232323256|-1Pcae.Q eUoe.Q 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 8Hz0","Asia/Taipei|JWST JST CST CDT|-80 -90 -80 -90|01232323232323232323232323232323232323232|-1iw80 joM0 1yo0 Tz0 1ip0 1jX0 1cN0 11b0 1oN0 11b0 1oN0 11b0 1oN0 11b0 10N0 1BX0 10p0 1pz0 10p0 1pz0 10p0 1db0 1dd0 1db0 1cN0 1db0 1cN0 1db0 1cN0 1db0 1BB0 ML0 1Bd0 ML0 uq10 1db0 1cN0 1db0 97B0 AL0","Asia/Tashkent|LMT TAST TAST TASST TASST UZST UZT|-4B.b -50 -60 -70 -60 -60 -50|01232323232323232323232456|-1Pc4B.b eUnB.b 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 11y0 bf0","Asia/Tbilisi|TBMT TBIT TBIT TBIST TBIST GEST GET GET GEST|-2X.b -30 -40 -50 -40 -40 -30 -40 -50|0123232323232323232323245656565787878787878787878567|-1Pc2X.b 1jUnX.b WCL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 3y0 19f0 1cK0 1cL0 1cN0 1cL0 1cN0 1cL0 1cM0 1cL0 1fB0 3Nz0 11B0 1nX0 11B0 1qL0 WN0 1qL0 WN0 1qL0 11B0 1nX0 11B0 1nX0 11B0 An0 Os0 WM0","Asia/Tehran|LMT TMT IRST IRST IRDT IRDT|-3p.I -3p.I -3u -40 -50 -4u|01234325252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252|-2btDp.I 1d3c0 1huLT.I TXu 1pz0 sN0 vAu 1cL0 1dB0 1en0 pNB0 UL0 1cN0 1dz0 1cp0 1dz0 1cp0 1dz0 1cp0 1dz0 1cp0 1dz0 1cN0 1dz0 1cp0 1dz0 1cp0 1dz0 1cp0 1dz0 1cN0 1dz0 1cp0 1dz0 1cp0 1dz0 1cp0 1dz0 1cN0 1dz0 64p0 1dz0 1cN0 1dz0 1cp0 1dz0 1cp0 1dz0 1cp0 1dz0 1cN0 1dz0 1cp0 1dz0 1cp0 1dz0 1cp0 1dz0 1cN0 1dz0 1cp0 1dz0 1cp0 1dz0 1cp0 1dz0 1cN0 1dz0 1cp0 1dz0 1cp0 1dz0 1cp0 1dz0 1cN0 1dz0 1cp0 1dz0 1cp0 1dz0 1cp0 1dz0 1cp0 1dz0 1cN0 1dz0 1cp0 1dz0 1cp0 1dz0 1cp0 1dz0 1cN0 1dz0 1cp0 1dz0 1cp0 1dz0 1cp0 1dz0","Asia/Thimbu|LMT IST BTT|-5W.A -5u -60|012|-Su5W.A 1BGMs.A","Asia/Tokyo|JCST JST JDT|-90 -90 -a0|0121212121|-1iw90 pKq0 QL0 1lB0 13X0 1zB0 NX0 1zB0 NX0","Asia/Ulaanbaatar|LMT ULAT ULAT ULAST|-77.w -70 -80 -90|012323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232|-2APH7.w 2Uko7.w cKn0 1db0 1dd0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1fB0 1cL0 1cN0 1cL0 1cN0 1cL0 6hD0 11z0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 kEp0 1cJ0 1cP0 1cJ0 1cP0 1fx0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1fx0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1fx0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1fx0 1cP0 1cJ0 1cP0 1cJ0 1cP0 1cJ0","Asia/Ust-Nera|LMT YAKT YAKT MAGST MAGT MAGST MAGT MAGT VLAT VLAT|-9w.S -80 -90 -c0 -b0 -b0 -a0 -c0 -b0 -a0|0123434343434343434343456434343434343434343434343434343434343434789|-21Q9w.S pApw.S 23CL0 1d90 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 17V0 7zD0","Asia/Vladivostok|LMT VLAT VLAT VLAST VLAST VLAT|-8L.v -90 -a0 -b0 -a0 -b0|012323232323232323232324123232323232323232323232323232323232323252|-1SJIL.v itXL.v 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 8Hz0","Asia/Yakutsk|LMT YAKT YAKT YAKST YAKST YAKT|-8C.W -80 -90 -a0 -90 -a0|012323232323232323232324123232323232323232323232323232323232323252|-21Q8C.W pAoC.W 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 8Hz0","Asia/Yekaterinburg|LMT PMT SVET SVET SVEST SVEST YEKT YEKST YEKT|-42.x -3J.5 -40 -50 -60 -50 -50 -60 -60|0123434343434343434343435267676767676767676767676767676767676767686|-2ag42.x 7mQh.s qBvJ.5 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 8Hz0","Asia/Yerevan|LMT YERT YERT YERST YERST AMST AMT AMT AMST|-2W -30 -40 -50 -40 -40 -30 -40 -50|0123232323232323232323245656565657878787878787878787878787878787|-1Pc2W 1jUnW WCL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1am0 2r0 1cJ0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 3Fb0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0","Atlantic/Azores|HMT AZOT AZOST AZOMT AZOT AZOST WET|1S.w 20 10 0 10 0 0|01212121212121212121212121212121212121212121232123212321232121212121212121212121212121212121212121454545454545454545454545454545456545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454|-2ldW5.s aPX5.s Sp0 LX0 1vc0 Tc0 1uM0 SM0 1vc0 Tc0 1vc0 SM0 1vc0 6600 1co0 3E00 17c0 1fA0 1a00 1io0 1a00 1io0 17c0 3I00 17c0 1cM0 1cM0 3Fc0 1cM0 1a00 1fA0 1io0 17c0 1cM0 1cM0 1a00 1fA0 1io0 1qM0 Dc0 1tA0 1cM0 1dc0 1400 gL0 IM0 s10 U00 dX0 Rc0 pd0 Rc0 gL0 Oo0 pd0 Rc0 gL0 Oo0 pd0 14o0 1cM0 1cP0 1cM0 1cM0 1cM0 1cM0 1cM0 3Co0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 qIl0 1cM0 1fA0 1cM0 1cM0 1cN0 1cL0 1cN0 1cM0 1cM0 1cM0 1cM0 1cN0 1cL0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cL0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Atlantic/Bermuda|LMT AST ADT|4j.i 40 30|0121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-1BnRE.G 1LTbE.G 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","Atlantic/Canary|LMT CANT WET WEST|11.A 10 0 -10|01232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232|-1UtaW.o XPAW.o 1lAK0 1a10 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Atlantic/Cape_Verde|LMT CVT CVST CVT|1y.4 20 10 10|01213|-2xomp.U 1qOMp.U 7zX0 1djf0","Atlantic/Faeroe|LMT WET WEST|r.4 0 -10|01212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2uSnw.U 2Wgow.U 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Atlantic/Madeira|FMT MADT MADST MADMT WET WEST|17.A 10 0 -10 0 -10|01212121212121212121212121212121212121212121232123212321232121212121212121212121212121212121212121454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454|-2ldWQ.o aPWQ.o Sp0 LX0 1vc0 Tc0 1uM0 SM0 1vc0 Tc0 1vc0 SM0 1vc0 6600 1co0 3E00 17c0 1fA0 1a00 1io0 1a00 1io0 17c0 3I00 17c0 1cM0 1cM0 3Fc0 1cM0 1a00 1fA0 1io0 17c0 1cM0 1cM0 1a00 1fA0 1io0 1qM0 Dc0 1tA0 1cM0 1dc0 1400 gL0 IM0 s10 U00 dX0 Rc0 pd0 Rc0 gL0 Oo0 pd0 Rc0 gL0 Oo0 pd0 14o0 1cM0 1cP0 1cM0 1cM0 1cM0 1cM0 1cM0 3Co0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 qIl0 1cM0 1fA0 1cM0 1cM0 1cN0 1cL0 1cN0 1cM0 1cM0 1cM0 1cM0 1cN0 1cL0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Atlantic/Reykjavik|LMT IST ISST GMT|1s 10 0 0|012121212121212121212121212121212121212121212121212121212121212121213|-2uWmw mfaw 1Bd0 ML0 1LB0 Cn0 1LB0 3fX0 C10 HrX0 1cO0 LB0 1EL0 LA0 1C00 Oo0 1wo0 Rc0 1wo0 Rc0 1wo0 Rc0 1zc0 Oo0 1zc0 14o0 1lc0 14o0 1lc0 14o0 1o00 11A0 1lc0 14o0 1o00 14o0 1lc0 14o0 1lc0 14o0 1lc0 14o0 1lc0 14o0 1o00 14o0 1lc0 14o0 1lc0 14o0 1lc0 14o0 1lc0 14o0 1lc0 14o0 1o00 14o0 1lc0 14o0 1lc0 14o0 1lc0 14o0 1lc0 14o0 1o00 14o0","Atlantic/South_Georgia|GST|20|0|","Atlantic/Stanley|SMT FKT FKST FKT FKST|3P.o 40 30 30 20|0121212121212134343212121212121212121212121212121212121212121212121212|-2kJw8.A 12bA8.A 19X0 1fB0 19X0 1ip0 19X0 1fB0 19X0 1fB0 19X0 1fB0 Cn0 1Cc10 WL0 1qL0 U10 1tz0 U10 1qM0 WN0 1qL0 WN0 1qL0 WN0 1qL0 WN0 1tz0 U10 1tz0 WN0 1qL0 WN0 1qL0 WN0 1qL0 WN0 1qL0 WN0 1tz0 WN0 1qL0 WN0 1qL0 WN0 1qL0 WN0 1qL0 WN0 1qN0 U10 1wn0 Rd0 1wn0 U10 1tz0 U10 1tz0 U10 1tz0 U10 1tz0 U10 1wn0 U10 1tz0 U10 1tz0 U10","Australia/ACT|AEST AEDT|-a0 -b0|0101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101|-293lX xcX 10jd0 yL0 1cN0 1cL0 1fB0 19X0 17c10 LA0 1C00 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 14o0 1o00 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 U00 1qM0 WM0 1tA0 WM0 1tA0 U00 1tA0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 11A0 1o00 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 11A0 1o00 WM0 1qM0 14o0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0","Australia/Adelaide|ACST ACDT|-9u -au|0101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101|-293lt xcX 10jd0 yL0 1cN0 1cL0 1fB0 19X0 17c10 LA0 1C00 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 U00 1qM0 WM0 1tA0 WM0 1tA0 U00 1tA0 U00 1tA0 Oo0 1zc0 WM0 1qM0 Rc0 1zc0 U00 1tA0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 11A0 1o00 WM0 1qM0 14o0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0","Australia/Brisbane|AEST AEDT|-a0 -b0|01010101010101010|-293lX xcX 10jd0 yL0 1cN0 1cL0 1fB0 19X0 17c10 LA0 H1A0 Oo0 1zc0 Oo0 1zc0 Oo0","Australia/Broken_Hill|ACST ACDT|-9u -au|0101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101|-293lt xcX 10jd0 yL0 1cN0 1cL0 1fB0 19X0 17c10 LA0 1C00 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 14o0 1o00 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 U00 1qM0 WM0 1tA0 WM0 1tA0 U00 1tA0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 11A0 1o00 WM0 1qM0 14o0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0","Australia/Currie|AEST AEDT|-a0 -b0|0101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101|-29E80 19X0 10jd0 yL0 1cN0 1cL0 1fB0 19X0 17c10 LA0 1C00 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 11A0 1qM0 WM0 1qM0 Oo0 1zc0 Oo0 1zc0 Oo0 1wo0 WM0 1tA0 WM0 1tA0 U00 1tA0 U00 1tA0 11A0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 11A0 1o00 1io0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1cM0 1a00 1io0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0","Australia/Darwin|ACST ACDT|-9u -au|010101010|-293lt xcX 10jd0 yL0 1cN0 1cL0 1fB0 19X0","Australia/Eucla|ACWST ACWDT|-8J -9J|0101010101010101010|-293kI xcX 10jd0 yL0 1cN0 1cL0 1gSp0 Oo0 l5A0 Oo0 iJA0 G00 zU00 IM0 1qM0 11A0 1o00 11A0","Australia/Hobart|AEST AEDT|-a0 -b0|010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101|-29E80 19X0 10jd0 yL0 1cN0 1cL0 1fB0 19X0 VfB0 1cM0 1o00 Rc0 1wo0 Rc0 1wo0 U00 1wo0 LA0 1C00 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 11A0 1qM0 WM0 1qM0 Oo0 1zc0 Oo0 1zc0 Oo0 1wo0 WM0 1tA0 WM0 1tA0 U00 1tA0 U00 1tA0 11A0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 11A0 1o00 1io0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1cM0 1a00 1io0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0","Australia/LHI|AEST LHST LHDT LHDT|-a0 -au -bu -b0|0121212121313131313131313131313131313131313131313131313131313131313131313131313131313131313131313131313131313131313|raC0 1zdu Rb0 1zd0 On0 1zd0 On0 1zd0 On0 1zd0 TXu 1qMu WLu 1tAu WLu 1tAu TXu 1tAu Onu 1zcu Onu 1zcu Onu 1zcu Rbu 1zcu Onu 1zcu Onu 1zcu 11zu 1o0u 11zu 1o0u 11zu 1o0u 11zu 1qMu WLu 11Au 1nXu 1qMu 11zu 1o0u 11zu 1o0u 11zu 1qMu WLu 1qMu 11zu 1o0u WLu 1qMu 14nu 1cMu 1cLu 1cMu 1cLu 1cMu 1cLu 1cMu 1cLu 1fAu 1cLu 1cMu 1cLu 1cMu 1cLu 1cMu 1cLu 1cMu 1cLu 1cMu 1cLu 1fAu 1cLu 1cMu 1cLu 1cMu 1cLu 1cMu 1cLu 1cMu 1cLu 1cMu 1fzu 1cMu 1cLu 1cMu 1cLu 1cMu 1cLu 1cMu 1cLu 1cMu 1cLu 1fAu 1cLu 1cMu 1cLu 1cMu 1cLu 1cMu 1cLu 1cMu 1cLu 1cMu 1cLu 1fAu 1cLu 1cMu 1cLu 1cMu","Australia/Lindeman|AEST AEDT|-a0 -b0|010101010101010101010|-293lX xcX 10jd0 yL0 1cN0 1cL0 1fB0 19X0 17c10 LA0 H1A0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0","Australia/Melbourne|AEST AEDT|-a0 -b0|0101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101|-293lX xcX 10jd0 yL0 1cN0 1cL0 1fB0 19X0 17c10 LA0 1C00 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 U00 1qM0 WM0 1qM0 11A0 1tA0 U00 1tA0 U00 1tA0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 11A0 1o00 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 11A0 1o00 WM0 1qM0 14o0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0","Australia/Perth|AWST AWDT|-80 -90|0101010101010101010|-293jX xcX 10jd0 yL0 1cN0 1cL0 1gSp0 Oo0 l5A0 Oo0 iJA0 G00 zU00 IM0 1qM0 11A0 1o00 11A0","CET|CET CEST|-10 -20|01010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-2aFe0 11d0 1iO0 11A0 1o00 11A0 Qrc0 6i00 WM0 1fA0 1cM0 1cM0 1cM0 16M0 1gMM0 1a00 1fA0 1cM0 1cM0 1cM0 1fA0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","CST6CDT|CST CDT CWT CPT|60 50 50 50|010102301010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-261s0 1nX0 11B0 1nX0 SgN0 8x30 iw0 QwN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","Chile/EasterIsland|EMT EAST EASST EAST EASST EAST|7h.s 70 60 60 50 50|012121212121212121212121212123434343434343434343434343434343434343434343434343434343434343434345|-1uSgG.w 1s4IG.w WL0 1zd0 On0 1ip0 11z0 1o10 11z0 1qN0 WL0 1ld0 14n0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 WL0 1qN0 1cL0 1cN0 11z0 1o10 11z0 1qN0 WL0 1fB0 19X0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 17b0 1ip0 11z0 1ip0 1fz0 1fB0 11z0 1qN0 WL0 1qN0 WL0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 17b0 1ip0 11z0 1o10 19X0 1fB0 1nX0 G10 1EL0 Op0 1zb0 Rd0 1wn0 Rd0 1wn0","EET|EET EEST|-20 -30|010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|hDB0 1a00 1fA0 1cM0 1cM0 1cM0 1fA0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","EST|EST|50|0|","EST5EDT|EST EDT EWT EPT|50 40 40 40|010102301010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-261t0 1nX0 11B0 1nX0 SgN0 8x40 iv0 QwN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","Eire|DMT IST GMT BST IST|p.l -y.D 0 -10 -10|01232323232324242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242424242|-2ax9y.D Rc0 1fzy.D 14M0 1fc0 1g00 1co0 1dc0 1co0 1oo0 1400 1dc0 19A0 1io0 1io0 WM0 1o00 14o0 1o00 17c0 1io0 17c0 1fA0 1a00 1lc0 17c0 1io0 17c0 1fA0 1a00 1io0 17c0 1io0 17c0 1fA0 1cM0 1io0 17c0 1fA0 1a00 1io0 17c0 1io0 17c0 1fA0 1a00 1io0 1qM0 Dc0 g5X0 14p0 1wn0 17d0 1io0 11A0 1o00 17c0 1fA0 1a00 1fA0 1cM0 1fA0 1a00 17c0 1fA0 1a00 1io0 17c0 1lc0 17c0 1fA0 1a00 1io0 17c0 1io0 17c0 1fA0 1a00 1a00 1qM0 WM0 1qM0 11A0 1o00 WM0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1tA0 IM0 90o0 U00 1tA0 U00 1tA0 U00 1tA0 U00 1tA0 WM0 1qM0 WM0 1qM0 WM0 1tA0 U00 1tA0 U00 1tA0 11z0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1o00 14o0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Etc/GMT+0|GMT|0|0|","Etc/GMT+1|GMT+1|10|0|","Etc/GMT+10|GMT+10|a0|0|","Etc/GMT+11|GMT+11|b0|0|","Etc/GMT+12|GMT+12|c0|0|","Etc/GMT+2|GMT+2|20|0|","Etc/GMT+3|GMT+3|30|0|","Etc/GMT+4|GMT+4|40|0|","Etc/GMT+5|GMT+5|50|0|","Etc/GMT+6|GMT+6|60|0|","Etc/GMT+7|GMT+7|70|0|","Etc/GMT+8|GMT+8|80|0|","Etc/GMT+9|GMT+9|90|0|","Etc/GMT-1|GMT-1|-10|0|","Etc/GMT-10|GMT-10|-a0|0|","Etc/GMT-11|GMT-11|-b0|0|","Etc/GMT-12|GMT-12|-c0|0|","Etc/GMT-13|GMT-13|-d0|0|","Etc/GMT-14|GMT-14|-e0|0|","Etc/GMT-2|GMT-2|-20|0|","Etc/GMT-3|GMT-3|-30|0|","Etc/GMT-4|GMT-4|-40|0|","Etc/GMT-5|GMT-5|-50|0|","Etc/GMT-6|GMT-6|-60|0|","Etc/GMT-7|GMT-7|-70|0|","Etc/GMT-8|GMT-8|-80|0|","Etc/GMT-9|GMT-9|-90|0|","Etc/UCT|UCT|0|0|","Etc/UTC|UTC|0|0|","Europe/Amsterdam|AMT NST NEST NET CEST CET|-j.w -1j.w -1k -k -20 -10|010101010101010101010101010101010101010101012323234545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545|-2aFcj.w 11b0 1iP0 11A0 1io0 1cM0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1co0 1io0 1yo0 Pc0 1a00 1fA0 1Bc0 Mo0 1tc0 Uo0 1tA0 U00 1uo0 W00 1s00 VA0 1so0 Vc0 1sM0 UM0 1wo0 Rc0 1u00 Wo0 1rA0 W00 1s00 VA0 1sM0 UM0 1w00 fV0 BCX.w 1tA0 U00 1u00 Wo0 1sm0 601k WM0 1fA0 1cM0 1cM0 1cM0 16M0 1gMM0 1a00 1fA0 1cM0 1cM0 1cM0 1fA0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Andorra|WET CET CEST|0 -10 -20|012121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-UBA0 1xIN0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Athens|AMT EET EEST CEST CET|-1y.Q -20 -30 -20 -10|012123434121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2a61x.Q CNbx.Q mn0 kU10 9b0 3Es0 Xa0 1fb0 1dd0 k3X0 Nz0 SCp0 1vc0 SO0 1cM0 1a00 1ao0 1fc0 1a10 1fG0 1cg0 1dX0 1bX0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Belfast|GMT BST BDST|0 -10 -20|0101010101010101010101010101010101010101010101010121212121210101210101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-2axa0 Rc0 1fA0 14M0 1fc0 1g00 1co0 1dc0 1co0 1oo0 1400 1dc0 19A0 1io0 1io0 WM0 1o00 14o0 1o00 17c0 1io0 17c0 1fA0 1a00 1lc0 17c0 1io0 17c0 1fA0 1a00 1io0 17c0 1io0 17c0 1fA0 1cM0 1io0 17c0 1fA0 1a00 1io0 17c0 1io0 17c0 1fA0 1a00 1io0 1qM0 Dc0 2Rz0 Dc0 1zc0 Oo0 1zc0 Rc0 1wo0 17c0 1iM0 FA0 xB0 1fA0 1a00 14o0 bb0 LA0 xB0 Rc0 1wo0 11A0 1o00 17c0 1fA0 1a00 1fA0 1cM0 1fA0 1a00 17c0 1fA0 1a00 1io0 17c0 1lc0 17c0 1fA0 1a00 1io0 17c0 1io0 17c0 1fA0 1a00 1a00 1qM0 WM0 1qM0 11A0 1o00 WM0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1tA0 IM0 90o0 U00 1tA0 U00 1tA0 U00 1tA0 U00 1tA0 WM0 1qM0 WM0 1qM0 WM0 1tA0 U00 1tA0 U00 1tA0 11z0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1o00 14o0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Belgrade|CET CEST|-10 -20|01010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-19RC0 3IP0 WM0 1fA0 1cM0 1cM0 1rc0 Qo0 1vmo0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Berlin|CET CEST CEMT|-10 -20 -30|01010101010101210101210101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-2aFe0 11d0 1iO0 11A0 1o00 11A0 Qrc0 6i00 WM0 1fA0 1cM0 1cM0 1cM0 kL0 Nc0 m10 WM0 1ao0 1cp0 dX0 jz0 Dd0 1io0 17c0 1fA0 1a00 1ehA0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Bratislava|CET CEST|-10 -20|010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-2aFe0 11d0 1iO0 11A0 1o00 11A0 Qrc0 6i00 WM0 1fA0 1cM0 16M0 1lc0 1tA0 17A0 11c0 1io0 17c0 1io0 17c0 1fc0 1ao0 1bNc0 1cM0 1fA0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Brussels|WET CET CEST WEST|0 -10 -20 -10|0121212103030303030303030303030303030303030303030303212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2ehc0 3zX0 11c0 1iO0 11A0 1o00 11A0 my0 Ic0 1qM0 Rc0 1EM0 UM0 1u00 10o0 1io0 1io0 17c0 1a00 1fA0 1cM0 1cM0 1io0 17c0 1fA0 1a00 1io0 1a30 1io0 17c0 1fA0 1a00 1io0 17c0 1cM0 1cM0 1a00 1io0 1cM0 1cM0 1a00 1fA0 1io0 17c0 1cM0 1cM0 1a00 1fA0 1io0 1qM0 Dc0 y00 5Wn0 WM0 1fA0 1cM0 16M0 1iM0 16M0 1C00 Uo0 1eeo0 1a00 1fA0 1cM0 1cM0 1cM0 1fA0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Bucharest|BMT EET EEST|-1I.o -20 -30|0121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-1xApI.o 20LI.o RA0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1Axc0 On0 1fA0 1a10 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cK0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cL0 1cN0 1cL0 1fB0 1nX0 11E0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Budapest|CET CEST|-10 -20|0101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-2aFe0 11d0 1iO0 11A0 1ip0 17b0 1op0 1tb0 Q2m0 3Ne0 WM0 1fA0 1cM0 1cM0 1oJ0 1dc0 1030 1fA0 1cM0 1cM0 1cM0 1cM0 1fA0 1a00 1iM0 1fA0 8Ha0 Rb0 1wN0 Rb0 1BB0 Lz0 1C20 LB0 SNX0 1a10 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Busingen|CET CEST|-10 -20|01010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-19Lc0 11A0 1o00 11A0 1xG10 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Chisinau|CMT BMT EET EEST CEST CET MSK MSD|-1T -1I.o -20 -30 -20 -10 -30 -40|0123232323232323232345454676767676767676767623232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232|-26jdT wGMa.A 20LI.o RA0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 27A0 2en0 39g0 WM0 1fA0 1cM0 V90 1t7z0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1ty0 2bD0 1cM0 1cK0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1fB0 1nX0 11E0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Copenhagen|CET CEST|-10 -20|0101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-2azC0 Tz0 VuO0 60q0 WM0 1fA0 1cM0 1cM0 1cM0 S00 1HA0 Nc0 1C00 Dc0 1Nc0 Ao0 1h5A0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Gibraltar|GMT BST BDST CET CEST|0 -10 -20 -10 -20|010101010101010101010101010101010101010101010101012121212121010121010101010101010101034343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343|-2axa0 Rc0 1fA0 14M0 1fc0 1g00 1co0 1dc0 1co0 1oo0 1400 1dc0 19A0 1io0 1io0 WM0 1o00 14o0 1o00 17c0 1io0 17c0 1fA0 1a00 1lc0 17c0 1io0 17c0 1fA0 1a00 1io0 17c0 1io0 17c0 1fA0 1cM0 1io0 17c0 1fA0 1a00 1io0 17c0 1io0 17c0 1fA0 1a00 1io0 1qM0 Dc0 2Rz0 Dc0 1zc0 Oo0 1zc0 Rc0 1wo0 17c0 1iM0 FA0 xB0 1fA0 1a00 14o0 bb0 LA0 xB0 Rc0 1wo0 11A0 1o00 17c0 1fA0 1a00 1fA0 1cM0 1fA0 1a00 17c0 1fA0 1a00 1io0 17c0 1lc0 17c0 1fA0 10Jz0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Helsinki|HMT EET EEST|-1D.N -20 -30|0121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-1WuND.N OULD.N 1dA0 1xGq0 1cM0 1cM0 1cM0 1cN0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Kaliningrad|CET CEST CET CEST MSK MSD EEST EET FET|-10 -20 -20 -30 -30 -40 -30 -20 -30|0101010101010232454545454545454545454676767676767676767676767676767676767676787|-2aFe0 11d0 1iO0 11A0 1o00 11A0 Qrc0 6i00 WM0 1fA0 1cM0 1cM0 Am0 Lb0 1en0 op0 1pNz0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 1cJ0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 8Hz0","Europe/Kiev|KMT EET MSK CEST CET MSD EEST|-22.4 -20 -30 -20 -10 -40 -30|0123434252525252525252525256161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161|-1Pc22.4 eUo2.4 rnz0 2Hg0 WM0 1fA0 da0 1v4m0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 Db0 3220 1cK0 1cL0 1cN0 1cL0 1cN0 1cL0 1cQ0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Lisbon|LMT WET WEST WEMT CET CEST|A.J 0 -10 -20 -10 -20|012121212121212121212121212121212121212121212321232123212321212121212121212121212121212121212121214121212121212121212121212121212124545454212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2ldXn.f aPWn.f Sp0 LX0 1vc0 Tc0 1uM0 SM0 1vc0 Tc0 1vc0 SM0 1vc0 6600 1co0 3E00 17c0 1fA0 1a00 1io0 1a00 1io0 17c0 3I00 17c0 1cM0 1cM0 3Fc0 1cM0 1a00 1fA0 1io0 17c0 1cM0 1cM0 1a00 1fA0 1io0 1qM0 Dc0 1tA0 1cM0 1dc0 1400 gL0 IM0 s10 U00 dX0 Rc0 pd0 Rc0 gL0 Oo0 pd0 Rc0 gL0 Oo0 pd0 14o0 1cM0 1cP0 1cM0 1cM0 1cM0 1cM0 1cM0 3Co0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 pvy0 1cM0 1cM0 1fA0 1cM0 1cM0 1cN0 1cL0 1cN0 1cM0 1cM0 1cM0 1cM0 1cN0 1cL0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Luxembourg|LMT CET CEST WET WEST WEST WET|-o.A -10 -20 0 -10 -20 -10|0121212134343434343434343434343434343434343434343434565651212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2DG0o.A t6mo.A TB0 1nX0 Up0 1o20 11A0 rW0 CM0 1qP0 R90 1EO0 UK0 1u20 10m0 1ip0 1in0 17e0 19W0 1fB0 1db0 1cp0 1in0 17d0 1fz0 1a10 1in0 1a10 1in0 17f0 1fA0 1a00 1io0 17c0 1cM0 1cM0 1a00 1io0 1cM0 1cM0 1a00 1fA0 1io0 17c0 1cM0 1cM0 1a00 1fA0 1io0 1qM0 Dc0 vA0 60L0 WM0 1fA0 1cM0 17c0 1io0 16M0 1C00 Uo0 1eeo0 1a00 1fA0 1cM0 1cM0 1cM0 1fA0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Madrid|WET WEST WEMT CET CEST|0 -10 -20 -10 -20|01010101010101010101010121212121234343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343|-28dd0 11A0 1go0 19A0 1co0 1dA0 b1A0 18o0 3I00 17c0 1fA0 1a00 1io0 1a00 1io0 17c0 iyo0 Rc0 18o0 1hc0 1io0 1a00 14o0 5aL0 MM0 1vc0 17A0 1i00 1bc0 1eo0 17d0 1in0 17A0 6hA0 10N0 XIL0 1a10 1in0 17d0 19X0 1cN0 1fz0 1a10 1fX0 1cp0 1cO0 1cM0 1fA0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Malta|CET CEST|-10 -20|0101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-2as10 M00 1cM0 1cM0 14o0 1o00 WM0 1qM0 17c0 1cM0 M3A0 5M20 WM0 1fA0 1cM0 1cM0 1cM0 16m0 1de0 1lc0 14m0 1lc0 WO0 1qM0 GTW0 On0 1C10 Lz0 1C10 Lz0 1EN0 Lz0 1C10 Lz0 1zd0 Oo0 1C00 On0 1cp0 1cM0 1lA0 Xc0 1qq0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1iN0 19z0 1fB0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Minsk|MMT EET MSK CEST CET MSD EEST FET|-1O -20 -30 -20 -10 -40 -30 -30|012343432525252525252525252616161616161616161616161616161616161616172|-1Pc1O eUnO qNX0 3gQ0 WM0 1fA0 1cM0 Al0 1tsn0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 3Fc0 1cN0 1cK0 1cM0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 8Hy0","Europe/Monaco|PMT WET WEST WEMT CET CEST|-9.l 0 -10 -20 -10 -20|01212121212121212121212121212121212121212121212121232323232345454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454|-2nco9.l cNb9.l HA0 19A0 1iM0 11c0 1oo0 Wo0 1rc0 QM0 1EM0 UM0 1u00 10o0 1io0 1wo0 Rc0 1a00 1fA0 1cM0 1cM0 1io0 17c0 1fA0 1a00 1io0 1a00 1io0 17c0 1fA0 1a00 1io0 17c0 1cM0 1cM0 1a00 1io0 1cM0 1cM0 1a00 1fA0 1io0 17c0 1cM0 1cM0 1a00 1fA0 1io0 1qM0 Df0 2RV0 11z0 11B0 1ze0 WM0 1fA0 1cM0 1fa0 1aq0 16M0 1ekn0 1cL0 1fC0 1a00 1fA0 1cM0 1cM0 1cM0 1fA0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Moscow|MMT MMT MST MDST MSD MSK MSM EET EEST MSK|-2u.h -2v.j -3v.j -4v.j -40 -30 -50 -20 -30 -40|012132345464575454545454545454545458754545454545454545454545454545454545454595|-2ag2u.h 2pyW.W 1bA0 11X0 GN0 1Hb0 c20 imv.j 3DA0 dz0 15A0 c10 2q10 iM10 23CL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 IM0 rU0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 8Hz0","Europe/Paris|PMT WET WEST CEST CET WEMT|-9.l 0 -10 -20 -10 -20|0121212121212121212121212121212121212121212121212123434352543434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434|-2nco8.l cNb8.l HA0 19A0 1iM0 11c0 1oo0 Wo0 1rc0 QM0 1EM0 UM0 1u00 10o0 1io0 1wo0 Rc0 1a00 1fA0 1cM0 1cM0 1io0 17c0 1fA0 1a00 1io0 1a00 1io0 17c0 1fA0 1a00 1io0 17c0 1cM0 1cM0 1a00 1io0 1cM0 1cM0 1a00 1fA0 1io0 17c0 1cM0 1cM0 1a00 1fA0 1io0 1qM0 Df0 Ik0 5M30 WM0 1fA0 1cM0 Vx0 hB0 1aq0 16M0 1ekn0 1cL0 1fC0 1a00 1fA0 1cM0 1cM0 1cM0 1fA0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Riga|RMT LST EET MSK CEST CET MSD EEST|-1A.y -2A.y -20 -30 -20 -10 -40 -30|010102345454536363636363636363727272727272727272727272727272727272727272727272727272727272727272727272727272727272727272727272|-25TzA.y 11A0 1iM0 ko0 gWm0 yDXA.y 2bX0 3fE0 WM0 1fA0 1cM0 1cM0 4m0 1sLy0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cN0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 1o00 11A0 1o00 11A0 1qM0 3oo0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Rome|CET CEST|-10 -20|0101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-2as10 M00 1cM0 1cM0 14o0 1o00 WM0 1qM0 17c0 1cM0 M3A0 5M20 WM0 1fA0 1cM0 16K0 1iO0 16m0 1de0 1lc0 14m0 1lc0 WO0 1qM0 GTW0 On0 1C10 Lz0 1C10 Lz0 1EN0 Lz0 1C10 Lz0 1zd0 Oo0 1C00 On0 1C10 Lz0 1zd0 On0 1C10 LA0 1C00 LA0 1zc0 Oo0 1C00 Oo0 1zc0 Oo0 1fC0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Samara|LMT SAMT SAMT KUYT KUYST MSD MSK EEST KUYT SAMST SAMST|-3k.k -30 -40 -40 -50 -40 -30 -30 -30 -50 -40|012343434343434343435656782929292929292929292929292929292929292a12|-22WNk.k qHak.k bcn0 1Qqo0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cN0 1cM0 1fA0 1cM0 1cN0 8o0 14j0 1cL0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qN0 WM0","Europe/Simferopol|SMT EET MSK CEST CET MSD EEST MSK|-2g -20 -30 -20 -10 -40 -30 -40|012343432525252525252525252161616525252616161616161616161616161616161616172|-1Pc2g eUog rEn0 2qs0 WM0 1fA0 1cM0 3V0 1u0L0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1Q00 4eL0 1cL0 1cN0 1cL0 1cN0 dX0 WL0 1cN0 1cL0 1fB0 1o30 11B0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11z0 1nW0","Europe/Sofia|EET CET CEST EEST|-20 -10 -20 -30|01212103030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030|-168L0 WM0 1fA0 1cM0 1cM0 1cN0 1mKH0 1dd0 1fb0 1ap0 1fb0 1a20 1fy0 1a30 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cK0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1fB0 1nX0 11E0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Stockholm|CET CEST|-10 -20|01010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-2azC0 TB0 2yDe0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Tallinn|TMT CET CEST EET MSK MSD EEST|-1D -10 -20 -20 -30 -40 -30|012103421212454545454545454546363636363636363636363636363636363636363636363636363636363636363636363636363636363636363636363|-26oND teD 11A0 1Ta0 4rXl KSLD 2FX0 2Jg0 WM0 1fA0 1cM0 18J0 1sTX0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cN0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o10 11A0 1qM0 5QM0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Tirane|LMT CET CEST|-1j.k -10 -20|01212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2glBj.k 14pcj.k 5LC0 WM0 4M0 1fCK0 10n0 1op0 11z0 1pd0 11z0 1qN0 WL0 1qp0 Xb0 1qp0 Xb0 1qp0 11z0 1lB0 11z0 1qN0 11z0 1iN0 16n0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Uzhgorod|CET CEST MSK MSD EET EEST|-10 -20 -30 -40 -20 -30|010101023232323232323232320454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454|-1cqL0 6i00 WM0 1fA0 1cM0 1ml0 1Cp0 1r3W0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1Q00 1Nf0 2pw0 1cL0 1cN0 1cL0 1cN0 1cL0 1cQ0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Vienna|CET CEST|-10 -20|0101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-2aFe0 11d0 1iO0 11A0 1o00 11A0 3KM0 14o0 LA00 6i00 WM0 1fA0 1cM0 1cM0 1cM0 400 2qM0 1a00 1cM0 1cM0 1io0 17c0 1gHa0 19X0 1cP0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Vilnius|WMT KMT CET EET MSK CEST MSD EEST|-1o -1z.A -10 -20 -30 -20 -40 -30|012324525254646464646464646464647373737373737352537373737373737373737373737373737373737373737373737373737373737373737373|-293do 6ILM.o 1Ooz.A zz0 Mfd0 29W0 3is0 WM0 1fA0 1cM0 LV0 1tgL0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cN0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11B0 1o00 11A0 1qM0 8io0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Volgograd|LMT TSAT STAT STAT VOLT VOLST VOLST VOLT MSD MSK MSK|-2V.E -30 -30 -40 -40 -50 -40 -30 -40 -30 -40|0123454545454545454546767489898989898989898989898989898989898989a9|-21IqV.E cLXV.E cEM0 1gqn0 Lco0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cN0 1cM0 1fA0 1cM0 2pz0 1cJ0 1cQ0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 8Hz0","Europe/Warsaw|WMT CET CEST EET EEST|-1o -10 -20 -20 -30|012121234312121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121|-2ctdo 1LXo 11d0 1iO0 11A0 1o00 11A0 1on0 11A0 6zy0 HWP0 5IM0 WM0 1fA0 1cM0 1dz0 1mL0 1en0 15B0 1aq0 1nA0 11A0 1io0 17c0 1fA0 1a00 iDX0 LA0 1cM0 1cM0 1C00 Oo0 1cM0 1cM0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1C00 LA0 uso0 1a00 1fA0 1cM0 1cM0 1cM0 1fA0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cN0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","Europe/Zaporozhye|CUT EET MSK CEST CET MSD EEST|-2k -20 -30 -20 -10 -40 -30|01234342525252525252525252526161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161|-1Pc2k eUok rdb0 2RE0 WM0 1fA0 8m0 1v9a0 1db0 1cN0 1db0 1cN0 1db0 1dd0 1cO0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cK0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cQ0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","HST|HST|a0|0|","Indian/Chagos|LMT IOT IOT|-4N.E -50 -60|012|-2xosN.E 3AGLN.E","Indian/Christmas|CXT|-70|0|","Indian/Cocos|CCT|-6u|0|","Indian/Kerguelen|zzz TFT|0 -50|01|-MG00","Indian/Mahe|LMT SCT|-3F.M -40|01|-2yO3F.M","Indian/Maldives|MMT MVT|-4S -50|01|-olgS","Indian/Mauritius|LMT MUT MUST|-3O -40 -50|012121|-2xorO 34unO 14L0 12kr0 11z0","Indian/Reunion|LMT RET|-3F.Q -40|01|-2mDDF.Q","Kwajalein|MHT KWAT MHT|-b0 c0 -c0|012|-AX0 W9X0","MET|MET MEST|-10 -20|01010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-2aFe0 11d0 1iO0 11A0 1o00 11A0 Qrc0 6i00 WM0 1fA0 1cM0 1cM0 1cM0 16M0 1gMM0 1a00 1fA0 1cM0 1cM0 1cM0 1fA0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00","MST|MST|70|0|","MST7MDT|MST MDT MWT MPT|70 60 60 60|010102301010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-261r0 1nX0 11B0 1nX0 SgN0 8x20 ix0 QwN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","NZ-CHAT|CHAST CHAST CHADT|-cf -cJ -dJ|012121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212|-WqAf 1adef IM0 1C00 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1qM0 14o0 1lc0 14o0 1lc0 14o0 1lc0 17c0 1io0 17c0 1io0 17c0 1io0 17c0 1lc0 14o0 1lc0 14o0 1lc0 17c0 1io0 17c0 1io0 17c0 1lc0 14o0 1lc0 14o0 1lc0 17c0 1io0 17c0 1io0 17c0 1io0 17c0 1io0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1io0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1fA0 1a00 1fA0 1a00","PST8PDT|PST PDT PWT PPT|80 70 70 70|010102301010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-261q0 1nX0 11B0 1nX0 SgN0 8x10 iy0 QwN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0","Pacific/Apia|LMT WSST SST SDT WSDT WSST|bq.U bu b0 a0 -e0 -d0|01232345454545454545454545454545454545454545454545454545454|-2nDMx.4 1yW03.4 2rRbu 1ff0 1a00 CI0 AQ0 1cM0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1io0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1a00 1fA0 1cM0 1fA0 1a00 1fA0 1a00","Pacific/Bougainville|PGT JST BST|-a0 -90 -b0|0102|-16Wy0 7CN0 2MQp0","Pacific/Chuuk|CHUT|-a0|0|","Pacific/Efate|LMT VUT VUST|-bd.g -b0 -c0|0121212121212121212121|-2l9nd.g 2Szcd.g 1cL0 1oN0 10L0 1fB0 19X0 1fB0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1fB0 Lz0 1Nd0 An0","Pacific/Enderbury|PHOT PHOT PHOT|c0 b0 -d0|012|nIc0 B8n0","Pacific/Fakaofo|TKT TKT|b0 -d0|01|1Gfn0","Pacific/Fiji|LMT FJT FJST|-bT.I -c0 -d0|012121212121212121212121212121212121212121212121212121212121212|-2bUzT.I 3m8NT.I LA0 1EM0 IM0 nJc0 LA0 1o00 Rc0 1wo0 Ao0 1Nc0 Ao0 1Q00 xz0 1SN0 uM0 1SM0 xA0 1SM0 uM0 1SM0 uM0 1SM0 uM0 1SM0 uM0 1SM0 xA0 1SM0 uM0 1SM0 uM0 1SM0 uM0 1SM0 uM0 1SM0 uM0 1SM0 xA0 1SM0 uM0 1SM0 uM0 1SM0 uM0 1SM0 uM0 1SM0 uM0 1VA0 uM0 1SM0 uM0 1SM0 uM0 1SM0 uM0 1SM0 uM0 1SM0","Pacific/Funafuti|TVT|-c0|0|","Pacific/Galapagos|LMT ECT GALT|5W.o 50 60|012|-1yVS1.A 2dTz1.A","Pacific/Gambier|LMT GAMT|8X.M 90|01|-2jof0.c","Pacific/Guadalcanal|LMT SBT|-aD.M -b0|01|-2joyD.M","Pacific/Guam|GST ChST|-a0 -a0|01|1fpq0","Pacific/Honolulu|HST HDT HST|au 9u a0|010102|-1thLu 8x0 lef0 8Pz0 46p0","Pacific/Kiritimati|LINT LINT LINT|aE a0 -e0|012|nIaE B8nk","Pacific/Kosrae|KOST KOST|-b0 -c0|010|-AX0 1bdz0","Pacific/Majuro|MHT MHT|-b0 -c0|01|-AX0","Pacific/Marquesas|LMT MART|9i 9u|01|-2joeG","Pacific/Midway|LMT NST BST SST|bm.M b0 b0 b0|0123|-2nDMB.c 2gVzB.c EyM0","Pacific/Nauru|LMT NRT JST NRT|-b7.E -bu -90 -c0|01213|-1Xdn7.E PvzB.E 5RCu 1ouJu","Pacific/Niue|NUT NUT NUT|bk bu b0|012|-KfME 17y0a","Pacific/Norfolk|NMT NFT|-bc -bu|01|-Kgbc","Pacific/Noumea|LMT NCT NCST|-b5.M -b0 -c0|01212121|-2l9n5.M 2EqM5.M xX0 1PB0 yn0 HeP0 Ao0","Pacific/Palau|PWT|-90|0|","Pacific/Pitcairn|PNT PST|8u 80|01|18Vku","Pacific/Pohnpei|PONT|-b0|0|","Pacific/Port_Moresby|PGT|-a0|0|","Pacific/Rarotonga|CKT CKHST CKT|au 9u a0|012121212121212121212121212|lyWu IL0 1zcu Onu 1zcu Onu 1zcu Rbu 1zcu Onu 1zcu Onu 1zcu Onu 1zcu Onu 1zcu Onu 1zcu Rbu 1zcu Onu 1zcu Onu 1zcu Onu","Pacific/Tahiti|LMT TAHT|9W.g a0|01|-2joe1.I","Pacific/Tarawa|GILT|-c0|0|","Pacific/Tongatapu|TOT TOT TOST|-ck -d0 -e0|01212121|-1aB0k 2n5dk 15A0 1wo0 xz0 1Q10 xz0","Pacific/Wake|WAKT|-c0|0|","Pacific/Wallis|WFT|-c0|0|","WET|WET WEST|0 -10|010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|hDB0 1a00 1fA0 1cM0 1cM0 1cM0 1fA0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00"],
links:["Africa/Abidjan|Africa/Bamako","Africa/Abidjan|Africa/Banjul","Africa/Abidjan|Africa/Conakry","Africa/Abidjan|Africa/Dakar","Africa/Abidjan|Africa/Freetown","Africa/Abidjan|Africa/Lome","Africa/Abidjan|Africa/Nouakchott","Africa/Abidjan|Africa/Ouagadougou","Africa/Abidjan|Africa/Sao_Tome","Africa/Abidjan|Africa/Timbuktu","Africa/Abidjan|Atlantic/St_Helena","Africa/Addis_Ababa|Africa/Asmara","Africa/Addis_Ababa|Africa/Asmera","Africa/Addis_Ababa|Africa/Dar_es_Salaam","Africa/Addis_Ababa|Africa/Djibouti","Africa/Addis_Ababa|Africa/Kampala","Africa/Addis_Ababa|Africa/Mogadishu","Africa/Addis_Ababa|Africa/Nairobi","Africa/Addis_Ababa|Indian/Antananarivo","Africa/Addis_Ababa|Indian/Comoro","Africa/Addis_Ababa|Indian/Mayotte","Africa/Bangui|Africa/Brazzaville","Africa/Bangui|Africa/Douala","Africa/Bangui|Africa/Kinshasa","Africa/Bangui|Africa/Lagos","Africa/Bangui|Africa/Libreville","Africa/Bangui|Africa/Luanda","Africa/Bangui|Africa/Malabo","Africa/Bangui|Africa/Niamey","Africa/Bangui|Africa/Porto-Novo","Africa/Blantyre|Africa/Bujumbura","Africa/Blantyre|Africa/Gaborone","Africa/Blantyre|Africa/Harare","Africa/Blantyre|Africa/Kigali","Africa/Blantyre|Africa/Lubumbashi","Africa/Blantyre|Africa/Lusaka","Africa/Blantyre|Africa/Maputo","Africa/Cairo|Egypt","Africa/Johannesburg|Africa/Maseru","Africa/Johannesburg|Africa/Mbabane","Africa/Juba|Africa/Khartoum","Africa/Tripoli|Libya","America/Adak|America/Atka","America/Adak|US/Aleutian","America/Anchorage|US/Alaska","America/Anguilla|America/Antigua","America/Anguilla|America/Dominica","America/Anguilla|America/Grenada","America/Anguilla|America/Guadeloupe","America/Anguilla|America/Marigot","America/Anguilla|America/Montserrat","America/Anguilla|America/Port_of_Spain","America/Anguilla|America/St_Barthelemy","America/Anguilla|America/St_Kitts","America/Anguilla|America/St_Lucia","America/Anguilla|America/St_Thomas","America/Anguilla|America/St_Vincent","America/Anguilla|America/Tortola","America/Anguilla|America/Virgin","America/Argentina/Buenos_Aires|America/Buenos_Aires","America/Argentina/Catamarca|America/Argentina/ComodRivadavia","America/Argentina/Catamarca|America/Catamarca","America/Argentina/Cordoba|America/Cordoba","America/Argentina/Cordoba|America/Rosario","America/Argentina/Jujuy|America/Jujuy","America/Argentina/Mendoza|America/Mendoza","America/Aruba|America/Curacao","America/Aruba|America/Kralendijk","America/Aruba|America/Lower_Princes","America/Atikokan|America/Coral_Harbour","America/Cayman|America/Panama","America/Chicago|US/Central","America/Denver|America/Shiprock","America/Denver|Navajo","America/Denver|US/Mountain","America/Detroit|US/Michigan","America/Edmonton|Canada/Mountain","America/Ensenada|America/Tijuana","America/Ensenada|Mexico/BajaNorte","America/Fort_Wayne|America/Indiana/Indianapolis","America/Fort_Wayne|America/Indianapolis","America/Fort_Wayne|US/East-Indiana","America/Halifax|Canada/Atlantic","America/Havana|Cuba","America/Indiana/Knox|America/Knox_IN","America/Indiana/Knox|US/Indiana-Starke","America/Jamaica|Jamaica","America/Kentucky/Louisville|America/Louisville","America/Los_Angeles|US/Pacific","America/Los_Angeles|US/Pacific-New","America/Manaus|Brazil/West","America/Mazatlan|Mexico/BajaSur","America/Mexico_City|Mexico/General","America/Montreal|America/Toronto","America/Montreal|Canada/Eastern","America/New_York|US/Eastern","America/Noronha|Brazil/DeNoronha","America/Phoenix|US/Arizona","America/Porto_Acre|America/Rio_Branco","America/Porto_Acre|Brazil/Acre","America/Regina|Canada/East-Saskatchewan","America/Regina|Canada/Saskatchewan","America/Santiago|Chile/Continental","America/Sao_Paulo|Brazil/East","America/St_Johns|Canada/Newfoundland","America/Vancouver|Canada/Pacific","America/Whitehorse|Canada/Yukon","America/Winnipeg|Canada/Central","Antarctica/McMurdo|Antarctica/South_Pole","Antarctica/McMurdo|NZ","Antarctica/McMurdo|Pacific/Auckland","Arctic/Longyearbyen|Atlantic/Jan_Mayen","Arctic/Longyearbyen|Europe/Oslo","Asia/Aden|Asia/Kuwait","Asia/Aden|Asia/Riyadh","Asia/Ashgabat|Asia/Ashkhabad","Asia/Bahrain|Asia/Qatar","Asia/Bangkok|Asia/Phnom_Penh","Asia/Bangkok|Asia/Vientiane","Asia/Calcutta|Asia/Kolkata","Asia/Chongqing|Asia/Chungking","Asia/Chongqing|Asia/Harbin","Asia/Chongqing|Asia/Shanghai","Asia/Chongqing|PRC","Asia/Dacca|Asia/Dhaka","Asia/Dubai|Asia/Muscat","Asia/Ho_Chi_Minh|Asia/Saigon","Asia/Hong_Kong|Hongkong","Asia/Istanbul|Europe/Istanbul","Asia/Istanbul|Turkey","Asia/Jerusalem|Asia/Tel_Aviv","Asia/Jerusalem|Israel","Asia/Kashgar|Asia/Urumqi","Asia/Kathmandu|Asia/Katmandu","Asia/Macao|Asia/Macau","Asia/Makassar|Asia/Ujung_Pandang","Asia/Nicosia|Europe/Nicosia","Asia/Seoul|ROK","Asia/Singapore|Singapore","Asia/Taipei|ROC","Asia/Tehran|Iran","Asia/Thimbu|Asia/Thimphu","Asia/Tokyo|Japan","Asia/Ulaanbaatar|Asia/Ulan_Bator","Atlantic/Faeroe|Atlantic/Faroe","Atlantic/Reykjavik|Iceland","Australia/ACT|Australia/Canberra","Australia/ACT|Australia/NSW","Australia/ACT|Australia/Sydney","Australia/Adelaide|Australia/South","Australia/Brisbane|Australia/Queensland","Australia/Broken_Hill|Australia/Yancowinna","Australia/Darwin|Australia/North","Australia/Hobart|Australia/Tasmania","Australia/LHI|Australia/Lord_Howe","Australia/Melbourne|Australia/Victoria","Australia/Perth|Australia/West","Chile/EasterIsland|Pacific/Easter","Eire|Europe/Dublin","Etc/GMT+0|Etc/GMT","Etc/GMT+0|Etc/GMT-0","Etc/GMT+0|Etc/GMT0","Etc/GMT+0|Etc/Greenwich","Etc/GMT+0|GMT","Etc/GMT+0|GMT+0","Etc/GMT+0|GMT-0","Etc/GMT+0|GMT0","Etc/GMT+0|Greenwich","Etc/UCT|UCT","Etc/UTC|Etc/Universal","Etc/UTC|Etc/Zulu","Etc/UTC|UTC","Etc/UTC|Universal","Etc/UTC|Zulu","Europe/Belfast|Europe/Guernsey","Europe/Belfast|Europe/Isle_of_Man","Europe/Belfast|Europe/Jersey","Europe/Belfast|Europe/London","Europe/Belfast|GB","Europe/Belfast|GB-Eire","Europe/Belgrade|Europe/Ljubljana","Europe/Belgrade|Europe/Podgorica","Europe/Belgrade|Europe/Sarajevo","Europe/Belgrade|Europe/Skopje","Europe/Belgrade|Europe/Zagreb","Europe/Bratislava|Europe/Prague","Europe/Busingen|Europe/Vaduz","Europe/Busingen|Europe/Zurich","Europe/Chisinau|Europe/Tiraspol","Europe/Helsinki|Europe/Mariehamn","Europe/Lisbon|Portugal","Europe/Moscow|W-SU","Europe/Rome|Europe/San_Marino","Europe/Rome|Europe/Vatican","Europe/Warsaw|Poland","Kwajalein|Pacific/Kwajalein","NZ-CHAT|Pacific/Chatham","Pacific/Chuuk|Pacific/Truk","Pacific/Chuuk|Pacific/Yap","Pacific/Guam|Pacific/Saipan","Pacific/Honolulu|Pacific/Johnston","Pacific/Honolulu|US/Hawaii","Pacific/Midway|Pacific/Pago_Pago","Pacific/Midway|Pacific/Samoa","Pacific/Midway|US/Samoa","Pacific/Pohnpei|Pacific/Ponape"]}),a});
(function($){$.cleditor={defaultOptions:{width:"auto",height:250,controls:"bold italic underline strikethrough subscript superscript | font size "+"style | color highlight removeformat | bullets numbering | outdent "+"indent | alignleft center alignright justify | undo redo | "+"rule image link unlink | cut copy paste pastetext | print source",colors:"FFF FCC FC9 FF9 FFC 9F9 9FF CFF CCF FCF "+"CCC F66 F96 FF6 FF3 6F9 3FF 6FF 99F F9F "+"BBB F00 F90 FC6 FF0 3F3 6CC 3CF 66C C6C "+"999 C00 F60 FC3 FC0 3C0 0CC 36F 63F C3C "+"666 900 C60 C93 990 090 399 33F 60C 939 "+"333 600 930 963 660 060 366 009 339 636 "+"000 300 630 633 330 030 033 006 309 303",fonts:"Arial,Arial Black,Comic Sans MS,Courier New,Narrow,Garamond,"+"Georgia,Impact,Sans Serif,Serif,Tahoma,Trebuchet MS,Verdana",sizes:"1,2,3,4,5,6,7",styles:[["Paragraph","<p>"],["Header 1","<h1>"],["Header 2","<h2>"],["Header 3","<h3>"],["Header 4","<h4>"],["Header 5","<h5>"],["Header 6","<h6>"]],useCSS:true,docType:'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',docCSSFile:"",bodyStyle:"margin:4px; font-family: Arial,Verdana; cursor:text",headCSS:""},buttons:{init:"bold,,|"+"italic,,|"+"underline,,|"+"strikethrough,,|"+"subscript,,|"+"superscript,,|"+"font,,fontname,|"+"size,Font Size,fontsize,|"+"style,,formatblock,|"+"color,Font Color,forecolor,|"+"highlight,Text Highlight Color,hilitecolor,color|"+"removeformat,Remove Formatting,|"+"bullets,,insertunorderedlist|"+"numbering,,insertorderedlist|"+"outdent,,|"+"indent,,|"+"alignleft,Align Text Left,justifyleft|"+"center,,justifycenter|"+"alignright,Align Text Right,justifyright|"+"justify,,justifyfull|"+"undo,,|"+"redo,,|"+"rule,Insert Horizontal Rule,inserthorizontalrule|"+"image,Insert Image,insertimage,url|"+"link,Insert Hyperlink,createlink,url|"+"unlink,Remove Hyperlink,|"+"cut,,|"+"copy,,|"+"paste,,|"+"pastetext,Paste as Text,inserthtml,|"+"print,,|"+"source,Show Source"},imagesPath:function(){return imagesPath()}};$.fn.cleditor=function(options){var $result=$([]);this.each(function(idx,elem){if(elem.tagName.toUpperCase()==="TEXTAREA"){var data=$.data(elem,CLEDITOR);if(!data)data=new cleditor(elem,options);$result=$result.add(data)}});return $result};var BACKGROUND_COLOR="backgroundColor",BLURRED="blurred",BUTTON="button",BUTTON_NAME="buttonName",CHANGE="change",CLEDITOR="cleditor",CLICK="click",DISABLED="disabled",DIV_TAG="<div>",FOCUSED="focused",TRANSPARENT="transparent",UNSELECTABLE="unselectable",MAIN_CLASS="cleditorMain",TOOLBAR_CLASS="cleditorToolbar",GROUP_CLASS="cleditorGroup",BUTTON_CLASS="cleditorButton",DISABLED_CLASS="cleditorDisabled",DIVIDER_CLASS="cleditorDivider",POPUP_CLASS="cleditorPopup",LIST_CLASS="cleditorList",COLOR_CLASS="cleditorColor",PROMPT_CLASS="cleditorPrompt",MSG_CLASS="cleditorMsg",ua=navigator.userAgent.toLowerCase(),ie=/msie/.test(ua),ie6=/msie\s6/.test(ua),iege11=/(trident)(?:.*rv:([\w.]+))?/.test(ua),webkit=/webkit/.test(ua),iOS=/iPhone|iPad|iPod/i.test(ua),popups={},documentClickAssigned,buttons=$.cleditor.buttons;$.each(buttons.init.split("|"),function(idx,button){var items=button.split(","),name=items[0];buttons[name]={stripIndex:idx,name:name,title:items[1]===""?name.charAt(0).toUpperCase()+name.substr(1):items[1],command:items[2]===""?name:items[2],popupName:items[3]===""?name:items[3]}});delete buttons.init;cleditor=function(area,options){var editor=this;editor.options=options=$.extend({},$.cleditor.defaultOptions,options);var $area=editor.$area=$(area).css({border:"none",margin:0,padding:0}).hide().data(CLEDITOR,editor).blur(function(){updateFrame(editor,true)});var $main=editor.$main=$(DIV_TAG).addClass(MAIN_CLASS).width(options.width).height(options.height);var $toolbar=editor.$toolbar=$(DIV_TAG).addClass(TOOLBAR_CLASS).appendTo($main);var $group=$(DIV_TAG).addClass(GROUP_CLASS).appendTo($toolbar);var groupWidth=0;$.each(options.controls.split(" "),function(idx,buttonName){if(buttonName==="")return true;if(buttonName==="|"){var $div=$(DIV_TAG).addClass(DIVIDER_CLASS).appendTo($group);$group.width(groupWidth+1);groupWidth=0;$group=$(DIV_TAG).addClass(GROUP_CLASS).appendTo($toolbar)}else{var button=buttons[buttonName];var $buttonDiv=$(DIV_TAG).data(BUTTON_NAME,button.name).addClass(BUTTON_CLASS).attr("title",button.title).bind(CLICK,$.proxy(buttonClick,editor)).appendTo($group).hover(hoverEnter,hoverLeave);groupWidth+=24;$group.width(groupWidth+1);var map={};if(button.css)map=button.css;else if(button.image)map.backgroundImage=imageUrl(button.image);if(button.stripIndex)map.backgroundPosition=button.stripIndex*-24;$buttonDiv.css(map);if(ie)$buttonDiv.attr(UNSELECTABLE,"on");if(button.popupName)createPopup(button.popupName,options,button.popupClass,button.popupContent,button.popupHover)}});$main.insertBefore($area).append($area);if(!documentClickAssigned){$(document).click(function(e){var $target=$(e.target);if(!$target.add($target.parents()).is("."+PROMPT_CLASS))hidePopups()});documentClickAssigned=true}if(/auto|%/.test(""+options.width+options.height))$(window).bind("resize.cleditor",function(){refresh(editor)});refresh(editor)};var fn=cleditor.prototype,methods=[["clear",clear],["disable",disable],["execCommand",execCommand],["focus",focus],["hidePopups",hidePopups],["sourceMode",sourceMode,true],["refresh",refresh],["select",select],["selectedHTML",selectedHTML,true],["selectedText",selectedText,true],["showMessage",showMessage],["updateFrame",updateFrame],["updateTextArea",updateTextArea]];$.each(methods,function(idx,method){fn[method[0]]=function(){var editor=this,args=[editor];for(var x=0;x<arguments.length;x++){args.push(arguments[x])}var result=method[1].apply(editor,args);if(method[2])return result;return editor}});fn.blurred=function(handler){var $this=$(this);return handler?$this.bind(BLURRED,handler):$this.trigger(BLURRED)};fn.change=function change(handler){var $this=$(this);return handler?$this.bind(CHANGE,handler):$this.trigger(CHANGE)};fn.focused=function(handler){var $this=$(this);return handler?$this.bind(FOCUSED,handler):$this.trigger(FOCUSED)};function buttonClick(e){var editor=this,buttonDiv=e.target,buttonName=$.data(buttonDiv,BUTTON_NAME),button=buttons[buttonName],popupName=button.popupName,popup=popups[popupName];if(editor.disabled||$(buttonDiv).attr(DISABLED)===DISABLED)return;var data={editor:editor,button:buttonDiv,buttonName:buttonName,popup:popup,popupName:popupName,command:button.command,useCSS:editor.options.useCSS};if(button.buttonClick&&button.buttonClick(e,data)===false)return false;if(buttonName==="source"){if(sourceMode(editor)){delete editor.range;editor.$area.hide();editor.$frame.show();buttonDiv.title=button.title}else{editor.$frame.hide();editor.$area.show();buttonDiv.title="Show Rich Text"}}else if(!sourceMode(editor)){if(popupName){var $popup=$(popup);if(popupName==="url"){if(buttonName==="link"&&selectedText(editor)===""){showMessage(editor,"A selection is required when inserting a link.",buttonDiv);return false}$popup.children(":button").unbind(CLICK).bind(CLICK,function(){var $text=$popup.find(":text"),url=$.trim($text.val());if(url!=="")execCommand(editor,data.command,url,null,data.button);$text.val("http://");hidePopups();focus(editor)})}else if(popupName==="pastetext"){$popup.children(":button").unbind(CLICK).bind(CLICK,function(){var $textarea=$popup.find("textarea"),text=$textarea.val().replace(/\n/g,"<br />");if(text!=="")execCommand(editor,data.command,text,null,data.button);$textarea.val("");hidePopups();focus(editor)})}if(buttonDiv!==$.data(popup,BUTTON)){showPopup(editor,popup,buttonDiv);return false}return}else if(buttonName==="print")editor.$frame[0].contentWindow.print();else if(!execCommand(editor,data.command,data.value,data.useCSS,buttonDiv))return false}focus(editor)}function hoverEnter(e){var $div=$(e.target).closest("div");$div.css(BACKGROUND_COLOR,$div.data(BUTTON_NAME)?"#FFF":"#FFC")}function hoverLeave(e){$(e.target).closest("div").css(BACKGROUND_COLOR,"transparent")}function popupClick(e){var editor=this,popup=e.data.popup,target=e.target;if(popup===popups.msg||$(popup).hasClass(PROMPT_CLASS))return;var buttonDiv=$.data(popup,BUTTON),buttonName=$.data(buttonDiv,BUTTON_NAME),button=buttons[buttonName],command=button.command,value,useCSS=editor.options.useCSS;if(buttonName==="font")value=target.style.fontFamily.replace(/"/g,"");else if(buttonName==="size"){if(target.tagName.toUpperCase()==="DIV")target=target.children[0];value=target.innerHTML}else if(buttonName==="style")value="<"+target.tagName+">";else if(buttonName==="color")value=hex(target.style.backgroundColor);else if(buttonName==="highlight"){value=hex(target.style.backgroundColor);if(ie)command="backcolor";else useCSS=true}var data={editor:editor,button:buttonDiv,buttonName:buttonName,popup:popup,popupName:button.popupName,command:command,value:value,useCSS:useCSS};if(button.popupClick&&button.popupClick(e,data)===false)return;if(data.command&&!execCommand(editor,data.command,data.value,data.useCSS,buttonDiv))return false;hidePopups();focus(editor)}function checksum(text){var a=1,b=0;for(var index=0;index<text.length;++index){a=(a+text.charCodeAt(index))%65521;b=(b+a)%65521}return b<<16|a}function clear(editor){editor.$area.val("");updateFrame(editor)}function createPopup(popupName,options,popupTypeClass,popupContent,popupHover){if(popups[popupName])return popups[popupName];var $popup=$(DIV_TAG).hide().addClass(POPUP_CLASS).appendTo("body");if(popupContent)$popup.html(popupContent);else if(popupName==="color"){var colors=options.colors.split(" ");if(colors.length<10)$popup.width("auto");$.each(colors,function(idx,color){$(DIV_TAG).appendTo($popup).css(BACKGROUND_COLOR,"#"+color)});popupTypeClass=COLOR_CLASS}else if(popupName==="font")$.each(options.fonts.split(","),function(idx,font){$(DIV_TAG).appendTo($popup).css("fontFamily",font).html(font)});else if(popupName==="size")$.each(options.sizes.split(","),function(idx,size){$(DIV_TAG).appendTo($popup).html('<font size="'+size+'">'+size+"</font>")});else if(popupName==="style")$.each(options.styles,function(idx,style){$(DIV_TAG).appendTo($popup).html(style[1]+style[0]+style[1].replace("<","</"))});else if(popupName==="url"){$popup.html('<label>Enter URL:<br /><input type="text" value="http://" style="width:200px" /></label><br /><input type="button" value="Submit" />');popupTypeClass=PROMPT_CLASS}else if(popupName==="pastetext"){$popup.html('<label>Paste your content here:<br /><textarea rows="3" style="width:200px"></textarea></label><br /><input type="button" value="Submit" />');popupTypeClass=PROMPT_CLASS}if(!popupTypeClass&&!popupContent)popupTypeClass=LIST_CLASS;$popup.addClass(popupTypeClass);if(ie){$popup.attr(UNSELECTABLE,"on").find("div,font,p,h1,h2,h3,h4,h5,h6").attr(UNSELECTABLE,"on")}if($popup.hasClass(LIST_CLASS)||popupHover===true)$popup.children().hover(hoverEnter,hoverLeave);popups[popupName]=$popup[0];return $popup[0]}function disable(editor,disabled){if(disabled){editor.$area.attr(DISABLED,DISABLED);editor.disabled=true}else{editor.$area.removeAttr(DISABLED);delete editor.disabled}try{if(ie)editor.doc.body.contentEditable=!disabled;else editor.doc.designMode=!disabled?"on":"off"}catch(err){}refreshButtons(editor)}function execCommand(editor,command,value,useCSS,button){restoreRange(editor);if(!ie){if(useCSS===undefined||useCSS===null)useCSS=editor.options.useCSS;editor.doc.execCommand("styleWithCSS",0,useCSS.toString())}var inserthtml=command.toLowerCase()==="inserthtml";if(ie&&inserthtml)getRange(editor).pasteHTML(value);else if(iege11&&inserthtml){var selection=getSelection(editor),range=selection.getRangeAt(0);range.deleteContents();range.insertNode(range.createContextualFragment(value));selection.removeAllRanges();selection.addRange(range)}else{var success=true,message;try{success=editor.doc.execCommand(command,0,value||null)}catch(err){message=err.message;success=false}if(!success){if("cutcopypaste".indexOf(command)>-1)showMessage(editor,"For security reasons, your browser does not support the "+command+" command. Try using the keyboard shortcut or context menu instead.",button);else showMessage(editor,message?message:"Error executing the "+command+" command.",button)}}refreshButtons(editor);updateTextArea(editor,true);return success}function focus(editor){setTimeout(function(){if(sourceMode(editor))editor.$area.focus();else editor.$frame[0].contentWindow.focus();refreshButtons(editor)},0)}function getRange(editor){if(ie)return getSelection(editor).createRange();return getSelection(editor).getRangeAt(0)}function getSelection(editor){if(ie)return editor.doc.selection;return editor.$frame[0].contentWindow.getSelection()}function hex(s){var m=/rgba?\((\d+), (\d+), (\d+)/.exec(s);if(m){s=(m[1]<<16|m[2]<<8|m[3]).toString(16);while(s.length<6)s="0"+s;return"#"+s}var c=s.split("");if(s.length===4)return"#"+c[1]+c[1]+c[2]+c[2]+c[3]+c[3];return s}function hidePopups(){$.each(popups,function(idx,popup){$(popup).hide().unbind(CLICK).removeData(BUTTON)})}function imagesPath(){href="../img/";return href}function imageUrl(filename){return"url("+imagesPath()+filename+")"}function refresh(editor){var $main=editor.$main,options=editor.options;if(editor.$frame)editor.$frame.remove();var $frame=editor.$frame=$('<iframe frameborder="0" src="javascript:true;" />').hide().appendTo($main);var contentWindow=$frame[0].contentWindow,doc=editor.doc=contentWindow.document,$doc=$(doc);doc.open();doc.write(options.docType+"<html>"+'<head><link rel="stylesheet" type="text/css" href="'+options.docCSSFile+'" /><style>'+options.headCSS+"</style></head>"+'<body style="'+options.bodyStyle+'"></body></html>');doc.close();if(ie||iege11)$doc.click(function(){focus(editor)});updateFrame(editor);if(ie||iege11){$doc.bind("beforedeactivate beforeactivate selectionchange keypress keyup",function(e){if(e.type==="beforedeactivate")editor.inactive=true;else if(e.type==="beforeactivate"){if(!editor.inactive&&editor.range&&editor.range.length>1)editor.range.shift();delete editor.inactive}else if(!editor.inactive){if(!editor.range)editor.range=[];editor.range.unshift(getRange(editor));while(editor.range.length>2)editor.range.pop()}});$frame.focus(function(){restoreRange(editor);$(editor).triggerHandler(FOCUSED)});$frame.blur(function(){$(editor).triggerHandler(BLURRED)})}else{$($frame[0].contentWindow).focus(function(){$(editor).triggerHandler(FOCUSED)}).blur(function(){$(editor).triggerHandler(BLURRED)})}$doc.click(hidePopups).keydown(function(e){if(ie&&getSelection(editor).type=="Control"&&e.keyCode==8){getSelection(editor).clear();e.preventDefault()}}).bind("keyup mouseup",function(){refreshButtons(editor);updateTextArea(editor,true)});if(iOS)editor.$area.show();else $frame.show();$(function(){var $toolbar=editor.$toolbar,$group=$toolbar.children("div:last"),wid=$main.width();var hgt=$group.offset().top+$group.outerHeight()-$toolbar.offset().top+1;$toolbar.height(hgt);hgt=(/%/.test(""+options.height)?$main.height():parseInt(options.height,10))-hgt;$frame.width(wid).height(hgt);editor.$area.width(wid).height(ie6?hgt-2:hgt);disable(editor,editor.disabled);refreshButtons(editor)})}function refreshButtons(editor){if(!iOS&&webkit&&!editor.focused){editor.$frame[0].contentWindow.focus();window.focus();editor.focused=true}var queryObj=editor.doc;if(ie)queryObj=getRange(editor);var inSourceMode=sourceMode(editor);$.each(editor.$toolbar.find("."+BUTTON_CLASS),function(idx,elem){var $elem=$(elem),button=$.cleditor.buttons[$.data(elem,BUTTON_NAME)],command=button.command,enabled=true;if(editor.disabled)enabled=false;else if(button.getEnabled){var data={editor:editor,button:elem,buttonName:button.name,popup:popups[button.popupName],popupName:button.popupName,command:button.command,useCSS:editor.options.useCSS};enabled=button.getEnabled(data);if(enabled===undefined)enabled=true}else if((inSourceMode||iOS)&&button.name!=="source"||ie&&(command==="undo"||command==="redo"))enabled=false;else if(command&&command!=="print"){if(ie&&command==="hilitecolor")command="backcolor";if(!ie&&!iege11||command!=="inserthtml"){try{enabled=queryObj.queryCommandEnabled(command)}catch(err){enabled=false}}}if(enabled){$elem.removeClass(DISABLED_CLASS);$elem.removeAttr(DISABLED)}else{$elem.addClass(DISABLED_CLASS);$elem.attr(DISABLED,DISABLED)}})}function restoreRange(editor){if(editor.range){if(ie)editor.range[0].select();else if(iege11)getSelection(editor).addRange(editor.range[0])}}function select(editor){setTimeout(function(){if(sourceMode(editor))editor.$area.select();else execCommand(editor,"selectall")},0)}function selectedHTML(editor){restoreRange(editor);var range=getRange(editor);if(ie)return range.htmlText;var layer=$("<layer>")[0];layer.appendChild(range.cloneContents());var html=layer.innerHTML;layer=null;return html}function selectedText(editor){restoreRange(editor);if(ie)return getRange(editor).text;return getSelection(editor).toString()}function showMessage(editor,message,button){var popup=createPopup("msg",editor.options,MSG_CLASS);popup.innerHTML=message;showPopup(editor,popup,button)}function showPopup(editor,popup,button){var offset,left,top,$popup=$(popup);if(button){var $button=$(button);offset=$button.offset();left=--offset.left;top=offset.top+$button.height()}else{var $toolbar=editor.$toolbar;offset=$toolbar.offset();left=Math.floor(($toolbar.width()-$popup.width())/2)+offset.left;top=offset.top+$toolbar.height()-2}hidePopups();$popup.css({left:left,top:top}).show();if(button){$.data(popup,BUTTON,button);$popup.bind(CLICK,{popup:popup},$.proxy(popupClick,editor))}setTimeout(function(){$popup.find(":text,textarea").eq(0).focus().select()},100)}function sourceMode(editor){return editor.$area.is(":visible")}function updateFrame(editor,checkForChange){var code=editor.$area.val(),options=editor.options,updateFrameCallback=options.updateFrame,$body=$(editor.doc.body);if(updateFrameCallback){var sum=checksum(code);if(checkForChange&&editor.areaChecksum===sum)return;editor.areaChecksum=sum}var html=updateFrameCallback?updateFrameCallback(code):code;html=html.replace(/<(?=\/?script)/gi,"&lt;");if(options.updateTextArea)editor.frameChecksum=checksum(html);if(html!==$body.html()){$body.html(html);$(editor).triggerHandler(CHANGE)}}function updateTextArea(editor,checkForChange){var html=$(editor.doc.body).html(),options=editor.options,updateTextAreaCallback=options.updateTextArea,$area=editor.$area;if(updateTextAreaCallback){var sum=checksum(html);if(checkForChange&&editor.frameChecksum===sum)return;editor.frameChecksum=sum}var code=updateTextAreaCallback?updateTextAreaCallback(html):html;if(options.updateFrame)editor.areaChecksum=checksum(code);if(code!==$area.val()){$area.val(code);$(editor).triggerHandler(CHANGE)}}})(jQuery);
var ttFrontEndEditor={};ttFrontEndEditor.data={edit_text:"Edit",save_text:"Save",cancel_text:"Cancel",fields:["the_title","the_content","the_category","the_tags","comment_text"],ajax_url:"/wp-admin/admin-ajax.php",spinner:"/wp-admin/images/loading.gif",nonce:"e89f8253f3",cleditor:{controls:"bold italic strikethrough | font size style | color removeformat | bullets numbering | link",css:"",height:250},image:{url:"/wp-admin/media-upload.php?post_id=0&type=image&editable_image=1&TB_iframe=true&width=640",change:"Change Image",insert:"Insert Image",revert:"(Clear)",tb_close:"/wp-includes/js/thickbox/tb-close.png"}};(function(b){b.cleditor.buttons.code={name:"code",image:"",title:"Code",command:"inserthtml",popupName:"pastecode",popupClass:"cleditorPrompt",popupContent:"Paste the code:<br /><textarea cols='40' rows='3'></textarea><br /><input type='button' value='Ok' />",buttonClick:a};ttFrontEndEditor.data.cleditor.controls=ttFrontEndEditor.data.cleditor.controls+" | code";function a(f,d){var c=d.editor;body=b(c.doc.body).html();if(body=="Add a detailed description..."){c.clear()}else{if(body=="Add your reply..."){c.clear()}else{if(body=="Add your comment..."){c.clear()}else{if(body=="Add your answer..."){c.clear()}}}}b(d.popup).children(":button").unbind("click").bind("click",function(j){var h=d.editor;var i=b(d.popup).find("textarea").val();i=i.replace(/</g,"&lt;");i=i.replace(/>/g,"&gt;");var g="<br /><pre>"+i+"</pre>";b(d.popup).find("textarea").val("");body=b(h.doc.body).html();if(body=="Add a detailed description..."){h.clear()}else{if(body=="Add your reply..."){h.clear()}else{if(body=="Add your comment..."){h.clear()}else{if(body=="Add your answer..."){h.clear()}}}}h.execCommand(d.command,g,null,d.button);h.hidePopups();h.focus()})}})(jQuery);(function(a){if(a(".comments-form-textarea").length>0){a(".comments-form-textarea").cleditor({controls:ttFrontEndEditor.data.cleditor.controls,width:"100%",height:"200",bodyStyle:"margin:4px; font:21px 'NeueHaasGroteskText W01', Helvetica, Arial, sans-serif; cursor:text;color:#666;"})}a("div[title='Code']").toggleClass("ttCleditorButton");a("div[title='Code']").after('<div class="cleditorDivider"></div>')})(jQuery);(function(a){a.fn.clEditorUXenhancements=function(b){a(" form .cleditorMain iframe").each(function(){var d=a(this);var c=a(d).contents().find("body").html();if(((c)=="<br>")||((c)=="")){a(d).contents().find("body").empty().css("color","#ddd").append(b)}a(d).contents().find("html").click(function(e){a(d).contents().find("body").css("color","#666");a(" form .cleditorMain iframe").each(function(){var f=a(this).contents().find("body").html();if((f)==(b)){a(this).contents().find("body").empty()}})});a("html").click(function(f){if(a(f.target).is(".cleditorPopup *")){return}a(" form .cleditorMain iframe").each(function(){var g=a(this);var e=a(g).contents().find("body").html();if(((e)=="<br>")||((e)=="")){a(g).parent(".cleditorMain").css("border","1px solid #bfbfbf");a(g).contents().find("body").empty().css("color","#ddd").append(b)}})})})}})(jQuery);
!function(a){"use strict";"function"==typeof define&&define.amd?define(["jquery"],a):"undefined"!=typeof exports?module.exports=a(require("jquery")):a(jQuery)}(function(a){"use strict";var b=window.Slick||{};b=function(){function c(c,d){var f,e=this;e.defaults={accessibility:!0,adaptiveHeight:!1,appendArrows:a(c),appendDots:a(c),arrows:!0,asNavFor:null,prevArrow:'<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',nextArrow:'<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',autoplay:!1,autoplaySpeed:3e3,centerMode:!1,centerPadding:"50px",cssEase:"ease",customPaging:function(b,c){return a('<button type="button" data-role="none" role="button" tabindex="0" />').text(c+1)},dots:!1,dotsClass:"slick-dots",draggable:!0,easing:"linear",edgeFriction:.35,fade:!1,focusOnSelect:!1,infinite:!0,initialSlide:0,lazyLoad:"ondemand",mobileFirst:!1,pauseOnHover:!0,pauseOnFocus:!0,pauseOnDotsHover:!1,respondTo:"window",responsive:null,rows:1,rtl:!1,slide:"",slidesPerRow:1,slidesToShow:1,slidesToScroll:1,speed:500,swipe:!0,swipeToSlide:!1,touchMove:!0,touchThreshold:5,useCSS:!0,useTransform:!0,variableWidth:!1,vertical:!1,verticalSwiping:!1,waitForAnimate:!0,zIndex:1e3},e.initials={animating:!1,dragging:!1,autoPlayTimer:null,currentDirection:0,currentLeft:null,currentSlide:0,direction:1,$dots:null,listWidth:null,listHeight:null,loadIndex:0,$nextArrow:null,$prevArrow:null,slideCount:null,slideWidth:null,$slideTrack:null,$slides:null,sliding:!1,slideOffset:0,swipeLeft:null,$list:null,touchObject:{},transformsEnabled:!1,unslicked:!1},a.extend(e,e.initials),e.activeBreakpoint=null,e.animType=null,e.animProp=null,e.breakpoints=[],e.breakpointSettings=[],e.cssTransitions=!1,e.focussed=!1,e.interrupted=!1,e.hidden="hidden",e.paused=!0,e.positionProp=null,e.respondTo=null,e.rowCount=1,e.shouldClick=!0,e.$slider=a(c),e.$slidesCache=null,e.transformType=null,e.transitionType=null,e.visibilityChange="visibilitychange",e.windowWidth=0,e.windowTimer=null,f=a(c).data("slick")||{},e.options=a.extend({},e.defaults,d,f),e.currentSlide=e.options.initialSlide,e.originalSettings=e.options,"undefined"!=typeof document.mozHidden?(e.hidden="mozHidden",e.visibilityChange="mozvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(e.hidden="webkitHidden",e.visibilityChange="webkitvisibilitychange"),e.autoPlay=a.proxy(e.autoPlay,e),e.autoPlayClear=a.proxy(e.autoPlayClear,e),e.autoPlayIterator=a.proxy(e.autoPlayIterator,e),e.changeSlide=a.proxy(e.changeSlide,e),e.clickHandler=a.proxy(e.clickHandler,e),e.selectHandler=a.proxy(e.selectHandler,e),e.setPosition=a.proxy(e.setPosition,e),e.swipeHandler=a.proxy(e.swipeHandler,e),e.dragHandler=a.proxy(e.dragHandler,e),e.keyHandler=a.proxy(e.keyHandler,e),e.instanceUid=b++,e.htmlExpr=/^(?:\s*(<[\w\W]+>)[^>]*)$/,e.registerBreakpoints(),e.init(!0)}var b=0;return c}(),b.prototype.activateADA=function(){var a=this;a.$slideTrack.find(".slick-active").attr({"aria-hidden":"false"}).find("a, input, button, select").attr({tabindex:"0"})},b.prototype.addSlide=b.prototype.slickAdd=function(b,c,d){var e=this;if("boolean"==typeof c)d=c,c=null;else if(c<0||c>=e.slideCount)return!1;e.unload(),"number"==typeof c?0===c&&0===e.$slides.length?a(b).appendTo(e.$slideTrack):d?a(b).insertBefore(e.$slides.eq(c)):a(b).insertAfter(e.$slides.eq(c)):d===!0?a(b).prependTo(e.$slideTrack):a(b).appendTo(e.$slideTrack),e.$slides=e.$slideTrack.children(this.options.slide),e.$slideTrack.children(this.options.slide).detach(),e.$slideTrack.append(e.$slides),e.$slides.each(function(b,c){a(c).attr("data-slick-index",b)}),e.$slidesCache=e.$slides,e.reinit()},b.prototype.animateHeight=function(){var a=this;if(1===a.options.slidesToShow&&a.options.adaptiveHeight===!0&&a.options.vertical===!1){var b=a.$slides.eq(a.currentSlide).outerHeight(!0);a.$list.animate({height:b},a.options.speed)}},b.prototype.animateSlide=function(b,c){var d={},e=this;e.animateHeight(),e.options.rtl===!0&&e.options.vertical===!1&&(b=-b),e.transformsEnabled===!1?e.options.vertical===!1?e.$slideTrack.animate({left:b},e.options.speed,e.options.easing,c):e.$slideTrack.animate({top:b},e.options.speed,e.options.easing,c):e.cssTransitions===!1?(e.options.rtl===!0&&(e.currentLeft=-e.currentLeft),a({animStart:e.currentLeft}).animate({animStart:b},{duration:e.options.speed,easing:e.options.easing,step:function(a){a=Math.ceil(a),e.options.vertical===!1?(d[e.animType]="translate("+a+"px, 0px)",e.$slideTrack.css(d)):(d[e.animType]="translate(0px,"+a+"px)",e.$slideTrack.css(d))},complete:function(){c&&c.call()}})):(e.applyTransition(),b=Math.ceil(b),e.options.vertical===!1?d[e.animType]="translate3d("+b+"px, 0px, 0px)":d[e.animType]="translate3d(0px,"+b+"px, 0px)",e.$slideTrack.css(d),c&&setTimeout(function(){e.disableTransition(),c.call()},e.options.speed))},b.prototype.getNavTarget=function(){var b=this,c=b.options.asNavFor;return c&&null!==c&&(c=a(c).not(b.$slider)),c},b.prototype.asNavFor=function(b){var c=this,d=c.getNavTarget();null!==d&&"object"==typeof d&&d.each(function(){var c=a(this).slick("getSlick");c.unslicked||c.slideHandler(b,!0)})},b.prototype.applyTransition=function(a){var b=this,c={};b.options.fade===!1?c[b.transitionType]=b.transformType+" "+b.options.speed+"ms "+b.options.cssEase:c[b.transitionType]="opacity "+b.options.speed+"ms "+b.options.cssEase,b.options.fade===!1?b.$slideTrack.css(c):b.$slides.eq(a).css(c)},b.prototype.autoPlay=function(){var a=this;a.autoPlayClear(),a.slideCount>a.options.slidesToShow&&(a.autoPlayTimer=setInterval(a.autoPlayIterator,a.options.autoplaySpeed))},b.prototype.autoPlayClear=function(){var a=this;a.autoPlayTimer&&clearInterval(a.autoPlayTimer)},b.prototype.autoPlayIterator=function(){var a=this,b=a.currentSlide+a.options.slidesToScroll;a.paused||a.interrupted||a.focussed||(a.options.infinite===!1&&(1===a.direction&&a.currentSlide+1===a.slideCount-1?a.direction=0:0===a.direction&&(b=a.currentSlide-a.options.slidesToScroll,a.currentSlide-1===0&&(a.direction=1))),a.slideHandler(b))},b.prototype.buildArrows=function(){var b=this;b.options.arrows===!0&&(b.$prevArrow=a(b.options.prevArrow).addClass("slick-arrow"),b.$nextArrow=a(b.options.nextArrow).addClass("slick-arrow"),b.slideCount>b.options.slidesToShow?(b.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),b.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),b.htmlExpr.test(b.options.prevArrow)&&b.$prevArrow.prependTo(b.options.appendArrows),b.htmlExpr.test(b.options.nextArrow)&&b.$nextArrow.appendTo(b.options.appendArrows),b.options.infinite!==!0&&b.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true")):b.$prevArrow.add(b.$nextArrow).addClass("slick-hidden").attr({"aria-disabled":"true",tabindex:"-1"}))},b.prototype.buildDots=function(){var c,d,b=this;if(b.options.dots===!0&&b.slideCount>b.options.slidesToShow){for(b.$slider.addClass("slick-dotted"),d=a("<ul />").addClass(b.options.dotsClass),c=0;c<=b.getDotCount();c+=1)d.append(a("<li />").append(b.options.customPaging.call(this,b,c)));b.$dots=d.appendTo(b.options.appendDots),b.$dots.find("li").first().addClass("slick-active").attr("aria-hidden","false")}},b.prototype.buildOut=function(){var b=this;b.$slides=b.$slider.children(b.options.slide+":not(.slick-cloned)").addClass("slick-slide"),b.slideCount=b.$slides.length,b.$slides.each(function(b,c){a(c).attr("data-slick-index",b).data("originalStyling",a(c).attr("style")||"")}),b.$slider.addClass("slick-slider"),b.$slideTrack=0===b.slideCount?a('<div class="slick-track"/>').appendTo(b.$slider):b.$slides.wrapAll('<div class="slick-track"/>').parent(),b.$list=b.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(),b.$slideTrack.css("opacity",0),b.options.centerMode!==!0&&b.options.swipeToSlide!==!0||(b.options.slidesToScroll=1),a("img[data-lazy]",b.$slider).not("[src]").addClass("slick-loading"),b.setupInfinite(),b.buildArrows(),b.buildDots(),b.updateDots(),b.setSlideClasses("number"==typeof b.currentSlide?b.currentSlide:0),b.options.draggable===!0&&b.$list.addClass("draggable")},b.prototype.buildRows=function(){var b,c,d,e,f,g,h,a=this;if(e=document.createDocumentFragment(),g=a.$slider.children(),a.options.rows>1){for(h=a.options.slidesPerRow*a.options.rows,f=Math.ceil(g.length/h),b=0;b<f;b++){var i=document.createElement("div");for(c=0;c<a.options.rows;c++){var j=document.createElement("div");for(d=0;d<a.options.slidesPerRow;d++){var k=b*h+(c*a.options.slidesPerRow+d);g.get(k)&&j.appendChild(g.get(k))}i.appendChild(j)}e.appendChild(i)}a.$slider.empty().append(e),a.$slider.children().children().children().css({width:100/a.options.slidesPerRow+"%",display:"inline-block"})}},b.prototype.checkResponsive=function(b,c){var e,f,g,d=this,h=!1,i=d.$slider.width(),j=window.innerWidth||a(window).width();if("window"===d.respondTo?g=j:"slider"===d.respondTo?g=i:"min"===d.respondTo&&(g=Math.min(j,i)),d.options.responsive&&d.options.responsive.length&&null!==d.options.responsive){f=null;for(e in d.breakpoints)d.breakpoints.hasOwnProperty(e)&&(d.originalSettings.mobileFirst===!1?g<d.breakpoints[e]&&(f=d.breakpoints[e]):g>d.breakpoints[e]&&(f=d.breakpoints[e]));null!==f?null!==d.activeBreakpoint?(f!==d.activeBreakpoint||c)&&(d.activeBreakpoint=f,"unslick"===d.breakpointSettings[f]?d.unslick(f):(d.options=a.extend({},d.originalSettings,d.breakpointSettings[f]),b===!0&&(d.currentSlide=d.options.initialSlide),d.refresh(b)),h=f):(d.activeBreakpoint=f,"unslick"===d.breakpointSettings[f]?d.unslick(f):(d.options=a.extend({},d.originalSettings,d.breakpointSettings[f]),b===!0&&(d.currentSlide=d.options.initialSlide),d.refresh(b)),h=f):null!==d.activeBreakpoint&&(d.activeBreakpoint=null,d.options=d.originalSettings,b===!0&&(d.currentSlide=d.options.initialSlide),d.refresh(b),h=f),b||h===!1||d.$slider.trigger("breakpoint",[d,h])}},b.prototype.changeSlide=function(b,c){var f,g,h,d=this,e=a(b.currentTarget);switch(e.is("a")&&b.preventDefault(),e.is("li")||(e=e.closest("li")),h=d.slideCount%d.options.slidesToScroll!==0,f=h?0:(d.slideCount-d.currentSlide)%d.options.slidesToScroll,b.data.message){case"previous":g=0===f?d.options.slidesToScroll:d.options.slidesToShow-f,d.slideCount>d.options.slidesToShow&&d.slideHandler(d.currentSlide-g,!1,c);break;case"next":g=0===f?d.options.slidesToScroll:f,d.slideCount>d.options.slidesToShow&&d.slideHandler(d.currentSlide+g,!1,c);break;case"index":var i=0===b.data.index?0:b.data.index||e.index()*d.options.slidesToScroll;d.slideHandler(d.checkNavigable(i),!1,c),e.children().trigger("focus");break;default:return}},b.prototype.checkNavigable=function(a){var c,d,b=this;if(c=b.getNavigableIndexes(),d=0,a>c[c.length-1])a=c[c.length-1];else for(var e in c){if(a<c[e]){a=d;break}d=c[e]}return a},b.prototype.cleanUpEvents=function(){var b=this;b.options.dots&&null!==b.$dots&&a("li",b.$dots).off("click.slick",b.changeSlide).off("mouseenter.slick",a.proxy(b.interrupt,b,!0)).off("mouseleave.slick",a.proxy(b.interrupt,b,!1)),b.$slider.off("focus.slick blur.slick"),b.options.arrows===!0&&b.slideCount>b.options.slidesToShow&&(b.$prevArrow&&b.$prevArrow.off("click.slick",b.changeSlide),b.$nextArrow&&b.$nextArrow.off("click.slick",b.changeSlide)),b.$list.off("touchstart.slick mousedown.slick",b.swipeHandler),b.$list.off("touchmove.slick mousemove.slick",b.swipeHandler),b.$list.off("touchend.slick mouseup.slick",b.swipeHandler),b.$list.off("touchcancel.slick mouseleave.slick",b.swipeHandler),b.$list.off("click.slick",b.clickHandler),a(document).off(b.visibilityChange,b.visibility),b.cleanUpSlideEvents(),b.options.accessibility===!0&&b.$list.off("keydown.slick",b.keyHandler),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().off("click.slick",b.selectHandler),a(window).off("orientationchange.slick.slick-"+b.instanceUid,b.orientationChange),a(window).off("resize.slick.slick-"+b.instanceUid,b.resize),a("[draggable!=true]",b.$slideTrack).off("dragstart",b.preventDefault),a(window).off("load.slick.slick-"+b.instanceUid,b.setPosition),a(document).off("ready.slick.slick-"+b.instanceUid,b.setPosition)},b.prototype.cleanUpSlideEvents=function(){var b=this;b.$list.off("mouseenter.slick",a.proxy(b.interrupt,b,!0)),b.$list.off("mouseleave.slick",a.proxy(b.interrupt,b,!1))},b.prototype.cleanUpRows=function(){var b,a=this;a.options.rows>1&&(b=a.$slides.children().children(),b.removeAttr("style"),a.$slider.empty().append(b))},b.prototype.clickHandler=function(a){var b=this;b.shouldClick===!1&&(a.stopImmediatePropagation(),a.stopPropagation(),a.preventDefault())},b.prototype.destroy=function(b){var c=this;c.autoPlayClear(),c.touchObject={},c.cleanUpEvents(),a(".slick-cloned",c.$slider).detach(),c.$dots&&c.$dots.remove(),c.$prevArrow&&c.$prevArrow.length&&(c.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),c.htmlExpr.test(c.options.prevArrow)&&c.$prevArrow.remove()),c.$nextArrow&&c.$nextArrow.length&&(c.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),c.htmlExpr.test(c.options.nextArrow)&&c.$nextArrow.remove()),c.$slides&&(c.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function(){a(this).attr("style",a(this).data("originalStyling"))}),c.$slideTrack.children(this.options.slide).detach(),c.$slideTrack.detach(),c.$list.detach(),c.$slider.append(c.$slides)),c.cleanUpRows(),c.$slider.removeClass("slick-slider"),c.$slider.removeClass("slick-initialized"),c.$slider.removeClass("slick-dotted"),c.unslicked=!0,b||c.$slider.trigger("destroy",[c])},b.prototype.disableTransition=function(a){var b=this,c={};c[b.transitionType]="",b.options.fade===!1?b.$slideTrack.css(c):b.$slides.eq(a).css(c)},b.prototype.fadeSlide=function(a,b){var c=this;c.cssTransitions===!1?(c.$slides.eq(a).css({zIndex:c.options.zIndex}),c.$slides.eq(a).animate({opacity:1},c.options.speed,c.options.easing,b)):(c.applyTransition(a),c.$slides.eq(a).css({opacity:1,zIndex:c.options.zIndex}),b&&setTimeout(function(){c.disableTransition(a),b.call()},c.options.speed))},b.prototype.fadeSlideOut=function(a){var b=this;b.cssTransitions===!1?b.$slides.eq(a).animate({opacity:0,zIndex:b.options.zIndex-2},b.options.speed,b.options.easing):(b.applyTransition(a),b.$slides.eq(a).css({opacity:0,zIndex:b.options.zIndex-2}))},b.prototype.filterSlides=b.prototype.slickFilter=function(a){var b=this;null!==a&&(b.$slidesCache=b.$slides,b.unload(),b.$slideTrack.children(this.options.slide).detach(),b.$slidesCache.filter(a).appendTo(b.$slideTrack),b.reinit())},b.prototype.focusHandler=function(){var b=this;b.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick","*:not(.slick-arrow)",function(c){c.stopImmediatePropagation();var d=a(this);setTimeout(function(){b.options.pauseOnFocus&&(b.focussed=d.is(":focus"),b.autoPlay())},0)})},b.prototype.getCurrent=b.prototype.slickCurrentSlide=function(){var a=this;return a.currentSlide},b.prototype.getDotCount=function(){var a=this,b=0,c=0,d=0;if(a.options.infinite===!0)for(;b<a.slideCount;)++d,b=c+a.options.slidesToScroll,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;else if(a.options.centerMode===!0)d=a.slideCount;else if(a.options.asNavFor)for(;b<a.slideCount;)++d,b=c+a.options.slidesToScroll,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;else d=1+Math.ceil((a.slideCount-a.options.slidesToShow)/a.options.slidesToScroll);return d-1},b.prototype.getLeft=function(a){var c,d,f,b=this,e=0;return b.slideOffset=0,d=b.$slides.first().outerHeight(!0),b.options.infinite===!0?(b.slideCount>b.options.slidesToShow&&(b.slideOffset=b.slideWidth*b.options.slidesToShow*-1,e=d*b.options.slidesToShow*-1),b.slideCount%b.options.slidesToScroll!==0&&a+b.options.slidesToScroll>b.slideCount&&b.slideCount>b.options.slidesToShow&&(a>b.slideCount?(b.slideOffset=(b.options.slidesToShow-(a-b.slideCount))*b.slideWidth*-1,e=(b.options.slidesToShow-(a-b.slideCount))*d*-1):(b.slideOffset=b.slideCount%b.options.slidesToScroll*b.slideWidth*-1,e=b.slideCount%b.options.slidesToScroll*d*-1))):a+b.options.slidesToShow>b.slideCount&&(b.slideOffset=(a+b.options.slidesToShow-b.slideCount)*b.slideWidth,e=(a+b.options.slidesToShow-b.slideCount)*d),b.slideCount<=b.options.slidesToShow&&(b.slideOffset=0,e=0),b.options.centerMode===!0&&b.options.infinite===!0?b.slideOffset+=b.slideWidth*Math.floor(b.options.slidesToShow/2)-b.slideWidth:b.options.centerMode===!0&&(b.slideOffset=0,b.slideOffset+=b.slideWidth*Math.floor(b.options.slidesToShow/2)),c=b.options.vertical===!1?a*b.slideWidth*-1+b.slideOffset:a*d*-1+e,b.options.variableWidth===!0&&(f=b.slideCount<=b.options.slidesToShow||b.options.infinite===!1?b.$slideTrack.children(".slick-slide").eq(a):b.$slideTrack.children(".slick-slide").eq(a+b.options.slidesToShow),c=b.options.rtl===!0?f[0]?(b.$slideTrack.width()-f[0].offsetLeft-f.width())*-1:0:f[0]?f[0].offsetLeft*-1:0,b.options.centerMode===!0&&(f=b.slideCount<=b.options.slidesToShow||b.options.infinite===!1?b.$slideTrack.children(".slick-slide").eq(a):b.$slideTrack.children(".slick-slide").eq(a+b.options.slidesToShow+1),c=b.options.rtl===!0?f[0]?(b.$slideTrack.width()-f[0].offsetLeft-f.width())*-1:0:f[0]?f[0].offsetLeft*-1:0,c+=(b.$list.width()-f.outerWidth())/2)),c},b.prototype.getOption=b.prototype.slickGetOption=function(a){var b=this;return b.options[a]},b.prototype.getNavigableIndexes=function(){var e,a=this,b=0,c=0,d=[];for(a.options.infinite===!1?e=a.slideCount:(b=a.options.slidesToScroll*-1,c=a.options.slidesToScroll*-1,e=2*a.slideCount);b<e;)d.push(b),b=c+a.options.slidesToScroll,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;return d},b.prototype.getSlick=function(){return this},b.prototype.getSlideCount=function(){var c,d,e,b=this;return e=b.options.centerMode===!0?b.slideWidth*Math.floor(b.options.slidesToShow/2):0,b.options.swipeToSlide===!0?(b.$slideTrack.find(".slick-slide").each(function(c,f){if(f.offsetLeft-e+a(f).outerWidth()/2>b.swipeLeft*-1)return d=f,!1}),c=Math.abs(a(d).attr("data-slick-index")-b.currentSlide)||1):b.options.slidesToScroll},b.prototype.goTo=b.prototype.slickGoTo=function(a,b){var c=this;c.changeSlide({data:{message:"index",index:parseInt(a)}},b)},b.prototype.init=function(b){var c=this;a(c.$slider).hasClass("slick-initialized")||(a(c.$slider).addClass("slick-initialized"),c.buildRows(),c.buildOut(),c.setProps(),c.startLoad(),c.loadSlider(),c.initializeEvents(),c.updateArrows(),c.updateDots(),c.checkResponsive(!0),c.focusHandler()),b&&c.$slider.trigger("init",[c]),c.options.accessibility===!0&&c.initADA(),c.options.autoplay&&(c.paused=!1,c.autoPlay())},b.prototype.initADA=function(){var b=this;b.$slides.add(b.$slideTrack.find(".slick-cloned")).attr({"aria-hidden":"true",tabindex:"-1"}).find("a, input, button, select").attr({tabindex:"-1"}),b.$slideTrack.attr("role","listbox"),b.$slides.not(b.$slideTrack.find(".slick-cloned")).each(function(c){a(this).attr({role:"option","aria-describedby":"slick-slide"+b.instanceUid+c})}),null!==b.$dots&&b.$dots.attr("role","tablist").find("li").each(function(c){a(this).attr({role:"presentation","aria-selected":"false","aria-controls":"navigation"+b.instanceUid+c,id:"slick-slide"+b.instanceUid+c})}).first().attr("aria-selected","true").end().find("button").attr("role","button").end().closest("div").attr("role","toolbar"),b.activateADA()},b.prototype.initArrowEvents=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.off("click.slick").on("click.slick",{message:"previous"},a.changeSlide),a.$nextArrow.off("click.slick").on("click.slick",{message:"next"},a.changeSlide))},b.prototype.initDotEvents=function(){var b=this;b.options.dots===!0&&b.slideCount>b.options.slidesToShow&&a("li",b.$dots).on("click.slick",{message:"index"},b.changeSlide),b.options.dots===!0&&b.options.pauseOnDotsHover===!0&&a("li",b.$dots).on("mouseenter.slick",a.proxy(b.interrupt,b,!0)).on("mouseleave.slick",a.proxy(b.interrupt,b,!1))},b.prototype.initSlideEvents=function(){var b=this;b.options.pauseOnHover&&(b.$list.on("mouseenter.slick",a.proxy(b.interrupt,b,!0)),b.$list.on("mouseleave.slick",a.proxy(b.interrupt,b,!1)))},b.prototype.initializeEvents=function(){var b=this;b.initArrowEvents(),b.initDotEvents(),b.initSlideEvents(),b.$list.on("touchstart.slick mousedown.slick",{action:"start"},b.swipeHandler),b.$list.on("touchmove.slick mousemove.slick",{action:"move"},b.swipeHandler),b.$list.on("touchend.slick mouseup.slick",{action:"end"},b.swipeHandler),b.$list.on("touchcancel.slick mouseleave.slick",{action:"end"},b.swipeHandler),b.$list.on("click.slick",b.clickHandler),a(document).on(b.visibilityChange,a.proxy(b.visibility,b)),b.options.accessibility===!0&&b.$list.on("keydown.slick",b.keyHandler),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().on("click.slick",b.selectHandler),a(window).on("orientationchange.slick.slick-"+b.instanceUid,a.proxy(b.orientationChange,b)),a(window).on("resize.slick.slick-"+b.instanceUid,a.proxy(b.resize,b)),a("[draggable!=true]",b.$slideTrack).on("dragstart",b.preventDefault),a(window).on("load.slick.slick-"+b.instanceUid,b.setPosition),a(document).on("ready.slick.slick-"+b.instanceUid,b.setPosition)},b.prototype.initUI=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.show(),a.$nextArrow.show()),a.options.dots===!0&&a.slideCount>a.options.slidesToShow&&a.$dots.show()},b.prototype.keyHandler=function(a){var b=this;a.target.tagName.match("TEXTAREA|INPUT|SELECT")||(37===a.keyCode&&b.options.accessibility===!0?b.changeSlide({data:{message:b.options.rtl===!0?"next":"previous"}}):39===a.keyCode&&b.options.accessibility===!0&&b.changeSlide({data:{message:b.options.rtl===!0?"previous":"next"}}))},b.prototype.lazyLoad=function(){function g(c){a("img[data-lazy]",c).each(function(){var c=a(this),d=a(this).attr("data-lazy"),e=document.createElement("img");e.onload=function(){c.animate({opacity:0},100,function(){c.attr("src",d).animate({opacity:1},200,function(){c.removeAttr("data-lazy").removeClass("slick-loading")}),b.$slider.trigger("lazyLoaded",[b,c,d])})},e.onerror=function(){c.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),b.$slider.trigger("lazyLoadError",[b,c,d])},e.src=d})}var c,d,e,f,b=this;b.options.centerMode===!0?b.options.infinite===!0?(e=b.currentSlide+(b.options.slidesToShow/2+1),f=e+b.options.slidesToShow+2):(e=Math.max(0,b.currentSlide-(b.options.slidesToShow/2+1)),f=2+(b.options.slidesToShow/2+1)+b.currentSlide):(e=b.options.infinite?b.options.slidesToShow+b.currentSlide:b.currentSlide,f=Math.ceil(e+b.options.slidesToShow),b.options.fade===!0&&(e>0&&e--,f<=b.slideCount&&f++)),c=b.$slider.find(".slick-slide").slice(e,f),g(c),b.slideCount<=b.options.slidesToShow?(d=b.$slider.find(".slick-slide"),g(d)):b.currentSlide>=b.slideCount-b.options.slidesToShow?(d=b.$slider.find(".slick-cloned").slice(0,b.options.slidesToShow),g(d)):0===b.currentSlide&&(d=b.$slider.find(".slick-cloned").slice(b.options.slidesToShow*-1),g(d))},b.prototype.loadSlider=function(){var a=this;a.setPosition(),a.$slideTrack.css({opacity:1}),a.$slider.removeClass("slick-loading"),a.initUI(),"progressive"===a.options.lazyLoad&&a.progressiveLazyLoad()},b.prototype.next=b.prototype.slickNext=function(){var a=this;a.changeSlide({data:{message:"next"}})},b.prototype.orientationChange=function(){var a=this;a.checkResponsive(),a.setPosition()},b.prototype.pause=b.prototype.slickPause=function(){var a=this;a.autoPlayClear(),a.paused=!0},b.prototype.play=b.prototype.slickPlay=function(){var a=this;a.autoPlay(),a.options.autoplay=!0,a.paused=!1,a.focussed=!1,a.interrupted=!1},b.prototype.postSlide=function(a){var b=this;b.unslicked||(b.$slider.trigger("afterChange",[b,a]),b.animating=!1,b.setPosition(),b.swipeLeft=null,b.options.autoplay&&b.autoPlay(),b.options.accessibility===!0&&b.initADA())},b.prototype.prev=b.prototype.slickPrev=function(){var a=this;a.changeSlide({data:{message:"previous"}})},b.prototype.preventDefault=function(a){a.preventDefault()},b.prototype.progressiveLazyLoad=function(b){b=b||1;var e,f,g,c=this,d=a("img[data-lazy]",c.$slider);d.length?(e=d.first(),f=e.attr("data-lazy"),g=document.createElement("img"),g.onload=function(){e.attr("src",f).removeAttr("data-lazy").removeClass("slick-loading"),c.options.adaptiveHeight===!0&&c.setPosition(),c.$slider.trigger("lazyLoaded",[c,e,f]),c.progressiveLazyLoad()},g.onerror=function(){b<3?setTimeout(function(){c.progressiveLazyLoad(b+1)},500):(e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),c.$slider.trigger("lazyLoadError",[c,e,f]),c.progressiveLazyLoad())},g.src=f):c.$slider.trigger("allImagesLoaded",[c])},b.prototype.refresh=function(b){var d,e,c=this;e=c.slideCount-c.options.slidesToShow,!c.options.infinite&&c.currentSlide>e&&(c.currentSlide=e),c.slideCount<=c.options.slidesToShow&&(c.currentSlide=0),d=c.currentSlide,c.destroy(!0),a.extend(c,c.initials,{currentSlide:d}),c.init(),b||c.changeSlide({data:{message:"index",index:d}},!1)},b.prototype.registerBreakpoints=function(){var c,d,e,b=this,f=b.options.responsive||null;if("array"===a.type(f)&&f.length){b.respondTo=b.options.respondTo||"window";for(c in f)if(e=b.breakpoints.length-1,d=f[c].breakpoint,f.hasOwnProperty(c)){for(;e>=0;)b.breakpoints[e]&&b.breakpoints[e]===d&&b.breakpoints.splice(e,1),e--;b.breakpoints.push(d),b.breakpointSettings[d]=f[c].settings}b.breakpoints.sort(function(a,c){return b.options.mobileFirst?a-c:c-a})}},b.prototype.reinit=function(){var b=this;b.$slides=b.$slideTrack.children(b.options.slide).addClass("slick-slide"),b.slideCount=b.$slides.length,b.currentSlide>=b.slideCount&&0!==b.currentSlide&&(b.currentSlide=b.currentSlide-b.options.slidesToScroll),b.slideCount<=b.options.slidesToShow&&(b.currentSlide=0),b.registerBreakpoints(),b.setProps(),b.setupInfinite(),b.buildArrows(),b.updateArrows(),b.initArrowEvents(),b.buildDots(),b.updateDots(),b.initDotEvents(),b.cleanUpSlideEvents(),b.initSlideEvents(),b.checkResponsive(!1,!0),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().on("click.slick",b.selectHandler),b.setSlideClasses("number"==typeof b.currentSlide?b.currentSlide:0),b.setPosition(),b.focusHandler(),b.paused=!b.options.autoplay,b.autoPlay(),b.$slider.trigger("reInit",[b])},b.prototype.resize=function(){var b=this;a(window).width()!==b.windowWidth&&(clearTimeout(b.windowDelay),b.windowDelay=window.setTimeout(function(){b.windowWidth=a(window).width(),b.checkResponsive(),b.unslicked||b.setPosition()},50))},b.prototype.removeSlide=b.prototype.slickRemove=function(a,b,c){var d=this;return"boolean"==typeof a?(b=a,a=b===!0?0:d.slideCount-1):a=b===!0?--a:a,!(d.slideCount<1||a<0||a>d.slideCount-1)&&(d.unload(),c===!0?d.$slideTrack.children().remove():d.$slideTrack.children(this.options.slide).eq(a).remove(),d.$slides=d.$slideTrack.children(this.options.slide),d.$slideTrack.children(this.options.slide).detach(),d.$slideTrack.append(d.$slides),d.$slidesCache=d.$slides,void d.reinit())},b.prototype.setCSS=function(a){var d,e,b=this,c={};b.options.rtl===!0&&(a=-a),d="left"==b.positionProp?Math.ceil(a)+"px":"0px",e="top"==b.positionProp?Math.ceil(a)+"px":"0px",c[b.positionProp]=a,b.transformsEnabled===!1?b.$slideTrack.css(c):(c={},b.cssTransitions===!1?(c[b.animType]="translate("+d+", "+e+")",b.$slideTrack.css(c)):(c[b.animType]="translate3d("+d+", "+e+", 0px)",b.$slideTrack.css(c)))},b.prototype.setDimensions=function(){var a=this;a.options.vertical===!1?a.options.centerMode===!0&&a.$list.css({padding:"0px "+a.options.centerPadding}):(a.$list.height(a.$slides.first().outerHeight(!0)*a.options.slidesToShow),a.options.centerMode===!0&&a.$list.css({padding:a.options.centerPadding+" 0px"})),a.listWidth=a.$list.width(),a.listHeight=a.$list.height(),a.options.vertical===!1&&a.options.variableWidth===!1?(a.slideWidth=Math.ceil(a.listWidth/a.options.slidesToShow),a.$slideTrack.width(Math.ceil(a.slideWidth*a.$slideTrack.children(".slick-slide").length))):a.options.variableWidth===!0?a.$slideTrack.width(5e3*a.slideCount):(a.slideWidth=Math.ceil(a.listWidth),a.$slideTrack.height(Math.ceil(a.$slides.first().outerHeight(!0)*a.$slideTrack.children(".slick-slide").length)));var b=a.$slides.first().outerWidth(!0)-a.$slides.first().width();a.options.variableWidth===!1&&a.$slideTrack.children(".slick-slide").width(a.slideWidth-b)},b.prototype.setFade=function(){var c,b=this;b.$slides.each(function(d,e){c=b.slideWidth*d*-1,b.options.rtl===!0?a(e).css({position:"relative",right:c,top:0,zIndex:b.options.zIndex-2,opacity:0}):a(e).css({position:"relative",left:c,top:0,zIndex:b.options.zIndex-2,opacity:0})}),b.$slides.eq(b.currentSlide).css({zIndex:b.options.zIndex-1,opacity:1})},b.prototype.setHeight=function(){var a=this;if(1===a.options.slidesToShow&&a.options.adaptiveHeight===!0&&a.options.vertical===!1){var b=a.$slides.eq(a.currentSlide).outerHeight(!0);a.$list.css("height",b)}},b.prototype.setOption=b.prototype.slickSetOption=function(){var c,d,e,f,h,b=this,g=!1;if("object"===a.type(arguments[0])?(e=arguments[0],g=arguments[1],h="multiple"):"string"===a.type(arguments[0])&&(e=arguments[0],f=arguments[1],g=arguments[2],"responsive"===arguments[0]&&"array"===a.type(arguments[1])?h="responsive":"undefined"!=typeof arguments[1]&&(h="single")),"single"===h)b.options[e]=f;else if("multiple"===h)a.each(e,function(a,c){b.options[a]=c});else if("responsive"===h)for(d in f)if("array"!==a.type(b.options.responsive))b.options.responsive=[f[d]];else{for(c=b.options.responsive.length-1;c>=0;)b.options.responsive[c].breakpoint===f[d].breakpoint&&b.options.responsive.splice(c,1),c--;b.options.responsive.push(f[d])}g&&(b.unload(),b.reinit())},b.prototype.setPosition=function(){var a=this;a.setDimensions(),a.setHeight(),a.options.fade===!1?a.setCSS(a.getLeft(a.currentSlide)):a.setFade(),a.$slider.trigger("setPosition",[a])},b.prototype.setProps=function(){var a=this,b=document.body.style;a.positionProp=a.options.vertical===!0?"top":"left","top"===a.positionProp?a.$slider.addClass("slick-vertical"):a.$slider.removeClass("slick-vertical"),void 0===b.WebkitTransition&&void 0===b.MozTransition&&void 0===b.msTransition||a.options.useCSS===!0&&(a.cssTransitions=!0),a.options.fade&&("number"==typeof a.options.zIndex?a.options.zIndex<3&&(a.options.zIndex=3):a.options.zIndex=a.defaults.zIndex),void 0!==b.OTransform&&(a.animType="OTransform",a.transformType="-o-transform",a.transitionType="OTransition",void 0===b.perspectiveProperty&&void 0===b.webkitPerspective&&(a.animType=!1)),void 0!==b.MozTransform&&(a.animType="MozTransform",a.transformType="-moz-transform",a.transitionType="MozTransition",void 0===b.perspectiveProperty&&void 0===b.MozPerspective&&(a.animType=!1)),void 0!==b.webkitTransform&&(a.animType="webkitTransform",a.transformType="-webkit-transform",a.transitionType="webkitTransition",void 0===b.perspectiveProperty&&void 0===b.webkitPerspective&&(a.animType=!1)),void 0!==b.msTransform&&(a.animType="msTransform",a.transformType="-ms-transform",a.transitionType="msTransition",void 0===b.msTransform&&(a.animType=!1)),void 0!==b.transform&&a.animType!==!1&&(a.animType="transform",a.transformType="transform",a.transitionType="transition"),a.transformsEnabled=a.options.useTransform&&null!==a.animType&&a.animType!==!1},b.prototype.setSlideClasses=function(a){var c,d,e,f,b=this;d=b.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden","true"),b.$slides.eq(a).addClass("slick-current"),b.options.centerMode===!0?(c=Math.floor(b.options.slidesToShow/2),b.options.infinite===!0&&(a>=c&&a<=b.slideCount-1-c?b.$slides.slice(a-c,a+c+1).addClass("slick-active").attr("aria-hidden","false"):(e=b.options.slidesToShow+a,
d.slice(e-c+1,e+c+2).addClass("slick-active").attr("aria-hidden","false")),0===a?d.eq(d.length-1-b.options.slidesToShow).addClass("slick-center"):a===b.slideCount-1&&d.eq(b.options.slidesToShow).addClass("slick-center")),b.$slides.eq(a).addClass("slick-center")):a>=0&&a<=b.slideCount-b.options.slidesToShow?b.$slides.slice(a,a+b.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false"):d.length<=b.options.slidesToShow?d.addClass("slick-active").attr("aria-hidden","false"):(f=b.slideCount%b.options.slidesToShow,e=b.options.infinite===!0?b.options.slidesToShow+a:a,b.options.slidesToShow==b.options.slidesToScroll&&b.slideCount-a<b.options.slidesToShow?d.slice(e-(b.options.slidesToShow-f),e+f).addClass("slick-active").attr("aria-hidden","false"):d.slice(e,e+b.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false")),"ondemand"===b.options.lazyLoad&&b.lazyLoad()},b.prototype.setupInfinite=function(){var c,d,e,b=this;if(b.options.fade===!0&&(b.options.centerMode=!1),b.options.infinite===!0&&b.options.fade===!1&&(d=null,b.slideCount>b.options.slidesToShow)){for(e=b.options.centerMode===!0?b.options.slidesToShow+1:b.options.slidesToShow,c=b.slideCount;c>b.slideCount-e;c-=1)d=c-1,a(b.$slides[d]).clone(!0).attr("id","").attr("data-slick-index",d-b.slideCount).prependTo(b.$slideTrack).addClass("slick-cloned");for(c=0;c<e;c+=1)d=c,a(b.$slides[d]).clone(!0).attr("id","").attr("data-slick-index",d+b.slideCount).appendTo(b.$slideTrack).addClass("slick-cloned");b.$slideTrack.find(".slick-cloned").find("[id]").each(function(){a(this).attr("id","")})}},b.prototype.interrupt=function(a){var b=this;a||b.autoPlay(),b.interrupted=a},b.prototype.selectHandler=function(b){var c=this,d=a(b.target).is(".slick-slide")?a(b.target):a(b.target).parents(".slick-slide"),e=parseInt(d.attr("data-slick-index"));return e||(e=0),c.slideCount<=c.options.slidesToShow?(c.setSlideClasses(e),void c.asNavFor(e)):void c.slideHandler(e)},b.prototype.slideHandler=function(a,b,c){var d,e,f,g,j,h=null,i=this;if(b=b||!1,(i.animating!==!0||i.options.waitForAnimate!==!0)&&!(i.options.fade===!0&&i.currentSlide===a||i.slideCount<=i.options.slidesToShow))return b===!1&&i.asNavFor(a),d=a,h=i.getLeft(d),g=i.getLeft(i.currentSlide),i.currentLeft=null===i.swipeLeft?g:i.swipeLeft,i.options.infinite===!1&&i.options.centerMode===!1&&(a<0||a>i.getDotCount()*i.options.slidesToScroll)?void(i.options.fade===!1&&(d=i.currentSlide,c!==!0?i.animateSlide(g,function(){i.postSlide(d)}):i.postSlide(d))):i.options.infinite===!1&&i.options.centerMode===!0&&(a<0||a>i.slideCount-i.options.slidesToScroll)?void(i.options.fade===!1&&(d=i.currentSlide,c!==!0?i.animateSlide(g,function(){i.postSlide(d)}):i.postSlide(d))):(i.options.autoplay&&clearInterval(i.autoPlayTimer),e=d<0?i.slideCount%i.options.slidesToScroll!==0?i.slideCount-i.slideCount%i.options.slidesToScroll:i.slideCount+d:d>=i.slideCount?i.slideCount%i.options.slidesToScroll!==0?0:d-i.slideCount:d,i.animating=!0,i.$slider.trigger("beforeChange",[i,i.currentSlide,e]),f=i.currentSlide,i.currentSlide=e,i.setSlideClasses(i.currentSlide),i.options.asNavFor&&(j=i.getNavTarget(),j=j.slick("getSlick"),j.slideCount<=j.options.slidesToShow&&j.setSlideClasses(i.currentSlide)),i.updateDots(),i.updateArrows(),i.options.fade===!0?(c!==!0?(i.fadeSlideOut(f),i.fadeSlide(e,function(){i.postSlide(e)})):i.postSlide(e),void i.animateHeight()):void(c!==!0?i.animateSlide(h,function(){i.postSlide(e)}):i.postSlide(e)))},b.prototype.startLoad=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.hide(),a.$nextArrow.hide()),a.options.dots===!0&&a.slideCount>a.options.slidesToShow&&a.$dots.hide(),a.$slider.addClass("slick-loading")},b.prototype.swipeDirection=function(){var a,b,c,d,e=this;return a=e.touchObject.startX-e.touchObject.curX,b=e.touchObject.startY-e.touchObject.curY,c=Math.atan2(b,a),d=Math.round(180*c/Math.PI),d<0&&(d=360-Math.abs(d)),d<=45&&d>=0?e.options.rtl===!1?"left":"right":d<=360&&d>=315?e.options.rtl===!1?"left":"right":d>=135&&d<=225?e.options.rtl===!1?"right":"left":e.options.verticalSwiping===!0?d>=35&&d<=135?"down":"up":"vertical"},b.prototype.swipeEnd=function(a){var c,d,b=this;if(b.dragging=!1,b.interrupted=!1,b.shouldClick=!(b.touchObject.swipeLength>10),void 0===b.touchObject.curX)return!1;if(b.touchObject.edgeHit===!0&&b.$slider.trigger("edge",[b,b.swipeDirection()]),b.touchObject.swipeLength>=b.touchObject.minSwipe){switch(d=b.swipeDirection()){case"left":case"down":c=b.options.swipeToSlide?b.checkNavigable(b.currentSlide+b.getSlideCount()):b.currentSlide+b.getSlideCount(),b.currentDirection=0;break;case"right":case"up":c=b.options.swipeToSlide?b.checkNavigable(b.currentSlide-b.getSlideCount()):b.currentSlide-b.getSlideCount(),b.currentDirection=1}"vertical"!=d&&(b.slideHandler(c),b.touchObject={},b.$slider.trigger("swipe",[b,d]))}else b.touchObject.startX!==b.touchObject.curX&&(b.slideHandler(b.currentSlide),b.touchObject={})},b.prototype.swipeHandler=function(a){var b=this;if(!(b.options.swipe===!1||"ontouchend"in document&&b.options.swipe===!1||b.options.draggable===!1&&a.type.indexOf("mouse")!==-1))switch(b.touchObject.fingerCount=a.originalEvent&&void 0!==a.originalEvent.touches?a.originalEvent.touches.length:1,b.touchObject.minSwipe=b.listWidth/b.options.touchThreshold,b.options.verticalSwiping===!0&&(b.touchObject.minSwipe=b.listHeight/b.options.touchThreshold),a.data.action){case"start":b.swipeStart(a);break;case"move":b.swipeMove(a);break;case"end":b.swipeEnd(a)}},b.prototype.swipeMove=function(a){var d,e,f,g,h,b=this;return h=void 0!==a.originalEvent?a.originalEvent.touches:null,!(!b.dragging||h&&1!==h.length)&&(d=b.getLeft(b.currentSlide),b.touchObject.curX=void 0!==h?h[0].pageX:a.clientX,b.touchObject.curY=void 0!==h?h[0].pageY:a.clientY,b.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(b.touchObject.curX-b.touchObject.startX,2))),b.options.verticalSwiping===!0&&(b.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(b.touchObject.curY-b.touchObject.startY,2)))),e=b.swipeDirection(),"vertical"!==e?(void 0!==a.originalEvent&&b.touchObject.swipeLength>4&&a.preventDefault(),g=(b.options.rtl===!1?1:-1)*(b.touchObject.curX>b.touchObject.startX?1:-1),b.options.verticalSwiping===!0&&(g=b.touchObject.curY>b.touchObject.startY?1:-1),f=b.touchObject.swipeLength,b.touchObject.edgeHit=!1,b.options.infinite===!1&&(0===b.currentSlide&&"right"===e||b.currentSlide>=b.getDotCount()&&"left"===e)&&(f=b.touchObject.swipeLength*b.options.edgeFriction,b.touchObject.edgeHit=!0),b.options.vertical===!1?b.swipeLeft=d+f*g:b.swipeLeft=d+f*(b.$list.height()/b.listWidth)*g,b.options.verticalSwiping===!0&&(b.swipeLeft=d+f*g),b.options.fade!==!0&&b.options.touchMove!==!1&&(b.animating===!0?(b.swipeLeft=null,!1):void b.setCSS(b.swipeLeft))):void 0)},b.prototype.swipeStart=function(a){var c,b=this;return b.interrupted=!0,1!==b.touchObject.fingerCount||b.slideCount<=b.options.slidesToShow?(b.touchObject={},!1):(void 0!==a.originalEvent&&void 0!==a.originalEvent.touches&&(c=a.originalEvent.touches[0]),b.touchObject.startX=b.touchObject.curX=void 0!==c?c.pageX:a.clientX,b.touchObject.startY=b.touchObject.curY=void 0!==c?c.pageY:a.clientY,void(b.dragging=!0))},b.prototype.unfilterSlides=b.prototype.slickUnfilter=function(){var a=this;null!==a.$slidesCache&&(a.unload(),a.$slideTrack.children(this.options.slide).detach(),a.$slidesCache.appendTo(a.$slideTrack),a.reinit())},b.prototype.unload=function(){var b=this;a(".slick-cloned",b.$slider).remove(),b.$dots&&b.$dots.remove(),b.$prevArrow&&b.htmlExpr.test(b.options.prevArrow)&&b.$prevArrow.remove(),b.$nextArrow&&b.htmlExpr.test(b.options.nextArrow)&&b.$nextArrow.remove(),b.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden","true").css("width","")},b.prototype.unslick=function(a){var b=this;b.$slider.trigger("unslick",[b,a]),b.destroy()},b.prototype.updateArrows=function(){var b,a=this;b=Math.floor(a.options.slidesToShow/2),a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&!a.options.infinite&&(a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false"),a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false"),0===a.currentSlide?(a.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true"),a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false")):a.currentSlide>=a.slideCount-a.options.slidesToShow&&a.options.centerMode===!1?(a.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")):a.currentSlide>=a.slideCount-1&&a.options.centerMode===!0&&(a.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")))},b.prototype.updateDots=function(){var a=this;null!==a.$dots&&(a.$dots.find("li").removeClass("slick-active").attr("aria-hidden","true"),a.$dots.find("li").eq(Math.floor(a.currentSlide/a.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden","false"))},b.prototype.visibility=function(){var a=this;a.options.autoplay&&(document[a.hidden]?a.interrupted=!0:a.interrupted=!1)},a.fn.slick=function(){var f,g,a=this,c=arguments[0],d=Array.prototype.slice.call(arguments,1),e=a.length;for(f=0;f<e;f++)if("object"==typeof c||"undefined"==typeof c?a[f].slick=new b(a[f],c):g=a[f].slick[c].apply(a[f].slick,d),"undefined"!=typeof g)return g;return a}});

/*! TT modules */
Modernizr.load({
    test: Modernizr.input.placeholder,
    nope: [
            'http://cdn.ttgtmedia.com/rms/ux/responsive/css/libs/placeholder_polyfill.min.css',
            'http://cdn.ttgtmedia.com/rms/ux/responsive/js/libs/placeholder_polyfill.jquery.min.js'
          ]
});
;(function ($) {

	'use strict';

	$.fn.setAllToMaxHeight = function(){
		return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) );
	};

})(jQuery);
/*
 * Plugin: Get Video Thumbnail
 * Version: 1.0 (May 21 2012)
 * Description: retrieves urls for thumbnail images used by videos
 * Author: Morgan Wigmanich, TechTarget
 * Options: none
 * Dependencies: none
 */
;(function ($) {

	'use strict';

	$.fn.getVideoThumbnail = function () {

		// thumbnail service config
		var config = {
			apiName: 'Brightcove Media API', // http://docs.brightcove.com/en/media/
			apiUrl: 'http://api.brightcove.com/services/library',
			apiKey: 'G8thULsWpbbOlMthhkzc6qf31MLILp5SuJSSpsFUcP0.'
		};

		// parameters to be sent to the Brightcove api to retreive thumbnail (video_id must be set before query)
		var requestParams = {
			command: 'find_videos_by_ids',
			token: config.apiKey,
			video_ids: '',
			video_fields: 'thumbnailURL,videoStillURL'
		};

		function log(m) {
			return window.console && window.console.log && window.console.log(m);
		}

		// an array of ids to batch query the api
		var ids = [];

		// filter set to find only the images we need to query the api for thumbnails
		var needThumbnails = this.filter(function () {

			// cache element
			var img = $(this);

			// video id
			var id = img.data('videoId');

			// check that this plugin is operating on an img element
			// and that it also has a data-attr of 'data-video-id'
			// and that the data-attr is not empty
			if (img.is('img') && typeof id !== 'undefined' && id !== '') {

				// sometimes the video id will be a qualified url which will work as is
				// so check if video id is a url and if so, don't return this to the filtered set
				if (id.toString().substr(0, 4) === 'http') {

					// copy url into the src attr and display image
					img.attr('src', id).css('display', 'block');

				} else {

					// add id to request array
					ids.push(id);

					// return element to set
					return img;

				}

			}

		});

		// if there are any images that need urls make request to api
		if (ids.length > 0) {

			// parse the returned json and add image videoStillURL urls to their respective src attrs
			var setThumbnailUrls = function (json) {

				needThumbnails.map(function (i) {

					// if there's an incorrect id, api returns null
					if (json.items[i] !== null) {

						// set the img src to the url returned from the api and display
						$(this).attr('src', decodeURIComponent(json.items[i].videoStillURL)).css('display', 'block')
							// wrap with a "holder" div so the image is cropped instead of squashed
							.wrap('<div class="videoThumbnailHolder"></div>');

					} else {

						// log error when there's a seemingly incorrect image id
						log( config.apiName + ' is returning ' + json.items[i] + ' for this image ID -> ' + $(this).data('videoId'), 'error');

					}

				});

			};

			// convert array to csv string and store in request params
			requestParams.video_ids = ids.toString();

			// query the brightcove service api to retrieve thumbnail and video still urls and copy that into src
			var request = $.ajax({
				url: config.apiUrl,
				contentType: 'application/json; charset=utf-8',
				dataType: 'jsonp',
				data: requestParams
			});

			request.done(function (data) {

				// if data isn't null and there are no errors, pass it to function to set the thumbnail images
				if (data && !data.error) {
					setThumbnailUrls(data);
				} else if (data.error) {
					log('Error connecting to ' + config.apiName + '. Error: ' + data.error);
				}

			});

			request.fail(function (jqXHR, textStatus) {
				log('Error connecting to ' + config.apiName + '. Status: ' + textStatus);
			});

		}

	};

})(jQuery);
/*!
 * Scroll Sneak
 * http://mrcoles.com/scroll-sneak/
 *
 * Copyright 2010, Peter Coles
 * Licensed under the MIT licenses.
 * http://mrcoles.com/media/mit-license.txt
 *
 * Date: Mon Mar 8 10:00:00 2010 -0500
 */
var ScrollSneak = function(prefix, wait) {
    // clean up arguments (allows prefix to be optional - a bit of overkill)
    if (typeof(wait) == 'undefined' && prefix === true) prefix = null, wait = true;
    prefix = (typeof(prefix) == 'string' ? prefix : window.location.host).split('_').join('');
    var pre_name;

    // scroll function, if window.name matches, then scroll to that position and clean up window.name
    this.scroll = function() {
        if (window.name.search('^'+prefix+'_(\\d+)_(\\d+)_') == 0) {
            var name = window.name.split('_');
            window.scrollTo(name[1], name[2]);
            window.name = name.slice(3).join('_');
        }
    }
    // if not wait, scroll immediately
    if (!wait) this.scroll();

    this.sneak = function() {
	// prevent multiple clicks from getting stored on window.name
	if (typeof(pre_name) == 'undefined') pre_name = window.name;

	// get the scroll positions
        var top = 0, left = 0;
        if (typeof(window.pageYOffset) == 'number') { // netscape
            top = window.pageYOffset, left = window.pageXOffset;
        } else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) { // dom
            top = document.body.scrollTop, left = document.body.scrollLeft;
        } else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) { // ie6
            top = document.documentElement.scrollTop, left = document.documentElement.scrollLeft;
        }
	// store the scroll
        if (top || left) window.name = prefix + '_' + left + '_' + top + '_' + pre_name;
        return true;
    }
}
/*
 * Plugin: TabList
 * Author: Morgan Wigmanich, TechTarget
 * Version: 1.3 (April 3 2012)
 * Description: a basic tabs plugin; call it on a list of links (tabs) and pass in another jquery object of the tab content (ie. the html that will be show or hidden by the tabs)
 * Options:
 *      tabContent: object; a *required* jquery object that contains the tab content (the items that are being hidden/revealed)
 *      icon: boolean; if true, appends a span element that can be used for an arrow or other icon to indicate selected tab
 *      callback: function; optional function fired on tab select event
 *      mouseEvent: string; which mouse event will trigger tab selection
 *      autoPlay: boolean; this will automatically rotate through the tabs
 *      autoPlaySpeed: integer; the speed at which the autoplay will rotate through the items (in ms)
 * Dependencies: none
 */
;(function ($) {

	'use strict';

	$.fn.tabList = function (options) {

		var defaults = {
			tabContent: '',
			icon: true,
			callback: function () {},
			mouseEvent: 'click',
			autoPlay: false,
			autoPlaySpeed: 5000
		};
		var o = $.extend(defaults, options);

		return this.each(function () {

			var tabs = $(this).children('li');
			var tabsCount = tabs.length;
			var tabLinks = tabs.children('a');
			var tabContent = o.tabContent;
			var currentTab = 0;
			var selectedTab;
			var timer;

			// do tabby stuff
			var tabify = function (el) {

				selectTab(el);
				showContent(String(el.hash));

				// optional callback function
				o.callback.call(this);

			};

			// add 'selected' class to new tab and remove it from old tab
			var selectTab = function (el) {

				tabs.filter('.selected').removeClass('selected');
				$(el).parent('li').addClass('selected');

			};

			// if one of the tab links has a hash of 'all' then display every piece of content, otherwise display the selected tabs content
			var showContent = function (tabHash) {

				if (tabHash === '#all') {
					tabContent.show();
				} else {
					tabContent.hide().filter(tabHash).fadeIn('fast');
				}

			};

			// timer function for autoplay
			var autoPlayTimer = function () {
				timer = setTimeout(function () {
					autoPlay();
				}, o.autoPlaySpeed);
			};

			// check which tab is selected and select next sibling
			var autoPlay = function () {

				if (currentTab < tabsCount - 1) {
					currentTab++;
				} else {
					currentTab = 0;
				}
				tabify(tabLinks.get(currentTab));
				autoPlayTimer();

			};

			// autoplay init
			if (o.autoPlay) {
				if (tabsCount > 1) {
					autoPlayTimer();
				}
			}

			// event handler for tabs
			tabLinks.on(o.mouseEvent, function (e) {

				e.preventDefault();
				tabify(this);

				// if autoplay is enabled, restart it
				if (o.autoPlay) {
					clearTimeout(timer);
					autoPlayTimer();
					currentTab = tabLinks.index(this);
				}

			});

			// add a span element if icon option set
			if (o.icon) {
				tabLinks.append('<span></span>');
			}

			// check if any of the tabs have a selected class and if so, display that tab content; otherwise select the first tab
			if (tabs.hasClass('selected')) {
				selectedTab = tabs.filter('.selected').children('a');
				currentTab = tabLinks.index(selectedTab);
				showContent(selectedTab.attr('href'));
			} else {
				tabs.first().addClass('selected');
			}

		});

	};

})(jQuery);
/*
    Thumbelina Content Slider
    V1.0 Rev 1302190900

    A lightweight horizontal and vertical content slider designed for image thumbnails.
    http://www.starplugins.com/thumbelina

    Developed by Star Plugins
    http://www.starplugins.com

    Copyright 2013, Star Plugins
    License: GNU General Public License, version 3 (GPL-3.0)
    http://www.opensource.org/licenses/gpl-3.0.html
*/
;(function($) {
    $.fn.Thumbelina = function(settings) {
        var $container = this,      // Handy reference to container.
            $list = $('ul',this),   // Handy reference to the list element.
            moveDir = 0,            // Current direction of movement.
            pos = 0,                // Current actual position.
            destPos = 0,            // Current destination position.
            listDimension = 0,      // Size (width or height depending on orientation) of list element.
            idle = 0,
            outerFunc,
            orientData              // Stores function calls and CSS attribute for horiz or vert mode.
        
        // Add thumblina CSS class, and create an inner wrapping container, within which the list will slide with overflow hidden.
        $list.addClass('thumbelina').wrap('<div style="position:absolute;overflow:hidden;width:100%;height:100%;">');
        // Create settings by merging user settings into defaults.
        settings = $.extend({}, $.fn.Thumbelina.defaults, settings);
        
        // Depending on vertical or horizontal, get functions to call and CSS attribute to change.
        if(settings.orientation == 'vertical') 
            orientData = {outerSizeFunc:  'outerHeight', cssAttr: 'top', display: 'block'};
        else
            orientData = {outerSizeFunc:  'outerWidth', cssAttr: 'left', display: 'inline-block'};
       
        // Apply display type of list items.
        $('li',$list).css({display: orientData.display});
        
        // Function to bind events to buttons.
        var bindButEvents = function($elem,dir) {
            $elem.bind('mousedown mouseup touchend touchstart',function(evt) {
                if (evt.type=='mouseup' || evt.type=='touchend') moveDir = 0;
                else moveDir = dir;
                return false;
            });
        };
        
        // Bind the events.
        bindButEvents(settings.$bwdBut,1);
        bindButEvents(settings.$fwdBut,-1);
        
        // Store ref to outerWidth() or outerHeight() function.
        outerFunc = orientData.outerSizeFunc; 
   
        // Function to animate. Moves the list element inside the container.
        // Does various bounds checks.
        var animate = function() {
            var minPos;
            
            // If no movement or resize for 100 cycles, then go into 'idle' mode to save CPU.
            if (!moveDir && pos == destPos && listDimension == $container[outerFunc]() ) {  
                idle++;
                if (idle>100) return;
            }else {
                 // Make a note of current size for idle comparison next cycle.
                listDimension = $container[outerFunc]();
                idle = 0;
            }
          
            // Update destination pos.
            destPos += settings.maxSpeed * moveDir;
     
            // Work out minimum scroll position.
            // This will also cause the thumbs to drag back out again when increasing container size.
            minPos = listDimension - $list[outerFunc]();
            
          
            // Minimum pos should always be <= 0;
            if (minPos > 0) minPos = 0;
            // Bounds check (maximum advance i.e list moving left/up)
            if (destPos < minPos) destPos = minPos;
            // Bounds check (maximum retreat i.e list moving right/down)
            if (destPos>0) destPos = 0;
            
            // Disable/enable buttons depending min/max extents.
            if (destPos == minPos) settings.$fwdBut.addClass('disabled');
            else settings.$fwdBut.removeClass('disabled');
            if (destPos == 0) settings.$bwdBut.addClass('disabled');
            else settings.$bwdBut.removeClass('disabled');
            
            // Animate towards destination with a simple easing calculation.
            pos += (destPos - pos) / settings.easing;
            
            // If within 1000th of a pixel to dest, then just 'snap' to exact value.
            // Do this so pos will end up exactly == destPos (deals with rounding errors).
            if (Math.abs(destPos-pos)<0.001) pos = destPos;
            
            $list.css(orientData.cssAttr, Math.floor(pos));
        }
        
        setInterval(function(){
            animate();
        },500/60);
    }
    
    $.fn.Thumbelina.defaults = {
        orientation:    "horizontal",   // Orientation mode, horizontal or vertical.
        easing:         8,              // Amount of easing (min 1) larger = more drift.
        maxSpeed:       5,              // Max speed of movement (pixels per cycle).
        $bwdBut:   null,                // jQuery element used as backward button.
        $fwdBut:    null                // jQuery element used as forward button.
    }
    
})(jQuery);

// UI main module
TT("ui/main", ["context", "context/ui", "lib/jquery"], function (ctx, ui, $, require) {

	// can't make this a proper dependency because it will be circular
	// ui/utils has a dependency on main. defer using require in init.
	var utils;

	var TT = {
		// Tracks the view state, or "active" media query of the app
		mq: 320,

		// Listing of different breakpoints used in the project
		// TT.mq is compared against these values to determine which
		// view state the app is in
		mqs: {
			mobile: 320,
			tablet: 640,
			desktop: 960,
			desktop_w: 1280
		},

		$body: $('body'),

		click: 'click',

		init:function(){
			// deferred to fix a circular dependency between main an utils
			utils = require("../utils");

			// Set the initial state of the app (320,480,760,1120)
			utils.setMq();

			// Swap in HighRes images for devices with higher resolution
			utils.swapInHighResImgs();

			// Apply image credits
			utils.imageCredits();

			// Resize search results topics next to tall ad
			utils.resizeSRT();

			// Setup photostory thumb slider
			utils.setupPhotostory();

			// Set up E-Handbook special footer
			utils.setupEHandbook();

			// If on new and notable, move box ad on topics pages at 960 breakpoint
			if ($('.new-notable').length > 0) {
				utils.moveTopicsBoxAd();
			}

			// Bind Global Event Handlers
			this.bindEventHandlers();

			// Make sure Icon Font appears properly in Internet Explorer
			utils.fixPseudoIE();

			// Freeze Leaderboard ad for 5 seconds
			utils.freezeLeaderboard();

			// Set click event to either 'tap' or 'click'
			this.getClickEvent();

			// Set up Related Terms module if it exists
			if($('.related-terms-all-wrapper').length > 0){
				this.setupRelatedTermsModule();
			}

			// Set up Pro Features module if it exists
			if ($('.pro-features-wrapper').length && $('.pro-features-content').length) {
				this.setupProFeaturesModule();
			}

			// Set up Discussion module if it exists
			if ($('.join-discussion-wrapper').length && $('.join-discussion-content').length) {
				this.setupDiscussionModule();
			}

			// Set up Homepage module if it exists
			if ($('.learn-more-resources').length || $('.from-across-definitions').length) {
				this.setupHomepageModules();
			}

			// Add mask fade to bottom of infotype topics sidebar
			if($('.infotype-desktop-chapters-bar').length > 0) {
				$('.infotype-desktop-chapters-bar').append("<div class='chapters-bar-item-mask'></div>");
			}

			if($('.infotype-desktop-chapters-bar, #resourcesChild .desktop-chapters-bar').length > 0){

				$(function() {
					var topicsList = $('.infotype-desktop-chapters-bar .chapters-bar-list, #resourcesChild .desktop-chapters-bar .chapters-bar-list');

					topicsList.jScrollPane();

					var scrollTo = $('.chapters-bar-item-link.selected');
					if (scrollTo.length > 0) {
						var api = topicsList.data('jsp');
						var yPos = scrollTo.eq(0).position();
						api.scrollTo(0, yPos.top - 100);
					}
				});
			}

			// Add Search TechTarget to fixed scrolling search bar
			$('.nav-list').append('<li class="desktop-fixed-search">' + ctx.i18n('navbar.search_default_text') + '</li>');

			// Dig Deeper tabs
			$('#dig-deeper-tabs').tabList({
				tabContent: $('#dig-deeper-tabs-content').children('div')
			});

			// Show float mask for inline reg
			if ($('#inlineRegistrationWrapper').length > 0) {
				$('.float-mask').css('display', 'block');
			}

			// makes an ajax call to Brightcove to get thumbnail images for a video
			$('.getVideoThumbnail').getVideoThumbnail();

			// if podcast player exists on page, get mediaelementjs and fire
			if ($('.podcastdownload').length > 0) {
				$.getScript('http://cdn.ttgtmedia.com/rms/ux/responsive/js/libs/mediaelement-and-player.min.js', function () {
					$('audio').mediaelementplayer({pluginPath: 'http://cdn.ttgtmedia.com/rms/ux/responsive/flash/'});
				});
			}

			// if podcast player v2 exists on page, get mediaelementjs and fire
			if ($('.podcastdownload-new').length > 0) {
				$.getScript('http://cdn.ttgtmedia.com/rms/ux/responsive/js/libs/mediaelement-and-player.min.js', function () {
					$('audio').mediaelementplayer({pluginPath: 'http://cdn.ttgtmedia.com/rms/ux/responsive/flash/'});
				});
			}

			// remove show more from guide parent experts page if 8 or less experts
			if ($('.experts-list li').length < 9) {
					$('.experts-section-content').removeClass('closed').css('padding-bottom', '0');
					$('.experts-section-content .collapse-toggle').css('display', 'none');
			}

			// remove show more from blogroll if 10 or less total blogs
			if ($('.multimedia-sidebar-list.blogroll li').length < 12) {
					$('.multimedia-sidebar-list.blogroll').removeClass('closed').css('padding-bottom', '0');
					$('.multimedia-sidebar-list.blogroll .collapse-toggle').css('display', 'none');
			}

			// move second search result topic to after first six results, or bottom if less than four results
			if ($('.search-result').length < 4) {
				$('.second-topic').insertAfter('.search-result:last').css('display', 'block');
			} else {
				$('.second-topic').insertAfter('.search-result:eq(3)').css('display', 'block');
			}

			// move cw sidebar splash images into ezine sidebar
			if ($('.sidebar-splashes').length > 0) {
				$('.sidebar-splashes').appendTo('.latest-news-sidebar').css('display', 'block');
			}

			// move discussions splash on photostory page
			if ($('#photostory-detail .join-discussion-wrapper').length > 0) {
				$('#photostory-detail .join-discussion-wrapper').appendTo('.photostory-about').css('display', 'block');
			}

			// move discussions and transcript splash on video page
			if ($('#video-detail .join-discussion-wrapper').length > 0) {
				$('#video-detail .join-discussion-wrapper').appendTo('.video-about').css('display', 'block');
			}

			// move discussions and transcript splash on podcast page
			if ($('#podcast-detail .join-discussion-wrapper').length > 0) {
				$('#podcast-detail .join-discussion-wrapper').appendTo('.podcast-about').css('display', 'block');
			}

			// move related cartoons splash on cartoon page
			if ($('#cartoon-detail .cartoon-more').length > 0) {
				$('#cartoon-detail .cartoon-more').appendTo('.cartoon-about').css('display', 'block');
			}

			if ($('#video-detail .video-transcript-content').length > 0) {
				$('#video-detail .video-transcript-content').appendTo('.video-about').css('display', 'block');
			}

			if ($('#podcast-detail .podcast-transcript-content').length > 0) {
				$('#podcast-detail .podcast-transcript-content').appendTo('.podcast-about').css('display', 'block');
			}

			// video transcript length toggle
			if ($('.video-transcript-content').length > 0) {
				if ($('.video-transcript-content').height() > 50) {
					$('.video-transcript-content .collapse-toggle').css('display', 'block');
				} else {
					$('.video-transcript-content .collapse-toggle').css('display', '');
				}
			}

			// podcast transcript length toggle
			if ($('.podcast-transcript-content').length > 0) {
				if ($('.podcast-transcript-content').height() > 50) {
					$('.podcast-transcript-content .collapse-toggle').css('display', 'block');
				} else {
					$('.podcast-transcript-content .collapse-toggle').css('display', '');
				}
			}

			// move view all button on video page
			$('#video-detail .video-about > .button').appendTo('.related-video-bar');

			// move view all button on podcast page
			$('#podcast-detail .podcast-about > .button').appendTo('.related-podcast-bar');

			// move CW refinery header from content to wrapper so height is not counted
			$('.read-more-header').prependTo('.read-more-wrapper');

			// setup multimedia pages
			if ($('.multimedia-section').length > 0) {
				$('.multimedia-section-inner, .multimedia-sidebar').appendTo('.multimedia-section.main-section').css('display', 'block');
				$('.multimedia-sidebar-item, .page-header-ad-tall').appendTo('.multimedia-sidebar').css('display', 'block');
			}

			// creating mobile sidebars from desktop sidebars
			$('.chapters-bar.desktop-chapters-bar').clone().toggleClass('desktop-chapters-bar mobile-chapters-bar').appendTo($('.mobile-sidebars'));
			$('.resources-bar.desktop-resources-bar').clone().toggleClass('desktop-resources-bar mobile-resources-bar').appendTo($('.mobile-sidebars'));
			$('.mobile-sidebars .infotype-desktop-chapters-bar').toggleClass('infotype-desktop-chapters-bar infotype-mobile-chapters-bar');

			$('.browse-defs-bar, .search-defs-bar').clone().addClass('mobile-def-listing-bar').appendTo($('.mobile-sidebars'));

			// microsite promo page dropdown
			$('.micrositeWhatIsThis').on('click',function(){
				$('i',this).attr('data-icon',($('i',this).attr('data-icon') == '3' ? '5' : '3'));
				$('.micrositeWhatIsThis-dropDownPanel').slideToggle(200);
			});

			// sdef 3 step reg
			$("#expand-ctd").on('click',function(){
				$("#expand-ctd").hide();
				$(".sign-up-form.cascading-reg-form.page1, .sign-up-fine-print, .safeHarbor").show();
			});

			// dlo with no image, reduce header height
			if ($('.sign-up-wrapper.cascading-reg .sign-up-header .reg-cover').length < 1) {
				$('.sign-up-wrapper.cascading-reg .sign-up-header').css('min-height', '0');
			}

			// dlo topic convert label to placeholder
			$('#topicLeaf .sign-up-wrapper.cascading-reg form label').each(function() {
				var label = $(this);
				var placeholder = label.text();
				label.closest('li').find('input').attr('placeholder', placeholder);
			});

			// dlo topic apply class to download button
			$('#topicLeaf .sign-up-wrapper.cascading-reg form .sign-up-btn').closest('li').addClass('button');

			// move related sidebar to the right at 1280 when itcs sidebar is on page
			if ($('.desktop-itcs-bar').length > 0 && $('#sdef .desktop-itcs-bar').length < 1) {
				$('.desktop-related-bar').appendTo('.desktop-resources-bar');
			}

			$('.section.definition-section').next('p').css('margin-top','0');

			// prevent wrapping on guide subtitles with images
			if ($('.main-article-subtitle img').length > 0) {
				$('.main-article-subtitle').contents().filter(function() {
					return this.nodeType === 3;
				}).wrap('<p>');
			}

			// social icons modal window
			$(".inline-social-icons").colorbox({
				inline:true,
				opacity:0.6,
				width:'300px',
				height:'150px', 
				onComplete:function(){$('#cboxTitle').hide();}
			});
			
			// tss home modal window
			$('.from-the-community-wrapper .grey-button, .from-the-community-wrapper .submit-story').on(TT.click, function(e){
				e.preventDefault();
			});

			$(".from-the-community-wrapper .grey-button, .from-the-community-wrapper .submit-story").colorbox({iframe:true, width:"630px", height:"90%"});

			$('.modal-register, .modal-login').hover(function() {
				$(this).addClass('hovered');
			}, function() {
				$(this).removeClass('hovered');
			});

			$('.modal-register input, .modal-login input').focus(function() {
				$(this).parent().addClass('focused');
			});

			$('.modal-register input, .modal-login input').blur(function() {
				$(this).parent().removeClass('focused');
			});

			// whatis search result expand/collapse
			$(".search-related-button").on('click',function(){
				$(this).parents().toggleClass('open');
				$(this).find('.icon').attr('data-icon', function(index, attr){
					return attr == '5' ? 'A' : '5';
				});
			});

			// whatis homepage tabs
			$('ul.tt-tabs li').click(function(){
				var tab_id = $(this).attr('data-tab');

				$('ul.tt-tabs li').removeClass('current');
				$('.tt-tab-content').removeClass('current');

				$(this).addClass('current');
				$("#"+tab_id).addClass('current');
			});

			// whatis homepage signup
			$(".whatis-wotd-home button.blue-button").on('click',function(){
				$(this).hide();
				$('.whatis-wotd-home .archive-link').hide();
				$('.whatis-wotd-home-form').css('display','block');
			});

			// whatis site detail move recently to header
			if ($('#siteDetail .recently-on-site').length > 0) {
				$('#siteDetail .recently-on-site').appendTo('.site-detail-header').css('display','block');
			}

			// infotype tooltip
			$('.infotype-eyebrow').hover(function() {
				$('.infotype-eyebrow-tooltip').css('display','block');
			}, function() {
				$('.infotype-eyebrow-tooltip').css('display','none');
			});

			// header v2 border fix
			if ($('.nav-list-item.nav-list-item-sections').length < 1) {
				$('.nav-list-item.nav-list-item-topics .nav-list-item-header-title, .nav-list-item.nav-list-item-topics .nav-list-item-header').css('border','0');
			}

			// definition listing spacing
			if ($('.section.definition-listing').length > 0) {
				$('.section.definition-listing ul').children(':last').addClass('last');
				
				var listItem = $('.section.definition-listing ul li');
				var listItemFirst = $('.section.definition-listing ul li:first-child');
				listItem.each(function() {
					if ($(this).find('.letter').length > 0) {
						$(this).prev('li').addClass('separator');
					}
				});
				listItemFirst.each(function() {
					if ($(this).find('.letter').length > 0) {
						$(this).parents().prevAll('ul').eq(0).find('li:last-child').addClass('separator');
					}
				});
			}

			$('.eHandbookCarousel').slick({
				arrows: false,
				infinite: false,
				slidesToShow: 4,
				slidesToScroll: 1,
				prevArrow: "<button type=\"button\" class=\"slick-prev\"><i class=\"icon\" data-icon=\"A\"></i></button>",
				nextArrow: "<button type=\"button\" class=\"slick-next\"><i class=\"icon\" data-icon=\"A\"></i></button>",
				responsive: [
				{
					breakpoint: 1280,
					settings: {
						arrows: true,
						slidesToShow: 3,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 960,
					settings: {
						arrows: true,
						slidesToShow: 2,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 640,
					settings: {
						arrows: true,
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}]
			});

			var currentSlide = $('.currentItem').index();

			if (TT.mq == 320) {
				if (currentSlide == $('.eHandbookNavItem').length - 1) {
					$('.eHandbookCarousel').slick('slickGoTo', currentSlide + 1);
					$('.upnext').css({'visibility':'hidden'});
				} else {
					$('.eHandbookCarousel').slick('slickGoTo', currentSlide + 1);
				}
			}

			var item = $('.eHandbookNavItem');
			
			for (var n = 0; n < item.length; n++) {
				if ($(item[n]).hasClass('currentItem')) {
					if (n == item.length - 1) {
						$('.eHandbookCarousel').slick('slickGoTo', n - 1);
						$('.eHandbookCarousel').slick('slickNext');
					} else if (n == item.length - 2) {
						$('.eHandbookCarousel').slick('slickGoTo', n - 1);
					} else {
						$('.eHandbookCarousel').slick('slickGoTo', n);
					}
				}
			}

		},


		bindEventHandlers:function(){
			ui.on("layoutChange", function(){
				utils.setMq();
				$('.infotype-desktop-chapters-bar .chapters-bar-list').jScrollPane();
				if ($('.new-notable').length > 0) {
					utils.moveTopicsBoxAd();
				}
			}).on("load", function() {
				utils.articleImageMasks();

			}).on("load layoutChange",function(){
				$('.news-archive-list, #defListing .desktop-chapters-bar .chapters-bar-list, #defTopic .desktop-chapters-bar .chapters-bar-list').jScrollPane();

				if (TT.mq === TT.mqs.mobile) {
					if ($('#photostory-detail .main-article-author-contact').children().length > 2) {
						$('.main-article-author-info').css('width','100%');
						$('.main-article-author-contact').css('float','left');
					}

					if ($('#video-detail .main-article-author-contact').children().length > 2) {
						$('.main-article-author-info').css('width','100%');
						$('.main-article-author-contact').css('float','left');
					}

					if ($('#cartoon-detail .main-article-author-contact').children().length > 2) {
						$('.main-article-author-info').css('width','100%');
						$('.main-article-author-contact').css('float','left');
					}

					if ($('#podcast-detail .main-article-author-contact').children().length > 2) {
						$('.main-article-author-info').css('width','100%');
						$('.main-article-author-contact').css('float','left');
					}
				}

				if(TT.mq > TT.mqs.mobile) {
					if ($('.network-hub-sites-content').length > 0) {
						var hubSitesList = $('.network-hub-sites-content');

						hubSitesList.each(function() {
							var thisSitesList = $(this).find('.network-hub-list-sites');
							if (thisSitesList.height() > 210) {
								$(this).find('.collapse-toggle').css('display', 'block');
							} else {
								$(this).find('.collapse-toggle').css('display', '');
							}
						});
					}
				} else {
					$('.network-hub-sites-content .collapse-toggle').css('display', '');
				}

				if(TT.mq == TT.mqs.tablet) {
					$('.cw-article-image').prependTo('.main-article-author');
				} else if (TT.mq == TT.mqs.desktop || TT.mq == TT.mqs.desktop_w) {
					$('.cw-article-image').prependTo('.locking-left-bar');
				} else {
					$('.cw-article-image').appendTo('.main-article-header');
				}

				if(TT.mq == TT.mqs.desktop) {
					$('#photostory-detail .main-article-author').prependTo('.content-left .locking-full-bar .locking-left-bar');
					$('#photostory-detail .share-bar-desktop').insertAfter('.content-left .desktop-related-bar');
					$('#video-detail .main-article-author').prependTo('.content-left .locking-full-bar .locking-left-bar');
					$('#video-detail .share-bar-desktop').insertAfter('.content-left .desktop-related-bar');
					$('#cartoon-detail .main-article-author').prependTo('.content-left .locking-full-bar .locking-left-bar');
					$('#cartoon-detail .share-bar-desktop').insertAfter('.content-left .desktop-related-bar');
					$('#podcast-detail .main-article-author').prependTo('.content-left .locking-full-bar');
				} else if (TT.mq == TT.mqs.desktop_w) {
					$('#photostory-detail .main-article-author').prependTo('.content-left .locking-left-bar');
					$('#photostory-detail .share-bar-desktop').insertAfter('.content-left .desktop-related-bar');
					$('#video-detail .main-article-author').prependTo('.content-left .locking-left-bar');
					$('#video-detail .share-bar-desktop').insertAfter('.content-left .desktop-related-bar');
					$('#cartoon-detail .main-article-author').prependTo('.content-left .locking-left-bar');
					$('#cartoon-detail .share-bar-desktop').insertAfter('.content-left .desktop-related-bar');
					$('#podcast-detail .main-article-author').prependTo('.content-left .locking-left-bar');
				} else {
					$('#photostory-detail .main-article-author').insertAfter('.main-article-share-counts');
					$('#photostory-detail .share-bar-desktop').insertBefore('.content-left .desktop-related-bar');
					$('#video-detail .main-article-author').insertAfter('.main-article-share-counts');
					$('#video-detail .share-bar-desktop').insertBefore('.content-left .desktop-related-bar');
					$('#cartoon-detail .main-article-author').insertAfter('.main-article-share-counts');
					$('#cartoon-detail .share-bar-desktop').insertBefore('.content-left .desktop-related-bar');
					$('#podcast-detail .main-article-author').insertAfter('.main-article-share-counts');
				}

				if (TT.mq == TT.mqs.desktop_w) {
					$('#blog-post .main-article-author').prependTo('.content-left .locking-left-bar').css('display','block');
				} else {
					$('#blog-post .main-article-author').appendTo('.main-article-header');
				}

				if(TT.mq < TT.mqs.desktop) {
					if ($('.multimedia-sidebar').length > 0) {
						$('.multimedia-sidebar').css('display','none');
					}
					$('.related-video-bar').appendTo('.video-about').css('display','block');
					$('.related-podcast-bar').appendTo('.podcast-about').css('display','block');
					$('.view-by-topics-item').appendTo('.multimedia-section-inner');
					$('.blogroll-item').appendTo('.multimedia-section-inner');
				} else {
					$('.multimedia-sidebar').css('display','block');
					$('.related-video-bar').insertAfter('.main-article-author');
					$('.related-podcast-bar').insertAfter('.main-article-author');
					$('.view-by-topics-item').insertBefore('.latest-news-item');
					$('.blogroll-item').insertBefore('.latest-news-item');
				}

				// sdef update
				if (TT.mq === TT.mqs.desktop)
				{
					$('.resources-bar.desktop-resources-bar .ribbon-wrapper').prependTo('.actions-bar');
				} else {
					$('.actions-bar .ribbon-wrapper').prependTo('.resources-bar.desktop-resources-bar');
				}

				if (TT.mq === TT.mqs.desktop_w)
				{
					// move content up on updated sdef pages
					$('#sdef .desktop-resources-bar .share-bar-desktop').appendTo('.definition-header').css('display','block');
					$('.definition-authors').insertAfter('.definition-title').css('display','block');

					var defHeaderHeight = $('#sdef .definition-header').height() - $('#sdef .definition-title').height() - $('#sdef .definition-guide-subtitle').outerHeight() - 15;
					$('#sdef .section.definition-section').css('margin-top', -defHeaderHeight);
				} else {
					$('#sdef .definition-header .share-bar-desktop').css('display', 'none');
					$('.definition-authors').appendTo('.definition-header').css('display', '');

					$('#sdef .section.definition-section').css('margin-top', '');
				}

				// share bar update
				if (TT.mq < TT.mqs.desktop)
				{
					$('.locking-left-bar .share-bar-desktop').prependTo('.actions-bar').css('display','block');
				} else {
					$('.share-bar-desktop').appendTo('.locking-left-bar').css('display', 'block');
				}

				// sdef update v2
				if (TT.mq < TT.mqs.desktop_w)
				{
					$('#sdef .share-bar-desktop').prependTo('.actions-bar').css('display','block');
				} else {
					$('#sdef .share-bar-desktop').appendTo('.definition-header').css('display', 'block');
					
					// scrolls the new share bar, necessary without having to rework template
					if ($('#sdef .share-bar-desktop').length > 0) {
						$(function() {
							var top = $('#sdef .share-bar-desktop').offset().top - parseFloat($('#sdef .share-bar-desktop').css('marginTop').replace(/auto/, 0)) - 190;
							var commentsTop = $('.main-content.comments-wrapper').offset().top - parseFloat($('.main-content.comments-wrapper').css('marginTop').replace(/auto/, 0));

							var maxY = commentsTop - $('#sidebar').outerHeight();

							$(window).scroll(function(evt) {
								var y = $(this).scrollTop();
								if (y > top) {
									if (y < maxY) {
										$('#sdef .share-bar-desktop').addClass('fixed').removeAttr('style');
									} else {
										$('#sdef .share-bar-desktop').removeClass('fixed').css({
											position: 'absolute',
											top: (maxY - top) + 'px'
										});
									}
								} else {
									$('#sdef .share-bar-desktop').removeClass('fixed');
								}
							});
						});
					}
				}

				// whatis def sidebar
				if (TT.mq < TT.mqs.desktop_w)
				{
					$('#sdef .desktop-resources-bar .wotd-bar, .desktop-resources-bar .newest-terms-bar').insertAfter('#sdef .main-content.comments-wrapper').wrapAll('<div class="sidebar-wrapper-lower"></div>').css('display','block');
					$('#search .desktop-resources-bar .wotd-bar, .desktop-resources-bar .newest-terms-bar').appendTo('#search .main-content .content-columns .content-center').wrapAll('<div class="sidebar-wrapper-lower"></div>').css('display','block');
					$('#topicLanding .desktop-resources-bar .wotd-bar, .desktop-resources-bar .newest-terms-bar').appendTo('#topicLanding .main-content .content-columns .content-center').wrapAll('<div class="sidebar-wrapper-lower"></div>').css('display','block');
					$('#resources .desktop-resources-bar .wotd-bar, .desktop-resources-bar .newest-terms-bar').appendTo('#resources .main-content .content-columns .content-center').wrapAll('<div class="sidebar-wrapper-lower"></div>').css('display','block');
					$('#resourcesChild .desktop-resources-bar .wotd-bar, .desktop-resources-bar .newest-terms-bar').appendTo('#resourcesChild .main-content .content-columns .content-center').wrapAll('<div class="sidebar-wrapper-lower"></div>').css('display','block');
					$('#extensions .desktop-resources-bar .wotd-bar, .desktop-resources-bar .newest-terms-bar').appendTo('#extensions .main-content .content-columns .content-center').wrapAll('<div class="sidebar-wrapper-lower"></div>').css('display','block');
					$('#browseAlphaDef .desktop-resources-bar .wotd-bar, .desktop-resources-bar .newest-terms-bar, .desktop-resources-bar .main-article-author, .desktop-resources-bar .whatis-msplash').appendTo('#browseAlphaDef .main-content .content-columns .content-center').wrapAll('<div class="sidebar-wrapper-lower"></div>').css('display','block');
					$('#browseAlphaExt .desktop-resources-bar .wotd-bar, .desktop-resources-bar .newest-terms-bar, .desktop-resources-bar .ask-peers-bar, .desktop-resources-bar .top-ext-bar').appendTo('#browseAlphaExt .main-content .content-columns .content-center').wrapAll('<div class="sidebar-wrapper-lower"></div>').css('display','block');
					$('#glossaryChild .desktop-resources-bar .wotd-bar, .desktop-resources-bar .newest-terms-bar, .desktop-resources-bar .ask-peers-bar').appendTo('#glossaryChild .main-content .content-columns .content-center').wrapAll('<div class="sidebar-wrapper-lower"></div>').css('display','block');
					$('#glossaryParent .desktop-resources-bar .wotd-bar, .desktop-resources-bar .newest-terms-bar, .desktop-resources-bar .ask-peers-bar').appendTo('#glossaryParent .main-content .content-columns .content-center').wrapAll('<div class="sidebar-wrapper-lower"></div>').css('display','block');
					$('#fileExtDetail .desktop-resources-bar .top-ext-bar, .desktop-resources-bar .wotd-bar, .desktop-resources-bar .newest-terms-bar').insertAfter('#fileExtDetail .main-content.comments-wrapper').wrapAll('<div class="sidebar-wrapper-lower"></div>').css('display','block');
					$('#siteListing .desktop-resources-bar .wotd-bar, .desktop-resources-bar .newest-terms-bar').appendTo('#siteListing .main-content .content-columns .content-center').wrapAll('<div class="sidebar-wrapper-lower"></div>').css('display','block');
					$('#siteDetail .desktop-resources-bar .wotd-bar, .desktop-resources-bar .newest-terms-bar').appendTo('#siteDetail .main-content .content-columns .content-center').wrapAll('<div class="sidebar-wrapper-lower"></div>').css('display','block');
					$('#home .wotd-bar').css('display','block');
				} else {
					$('.mobile-resources-bar .wotd-bar, .mobile-resources-bar .newest-terms-bar, .mobile-resources-bar .main-article-author, .mobile-resources-bar .whatis-msplash, .mobile-resources-bar .ask-peers-bar, .mobile-resources-bar .top-ext-bar').remove();
					$('.wotd-bar').prependTo('.resources-bar.desktop-resources-bar').css('display','block');
					$('#browseAlphaExt .top-ext-bar').prependTo('.resources-bar.desktop-resources-bar').css('display','block');
					$('#browseAlphaExt .ask-peers-bar').prependTo('.resources-bar.desktop-resources-bar').css('display','block');
					$('#fileExtDetail .top-ext-bar').prependTo('.resources-bar.desktop-resources-bar').css('display','block');
					$('#glossaryChild .ask-peers-bar').prependTo('.resources-bar.desktop-resources-bar').css('display','block');
					$('#glossaryParent .ask-peers-bar').prependTo('.resources-bar.desktop-resources-bar').css('display','block');
					$('.newest-terms-bar').appendTo('.resources-bar.desktop-resources-bar').css('display','block');
					$('#browseAlphaDef .main-article-author').appendTo('.resources-bar.desktop-resources-bar').css('display','block');
					$('#browseAlphaDef .whatis-msplash').appendTo('.resources-bar.desktop-resources-bar').css('display','block');
					$('.sidebar-wrapper-lower').remove();
				}

				// scrolls the new share bar on eproducts pages, necessary without having to rework template
				if (TT.mq < TT.mqs.desktop)
				{
					$('#ebookDetail .share-bar-desktop, #ebookDetailLogin .share-bar-desktop, #ezineDetail .share-bar-desktop, #ezineDetailLogin .share-bar-desktop, #ebookChapter .share-bar-desktop, #ebookChapterLogin .share-bar-desktop').prependTo('.actions-bar').css('display','block');
				} else {
					$('#ebookDetail .share-bar-desktop, #ebookDetailLogin .share-bar-desktop, #ezineDetail .share-bar-desktop, #ezineDetailLogin .share-bar-desktop, #ebookChapter .share-bar-desktop, #ebookChapterLogin .share-bar-desktop').appendTo('.eproducts-section.issue-detail').css('display', 'block');

					if ($('#ebookDetail .share-bar-desktop, #ebookDetailLogin .share-bar-desktop, #ezineDetail .share-bar-desktop, #ezineDetailLogin .share-bar-desktop, #ebookChapter .share-bar-desktop, #ebookChapterLogin .share-bar-desktop').length > 0) {
						$(function() {
							if ($('body').hasClass("header-desktop-fixed")) {
								addHeader = 100;
							} else {
								addHeader = 0;
							}

							var top = $('#ebookDetail .share-bar-desktop, #ebookDetailLogin .share-bar-desktop, #ezineDetail .share-bar-desktop, #ezineDetailLogin .share-bar-desktop, #ebookChapter .share-bar-desktop, #ebookChapterLogin .share-bar-desktop').offset().top - parseFloat($('#ebookDetail .share-bar-desktop, #ebookDetailLogin .share-bar-desktop, #ezineDetail .share-bar-desktop, #ezineDetailLogin .share-bar-desktop, #ebookChapter .share-bar-desktop, #ebookChapterLogin .share-bar-desktop').css('marginTop').replace(/auto/, 0)) - 330 + addHeader;
							// var commentsTop = $('.ezine-more-downloads').offset().top - parseFloat($('.ezine-more-downloads').css('marginTop').replace(/auto/, 0)) - 670;
							var issueDetailHeight = $('.issue-detail').height() + $('.issue-contents').height() - 200;

							var maxY = issueDetailHeight;

							$(window).scroll(function(evt) {
								if ($('.header-leaderboard.ad-desktop').css('position') == 'fixed') {
									addLB = 90;
								} else {
									addLB = 0;
								}

								var y = $(this).scrollTop();
								if (y > top) {
									if (y < maxY - addLB) {
										$('#ebookDetail .share-bar-desktop, #ebookDetailLogin .share-bar-desktop, #ezineDetail .share-bar-desktop, #ezineDetailLogin .share-bar-desktop, #ebookChapter .share-bar-desktop, #ebookChapterLogin .share-bar-desktop').addClass('fixed').removeAttr('style');
									} else {
										$('#ebookDetail .share-bar-desktop, #ebookDetailLogin .share-bar-desktop, #ezineDetail .share-bar-desktop, #ezineDetailLogin .share-bar-desktop, #ebookChapter .share-bar-desktop, #ebookChapterLogin .share-bar-desktop').removeClass('fixed').css({
											position: 'absolute',
											'margin-top': (maxY - top) + 'px'
										});
									}
								} else {
									$('#ebookDetail .share-bar-desktop, #ebookDetailLogin .share-bar-desktop, #ezineDetail .share-bar-desktop, #ezineDetailLogin .share-bar-desktop, #ebookChapter .share-bar-desktop, #ebookChapterLogin .share-bar-desktop').removeClass('fixed');
								}
							});
						});
					}
				}

				// iot sponsored blog
				if (TT.mq < TT.mqs.desktop)
				{
					$('#blog-post .main-guide-hero-header.sponsored .main-guide-hero-header-right').insertBefore('.main-guide-hero-header').css('display','block');
				} else {
					$('#blog-post .main-guide-hero-header-right').appendTo('.main-guide-hero-header');
				}

				// iot moving homepage topics splash
				$('.homepage-topics-iot').appendTo('.trending-topics-sidebar').css('display', 'block');

				if (TT.mq > TT.mqs.tablet)
				{
					$('.browse-alpha-bridge-nav').jScrollPane();
					$('#glossaryParent .desktop-chapters-bar .chapters-bar-list').jScrollPane();
					$('#siteListing .desktop-chapters-bar .chapters-bar-list').jScrollPane();
				}

				// switch whatis search text at 960 to fit
				if (TT.mq === TT.mqs.desktop)
				{
					$(".header-search-input.whatis").attr("placeholder","Search Definitions");
				} else {
					$(".header-search-input.whatis").attr("placeholder","Search Thousands of Tech Definitions");
				}

				// switch search site text at 960 to fit
				if (TT.mq === TT.mqs.desktop)
				{
					$(".header_v2 .header-search-input, .header-search-input.tss").attr("placeholder","Search TechTarget");
				} else {
					$(".header_v2 .header-search-input, .header-search-input.tss").attr("placeholder","Search the TechTarget Network");
				}

				if (TT.mq === TT.mqs.desktop)
				{
					$(".header-search-input.lemag, .header_v2.fr .header-search-input").attr("placeholder","Rechercher sur LeMagIT");
				} else {
					$(".header-search-input.lemag, .header_v2.fr .header-search-input").attr("placeholder","Rechercher sur LeMagIT");
				}

				if (TT.mq === TT.mqs.desktop)
				{
					$(".header-search-input.german").attr("placeholder","TechTarget-Netzwerk durchsuchen");
				} else {
					$(".header-search-input.german").attr("placeholder","TechTarget-Netzwerk durchsuchen");
				}

				if (TT.mq === TT.mqs.desktop)
				{
					$(".header-search-input.bm").attr("placeholder","Search BrianMadden.com");
				} else {
					$(".header-search-input.bm").attr("placeholder","Search BrianMadden.com");
				}

				if (TT.mq === TT.mqs.desktop)
				{
					$(".header-search-input.cw").attr("placeholder","Search Computer Weekly");
				} else {
					$(".header-search-input.cw").attr("placeholder","Search Computer Weekly");
				}

				if (TT.mq === TT.mqs.desktop)
				{
					$(".header-search-input.microscope").attr("placeholder","Search MicroScope");
				} else {
					$(".header-search-input.microscope").attr("placeholder","Search MicroScope");
				}

				if (TT.mq === TT.mqs.desktop)
				{
					$(".header_v2.es .header-search-input").attr("placeholder","Buscar en la red de TechTarget");
				} else {
					$(".header_v2.es .header-search-input").attr("placeholder","Buscar en la red de TechTarget");
				}
			});

			$('.guide-collapse-toggle').on(TT.click, function(){
				var collapseToggle = $(this);
				utils.guideCollapseToggle(collapseToggle);
				return false;
			});

			$('.network-hub-group-toggle .toggle-marker').on(TT.click, function(){
				var collapseToggle = $(this).parent();
				utils.guideCollapseToggle(collapseToggle);
				return false;
			});

			$('.headshots-hub-content .collapse-toggle-inner').on(TT.click, function(){
				var collapseHeadshots = $(this);
				utils.hubHeadshotsCollapseToggle(collapseHeadshots);
				return false;
			});

			$('.network-hub-sites-content .collapse-toggle-inner').on(TT.click, function(){
				var collapseSites = $(this);
				utils.hubSitesCollapseToggle(collapseSites);
				return false;
			});

			$('.video-transcript-content .collapse-toggle-inner, .podcast-transcript-content .collapse-toggle-inner').on(TT.click, function(){
				var collapseTranscript = $(this);
				utils.videoTranscriptCollapseToggle(collapseTranscript);
				return false;
			});

			$('.news-archive-collapse-toggle').on(TT.click, function(){
				var collapseNews = $(this);
				utils.newsArchiveCollapseToggle(collapseNews);
				return false;
			});

			$('.multimedia-sidebar-list.blogroll .collapse-toggle-inner').on(TT.click, function(){
				var collapseBlogroll = $(this);
				utils.blogrollCollapseToggle(collapseBlogroll);
				return false;
			});

			/* click handler for guide section links to scroll like chapters bar links
			$(".refinery-guide-sections").hammer().on(TT.click, 'li', function(e){
				e.preventDefault();

				var theGuideID = $(this).find('a').attr('href');
				var selector = theGuideID;
				var scrollHere = $(selector);
				$.scrollTo(scrollHere.offset().top - 82, 500);
			});*/

			// scrolling to specified hash id on page load
			$(function(){
				if (window.location.hash.indexOf('guide') == 1 ){
					var hash = window.location.hash;
					$('html, body').animate({ scrollTop: $(hash).offset().top - 82});
				}
			});
		},


		getClickEvent:function(){
			if(Modernizr.touch){
				this.click = 'tap';
			}
			else{
				this.click = 'click';
			}
		},


		setupRelatedTermsModule:function(){
			var $relTerms = $('.related-terms-all-wrapper');
			
			if(TT.mq === TT.mqs.tablet && $relTerms.length > 0 && $relTerms.height() > 150){
				$relTerms.addClass('related-terms-all-closed');
				$relTerms.on('click', '.related-terms-show-more', function(){
					$relTerms.toggleClass('related-terms-all-closed related-terms-all-open');
					
					if($relTerms.hasClass('related-terms-all-closed')){
						$relTerms.find('.related-terms-show-more-inner').text(ctx.i18n('toggle_box.show_more'));
					}
					else{
						$relTerms.find('.related-terms-show-more-inner').text(ctx.i18n('toggle_box.show_less'));
					}
				});
			}
			else{
				$('.related-terms-show-more').remove();
			}
		},

		setupProFeaturesModule:function() {
			$('.pro-features-wrapper').css('display', 'block').append($('.pro-features-content'));
			$('.pro-features-content').css('display', 'block');
		},

		setupDiscussionModule:function() {
			$('.join-discussion-wrapper').css('display', 'block').append($('.join-discussion-content'));
			$('.join-discussion-content').css('display', 'block');
		},

		setupHomepageModules:function() {
			$('.learn-more-home').append($('.learn-more-resources'));
			$('.from-across-home').append($('.from-across-definitions'));
		}

	};

	$(function(){
		TT.init();
	});

	return TT;

});
// UI utils module
TT("ui/utils", ["../main", "lib/jquery", "context", "context/ui"], function (TT, $, ctx, ui) {

	return {
		// Grabs z-index value from the body element
		// which is where we are storing the current media-query's value
		// Modified version of http://bricss.net/post/22198838298/easily-checking-in-javascript-if-a-css-media-query-has
		// Works across all browsers including IE7+, whereas method linked above fails in certain IE versions
		setMq:function(){
			var mq = $('body').css('z-index');
			TT.mq = parseInt(mq, 10);
		},


		// Check if min-width media-query for the given width is active.
		// Modernizr.mq() accomplishes this same task, but fails in IE8
		// and other browsers that do not natively support media-queries
		checkMq:function(width){
			if(TT.mq >= width){
				return true;
			}
			return false;
		},


		// If on a high-res device, swaps img tags for high-res src
		swapInHighResImgs:function() {
			var els = $(".replace_2x");
			
			if($('.replace_2x').css('font-size') == "1px"){
				for(var i = 0; i < els.length; i++) {
					var img = els[i];
					var src = img.getAttribute('src');
					src = src.substr(0, src.length-4) + '@2x' + src.substr(src.length-4);
					img.src = src;
				}
			}
		},


		// Image credits
		imageCredits:function() {
			var imageCredit = $('img[data-credit]');
			
			imageCredit.each(function() {
				var thisImage = $(this);
				var thisCredit = $(this).data('credit');
				$(thisImage).wrap('<div class="imageWithCredit" />');
				$("<p>" + thisCredit + "</p>").insertAfter(thisImage);
			});
		},

		
		// Applying fade mask to article images greater than 480px tall
		articleImageMasks:function() {
			var articleImage = $('.main-article-image');

			articleImage.each(function() {
				var thisArticleImage = $(this).find('img');
				if (thisArticleImage.height() > 480) {
					$(this).append("<div class='main-article-image-mask'></div>");
				}
			});
		},


		// Resizes a topic search result if it appears as the first or second result so that its border shows correctly next to the tall ad
		resizeSRT:function() {
			var SRT = $('.search-result-topic');
			
			SRT.each(function() {
				var thisSRT = $(this);
				var thisSRTOffset = thisSRT.offset();
				if (thisSRTOffset.top > 10 && thisSRTOffset.top < 1020) {
					thisSRT.css('max-width', '760px');
				}
				if ((thisSRT).hasClass('second-topic')) {
					thisSRT.css('max-width', '');
				}
			});
		},


		// Move box ad on topics pages at 960 breakpoint into new and notable
		moveTopicsBoxAd:function() {
			if(TT.mq === 960) {
				var topicHeaderHeight = $('.topic-header').height();
				$('.topic-header .page-header-ad').css({
					'top' : topicHeaderHeight + 90,
					'display' : 'block'
				});
			} else {
				$('.topic-header .page-header-ad').css({
					'top' : '',
					'display' : ''
				});
			}
		},

		// Photostory page
		setupPhotostory:function() {
			if ($('.photostory-detail').length > 0) {
				// Sneak scroll stuff
				(function() {
					var sneaky = new ScrollSneak(location.hostname), tabs = document.getElementById('photostory-thumbs-slider').getElementsByTagName('li'), i = 0, len = tabs.length;
					for (; i < len; i++) {
						tabs[i].onclick = sneaky.sneak;
					}
					document.getElementById('arrow-prev').onclick = sneaky.sneak;
					document.getElementById('arrow-next').onclick = sneaky.sneak;
					document.getElementById('icon-prev').onclick = sneaky.sneak;
					document.getElementById('icon-next').onclick = sneaky.sneak;
				})();

				// Scroll to thumbnail if beyond visible number
				$(function(){
					var thumbCount = $('.thumbelina').children().length;
					var thumbSelectedPos = $('.thumbelina li').index($('li.selected')[0]);

					if(TT.mq === TT.mqs.desktop) {
						var scrollAmount = 437 - (thumbSelectedPos * 91);
						if (thumbCount > 6 && thumbSelectedPos > 5) {
							$('.thumbelina').css('left', scrollAmount);
						}
					} else {
						var scrollAmount = 728 - (thumbSelectedPos * 91);
						if (thumbCount > 9 && thumbSelectedPos > 8) {
							$('.thumbelina').css('left', scrollAmount);
							
						}
					}
				});

				// Scrolling photostory thumbs
				$('.photostory-thumbs ul').addClass('thumbelina').wrap('<div style="position:absolute;overflow:hidden;width:100%;height:100%;">');
				var container = $('.thumbelina');
				var items = container.children('li');
				var itemCount = items.length;
				var slideMovement = 15;
				var slideLeft = $('.thumbelina-but.horiz.left');
				var slideRight = $('.thumbelina-but.horiz.right');
				var slideSpeed = 30;
				var sliding;

				if(TT.mq === TT.mqs.desktop) {
					var bounds = {
						'left': 0,
						'right': -((91 * itemCount) - 530)
					};
				} else {
					var bounds = {
						'left': 0,
						'right': -((91 * itemCount) - 820)
					};
				}

				function moveItemsLeft() {
					if(container.position().left < bounds.left) {
						$(slideLeft).removeClass('disabled');
						$(slideRight).removeClass('disabled');
						container.css({
							left : Math.round((container.position().left + slideMovement)/15)*15
						});
					} else {
						$(slideLeft).addClass('disabled');
					}
				}

				function moveItemsRight() {
					if(container.position().left > bounds.right) {
						$(slideLeft).removeClass('disabled');
						$(slideRight).removeClass('disabled');
						container.css({
							left : container.position().left - slideMovement
						});
					} else {
						$(slideRight).addClass('disabled');
					}
				}

				slideLeft.on({
					click: function (e) {
						e.preventDefault();
					},
					mousedown: function () {
						sliding = window.setInterval(moveItemsLeft, slideSpeed);
					},
					mouseup: function () {
						window.clearInterval(sliding);
					}
				});

				slideRight.on({
					click: function (e) {
						e.preventDefault();
					},
					mousedown: function () {
						sliding = window.setInterval(moveItemsRight, slideSpeed);
					},
					mouseup: function () {
						window.clearInterval(sliding);
					}
				});

				$(window).load(function() {
					if (container.position().left == 0) {
						$(slideLeft).addClass('disabled')
					}
					if ($('.thumbelina li:last-child').hasClass('selected')) {
						$(slideRight).addClass('disabled')
					}
					if (TT.mq === TT.mqs.desktop) {
						if ($('.thumbelina').children().length < 7) {
							$(slideRight).addClass('disabled')
						}
					} else {
						if ($('.thumbelina').children().length < 10) {
							$(slideRight).addClass('disabled')
						}
					}
				});
			}
		},

		setupEHandbook:function() {
			if ($('.eHandbook-landing').length || $('.eHandbook-article').length) {

				$('<div class="fullScreenImage"><div class="backgroundGradient"></div></div>').insertBefore('#main-content.main-content');

				var bkgImage = $('.background-image'),
					headerHTML = $('.eproducts-header').html(),
					source,
					mHeight;

				if (TT.mq === 1280) {
					if (bkgImage.data('img1280').length === 0) {
						source = bkgImage.attr('src');
					} else {
						source = bkgImage.data('img1280');
					}
					mHeight = 590;
				} else if (TT.mq === 960) {
					if (bkgImage.data('img960').length === 0) {
						source = bkgImage.attr('src');
					} else {
						source = bkgImage.data('img960');
					}
					mHeight = 489;
				} else if (TT.mq === 640) {
					if (bkgImage.data('img640').length === 0) {
						source = bkgImage.attr('src');
					} else {
						source = bkgImage.data('img640');
					}
					mHeight = 318;
				} else if (TT.mq === 320) {
					source = bkgImage.attr('src');
					mHeight = 174;
				}

				var ebookTitle = $('#eHandbookSlideUpNavContentTop .title').text();

				// $('#eHandbookSlideUpNavContentTop .title').html('<span class="eyebrow">E-Handbook: </span><span class="titleText">' + ebookTitle.substring(12) + '</span>');

				$('.fullScreenImage').css({
					'background': 'url(' + source + ') center 0 no-repeat',
					'background-size': 'cover',
					'height': '100%',
					'max-height': mHeight * 2
				});

				$('<div class="eHandbook-header"></div>').insertBefore('.eHandbook-article #main-content.main-content');

				$('.eHandbook-header').append(headerHTML);

				if ($('.image-credit').length) {
					$('footer').prepend($('.image-credit'));
				}

				if(TT.mq > TT.mqs.tablet) {
					$('.eHandbookRelatedArticles > ul > li > h3').setAllToMaxHeight();
				}

				var collapseFlyout = function() {
					footerUnseen = false;
					readyToPeek = true;

					$('#eHandbookSlideUpNavContainer').stop().removeClass('expanded firstPeeking').addClass('collapsed');
				};

				$('#eHandbookSlideUpNavContainer #slideUpCloseButton').click(collapseFlyout);

				var scrollExpands = function() {
					var footerUnseen = true;

					$(window).scroll(function() {
						if(footerUnseen == true) {
							var triggerPosition = $(document).height() - 600;
							var triggerItem = $('#eHandbookSlideUpNavContainer').offset().top + $('#eHandbookSlideUpNavContainer.collapsed').height();

							if(triggerItem > triggerPosition) {
								footerUnseen = false;
								readyToPeek = false;

								$('#eHandbookSlideUpNavContainer').removeClass('collapsed peeking firstPeeking').addClass('expanded');
								$(window).trigger('resize');
							}
						}
					});
				}

				var footerUnseen = true;
				var readyToPeek = true;

				// Peek on load just once.
				$('#eHandbookSlideUpNavContainer').addClass('collapsed firstPeeking');

				var peeking = function() {
					$(this).removeClass('firstPeeking');

					if (readyToPeek) {
						// Peek on hover
						if($('#eHandbookSlideUpNavContainer').hasClass('collapsed')){
							$('#eHandbookSlideUpNavContainer').addClass('peeking');
						}
					}
				};

				var removePeeking = function() {
					if(readyToPeek) {
						$('#eHandbookSlideUpNavContainer').removeClass('peeking');
					}
				}

				// Peek on mouseenter, unpeek on mouseleave
				$('#eHandbookSlideUpNavContainer').mouseenter(peeking);
				$('#eHandbookSlideUpNavContainer').mouseleave(removePeeking);

				$.fn.clickToggle = function(func1, func2) {
					var funcs = [func1, func2];
					this.data('toggleclicked', 0);
					this.click(function() {
						var data = $(this).data();
						var tc = data.toggleclicked;
						$.proxy(funcs[tc], this)();
						data.toggleclicked = (tc + 1) % 2;
					});
					return this;
				};

				$('#eHandbookSlideUpNavContentTop').clickToggle(function() {
					readyToPeek = false;
					$('#eHandbookSlideUpNavContainer').stop().removeClass('collapsed peeking firstPeeking').addClass('expanded');
					$(window).trigger('resize');
				}, function() {
					footerUnseen = false;
					readyToPeek = true;

					$('#eHandbookSlideUpNavContainer').stop().removeClass('expanded firstPeeking').addClass('collapsed');
				});

				$('.collapsedNavArrows, #eHandbookSlideUpNavContainer #slideUpCloseButton').click(function(e) {
					e.stopPropagation();
				});

				scrollExpands();
			}
		},


		// Sample use-case: When a menu is closed, you might want to remove the inline style
		// controlling the 'display' CSS property, so this inline style will not interfere with
		// other views (eg. 760/1120 where menus are never hidden)
		clearDisplayProp:function(element){
			var $element = $(element);
			if(!$element.is(':visible')){
				$element.css('display', '');
			}
		},
		
		
		// Returns true if viewing site on an iOS device (iPhone, iPad, iPod)
		isIos:function(){
			return (navigator.userAgent.match(/(iPad|iPhone|iPod)/i) ? true : false);
		},
		
		
		// Function to bind a click handler that fires a callback if a user clicks outside of the selector
		// Ideal for dropdown/ popup menus
		bindClickOut:function(selector, callback, subspace) {
			// Optional subspace param allows us to have multiple clickout events bound on different items
			// We can then unbind only that sub-namespaced clickout event when appropriate
			var subspace = '.'+subspace || '';
			
			// Listener for clickout
			$('body').on('click.clickout'+subspace, function(e) {
				
				//if user clicks outside of something not a child of a dropdown
				if ( $(e.target).closest($(selector)).length === 0 ) {
					$('body').off('click.clickout'+subspace);
				
					callback();

					// Prevent the click from propagating
					e.preventDefault();
				}
			});
		},

		// Function for collapsing sections on guide parent page at mobile breakpoint
		guideCollapseToggle:function(collapseItem){
			if (TT.mq === TT.mqs.mobile) {
				var $guideToggle = $(collapseItem).find('.toggle-marker');
				var $guideSection = $(collapseItem).parents('.guide-section, .network-hub-group');

				if($guideSection.hasClass('closed')){
					$guideToggle.text('-');
					$guideSection.removeClass('closed');
				} else {
					$guideToggle.text('+');
					$guideSection.addClass('closed');
				}
			} else { 
				return false; 
			}
		},

		// Collapsing hub headshots
		hubHeadshotsCollapseToggle:function(collapseItemHeadshot){
			if(TT.mq < TT.mqs.desktop) {
				var $headshotsToggle = $(collapseItemHeadshot);
				var $headshotsSection = $(collapseItemHeadshot).parents('.headshots-hub-content');

				if($headshotsSection.hasClass('closed')){
					$headshotsToggle.text(ctx.i18n('toggle_box.show_less'));
					$headshotsSection.removeClass('closed');
				}
				else {
					$headshotsToggle.text(ctx.i18n('toggle_box.show_more'));
					$headshotsSection.addClass('closed');
					window.scrollTo(0,-500);
				}
			}
		},

		// Collapsing hub sites groups
		hubSitesCollapseToggle:function(collapseItemSites){
			if(TT.mq > TT.mqs.mobile) {
				var $hubSitesToggle = $(collapseItemSites);
				var $hubSitesSection = $(collapseItemSites).parents('.network-hub-sites-content');

				if($hubSitesSection.hasClass('closed')){
					$hubSitesToggle.text('- Show Less');
					$hubSitesSection.removeClass('closed');
				}
				else {
					$hubSitesToggle.text('+ Show More');
					$hubSitesSection.addClass('closed');
				}
			}
		},

		// Collapsing news archive sidebar items
		newsArchiveCollapseToggle:function(collapseArchiveList){
			var $newsArchiveToggle = $(collapseArchiveList).find('.toggle-marker');
			var $newsArchiveSection = $(collapseArchiveList).parents('.news-archive-item');
			var $newsArchiveList = $(collapseArchiveList);

			if($newsArchiveSection.hasClass('closed')){
				$newsArchiveToggle.attr('data-icon', '3');
				$newsArchiveSection.removeClass('closed');
				$('.news-archive-list').jScrollPane();
			}
			else {
				$newsArchiveToggle.attr('data-icon', '5');
				$newsArchiveSection.addClass('closed');
				$('.news-archive-list').jScrollPane();
			}
		},

		// Collapsing video transcript
		videoTranscriptCollapseToggle:function(collapseItemTranscript){
			var $videoTranscriptToggle = $(collapseItemTranscript);
			var $videoTranscriptSection = $(collapseItemTranscript).parents('.video-transcript-content, .podcast-transcript-content');

			if($videoTranscriptSection.hasClass('closed')){
				$videoTranscriptToggle.text('- Hide Transcript');
				$videoTranscriptSection.removeClass('closed');
			}
			else {
				var $videoTranscriptTop = $('.video-transcript-content, .podcast-transcript-content').offset().top - 100;
				$videoTranscriptToggle.text('+ Show Transcript');
				$videoTranscriptSection.addClass('closed');
				window.scrollTo(0,$videoTranscriptTop);
			}
		},
		
		// Collapsing cw blogroll
		blogrollCollapseToggle:function(collapseblog){
			var $blogrollToggle = $(collapseblog);
			var $blogrollSection = $(collapseblog).parents('.multimedia-sidebar-list.blogroll');

			if($blogrollSection.hasClass('closed')){
				$blogrollToggle.text('- Close ');
				$blogrollSection.removeClass('closed');
			}
			else {
				var $blogrollTop = $('.multimedia-sidebar-list.blogroll').offset().top - 100;
				$blogrollToggle.text('+ View All Blogs');
				$blogrollSection.addClass('closed');
				window.scrollTo(0,$blogrollTop);
			}
		},
		
		fixPseudoIE: function() {
			// Fix for icon fonts and any other psuedo elements
			if( $('html').hasClass('lt-ie9') ) {
				var head = document.getElementsByTagName('head')[0];
				var style = document.createElement('style');
			
				style.type = 'text/css';
				style.styleSheet.cssText = ':before,:after{content:none !important';
				head.appendChild(style);
				setTimeout(function(){
					head.removeChild(style);
				}, 0);
			}
		},

		freezeLeaderboard: function() {
			// Freeze leaderboard ad for 5 seconds
			var testLB = true;
			window.$(window).scroll(function() {
				if (testLB){
				  if (window.$(this).scrollTop() >= 155) {
						showAd();
						hideTimeer();
					  testLB = false;
				  }
				}
				else{
				  if (window.$(this).scrollTop() <= 155) {
					hideAd();
				  }
				}
			});


			function hideTimeer(){
			  setTimeout(hideAd, 5000);
			}

			function hideAd(){
				window.$(".header-leaderboard").css({"position":"inherit","z-index":"99","margin":"0 auto", "width":"100%","margin-top":"0px","background-color":"#f1f1f1","top":"auto"});
				window.$(".locking-full-bar .locking-left-bar, .locking-full-bar .resources-bar").removeClass('move-sharebar-down');
				window.$(".content-center .actions-bar").removeClass("move-actionsbar-down");
			}

			function showAd(){
				if($(".nav-pro-callout, .nav-links").length > 0){
				  window.$(".header-leaderboard").css({"position":"fixed","z-index":"99","margin":"0 auto", "width":"100%","margin-top":"47px","background-color":"#f1f1f1","top":"10px"});
				  window.$(".locking-full-bar .locking-left-bar, .locking-full-bar .resources-bar, .main-article-hero-image").addClass('move-sharebar-down');
				  window.$(".content-center .actions-bar").addClass("move-actionsbar-down");
				}
				else{
				  window.$(".header-leaderboard").css({"position":"fixed","z-index":"99","margin":"0 auto", "width":"100%","margin-top":"32px","background-color":"#f1f1f1","top":"10px"});
				  window.$(".locking-full-bar .locking-left-bar, .locking-full-bar .resources-bar, .main-article-hero-image").addClass('move-sharebar-down');
				  window.$(".content-center .actions-bar").addClass("move-actionsbar-down");
				}

			}
		},

		adjustHomepageElements: function() {

			// New and Notable move topics menu at mobile -> bottom, at all other breakpoints -> right
			// Sections From Across move View All button
			if(TT.mq < TT.mqs.tablet) {
				$('.homepage-topics').insertAfter('.nn-home-item:last').css('display', 'block');
				$('.fa-item-1-button').appendTo('.from-across-items');
				$('.ezine-pro-plus-list h3').setAllToMaxHeight();
			} else {
				$('.homepage-topics').insertAfter('.nn-item-1').css('display', 'block');
				$('.fa-item-1-button').appendTo('.fa-item-1');
			}

			// tablet breakpoint - equal heights
			// New and Notable make 4th and 5th items line up correctly at tablet breakpoint
			// Resize titles for News splash
			if(TT.mq === TT.mqs.tablet) {
				var nnWidth = $('.new-notable-home').width();
				var nnItem4Width = $('.nn-item-4').width();
				var nnTopicsWidth = $('.homepage-topics').width();

				$('.nn-item-4').css('margin-right', nnWidth - nnItem4Width - nnTopicsWidth + 20);
				$('.nn-item-5').width(nnTopicsWidth);

				// resize titles for News splash because tablet has adjustable width for titles

				$('.tt-item-1 h3, .tt-item-2 h3, .tt-item-3 h3, .tt-item-4 h3, .tt-item-5 h3, .tt-item-6 h3, .tt-item-1 h4, .tt-item-2 h4, .tt-item-3 h4, .tt-item-4 h4, .tt-item-5 h4, .tt-item-6 h4, .tt-item-1 p, .tt-item-2 p, .tt-item-3 p, .tt-item-4 p, .tt-item-5 p, .tt-item-6 p, .epf-list-item-1 h4, .epf-list-item-2 h4, .latest-issue-features > ul:nth-child(2) > li h4, .latest-issue-features > ul:nth-child(3) > li h4, .latest-issue-news > ul:nth-child(2) > li h4, .latest-issue-news > ul:nth-child(3) > li h4, .latest-issue-columns > ul:nth-child(2) > li h4, .latest-issue-columns > ul:nth-child(3) > li h4, .headshots-hub-list li, .contributor-articles-list li h5, .contributor-alpha-list ul li, .refinery-latest-posts li, .network-hub-group > h3, .network-hub-group > p, .unisites-sections li h4, .unisites-sections li p, .top-stories-home div h4, .latest-news-home li h4, .latest-news-home li .ln-item-img, .tips-home li .tips-item-img, .video-home li h4, .multimedia-items li h4, .multimedia-items li p, .multimedia-section.video-section li h4, .multimedia-section.podcast-section li h4, .multimedia-section.photostory-section li h4').css('height', '');

				$('.tt-item-1 h3, .tt-item-2 h3').setAllToMaxHeight();
				$('.tt-item-3 h3, .tt-item-4 h3').setAllToMaxHeight();
				$('.tt-item-5 h3, .tt-item-6 h3').setAllToMaxHeight();
				$('.tt-item-1 h4, .tt-item-2 h4').setAllToMaxHeight();
				$('.tt-item-3 h4, .tt-item-4 h4').setAllToMaxHeight();
				$('.tt-item-5 h4, .tt-item-6 h4').setAllToMaxHeight();
				$('.tt-item-1 p, .tt-item-2 p').setAllToMaxHeight();
				$('.tt-item-3 p, .tt-item-4 p').setAllToMaxHeight();
				$('.tt-item-5 p, .tt-item-6 p').setAllToMaxHeight();
				$('.epf-list-item-1 h4, .epf-list-item-2 h4').setAllToMaxHeight();
				$('.latest-issue-features > ul:nth-child(2) > li h4').setAllToMaxHeight();
				$('.latest-issue-features > ul:nth-child(3) > li h4').setAllToMaxHeight();
				$('.latest-issue-news > ul:nth-child(2) > li h4').setAllToMaxHeight();
				$('.latest-issue-news > ul:nth-child(3) > li h4').setAllToMaxHeight();
				$('.latest-issue-columns > ul:nth-child(2) > li h4').setAllToMaxHeight();
				$('.latest-issue-columns > ul:nth-child(3) > li h4').setAllToMaxHeight();
				$('.hub-list-item-1, .hub-list-item-2').setAllToMaxHeight();
				$('.hub-list-item-3, .hub-list-item-4').setAllToMaxHeight();
				$('.hub-list-item-5, .hub-list-item-6').setAllToMaxHeight();
				$('.hub-list-item-7, .hub-list-item-8').setAllToMaxHeight();
				$('.hub-list-item-9, .hub-list-item-10').setAllToMaxHeight();
				$('.hub-list-item-11, .hub-list-item-12').setAllToMaxHeight();
				$('.hub-list-item-13, .hub-list-item-14').setAllToMaxHeight();
				$('.hub-list-item-15, .hub-list-item-16').setAllToMaxHeight();
				$('.hub-list-item-17, .hub-list-item-18').setAllToMaxHeight();
				$('.hub-list-item-19, .hub-list-item-20').setAllToMaxHeight();
				$('.hub-list-item-21, .hub-list-item-22').setAllToMaxHeight();
				$('.hub-list-item-23, .hub-list-item-24').setAllToMaxHeight();
				$('.hub-list-item-25, .hub-list-item-26').setAllToMaxHeight();
				$('.hub-list-item-27, .hub-list-item-28').setAllToMaxHeight();
				$('.hub-list-item-29, .hub-list-item-30').setAllToMaxHeight();
				$('.hub-list-item-31, .hub-list-item-32').setAllToMaxHeight();
				$('.hub-list-item-33, .hub-list-item-34').setAllToMaxHeight();
				$('.hub-list-item-35, .hub-list-item-36').setAllToMaxHeight();
				$('.hub-list-item-37, .hub-list-item-38').setAllToMaxHeight();
				$('.hub-list-item-39, .hub-list-item-40').setAllToMaxHeight();
				$('.hub-list-item-41, .hub-list-item-42').setAllToMaxHeight();
				$('.hub-list-item-43, .hub-list-item-44').setAllToMaxHeight();
				$('.hub-list-item-45, .hub-list-item-46').setAllToMaxHeight();
				$('.hub-list-item-47, .hub-list-item-48').setAllToMaxHeight();
				$('.hub-list-item-49, .hub-list-item-50').setAllToMaxHeight();
				$('.alpha-list-item-1, .alpha-list-item-2').setAllToMaxHeight();
				$('.alpha-list-item-3, .alpha-list-item-4').setAllToMaxHeight();
				$('.alpha-list-item-5, .alpha-list-item-6').setAllToMaxHeight();
				$('.alpha-list-item-7, .alpha-list-item-8').setAllToMaxHeight();
				$('.alpha-list-item-9, .alpha-list-item-10').setAllToMaxHeight();
				$('.alpha-list-item-11, .alpha-list-item-12').setAllToMaxHeight();
				$('.alpha-list-item-13, .alpha-list-item-14').setAllToMaxHeight();
				$('.alpha-list-item-15, .alpha-list-item-16').setAllToMaxHeight();
				$('.alpha-list-item-17, .alpha-list-item-18').setAllToMaxHeight();
				$('.alpha-list-item-19, .alpha-list-item-20').setAllToMaxHeight();
				$('.alpha-list-item-21, .alpha-list-item-22').setAllToMaxHeight();
				$('.alpha-list-item-23, .alpha-list-item-24').setAllToMaxHeight();
				$('.alpha-list-item-25, .alpha-list-item-26').setAllToMaxHeight();
				$('.alpha-list-item-27, .alpha-list-item-28').setAllToMaxHeight();
				$('.alpha-list-item-29, .alpha-list-item-30').setAllToMaxHeight();
				$('.alpha-list-item-31, .alpha-list-item-32').setAllToMaxHeight();
				$('.alpha-list-item-33, .alpha-list-item-34').setAllToMaxHeight();
				$('.alpha-list-item-35, .alpha-list-item-36').setAllToMaxHeight();
				$('.alpha-list-item-37, .alpha-list-item-38').setAllToMaxHeight();
				$('.alpha-list-item-39, .alpha-list-item-40').setAllToMaxHeight();
				$('.alpha-list-item-41, .alpha-list-item-42').setAllToMaxHeight();
				$('.alpha-list-item-43, .alpha-list-item-44').setAllToMaxHeight();
				$('.alpha-list-item-45, .alpha-list-item-46').setAllToMaxHeight();
				$('.alpha-list-item-47, .alpha-list-item-48').setAllToMaxHeight();
				$('.alpha-list-item-49, .alpha-list-item-50').setAllToMaxHeight();
				$('.alpha-list-item-51, .alpha-list-item-52').setAllToMaxHeight();
				$('.alpha-list-item-53, .alpha-list-item-54').setAllToMaxHeight();
				$('.alpha-list-item-55, .alpha-list-item-56').setAllToMaxHeight();
				$('.alpha-list-item-57, .alpha-list-item-58').setAllToMaxHeight();
				$('.alpha-list-item-59, .alpha-list-item-60').setAllToMaxHeight();
				$('.alpha-list-item-61, .alpha-list-item-62').setAllToMaxHeight();
				$('.alpha-list-item-63, .alpha-list-item-64').setAllToMaxHeight();
				$('.alpha-list-item-65, .alpha-list-item-66').setAllToMaxHeight();
				$('.alpha-list-item-67, .alpha-list-item-68').setAllToMaxHeight();
				$('.alpha-list-item-69, .alpha-list-item-70').setAllToMaxHeight();
				$('.alpha-list-item-71, .alpha-list-item-72').setAllToMaxHeight();
				$('.alpha-list-item-73, .alpha-list-item-74').setAllToMaxHeight();
				$('.alpha-list-item-75, .alpha-list-item-76').setAllToMaxHeight();
				$('.alpha-list-item-77, .alpha-list-item-78').setAllToMaxHeight();
				$('.alpha-list-item-79, .alpha-list-item-80').setAllToMaxHeight();
				$('.alpha-list-item-81, .alpha-list-item-82').setAllToMaxHeight();
				$('.alpha-list-item-83, .alpha-list-item-84').setAllToMaxHeight();
				$('.alpha-list-item-85, .alpha-list-item-86').setAllToMaxHeight();
				$('.alpha-list-item-87, .alpha-list-item-88').setAllToMaxHeight();
				$('.alpha-list-item-89, .alpha-list-item-90').setAllToMaxHeight();
				$('.alpha-list-item-91, .alpha-list-item-92').setAllToMaxHeight();
				$('.alpha-list-item-93, .alpha-list-item-94').setAllToMaxHeight();
				$('.alpha-list-item-95, .alpha-list-item-96').setAllToMaxHeight();
				$('.alpha-list-item-97, .alpha-list-item-98').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-1 h5, .contributor-articles-list .list-item-2 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-3 h5, .contributor-articles-list .list-item-4 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-5 h5, .contributor-articles-list .list-item-6 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-7 h5, .contributor-articles-list .list-item-8 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-9 h5, .contributor-articles-list .list-item-10 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-11 h5, .contributor-articles-list .list-item-12 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-13 h5, .contributor-articles-list .list-item-14 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-15 h5, .contributor-articles-list .list-item-16 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-17 h5, .contributor-articles-list .list-item-18 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-19 h5, .contributor-articles-list .list-item-20 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-21 h5, .contributor-articles-list .list-item-22 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-23 h5, .contributor-articles-list .list-item-24 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-25 h5, .contributor-articles-list .list-item-26 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-27 h5, .contributor-articles-list .list-item-28 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-29 h5, .contributor-articles-list .list-item-30 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-31 h5, .contributor-articles-list .list-item-32 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-33 h5, .contributor-articles-list .list-item-34 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-35 h5, .contributor-articles-list .list-item-36 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-37 h5, .contributor-articles-list .list-item-38 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-39 h5, .contributor-articles-list .list-item-40 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-41 h5, .contributor-articles-list .list-item-42 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-43 h5, .contributor-articles-list .list-item-44 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-45 h5, .contributor-articles-list .list-item-46 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-47 h5, .contributor-articles-list .list-item-48 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-49 h5, .contributor-articles-list .list-item-50 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-51 h5, .contributor-articles-list .list-item-52 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-53 h5, .contributor-articles-list .list-item-54 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-55 h5, .contributor-articles-list .list-item-56 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-57 h5, .contributor-articles-list .list-item-58 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-59 h5, .contributor-articles-list .list-item-60 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-61 h5, .contributor-articles-list .list-item-62 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-63 h5, .contributor-articles-list .list-item-64 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-65 h5, .contributor-articles-list .list-item-66 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-67 h5, .contributor-articles-list .list-item-68 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-69 h5, .contributor-articles-list .list-item-70 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-71 h5, .contributor-articles-list .list-item-72 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-73 h5, .contributor-articles-list .list-item-74 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-75 h5, .contributor-articles-list .list-item-76 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-77 h5, .contributor-articles-list .list-item-78 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-79 h5, .contributor-articles-list .list-item-80 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-81 h5, .contributor-articles-list .list-item-82 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-83 h5, .contributor-articles-list .list-item-84 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-85 h5, .contributor-articles-list .list-item-86 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-87 h5, .contributor-articles-list .list-item-88 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-89 h5, .contributor-articles-list .list-item-90 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-91 h5, .contributor-articles-list .list-item-92 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-93 h5, .contributor-articles-list .list-item-94 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-95 h5, .contributor-articles-list .list-item-96 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-97 h5, .contributor-articles-list .list-item-98 h5').setAllToMaxHeight();
				$('.network-hub-group.item-1 > h3, .network-hub-group.item-2 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-3 > h3, .network-hub-group.item-4 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-5 > h3, .network-hub-group.item-6 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-7 > h3, .network-hub-group.item-8 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-9 > h3, .network-hub-group.item-10 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-11 > h3, .network-hub-group.item-12 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-13 > h3, .network-hub-group.item-14 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-15 > h3, .network-hub-group.item-16 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-1 > p, .network-hub-group.item-2 > p').setAllToMaxHeight();
				$('.network-hub-group.item-3 > p, .network-hub-group.item-4 > p').setAllToMaxHeight();
				$('.network-hub-group.item-5 > p, .network-hub-group.item-6 > p').setAllToMaxHeight();
				$('.network-hub-group.item-7 > p, .network-hub-group.item-8 > p').setAllToMaxHeight();
				$('.network-hub-group.item-9 > p, .network-hub-group.item-10 > p').setAllToMaxHeight();
				$('.network-hub-group.item-11 > p, .network-hub-group.item-12 > p').setAllToMaxHeight();
				$('.network-hub-group.item-13 > p, .network-hub-group.item-14 > p').setAllToMaxHeight();
				$('.network-hub-group.item-15 > p, .network-hub-group.item-16 > p').setAllToMaxHeight();
				$('.unisites-sections .item-1 h4, .unisites-sections .item-2 h4').setAllToMaxHeight();
				$('.unisites-sections .item-3 h4, .unisites-sections .item-4 h4').setAllToMaxHeight();
				$('.unisites-sections .item-5 h4, .unisites-sections .item-6 h4').setAllToMaxHeight();
				$('.unisites-sections .item-7 h4, .unisites-sections .item-8 h4').setAllToMaxHeight();
				$('.unisites-sections .item-9 h4, .unisites-sections .item-10 h4').setAllToMaxHeight();
				$('.unisites-sections .item-11 h4, .unisites-sections .item-12 h4').setAllToMaxHeight();
				$('.unisites-sections .item-13 h4, .unisites-sections .item-14 h4').setAllToMaxHeight();
				$('.unisites-sections .item-15 h4, .unisites-sections .item-16 h4').setAllToMaxHeight();
				$('.unisites-sections .item-17 h4, .unisites-sections .item-18 h4').setAllToMaxHeight();
				$('.unisites-sections .item-19 h4, .unisites-sections .item-20 h4').setAllToMaxHeight();
				$('.unisites-sections .item-21 h4, .unisites-sections .item-22 h4').setAllToMaxHeight();
				$('.unisites-sections .item-23 h4, .unisites-sections .item-24 h4').setAllToMaxHeight();
				$('.unisites-sections .item-1 p, .unisites-sections .item-2 p').setAllToMaxHeight();
				$('.unisites-sections .item-3 p, .unisites-sections .item-4 p').setAllToMaxHeight();
				$('.unisites-sections .item-5 p, .unisites-sections .item-6 p').setAllToMaxHeight();
				$('.unisites-sections .item-7 p, .unisites-sections .item-8 p').setAllToMaxHeight();
				$('.unisites-sections .item-9 p, .unisites-sections .item-10 p').setAllToMaxHeight();
				$('.unisites-sections .item-11 p, .unisites-sections .item-12 p').setAllToMaxHeight();
				$('.unisites-sections .item-13 p, .unisites-sections .item-14 p').setAllToMaxHeight();
				$('.unisites-sections .item-15 p, .unisites-sections .item-16 p').setAllToMaxHeight();
				$('.unisites-sections .item-17 p, .unisites-sections .item-18 p').setAllToMaxHeight();
				$('.unisites-sections .item-19 p, .unisites-sections .item-20 p').setAllToMaxHeight();
				$('.unisites-sections .item-21 p, .unisites-sections .item-22 p').setAllToMaxHeight();
				$('.unisites-sections .item-23 p, .unisites-sections .item-24 p').setAllToMaxHeight();
				$('.multimedia-main-item-1 h4, .multimedia-main-item-2 h4').setAllToMaxHeight();
				$('.multimedia-item-1 h4, .multimedia-item-2 h4').setAllToMaxHeight();
				$('.multimedia-item-3 h4, .multimedia-item-4 h4').setAllToMaxHeight();
				$('.multimedia-item-5 h4, .multimedia-item-6 h4').setAllToMaxHeight();
				$('.multimedia-item-7 h4, .multimedia-item-8 h4').setAllToMaxHeight();
				$('.multimedia-item-9 h4, .multimedia-item-10 h4').setAllToMaxHeight();
				$('.multimedia-item-11 h4, .multimedia-item-12 h4').setAllToMaxHeight();
				$('.multimedia-item-13 h4, .multimedia-item-14 h4').setAllToMaxHeight();
				$('.multimedia-item-15 h4, .multimedia-item-16 h4').setAllToMaxHeight();
				$('.multimedia-item-17 h4, .multimedia-item-18 h4').setAllToMaxHeight();
				$('.multimedia-item-19 h4, .multimedia-item-20 h4').setAllToMaxHeight();
				$('.multimedia-item-21 h4, .multimedia-item-22 h4').setAllToMaxHeight();
				$('.multimedia-item-23 h4, .multimedia-item-24 h4').setAllToMaxHeight();
				$('.multimedia-main-item-1 > p, .multimedia-main-item-2 > p').setAllToMaxHeight();
				$('.multimedia-item-1 > p, .multimedia-item-2 > p').setAllToMaxHeight();
				$('.multimedia-item-3 > p, .multimedia-item-4 > p').setAllToMaxHeight();
				$('.multimedia-item-5 > p, .multimedia-item-6 > p').setAllToMaxHeight();
				$('.multimedia-item-7 > p, .multimedia-item-8 > p').setAllToMaxHeight();
				$('.multimedia-item-9 > p, .multimedia-item-10 > p').setAllToMaxHeight();
				$('.multimedia-item-11 > p, .multimedia-item-12 > p').setAllToMaxHeight();
				$('.multimedia-item-13 > p, .multimedia-item-14 > p').setAllToMaxHeight();
				$('.multimedia-item-15 > p, .multimedia-item-16 > p').setAllToMaxHeight();
				$('.multimedia-item-17 > p, .multimedia-item-18 > p').setAllToMaxHeight();
				$('.multimedia-item-19 > p, .multimedia-item-20 > p').setAllToMaxHeight();
				$('.multimedia-item-21 > p, .multimedia-item-22 > p').setAllToMaxHeight();
				$('.multimedia-item-23 > p, .multimedia-item-24 > p').setAllToMaxHeight();
				$('.multimedia-section .video-item-1 h4, .multimedia-section .video-item-2 h4').setAllToMaxHeight();
				$('.multimedia-section .video-item-3 h4, .multimedia-section .video-item-4 h4').setAllToMaxHeight();
				$('.multimedia-section .podcast-item-1 h4, .multimedia-section .podcast-item-2 h4').setAllToMaxHeight();
				$('.multimedia-section .podcast-item-3 h4, .multimedia-section .podcast-item-4 h4').setAllToMaxHeight();
				$('.multimedia-section .photostory-item-1 h4, .multimedia-section .photostory-item-2 h4').setAllToMaxHeight();
				$('.multimedia-section .photostory-item-3 h4, .multimedia-section .photostory-item-4 h4').setAllToMaxHeight();
				// cw
				$('.ts-item-2 h4, .ts-item-3 h4').setAllToMaxHeight();
				$('.ts-item-4 h4, .ts-item-5 h4').setAllToMaxHeight();
				$('.ts-item-6 h4, .ts-item-7 h4').setAllToMaxHeight();
				$('.ts-item-2 .ts-item-img, .ts-item-3 .ts-item-img').setAllToMaxHeight();
				$('.ts-item-4 .ts-item-img, .ts-item-5 .ts-item-img').setAllToMaxHeight();
				$('.ts-item-6 .ts-item-img, .ts-item-7 .ts-item-img').setAllToMaxHeight();
				$('.ln-item-1 h4, .ln-item-2 h4').setAllToMaxHeight();
				$('.ln-item-3 h4, .ln-item-4 h4').setAllToMaxHeight();
				$('.tips-item-1 h4, .tips-item-2 h4').setAllToMaxHeight();
				$('.tips-item-3 h4, .tips-item-4 h4').setAllToMaxHeight();
				$('.tips-item-1 > p, .tips-item-2 > p').setAllToMaxHeight();
				$('.tips-item-3 > p, .tips-item-4 > p').setAllToMaxHeight();
				$('.tips-home .opinions-item-1 h4, .tips-home .opinions-item-2 h4').setAllToMaxHeight();
				$('.tips-home .opinions-item-3 h4, .tips-home .opinions-item-4 h4').setAllToMaxHeight();
				$('.tips-home .opinions-item-1 > p, .tips-home .opinions-item-2 > p').setAllToMaxHeight();
				$('.tips-home .opinions-item-3 > p, .tips-home .opinions-item-4 > p').setAllToMaxHeight();
				$('.video-home .video-item-1 h4, .video-home .video-item-2 h4').setAllToMaxHeight();
				$('.video-home .video-item-3 h4, .video-home .video-item-4 h4').setAllToMaxHeight();
				// cw temp 
				$('.ln-item-1 .ln-item-img, .ln-item-2 .ln-item-img').setAllToMaxHeight();
				$('.ln-item-3 .ln-item-img, .ln-item-4 .ln-item-img').setAllToMaxHeight();
				$('.tips-item-1 .tips-item-img, .tips-item-2 .tips-item-img').setAllToMaxHeight();
				$('.tips-item-3 .tips-item-img, .tips-item-4 .tips-item-img').setAllToMaxHeight();
				// lemag cartoon
				$('#cartoon .multimedia-items li h4, #cartoon .multimedia-items li p').css('height', '');
				$('#cartoon .multimedia-items .multimedia-main-item-1 h4, #cartoon .multimedia-items .multimedia-main-item-2 h4').setAllToMaxHeight();
			} else {
				$('.nn-item-4').css('margin-right', '');
				$('.nn-item-5').width('');

				$('.tt-item-1 h3, .tt-item-2 h3, .tt-item-3 h3, .tt-item-4 h3, .tt-item-5 h3, .tt-item-6 h3, .tt-item-1 h4, .tt-item-2 h4, .tt-item-3 h4, .tt-item-4 h4, .tt-item-5 h4, .tt-item-6 h4, .tt-item-1 p, .tt-item-2 p, .tt-item-3 p, .tt-item-4 p, .tt-item-5 p, .tt-item-6 p, .epf-list-item-1 h4, .epf-list-item-2 h4, .latest-issue-features > ul:nth-child(2) > li h4, .latest-issue-features > ul:nth-child(3) > li h4, .latest-issue-news > ul:nth-child(2) > li h4, .latest-issue-news > ul:nth-child(3) > li h4, .latest-issue-columns > ul:nth-child(2) > li h4, .latest-issue-columns > ul:nth-child(3) > li h4, .headshots-hub-list li, .contributor-articles-list li h5, .contributor-alpha-list ul li, .network-hub-group > h3, .network-hub-group > p, .unisites-sections li h4, .unisites-sections li p, .top-stories-home div h4, .latest-news-home li h4, .video-home li h4, .multimedia-items li h4, .multimedia-items li p, .multimedia-section.video-section li h4, .multimedia-section.podcast-section li h4, .multimedia-section.photostory-section li h4').css('height', '');
			}

			// desktop breakpoint - equal heights
			// New and Notable resizing heights and title heights
			// Resize titles for News splash
			if(TT.mq === TT.mqs.desktop) {
				// reset heights
				$('.nn-item-1, .nn-item-2, .nn-item-3, .nn-item-4, .nn-item-5, .nn-item-6, .nn-item-7').css({'height' : '', 'display' : 'block'});
				$('.nn-item-1 h4, .nn-item-2 h4, .nn-item-3 h4, .nn-item-4 h4, .nn-item-5 h4, .nn-item-6 h4, .nn-item-7 h4, .find-solutions-sections-links li:first-child, .find-solutions-sections-links li:nth-child(2), .find-solutions-sections-links li:nth-child(3), .multimedia-sections-links li:first-child, .multimedia-sections-links li:nth-child(2), .multimedia-sections-links li:nth-child(3), .tt-item-1 h3, .tt-item-2 h3, .tt-item-3 h3, .tt-item-4 h3, .tt-item-5 h3, .tt-item-6 h3, .tt-item-1 h4, .tt-item-2 h4, .tt-item-3 h4, .tt-item-4 h4, .tt-item-5 h4, .tt-item-6 h4, .tt-item-1 p, .tt-item-2 p, .tt-item-3 p, .tt-item-4 p, .tt-item-5 p, .tt-item-6 p, .epf-list-item-1 h4, .epf-list-item-2 h4, .epf-list-item-3 h4, .latest-issue-features > ul:nth-child(2) > li h4, .latest-issue-features > ul:nth-child(3) > li h4, .latest-issue-news > ul:nth-child(2) > li h4, .latest-issue-news > ul:nth-child(3) > li h4, .latest-issue-columns > ul:nth-child(2) > li h4, .latest-issue-columns > ul:nth-child(3) > li h4, .headshots-hub-list li, .contributor-articles-list li h5, .contributor-alpha-list ul li, .refinery-latest-posts li, .network-hub-group > h3, .network-hub-group > p, .unisites-sections li h4, .unisites-sections li p, .top-stories-home div h4, .latest-news-home li h4, .latest-news-home li .ln-item-img, .tips-home li .tips-item-img, .in-depth-home li h4, .video-home li h4, .multimedia-items li h4, .multimedia-items li p, .multimedia-section.video-section li h4, .multimedia-section.podcast-section li h4, .multimedia-section.photostory-section li h4, .featured-authors ul li, .featured-authors .author-info').css('height', '');

				// resize heights
				$('.nn-item-2, .nn-item-3').setAllToMaxHeight().css('display', 'block');
				$('.nn-item-4, .nn-item-5').setAllToMaxHeight().css('display', 'block');
				$('.nn-item-6, .nn-item-7').setAllToMaxHeight().css('display', 'block');
				$('.nn-item-2 h4, .nn-item-3 h4').setAllToMaxHeight();
				$('.nn-item-4 h4, .nn-item-5 h4').setAllToMaxHeight();
				$('.nn-item-6 h4, .nn-item-7 h4').setAllToMaxHeight();
				$('.find-solutions-sections-links li:first-child').setAllToMaxHeight();
				$('.find-solutions-sections-links li:nth-child(2)').setAllToMaxHeight();
				$('.find-solutions-sections-links li:nth-child(3)').setAllToMaxHeight();
				$('.multimedia-sections-links li:first-child').setAllToMaxHeight();
				$('.multimedia-sections-links li:nth-child(2)').setAllToMaxHeight();
				$('.multimedia-sections-links li:nth-child(3)').setAllToMaxHeight();
				$('.tt-item-1 h3, .tt-item-2 h3').setAllToMaxHeight();
				$('.tt-item-3 h3, .tt-item-4 h3').setAllToMaxHeight();
				$('.tt-item-5 h3, .tt-item-6 h3').setAllToMaxHeight();
				$('.tt-item-1 h4, .tt-item-2 h4').setAllToMaxHeight();
				$('.tt-item-3 h4, .tt-item-4 h4').setAllToMaxHeight();
				$('.tt-item-5 h4, .tt-item-6 h4').setAllToMaxHeight();
				$('.tt-item-1 p, .tt-item-2 p').setAllToMaxHeight();
				$('.tt-item-3 p, .tt-item-4 p').setAllToMaxHeight();
				$('.tt-item-5 p, .tt-item-6 p').setAllToMaxHeight();
				$('.epf-list-item-1 h4, .epf-list-item-2 h4, .epf-list-item-3 h4').setAllToMaxHeight();
				$('.latest-issue-features > ul:nth-child(2) > li h4').setAllToMaxHeight();
				$('.latest-issue-features > ul:nth-child(3) > li h4').setAllToMaxHeight();
				$('.latest-issue-news > ul:nth-child(2) > li h4').setAllToMaxHeight();
				$('.latest-issue-news > ul:nth-child(3) > li h4').setAllToMaxHeight();
				$('.latest-issue-columns > ul:nth-child(2) > li h4').setAllToMaxHeight();
				$('.latest-issue-columns > ul:nth-child(3) > li h4').setAllToMaxHeight();
				$('.hub-list-item-1, .hub-list-item-2, .hub-list-item-3').setAllToMaxHeight();
				$('.hub-list-item-4, .hub-list-item-5, .hub-list-item-6').setAllToMaxHeight();
				$('.hub-list-item-7, .hub-list-item-8, .hub-list-item-9').setAllToMaxHeight();
				$('.hub-list-item-10, .hub-list-item-11, .hub-list-item-12').setAllToMaxHeight();
				$('.hub-list-item-13, .hub-list-item-14, .hub-list-item-15').setAllToMaxHeight();
				$('.hub-list-item-16, .hub-list-item-17, .hub-list-item-18').setAllToMaxHeight();
				$('.hub-list-item-19, .hub-list-item-20, .hub-list-item-21').setAllToMaxHeight();
				$('.hub-list-item-22, .hub-list-item-23, .hub-list-item-24').setAllToMaxHeight();
				$('.hub-list-item-25, .hub-list-item-26, .hub-list-item-27').setAllToMaxHeight();
				$('.hub-list-item-28, .hub-list-item-29, .hub-list-item-30').setAllToMaxHeight();
				$('.hub-list-item-31, .hub-list-item-32, .hub-list-item-33').setAllToMaxHeight();
				$('.hub-list-item-34, .hub-list-item-35, .hub-list-item-36').setAllToMaxHeight();
				$('.hub-list-item-37, .hub-list-item-38, .hub-list-item-39').setAllToMaxHeight();
				$('.hub-list-item-40, .hub-list-item-41, .hub-list-item-42').setAllToMaxHeight();
				$('.hub-list-item-43, .hub-list-item-44, .hub-list-item-45').setAllToMaxHeight();
				$('.hub-list-item-46, .hub-list-item-47, .hub-list-item-48').setAllToMaxHeight();
				$('.hub-list-item-49, .hub-list-item-50').setAllToMaxHeight();
				$('.alpha-list-item-1, .alpha-list-item-2, .alpha-list-item-3').setAllToMaxHeight();
				$('.alpha-list-item-4, .alpha-list-item-5, .alpha-list-item-6').setAllToMaxHeight();
				$('.alpha-list-item-7, .alpha-list-item-8, .alpha-list-item-9').setAllToMaxHeight();
				$('.alpha-list-item-10, .alpha-list-item-11, .alpha-list-item-12').setAllToMaxHeight();
				$('.alpha-list-item-13, .alpha-list-item-14, .alpha-list-item-15').setAllToMaxHeight();
				$('.alpha-list-item-16, .alpha-list-item-17, .alpha-list-item-18').setAllToMaxHeight();
				$('.alpha-list-item-19, .alpha-list-item-20, .alpha-list-item-21').setAllToMaxHeight();
				$('.alpha-list-item-22, .alpha-list-item-23, .alpha-list-item-24').setAllToMaxHeight();
				$('.alpha-list-item-25, .alpha-list-item-26, .alpha-list-item-27').setAllToMaxHeight();
				$('.alpha-list-item-28, .alpha-list-item-29, .alpha-list-item-30').setAllToMaxHeight();
				$('.alpha-list-item-31, .alpha-list-item-32, .alpha-list-item-33').setAllToMaxHeight();
				$('.alpha-list-item-34, .alpha-list-item-35, .alpha-list-item-36').setAllToMaxHeight();
				$('.alpha-list-item-37, .alpha-list-item-38, .alpha-list-item-39').setAllToMaxHeight();
				$('.alpha-list-item-40, .alpha-list-item-41, .alpha-list-item-42').setAllToMaxHeight();
				$('.alpha-list-item-43, .alpha-list-item-44, .alpha-list-item-45').setAllToMaxHeight();
				$('.alpha-list-item-46, .alpha-list-item-47, .alpha-list-item-48').setAllToMaxHeight();
				$('.alpha-list-item-49, .alpha-list-item-50, .alpha-list-item-51').setAllToMaxHeight();
				$('.alpha-list-item-52, .alpha-list-item-53, .alpha-list-item-54').setAllToMaxHeight();
				$('.alpha-list-item-55, .alpha-list-item-56, .alpha-list-item-57').setAllToMaxHeight();
				$('.alpha-list-item-58, .alpha-list-item-59, .alpha-list-item-60').setAllToMaxHeight();
				$('.alpha-list-item-61, .alpha-list-item-62, .alpha-list-item-63').setAllToMaxHeight();
				$('.alpha-list-item-64, .alpha-list-item-65, .alpha-list-item-66').setAllToMaxHeight();
				$('.alpha-list-item-67, .alpha-list-item-68, .alpha-list-item-69').setAllToMaxHeight();
				$('.alpha-list-item-70, .alpha-list-item-71, .alpha-list-item-72').setAllToMaxHeight();
				$('.alpha-list-item-73, .alpha-list-item-74, .alpha-list-item-75').setAllToMaxHeight();
				$('.alpha-list-item-76, .alpha-list-item-77, .alpha-list-item-78').setAllToMaxHeight();
				$('.alpha-list-item-79, .alpha-list-item-80, .alpha-list-item-81').setAllToMaxHeight();
				$('.alpha-list-item-82, .alpha-list-item-83, .alpha-list-item-84').setAllToMaxHeight();
				$('.alpha-list-item-85, .alpha-list-item-86, .alpha-list-item-87').setAllToMaxHeight();
				$('.alpha-list-item-88, .alpha-list-item-89, .alpha-list-item-90').setAllToMaxHeight();
				$('.alpha-list-item-91, .alpha-list-item-92, .alpha-list-item-93').setAllToMaxHeight();
				$('.alpha-list-item-94, .alpha-list-item-95, .alpha-list-item-96').setAllToMaxHeight();
				$('.alpha-list-item-97, .alpha-list-item-98, .alpha-list-item-99').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-1 h5, .contributor-articles-list .list-item-2 h5, .contributor-articles-list .list-item-3 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-4 h5, .contributor-articles-list .list-item-5 h5, .contributor-articles-list .list-item-6 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-7 h5, .contributor-articles-list .list-item-8 h5, .contributor-articles-list .list-item-9 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-10 h5, .contributor-articles-list .list-item-11 h5, .contributor-articles-list .list-item-12 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-13 h5, .contributor-articles-list .list-item-14 h5, .contributor-articles-list .list-item-15 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-16 h5, .contributor-articles-list .list-item-17 h5, .contributor-articles-list .list-item-18 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-19 h5, .contributor-articles-list .list-item-20 h5, .contributor-articles-list .list-item-21 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-22 h5, .contributor-articles-list .list-item-23 h5, .contributor-articles-list .list-item-24 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-25 h5, .contributor-articles-list .list-item-26 h5, .contributor-articles-list .list-item-27 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-28 h5, .contributor-articles-list .list-item-29 h5, .contributor-articles-list .list-item-30 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-31 h5, .contributor-articles-list .list-item-32 h5, .contributor-articles-list .list-item-33 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-34 h5, .contributor-articles-list .list-item-35 h5, .contributor-articles-list .list-item-36 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-37 h5, .contributor-articles-list .list-item-38 h5, .contributor-articles-list .list-item-39 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-40 h5, .contributor-articles-list .list-item-41 h5, .contributor-articles-list .list-item-42 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-43 h5, .contributor-articles-list .list-item-44 h5, .contributor-articles-list .list-item-45 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-46 h5, .contributor-articles-list .list-item-47 h5, .contributor-articles-list .list-item-48 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-49 h5, .contributor-articles-list .list-item-50 h5, .contributor-articles-list .list-item-51 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-52 h5, .contributor-articles-list .list-item-53 h5, .contributor-articles-list .list-item-54 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-55 h5, .contributor-articles-list .list-item-56 h5, .contributor-articles-list .list-item-57 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-58 h5, .contributor-articles-list .list-item-59 h5, .contributor-articles-list .list-item-60 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-61 h5, .contributor-articles-list .list-item-62 h5, .contributor-articles-list .list-item-63 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-64 h5, .contributor-articles-list .list-item-65 h5, .contributor-articles-list .list-item-66 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-67 h5, .contributor-articles-list .list-item-68 h5, .contributor-articles-list .list-item-69 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-70 h5, .contributor-articles-list .list-item-71 h5, .contributor-articles-list .list-item-72 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-73 h5, .contributor-articles-list .list-item-74 h5, .contributor-articles-list .list-item-75 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-76 h5, .contributor-articles-list .list-item-77 h5, .contributor-articles-list .list-item-78 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-79 h5, .contributor-articles-list .list-item-80 h5, .contributor-articles-list .list-item-81 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-82 h5, .contributor-articles-list .list-item-83 h5, .contributor-articles-list .list-item-84 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-85 h5, .contributor-articles-list .list-item-86 h5, .contributor-articles-list .list-item-87 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-88 h5, .contributor-articles-list .list-item-89 h5, .contributor-articles-list .list-item-90 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-91 h5, .contributor-articles-list .list-item-92 h5, .contributor-articles-list .list-item-93 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-94 h5, .contributor-articles-list .list-item-95 h5, .contributor-articles-list .list-item-96 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-97 h5, .contributor-articles-list .list-item-98 h5, .contributor-articles-list .list-item-99 h5').setAllToMaxHeight();
				$('.refinery-latest-posts.list-1 li').setAllToMaxHeight();
				$('.refinery-latest-posts.list-2 li').setAllToMaxHeight();
				$('.refinery-latest-posts.list-3 li').setAllToMaxHeight();
				$('.refinery-latest-posts.list-4 li').setAllToMaxHeight();
				$('.network-hub-group.item-1 > h3, .network-hub-group.item-2 > h3, .network-hub-group.item-3 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-4 > h3, .network-hub-group.item-5 > h3, .network-hub-group.item-6 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-7 > h3, .network-hub-group.item-8 > h3, .network-hub-group.item-9 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-10 > h3, .network-hub-group.item-11 > h3, .network-hub-group.item-12 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-13 > h3, .network-hub-group.item-14 > h3, .network-hub-group.item-15 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-1 > p, .network-hub-group.item-2 > p, .network-hub-group.item-3 > p').setAllToMaxHeight();
				$('.network-hub-group.item-4 > p, .network-hub-group.item-5 > p, .network-hub-group.item-6 > p').setAllToMaxHeight();
				$('.network-hub-group.item-7 > p, .network-hub-group.item-8 > p, .network-hub-group.item-9 > p').setAllToMaxHeight();
				$('.network-hub-group.item-10 > p, .network-hub-group.item-11 > p, .network-hub-group.item-12 > p').setAllToMaxHeight();
				$('.network-hub-group.item-13 > p, .network-hub-group.item-14 > p, .network-hub-group.item-15 > p').setAllToMaxHeight();
				$('.unisites-sections .item-1 h4, .unisites-sections .item-2 h4, .unisites-sections .item-3 h4').setAllToMaxHeight();
				$('.unisites-sections .item-4 h4, .unisites-sections .item-5 h4, .unisites-sections .item-6 h4').setAllToMaxHeight();
				$('.unisites-sections .item-7 h4, .unisites-sections .item-8 h4, .unisites-sections .item-9 h4').setAllToMaxHeight();
				$('.unisites-sections .item-10 h4, .unisites-sections .item-11 h4, .unisites-sections .item-12 h4').setAllToMaxHeight();
				$('.unisites-sections .item-13 h4, .unisites-sections .item-14 h4, .unisites-sections .item-15 h4').setAllToMaxHeight();
				$('.unisites-sections .item-16 h4, .unisites-sections .item-17 h4, .unisites-sections .item-18 h4').setAllToMaxHeight();
				$('.unisites-sections .item-19 h4, .unisites-sections .item-20 h4, .unisites-sections .item-21 h4').setAllToMaxHeight();
				$('.unisites-sections .item-22 h4, .unisites-sections .item-23 h4, .unisites-sections .item-24 h4').setAllToMaxHeight();
				$('.unisites-sections .item-1 p, .unisites-sections .item-2 p, .unisites-sections .item-3 p').setAllToMaxHeight();
				$('.unisites-sections .item-4 p, .unisites-sections .item-5 p, .unisites-sections .item-6 p').setAllToMaxHeight();
				$('.unisites-sections .item-7 p, .unisites-sections .item-8 p, .unisites-sections .item-9 p').setAllToMaxHeight();
				$('.unisites-sections .item-10 p, .unisites-sections .item-11 p, .unisites-sections .item-12 p').setAllToMaxHeight();
				$('.unisites-sections .item-13 p, .unisites-sections .item-14 p, .unisites-sections .item-15 p').setAllToMaxHeight();
				$('.unisites-sections .item-16 p, .unisites-sections .item-17 p, .unisites-sections .item-18 p').setAllToMaxHeight();
				$('.unisites-sections .item-19 p, .unisites-sections .item-20 p, .unisites-sections .item-21 p').setAllToMaxHeight();
				$('.unisites-sections .item-22 p, .unisites-sections .item-23 p, .unisites-sections .item-24 p').setAllToMaxHeight();
				$('.multimedia-main-item-1 h4, .multimedia-main-item-2 h4').setAllToMaxHeight();
				$('.multimedia-item-1 h4, .multimedia-item-2 h4').setAllToMaxHeight();
				$('.multimedia-item-3 h4, .multimedia-item-4 h4').setAllToMaxHeight();
				$('.multimedia-item-5 h4, .multimedia-item-6 h4').setAllToMaxHeight();
				$('.multimedia-item-7 h4, .multimedia-item-8 h4').setAllToMaxHeight();
				$('.multimedia-item-9 h4, .multimedia-item-10 h4').setAllToMaxHeight();
				$('.multimedia-item-11 h4, .multimedia-item-12 h4').setAllToMaxHeight();
				$('.multimedia-item-13 h4, .multimedia-item-14 h4').setAllToMaxHeight();
				$('.multimedia-item-15 h4, .multimedia-item-16 h4').setAllToMaxHeight();
				$('.multimedia-item-17 h4, .multimedia-item-18 h4').setAllToMaxHeight();
				$('.multimedia-item-19 h4, .multimedia-item-20 h4').setAllToMaxHeight();
				$('.multimedia-item-21 h4, .multimedia-item-22 h4').setAllToMaxHeight();
				$('.multimedia-item-23 h4, .multimedia-item-24 h4').setAllToMaxHeight();
				$('.multimedia-main-item-1 > p, .multimedia-main-item-2 > p').setAllToMaxHeight();
				$('.multimedia-item-1 > p, .multimedia-item-2 > p').setAllToMaxHeight();
				$('.multimedia-item-3 > p, .multimedia-item-4 > p').setAllToMaxHeight();
				$('.multimedia-item-5 > p, .multimedia-item-6 > p').setAllToMaxHeight();
				$('.multimedia-item-7 > p, .multimedia-item-8 > p').setAllToMaxHeight();
				$('.multimedia-item-9 > p, .multimedia-item-10 > p').setAllToMaxHeight();
				$('.multimedia-item-11 > p, .multimedia-item-12 > p').setAllToMaxHeight();
				$('.multimedia-item-13 > p, .multimedia-item-14 > p').setAllToMaxHeight();
				$('.multimedia-item-15 > p, .multimedia-item-16 > p').setAllToMaxHeight();
				$('.multimedia-item-17 > p, .multimedia-item-18 > p').setAllToMaxHeight();
				$('.multimedia-item-19 > p, .multimedia-item-20 > p').setAllToMaxHeight();
				$('.multimedia-item-21 > p, .multimedia-item-22 > p').setAllToMaxHeight();
				$('.multimedia-item-23 > p, .multimedia-item-24 > p').setAllToMaxHeight();
				$('.multimedia-section .video-item-1 h4, .multimedia-section .video-item-2 h4').setAllToMaxHeight();
				$('.multimedia-section .video-item-3 h4, .multimedia-section .video-item-4 h4').setAllToMaxHeight();
				$('.multimedia-section .podcast-item-1 h4, .multimedia-section .podcast-item-2 h4').setAllToMaxHeight();
				$('.multimedia-section .podcast-item-3 h4, .multimedia-section .podcast-item-4 h4').setAllToMaxHeight();
				$('.multimedia-section .podcast-item-5 h4, .multimedia-section .podcast-item-6 h4').setAllToMaxHeight();
				$('.multimedia-section .photostory-item-1 h4, .multimedia-section .photostory-item-2 h4').setAllToMaxHeight();
				$('.multimedia-section .photostory-item-3 h4, .multimedia-section .photostory-item-4 h4').setAllToMaxHeight();
				$('#podcast .multimedia-section .video-item-5 h4, #podcast .multimedia-section .video-item-6 h4').setAllToMaxHeight();
				// cw
				$('.ts-item-2 h4, .ts-item-3 h4').setAllToMaxHeight();
				$('.ts-item-4 h4, .ts-item-5 h4').setAllToMaxHeight();
				$('.ts-item-6 h4, .ts-item-7 h4').setAllToMaxHeight();
				$('.ts-item-2 .ts-item-img, .ts-item-3 .ts-item-img').setAllToMaxHeight();
				$('.ts-item-4 .ts-item-img, .ts-item-5 .ts-item-img').setAllToMaxHeight();
				$('.ts-item-6 .ts-item-img, .ts-item-7 .ts-item-img').setAllToMaxHeight();
				$('.ln-item-1 h4, .ln-item-2 h4').setAllToMaxHeight();
				$('.ln-item-3 h4, .ln-item-4 h4').setAllToMaxHeight();
				$('.tips-item-1 h4, .tips-item-2 h4').setAllToMaxHeight();
				$('.tips-item-3 h4, .tips-item-4 h4').setAllToMaxHeight();
				$('.tips-item-1 > p, .tips-item-2 > p').setAllToMaxHeight();
				$('.tips-item-3 > p, .tips-item-4 > p').setAllToMaxHeight();
				$('.tips-home .opinions-item-1 h4, .tips-home .opinions-item-2 h4').setAllToMaxHeight();
				$('.tips-home .opinions-item-3 h4, .tips-home .opinions-item-4 h4').setAllToMaxHeight();
				$('.tips-home .opinions-item-1 > p, .tips-home .opinions-item-2 > p').setAllToMaxHeight();
				$('.tips-home .opinions-item-3 > p, .tips-home .opinions-item-4 > p').setAllToMaxHeight();
				$('.id-item-1 h4, .id-item-2 h4').setAllToMaxHeight();
				$('.video-home .video-item-1 h4, .video-home .video-item-2 h4').setAllToMaxHeight();
				$('.video-home .video-item-3 h4, .video-home .video-item-4 h4').setAllToMaxHeight();
				// lemag cartoon
				$('#cartoon .multimedia-items li h4, #cartoon .multimedia-items li p').css('height', '');
				$('#cartoon .multimedia-items .multimedia-main-item-1 h4, #cartoon .multimedia-items .multimedia-main-item-2 h4').setAllToMaxHeight();
				// tss
				$('.fas-item-1 .author-info, .fas-item-2 .author-info').setAllToMaxHeight();
				$('.fas-item-3 .author-info, .fas-item-4 .author-info').setAllToMaxHeight();
				$('.fas-item-5 .author-info, .fas-item-6 .author-info').setAllToMaxHeight();
				$('.fas-item-1, .fas-item-2').setAllToMaxHeight();
				$('.fas-item-3, .fas-item-4').setAllToMaxHeight();
				$('.fas-item-5, .fas-item-6').setAllToMaxHeight();

				// resize titles for News splash because tablet has adjustable width for titles
				$('.topic-news-item h3, .topic-news-item .topic-news-description').css('height', '');

				var topicsMinHeight = $('.nn-item-1').height() + $('.nn-item-2').height() + 80;
				$('.homepage-topics').css('min-height', topicsMinHeight);

				// Move ad on contributor page to align top
				if($('#contributor .contributor-bio').length > 0) {
					var cBioPos = $('#contributor .contributor-bio').position().top;
					$('.contributor-header .page-header-ad').css('top', cBioPos + 60);
				}

			} else if (TT.mq === TT.mqs.desktop_w) {
				// reset heights
				$('.contributor-header .page-header-ad').css('top', '');
				$('.nn-item-1, .nn-item-2, .nn-item-3, .nn-item-4, .nn-item-5, .nn-item-6, .nn-item-7').css('height', '').css('display', 'block');
				$('.nn-item-1 h4, .nn-item-2 h4, .nn-item-3 h4, .nn-item-4 h4, .nn-item-5 h4, .nn-item-6 h4, .nn-item-7 h4, .find-solutions-sections-links li:first-child, .find-solutions-sections-links li:nth-child(2), .find-solutions-sections-links li:nth-child(3), .multimedia-sections-links li:first-child, .multimedia-sections-links li:nth-child(2), .multimedia-sections-links li:nth-child(3), .tt-item-1 h3, .tt-item-2 h3, .tt-item-3 h3, .tt-item-4 h3, .tt-item-5 h3, .tt-item-6 h3, .tt-item-1 h4, .tt-item-2 h4, .tt-item-3 h4, .tt-item-4 h4, .tt-item-5 h4, .tt-item-6 h4, .tt-item-1 p, .tt-item-2 p, .tt-item-3 p, .tt-item-4 p, .tt-item-5 p, .tt-item-6 p, .epf-list-item-1 h4, .epf-list-item-2 h4, .epf-list-item-3 h4, .latest-issue-features > ul:nth-child(2) > li h4, .latest-issue-features > ul:nth-child(3) > li h4, .latest-issue-news > ul:nth-child(2) > li h4, .latest-issue-news > ul:nth-child(3) > li h4, .latest-issue-columns > ul:nth-child(2) > li h4, .latest-issue-columns > ul:nth-child(3) > li h4, .headshots-hub-list li, .contributor-articles-list li, .contributor-alpha-list ul li h5, .refinery-latest-posts li, .network-hub-group > h3, .network-hub-group > p, .unisites-sections li h4, .unisites-sections li p, .top-stories-home div h4, .latest-news-home li h4, .video-home li h4, .multimedia-items li h4, .multimedia-items li p, .multimedia-section.video-section li h4, .multimedia-section.podcast-section li h4, .multimedia-section.photostory-section li h4, .featured-authors ul li, .featured-authors .author-info').css('height', '');

				// resize heights
				$('.nn-item-1, .nn-item-2').setAllToMaxHeight().css('display', 'block');
				$('.nn-item-3 h4, .nn-item-4 h4, .nn-item-5 h4').setAllToMaxHeight();
				$('.nn-item-6 h4, .nn-item-7 h4, .nn-item-8 h4').setAllToMaxHeight();
				$('.homepage-topics').css('min-height', $('.nn-item-1').height() + 60);
				$('.find-solutions-sections-links li:first-child').setAllToMaxHeight();
				$('.find-solutions-sections-links li:nth-child(2)').setAllToMaxHeight();
				$('.find-solutions-sections-links li:nth-child(3)').setAllToMaxHeight();
				$('.multimedia-sections-links li:first-child').setAllToMaxHeight();
				$('.multimedia-sections-links li:nth-child(2)').setAllToMaxHeight();
				$('.multimedia-sections-links li:nth-child(3)').setAllToMaxHeight();
				$('.tt-item-1 h3, .tt-item-2 h3, .tt-item-3 h3').setAllToMaxHeight();
				$('.tt-item-4 h3, .tt-item-5 h3, .tt-item-6 h3').setAllToMaxHeight();
				$('.tt-item-1 h4, .tt-item-2 h4, .tt-item-3 h4').setAllToMaxHeight();
				$('.tt-item-4 h4, .tt-item-5 h4, .tt-item-6 h4').setAllToMaxHeight();
				$('.tt-item-1 p, .tt-item-2 p, .tt-item-3 p').setAllToMaxHeight();
				$('.tt-item-4 p, .tt-item-5 p, .tt-item-6 p').setAllToMaxHeight();
				$('.epf-list-item-1 h4, .epf-list-item-2 h4, .epf-list-item-3 h4').setAllToMaxHeight();
				$('.latest-issue-features > ul:nth-child(2) > li h4').setAllToMaxHeight();
				$('.latest-issue-features > ul:nth-child(3) > li h4').setAllToMaxHeight();
				$('.latest-issue-news > ul:nth-child(2) > li h4').setAllToMaxHeight();
				$('.latest-issue-news > ul:nth-child(3) > li h4').setAllToMaxHeight();
				$('.latest-issue-columns > ul:nth-child(2) > li h4').setAllToMaxHeight();
				$('.latest-issue-columns > ul:nth-child(3) > li h4').setAllToMaxHeight();
				$('.hub-list-item-1, .hub-list-item-2, .hub-list-item-3, .hub-list-item-4').setAllToMaxHeight();
				$('.hub-list-item-5, .hub-list-item-6, .hub-list-item-7, .hub-list-item-8').setAllToMaxHeight();
				$('.hub-list-item-9, .hub-list-item-10, .hub-list-item-11, .hub-list-item-12').setAllToMaxHeight();
				$('.hub-list-item-13, .hub-list-item-14, .hub-list-item-15, .hub-list-item-16').setAllToMaxHeight();
				$('.hub-list-item-17, .hub-list-item-18, .hub-list-item-19, .hub-list-item-20').setAllToMaxHeight();
				$('.hub-list-item-21, .hub-list-item-22, .hub-list-item-23, .hub-list-item-24').setAllToMaxHeight();
				$('.hub-list-item-25, .hub-list-item-26, .hub-list-item-27, .hub-list-item-28').setAllToMaxHeight();
				$('.hub-list-item-29, .hub-list-item-30, .hub-list-item-31, .hub-list-item-32').setAllToMaxHeight();
				$('.hub-list-item-33, .hub-list-item-34, .hub-list-item-35, .hub-list-item-36').setAllToMaxHeight();
				$('.hub-list-item-37, .hub-list-item-38, .hub-list-item-39, .hub-list-item-40').setAllToMaxHeight();
				$('.hub-list-item-41, .hub-list-item-42, .hub-list-item-43, .hub-list-item-44').setAllToMaxHeight();
				$('.hub-list-item-45, .hub-list-item-46, .hub-list-item-47, .hub-list-item-48').setAllToMaxHeight();
				$('.hub-list-item-49, .hub-list-item-50').setAllToMaxHeight();
				$('.alpha-list-item-1, .alpha-list-item-2, .alpha-list-item-3').setAllToMaxHeight();
				$('.alpha-list-item-4, .alpha-list-item-5, .alpha-list-item-6').setAllToMaxHeight();
				$('.alpha-list-item-7, .alpha-list-item-8, .alpha-list-item-9').setAllToMaxHeight();
				$('.alpha-list-item-10, .alpha-list-item-11, .alpha-list-item-12').setAllToMaxHeight();
				$('.alpha-list-item-13, .alpha-list-item-14, .alpha-list-item-15').setAllToMaxHeight();
				$('.alpha-list-item-16, .alpha-list-item-17, .alpha-list-item-18').setAllToMaxHeight();
				$('.alpha-list-item-19, .alpha-list-item-20, .alpha-list-item-21').setAllToMaxHeight();
				$('.alpha-list-item-22, .alpha-list-item-23, .alpha-list-item-24').setAllToMaxHeight();
				$('.alpha-list-item-25, .alpha-list-item-26, .alpha-list-item-27').setAllToMaxHeight();
				$('.alpha-list-item-28, .alpha-list-item-29, .alpha-list-item-30').setAllToMaxHeight();
				$('.alpha-list-item-31, .alpha-list-item-32, .alpha-list-item-33').setAllToMaxHeight();
				$('.alpha-list-item-34, .alpha-list-item-35, .alpha-list-item-36').setAllToMaxHeight();
				$('.alpha-list-item-37, .alpha-list-item-38, .alpha-list-item-39').setAllToMaxHeight();
				$('.alpha-list-item-40, .alpha-list-item-41, .alpha-list-item-42').setAllToMaxHeight();
				$('.alpha-list-item-43, .alpha-list-item-44, .alpha-list-item-45').setAllToMaxHeight();
				$('.alpha-list-item-46, .alpha-list-item-47, .alpha-list-item-48').setAllToMaxHeight();
				$('.alpha-list-item-49, .alpha-list-item-50, .alpha-list-item-51').setAllToMaxHeight();
				$('.alpha-list-item-52, .alpha-list-item-53, .alpha-list-item-54').setAllToMaxHeight();
				$('.alpha-list-item-55, .alpha-list-item-56, .alpha-list-item-57').setAllToMaxHeight();
				$('.alpha-list-item-58, .alpha-list-item-59, .alpha-list-item-60').setAllToMaxHeight();
				$('.alpha-list-item-61, .alpha-list-item-62, .alpha-list-item-63').setAllToMaxHeight();
				$('.alpha-list-item-64, .alpha-list-item-65, .alpha-list-item-66').setAllToMaxHeight();
				$('.alpha-list-item-67, .alpha-list-item-68, .alpha-list-item-69').setAllToMaxHeight();
				$('.alpha-list-item-70, .alpha-list-item-71, .alpha-list-item-72').setAllToMaxHeight();
				$('.alpha-list-item-73, .alpha-list-item-74, .alpha-list-item-75').setAllToMaxHeight();
				$('.alpha-list-item-76, .alpha-list-item-77, .alpha-list-item-78').setAllToMaxHeight();
				$('.alpha-list-item-79, .alpha-list-item-80, .alpha-list-item-81').setAllToMaxHeight();
				$('.alpha-list-item-82, .alpha-list-item-83, .alpha-list-item-84').setAllToMaxHeight();
				$('.alpha-list-item-85, .alpha-list-item-86, .alpha-list-item-87').setAllToMaxHeight();
				$('.alpha-list-item-88, .alpha-list-item-89, .alpha-list-item-90').setAllToMaxHeight();
				$('.alpha-list-item-91, .alpha-list-item-92, .alpha-list-item-93').setAllToMaxHeight();
				$('.alpha-list-item-94, .alpha-list-item-95, .alpha-list-item-96').setAllToMaxHeight();
				$('.alpha-list-item-97, .alpha-list-item-98, .alpha-list-item-99').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-1 h5, .contributor-articles-list .list-item-2 h5, .contributor-articles-list .list-item-3 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-4 h5, .contributor-articles-list .list-item-5 h5, .contributor-articles-list .list-item-6 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-7 h5, .contributor-articles-list .list-item-8 h5, .contributor-articles-list .list-item-9 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-10 h5, .contributor-articles-list .list-item-11 h5, .contributor-articles-list .list-item-12 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-13 h5, .contributor-articles-list .list-item-14 h5, .contributor-articles-list .list-item-15 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-16 h5, .contributor-articles-list .list-item-17 h5, .contributor-articles-list .list-item-18 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-19 h5, .contributor-articles-list .list-item-20 h5, .contributor-articles-list .list-item-21 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-22 h5, .contributor-articles-list .list-item-23 h5, .contributor-articles-list .list-item-24 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-25 h5, .contributor-articles-list .list-item-26 h5, .contributor-articles-list .list-item-27 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-28 h5, .contributor-articles-list .list-item-29 h5, .contributor-articles-list .list-item-30 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-31 h5, .contributor-articles-list .list-item-32 h5, .contributor-articles-list .list-item-33 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-34 h5, .contributor-articles-list .list-item-35 h5, .contributor-articles-list .list-item-36 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-37 h5, .contributor-articles-list .list-item-38 h5, .contributor-articles-list .list-item-39 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-40 h5, .contributor-articles-list .list-item-41 h5, .contributor-articles-list .list-item-42 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-43 h5, .contributor-articles-list .list-item-44 h5, .contributor-articles-list .list-item-45 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-46 h5, .contributor-articles-list .list-item-47 h5, .contributor-articles-list .list-item-48 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-49 h5, .contributor-articles-list .list-item-50 h5, .contributor-articles-list .list-item-51 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-52 h5, .contributor-articles-list .list-item-53 h5, .contributor-articles-list .list-item-54 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-55 h5, .contributor-articles-list .list-item-56 h5, .contributor-articles-list .list-item-57 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-58 h5, .contributor-articles-list .list-item-59 h5, .contributor-articles-list .list-item-60 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-61 h5, .contributor-articles-list .list-item-62 h5, .contributor-articles-list .list-item-63 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-64 h5, .contributor-articles-list .list-item-65 h5, .contributor-articles-list .list-item-66 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-67 h5, .contributor-articles-list .list-item-68 h5, .contributor-articles-list .list-item-69 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-70 h5, .contributor-articles-list .list-item-71 h5, .contributor-articles-list .list-item-72 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-73 h5, .contributor-articles-list .list-item-74 h5, .contributor-articles-list .list-item-75 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-76 h5, .contributor-articles-list .list-item-77 h5, .contributor-articles-list .list-item-78 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-79 h5, .contributor-articles-list .list-item-80 h5, .contributor-articles-list .list-item-81 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-82 h5, .contributor-articles-list .list-item-83 h5, .contributor-articles-list .list-item-84 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-85 h5, .contributor-articles-list .list-item-86 h5, .contributor-articles-list .list-item-87 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-88 h5, .contributor-articles-list .list-item-89 h5, .contributor-articles-list .list-item-90 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-91 h5, .contributor-articles-list .list-item-92 h5, .contributor-articles-list .list-item-93 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-94 h5, .contributor-articles-list .list-item-95 h5, .contributor-articles-list .list-item-96 h5').setAllToMaxHeight();
				$('.contributor-articles-list .list-item-97 h5, .contributor-articles-list .list-item-98 h5, .contributor-articles-list .list-item-99 h5').setAllToMaxHeight();
				$('.refinery-latest-posts.list-1 li').setAllToMaxHeight();
				$('.refinery-latest-posts.list-2 li').setAllToMaxHeight();
				$('.refinery-latest-posts.list-3 li').setAllToMaxHeight();
				$('.refinery-latest-posts.list-4 li').setAllToMaxHeight();
				$('.network-hub-group.item-1 > h3, .network-hub-group.item-2 > h3, .network-hub-group.item-3 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-4 > h3, .network-hub-group.item-5 > h3, .network-hub-group.item-6 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-7 > h3, .network-hub-group.item-8 > h3, .network-hub-group.item-9 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-10 > h3, .network-hub-group.item-11 > h3, .network-hub-group.item-12 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-13 > h3, .network-hub-group.item-14 > h3, .network-hub-group.item-15 > h3').setAllToMaxHeight();
				$('.network-hub-group.item-1 > p, .network-hub-group.item-2 > p, .network-hub-group.item-3 > p').setAllToMaxHeight();
				$('.network-hub-group.item-4 > p, .network-hub-group.item-5 > p, .network-hub-group.item-6 > p').setAllToMaxHeight();
				$('.network-hub-group.item-7 > p, .network-hub-group.item-8 > p, .network-hub-group.item-9 > p').setAllToMaxHeight();
				$('.network-hub-group.item-10 > p, .network-hub-group.item-11 > p, .network-hub-group.item-12 > p').setAllToMaxHeight();
				$('.network-hub-group.item-13 > p, .network-hub-group.item-14 > p, .network-hub-group.item-15 > p').setAllToMaxHeight();
				$('.unisites-sections .item-1 h4, .unisites-sections .item-2 h4, .unisites-sections .item-3 h4').setAllToMaxHeight();
				$('.unisites-sections .item-4 h4, .unisites-sections .item-5 h4, .unisites-sections .item-6 h4').setAllToMaxHeight();
				$('.unisites-sections .item-7 h4, .unisites-sections .item-8 h4, .unisites-sections .item-9 h4').setAllToMaxHeight();
				$('.unisites-sections .item-10 h4, .unisites-sections .item-11 h4, .unisites-sections .item-12 h4').setAllToMaxHeight();
				$('.unisites-sections .item-13 h4, .unisites-sections .item-14 h4, .unisites-sections .item-15 h4').setAllToMaxHeight();
				$('.unisites-sections .item-16 h4, .unisites-sections .item-17 h4, .unisites-sections .item-18 h4').setAllToMaxHeight();
				$('.unisites-sections .item-19 h4, .unisites-sections .item-20 h4, .unisites-sections .item-21 h4').setAllToMaxHeight();
				$('.unisites-sections .item-22 h4, .unisites-sections .item-23 h4, .unisites-sections .item-24 h4').setAllToMaxHeight();
				$('.unisites-sections .item-1 p, .unisites-sections .item-2 p, .unisites-sections .item-3 p').setAllToMaxHeight();
				$('.unisites-sections .item-4 p, .unisites-sections .item-5 p, .unisites-sections .item-6 p').setAllToMaxHeight();
				$('.unisites-sections .item-7 p, .unisites-sections .item-8 p, .unisites-sections .item-9 p').setAllToMaxHeight();
				$('.unisites-sections .item-10 p, .unisites-sections .item-11 p, .unisites-sections .item-12 p').setAllToMaxHeight();
				$('.unisites-sections .item-13 p, .unisites-sections .item-14 p, .unisites-sections .item-15 p').setAllToMaxHeight();
				$('.unisites-sections .item-16 p, .unisites-sections .item-17 p, .unisites-sections .item-18 p').setAllToMaxHeight();
				$('.unisites-sections .item-19 p, .unisites-sections .item-20 p, .unisites-sections .item-21 p').setAllToMaxHeight();
				$('.unisites-sections .item-22 p, .unisites-sections .item-23 p, .unisites-sections .item-24 p').setAllToMaxHeight();
				$('.multimedia-main-item-1 h4, .multimedia-main-item-2 h4').setAllToMaxHeight();
				$('.multimedia-item-1 h4, .multimedia-item-2 h4, .multimedia-item-3 h4').setAllToMaxHeight();
				$('.multimedia-item-4 h4, .multimedia-item-5 h4, .multimedia-item-6 h4').setAllToMaxHeight();
				$('.multimedia-item-7 h4, .multimedia-item-8 h4, .multimedia-item-9 h4').setAllToMaxHeight();
				$('.multimedia-item-10 h4, .multimedia-item-11 h4, .multimedia-item-12 h4').setAllToMaxHeight();
				$('.multimedia-item-13 h4, .multimedia-item-14 h4, .multimedia-item-15 h4').setAllToMaxHeight();
				$('.multimedia-item-16 h4, .multimedia-item-17 h4, .multimedia-item-18 h4').setAllToMaxHeight();
				$('.multimedia-item-19 h4, .multimedia-item-20 h4, .multimedia-item-21 h4').setAllToMaxHeight();
				$('.multimedia-item-22 h4, .multimedia-item-23 h4, .multimedia-item-24 h4').setAllToMaxHeight();
				$('.multimedia-main-item-1 > p, .multimedia-main-item-2 > p').setAllToMaxHeight();
				$('.multimedia-item-1 > p, .multimedia-item-2 > p, .multimedia-item-3 > p').setAllToMaxHeight();
				$('.multimedia-item-4 > p, .multimedia-item-5 > p, .multimedia-item-6 > p').setAllToMaxHeight();
				$('.multimedia-item-7 > p, .multimedia-item-8 > p, .multimedia-item-9 > p').setAllToMaxHeight();
				$('.multimedia-item-10 > p, .multimedia-item-11 > p, .multimedia-item-12 > p').setAllToMaxHeight();
				$('.multimedia-item-13 > p, .multimedia-item-14 > p, .multimedia-item-15 > p').setAllToMaxHeight();
				$('.multimedia-item-16 > p, .multimedia-item-17 > p, .multimedia-item-18 > p').setAllToMaxHeight();
				$('.multimedia-item-19 > p, .multimedia-item-20 > p, .multimedia-item-21 > p').setAllToMaxHeight();
				$('.multimedia-item-22 > p, .multimedia-item-23 > p, .multimedia-item-24 > p').setAllToMaxHeight();
				$('.multimedia-section .video-item-1 h4, .multimedia-section .video-item-2 h4').setAllToMaxHeight();
				$('.multimedia-section .video-item-3 h4, .multimedia-section .video-item-4 h4, .multimedia-section .video-item-5 h4').setAllToMaxHeight();
				$('.multimedia-section .podcast-item-1 h4, .multimedia-section .podcast-item-2 h4, .multimedia-section .podcast-item-3 h4').setAllToMaxHeight();
				$('.multimedia-section .podcast-item-4 h4, .multimedia-section .podcast-item-5 h4, .multimedia-section .podcast-item-6 h4').setAllToMaxHeight();
				$('.multimedia-section .photostory-item-1 h4, .multimedia-section .photostory-item-2 h4').setAllToMaxHeight();
				$('.multimedia-section .photostory-item-3 h4, .multimedia-section .photostory-item-4 h4, .multimedia-section .photostory-item-5 h4').setAllToMaxHeight();
				$('#podcast .multimedia-section .video-item-1 h4, #podcast .multimedia-section .video-item-2 h4, #podcast .multimedia-section .video-item-3 h4').setAllToMaxHeight();
				$('#podcast .multimedia-section .video-item-4 h4, #podcast .multimedia-section .video-item-5 h4, #podcast .multimedia-section .video-item-6 h4').setAllToMaxHeight();
				// cw
				$('.ts-item-2 h4, .ts-item-3 h4, .ts-item-4 h4').setAllToMaxHeight();
				$('.ts-item-5 h4, .ts-item-6 h4, .ts-item-7 h4').setAllToMaxHeight();
				$('.ts-item-2 .ts-item-img, .ts-item-3 .ts-item-img, .ts-item-4 .ts-item-img').setAllToMaxHeight();
				$('.ts-item-5 .ts-item-img, .ts-item-6 .ts-item-img, .ts-item-7 .ts-item-img').setAllToMaxHeight();
				$('.ln-item-1 h4, .ln-item-2 h4, .ln-item-3 h4').setAllToMaxHeight();
				$('.ln-item-4 h4, .ln-item-5 h4, .ln-item-6 h4').setAllToMaxHeight();
				$('.tips-item-1 h4, .tips-item-2 h4, .tips-item-3 h4').setAllToMaxHeight();
				$('.tips-item-4 h4, .tips-item-5 h4, .tips-item-6 h4').setAllToMaxHeight();
				$('.tips-item-1 > p, .tips-item-2 > p, .tips-item-3 > p').setAllToMaxHeight();
				$('.tips-item-4 > p, .tips-item-5 > p, .tips-item-6 > p').setAllToMaxHeight();
				$('.tips-home .opinions-item-1 h4, .tips-home .opinions-item-2 h4, .tips-home .opinions-item-3 h4').setAllToMaxHeight();
				$('.tips-home .opinions-item-4 h4, .tips-home .opinions-item-5 h4, .tips-home .opinions-item-6 h4').setAllToMaxHeight();
				$('.tips-home .opinions-item-1 > p, .tips-home .opinions-item-2 > p, .tips-home .opinions-item-3 > p').setAllToMaxHeight();
				$('.tips-home .opinions-item-4 > p, .tips-home .opinions-item-5 > p, .tips-home .opinions-item-6 > p').setAllToMaxHeight();
				$('.video-home .video-item-1 h4, .video-home .video-item-2 h4, .video-home .video-item-3 h4').setAllToMaxHeight();
				$('.video-home .video-item-4 h4, .video-home .video-item-5 h4, .video-home .video-item-6 h4').setAllToMaxHeight();
				// lemag cartoon
				$('#cartoon .multimedia-items li h4, #cartoon .multimedia-items li p').css('height', '');
				$('#cartoon .multimedia-items .multimedia-main-item-1 h4, #cartoon .multimedia-items .multimedia-main-item-2 h4, #cartoon .multimedia-items .multimedia-main-item-3 h4').setAllToMaxHeight();
				// tss
				$('.fas-item-1 .author-info, .fas-item-2 .author-info, .fas-item-3 .author-info').setAllToMaxHeight();
				$('.fas-item-4 .author-info, .fas-item-5 .author-info, .fas-item-6 .author-info').setAllToMaxHeight();
				$('.fas-item-1, .fas-item-2, .fas-item-3').setAllToMaxHeight();
				$('.fas-item-4, .fas-item-5, .fas-item-6').setAllToMaxHeight();


			} else {
				// reset heights
				$('.nn-item-1, .nn-item-2, .nn-item-3, .nn-item-4, .nn-item-5').css('height', '').css('display', 'block');
				$('.nn-item-1 h4, .nn-item-2 h4, .nn-item-3 h4, .nn-item-4 h4, .nn-item-5 h4').css('height', '');
				$('.homepage-topics').css('min-height', '');
			}

			if(TT.mq < TT.mqs.desktop) {
				$('.box-ad-wrapper').appendTo('.tt-list-1');
				$('.latest-news-sidebar').insertAfter('.latest-news-home > .button').css('display', 'block').trigger('moved');
			} else {
				$('.box-ad-wrapper').appendTo('.trending-topics-home h2');
				// event "moved" is used by ads not to render until after
				$('.latest-news-sidebar').insertAfter('.latest-news-home > ul').css('display', 'block').trigger('moved');
			}

			// greater than mobile breakpoint - equal heights
			if(TT.mq > TT.mqs.mobile) {
				// reset heights
				$('.pro-downloads-list li h4, .learn-more-items li h4, .from-across-items li h3, .from-across-items li h4, .multimedia-sections-primary h4, .blog-sections > li, .ezine-pro-plus-list li p, .ezine-more-downloads .ezine-previous-issues-list li h4, .epi-list-1 li h4, .epi-list-2 li h4, .epi-list-3 li h4, .epi-list-4 li h4, .epi-list-5 li h4, .epi-list-6 li h4, .epi-list-7 li h4, .epi-list-8 li h4, .epi-list-9 li h4, .epi-list-10 li h4, .epi-list-11 li h4, .epi-list-12 li h4, .epi-list-13 li h4, .ebook-all-issues-list-1, .ebook-all-issues-list-2, .ebook-all-issues-list-3, .ebook-all-issues ul li ul li:first-child h4, .ebook-all-issues ul li ul li:nth-child(2) h4, .ebook-all-issues ul li ul li:nth-child(3) h4, .ebook-all-issues ul li ul li:nth-child(4) h4, .ebook-all-issues ul li ul li:nth-child(5) h4, .ezine-pro-plus-list h3, .cascading-fulfillment-section .related-content li h4, .cascading-fulfillment-section .related-content li p, .new-notable.news-listing ul li h4').css('height', '');
				
				// resize heights
				$('.pro-downloads-list li h4').setAllToMaxHeight();
				$('.learn-more-items li h4').setAllToMaxHeight();
				$('.from-across-items li:nth-child(2) h3, .from-across-items li:nth-child(3) h3, .from-across-items li:nth-child(4) h3').setAllToMaxHeight();
				$('.from-across-items li h4').setAllToMaxHeight();
				$('.multimedia-sections-primary h4').setAllToMaxHeight();
				$('.blog-sections > li').setAllToMaxHeight();
				$('.ezine-pro-plus-list li p').setAllToMaxHeight();
				$('.ezine-more-downloads .ezine-previous-issues-list li h4').setAllToMaxHeight();
				$('.epi-list-1 li h4').setAllToMaxHeight();
				$('.epi-list-2 li h4').setAllToMaxHeight();
				$('.epi-list-3 li h4').setAllToMaxHeight();
				$('.epi-list-4 li h4').setAllToMaxHeight();
				$('.epi-list-5 li h4').setAllToMaxHeight();
				$('.epi-list-6 li h4').setAllToMaxHeight();
				$('.epi-list-7 li h4').setAllToMaxHeight();
				$('.epi-list-8 li h4').setAllToMaxHeight();
				$('.epi-list-9 li h4').setAllToMaxHeight();
				$('.epi-list-10 li h4').setAllToMaxHeight();
				$('.epi-list-11 li h4').setAllToMaxHeight();
				$('.epi-list-12 li h4').setAllToMaxHeight();
				$('.epi-list-13 li h4').setAllToMaxHeight();
				$('.ebook-all-issues ul li ul li:first-child h4').setAllToMaxHeight().parents('.ebook-all-issues-list-1, .ebook-all-issues-list-2, .ebook-all-issues-list-3').css('position', 'relative').find('.button').css({'position' : 'absolute', 'bottom' : '0'});
				$('.ebook-all-issues ul li ul li:nth-child(2) h4').setAllToMaxHeight();
				$('.ebook-all-issues ul li ul li:nth-child(3) h4').setAllToMaxHeight();
				$('.ebook-all-issues ul li ul li:nth-child(4) h4').setAllToMaxHeight();
				$('.ebook-all-issues ul li ul li:nth-child(5) h4').setAllToMaxHeight();
				$('.ebook-all-issues-list-1, .ebook-all-issues-list-2, .ebook-all-issues-list-3').setAllToMaxHeight();
				$('.cascading-fulfillment-section .related-content .item-1 h4, .cascading-fulfillment-section .related-content .item-2 h4').setAllToMaxHeight();
				$('.cascading-fulfillment-section .related-content .item-3 h4, .cascading-fulfillment-section .related-content .item-4 h4').setAllToMaxHeight();
				$('.cascading-fulfillment-section .related-content .item-5 h4, .cascading-fulfillment-section .related-content .item-6 h4').setAllToMaxHeight();
				$('.cascading-fulfillment-section .related-content .item-1 p, .cascading-fulfillment-section .related-content .item-2 p').setAllToMaxHeight();
				$('.cascading-fulfillment-section .related-content .item-3 p, .cascading-fulfillment-section .related-content .item-4 p').setAllToMaxHeight();
				$('.cascading-fulfillment-section .related-content .item-5 p, .cascading-fulfillment-section .related-content .item-6 p').setAllToMaxHeight();

				// display all search results type after mobile breakpoint
				$('.search-results-type-menu .dropdown-menu').css('display', 'block');

				// make titles same heights for News splash
				$('.topic-news-item.item-1 h3, .topic-news-item.item-2 h3').setAllToMaxHeight();
				$('.topic-news-item.item-3 h3, .topic-news-item.item-4 h3').setAllToMaxHeight();
				$('.topic-news-item.item-1 .topic-news-description, .topic-news-item.item-2 .topic-news-description').setAllToMaxHeight();
				$('.topic-news-item.item-3 .topic-news-description, .topic-news-item.item-4 .topic-news-description').setAllToMaxHeight();

				// cw
				$('.new-notable.news-listing ul li:nth-child(1) h4, .new-notable.news-listing ul li:nth-child(2) h4').setAllToMaxHeight();
				$('.new-notable.news-listing ul li:nth-child(3) h4, .new-notable.news-listing ul li:nth-child(4) h4').setAllToMaxHeight();

				// whatis
				$('.dig-deeper.whatis ul li:nth-child(1) h5, .dig-deeper.whatis ul li:nth-child(2) h5').setAllToMaxHeight();
				$('.dig-deeper.whatis ul li:nth-child(3) h5, .dig-deeper.whatis ul li:nth-child(4) h5').setAllToMaxHeight();

			} else {
				// reset heights
				$('.pro-downloads-list li h4, .learn-more-items li h4, .from-across-items li h3, .from-across-items li h4, .multimedia-sections-primary h4, .blog-sections > li, .ezine-pro-plus-list li p, .ezine-more-downloads .ezine-previous-issues-list li h4, .epi-list-1 li h4, .epi-list-2 li h4, .epi-list-3 li h4, .epi-list-4 li h4, .epi-list-5 li h4, .epi-list-6 li h4, .epi-list-7 li h4, .epi-list-8 li h4, .epi-list-9 li h4, .epi-list-10 li h4, .epi-list-11 li h4, .epi-list-12 li h4, .epi-list-13 li h4, .ebook-all-issues-list-1, .ebook-all-issues-list-2, .ebook-all-issues-list-3, .ebook-all-issues ul li ul li:first-child h4, .ebook-all-issues ul li ul li:nth-child(2) h4, .ebook-all-issues ul li ul li:nth-child(3) h4, .ebook-all-issues ul li ul li:nth-child(4) h4, .ebook-all-issues ul li ul li:nth-child(5) h4, .cascading-fulfillment-section .related-content li h4, .cascading-fulfillment-section .related-content li p, .new-notable.news-listing ul li h4').css('height', '');
				$('.ebook-all-issues-list-1').css('position', '').find('.button').css({'position' : '', 'bottom' : ''});
				$('.ebook-all-issues-list-2').css('position', '').find('.button').css({'position' : '', 'bottom' : ''});
				$('.ebook-all-issues-list-3').css('position', '').find('.button').css({'position' : '', 'bottom' : ''});
				// hide search results type at mobile breakpoint
				$('.search-results-type-menu .dropdown-menu').css('display', '');

				// resize titles for News splash
				$('.topic-news-item h3, .topic-news-item .topic-news-description').css('height', '');
			}

			// greater than tablet breakpoint - equal heights
			if(TT.mq > TT.mqs.tablet) {
				// reset heights
				$('.find-solutions-sections li h4, .find-solutions-sections-primary p, .expert-question-home, .discussion-home, .custom-native-sections li h4, .custom-native-sections li p, .expert-question-home, .search-def-home, .opinions-home li h4, .opinions-home li > p, .tutorials-home li h4, .tutorials-home li > p, .features-home li h4, .features-home li > p, .answers-home li h4, .answers-home li > p, .new-notable-home-wrapper, .from-the-community-wrapper').css('height', '');

				// resize heights
				$('.find-solutions-sections li h4').setAllToMaxHeight();
				$('.find-solutions-sections-primary p').setAllToMaxHeight();
				$('.expert-question-home, .discussion-home').setAllToMaxHeight();
				$('.custom-native-sections .item-1 h4, .custom-native-sections .item-2 h4').setAllToMaxHeight();
				$('.custom-native-sections .item-3 h4, .custom-native-sections .item-4 h4').setAllToMaxHeight();
				$('.custom-native-sections .item-5 h4, .custom-native-sections .item-6 h4').setAllToMaxHeight();
				$('.custom-native-sections .item-7 h4, .custom-native-sections .item-8 h4').setAllToMaxHeight();
				$('.custom-native-sections .item-9 h4, .custom-native-sections .item-10 h4').setAllToMaxHeight();
				$('.custom-native-sections .item-1 p, .custom-native-sections .item-2 p').setAllToMaxHeight();
				$('.custom-native-sections .item-3 p, .custom-native-sections .item-4 p').setAllToMaxHeight();
				$('.custom-native-sections .item-5 p, .custom-native-sections .item-6 p').setAllToMaxHeight();
				$('.custom-native-sections .item-7 p, .custom-native-sections .item-8 p').setAllToMaxHeight();
				$('.custom-native-sections .item-9 p, .custom-native-sections .item-10 p').setAllToMaxHeight();
				// cw
				$('.expert-question-home, .search-def-home').setAllToMaxHeight();
				$('.opinions-home .opinions-item-1 h4, .opinions-home .opinions-item-2 h4, .opinions-home .opinions-item-3 h4').setAllToMaxHeight();
				$('.opinions-home .opinions-item-1 > p, .opinions-home .opinions-item-2 > p, .opinions-home .opinions-item-3 > p').setAllToMaxHeight();
				$('.features-home .opinions-item-1 h4, .features-home .opinions-item-2 h4, .features-home .opinions-item-3 h4').setAllToMaxHeight();
				$('.features-home .opinions-item-1 > p, .features-home .opinions-item-2 > p, .features-home .opinions-item-3 > p').setAllToMaxHeight();
				$('.tutorials-home .tutorials-item-1 h4, .tutorials-home .tutorials-item-2 h4, .tutorials-home .tutorials-item-3 h4').setAllToMaxHeight();
				$('.tutorials-home .tutorials-item-1 > p, .tutorials-home .tutorials-item-2 > p, .tutorials-home .tutorials-item-3 > p').setAllToMaxHeight();
				$('.answers-home .answers-item-1 h4, .answers-home .answers-item-2 h4, .answers-home .answers-item-3 h4').setAllToMaxHeight();
				$('.answers-home .answers-item-1 > p, .answers-home .answers-item-2 > p, .answers-home .answers-item-3 > p').setAllToMaxHeight();
				// tss
				$('.new-notable-home-wrapper, .from-the-community-wrapper').setAllToMaxHeight();
				$('.tss-nn-item-2 h4, .tss-nn-item-3 h4').setAllToMaxHeight();
				$('.tss-nn-item-4 h4, .tss-nn-item-5 h4').setAllToMaxHeight();
				$('.tss-nn-item-2 > p, .tss-nn-item-3 > p').setAllToMaxHeight();
				$('.tss-nn-item-4 > p, .tss-nn-item-5 > p').setAllToMaxHeight();

				
			} else {
				// reset heights
				$('.find-solutions-sections li h4, .find-solutions-sections-primary p, .expert-question-home, .discussion-home, .find-solutions-sections-links li:first-child, .find-solutions-sections-links li:nth-child(2), .find-solutions-sections-links li:nth-child(3), .multimedia-sections-links li:first-child, .multimedia-sections-links li:nth-child(2), .multimedia-sections-links li:nth-child(3), .custom-native-sections li h4, .custom-native-sections li p, .expert-question-home, .search-def-home, .opinions-home li h4, .opinions-home li > p, .tutorials-home li h4, .tutorials-home li > p, .features-home li h4, .features-home li > p, .answers-home li h4, .answers-home li > p, .new-notable-home-wrapper, .from-the-community-wrapper, .new-notable-home-wrapper .nn-home-item h4, .new-notable-home-wrapper .nn-home-item > p').css('height', '');
			}

			// greater than desktop breakpoint - equal heights
			if(TT.mq > TT.mqs.desktop) {
				$('.blog-section-1 h4, .blog-section-2 h4').setAllToMaxHeight();
			} else {
				$('.blog-section-1 h4, .blog-section-2 h4').css('height', '');
			}

			// cw blogs 
			if(TT.mq > TT.mqs.tablet) {
				$('.cw-blogs .blog-section-1 h4, .cw-blogs .blog-section-2 h4').setAllToMaxHeight();
				$('.cw-blogs .blog-section-3 h4, .cw-blogs .blog-section-4 h4').setAllToMaxHeight();
			}

			// whatis splashes home
			$('.whatis-splashes-home .splashes-list li').setAllToMaxHeight();

		}
	};
});
// UI Ads module
TT(["ads", "context/ui", "util/logging", "lib/jquery"],
	function (ads, ui, logging, $) {

	var isDebug = !!ads.isDebug,
		logger = logging.getLogger(isDebug);

	ui.on("ready", hideUncalledAds)
	.on("adLoad", handleAdLoaded)
	.on("adInterstitial", function (evt) {
		logger.log("Interstitial state=%s", evt.state);
	});

	function handleAdLoaded(evt) {
		var ad = evt.ad;
		// adjust the float and the start of the right column
		if (ad.location == "content-header" && (ad.layout == "desktop" || ad.layout == "tablet_h")) {
			setFloatMaskHeight();
		}
		// apply display fixes after interstitial goes away
		else if (ad.type == "ist" && !ad.empty) {
			// Poll for when the interstitial is activated
			// poll for when the interstitial is gone by testing whether the
			// site-container is visible. Polling can stop after 30 seconds.
			var $el = $('div.site-container').eq(0),
				sta = $el.is(":visible"),
				max = 30000,
				t = 250;
			if (!sta) {
				// if already active fire event
				fireInterstitialStateChange(1); // on
			}
			var i = setInterval(function () {
				max -= t;
				var cur = $el.is(":visible");
				if (!cur && sta) {
					// if now active but wasn't before
					sta = cur;
					fireInterstitialStateChange(1); // on
				}
				else if (cur && !sta) {
					// if now cleared and previously active
					clearInterval(i);
					fireInterstitialStateChange(0); // off
				}
				else if (max == 0) {
					// we came to the end and couldn't determine
					clearInterval(i);
					fireInterstitialStateChange(2); // undetermined
				}
			}, t);
		}
	}

	function fireInterstitialStateChange(i) {
		ui.fireEvent("adInterstitial", {state: i});
	}

	function hideUncalledAds() {
		$.each(ads.uncalled, function (i, ad) {
			// some ads are nested in elements below an ad wrapper
			var $div = $("#" + ad.divId);
			var $wrapper = $div.parent().closest("div.ad-wrapper");
			if (ad.type == "pr") {
				if (ad.zone != "HOME") {
					if (ad.targeting.pos == "bottom") {
						// hide just the inner to keep the padding
						$div.parent("div.body-pro-callout-inner").hide();
						return;
					}
				}
			}
			// hide wrappers if the only ad is the uncalled ad
			// otherwise assume the wrapper is holding multiple ads
			// that toggle based on breakpoint
			if ($wrapper.find("div.ad").length == 1) {
				$wrapper.hide();
			}
		});
	}

	function setFloatMaskHeight() {
		// Calculate the height by subtracting the difference between
		// the top of the center column and the top of the ad wrapper
		// from the height of the ad wrapper
		var $wrapper = $("article.content-columns").find("div.ad-wrapper").eq(0);
		var y1 = $wrapper.offset().top;
		var h = $wrapper.height();

		var $col = $("div.content-center").eq(0);
		var y2 = $col.offset().top;

		if (y1 + h > y2) {
			var $mask = $col.find("div.float-mask").eq(0);

			var h = Math.max(0, h - (y2 - y1));
			if (h > 0) {
				$mask.height(h).show();
			}
		}
	}

});
// UI images module
TT(["context/ui", "ui/main", "lib/jquery"], function (ui, TT, $) {

	// Handles swapping images for appropriate screen size
	// on page load and on window.resize 

	var Images = {
		imageSize: 320,
		imageSwapTimeout: null,
		
		breakpointArray: [1280, 960, 640, 320],
		
		init:function(){
			this.checkIfSwapImages();
			this.setResizeListener();
		},
		
		setResizeListener:function(){
			ui.on("layoutChange", function() {
				clearTimeout(Images.imageSwapTimeout);
				Images.imageSwapTimeout = setTimeout(Images.checkIfSwapImages, 100);
			});
		},
		
		checkIfSwapImages:function(){
			// If the MQ is not for the default image, load correctly sized image
			if(TT.mq !== this.imageSize){
				Images.imageSize = TT.mq; 
				Images.swapImages();
			}
		},
		
		swapImages:function(){
			var $images = $('.swap');
			var breakpointsToCheck = this.getBreakPointsToCheck(TT.mq);
			var swapTo;
			var newSrc;
			
			for(var i=0; i<$images.length; i++){
				
				for(var j=0; j<breakpointsToCheck.length; j++){
					if($images.eq(i).data('img'+breakpointsToCheck[j])){
						swapTo = breakpointsToCheck[j];
						break;
					}
				}
				
				newSrc = $images.eq(i).data('img'+swapTo);
				$images.eq(i).attr('src', newSrc);
			}
		},
		
		// Get array of breakpoints less than or equal to given breakpoint
		// This array is then used to swap to the next highest image, 
		// if one for the given breakpoint is not specified 
		getBreakPointsToCheck:function(breakpoint){
			var breakpoints = Images.breakpointArray;
			var breakpointsToCheck = [];
			
			for(var i=0; i<breakpoints.length; i++){
				if(breakpoints[i] <= breakpoint){
					breakpointsToCheck.push(breakpoints[i])
				}
			}
			
			return breakpointsToCheck;
		}
	}

	$(function(){
		Images.init();
	});

});
// UI header module
TT(["context/ui", "ui/main", "ui/utils", "lib/jquery"], function (ui, TT, utils, $) {

	var Header = {
		$header: $('.header'),
		headerTop: 0,
		headerBottom: 0,
		
		searchsitesJscroll: null,
		topicsJscroll: null,
		sectionsJscroll: null,
		
		navItemOpenSpeed: 300,
		
		adjustTimeout: {},
		jscrollTimeout: {},
		
		globalJscrollOptions: { hideFocus: true },
		
		init:function(){
			this.getHeaderValues();
			this.bindEventHandlers();
			this.adjustHeader();
			this.setSearchWidth();
			this.setupAutoComplete();
		},
		
		
		bindEventHandlers:function(){
			// Click handler for opening search
			$('.header-search-toggle, .desktop-fixed-search').hammer().on(TT.click, function(){
				var $form = $('.header-search');
				var $input = $('.header-search-input');
				var $whatIsAlpha = $('.header-browse-alpha');
				
				if(TT.mq < TT.mqs.desktop){
					
					$(this).toggleClass('selected');
				
					$form.toggle(0);
					$input.focus();
					$whatIsAlpha.toggle(0);
					
					// Close nav menu
					$('.nav-toggle').removeClass('selected');
					$('.nav').css('display', '');
				}
				else if(TT.mq === TT.mqs.desktop || TT.$body.hasClass('header-desktop-fixed')){
					if($form.is(':visible')){
						$form.submit();
					}
					else {
						$(this).addClass('form-open');
						$form.toggle(0);
						$input.focus();
					}
				}
				else if(TT.mq === TT.mqs.desktop_w){
					$form.submit();
				}
			});
			
			
			// Keyup handler for closing search query when user presses ESC key
			$('.header-search-input').keyup(function(e){
				if(e.keyCode === 27){
					$('.header-search-input').blur();
				}
			});
			
			// Blur handler for closing search query when user leaves input
			$('.header-search-input').on('blur', function(e){
				setTimeout(Header.hideSearch, 200);
			});

			// Opening More Content
			$('.header_v2 .more-content').hover(function() {
				$(this).addClass('hovered');
			}, function() {
				$(this).removeClass('hovered');
			});

			// Hover for header menus (search sites and sites with new topics/subtopics)
			if(TT.mq > TT.mqs.tablet) {
				$('.header_v2 .nav-list-item').hover(function() {
					$(this).addClass('hovered');
				}, function() {
					$(this).removeClass('hovered');
				});
			}

			// Hover for header menus (sites with current menu, no topics/subtopics)
			if(TT.mq > TT.mqs.tablet) {
				$('.header_v2-keep-nav .nav-list-item').hover(function() {
					$(this).addClass('hovered');
					$(this).siblings('.nav-list-item').addClass('nav-list-item-border');
				}, function() {
					$(this).removeClass('hovered');
					$(this).siblings('.nav-list-item').removeClass('nav-list-item-border');
				});
			}

			// Click handler for opening navigation
			$('.nav-toggle').hammer().on(TT.click, function(){
				var $nav = $('.nav');
				$(this).toggleClass('selected');
				$nav.slideToggle(250, function(){
					utils.clearDisplayProp($nav[0]);
				});
				
				// Close search bar
				$('.header-search-toggle').removeClass('selected');
				$('.header-search').css('display', '');
			});

			// On WhatIs, header menu links and arrow opens dropdown
			// On search sites and others, entire header menu opens dropdown

			// WHATIS
			if ($('.nav-list-item-topics.whatis').length) {
				// Click handler for opening top-level nav items
				$('.nav').on('click', '.nav-list-item-header .icon-arrow-down', function(e){
					// We set a custom event handler on body (click.clickout) inside of openNavItem
					// This custom event handler closes all dropdowns
					// So stop this click event from bubbling up to body or it will trigger that event
					e.stopPropagation();
					
					var $navItem = $(this).closest('.nav-list-item')
					
					if($navItem.hasClass('selected')){
						Header.closeNavItems($navItem);
					}
					else {
						Header.closeNavItems();
						Header.openNavItem($(this).parents());
					}
				});

			// SEARCH SITES AND SIMILAR
			} else if($('.header_v2').length || $('.header_v2-keep-nav').length) {
			
				if(TT.mq < TT.mqs.desktop) {
					$('.nav').on('click', '.nav-list-item .nav-list-item-header', function(e){
						// We set a custom event handler on body (click.clickout) inside of openNavItem
						// This custom event handler closes all dropdowns
						// So stop this click event from bubbling up to body or it will trigger that event
						e.stopPropagation();
						
						var $navItem = $(this).closest('.nav-list-item')
						
						if($navItem.hasClass('selected')){
							Header.closeNavItems($navItem);
						}
						else {
							Header.closeNavItems();
							Header.openNavItem($(this));
						}
					});
				}
			} else {
				$('.nav').on('click', '.nav-list-item .nav-list-item-header', function(e){
					// We set a custom event handler on body (click.clickout) inside of openNavItem
					// This custom event handler closes all dropdowns
					// So stop this click event from bubbling up to body or it will trigger that event
					e.stopPropagation();
					
					var $navItem = $(this).closest('.nav-list-item')
					
					if($navItem.hasClass('selected')){
						Header.closeNavItems($navItem);
					}
					else {
						Header.closeNavItems();
						Header.openNavItem($(this));
					}
				});
			}

			// Copy of Click handler above for opening the Search Site menu with just the arrow, search site name goes to homepage
			/*$('.nav').on('click', '.nav-list-item-all-sites .nav-list-item-header, .nav-list-item-searchsites .nav-list-item-header .icon', function(e){
				e.stopPropagation();
				
				var $navItem = $(this).closest('.nav-list-item')
				
				if($navItem.hasClass('selected')){
					Header.closeNavItems($navItem);
				}
				else {
					Header.closeNavItems();
					Header.openNavItem($('.nav-list li:nth-child(1) .nav-list-item-header'));
				}
			});
			*/
			
			
			// Click handler for showing all Parent Topics
			$('.nav-list-sublist-header-topics').on('click', '.view-all', function(){
				$('.nav-list-child-topics').hide();
				$('.nav-list-parent-topics').show();
				
				Header.topicsJscroll.reinitialise();
			});

			// Click handler for hub page all sites
			$('.nav-list-sublist-header-sites').on('click', '.view-all', function(){
				$('.nav-list-child-sites').hide();
				$('.nav-list-parent-sites').show();
				
				Header.topicsJscroll.reinitialise();
			});
			
			
			// Click handler for selecting a Parent Topic
			$('.nav-list-parent-topics').on('click', 'li', function(){
				var topic = $(this).text();
				var topicid = $(this).attr('id');
				$('.nav-list-parent-topics').hide();
				$('.nav-list-child-topics').hide();
				$('#child-topic-' + topicid.substring(13)).show();
				
				$('.nav-list-sublist-header-topics-title').text(topic);
				
				Header.topicsJscroll.reinitialise();
				
				// TT-FIXME: Make request for Child Topics, based on selected Parent Topic
				// And populate the Child Topic list via the following line
				// $('.nav-list-child-topics').find('.nav-list-sublist').html(*insert child topic html here*);
			});

			// Click handler for selecting a hub parent group of sites
			$('.nav-list-parent-sites').on('click', 'li.nav-list-sublist-item', function(){
				var topic = $(this).text();
				var topicid = $(this).attr('id');
				$('.nav-list-parent-sites').hide();
				$('.nav-list-child-sites').hide();
				$('#child-sites-' + topicid.substring(13)).show();
				
				$('.nav-list-sublist-header-sites-title').text(topic);
				
				Header.topicsJscroll.reinitialise();
				
			});
			
			
			ui.on("resize", function(){
				// If in desktop view or greater, remove 'header-mobile-fixed' if it is applied
				// Conversely, remove 'header-desktop-fixed' if below desktop view
				if(TT.mq >= TT.mqs.desktop){
					TT.$body.removeClass('header-mobile-fixed');
				}
				else {
					TT.$body.removeClass('header-desktop-fixed');
				}
				
				Header.getHeaderValues();
				Header.setSearchWidth();
				
				clearTimeout(Header.adjustTimeout);
				Header.adjustTimeout = setTimeout(Header.adjustHeader, 100);
				
				if(TT.mq < 960){
					Header.destroyJscrolls();
				}
			}).on("scroll", function(){
				Header.adjustHeader();
				Header.setSearchWidth();
				
				// Close Search AutoComplete menu
				$('.header-search-input').autocomplete('close');
			});
			
			
			// Custom event "navChange" triggered when nav changes from small to large nav or vice versa
			$(window).on('navChange', function(){
				clearTimeout(Header.jscrollTimeout);
				Header.jscrollTimeout = setTimeout(Header.reinitJscrolls, 100);
			});
		},
			
		
		openNavItem:function($navHeader){
			$navHeader.parents('.nav-list-item').toggleClass('selected');
			$navHeader.siblings('.nav-list-item-dropdown').slideToggle(Header.navItemOpenSpeed, function(){
				if(TT.mq >=960 && !utils.isIos()){
					Header.initJscroll($navHeader);
				}
			});
			
			// Add a bottom border to the sibling nav-list-item if on desktop view
			if(TT.mq >= TT.mqs.desktop){
				if($navHeader.parents('.nav-list-item').hasClass('nav-list-item-topics')){
					$('.nav-list-item-sections').toggleClass('nav-list-item-border');
				}
				else if($navHeader.parents('.nav-list-item').hasClass('nav-list-item-sections')){
					$('.nav-list-item-topics').toggleClass('nav-list-item-border');
				}
				else if($navHeader.parents('.nav-list-item').hasClass('nav-list-item-cw')){
					$('.nav-list-item-cw').toggleClass('nav-list-item-border');
				}
				else if($navHeader.parents('.nav-list-item').hasClass('nav-list-item-lemag')){
					$('.nav-list-item-lemag').toggleClass('nav-list-item-border');
				}
				else if($navHeader.parents('.nav-list-item').hasClass('nav-list-item-microscope')){
					$('.nav-list-item-microscope').toggleClass('nav-list-item-border');
				}
				else if($navHeader.parents('.nav-list-item').hasClass('nav-list-item-german')){
					$('.nav-list-item-german').toggleClass('nav-list-item-border');
				}
				else if($navHeader.parents('.nav-list-item').hasClass('nav-list-item-bm')){
					$('.nav-list-item-bm').toggleClass('nav-list-item-border');
				}
				else if($navHeader.parents('.nav-list-item').hasClass('nav-list-item-tss')){
					$('.nav-list-item-tss').toggleClass('nav-list-item-border');
				}
			}
			
			utils.bindClickOut('.nav-list-item-dropdown', function(){ Header.closeNavItems(); }, 'nav-list-item');
		},
		
		
		// Close a single nav item
		// Or ALL items if optional $item parameter is not passed in
		closeNavItems:function($item){
			var $items = $item || $('.nav-list-item');
			
			$items.removeClass('selected nav-list-item-border');
			$items.children('.nav-list-item-dropdown').slideUp(100);
			
			// Unbind custom click event listener set in openNavItem
			$('body').off('click.clickout.nav-list-item')
		},
		
		
		adjustHeader:function(){
			var scrollY = ui.scroll.y;
		
			if(TT.mq >= TT.mqs.desktop){
				if(scrollY > Header.headerBottom && !$('body').hasClass('header-desktop-fixed')){
					TT.$body.addClass('header-desktop-fixed');
					Header.$header.animate({'top': 0}, 200);
					
					$(window).trigger('navChange');
					utils.fixPseudoIE();
				}
				else if(scrollY <= Header.headerBottom && $('body').hasClass('header-desktop-fixed')) {
					TT.$body.removeClass('header-desktop-fixed');
					Header.$header.stop().css('top', '');
					
					$(window).trigger('navChange');
				}
			}
			else {
				if(scrollY >= Header.headerTop){
					TT.$body.addClass('header-mobile-fixed');
				}
				else {
					TT.$body.removeClass('header-mobile-fixed');
				}
			}
		},
		
		
		// Initialize or reinitialize jScrollPane on nav dropdowns
		// This function is called as a callback on the slideToggle animation that opens a nav item
		initJscroll:function($navHeader){
			var $navItem = $navHeader.parents('.nav-list-item');
			var $jscrollDiv = $navItem.find('.nav-list-item-jscroll');
			
			if(!$jscrollDiv.is(':visible')){ return; }
			
			if($navItem.hasClass('nav-list-item-searchsites')){
				Header.searchsitesJscroll = Header.searchsitesJscroll || $jscrollDiv.jScrollPane(this.globalJscrollOptions).data('jsp');
				Header.searchsitesJscroll.reinitialise();
			}
			else if($navItem.hasClass('nav-list-item-topics')){
				Header.topicsJscroll = Header.topicsJscroll || $jscrollDiv.jScrollPane(this.globalJscrollOptions).data('jsp');
				Header.topicsJscroll.reinitialise();
			}
			else if($navItem.hasClass('nav-list-item-sections')){
				Header.sectionsJscroll = Header.sectionsJscroll || $jscrollDiv.jScrollPane(this.globalJscrollOptions).data('jsp');
				Header.sectionsJscroll.reinitialise();
			}
		},
		
		
		reinitJscrolls:function(){
			if(Header.searchsitesJscroll && $('.nav-list-item-searchsites').find('.nav-list-item-jscroll').is(':visible')){ Header.searchsitesJscroll.reinitialise(); }
			if(Header.topicsJscroll && $('.nav-list-item-topics').find('.nav-list-item-jscroll').is(':visible')){ Header.topicsJscroll.reinitialise(); }
			if(Header.sectionsJscroll && $('.nav-list-item-sections').find('.nav-list-item-jscroll').is(':visible')){ Header.sectionsJscroll.reinitialise(); }
		},
		
		
		// Remove jScrollPanes from navigation dropdowns
		// This is called on window.resize, when scaling down to tablet or mobile view
		destroyJscrolls:function(){
			if(Header.searchsitesJscroll){ Header.searchsitesJscroll.destroy(); }
			if(Header.topicsJscroll){ Header.topicsJscroll.destroy(); }
			if(Header.sectionsJscroll){ Header.sectionsJscroll.destroy(); }
			
			Header.searchsitesJscroll = Header.topicsJscroll = Header.sectionsJscroll = null;
		},
		
		
		// Recalculate Header position values
		getHeaderValues:function(){
			this.headerTop = $('.header-leaderboard').height();
			this.headerBottom = this.headerTop + this.$header.outerHeight();
		},
		
		
		// For desktop view, search bar should expand to a specific width
		// Search bar toggle icon should also expand to create larger click area
		setSearchWidth:function(){
			var windowWidth = $(window).width();
			var navListWidth = $('.header-logo').outerWidth() + 12; // Subtract 12 because toggle button has 6px padding on each side
			
			$('.nav-list-item').each(function(){
				navListWidth += $(this).outerWidth();
			});

			 $('.desktop-fixed-search').width(''); // needed as a reset for line 336
			
			if($('.header_v2').length || $('.header_v2-keep-nav').length) {

				if(TT.$body.hasClass('header-desktop-fixed')){
					$('.header-search').width(windowWidth - 740);
					if ($('.nav-list-item-topics').length && $('.nav-list-item.whatis, .nav-list-item.tss').length < 1) {
						$('.header-search-toggle').width(windowWidth - navListWidth);
						// fix for issue where text is too long for space and wraps below navbar
						$('.desktop-fixed-search').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item-cw').length) {
						$('.header-search').width(windowWidth - 822);
						$('.header-search-toggle').width(windowWidth - navListWidth - 112);
						// fix for issue where text is too long for space and wraps below navbar
						$('.desktop-fixed-search').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item-lemag').length) {
						$('.header-search').width(windowWidth - 852);
						$('.header-search-toggle').width(windowWidth - navListWidth - 112);
						// fix for issue where text is too long for space and wraps below navbar
						$('.desktop-fixed-search').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item-microscope').length) {
						$('.header-search').width(windowWidth - 801);
						$('.header-search-toggle').width(windowWidth - navListWidth - 112);
						// fix for issue where text is too long for space and wraps below navbar
						$('.desktop-fixed-search').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item-german').length) {
						$('.header-search').width(windowWidth - 728);
						$('.header-search-toggle').width(windowWidth - navListWidth + 0);
						// fix for issue where text is too long for space and wraps below navbar
						$('.desktop-fixed-search').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item-bm').length) {
						$('.header-search').width(windowWidth - 862);
						$('.header-search-toggle').width(windowWidth - navListWidth - 115);
						// fix for issue where text is too long for space and wraps below navbar
						$('.desktop-fixed-search').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item.tss').length) {
						$('.header-search').width(windowWidth - 801);
						$('.header-search-toggle').width(windowWidth - navListWidth - 115);
						// fix for issue where text is too long for space and wraps below navbar
						$('.desktop-fixed-search').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item.whatis').length) {
						$('.header-search').width(windowWidth - 547);
					}
					else {
						$('.header-search-toggle').width(windowWidth - 344);
					}
				}
				else if(TT.mq === TT.mqs.desktop){
					$('.header-search').width(windowWidth - 775);
					if ($('.nav-list-item-topics').length && $('.nav-list-item.whatis, .nav-list-item-iota, .nav-list-item.tss, .nav-list-item-doa').length < 1) {
						$('.header-search-toggle').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item-iota').length) {
						$('.header-search').width(windowWidth - 835);
						$('.header-search-toggle').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item-doa').length) {
						$('.header-search').width(windowWidth - 835);
						$('.header-search-toggle').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item-cw').length) {
						$('.header-search').width(windowWidth - 845);
						$('.header-search-toggle').width(windowWidth - navListWidth - 112);
					}
					else if ($('.nav-list-item-lemag').length) {
						$('.header-search').width(windowWidth - 884);
						$('.header-search-toggle').width(windowWidth - navListWidth - 120);
					}
					else if ($('.nav-list-item-microscope').length) {
						$('.header-search').width(windowWidth - 826);
						$('.header-search-toggle').width(windowWidth - navListWidth - 120);
					}
					else if ($('.nav-list-item-german').length) {
						$('.header-search').width(windowWidth - 895);
						$('.header-search-toggle').width(windowWidth - navListWidth + 4);
					}
					else if ($('.nav-list-item-bm').length) {
						$('.header-search').width(windowWidth - 876);
						$('.header-search-toggle').width(windowWidth - navListWidth -113);
					}
					else if ($('.nav-list-item.tss').length) {
						$('.header-search').width(windowWidth - 800);
						$('.header-search-toggle').width(windowWidth - navListWidth -113);
					}
					else if ($('.nav-list-item.whatis').length) {
						$('.header-search').width(windowWidth - navListWidth - 69);
						$('.header-browse-alpha').width(windowWidth - navListWidth - 24);
					}
					else {
						$('.header-search-toggle').width(windowWidth - 370);
					}
				}
				else if(TT.mq === TT.mqs.desktop_w){
					if ($('.nav-list-item-topics').length && $('.nav-list-item.whatis, .nav-list-item-iota, .nav-list-item.tss, .nav-list-item-doa').length < 1) {
						$('.header-search').width(windowWidth - 795);
					}
					// DELETE ME TECHFEST
					else if ($('.nav-list-item-topics.my-profile').length) {
						$('.header-search').width(windowWidth - 562);
						$('.header-search-toggle').width(windowWidth - navListWidth - 112);
					}
					else if ($('.nav-list-item-iota').length) {
						$('.header-search').width(windowWidth - 855);
					}
					else if ($('.nav-list-item-doa').length) {
						$('.header-search').width(windowWidth - 855);
					}
					else if ($('.nav-list-item-cw').length) {
						$('.header-search').width(windowWidth - 845);
					}
					else if ($('.nav-list-item-lemag').length) {
						$('.header-search').width(windowWidth - 886);
					}
					else if ($('.nav-list-item-microscope').length) {
						$('.header-search').width(windowWidth - 828);
					}
					else if ($('.nav-list-item-german').length) {
						$('.header-search').width(windowWidth - 970);
					}
					else if ($('.nav-list-item-bm').length) {
						$('.header-search').width(windowWidth - 876);
					}
					else if ($('.nav-list-item.tss').length) {
						$('.header-search').width(windowWidth - 892);
					}
					else if ($('.nav-list-item.whatis').length) {
						$('.header-search').width(495);
						$('.header-browse-alpha').width('');
					}
					else {
						$('.header-search').width(windowWidth - 362);
					}
					$('.header-search-toggle').width('');
				}
				else {
					$('.header-search').width('');
					$('.header-search-toggle').width('');
				}

			} else {

				if(TT.$body.hasClass('header-desktop-fixed')){
					$('.header-search').width(windowWidth - 334);
					if ($('.nav-list-item-topics').length && $('.nav-list-item.whatis').length < 1) {
						$('.header-search-toggle').width(windowWidth - navListWidth);
						// fix for issue where text is too long for space and wraps below navbar
						$('.desktop-fixed-search').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item-cw').length) {
						$('.header-search').width(windowWidth - 344);
						$('.header-search-toggle').width(windowWidth - navListWidth - 112);
						// fix for issue where text is too long for space and wraps below navbar
						$('.desktop-fixed-search').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item-lemag').length) {
						$('.header-search').width(windowWidth - 344);
						$('.header-search-toggle').width(windowWidth - navListWidth - 112);
						// fix for issue where text is too long for space and wraps below navbar
						$('.desktop-fixed-search').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item-microscope').length) {
						$('.header-search').width(windowWidth - 344);
						$('.header-search-toggle').width(windowWidth - navListWidth - 112);
						// fix for issue where text is too long for space and wraps below navbar
						$('.desktop-fixed-search').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item-german').length) {
						$('.header-search').width(windowWidth - 334);
						$('.header-search-toggle').width(windowWidth - navListWidth + 0);
						// fix for issue where text is too long for space and wraps below navbar
						$('.desktop-fixed-search').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item-bm').length) {
						$('.header-search').width(windowWidth - 344);
						$('.header-search-toggle').width(windowWidth - navListWidth - 115);
						// fix for issue where text is too long for space and wraps below navbar
						$('.desktop-fixed-search').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item-tss').length) {
						$('.header-search').width(windowWidth - 344);
						$('.header-search-toggle').width(windowWidth - navListWidth - 115);
						// fix for issue where text is too long for space and wraps below navbar
						$('.desktop-fixed-search').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item.whatis').length) {
						$('.header-search').width(windowWidth - 547);
					}
					else {
						$('.header-search-toggle').width(windowWidth - 344);
					}
				}
				else if(TT.mq === TT.mqs.desktop){
					$('.header-search').width(windowWidth - 360);
					if ($('.nav-list-item-topics').length && $('.nav-list-item.whatis').length < 1) {
						$('.header-search-toggle').width(windowWidth - navListWidth);
					}
					else if ($('.nav-list-item-cw').length) {
						$('.header-search').width(windowWidth - 340);
						$('.header-search-toggle').width(windowWidth - navListWidth - 112);
					}
					else if ($('.nav-list-item-lemag').length) {
						$('.header-search').width(windowWidth - 348);
						$('.header-search-toggle').width(windowWidth - navListWidth - 120);
					}
					else if ($('.nav-list-item-microscope').length) {
						$('.header-search').width(windowWidth - 343);
						$('.header-search-toggle').width(windowWidth - navListWidth - 120);
					}
					else if ($('.nav-list-item-german').length) {
						$('.header-search').width(windowWidth - 360);
						$('.header-search-toggle').width(windowWidth - navListWidth + 4);
					}
					else if ($('.nav-list-item-bm').length) {
						$('.header-search').width(windowWidth - 330);
						$('.header-search-toggle').width(windowWidth - navListWidth -113);
					}
					else if ($('.nav-list-item-tss').length) {
						$('.header-search').width(windowWidth - 415);
						$('.header-search-toggle').width(windowWidth - navListWidth -113);
					}
					else if ($('.nav-list-item.whatis').length) {
						$('.header-search').width(windowWidth - navListWidth - 69);
						$('.header-browse-alpha').width(windowWidth - navListWidth - 24);
					}
					else {
						$('.header-search-toggle').width(windowWidth - 370);
					}
				}
				else if(TT.mq === TT.mqs.desktop_w){
					if ($('.nav-list-item-topics').length && $('.nav-list-item.whatis').length < 1) {
						$('.header-search').width(windowWidth - 762);
					}
					// DELETE ME TECHFEST
					else if ($('.nav-list-item-topics.my-profile').length) {
						$('.header-search').width(windowWidth - 562);
						$('.header-search-toggle').width(windowWidth - navListWidth - 112);
					}
					else if ($('.nav-list-item-cw').length) {
						$('.header-search').width(windowWidth - 765);
					}
					else if ($('.nav-list-item-lemag').length) {
						$('.header-search').width(windowWidth - 804);
					}
					else if ($('.nav-list-item-microscope').length) {
						$('.header-search').width(windowWidth - 744);
					}
					else if ($('.nav-list-item-german').length) {
						$('.header-search').width(windowWidth - 886);
					}
					else if ($('.nav-list-item-bm').length) {
						$('.header-search').width(windowWidth - 792);
					}
					else if ($('.nav-list-item-tss').length) {
						$('.header-search').width(windowWidth - 767);
					}
					else if ($('.nav-list-item.whatis').length) {
						$('.header-search').width(495);
						$('.header-browse-alpha').width('');
					}
					else {
						$('.header-search').width(windowWidth - 362);
					}
					$('.header-search-toggle').width('');
				}
				else {
					$('.header-search').width('');
					$('.header-search-toggle').width('');
				}
			}
		},
		
		
		hideSearch:function(){
			if((TT.mq === TT.mqs.desktop || TT.$body.hasClass('header-desktop-fixed')) && $('.header-search').is(':visible')){
				$('.header-search').hide().css('display', '');
				$('.header-search-toggle').removeClass('form-open');
			}
		},
		
		
		// Set up AutoComplete behavior for Search bar
		setupAutoComplete:function(){
			var testData = [
				"George Washington",
				"John Adams",
				"Thomas Jefferson",
				"James Madison",
				"James Monroe",
				"John Quincy Adams",
				"Andrew Jackson",
				"Martin Van Buren",
				"William Henry Harrison",
				"John Tyler",
				"James K. Polk",
				"Zachary Taylor",
				"Millard Fillmore",
				"Franklin Pierce",
				"James Buchanan",
				"Abraham Lincoln",
				"Andrew Johnson",
				"Ulysses S. Grant",
				"Rutherford B. Hayes",
				"James A. Garfield",
				"Chester A. Arthur",
				"Grover Cleveland",
				"Benjamin Harrison",
				"Grover Cleveland",
				"William McKinley",
				"Theodore Roosevelt",
				"William H. Taft",
				"Woodrow Wilson",
				"Warren G. Harding",
				"Calvin Coolidge",
				"Herbert Hoover",
				"Franklin D. Roosevelt",
				"Harry S. Truman",
				"Dwight D. Eisenhower",
				"John F. Kennedy",
				"Lyndon B. Johnson",
				"Richard M. Nixon",
				"Gerald R. Ford",
				"Jimmy Carter",
				"Ronald Reagan",
				"George Bush",
				"Bill Clinton",
				"George W. Bush",
				"Barack Obama"
			];
			
			$('.header-search-input').autocomplete({
				minLength: 3,
				appendTo: '.header-search',
				source: testData,
				
				response:function(event, ui){
					// Limit suggestions to 3
					while(ui.content.length > 3){
						ui.content.pop();
					}
				},
				
				open:function(){
					var newTop = $('.header-search').height();
					$('.ui-autocomplete').css('top', newTop);
				}
			});
		}
	};

	$(function(){
		Header.init();
	});

});
// UI refinery module
TT(["context", "context/ui", "ui/main", "lib/jquery"], function (ctx, ui, TT, $) {

	var Refinery = {
		init:function(){
			this.bindEventHandlers();
		},
		
		
		bindEventHandlers:function(){
			// on click toggle open / closed
			$('.refinery-collapse-toggle, .collapse-toggle, .read-more-collapse-toggle').on('click', function(){
				Refinery.collapseToggle();

				return false;
			});

			// Show/hide refinery toggle based on content body height
			ui.on("load resize", function(){
				if(TT.mq < TT.mqs.tablet) {
					$('.refinery-body').append($('.refinery-topics-wrapper'));
				} else {
					$('.refinery-header').append($('.refinery-topics-wrapper'));
				}

				if(TT.mq > TT.mqs.mobile) {
					if ($('.refinery-content.definition-page').length > 0 || $('.refinery-content.guide-page').length > 0) {
						$('.refinery-body, .refinery-header').setAllToMaxHeight();
					} else if ($('.refinery-wrapper.contributor').length > 0 || $('.refinery-wrapper.cw-refinery').length > 0) {
					} else {
						$('.refinery-body, .refinery-header').setAllToMaxHeight();

						if ($('.refinery-articles-wrapper').height() > 185 || $('.refinery-tags').height() > 185 || ($('.refinery-header h2').height() + $('.refinery-topics-wrapper').height()) > 160) {
							$('.refinery-collapse-toggle').css('display', 'block');
						} else {
							$('.refinery-collapse-toggle').css('display', '');
						}
					}
				} else {
					$('.refinery-body, .refinery-header').css('height', '');
				}

				if ($('.refinery-guide-wrapper').height() > 180) {
					$('.guide-page .refinery-collapse-toggle').css('display', 'block');
				} else {
					$('.guide-page .refinery-collapse-toggle').css('display', '');
				}

				if ($('.refinery-wrapper.contributor .refinery-body').height() > 250) {
					$('.refinery-wrapper.contributor .refinery-collapse-toggle').css('display', 'block');
					$('.refinery-wrapper.contributor .refinery-body').css('padding-bottom', '65px');
				} else {
					$('.refinery-wrapper.contributor .refinery-collapse-toggle').css('display', '');
					$('.refinery-wrapper.contributor .refinery-body').css('padding-bottom', '20px');
				}

				if ($('.read-more-body').height() > 130) {
					$('.read-more-content .read-more-collapse-toggle').css('display', 'block');
					$('.read-more-content .read-more-body').css('padding-bottom', '50px');
				} else {
					$('.read-more-content .read-more-collapse-toggle').css('display', '');
					$('.read-more-content .read-more-body').css('padding-bottom', '20px');
				}
			});
		},


		collapseToggle:function(){
			var $toggle_btn = $('.refinery-collapse-toggle-inner, .collapse-toggle-inner, .read-more-collapse-toggle-inner');
			var $parent_wrap = $('.refinery-content, .experts-section-content, .read-more-content');

			if($parent_wrap.hasClass('closed')){
				$toggle_btn.text(ctx.i18n('toggle_box.show_less'));
				$parent_wrap.removeClass('closed');
			}
			else {
				$toggle_btn.text(ctx.i18n('toggle_box.show_more'));
				$parent_wrap.addClass('closed');
			}
		}
	};

	$(function(){
		Refinery.init();
	});

});
// UI sidebars module
TT(["context", "context/ui", "ui/main", "lib/jquery"], function (ctx, ui, TT, $) {

	var isSDef = $('#sdef').length > 0;
	var isBlogPost = $('#blog-post').length > 0;
	var isWGlossary = $('#topicLanding').length > 0;
	var isWChild = $('#resourcesChild').length > 0;
	var isWBrowseAlpha = $('#browseAlphaDef, #browseAlphaExt').length > 0;
	var isEProductsWithShare = $('#ebookDetail, #ebookDetailLogin, #ezineDetail, #ezineDetailLogin, #ebookChapter, #ebookChapterLogin').length > 0;
	var isWGlossaryP = $('#glossaryParent').length > 0;
	var isWFileExt = $('#fileExtDetail').length > 0;
	var isWSiteListing = $('#siteListing').length > 0;
	var isWSiteDetail = $('#siteDetail').length > 0;
	var isDefList = $('#defListing, #defAlpha, #defTopic').length > 0;

	var Sidebars = {
		scrollPosition: 0,
		actionBarY: 0,
		
		chapters: {},
		chaptersLength: 0,
		
		init:function(){
			this.bindEventHandlers();
			this.createChapterLookup();
			this.getChaptersLength();
			
			var scrollY = $(window).scrollTop();
			if(TT.mq < TT.mqs.desktop){
				Sidebars.lockActionBar(scrollY);
			}
			// sdef update
			else if (TT.mq === TT.mqs.desktop && isSDef)
			{
				Sidebars.lockActionBar(scrollY);
			}
			else {
				Sidebars.lockSidebars(scrollY);
			}
		},
		
		
		bindEventHandlers:function(){
			$('.actions-bar-chapters').hammer().on(TT.click, function(){
				if($('.site-container').hasClass('shifted')){
					Sidebars.slideInContent();
				}
				else{
					Sidebars.slideInSidebar('.mobile-chapters-bar');

					if ($('.mobile-def-listing-bar').length > 0) {
						$('.mobile-def-listing-bar').show();
						$('.mobile-resources-bar').hide();
					}
				}
			});
			$('.actions-bar-resources').hammer().on(TT.click, function(){
				if($('.site-container').hasClass('shifted')){
					Sidebars.slideInContent();
				}
				else{
					Sidebars.slideInSidebar('.mobile-resources-bar');
				}
			});
			$('.actions-bar-related').hammer().on(TT.click, function(){
				if($('.site-container').hasClass('shifted')){
					Sidebars.slideInContent();
				}
				else{
					Sidebars.slideInSidebar('.mobile-related-bar');
				}
			});
			
			// Click handler for opening Mobile Share Bar
			$('.actions-bar-share').children('.icon').hammer().on(TT.click, function(){
				if($('.site-container').hasClass('shifted')){
					Sidebars.slideInContent();
				}
				
				$(this).parents('.actions-bar-share').toggleClass('selected');
				$(this).siblings('.share-bar-mobile').toggle();
			});
			
			// Click handler for items in the Mobile Chapters Bar
			$('.mobile-chapters-bar').hammer().on(TT.click, '.chapters-bar-item', function(e){
				e.preventDefault();
				
				var index = $('.mobile-chapters-bar').find('.chapters-bar-item').index(this);
				Sidebars.slideInContent(index);
			});
			
			// Click handler for Desktop Chapters Bar
			$('.desktop-chapters-bar').hammer().on(TT.click, '.chapters-bar-item', function(e){
				e.preventDefault();
				
				if($(this).hasClass('selected')){ return; }
				
				var index = $('.desktop-chapters-bar').find('.chapters-bar-item').index(this);
				
				if ($("body").hasClass("header-desktop-fixed")) {
					$.scrollTo(Sidebars.chapters[index].position - 82, 250);
				} else {
					$.scrollTo(Sidebars.chapters[index].position - 180, 250);
				}
			});
			
			// Click handler for expanding Vendor Resources bar
			$('.resources-bar-toggle').hammer().on(TT.click, function(){
				var $list = $(this).siblings('.resources-bar-list');
				
				if($list.hasClass('closed')){
					$(this).text(ctx.i18n('toggle_box.show_less'));
					$list.removeClass('closed');
				}
				else {
					$(this).text(ctx.i18n('toggle_box.show_more'));
					$list.addClass('closed');
				}
				
				Sidebars.lockSidebars($(window).scrollTop());
			});

			$('.mobile-sidebars .news-archive-collapse-toggle').hammer().on(TT.click, function(){
				var $list = $(this).parents('.news-archive-item');
				
				if($list.hasClass('closed')){
					$(this).find('.toggle-marker').attr('data-icon', '3');
					$list.removeClass('closed');
					$('.news-archive-list').jScrollPane();
				}
				else {
					$(this).find('.toggle-marker').attr('data-icon', '5');
					$list.addClass('closed');
					$('.news-archive-list').jScrollPane();
				}
				
				Sidebars.lockSidebars($(window).scrollTop());
			});
			
			// Scroll handler for locking sidebars
			ui.on("load scroll", function(){
				var scrollY = ui.scroll.y;
				
				if(TT.mq < TT.mqs.desktop){
					Sidebars.lockActionBar(scrollY);
				}
				// sdef update
				else if (TT.mq === TT.mqs.desktop && isSDef)
				{
					Sidebars.lockActionBar(scrollY);
				}
				else {
					Sidebars.lockSidebars(scrollY);
				}
				// Recalculate chapter positions and highlight the active chapter
				Sidebars.createChapterLookup();
				Sidebars.highlightChapter(scrollY);
			})
			// Resize handler for various tasks
			.on("resize", function(){
				var scrollY = ui.scroll.y;
				
				if(TT.mq < TT.mqs.desktop){
					Sidebars.lockActionBar(scrollY);
				}
				// sdef update
				else if (TT.mq === TT.mqs.desktop && isSDef)
				{
					Sidebars.lockActionBar(scrollY);
				}
				else {
					Sidebars.lockSidebars(scrollY);
				}
				
				// Recalculate chapter positions
				Sidebars.createChapterLookup();
			})
			.on("adLoad", function (evt) {
				var ad = evt.ad;
				// reposition so the ad doesn't cover the vendor resources
				if (ad.location == "content-header" && ad.layout == "desktop") {
					Sidebars.lockSidebars(ui.scroll.y);
				}
			});
		},
		
		
		slideInSidebar:function(sidebarSelector){
			var $container = $('.site-container');
			var offset = (TT.mq < TT.mqs.tablet) ? 30 : 40;
			var desktopOffset = (TT.mq === TT.mqs.desktop) ? ($(window).width() - 905) / 2 : 0;

			
			// Hide the header
			$('.header').hide();
			
			// Save the users scroll position in case we need to return
			Sidebars.scrollPosition = $(window).scrollTop();
			
			// Move to top of page
			$(window).scrollTop(0);
			
			// Show the designated sidebar and hide the others
			$(sidebarSelector).show().siblings().hide();
			
			// Animate the sidebar and main content
			$container.addClass('shifted').css('top', -Sidebars.scrollPosition)
			
			if(Modernizr.csstransitions){
				$('.mobile-sidebars').css({'left': 0, 'height': 'auto', 'overflow': 'visible'});
				$container.css('left', $(window).width() - offset - desktopOffset);
				setTimeout(function(){
					$container.css('left', $(window).width() - offset - desktopOffset);
				}, 300);
			}
			else{
				$('.mobile-sidebars').css({'height': 'auto', 'overflow': 'visible'}).animate({'left': 0}, {duration: 300, queue: false});
				$container.animate({'left': $(window).width() - offset - desktopOffset}, {duration: 300, queue: false, complete: function(){
					// Check for miscalculation due to scrollbar
					$container.css('left', $(window).width() - offset - desktopOffset);
				}});
			}
		},
		
		
		// Animate the sidebar and main content
		// 1. Set CSS top so that while animating back in, the content appears in the right place
		// 2. On complete, remove the .shifted class to make the container position: static
		// 3. Set the scrollTop back to the user's previous position
		// 4. Recalculate Chapter positions
		// 5. Scroll to the Chapter selected by the user
		slideInContent:function(index){
			var $container = $('.site-container');
			var scrollY;

			if(Modernizr.csstransitions){
				$('.mobile-sidebars').css({'left': '-100%', 'height': 0, 'overflow': 'hidden'});
				$container.css({'top': -this.scrollPosition, 'left': 0});
				setTimeout(function(){
					// Bring the header back into view
					$('.header').css('display', '');
					
					$container.removeClass('shifted');
					$(window).scrollTop(Sidebars.scrollPosition);
				
					Sidebars.createChapterLookup();
					scrollY = (typeof index != 'undefined') ? Sidebars.chapters[index].position - 80 : Sidebars.scrollPosition;
					
					setTimeout(function(){ $.scrollTo(scrollY, 300); }, 200);
					
				}, 300);
			}
			else{
				$('.mobile-sidebars').css({'height': 0, 'overflow': 'hidden'}).animate({'left': '-100%'}, {duration: 300, queue: false});
				$container.css('top', -this.scrollPosition).animate({'left': 0}, {duration: 300, queue: false, complete: function(){
					// Bring the header back into view
					$('.header').css('display', '');
					$container.removeClass('shifted');
					$(window).scrollTop(Sidebars.scrollPosition);
				
					Sidebars.createChapterLookup();
					
					scrollY = (typeof index != 'undefined') ? Sidebars.chapters[index].position - 80 : Sidebars.scrollPosition;
					
					setTimeout(function(){ $.scrollTo(scrollY, 300); }, 200);
				}});
			}
		},
		
		
		lockActionBar:function(scrollY){
			// fix for ui error on standalone microsites that don't define .content-center
			var $contentCenter = $('.content-center');
			if (!$contentCenter.length) return;

			var bodyY = $contentCenter.eq(0).offset().top;
			var $actionsBar = $('.actions-bar');
			var endPoint = bodyY + $contentCenter.eq(0).height() - $actionsBar.height() - 90;

			if (isEProductsWithShare){
				var $issueDetail = $('.issue-detail');
				var $issueContents = $('.issue-contents');
				var bodyY = $issueDetail.eq(0).offset().top;
				var endPoint = bodyY + $issueDetail.eq(0).height() + $issueContents.height() - $actionsBar.height() - 214;
			}
			
			// Make sure Locking Bar (for Desktop) doesn't have a margin left over
			$('.locking-full-bar').css('margin-top', '');

			// Make the actions bar smooth on mobile with fixed positioning instead of absolute positioning (old method at bottom for reference)
			if(scrollY >= bodyY - 60 && scrollY < endPoint && !$('.site-container').hasClass('shifted')) {
				// sdef update
				if (isSDef)
				{
					if (TT.mq === TT.mqs.desktop)
					{
						$actionsBar.css({
							position: 'fixed',
							top: 80,
							left: ($(window).width() - 920)/2
						});
					}
					else {
						$actionsBar.css({
							position: 'fixed',
							top: 80,
							left: 0
						});
					}
				}
				else {
					$actionsBar.css({
						position: 'fixed',
						top: 80,
						left: 0
					});
				}
			}

			else if(scrollY >= bodyY && scrollY >= endPoint && !$('.site-container').hasClass('shifted')) {
				// sdef update
				if (isSDef)
				{
					if (TT.mq === TT.mqs.desktop)
					{
						$actionsBar.css({
							top: endPoint - bodyY + $actionsBar.height() - 265,
							position:'',
							left:''
						});
					}
					else {
						$actionsBar.css({
							top: endPoint - bodyY + $actionsBar.height(),
							position:'',
							left:''
						});
					}
				}
				else {
					$actionsBar.css({
						top: endPoint - bodyY + $actionsBar.height(),
						position:'',
						left:''
					});
				}
			}

			else if(scrollY >= bodyY - 60 && scrollY < endPoint && $('.site-container').hasClass('shifted')) {
				$actionsBar.css({
					position: 'absolute',
					top: scrollY - bodyY + 91,
					left: '-60px'
				});
			}

			else if(scrollY >= bodyY && scrollY >= endPoint && $('.site-container').hasClass('shifted')) {
				$actionsBar.css({
					top: endPoint - bodyY + $actionsBar.height(),
					position:'',
					left:''
				});
			}

			else{
				$actionsBar.css({
					top:'',
					position:'',
					left:''
				});
			}

			/* Old method
			if(scrollY >= bodyY - 60 && scrollY < endPoint){
				$actionsBar.css('top', scrollY - bodyY + 91);
			}
			else if(scrollY >= bodyY && scrollY >= endPoint){
				$actionsBar.css('top', endPoint - bodyY + $actionsBar.height());
			}
			else{
				$actionsBar.css('top', '');
			} */
		},
		
		
		lockSidebars:function(scrollY){
			// fix for ui error on standalone microsites that don't define .content-center
			var $contentCenter = $('.content-center');
			if (!$contentCenter.length) return;

			var $contentDiv = $contentCenter.eq(0);
			var bodyHeight = $contentDiv.height();
			var targetY = $contentDiv.offset().top - 31;
			
			var $fullBar = $('.locking-full-bar');
			var fullHeight = $fullBar.height();
			var $leftBar = $('.locking-left-bar');
			var leftHeight = $leftBar.height();
			var $resourcesBar = $('.desktop-resources-bar');
			var resourcesHeight = $resourcesBar.outerHeight();
			
			var endPoint = $contentDiv.offset().top + bodyHeight;

			// var resourcesOffset = $('.main-article').length > 0 ? 234 : 0;

			if (isSDef) {
				var resourcesOffset = 0;
			} else {

				if ($('#inlineRegistrationWrapper').length > 0) {
					var resourcesOffset = 870;
				} else {
					var resourcesOffset = $contentDiv.find("div.float-mask").eq(0).height();
				}
			}

			if(TT.mq < TT.mqs.desktop_w){
				// Make sure position, margin and top 
				// are reset on resize
				$resourcesBar.css({
					position: 'relative',
					'margin-top': '',
					top: ''
				});
				$fullBar.css({
					position: 'relative',
					'margin-top': '',
					top: ''
				});
				$leftBar.css({
					position: 'relative',
					'margin-top': '',
					top: ''
				});			
				$('.share-bar-desktop').css({
					position: '',
					'margin-top': '',
					top: ''
				})
				this.lockBar($fullBar, fullHeight, bodyHeight, scrollY, targetY, endPoint, 0)
			}
			else if (TT.mq > TT.mqs.desktop) {

				// Lock Left Bar (usually the Chapters Bar)
				this.lockBar($leftBar, leftHeight, bodyHeight, scrollY, targetY, endPoint, 0);
				
				// On Wide Desktop, lock the Vendor Resources Bar as well
				if (isWGlossary) {
					var rightTargetY = targetY + resourcesOffset + 180;
				} else if (isWChild) {
					var rightTargetY = targetY + resourcesOffset + 130;
				} else if (isWBrowseAlpha) {
					var rightTargetY = targetY + resourcesOffset + 20;
				} else if (isWSiteDetail) {
					var siteDetailHeaderHeight = $('#siteDetail .site-detail-header').height() + -200;
					var rightTargetY = targetY + resourcesOffset - siteDetailHeaderHeight;
				} else if (isDefList) {
					var rightTargetY = targetY + resourcesOffset + 500;
				} else {
					var rightTargetY = targetY + resourcesOffset;
				}

				this.lockBar($resourcesBar, resourcesHeight, bodyHeight, scrollY, rightTargetY, endPoint, resourcesOffset+80);
			}
		},
		
		
		lockBar:function($bar, barHeight, bodyHeight, scrollY, targetY, endPoint, offset){
			var viewHeight = $(window).height() - 80; // subtract enough to account for locked header and some buffer
			
			/* share bar moved to middle
			var $shareBar = $('.share-bar-desktop');
			var shareBarHeight = $shareBar.height();
			*/

			// what we do for narrow desktop
			if(TT.mq < TT.mqs.desktop_w) {
				if(scrollY > targetY){
					// If at bottom of article
					if(scrollY > endPoint - barHeight - 80){
						$bar.css({
							position: '',
							top: '',
							'margin-top': bodyHeight - barHeight - 80 - offset
						});
					}
					// Else lock to top
					else {
						$bar.css({
							position: 'fixed',
							top: 80,
							'margin-top': ''
						});
					}
				}
				// Default Position
				else {
					$bar.css({
						position: '',
						top: '',
						'margin-top': ''
					});
				}
			}
			// what we do for wide desktop
			else {
				if(scrollY > targetY){
					// If at bottom of article
					if(scrollY > (endPoint - barHeight - 80)){
						if($bar.hasClass('desktop-resources-bar')){

							// varying content heights, vendor resources bar needs to adjust accordingly, this sets top position for locking at bottom
							
							
							/* var defaultTop = 600 - ($('.content-center').offset().top - $('.tall-box-ad').offset().top) + 40;
						
							if (defaultTop < 40) {
								defaultTop = 50;
							}
							*/

							$bar.css({
								position: 'absolute',
								//top: defaultTop,
								top: '',
								'margin-top': bodyHeight - barHeight - 80
							});

							if ($('#inlineRegistrationWrapper').length > 0 && !isSDef) {
								$bar.css('margin-top', bodyHeight - barHeight - offset + 900);
							}
						}
						else{
							/* share bar moved to middle
							$shareBar.css({
								position: 'relative',
								top: ''
							})
							*/
							$bar.css({
								position: '',
								top: '',
								'margin-top': bodyHeight - barHeight - offset - /* share bar moved to middle shareBarHeight - */ 50
							});
						}
					}
					// Else lock to top
					else {
						/* share bar moved to middle
						if($bar.hasClass('locking-left-bar')) {
							$shareBar.css({
								position: 'fixed',
								top: barHeight + 80
							})
						}
						*/
						$bar.css({
							position: 'fixed',
							top: 80,
							'margin-top': '',
							'padding-top': isWGlossary ? '0' : isWChild ? '0' : isWBrowseAlpha ? '0' : isWSiteDetail ? '0' : isDefList ? '0' : ''
						});
						$('.locking-full-bar').css({
							position: '',
							top: '',
							'margin-top': ''
						})
					}
				}
				// default position
				else {
					if($bar.hasClass('desktop-resources-bar')){

						// varying content heights, vendor resources bar needs to adjust accordingly, this sets top position for default
						
						
						/* var defaultTop = 600 - ($('.content-center').offset().top - $('.tall-box-ad').offset().top) + 40;
						
						if (defaultTop < 40) {
							defaultTop = 50;
						}
						*/
						var siteDetailHeaderHeight = $('#siteDetail .site-detail-header').height() + -200;

						$bar.css({
							position: isSDef ? '' : 'absolute',
							// top: defaultTop,
							top: isWSiteDetail ? -siteDetailHeaderHeight : '',
							'margin-top': isSDef ? '' : isBlogPost ? '' : isWGlossaryP ? '' : isWSiteListing ? '' : isWFileExt ? offset + 45 :offset,
							'padding-top': ''
						});

						if ($('#inlineRegistrationWrapper').length > 0 && !isSDef) {
							$bar.css('top', '920px');
						}
					}
					else {
						/* share bar moved to middle
						$shareBar.css({
							position: 'relative',
							top: ''
						})
						*/
						$bar.css({
							position: 'relative',
							top: '',
							'margin-top': ''
						});
					}
				}
			}
		},
		
		
		// Count and store the number of chapters
		getChaptersLength:function(){
			var count = 0;
			var i;
			
			for(i in this.chapters){
				if(this.chapters.hasOwnProperty(i)){
					count++;
				}
			}
			
			this.chaptersLength = count;
		},
		
		
		// Populate the Sidebars.chapters object with each section/chapter and its position on the page 
		createChapterLookup:function(){
			var sections = $('.section');
			var desktopMarkers = $('.desktop-chapters-bar').find('.chapters-bar-item');
			var mobileMarkers = $('.mobile-chapters-bar').find('.chapters-bar-item');
			
			for(var i=0; i<sections.length; i++){
				this.chapters[i] = {
					chapter: sections.eq(i)[0],
					chapterMarkers: {
						desktop: desktopMarkers.eq(i)[0],
						mobile: mobileMarkers.eq(i)[0]
					},
					position: sections.eq(i).offset().top
				};
			}
		},
		
		
		// Highlight the chapter currently in view (ie. give it its selected state)
		highlightChapter:function(scrollY){
			var chapters = Sidebars.chapters;
			var markers;
			
			for(var i = this.chaptersLength - 1; i >= 0; i--){
				if(scrollY > chapters[i].position - 90){
					markers = chapters[i].chapterMarkers;
					$(markers.desktop).add(markers.mobile).addClass('selected').siblings('.chapters-bar-item').removeClass('selected');
					break;
				}
			}
		}
	};

	$(function(){
		Sidebars.init();
	});

});
// UI Dig Deeper module
TT(["context/ui", "util/logging", "lib/jquery", "util/debug"], function (ui, logging, $, debug) {

	var isDebug = debug.check("debug_ui");
	var logger = logging.getLogger(isDebug);
	var DigDeeper = {

		init: function() {

			this.setNavWidth();
			
			ui.on("resize", function() {
				DigDeeper.setNavWidth();
			});
			
			$('.dig-deeper-load-more').show().click(function() {
				DigDeeper.loadMore();
			});

		},

		setNavWidth: function(){
			var width = 0;
			var items = $('.dig-deeper-nav-item, .search-results-filter-item');
			
			for(var i=0; i<items.length; i++){
				width += items.eq(i).outerWidth(true);
			}
			
			$('.dig-deeper-nav, .search-results-filter').width(width+20);
		},

		loadMore: function() {
			logger.log("Loading more content for DigDeeper...");
			
			var ecmId = $('.dig-deeper').data('digdeeperReferenceId');
			var filterTypeKey = $('.dig-deeper-nav-item.selected').attr('data-filterType');
			var nextPage = parseInt($('.dig-deeper-nav-item.selected').attr('data-page')) + 1;
			var url = '/techtarget-ecm/services/loadMore/digDeeper/' + ecmId +'/'+ filterTypeKey + '/' + nextPage;
			logger.log("Loading more with url: " +  url);
			$.getJSON(url).always(function(data) {
				if (!data.success) {
					logger.log("Unable to load more content for DigDeeper");
					return;
				}
				if (data.content.length) {
					$.each(data.content, function(innerInd, item) {
						var iconHtml = !!data.showIcons ? '<i class="icon" data-icon="' + item.icon + '"></i>' : '';
						var tabContent = $('#digDeeperContent_' + filterTypeKey);
						logger.log('Adding item "' + item.title + '" to #digDeeperContent_' + filterTypeKey + "...");
						var html = '<li class="dig-deeper-content-item"><h5><a href="' + item.url + '">' + item.title +'</a></h5>'+ iconHtml + '</li>';
						tabContent.append(html);
					});
				} else {
					logger.log("No more content to be loaded for filterTypeKey '" + filterTypeKey + "'");
				}
				
				// Button no longer needed
				if (!data.hasMoreContent) {
					logger.log("No more content to be loaded; removing button");
					$('#digDeeperLoadMore_' + filterTypeKey).remove();
				} else {
					logger.log("More content can be loaded; updating page number");
					$('.dig-deeper-nav-item.selected').attr('data-page', nextPage);
				}
			});
		}
	};

	$(function(){
		DigDeeper.init();
	});

});
// UI Ezine module
TT(["context/ui", "util/logging", "lib/jquery", "util/debug"], function (ui, logging, $, debug) {

	var isDebug = debug.check("debug_ui");
	var logger = logging.getLogger(isDebug);
	function init() {

		// Hide the previous issues that are not found in the most recent year.
		$('div.unselected-previous-issues').hide();
		var loadMoreButton = $('div.selected-previous-issues').find('button.eproducts-load-more');
		if (loadMoreButton) {
			loadMoreButton.show();
		}

		$('div.ezine-previous-issues-menu').show().on('click', 'li', function(event) {
			event.preventDefault();
			selectYear(event);
		});

		$('section.ezine-previous-issues').on('click', 'button.eproducts-load-more', function() {
			loadMore();
		});

	};

	function selectYear(event) {
		var year = $(event.target).text();
		var oldSelectedYear = $('div.selected-previous-issues').data('issueYear');

		// Show the newly selected year's issues			
		$('div.selected-previous-issues').hide().attr("class", "ezine-previous-issues-list unselected-previous-issues");
		$('#previous_' + year).show().attr("class", "ezine-previous-issues-list selected-previous-issues");

		// Update dropdown menu
		$('ul.dropdown-menu').hide();
		$('ul.dropdown-menu').find("li").each(function() {
			if ($(this).text() == year) {
				$(this).hide();
			}
			if ($(this).text() == oldSelectedYear) {
				$(this).show();
			} 
		});
		$('#issuesDropdownYear').text(year);
		$('div.dropdown-toggle').removeClass('selected');

		var loadMoreButton = $('div.selected-previous-issues').find('button.eproducts-load-more');
		if (loadMoreButton) {
			loadMoreButton.show();
		}
		
	};

	function loadMore() {
		var selectedYear = $('div.selected-previous-issues');
		var loadMoreButton = selectedYear.find('button.eproducts-load-more');
		var year = selectedYear.data('issueYear');
		var page = parseInt(selectedYear.attr('data-load-more-page'), 10);
		var ezineId = $('#allPreviousIssues').data('ezineId');
		var url = '/techtarget-ecm/services/loadMore/previousIssues/' + ezineId +'/'+ year + '/' + page;

		logger.log("Loading more issues with url: " +  url);
		$.getJSON(url).always(function(data) {
			if (!data.success) {
				logger.log("Unable to load more content for Previous Issues");
				return;
			}
			
			// Create a new epi-list and add each item to it, then add to page.
			if (data.content.length) {
				var listCount = selectedYear.find('ul.ezine-previous-issues-list').length;
				var html = '<ul class="ezine-previous-issues-list epi-list-' + (listCount + 1) + '">';
				$.each(data.content, function(index, item) {
					var issueDate = item.date ? moment(item.date).format('MMM YYYY'): '';					
					var itemHtml = '<li class="epi-list-item-' + (index + 1) + '"><div class="image-resize"><a href="' 
						+ item.url + '"><img src="' + item.summaryImageUrl + '"></a></div><h3>E-Zine<span> | ' 
						+ issueDate + '</span></h3><h4><a href="' + item.url + '">' + item.title + '</a></h4><a href="' 
						+ item.url + '" class="eproducts-download"><i class="icon" data-icon="C"></i> Download</a></li>';
					logger.log("itemHtml: " + itemHtml);
					html += itemHtml;
				});
				loadMoreButton.before(html+ '</ul>');
			} else {
				logger.log("No more previous issues to be loaded");
			}
			
			if (!data.hasMoreContent) {
				logger.log("No more issues to be loaded; removing button");
				loadMoreButton.remove();
			} else {
				logger.log("More issues can be loaded; updating page number");
				selectedYear.attr('data-load-more-page', page + 1);
			}
		});


	};

	$(function(){
		init();
	});

});
// UI filtered_by_topic module
TT(["context/ui", "util/logging", "lib/jquery"], function (ui, logging, $) {

	var logger = logging.getLogger(true);

	function init() {

		// Hide the previous issues that are not found in the most recent year.
		$('div.unselected-topic').hide();
		var loadMoreButton = $('div.selected-topic').find('button.eproducts-load-more');
		if (loadMoreButton) {
			loadMoreButton.show();
		}

		$('div.eproducts-lp-topics-menu').show().on('click', 'li', function(event) {
			event.preventDefault();
			selectTopic(event);
		});

		$('section.eproducts-lp-topics').on('click', 'button.eproducts-load-more', function() {
			loadMore();
		});

	};

	function selectTopic(event) {
		var topicId = $(event.target).parent().data('topicId');
		var formerTopicId = $('div.selected-topic').data('topicId');

		// Show the newly topic's content			
		$('div.selected-topic').hide().attr("class", "eproducts-lp-topics-list unselected-topic");
		$('#topic_' + topicId).show().attr("class", "eproducts-lp-topics-list selected-topic");

		// Update dropdown menu
		$('ul.dropdown-menu').hide();
		$('ul.dropdown-menu').find("li").each(function() {
			if ($(this).data('topicId') == topicId) {
				$(this).hide();
			}
			if ($(this).data('topicId') == formerTopicId) {
				$(this).show();
			} 
		});
		$('#topicsDropdownName').text($(event.target).text());
		$('div.dropdown-toggle').removeClass('selected');

		var loadMoreButton = $('div.selected-topic').find('button.eproducts-load-more');
		if (loadMoreButton) {
			loadMoreButton.show();
		}
		
	};

	function loadMore() {
		var selectedTopic = $('div.selected-topic');
		var loadMoreButton = selectedTopic.find('button.eproducts-load-more');
		var topicId = selectedTopic.data('topicId');
		var page = parseInt(selectedTopic.attr('data-load-more-page'), 10);
		var dedupe = parseInt(selectedTopic.data('dedupe'), 10);
		var siteId = $('#allFilteredContent').data('siteId');
		var typedef = $('#allFilteredContent').data('typedef');
		var locale = $('#allFilteredContent').data('locale');
		var url = '/techtarget-ecm/services/loadMore/filteredTopicEProducts/' + locale + '/'
			+ siteId + '/' + topicId +'/' + typedef + '/' + page + '/' + dedupe;

		logger.log("Loading more eproducts with url: " +  url);
		$.getJSON(url).always(function(data) {
			if (!data.success) {
				logger.log("Unable to load more content for filtered topics");
				return;
			}
			
			// Create a new epi-list and add each item to it, then add to page.
			if (data.content.length) {
				var listCount = selectedTopic.find('ul.eproducts-lp-topics-list').length;
				var html = '<ul class="eproducts-lp-topics-list epi-list-' + (listCount + 1) + '">';
				var itemDate,typeLabel, itemHtml;
				$.each(data.content, function(index, item) {
					itemDate = (!item.issueDate) ? ' ' : '<span> | ' + item.issueDate + '</span>';
					typeLabel = item.typeLabel ?  item.typeLabel : 'E-Zine';
					logger.log("itemDate: " + itemDate);				
					itemHtml = '<li class="epi-list-item-' + (index + 1) + '"><div class="image-resize"><a href="' 
						+ item.url + '"><img src="' + item.summaryImageUrl + '"></a></div><h3>' + typeLabel;
					if (!!itemDate) {
						itemHtml += itemDate;
					}
					itemHtml += '</h3><h4><a href="' + item.url + '">' + item.title + '</a></h4><a href="' 
						+ item.url + '" class="eproducts-download"><i class="icon" data-icon="C"></i> Download</a></li>';
					logger.log("itemHtml: " + itemHtml);
					html += itemHtml;
				});
				loadMoreButton.before(html+ '</ul>');
			} else {
				logger.log("No more eproducts to be loaded");
			}
			
			if (!data.hasMoreContent) {
				logger.log("No more eproducts to be loaded; removing button");
				loadMoreButton.remove();
			} else {
				logger.log("More eproducts can be loaded; updating page number");
				selectedTopic.attr('data-load-more-page', page + 1);
			}
		});
	};

	$(function(){
		init();
	});

});
TT([ "context/ui", "util/logging", "lib/jquery", "util/debug", "ui/utils", "context"],
	function(ui, logging, $, debug, utils, ctx) {
		var isDebug = debug.check("debug_ui");
		var logger = logging.getLogger(isDebug);

		function init() {
			$('#allFilteredContent a.button.black-button').on('click', null, function(event) {
				event.preventDefault();
				loadMore();
			});

			$('ul.filter-by-topic-list').on('click', 'li', function(event) {
				event.preventDefault();
				selectTopic(event);
			});
		};

		function selectTopic(event) {
			var topicId = $(event.target).parent().data('topicId');
			var formerTopicId = $('div.selected-topic').data('topicId');

			$('div.selected-topic').hide().attr("class", "unselected-topic");
			if (topicId == formerTopicId) {
				// Disable currently selected topic filter
				$('#topic_all').show().attr("class", "selected-topic");
			} else {
				// Show topic's content
				$('#topic_' + topicId).show().attr("class", "selected-topic");
			}

			// Fix height of items.
			utils.adjustHomepageElements();

			if (topicId == formerTopicId) {
				// Remove highlight for previously selected filter
				$('ul.filter-by-topic-list').find(".on").attr("class", "");
			} else {
				// Highlight topic in filter
				$('ul.filter-by-topic-list').find("li").each(function() {
					if ($(this).data('topicId') == topicId) {
						$(this).attr("class", "on");
					}
					if ($(this).data('topicId') == formerTopicId) {
						$(this).attr("class", "");
					} 
				});
			}
			
		};
		
		function loadMore(){
			var language = $('#allFilteredContent').attr('data-locale');
			var contributorId = $('#allFilteredContent').attr('data-contributor-id');
			var topicId = $('div.selected-topic').attr('data-topic-id');
			var loadMorePage =  $('div.selected-topic').attr('data-load-more-page');
			var url = '/techtarget-ecm/services/loadMore/contributorContent/'+ language + '/' +contributorId +'/'+ topicId + '/' + loadMorePage;
			logger.log("Loading contributor content with url: " +  url);
			//With localized moment, you can custom what you want as te shortnened month names - in this case
			//business wants it to be different than what the standard moment provides. Consider moving this to a 
			//more centralized place.
			moment.locale(language, {
			    monthsShort : ctx.i18n('date.shortMonths').split(',')    
			});
			$.getJSON(url).always(function(data) {
				if (!data.success) {
					logger.log("Unable to load more content for contributor");
					return;
				}		
				if(!data.hasMoreContent){
					$('div.selected-topic a.button.black-button').hide();
				}				
				if(data.content.length){
					var selectedTopic = $('div.selected-topic');
					var itemCount = selectedTopic.find('.contributor-articles-list li').size();
					$.each(data.content, function(index, item) {
						var publishDate = item.date ? moment(item.date).format('DD MMM YYYY'): '';
						var contentHtml = '<li class="list-item-' + (++itemCount) + '"><i class="icon" data-icon="'+ item.icon + '"></i><div class="contributor-article-info"><h4><a href="'
							+ item.siteUrl + '"><span class="logo-search">' + item.siteDisplayNameHtml + '</a></h4><h5><a href="' + item.url + '">' +
							item.title + '</a></h5><span>' + publishDate + '</span></div></li>';
						logger.log("contentHtml: " + contentHtml);
						selectedTopic.find('ul.contributor-articles-list').append(contentHtml);
					});
					//Increment loadMorePage
					selectedTopic.attr('data-load-more-page', parseInt(++loadMorePage));

					// Fix height of items.
					utils.adjustHomepageElements();
				}
			});
		};
		
		$(function(){
			init();
		});
});
// Download offer module
TT(["context", "context/ui", "util/logging", "util/debug", "lib/jquery"],
	function (ctx, ui, logging, debug, $) {

	var isDebug = debug.check("debug_dlo"),
		logger = logging.getLogger(isDebug);

	if (ctx.isDloFulfillment) ui.on("ready", fulfillment);
	else if (ctx.isDloWidget) ui.on("ready", widget);
	else logger.log("Context contains no download offer");

	var w;

	function widget() {
		// capture and transfer the custom button text to reg form page 2
		var $regDiv = $('section.sign-up-wrapper.cascading-reg div#inlineRegistration');
		if ($regDiv.length) {
			var $submit = $('#inlineRegSubmit', $regDiv);
			if ($submit.length) {
				var submitText = $submit.eq(0).attr('value');
				if (submitText) {
					$regDiv.on("tt.inlinereg.fullReg.load", function () {
						$submit = $('#inlineRegSubmitPage2', $regDiv);
						if ($submit.length) {
							$submit.eq(0).attr('value', submitText);
						}
					});
				}
			}
		}
	}

	function fulfillment() {
		// open asset button in new window automatically (if window doesn't already exist)
		var $assetButton = $('body#cascading-fulfillment div.download-now-info a.button.teal-button');
		if ($assetButton.length) {
			var href = $assetButton.eq(0).attr("href");
			if (href) {
				var name = "ff-download-" + ctx.dloid;
				checkDownloadWindow(href, name);
				// add click event to button to open if popups are blocked
				$assetButton.click(function (evt) {
					evt.preventDefault();
					checkDownloadWindow(href, name);
				});
			}
		}
	}

	function checkDownloadWindow(url, name) {
		if (w && !w.closed) {
			logger.log("Existing download window handle: ", w);
			w.focus();
		} else {
			openDownloadWindow(url, name);
		}
	}

	function openDownloadWindow(url, name) {
		var width = Math.max(0.8 * ui.viewport.w, 800);
		var height = Math.max(0.8 * ui.viewport.h, 600);
		var top = (screen.height/2) - (height/2);
		var left = (screen.width/2) - (width/2);
		var params = "scrollbars=1";
		params += ",height=" + height + "px";
		params += ",width=" + width + "px";
		params += ",top=" + top + "px";
		params += ",left=" + left + "px";
		w = window.open(url, name, params, false);
		if (w && !w.closed) {
			logger.log("New download window handle: ", w);
			w.focus();
		} else {
			logger.log("No download window handle: must have been blocked");
		}
	}

});
// Download offer module
TT(["context", "context/ui", "util/logging", "util/debug", "lib/jquery"],
	function (ctx, ui, logging, debug, $) {

	var isDebug = debug.check("debug_offer"),
		logger = logging.getLogger(isDebug);

	if (ctx.zone === 'MARKETING_OFFER_FULFILLMENT') ui.on("ready", fulfillment);
	else if (ctx.zone === 'MARKETING_OFFER') ui.on("ready", widget);
	else logger.log("You are not on an offer page.");

	var w;

	function widget() {
		// capture and transfer the custom button text to reg form page 2
		var $regDiv = $('section.sign-up-wrapper.cascading-reg div#inlineRegistration');
		if ($regDiv.length) {
			var $submit = $('#inlineRegSubmit', $regDiv);
			if ($submit.length) {
				var submitText = $submit.eq(0).attr('value');
				if (submitText) {
					$regDiv.on("tt.inlinereg.fullReg.load", function () {
						$submit = $('#inlineRegSubmitPage2', $regDiv);
						if ($submit.length) {
							$submit.eq(0).attr('value', submitText);
						}
					});
				}
			}
		}
	}

	function fulfillment() {
		// open asset button in new window automatically (if window doesn't already exist)
		var $assetButton = $('body#cascading-fulfillment div.download-now-info a.button.teal-button');
		if ($assetButton.length) {
			var href = $assetButton.eq(0).attr("href");
			if (href) {
				var name = "ff-download-" + ctx.uid;
				checkDownloadWindow(href, name);
				// add click event to button to open if popups are blocked
				$assetButton.click(function (evt) {
					evt.preventDefault();
					checkDownloadWindow(href, name);
				});
			}
		}
	}

	function checkDownloadWindow(url, name) {
		if (w && !w.closed) {
			logger.log("Existing download window handle: ", w);
			w.focus();
		} else {
			openDownloadWindow(url, name);
		}
	}

	function openDownloadWindow(url, name) {
		var width = Math.max(0.8 * ui.viewport.w, 800);
		var height = Math.max(0.8 * ui.viewport.h, 600);
		var top = (screen.height/2) - (height/2);
		var left = (screen.width/2) - (width/2);
		var params = "scrollbars=1";
		params += ",height=" + height + "px";
		params += ",width=" + width + "px";
		params += ",top=" + top + "px";
		params += ",left=" + left + "px";
		w = window.open(url, name, params, false);
		if (w && !w.closed) {
			logger.log("New download window handle: ", w);
			w.focus();
		} else {
			logger.log("No download window handle: must have been blocked");
		}
	}

});
// EProduct Fulfillment: sets a cookie that tracks the sign-up transaction
// displays an extra splash when the user completes registration and
// comes back to the originating page. We only care about the user coming
// back to the same page. If they don't finish reg and go to another page
// first we delete the cookie anyway.
TT(["context", "context/ui", "util/cookie", "lib/jquery"],
	function (ctx, ui, cookie, $) {

	var cookieName = "eprodFulfillment",
		cookieValue = window.location.href,
		loggedIn = ctx.getUser().loggedIn;

	if (cookie.getCookie(cookieName) == cookieValue && loggedIn) {
		// user registered and came back to the same page
		$('div.eproducts-fulfillment').show();
	}
	// if they have the cookie but are not logged in it means they bailed
	// on registration

	// delete the cookie regardless - it's a one-time thing
	cookie.deleteCookie(cookieName);

	// if they aren't logged in, add a submit handler to create the cookie
	// to test when they return from registering
	if (!loggedIn) {
		ui.on("ready", function () {
			// set the cookie on form submit
			$("#downloadEzineForm").submit(function () {
				cookie.setCookie(cookieName, cookieValue);
			});
		});
	}

});
// UI Search module
TT(["context", "util/logging", "util/url", "lib/jquery", "util/debug"], function (ctx, logging, url, $, debug) {

	var isDebug = debug.check("debug_ui");
	var logger = logging.getLogger(isDebug);
	var Search = {

		init: function() {
			
			$('.search-results-load-more').show().click(function() {
				Search.loadMore();
			});
			
		},

		loadMore: function() {
			logger.log("Loading more content for Search results...");
			
			var lang = $('#searchResults').data('lang');
			var searchTerm = url.getParam('q').split('+').join('%20');
			var searchType = url.getParam('type') || 'article';
			var sortField = url.getParam('sortField') || 'Relevancy';
			var pageNo = url.getParam('pageNo') || 1;
			var loadMorePage = $('#searchResults').attr('data-loadmorepage');
			var searchChannelId = $('div#searchResults').attr('data-searchChannel');
			var timeZone = $('div#searchResults').attr('data-timeZone');
			var filterType = $('div#searchResults').attr('data-filterType');
			var restrictSite = $('div#searchResults').attr('data-restrictSite');
			
			if (!searchTerm || !searchType) {
				logger.log("Unable to make load more request...");
				return;
			}
			
			var loadMoreUrl = '/techtarget-ecm/services/loadMore/searchResults/' + searchChannelId +'/' + searchTerm + '/' + searchType + '/' + filterType + '/' + sortField + '/' + pageNo + '/' + loadMorePage;
			logger.log("Loading more with url: " +  loadMoreUrl);
			$.getJSON(loadMoreUrl).always(function(data) {
				if (!data.success) {
					logger.log("Unable to load more content for search results");
					return;
				}
				
				moment.locale(lang, {
				    monthsShort : ctx.i18n('date.shortMonths').split(',')    
				});
				
				if (data.content.length) {
					$.each(data.content, function(index, item) {
						logger.log('Adding item with title "' + item.title + '":');
						var html;
						var siteName;
						if (!!item.siteName && !restrictSite) {
							siteName = (item.siteName.indexOf("search") != -1) ? item.siteName.replace('search', '<span>Search</span>') : item.siteName ;
						}
						var summary = item.summary ? (item.summary.length > 215 ? $.trim(item.summary).substring(0, 215).split(" ").slice(0, -1).join(" ") + "..." : item.summary) : '';
						var postDate = item.date ? moment(item.date).format(data.dateFormat) : '';
						if (data.isConversation) {
							var userHtml = '<span class="search-result-head-author">' + ctx.i18n('search.by_user', item.author) + '</span>';
							if (item.vgnContributorUrl) {
								userHtml = '<a href="' + item.vgnContributorUrl + '">' + userHtml + '</a>';
							}					
							html = '<section class="section search-result"><div class="search-result-head">';
							if (!restrictSite) {
								html += '<h3 class="search-result-head-site">' + siteName + '</h3>';
							} 
							html +=  + userHtml + '<span class="search-result-head-date">' + postDate + 
							'</span></div><div class="search-result-body"><h4 class="section-title no-icon"><a href="' + item.url + '">' + item.title + 
							'</a></h4><p>' + summary + ' <a href="' + item.url + '">' + ctx.i18n('search.continue_reading') + '</a></p></div></section>';
						} else if (data.isDefinition) {
							var glossaryHtml = '';
							if (item.resultLinks && item.resultLinks.length) {
								glossaryHtml = '<div class="associated-glossaries"><p>' + ctx.i18n('search.associated_glossaries') + '</p><ul>';
								$.each(item.resultLinks, function(index, link) {
									glossaryHtml += '<li><a href="' + link.url + '">' + link.text + '</a></li>';
								});
								glossaryHtml += '</ul></div>';
							}
							html = '<section class="section search-result"><div class="search-result-head">';
							if (!restrictSite) {
								html += '<h3 class="search-result-head-site">' + siteName + '</h3>';
							} 
							html += '<span class="search-result-head-definition">' + ctx.i18n('search.definition') + '</span></div><div class="search-result-body definition-body">' + 
							'<h4 class="section-title no-icon"><a href="' + item.url + '">' + item.title + '</a></h4><p>' + summary + ' <a href="' + item.url + '">' + 
							ctx.i18n('search.read_full_definition') + '</a></p>' + glossaryHtml + '</div></section>';
						} else {
							var resultLabelHtml = item.resultLabel ? '<span class="search-result-head-type">' + ctx.i18n('search.'+item.resultLabel) + '</span>' : '';
							var iconHtml = item.icon ? '<i class="icon" data-icon="' + item.icon + '"></i>' : '';
							var sectionTitleClass = item.icon ? 'section-title' : 'section-title no-icon';
							var userHtml = '';
							if(item.resultContributors.length){
								userHtml = '<span class="search-result-head-author">' + ctx.i18n('search.by_contributors');
								$.each(item.resultContributors, function(index, contributor) {
									userHtml +='<a href="' + contributor.url + '">'+  contributor.title + '</a>';
									if (index != item.resultContributors.length - 1){
										userHtml += ','
									}
								});
								userHtml += '</span>';
							}
							var headHtml; 
							if (item.contentContributor) {
								headHtml = '<div class="search-result-head"><div class="image-resize"> <img src="'+ item.summaryImageUrl + '"></div><div class="search-result-contributor-info"><a href="' 
								+ item.url + '">' + item.title + '</a>';
								var title = item.ttEmployee ? item.jobTitle : item.companyName;
								if (title) {
									headHtml += '<span class="search-result-head-title">' + title + '</span>';
								}
								headHtml +='</div></div>';
							} else {
								headHtml = '<div class="search-result-head">';
								if (!restrictSite) {
									headHtml += '<h3 class="search-result-head-site">' + siteName + '</h3>';
								}
								headHtml += userHtml + resultLabelHtml + '<span class="search-result-head-date">' + postDate + '</span></div>';
							}

							html = '<section class="section search-result">' + headHtml + '<div class="search-result-body"><h4 class="' + sectionTitleClass + '">' + 
							iconHtml + '<a href="' + item.url + '">' + item.title + '</a></h4><p>' + summary + ' <a href="' + item.url + '">' + ctx.i18n('search.continue_reading') + 
							'</a></p></div></div></section>';
							logger.log("Added following html", html);
						}
						$('#searchResults').append(html);
					});
				}
				
				// Button no longer needed
				if (!data.hasMoreContent) {
					logger.log("No more content to be loaded; removing button");
					$('.search-results-load-more').remove();
				} else {
					logger.log("More content can be loaded; updating page number");
					$('#searchResults').attr('data-loadmorepage', ++loadMorePage);
				}
			});
		}
	};

	$(function(){
		Search.init();
	});

});
// UI Chapters Menu module
TT(["lib/jquery"], function ($) {

	var ChaptersMenu = {

		init: function() {
			this.populateMenu();
		},

		populateMenu: function() {
			var requiredItem = $('.chapters-bar').data("required-item");

			if (requiredItem && $(requiredItem).length === 0) {
				$('.chapters-bar').hide();
				$('.actions-bar-chapters').hide();
			} else {
				var items = [];

				$('section.section').each(function() {
					if ($(this).data("menu-title")) {
						items.push('<li class="chapters-bar-item"><a href="#">' + $(this).data("menu-title") + '</a></li>');
					}
				});

				$('.chapters-bar-list').append( items.join('') );
			}
		}

	};

	$(function(){
		ChaptersMenu.init();
	});

});
// UI Social share counts module
TT("ui/sharecounts", ["util/logging", "util/debug", "lib/jquery", "context"], function (logging, debug, $, ctx) {

	var isDebug = debug.check("debug_sharecounts"),
		logger = logging.getLogger(isDebug);

	// Function returns a promise that is fulfilled upon completion of all 4 social
	// media service calls for share counts.
	// Steps were taken so that if an ajax call fails its count still resolves to 0
	// and the promise is resolved and not rejected.
	// Note that Google+ does not use the API and instead calls a url on our own
	// host. This is a workaround to bypass google developer api auth an quotas.
	// More information in the java controller class.
	function getCounts(pageUrl) {
		// Facebook
		var $fb = $.Deferred(function (defer) {
			$.ajax({
				url: "https://api.facebook.com/method/links.getStats?format=json&urls=" + pageUrl,
				dataType: 'jsonp'
			})
			.done(function (o) {defer.resolve(parseInt(o[0].total_count, 10))})
			.fail(function () {defer.resolve(0)});
		});
		// Twitter
		var $tw = $.Deferred(function (defer) {
			$.ajax({
				url: "http://urls.api.twitter.com/1/urls/count.json?url=" + pageUrl,
				dataType: 'jsonp'
			})
			.done(function (o) {defer.resolve(parseInt(o.count, 10))})
			.fail(function () {defer.resolve(0)});
		});
		// LinkedIn
		var $li = $.Deferred(function (defer) {
			$.ajax({
				url: "https://www.linkedin.com/countserv/count/share?format=jsonp&url=" + pageUrl,
				dataType: 'jsonp'
			})
			.done(function (o) {defer.resolve(parseInt(o.count, 10))})
			.fail(function () {defer.resolve(0)});
		});
		// Google+
		var $gp = $.Deferred(function (defer) {
			$.ajax({
				url: "/techtarget-ecm/services/socialCounts/google?url=" + pageUrl
			})
			.done(function (o) {defer.resolve(parseInt(o.count, 10))})
			.fail(function () {defer.resolve(0)});
		});
		// Xing
		var $xg = $.Deferred(function (defer) {
			if (ctx.lang != 'de') {
				logger.log("Not on a german page, not executing xing call.");
				defer.resolve(0);
				return;
			}
			logger.log("Executing Xing on German page");
			$.ajax({
				url: "/techtarget-ecm/services/socialCounts/xing?url=" + pageUrl
			})
			.done(function (o) {defer.resolve(parseInt(o.count, 10))})
			.fail(function () {defer.resolve(0)});
		});
		
		var promise = $.when($fb, $tw, $li, $gp, $xg)
			.then(function (fb, tw, li, gp, xg) {
				var o = {
					facebook: format(fb, 1),
					twitter: format(tw, 1),
					linkedin: format(li, 1),
					googleplus: format(gp, 1),
					xing: format(xg, 1),
					total: format(fb + tw + li + gp + xg, 1)
				};
				logger.log("socialcounts", o);
				return o;
			})
			.promise();

		return promise;
	}

	// Format the count to reduce the number of characters, e.g. 1.8k instead of 1,850.
	function format(number, decimalPlaces) {
		logger.log("Formatting number:", number);
		if (number < 1000) return number;
		var x = ('' + number).length;
		var decimalPlaces = Math.pow(10, decimalPlaces);
		x -= x % 3;
		var formattedNumber = Math.round(number * decimalPlaces / Math.pow(10, x)) / decimalPlaces + " kMGTPE"[x / 3];
		logger.log("formatted number:", formattedNumber);
		return formattedNumber;
	}

	return {
		getCounts: getCounts
	};

});
// UI Share Buttons module
TT(["context", "context/ui", "util/bitly", "util/debug", "util/logging", "lib/jquery"],
	function (ctx, ui, bitly, debug, logging, $) {

	var isDebug = debug.check("debug_sharebuttons"),
		logger = logging.getLogger(isDebug);

	// There are separate ready events for social, printer friendly, and email
	// so each can exit independently depending on what's on the page.

	// Social media buttons and counts. Exit if not applicable
	ui.on('ready', function () {

		var $fb = $('.socialMedia-facebook'),
			$tw = $('.socialMedia-twitter'),
			$gp = $('.socialMedia-google'),
			$li = $('.socialMedia-linkedin'),
			$xg = $('.socialMedia-xing');

		if ($fb.length + $tw.length + $gp.length + $li.length + $xg.length == 0) {
			// exit - this page doesn't have social buttons
			return;
		}

		var pageUrl = $('link[rel="canonical"]').attr('href') || window.location.href;

		//Remove Share Counts for all Social Networks VGNSITES-15261
		//May change in the future as new offerings pop up based on this change.
		//Disable share counts for microsites.
		/*if (!ctx.microsite) {
			sharecounts.getCounts(pageUrl).done(function (counts) {
				$('span', $fb).text(counts.facebook);
				$('span', $tw).text(counts.twitter);
				$('span', $gp).text(counts.googleplus);
				$('span', $li).text(counts.linkedin);
				$('span', $xg).text(counts.xing);
				if(counts.total != 0) {
					$('.main-article-share-count.inline-social-icons span').text(counts.total);
				}
			});
		}*/

		var $a_fb = $('a', $fb),
			$a_tw = $('a', $tw),
			$a_gp = $('a', $gp),
			$a_li = $('a', $li),
			$a_xg = $('a', $xg);

		var $any = $a_fb.add($a_tw).add($a_gp).add($a_li).add($a_xg);

		$any.one('click.bitly', function (e) {
			e.preventDefault();

			// cancel for all after any one
			$any.off('click.bitly');
			logger.log('clicked and removed');

			var $curr = $(this);
			var w = popup();

			logger.log("Fetching bit.ly url for shares...");
			bitly.shortenUrlPromise(pageUrl).done(function (shortenedUrl) {

				var articleUrl = encodeURIComponent(shortenedUrl || pageUrl);
				var articleTitle = encodeURIComponent($('.main-article-title').text() || document.title);
				var xingFollowUrl = encodeURIComponent($xg.data('follow') || '');
				// Facebook
				$a_fb.attr('href', 'http://www.facebook.com/share.php?u=' + articleUrl + '&t=' + articleTitle);
			    // Twitter
			    var twitterUrl = 'https://twitter.com/intent/tweet?text=' + articleTitle + '&url=' + articleUrl
				+ ($tw.attr('data-account') ? ('&via=' + encodeURIComponent($tw.attr('data-account'))) : '')
				+ ($tw.attr('data-related') ? ('&related=' + encodeURIComponent($tw.attr('data-related'))) : '')
			    $a_tw.attr('href', twitterUrl);
				// Google+
				$a_gp.attr('href', 'https://plus.google.com/share?url=' + articleUrl);
				// LinkedIn
				$a_li.attr('href', 'http://www.linkedin.com/shareArticle?mini=true&url=' + articleUrl + '&title=' + articleTitle);
				// Xing
				$a_xg.attr('href', 'https://www.xing.com/spi/shares/new?url=' + articleUrl + '&follow_url=' + xingFollowUrl);

				$any.on('click', popupEvt);

			}).then (function () {
				var href = $curr.attr('href');
				logger.log("Chaning popup location to %s", href);
				w.location = href;
			});

		});
	});

	// Printer friendly
	ui.on('ready', function () {
		$('li.contentTools-print a').attr('href', window.location.href.split('?')[0] + '?vgnextfmt=print');
	});

	// Email. Exit if no applicable
	ui.on('ready', function () {
		var $a = $('li.contentTools-email a');
		if (!$a.length) return;

		// set the link for emailing
		var endpointUrl = 'http://api.addthis.com/oexchange/0.8/forward/email/offer?';
		var emailTemplates = {
			'en': 'TechTargetSearchSites',
			'es': 'TechTargetSearchSitesSpanish'
		};
		var language = ctx.lang;
		var pageTitle = $('.main-article-title').text() || document.title;
		var endpointConfig = {
			pubid: 'uxtechtarget',
			url: window.location.href,
			title: pageTitle,
			email_template: emailTemplates[language],
			ct: 1
		};
		var emailUrl = endpointUrl + $.param(endpointConfig)
		$a.attr('href', emailUrl).on('click', popupEvt);
	});

	    function popup(url) {
		return window.open(url || '', 'popupWindow', 'width=600,height=600');
	}

	function popupEvt(e) {
		e.preventDefault();
	    popup($(this).attr('href'));
	}

});
TT([ "context", "lib/jquery", "util/logging", "util/debug"], function(ctx, $, logging, debug) {
	var isDebug = debug.check("debug_ui");
	var logger = logging.getLogger(isDebug);
	//Only execute any code in here if we are actually on an abstract/bpr page.
	if(!!ctx.bprAbstract && $('div#inlineRegistration').length){
		//Function used to handle the corner case where a user logs in from a search site and comes back to a microsite
		//abstract page with a custom domain. The cookie reflection that normally occurs here occurs after the crsCheck
		//that happens on userreg, so even though we are technically cookied on the custom domain, the page doesn't see the user
		//as 'logged in' and shows the logged out version of the page the first time the user lands here. If they refresh the page, the
		//CRS check will pass and they will see the logged in version of the page. 
		//
		//We have made the userreg logic/code that usually does the crs check an external function on the userreg side (instead of an anonymous 
		//function), so that we are able to call it ourselves, directly after the cookie reflection occurs. For now, this only affects
		//the abstract page registration form on standalone microsites.
		ctx.getUser(function () {
			if(window.micrositeInit && window.hasOwnProperty("micrositeInit") && typeof window.micrositeInit === "function"){
				logger.log("Calling micrositeInit()");
				window.micrositeInit();
			}
		});
	}
});
// UI latest module
TT(["context/ui", "ui/main", "lib/jquery"], function (ui, TT, $) {

	var Latest = {

		init:function(){
			this.bindEventHandlers();
			
			// only calculate nav width for mobile
			if(TT.mq < TT.mqs.tablet) 
				this.setNavWidth();
		},

		bindEventHandlers:function(){
			var $navItems = $('.latest-nav-item');

			$('.latest-nav-item').on('click', function(e){
				e.preventDefault();
				var goTo = $navItems.index(this);

				$navItems.removeClass('selected');
				$(this).addClass('selected');

				$('.latest-content').eq(goTo).fadeIn().siblings('.latest-content').hide();
				
				// resize titles on click for hidden ones
				Latest.resizeLatestTitles();
			});

			ui.on("load resize", function(){
				Latest.resizeLatestTitles();
				if (ui.isMobile()) {
					// only calculate nav width for mobile
					Latest.setNavWidth();
				}
			});

		},

		setNavWidth:function(){
			if(TT.mq < TT.mqs.tablet) {
				var width = 0;
				var items = $('.latest-nav-item');

				for(var i=0; i<items.length; i++){
					width += items.eq(i).outerWidth(true);
				}
				$('.latest-nav').width(width+35);

			} else {
				$('.latest-nav').css("width", "");
			}
		},

		// resizes the latest network coverage h3 titles to all be the height of the longest one so separator bars line up
		resizeLatestTitles:function(){
			var latestTitle = $('.latest-wrapper .latest-content li h3:visible');
			var maxHeight = 0;

			if(TT.mq > TT.mqs.tablet) {
				latestTitle.each(function() {
					maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
				});
				latestTitle.each(function() {
					$(this).height(maxHeight);
				});

			} else {
				latestTitle.css("height", "");
			}
		}

	};

	$(function(){
		Latest.init();
	});

});
// UI image overlay module
TT(["context/ui", "lib/jquery"], function (ui, $) {

	var ImageOverlay = {

		init:function(){
			this.bindEventHandlers();
		},
		
		
		bindEventHandlers:function(){
			$('.main-article-image').on('click', function(e){
				if($(e.target).closest('figcaption').length !== 0){ return; }
				var src = $(this).attr('data-img-fullsize');
				ImageOverlay.openOverlay(src);
			});
			
			$('.image-overlay').on('click', function(){
				$(this).fadeOut(200);
			});
			
			ui.on("resize", function(){
				if($('.image-overlay').is(':visible')){
					ImageOverlay.centerImage();
				}
			});
		},
		
		
		openOverlay:function(imgSrc){
			var $img = $('<img src="'+imgSrc+'">');

			$('.image-overlay').fadeIn(250).find('.image-overlay-img').html($img);
			
			this.centerImage();
		},
		
		
		centerImage:function(){
			var $img = $('.image-overlay').find('img');
			var margin = ($(window).height() - $img.height()) / 2;
			
			$img.css({
				// removing centering  --  'margin-top': margin,
				'margin-top': '50px',
				'visibility': 'visible'
			});
		}
	};

	$(function(){
		ImageOverlay.init();
	});

});
// UI homepage module
TT(["context/ui", "ui/main", "lib/jquery", "ui/utils"], function (ui, TT, $, utils) {

	var Homepage = {
		
		init:function(){
			this.bindEventHandlers();
		},
		
		bindEventHandlers:function(){

			// Click handler for expanding topics sections
			$('.topics-collapse-toggle h5').hammer().on(TT.click, function(){
				var $topicsToggle = $(this).parent().find('.toggle-marker');
				var $topicsSection = $(this).parent().find('.homepage-topics-subtopics');

				if($topicsSection.hasClass('open')){
					$topicsToggle.text('+');
					$topicsSection.removeClass('open');
				} else {
					$topicsToggle.text('-');
					$topicsSection.addClass('open');
				}
			});


			// Multimedia dropdown
			$('.dropdown-toggle').hammer().on(TT.click, function(e){
				// search results type drop down only on mobile
				if (TT.mq > TT.mqs.mobile && $(this).parents().hasClass('search-results-type-menu')) {
					return false;
				} else {
					$(this).toggleClass('selected');
					$(this).next('.dropdown-menu').toggle();
					e.stopPropagation();
				}
			});

			// Resizing and resetting heights on homepage elements
			ui.on("load resize", utils.adjustHomepageElements)
			.on("adInterstitial", function (evt) {
				if (!evt.state) {
					// interstitial is not on
					utils.adjustHomepageElements();
				}
			});
		}
	};

	$(function(){
		Homepage.init();
	});

});
// UI Ask a Question widget 
TT([ "context", "util/url", "lib/jquery" ], function(ctx, url, $) {

	var AskQuestion = {
		init: function() {

		var askQuestionError = $('#create_question_mini_widget_error');

		askQuestionError.hide();

		$("#create_question_mini_widget").submit(function (event) {
			var title = $.trim($('#create_question_mini_widget #post_title').val());
			if (title == '') {
					askQuestionError.show();
					event.preventDefault();
				}
			});
		}
	};

	$(function() {
		AskQuestion.init();
	});

});
// ITKE Commenting module
TT(["context", "util/debug", "util/logging", "lib/jquery", "lib/itkeRepoRegUser", "optimizely", "ui/main"],
	function (ctx, debug, logging, $, UserregUser, optly, TT) {

	var isDebug = debug.check("debug_commenting");
	var logger = logging.getLogger(isDebug);

	var Commenting = {

		init: function() {

			var repoComments = $('#commenting');

			var homeRepoComments =  $('#homesplashcommenting-1');

			/**
			 * This function is responsible for all the comment functionality a Comment section may need. 
			 * It's meant to be largely self-contained in case multiple comment sections exist on a single page.
			 * Type is accounted for in case some pages have different types of sections e.g. comments vs discussions
			 * References to self.item are to enforce self-containment and to allow access to properties inside AJAX callbacks.
			 */
			function commentItem(divId, type) {

				/**
				 * Various properties, configurations, and data stored by the object.
				 */

				// Self is used to refer to the current object instead of 'this' so that it may be called
				// from every method.
				var self = this;

				// The master-div of the object, essentially.
				self.item = divId;

				// id of the item being commented on; param is set as data attributes on item div in the source
				self.referenceId = $(self.item).data('commentReferenceId') || false;
				
				// itkerepo solr id of the item being commented on; param is set as data attributes on item div in the source
				self.itkeRepoSolrId = $(self.item).data('itkerepo-id') || false;

				// A comment item can belong either to a 'commentSection' or a 'discussion'.
				self.type = type;

				// commentSections refer to comment(s) as 'Comment(s)', whereas discussions refer to comments as 'Reply(ies)'
				self.postTypeSingular;
				self.postTypePlural;
				
				// the json service urls used in commenting
				var regexEng = new RegExp('.eng');
				var regexQa = new RegExp('.qa');
				if(regexEng.test(window.location.hostname)) {
					self.ajaxUrl = "http://itknowledgeexchange.eng.techtarget.com";
				} else if (regexQa.test(window.location.hostname)) {
					self.ajaxUrl = "http://itknowledgeexchange.qa.techtarget.com";
				} else {
					self.ajaxUrl = "http://itknowledgeexchange.techtarget.com";
				};
				var language =  $('html').attr('lang') || 'en';
				if( language == 'en' ) {
					self.ajaxUrlPath = "/discussions/wp-json/wp/v2/comments/";
				} else if ( language == 'es' ) {
					self.ajaxUrlPath = "/discussions-es/wp-json/wp/v2/comments/";
				} else if ( language == 'de') {
					self.ajaxUrlPath = "/discussions-de/wp-json/wp/v2/comments/";
				} else if ( language == 'fr') {
					self.ajaxUrlPath = "/discussions-fr/wp-json/wp/v2/comments/";
				}

				// div where comments are to be written.
				self.commentList = $(this.item).find('#repoComments');

				// Sort-order of the comments. Goes oldest ----> newest by default.
				self.sortByOldest = true;

				// An object that stores user data and methods to call userreg rest service. (Needs to be set outside of this object).
				self.user;

				// State of whether the minireg form is login or register
				self.registerForm;
				
				// reg/login form
				self.regLoginForm = $('#comments-reg-login');
				self.loginForm =  self.regLoginForm.find('.modal-login');
				self.regForm =  self.regLoginForm.find('.modal-register');
				self.forgotPasswordSelector = '#commenting, .modal-login';
				// update handle form
				self.updateHandleForm = $('#comments-reg-create');
				
				// Form the user submits.
				self.submittedForm;
				
				//Comment textarea being submitted
				self.commentTextArea;
				//Comment parent id
				self.commentParent;
				
				/** 
				 * Functions that deal with userreg interactions
				 */

				// This function dictates any userreg actions/error-handling that need to be taken before a user can submit a post.
				self.miniReg = function() {

					// Make sure no errors are saved from earlier userreg attempts.
					self.user.validations = [];

					if (!self.user.authenticated && self.registerForm) {
						
						self.register().always( function() {
							self.postComment();
						});

					} else if (!self.user.authenticated && !self.registerForm) {

						self.login().always( function() {
							self.postComment();
						});

					} else if (self.user.authenticated && !self.user.hasValidHandle) {

						// Attempt to create handle for an existing logged-in user, then post the comment.
						self.updateHandle().always( function() {
							self.postComment();
						});

					} else {
						// Logged-in user with handle. They're already good to go.
						self.postComment();
					};
				};
				
				// Function for registering a user, whose data can then be set on the page to login the newly created user.
				self.register = function() {
					logger.log("Register function called.");
					self.toggleLoginRegButton(true);

					// Set the email, handle, and passwords the user enters in the form.
					self.user.userEmail = self.regForm.find('input.comments-email').val();
					var password = self.regForm.find('input.comments-password').val();
					self.user.userDisplayName = self.regForm.find('input.comments-username').val();
					
					// Set the app-code for registration
					if (self.type == 'discussion') {
						self.user.appCode = 120;
					} else {
						self.user.appCode = 119;
					}

					// Make call to userreg to register the user, then set their cookies on the page.
					return self.user.register(password).always(function() {

						// Userreg request must have no errors, the user must be authenticated, and must have a valid handle.
						if (self.user.validations.length != 0 || !self.user.authenticated || !self.user.hasValidHandle) {

							// Display any userreg-caused errors on the page
							self.userregErrorHandling(self.user.validations);

							self.toggleLoginRegButton(false);
							
							return;
						};

						self.user.setCookies();
						logger.log("User registered....");
						//close reglogin modal
						$.colorbox.close();
						//unbind reg modal, bind submission
						$(".comments-form-btn.submit").removeClass('cboxElement');						

					});
				};

				// Function for retrieving a user's data + cookies from userreg, which can then be set on the page to login the user.
				self.login = function() {
					logger.log("Login function called.");
					self.toggleLoginRegButton(true);
					
					// Set the email, handle, and passwords the user enters in the form.
					self.user.userEmail = self.loginForm.find('input.comments-email').val();
					var password = self.loginForm.find('input.comments-password').val();

					// Make call to userreg to retrieve user data + cookies, then set on the page.
					return self.user.login(password).always(function() {

						// Userreg request must have no errors and the user must be authenticated.
						if (self.user.validations.length != 0 || !self.user.authenticated) {

							// Display any userreg-caused errors on the page
							self.userregErrorHandling(self.user.validations);

							self.toggleLoginRegButton(false);
							
							return;
						};

						self.user.setCookies();
						logger.log("User logged in....");
						// If user doesn't have a valid handle, show 'create user name' field. Otherwise, remove the field (even though it's hidden).
						if (!self.user.hasValidHandle)  {
							logger.log("User is logged-in but does not have a handle; showing create username modal.");
							if (TT.mq < TT.mqs.tablet) {
								$.colorbox({inline:true,scrolling:false,href:"#comments-reg-create",width:"320px",height:"310px"});
							} else {
								$.colorbox({inline:true,scrolling:false,href:"#comments-reg-create",width:"630px",height:"320px"});
							}
							self.toggleLoginRegButton(false);
							return;	
						} else {
							//close reglogin modal
							$.colorbox.close();
							//unbind reg modal, bind submission
							$(".comments-form-btn.submit").removeClass('cboxElement');
						};

					});

				};

				// Function for creating a handle for an existing user. Note that handle = userDisplayName = user name (sorry for the confusion!)
				self.updateHandle = function() {
					logger.log("UpdateHandle function called.");
					self.toggleLoginRegButton(true);
					// Set desired user name on user object so it can be used in call to userreg.
					var userHandleField = self.submittedForm.find('input.comments-create-username');
					self.user.userDisplayName = userHandleField.val();

					// Make call to userreg to update handle.
					return self.user.updateUserDisplayName().always(function() {

						// Userreg request must have no errors, the user must be authenticated, and must have a valid handle for this function to be successful.
						if (self.user.validations.length === 0 && self.user.authenticated && self.user.hasValidHandle) {
							logger.log("User handle update successful");
							//unbind reg modal, bind submission
							$(".comments-form-btn.submit").removeClass('cboxElement');
							//close reglogin modal
							$.colorbox.close();
							//Change copy of button
							$(".comments-form-btn.submit").html(ctx.i18n('commenting.add_my_comment'));
							self.user.hasValidHandle == true;
						} else {
							logger.log("User handle update unsuccessful");
							// Display any userreg-caused errors on the page (this function is found in itkeRepoReg.js)
							self.userregErrorHandling(self.user.validations);
							self.toggleLoginRegButton(false);
							return;
						};
					});

				};

				// CommentForm is the submitted form that has errors. Validations is the list of errors returned from self.user's call to userreg.			
				self.userregErrorHandling = function(validations) {
					logger.log("Handling userreg errors: " + validations.toString());

					if( self.registerForm ) {
						//show errors on reg form
						var miniRegHandle = self.regForm.find('input.comments-username');
						var miniRegEmail = self.regForm.find('input.comments-email');
						var miniRegPassword = self.regForm.find('input.comments-password');
					} else {
						//show errors on login form
						var miniRegHandle = self.submittedForm.find('input.comments-create-username');
						var miniRegEmail = self.loginForm.find('input.comments-email');
						var miniRegPassword = self.loginForm.find('input.comments-password');
					}

					// Remove any existing errors from the form
					self.removeErrors(self.submittedForm);

					// Dirty fix for when a "We could not locate your account" error is returned even though the user didn't enter an email
					var noEmail;

					// Iterates through each error and displays error message, updates class of field to "error"
					$.each(validations, function(index, error) {

						// Handle field errors
						if (error.id == 'handle') {

							// Handle was not supplied in call to userreg.
							if (error.status == 'REQUIRED') {
								self.displayError(ctx.i18n('commenting.please_enter_a_username'), miniRegHandle);
							};

							// Handle was not a valid length.
							if (error.status == 'INVALID_LENGTH') {
								self.displayError(ctx.i18n('commenting.usernames_must_be'), miniRegHandle);
							};

							// Handle was not a valid format.
							if (error.status == 'INVALID_FORMAT') {
								self.displayError(ctx.i18n('commenting.usernames_must_be'), miniRegHandle);
							};

							// Handle was not unique.
							if (error.status == 'NOT_UNIQUE') {
								self.displayError(ctx.i18n('commenting.this_username_is_already_taken'), miniRegHandle);
							};

						};

						// Email field errors
						if (error.id == 'email') {

							// Email was not supplied in call to userreg.
							if (error.status == 'REQUIRED') {
								self.displayError(ctx.i18n('commenting.please_enter_a_valid_email'), miniRegEmail);
								noEmail = true;
							};

							// No user was found in userreg with the input email address.
							if (error.status == 'NO_MATCH' && !(noEmail)) {				
								self.displayError(ctx.i18n('commenting.we_could_not_locate_your_account'), miniRegEmail);
							};

							// See RestRegistrationService.java in userreg for regexp responsible for format validation.
							if (error.status == 'INVALID_FORMAT') {
								self.displayError(ctx.i18n('commenting.please_enter_a_valid_email'), miniRegEmail);
							};

							// User already exists in userreg with that address. So push them to the login form.
							if (error.status == 'NOT_UNIQUE') {
								self.displayError(ctx.i18n('commenting.a_techtarget_account_already_exists'), miniRegEmail);
							};

						};

						// Password field errors
						if (error.id == 'password') {

							// Password was not supplied in call to userreg.
							if (error.status == 'REQUIRED') {
								self.displayError(ctx.i18n('commenting.please_enter_a_password'), miniRegPassword);
							};

							// Password does not match the stored value in userreg for the specified user.
							if (error.status == 'NO_MATCH') {
								self.displayError(ctx.i18n('commenting.your_password_is_incorrect', '<a href="#forgotPasswordModal" class="forgotPasswordLink">'), miniRegPassword);
							};

							// Password was not a valid length.
							if (error.status == 'INVALID_LENGTH') {
								self.displayError(ctx.i18n('commenting.please_enter_a_password'), miniRegPassword);
							};

						};

						// Userreg is unavailable.
						if (error.id == 'service' && error.status == 'UNAVAILABLE') {			
							self.displayError(ctx.i18n('commenting.there_was_an_error_processing_your_information'));
						};

						// For whatever reason, a siteId was not supplied during a registration attempt.
						if (error.id == 'siteId' && error.status == 'REQUIRED') {
							self.displayError(ctx.i18n('commenting.there_was_an_error_processing_your_information'));
						};

					});

				};

				/**
				 * Functions for posting a comment (after any userreg interaction has taken place)
				 */

				// validates comment form and associated params needed for posting a comment.
				self.commentIsValid = function () {
					var isValid = false;
					// Text fields needed for posting a comment.
					var commentFormBody = self.commentTextArea;
					logger.log("Comment texarea id="+commentFormBody.id);
					// If user hasn't entered text or tries to submit the 'Add your comment' default text, form ain't valid.
					if ( (commentFormBody.val() === '') || (commentFormBody.val().trim() == commentFormBody.attr('placeholder'))) {
						// Display the error and update the classes of fields that need to be fixed. Uses function in itkeRepoReg.js
						self.displayCommentBodyError(ctx.i18n('commenting.please_enter_a_comment'), commentFormBody);
					} else {
						isValid = true;
					}
					logger.log("Validating comment body; valid = " + isValid);
					return isValid;
				};

				// Get the form data then validate, post, and retrieve new comment from solr to display on page.
				self.postComment = function () {

					// Validates comment text and displays any subsequent errors
					var commentSuccess = self.commentIsValid();

					// If the user isn't fully authenticated with handle and no userreg errors, or the comment text is not valid, then comment cannot be posted :(
					if (!commentSuccess || !self.user.authenticated || !self.user.hasValidHandle || self.user.validations.length > 0) {
						logger.log("Comment failed authenticity/validation check");
						$('.comments-form-btn.submit').removeAttr('style'); // Remove processing gif
						return;
					};
					
					// fields for the comment form.  
					//var commentFormBody = self.submittedForm.find('.comments-form-textarea');
					var commentFormBody = self.commentTextArea;
					var userEmail = self.user.userEmail;
					var userId = self.user.userId;
					var userDisplayName = self.user.userDisplayName;
					var language =  $('html').attr('lang') || 'en';
					var isNotified = $('section#commenting fieldset > #notifications').is(':checked') || false;
					
					// params needed to add a comment.
					var commentFormParams = {
							parentSolrId: self.itkeRepoSolrId,
							content: commentFormBody.val(),
							userId: userId,
							userEmail: userEmail,
							userDisplayName: userDisplayName,
							language: language,
							notifications: isNotified
					};
					if( self.commentParent ) {
						//check if comment is a reply
						commentFormParams['parentCommentSolrId'] = self.commentParent;
					}

					logger.log("Posting comment with params: " + commentFormParams);

					// Record with Optimizely if available
					optly.push(["trackEvent", "successful_comment_submission"]);

					// Add processing gif; remove in postComment()
					$('.comments-form-btn.submit').prop("disabled", true);
					$('.comments-form-btn.submit').css({
						'background' : 'transparent url(http://media.techtarget.com/rms/ux/images/global/redesign2/iconProcessing.gif) no-repeat 50% 50%',
						'border' : 'none',
						'color' : 'transparent'
					});

					// if we have a successful post, add comment to the page and update display.
					$.ajaxSetup({
					    xhrFields: {
					        withCredentials: true
					    }
					});
					var ajaxUrl = self.ajaxUrl + self.ajaxUrlPath;
					logger.log("Posting to "+ajaxUrl);
					$.post(ajaxUrl, commentFormParams).always(function(data) {
						logger.log("Data returned from service:");
						logger.log(JSON.stringify(data));

						if (data.status != 'approved') {
							self.displayCommentBodyError(ctx.i18n('commenting.there_was_an_error_processing_your_information'), commentFormBody);
							$('.comments-form-btn.submit').removeAttr('style'); // Remove processing gif
							$('.comments-form-btn.submit').prop("disabled", false); // Enable button
							return;
						}

						// Remove errors from the page
						self.removeErrors(self.submittedForm);

						// Add comment to comment array.
						self.writeComment(data);

						// update total number of comments to section title + any other necessary display changes
						self.updateCounts(true);

						// Scroll to the comment
						if (self.sortByOldest) {
							logger.log("Scrolling to #" + data.solrId);
							$('html,body').animate({scrollTop: $('#' + data.solrId).parent().offset().top - 60},'slow');
						}

						// Reset form text
						commentFormBody.val(commentFormBody.attr('placeholder'));
						$("#commenting .cleditorMain iframe").contents().find('body').html(commentFormBody.attr('placeholder'));
						$('.comments-form-btn.submit').removeAttr('style'); // Remove processing gif
						$('.comments-form-btn.submit').prop("disabled", false); // Enable button
						
					});

				};

				/**
				 * Functions that are called to update the page after comments are loaded and/or comment has been submitted.
				 */

				// Updates the page with the total number of comments.
				// Also makes any other display changes necessitated by an updated comment count and/or successful post.
				self.updateCounts = function(increment) {
					// If this commentItem belongs to the comment section, update the title text.
					if (self.type == 'commentSection') {
						var total = $('.comments-count-num').text();
						if(increment) total++;
						if( total == 0 ) return;
						logger.log("Updating counts...");
						// It's a huge pain to change the text, so just remove it and add it back with updated text when change from 'comments' to 'comment' or vice-versa.
						// Otherwise, you only need to update the comment count.
						$('.comments-count').show();
						if (total === 1) {
							$('.comments-center').find('.comments-count').remove();
							$('.comments-center').prepend('<h3 class="comments-count"><i class="icon" data-icon="z"></i><span class="comments-count-num">1</span>&nbsp;' + self.postTypeSingular + '</h3>');
						} else if (total === 2) {
							$('.comments-center').find('.comments-count').remove();
							$('.comments-center').prepend('<h3 class="comments-count"><i class="icon" data-icon="z"></i><span class="comments-count-num">2</span>&nbsp;' + self.postTypePlural + '</h3>');
						} else {
							$('.comments-count-num').text(total);
						}
						
						// Update comment count at the top of the page, if it exists
						if ($('.main-article-comment-count').length) {
							$('.main-article-comment-count').find('span').text(total);							
						}
						if (total > 1) {
							$('#sort').show();
						}

						return;
					};

				};

				self.toggleLoginRegButton = function(processing) {
					if(processing) {
						// Add processing gif
						self.regLoginForm.find('.comments-form-btn').css({
							'background' : 'transparent url(http://media.techtarget.com/rms/ux/images/global/redesign2/iconProcessing.gif) no-repeat 50% 50%',
							'border' : 'none',
							'color' : 'transparent'
						});
						self.regLoginForm.find('.comments-form-btn').prop("disabled", true);
						// Add processing gif
						self.updateHandleForm.find('.comments-form-btn').css({
							'background' : 'transparent url(http://media.techtarget.com/rms/ux/images/global/redesign2/iconProcessing.gif) no-repeat 50% 50%',
							'border' : 'none',
							'color' : 'transparent'
						});
						self.updateHandleForm.find('.comments-form-btn').prop("disabled", true); 
					} else {
						// Remove processing gif
						self.regLoginForm.find('.comments-form-btn').removeAttr('style'); 
						self.regLoginForm.find('.comments-form-btn').prop("disabled", false);
						self.updateHandleForm.find('.comments-form-btn').removeAttr('style'); 
						self.updateHandleForm.find('.comments-form-btn').prop("disabled", false);
					}
				};
			
				//reset all modal fields
				self.clearModalFields = function() {
					// Remove any existing errors from the form
					self.removeErrors(self.submittedForm);
					$(':input', self.regForm).each(function() {
					  var type = this.type, tag = this.tagName.toLowerCase();
					  if (type == 'text' || type == 'password' || tag == 'textarea')
						this.value = '';
					  else if (type == 'checkbox' || type == 'radio')
						this.checked = false;
					  else if (tag == 'select')
						this.selectedIndex = -1;
					});
					$(':input', self.loginForm).each(function() {
					  var type = this.type, tag = this.tagName.toLowerCase();
					  if (type == 'text' || type == 'password' || tag == 'textarea')
						this.value = '';
					  else if (type == 'checkbox' || type == 'radio')
						this.checked = false;
					  else if (tag == 'select')
						this.selectedIndex = -1;
					});
					$(':input', self.updateHandleForm).each(function() {
					  var type = this.type, tag = this.tagName.toLowerCase();
					  if (type == 'text' || type == 'password' || tag == 'textarea')
						this.value = '';
					  else if (type == 'checkbox' || type == 'radio')
						this.checked = false;
					  else if (tag == 'select')
						this.selectedIndex = -1;
					});
				};
				
				self.displayError = function(errorText, inputField) {
					if (inputField) {
						logger.log("Adding error '" + errorText + "' with inputField=" + inputField.attr('class'));
						// Mark the field as having an error
						inputField.addClass("error");
					} else {
						logger.log("Adding error '" + errorText +"'");
					}

					// Add the error message to the page.
					var errorHtml = '<p class="comments-error-msg" style="display: block;">* ' + errorText + '</p>';
					inputField.after(errorHtml);
				};

				self.displayCommentBodyError = function(errorText, inputField) {
					var commentFormDiv = inputField.closest('.cleditorMain');
					logger.log("Adding error '" + errorText + "' with inputField=" + commentFormDiv.attr('class'));
					// Mark the field as having an error
					commentFormDiv.addClass("error");

					// Add the error message to the page.
					var errorHtml = '<p class="comments-error-msg" style="display: block;">* ' + errorText + '</p>';
					commentFormDiv.parent().prepend(errorHtml);
				};
				

				self.removeErrors = function(commentForm) {
					// Remove error-highlighting
					$(commentForm).find('.error').removeClass('error');

					// Remove error messages; refactor if we ever move to top/bottom forms
					$('.comments-error-msg').remove();
				}
				
				self.writeComment = function(comment) {
					logger.log("Writing new comment");
					var language =  $('html').attr('lang') || 'en';
					moment.locale(language, {
			    		monthsShort : ctx.i18n('date.shortMonths').split(',')    
					});
					var commentDate = comment.date_gmt + 'Z';
					var postDate = moment(commentDate).format('D MMM YYYY');
					var postTime = language == 'fr' ? moment(commentDate).format('H:mm') : moment(commentDate).format('h:mm A');
					var title = encodeURI($('.main-article-title').text());
					var mailToAddress = 'mailto:socialmedia@techtarget.com?subject=PLEASE REVIEW COMMENT ' + comment.id + ': ' + title + '&body=Please review a recent comment for inappropriate content.%0A%0AURL: ' + window.location + '%0AAuthor: ' + comment.author_name + '%0AComment ID: ' +  comment.id;
					var div_data;					
					
					var comment_to_insert;
					if( comment.parent == 0) {
						//parent level comment
						var commentClass = 'comment-parent';	
					} else {
						var commentClass = 'comment-child';
					}
					comment_to_insert = '<div class="'+commentClass+'" data-sort="'+moment(comment.date_gmt)+'">';
					comment_to_insert += '<div id="'+comment.solrId+'" class="comment-wrapper">';
					comment_to_insert += '<div class="comment-content">';
						comment_to_insert += '<div class="comment-user-info"><a href="#" class="comment-expand-toggle">[-]</a><div class="comment-thumbnail-wrap"><img class="comment-thumbnail" src="'+comment.userAvatar+'"></div><span class="comment-username">'+comment.author_name+'</span><span class="comment-date">&nbsp;-&nbsp;' + postDate + '&nbsp;' + postTime + ' </span></div>';
						comment_to_insert += '<div class="comment-body">'+comment.content.rendered+'</div>';
						comment_to_insert += '<input class="comments-form-btn" type="submit" value="'+ctx.i18n('commenting.reply')+'">';
						comment_to_insert += '<form class="comments-reply-form"><textarea class="comments-form-textarea" placeholder="'+ctx.i18n('commenting.share_your_comment')+'"></textarea><button type="submit" class="comments-form-btn submit" href="#comments-reg-login" data-comment-parent="'+comment.solrId+'">'+ctx.i18n('commenting.add_my_comment')+'</button><a href="#" class="cancel-reply">'+ctx.i18n('commenting.cancel')+'</a></form>';
					comment_to_insert += '</div>';
					comment_to_insert += '<div class="comment-share"><ul><li class="comment-flag"><a href="'+mailToAddress+'"><i class="icon" data-icon="t"></i> &nbsp;</a></li></ul></div>';						
					comment_to_insert += '</div>';
					
					// Either writes comments to the beginning or end of the comment list depending on the page's sort order.
					if (self.sortByOldest) {
						if( comment.parent == 0) {
							//parent level comment
							self.commentList.append($(comment_to_insert).hide().fadeIn(2000));	
						} else {
							//child level comment
							var parentComment = '_comment_'+comment.parent; 
							//close comment textarea
							$('[id$='+parentComment+']').find('.cancel-reply').trigger('click');
							self.commentList.find('[id$='+parentComment+']').parent().append($(comment_to_insert).hide().fadeIn(2000));	
						}										
					} else {
						if( comment.parent == 0) {
							//parent level comment
							self.commentList.prepend($(comment_to_insert).hide().fadeIn(2000));	
						} else {
							//child level comment
							var parentComment = '_comment_'+comment.parent; 
							//close comment textarea
							$('[id$='+parentComment+']').find('.cancel-reply').trigger('click');
							self.commentList.find('[id$='+parentComment+']').after($(comment_to_insert).hide().fadeIn(2000));	
						}							
					}
					
					//cleditorize new textarea
					$("#"+comment.solrId+" .comments-form-textarea").cleditor({
						controls: ttFrontEndEditor.data.cleditor.controls,
						width: '100%',
						height: '200',
						bodyStyle: "margin:4px; font:21px 'NeueHaasGroteskText W01', Helvetica, Arial, sans-serif; cursor:text;color:#666;"
					});
					 $("div[title='Code']").toggleClass('ttCleditorButton');
					 $("div[title='Code']").after('<div class="cleditorDivider"></div>');
					 $("#"+comment.solrId+" .cleditorMain").css('width', '100%');
					
					//rebind buttons
					self.bindButtons("#"+comment.solrId);

				};

				/**
				 * Click Events
				 */
				self.submitComment = function() {
					// Remove any existing errors
					self.removeErrors(self.submittedForm);

					// If user needs to register/login/update handle, go through minireg. Otherwise, just post comment.
					if (!self.user.authenticated || !self.user.hasValidHandle) {
						self.miniReg();
					} else {
						self.postComment();
					}
				}	

				// Event for clicking 'forgot password' on the top comment form
				$(self.forgotPasswordSelector).on('click', '.forgot-password, .forgotPasswordLink', function(e) {
					e.preventDefault();
					if (TT.mq < TT.mqs.tablet) {
						var height = "260px";
						var width = "320px";
					} else {
						var height = "270px";
						var width = "630px";
					}
					// forgot password modal
					$.colorbox({inline: true, scrolling: false, height: height, width: width, opacity: '0.50', href: "#forgotPasswordModal", onOpen: function(){

						// Backfill field with whatever user had entered in email field.
						var userEmail = self.loginForm.find('input.comments-email').val();

						$('#forgotPasswordModalFormMessage').val(userEmail);

					}});
					// shows confirm after user submits forgot password
					$('#forgotPasswordModalFormSubmit').colorbox({inline: true, scrolling: false, height: height, width: width, opacity: '0.50',  href: "#forgotPasswordModalConfirm", onLoad: function() {

						// address that user ultimately submitted	
						var userEmail = $('#forgotPasswordModalFormMessage').val();
						
						// Set email in confirmation message
						$('#forgotPasswordModalHeader .email').text(userEmail);

						// Call userreg to fire off email
						self.user.sendForgotPassword(userEmail);
					}});
				});
				
				$('.main-article-comment-count').click(function(e) {
					// Anchor to top of comment section head taking fixed nav into play
					e.preventDefault();
					$('html,body').animate({scrollTop: $('#commenting').offset().top - 160},'slow');
				});

				self.bindModalButtons = function() {
					//bind create username modal
					self.updateHandleForm.on('click', '.comments-form-btn', function(e) {
						e.preventDefault();
						logger.log("Submitting update handle form...");
						
						self.registerForm = false;
						self.submittedForm = self.updateHandleForm;						
						
						self.submitComment();
					});	
					
					//bind submission to reg modal submit button
					self.regLoginForm.on('click', '.comments-form-btn', function(e) {
						e.preventDefault();
						logger.log("Submitting logged-out form...");
						
						// determine if login or reg form being submitted
						var buttonId = $(this).attr('id');
						logger.log("Submission from "+buttonId+".");
						if( buttonId == 'submit-register' ) {
							self.registerForm = true;
							self.submittedForm = self.regForm;
						} else if( buttonId == 'submit-login' ) {
							self.registerForm = false;
							self.submittedForm = self.loginForm;
						}						
						
						self.submitComment();
					});	
				}
				
				self.bindButtons = function(container) {
					logger.log("Binding buttons for "+container);
					
					if( container == self.item ) {
						// Reply button
						$('.comment-wrapper .comment-content > .comments-form-btn').on('click', function(){
							$(this).next().show();
							$(this).hide();
						});
					} else {
						// Reply button
						$(container).parent().find('.comment-wrapper .comment-content > .comments-form-btn').on('click', function(){
							$(this).next().show();
							$(this).hide();
						});
					}
					
					// Cancel button
					$(container).find('.cancel-reply').on('click', function(e){
						e.preventDefault();
						self.removeErrors(self.item);
						$(' form .cleditorMain iframe').contents().find('body').html('');
						$(this).parent().hide();
						$(this).parent().prev().show();
					});
					
					// Textarea to CLEditor
					// ux updates to cleditor (placeholder text)
					var cleditorDefaultText = $('.comments-form-textarea').attr('placeholder');
					$(".comments-form-textarea").clEditorUXenhancements(cleditorDefaultText);
					
					// Collapsing comments
					$(container + ' .comment-expand-toggle').on('click', function(e){
						e.preventDefault();
						var $commentsToggle = $(this);
						var $commentsSection = $(this).parents('.comment-wrapper').parent('div');
						var $commentsChildrenNum = $commentsSection.find('.comment-child').length;

						if($commentsSection.hasClass('closed')){
							$commentsToggle.text('[-]');
							$commentsSection.removeClass('closed');
							$commentsSection.find('.comment-user-info .comment-num').remove();
						}
						else {
							$commentsToggle.text('[+]');
							$commentsSection.addClass('closed');
							$commentsSection.find('.comment-user-info').append('<span class=comment-num>' + '(' + $commentsChildrenNum + ' ' + ctx.i18n('commenting.replies') + ')');
						}
					});

					// Collapsing comments form
					$(container + ' .comments-center .section-title').on('click', function(e){
						e.preventDefault();
						var $commentsFormToggle = $(this);
						var $commentsFormSection = $('.comments-center .comments-form');

						if($commentsFormSection.hasClass('closed')){
							$commentsFormSection.removeClass('closed');
						}
						else {
							$commentsFormSection.addClass('closed');
						}

						if($commentsFormToggle.hasClass('closed')){
							$commentsFormToggle.removeClass('closed');
						}
						else {
							$commentsFormToggle.addClass('closed');
						}
					});
					
					//bind button based on user state	
					$(container).on('click', '.comments-form-btn.submit', function(e) {
						e.preventDefault();						
						// self.submittedForm is form being submitted
						self.submittedForm = $(this).form;						
						self.submittedButton = $(this);
						//closest comment textarea
						self.commentTextArea = $(this).parent().find('.comments-form-textarea');
						//check for reply
						if ($(this).attr('data-comment-parent')) {
							self.commentParent = $(this).attr('data-comment-parent');
						} else {
							self.commentParent = '';
						}

						
						if (self.user.authenticated && self.user.hasValidHandle) {
							logger.log("User is logged-in with valid handle.");	
							self.registerForm = false;
							self.submitComment();
						} else if(self.user.authenticated && !self.user.hasValidHandle) {
							logger.log("User is logged-in but missing handle.");	
							//clear any existing errors
							self.submittedForm = self.updateHandleForm;
							self.removeErrors(self.submittedForm);
							self.clearModalFields();
							$(".comments-form-btn.submit").attr("href", "#comments-reg-create");
							if (TT.mq < TT.mqs.tablet) {
								$.colorbox({inline: true, scrolling: false, width:"320px", height:"310px",opacity: '0.50', href: "#comments-reg-create"});
							} else {
								$.colorbox({inline: true, scrolling: false, width:"630px", height:"320px",opacity: '0.50', href: "#comments-reg-create"});
							}					
						} else {
							//bind logged out user
							logger.log("User is logged out.");																								
							//clear any existing errors
							self.removeErrors(self.submittedForm);			
							self.clearModalFields();
							//show reglogin modal
							if (TT.mq < TT.mqs.tablet) {
								$.colorbox({inline: true, scrolling: false, width:"320px", height:"600px",opacity: '0.50', href: "#comments-reg-login"});
							} else {
								$.colorbox({inline: true, scrolling: false, width:"630px", height:"560px",opacity: '0.50', href: "#comments-reg-login"});
							}
						}
					});	
				}

				// Order the comments.
				$(self.item).on('click', '.comments-order', function(e) { 
					  e.preventDefault();
					  logger.log("Sorting comments. Currently sort-by-oldest = " + self.sortByOldest);

					  // Change the page's sort order (this will be used for displaying the newly sorted comments)
					  self.sortByOldest = !self.sortByOldest;
					  
					  var $comments = self.commentList.children('.comment-parent');
					  
					  if( self.sortByOldest ) {
						  	$('#sortByOldest').show();
							$('#sortByNewest').hide();
						} else {
							$('#sortByOldest').hide();
							$('#sortByNewest').show();
						}

					  //sort comments, aka top level
					  $comments.sort(function(a,b){
						  if( self.sortByOldest ) {
							return $(a).attr("data-sort") - $(b).attr("data-sort");
						  } else {
							return $(b).attr("data-sort") - $(a).attr("data-sort");
						  }
					  });

					  //sort replies
					  $comments.each(function() {
						var p = $(this);
						p.append(p.children(".comment-child").get().sort(function(a,b) {
							if( self.sortByOldest ) {
								return $(a).attr("data-sort") - $(b).attr("data-sort");
							  } else {
								return $(b).attr("data-sort") - $(a).attr("data-sort");
							  }
						}));
					  });

					  //re-write comments
					  $comments.detach().appendTo(self.commentList);
		      });

				/**
				 * Section initialization
				 */

				// This function dictates how this section is presented on page load.
				self.initializeItem = function() {
					logger.log("Creating comment section....");

					// Determine the text of the comments based on type.
					if (self.type == 'commentSection') {
						self.postTypeSingular = ctx.i18n('commenting.comment_lowercase');
						self.postTypePlural = ctx.i18n('commenting.comments_lowercase');
					}
					//update counts
					self.updateCounts(false);					
					//Change copy of button if user is logged in without handle
					if(self.user.authenticated && !self.user.hasValidHandle) {
						//top form button
						$(".comments-form-btn.submit").html(ctx.i18n('commenting.create_username_and_add_my_comment'));
						$(".comments-form-btn.submit").attr("href", "#comments-reg-create");
					}
					
					//bind buttons
					self.bindButtons('#commenting');
					
					//bind buttons in login/reg and handle modals
					self.bindModalButtons();										
				};
			};						

			// If a comment section exists, create a userreg user and assign it to/initialize comments.
			if (repoComments.length) {
				logger.log("Enabling commenting and initializing user...");
				var repoUser = new UserregUser();
				repoUser.siteId = ctx.siteId;

				repoUser.initialize().always( function() {
					var commentSection = new commentItem('#commenting', 'commentSection');
					commentSection.user = repoUser;
					commentSection.initializeItem();
				});
			};
			if(homeRepoComments.length){
				logger.log("Loading comment count for home page splash...");
				var postCountConfig = 15; //TO-DO: Set as a config somewhere
				for (i = 1; i <= postCountConfig; i++) {
					var lineId = '#homesplashcommenting-'+i;
					var commentSection = new commentItem(lineId, 'commentSection');
					commentSection.initializeHomeSplashItem(lineId,i);
				}
			};

		}
	};

	$(function(){
		Commenting.init();
	});

});
TT([ "context", "util/debug", "util/logging","util/url", "lib/jquery" ], function(ctx, debug, logging, url, $) {

	var isDebug = debug.check("debug_contactus");
	var logger = logging.getLogger(isDebug);

	var ContactUsForm = {
		init: function() {

		$('#contactUsForm').submit(function (event) {

		logger.log("User submitted contact us form.");
		var errors = new Array();
		var senderName = $('#sender_name').val();
		var siteName = $('#site_name').val();
		var textBody = $('#emailTextBody').val();
		var receiveAddress = $('#contactUsFormContact').val();
		var senderAddress = $('#from_Address').val();  
			// Check for empty fields and display alert errors if empty
			if(!senderAddress.length || receiveAddress == 'SELOPT' || !textBody.length) {		
					errors = [];
					if (!senderAddress.length) {
						errors[0] = ctx.i18n('contactUsForm.please_enter_a_emailaddress');
					} 
					if ( receiveAddress == 'SELOPT') {
						errors[1] = ctx.i18n('contactUsForm.please_select_a_contact_option');
					} 
					if (!textBody.length) {
						errors[2] = ctx.i18n('contactUsForm.please_enter_feedback');
					}
					alert(errors.join("\n"));
					return false;
			} else {
				// if no errors, check for captcha, if empty return false
				if(!grecaptcha.getResponse().length) {

					$('#recaptcha_only_if_incorrect_sol').show();
		   			return false;

				} else {
				// if no errors and captcha pass, conduct ajax post of email data 
					datap = { 
						emailTextBody: ctx.i18n('contactUsForm.email_text', senderName, siteName, textBody),
						toAddresses: [{name: receiveAddress, emailId: receiveAddress}],
						fromAddress: {name: senderAddress, emailId: senderAddress},
						emailSubject: ctx.i18n('contactUsForm.email_subject')
					};

					$.ajax({
					    type: "POST",
					    url: "/techtarget-ecm/services/email/post",
					    contentType: "application/json; charset=utf-8",
					    dataType: "json",
					    data: JSON.stringify(datap),
					    success: function(json) {
					    }
					});
				// hide form and display thank you message once sucessfully submitted	
					event.preventDefault();
					$('#submit_thankyou_msg').css("display", "block");
					document.getElementsByClassName("required")[0].style.visibility = "hidden";
					$('#contactUsForm').hide();
					$('body').scrollTop(0);
			}
		}

		});
	}
}
	$(function() {
		ContactUsForm.init();
	});

});
// On preview, console log gets overriden by vuit.js and becomes undefined. This restores it so we can actually dev.
TT(["context", "lib/jquery"], function (ctx, $) {

	if (ctx.isPreview) {
		$(function () {
			if (console.log == 'function (){}') {
				delete window.console;
			}
			$("body").on("click", "img.x-form-arrow-trigger", function () {
				$('div.x-layer').css({zIndex: 999999});
			});
		});
	}

});
// UI Banner Messages module
TT(["context", "util/logging", "util/cookie", "lib/jquery", "util/debug"], function (ctx, logging, cookie, $, debug) {

	var isDebug = debug.check("debug_ui");
	var logger = logging.getLogger(isDebug);
	var BannerMessages = {

		init: function() {
			BannerMessages.displayPrivacyPolicyMessage();			
		},

		displayPrivacyPolicyMessage: function() {
			// Cookie expires after 90 days from release day
			var cookieName = "tt_privacy_policy_change";
			var bannerExpDateString = "Sat, 21 Mar 2015 05:00:00 GMT";

			var now = new Date();
			var bannerExpDate = new Date(bannerExpDateString);
			var cookieExists = cookie.getCookie(cookieName);
			if (now > bannerExpDate) {
				logger.log("Privacy policy banner is no longer displayed.");
				if (cookieExists) {
					logger.log("Removing banner cookie...");
					cookie.deleteCookie(cookieName);
				}		
				return;
			}
			if (cookieExists) {
				// User should only see the banner once. Keep cookie until 90 days have past.
				logger.log("User has already seen privacy policy banner.");
				return;
			}
			if (ctx.lang != "en") {
				logger.log("Privacy policy banner will not be displayed on foreign sites");
				return;
			}

			logger.log("Adding privacy policy banner to page...");
			var messageHtml = '<div class="header-privacy-policy">' +
				'<span class="pp-strong">Our Privacy Policy has changed.</span>' + 
				'<span>By continuing to use this site, you are agreeing to the new ' +
				'<a href="http://www.techtarget.com/html/privacy_policy.html" target="_blank">' +
				'Privacy Policy</a>.</span></div>';
			$('.site-container').prepend(messageHtml);
			
			cookie.setDomainCookie(cookieName, true, bannerExpDateString);
		}
	};

	$(function(){
		BannerMessages.init();
	});

});
/**
 * Module for DataPoint Media (DPM) integration
 */
TT(["context", "util/logging", "util/debug", "lib/jquery"],
	function (ctx, logging, debug, $) {

	var isDebug = debug.check("debug_dpm"),
		isTest = ctx.envName != "prod" || ctx.isPreview;

	// Exit if page is not prod delivery unless debugging
	if (isTest && !isDebug) {
		return;
	}

	var logger = logging.getLogger(isDebug);

	// ClikIQ: VGNSITES-15161 VGNSITES-15225 VGNSITES-15281
	logger.log("ClikIQ: fetching userid from user");
	ctx.getUser(function (user) {
		var userid = user.uid;
		if (userid) {
			logger.log("ClikIQ: userid=%s", userid);
			if (isTest) {
				userid = "-" + userid;
				logger.log("ClikIQ: negating userid for test (%s)", userid);
			}
			var url = "https://a.dpmsrv.com/dpmpxl/index.php?q=ttgtSync&ttgtID=" + userid;
			logger.log("ClikIQ: sending url=%s", url);
			$.getScript(url)
				.done(function (data, textStatus) {
					logger.log("ClikIQ: %s", textStatus);
				})
				.fail(function (jqXHR, textStatus, errorThrown) {
					logger.log("ClikIQ %s: %s", textStatus, errorThrown);
				});
		} else {
			logger.log("ClikIQ: userid is not known. will not call endpoint");
		}
	});

});
// UI pinning module
TT(["context", "context/ui", "util/url", "lib/jquery"], function (ctx, ui, url, $) {

	var pinning = url.getParam("pinning");

	if (pinning) {
		$('head').append('<link rel="stylesheet" href="http://cdn.ttgtmedia.com/rms/ux/responsive/css/screenshot.css" type="text/css" />');
	}

});
