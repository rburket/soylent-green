package com.techtarget.soylentgreen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CentralTaxonomyClient {
	
	static final String PATH = "/solr/vignette/central-taxonomy?&sort=publishDate desc&q=*:*&joinString=vignetteContentIds:{vcmId}&fl=title,url&rows={rows}&wt=json";

	private String baseUri = "http://cluster.solr.eng.techtarget.com";
	
	@Autowired @Qualifier("restTemplate")
	private RestTemplate restTemplate;
	
	public CentralTaxonomyResponse findRelatedContent(String vcmId, Integer rows) {
		return restTemplate.getForObject(baseUri + PATH, CentralTaxonomyResponse.class,
				new Object[] { vcmId, rows });
	}
}
