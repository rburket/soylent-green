package com.techtarget.soylentgreen;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class SocialController {
	
	@Autowired
	private CentralTaxonomyClient centralTaxonomyClient;
    
    @RequestMapping("/soylentgreen")
    public String greeting( Model model) {
        
        CentralTaxonomyResponse resp = centralTaxonomyClient.findRelatedContent("1c1f7153962bc510VgnVCM100000ef01c80aRCRD", 4);
        model.addAttribute("docs", resp.getResponse().getDocs());
        
        
         
        return "social_wrapper";
    }


}
