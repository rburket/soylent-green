
package com.techtarget.soylentgreen;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "fl",
    "sort",
    "joinString",
    "q",
    "wt",
    "rows"
})
public class Params {

    @JsonProperty("fl")
    private String fl;
    @JsonProperty("sort")
    private String sort;
    @JsonProperty("joinString")
    private String joinString;
    @JsonProperty("q")
    private String q;
    @JsonProperty("wt")
    private String wt;
    @JsonProperty("rows")
    private String rows;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("fl")
    public String getFl() {
        return fl;
    }

    @JsonProperty("fl")
    public void setFl(String fl) {
        this.fl = fl;
    }

    @JsonProperty("sort")
    public String getSort() {
        return sort;
    }

    @JsonProperty("sort")
    public void setSort(String sort) {
        this.sort = sort;
    }

    @JsonProperty("joinString")
    public String getJoinString() {
        return joinString;
    }

    @JsonProperty("joinString")
    public void setJoinString(String joinString) {
        this.joinString = joinString;
    }

    @JsonProperty("q")
    public String getQ() {
        return q;
    }

    @JsonProperty("q")
    public void setQ(String q) {
        this.q = q;
    }

    @JsonProperty("wt")
    public String getWt() {
        return wt;
    }

    @JsonProperty("wt")
    public void setWt(String wt) {
        this.wt = wt;
    }

    @JsonProperty("rows")
    public String getRows() {
        return rows;
    }

    @JsonProperty("rows")
    public void setRows(String rows) {
        this.rows = rows;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
