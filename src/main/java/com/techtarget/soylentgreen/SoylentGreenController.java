package com.techtarget.soylentgreen;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.catalina.servlet4preview.ServletContext;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SoylentGreenController {
	
	@Autowired
	private CentralTaxonomyClient centralTaxonomyClient;
	
    @RequestMapping("/")
    public String index() {
        return "Soylent Green is people!!!";
    }
	
    @RequestMapping(path = "/related", method = RequestMethod.GET)
    public @ResponseBody CentralTaxonomyResponse getRelated(@RequestParam String vcmId, @RequestParam Integer rows) {
    	// This is just for testing that the related content stuff works.  It simply returns whatever is received from Solr.
        return centralTaxonomyClient.findRelatedContent(vcmId, rows);
    }
    

    @Autowired
    ServletContext servletContext;
    
    @RequestMapping(value = "/images", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] testphoto() throws IOException {
        InputStream in = new FileInputStream(Utils.getRandomImageFile(servletContext));
        return IOUtils.toByteArray(in);
    }



//	@ExceptionHandler
//	void handleIllegalArgumentException(Exception e, HttpServletResponse response) throws IOException {
//	    response.sendError(HttpStatus.BAD_REQUEST.value(), e.toString());
//	}

}
