package com.techtarget.soylentgreen;

import java.io.File;
import java.util.Random;

import org.apache.catalina.servlet4preview.ServletContext;

public class Utils {

	/**
	 * getRandomImageFile
	 * @param servletContext
	 * @return
	 */
    public static File getRandomImageFile(ServletContext servletContext){   	
    	File dir = new File(servletContext.getRealPath("/SG_IMAGES/"));
    	File[] files = dir.listFiles();
    	Random rand = new Random();
    	File file = files[rand.nextInt(files.length)];
    	return file;
    }
	
}
