package com.techtarget.soylentgreen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.techtarget.common.services.HttpClient4Factory;
import com.techtarget.common.services.HttpClientConfig;


@SpringBootApplication
public class SoylentGreenApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(SoylentGreenApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SoylentGreenApplication.class);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate(httpRequestFactory());
	}

	private ClientHttpRequestFactory httpRequestFactory() {

		HttpClientConfig httpClientConfig = new HttpClientConfig();
		httpClientConfig.setTimeout(3000);
		httpClientConfig.setStaleCheckingEnabled(true);

		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setHttpClient(HttpClient4Factory.getHttpClient(httpClientConfig));
		factory.setConnectTimeout(3000);

		return factory;
	}
}
