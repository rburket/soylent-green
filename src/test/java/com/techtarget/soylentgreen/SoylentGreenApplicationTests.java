package com.techtarget.soylentgreen;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SoylentGreenApplicationTests {
	
	@Autowired
	private CentralTaxonomyClient centralTaxonomyClient;

	@Test
	public void shouldReturnResults() {

        CentralTaxonomyResponse response = centralTaxonomyClient.findRelatedContent("1c1f7153962bc510VgnVCM100000ef01c80aRCRD", 10);
        
        
        for (Doc doc : response.getResponse().getDocs()) {
        	System.out.println(doc.getTitle() + "=>" + doc.getUrl());
        }
	}

}
